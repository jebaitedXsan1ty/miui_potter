## mktool v3.0
![image](tools/menu.png)

mktool is for unpacking & repacking the android boot,
recovery, or loki images and also for loki patching.

**Tested on ubuntu 12.04 to 16.04**

**Project page:**
https://intechgeek.com/mktool/

## Usage
Make sure the "mktool" file has execute permissions.

1. Place your images in the input folder.
2. Execute the mktool file as follows in terminal: `./mktool`
3. optional - Use the "Add Desktop Shortcut" option for quick access

### Resources
- [mkbootimg](https://github.com/osm0sis/mkbootimg)
- [loki](https://github.com/djrbliss/loki)
