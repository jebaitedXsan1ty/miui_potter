.class public Lcom/android/incallui/AnswerFragment;
.super Lcom/android/incallui/BaseFragment;
.source "AnswerFragment.java"

# interfaces
.implements Lcom/android/incallui/AnswerPresenter$AnswerUi;
.implements Lcom/android/incallui/view/AnswerLayout$SlideEndListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseFragment",
        "<",
        "Lcom/android/incallui/AnswerPresenter;",
        "Lcom/android/incallui/AnswerPresenter$AnswerUi;",
        ">;",
        "Lcom/android/incallui/AnswerPresenter$AnswerUi;",
        "Lcom/android/incallui/view/AnswerLayout$SlideEndListener;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

.field private mRootView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/incallui/AnswerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/incallui/AnswerFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/BaseFragment;-><init>()V

    return-void
.end method

.method private varargs videoCallStateRecord(ZZ[Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/AnswerFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/AnswerPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/AnswerPresenter;->isVtConferenceCall()Z

    move-result v1

    invoke-virtual {p0}, Lcom/android/incallui/AnswerFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/AnswerPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/AnswerPresenter;->isVideoCall()Z

    move-result v0

    invoke-static {p1, p2, v1, v0, p3}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordVideoCallCountEvent(ZZZZ[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public configureMessageDialog(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/AnswerLayout;->configureMessageDialog(Ljava/util/List;)V

    return-void
.end method

.method public createPresenter()Lcom/android/incallui/AnswerPresenter;
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->getAnswerPresenter()Lcom/android/incallui/AnswerPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createPresenter()Lcom/android/incallui/Presenter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/AnswerFragment;->createPresenter()Lcom/android/incallui/AnswerPresenter;

    move-result-object v0

    return-object v0
.end method

.method protected getUi()Lcom/android/incallui/AnswerPresenter$AnswerUi;
    .locals 0

    return-object p0
.end method

.method protected bridge synthetic getUi()Lcom/android/incallui/Ui;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/AnswerFragment;->getUi()Lcom/android/incallui/AnswerPresenter$AnswerUi;

    move-result-object v0

    return-object v0
.end method

.method public onAnswer(I)V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/incallui/AnswerFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/AnswerPresenter;

    invoke-virtual {v0, p1}, Lcom/android/incallui/AnswerPresenter;->onAnswer(I)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "video_call_answer_conference"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "video_call_answer_video"

    aput-object v1, v0, v3

    const-string/jumbo v1, "video_call_answer_voice"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-direct {p0, v3, v3, v0}, Lcom/android/incallui/AnswerFragment;->videoCallStateRecord(ZZ[Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f030003

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/AnswerFragment;->mRootView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/AnswerFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/AnswerPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/AnswerPresenter;->clearCall()V

    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/AnswerLayout;->clearAll()V

    iput-object v1, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/android/incallui/AnswerFragment;->mRootView:Landroid/view/View;

    :cond_1
    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onDestroyView()V

    return-void
.end method

.method public onMessage(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/incallui/AnswerFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/AnswerPresenter;

    invoke-virtual {v0, p1}, Lcom/android/incallui/AnswerPresenter;->rejectCallWithMessage(Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "video_call_conference_incoming_message"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "video_call_incoming_message"

    aput-object v1, v0, v3

    const-string/jumbo v1, "video_call_voice_incoming_message"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-direct {p0, v3, v3, v0}, Lcom/android/incallui/AnswerFragment;->videoCallStateRecord(ZZ[Ljava/lang/String;)V

    return-void
.end method

.method public onReject()V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/incallui/AnswerFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/AnswerPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/AnswerPresenter;->onDecline()V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "video_call_conference_incoming_end_call"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "video_call_incoming_end_call"

    aput-object v1, v0, v3

    const-string/jumbo v1, "video_call_voice_incoming_end_call"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-direct {p0, v3, v3, v0}, Lcom/android/incallui/AnswerFragment;->videoCallStateRecord(ZZ[Ljava/lang/String;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onStart()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/incallui/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    check-cast p1, Lcom/android/incallui/view/SlideUpLayout;

    iput-object p1, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    invoke-virtual {v0, p0}, Lcom/android/incallui/view/AnswerLayout;->setSlideEndListener(Lcom/android/incallui/view/AnswerLayout$SlideEndListener;)V

    return-void
.end method

.method public resetAnswerUi()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/AnswerLayout;->resetAnswerUi()V

    return-void
.end method

.method public setVideoCall(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/AnswerLayout;->setVideoCall(Z)V

    return-void
.end method

.method public showAnswerUi(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/AnswerLayout;->showAnswerUi(Z)V

    return-void
.end method

.method public showTextButton(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/AnswerFragment;->mAnswerLayout:Lcom/android/incallui/view/AnswerLayout;

    invoke-virtual {v0, p1}, Lcom/android/incallui/view/AnswerLayout;->showTextButton(Z)V

    return-void
.end method
