.class public Lcom/android/incallui/CallCardPresenter;
.super Lcom/android/incallui/Presenter;
.source "CallCardPresenter.java"

# interfaces
.implements Lcom/android/incallui/InCallPresenter$InCallStateListener;
.implements Lcom/android/incallui/InCallPresenter$IncomingCallListener;
.implements Lcom/android/incallui/InCallPresenter$InCallDialPadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/CallCardPresenter$1;,
        Lcom/android/incallui/CallCardPresenter$CallCardState;,
        Lcom/android/incallui/CallCardPresenter$CallCardUi;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/Presenter",
        "<",
        "Lcom/android/incallui/CallCardPresenter$CallCardUi;",
        ">;",
        "Lcom/android/incallui/InCallPresenter$InCallStateListener;",
        "Lcom/android/incallui/InCallPresenter$IncomingCallListener;",
        "Lcom/android/incallui/InCallPresenter$InCallDialPadListener;"
    }
.end annotation


# static fields
.field private static final synthetic -com-android-incallui-CallCardPresenter$CallCardStateSwitchesValues:[I


# instance fields
.field private mCallTimer:Lcom/android/incallui/CallTimer;

.field private mContactInfoCache:Lcom/android/incallui/ContactInfoCache;

.field private mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

.field private mContext:Landroid/content/Context;

.field private mCrbtHoldCallPrompt:Ljava/lang/String;

.field private mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

.field private mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

.field private mDialPadVisible:Z

.field private mDigitsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private mDoublePrimaryCallTimer:Lcom/android/incallui/CallTimer;

.field private mDoubleSecondaryCallTimer:Lcom/android/incallui/CallTimer;

.field private mForceProcessIncoming:Z

.field private mIsCrbtInDouble:Z

.field private mIsCurrentCallConference:Z

.field private mIsCurrentCallVideoConference:Z

.field private mIsCurrentVideoCall:Z

.field private mIsCurrentVideoCrbtCall:Z

.field private mIsCurrentVideoCrbtVtCall:Z

.field private mIsIncomingCall:Z

.field private mIsLastCallConference:Z

.field private mIsLastVideoCall:Z

.field private mLastCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

.field private mLastInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

.field private mLastPrimaryCallId:Ljava/lang/String;

.field private mLastPrimaryCallKey:Ljava/lang/String;

.field private mLastRingCallKey:Ljava/lang/String;

.field private mLastSecondaryCallKey:Ljava/lang/String;

.field private mPrimary:Lcom/android/incallui/Call;

.field private mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

.field private mRingCall:Lcom/android/incallui/Call;

.field protected mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

.field private mSecondary:Lcom/android/incallui/Call;

.field private mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

.field private mVideoCallTimer:Lcom/android/incallui/CallTimer;

.field private mVideoHoldCallTimer:Lcom/android/incallui/CallTimer;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/CallCardPresenter;)Lcom/android/incallui/CallCardPresenter$CallCardState;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/CallCardPresenter;)Lcom/android/incallui/InCallPresenter$InCallState;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/incallui/CallCardPresenter;)Lcom/android/incallui/Call;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/incallui/CallCardPresenter;)Lcom/android/incallui/Call;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/incallui/CallCardPresenter;)Lcom/android/incallui/Call;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    return-object v0
.end method

.method private static synthetic -getcom-android-incallui-CallCardPresenter$CallCardStateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/incallui/CallCardPresenter;->-com-android-incallui-CallCardPresenter$CallCardStateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/incallui/CallCardPresenter;->-com-android-incallui-CallCardPresenter$CallCardStateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/incallui/CallCardPresenter$CallCardState;->values()[Lcom/android/incallui/CallCardPresenter$CallCardState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_SECONDARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_SECONDARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->SINGLE_CALL_ACTIVE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->SINGLE_CALL_DIALING:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    sput-object v0, Lcom/android/incallui/CallCardPresenter;->-com-android-incallui-CallCardPresenter$CallCardStateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1

    :catch_8
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/incallui/CallCardPresenter;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/incallui/CallCardPresenter;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/CallCardPresenter;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/CallCardPresenter;->handleAvatarAndPhotoByCallCardState(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/incallui/CallCardPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->handleSetImageByCallCardState()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/incallui/CallCardPresenter;Ljava/lang/String;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/CallCardPresenter;->updateContactEntry(Ljava/lang/String;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/incallui/Presenter;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mDigitsMap:Ljava/util/HashMap;

    iput-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    iput-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallConference:Z

    iput-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsLastCallConference:Z

    iput-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallVideoConference:Z

    iput-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    iput-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsLastVideoCall:Z

    iput-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    iput-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtVtCall:Z

    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mLastInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mLastCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iput-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mLastRingCallKey:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mLastPrimaryCallKey:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mLastSecondaryCallKey:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mLastPrimaryCallId:Ljava/lang/String;

    new-instance v0, Lcom/android/incallui/CallCardPresenter$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/CallCardPresenter$1;-><init>(Lcom/android/incallui/CallCardPresenter;)V

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    new-instance v0, Lcom/android/incallui/CallTimer;

    new-instance v1, Lcom/android/incallui/CallCardPresenter$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardPresenter$2;-><init>(Lcom/android/incallui/CallCardPresenter;)V

    invoke-direct {v0, v1}, Lcom/android/incallui/CallTimer;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    new-instance v0, Lcom/android/incallui/CallTimer;

    new-instance v1, Lcom/android/incallui/CallCardPresenter$3;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardPresenter$3;-><init>(Lcom/android/incallui/CallCardPresenter;)V

    invoke-direct {v0, v1}, Lcom/android/incallui/CallTimer;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mDoublePrimaryCallTimer:Lcom/android/incallui/CallTimer;

    new-instance v0, Lcom/android/incallui/CallTimer;

    new-instance v1, Lcom/android/incallui/CallCardPresenter$4;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardPresenter$4;-><init>(Lcom/android/incallui/CallCardPresenter;)V

    invoke-direct {v0, v1}, Lcom/android/incallui/CallTimer;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mDoubleSecondaryCallTimer:Lcom/android/incallui/CallTimer;

    new-instance v0, Lcom/android/incallui/CallTimer;

    new-instance v1, Lcom/android/incallui/CallCardPresenter$5;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardPresenter$5;-><init>(Lcom/android/incallui/CallCardPresenter;)V

    invoke-direct {v0, v1}, Lcom/android/incallui/CallTimer;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mVideoCallTimer:Lcom/android/incallui/CallTimer;

    new-instance v0, Lcom/android/incallui/CallTimer;

    new-instance v1, Lcom/android/incallui/CallCardPresenter$6;

    invoke-direct {v1, p0}, Lcom/android/incallui/CallCardPresenter$6;-><init>(Lcom/android/incallui/CallCardPresenter;)V

    invoke-direct {v0, v1}, Lcom/android/incallui/CallTimer;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mVideoHoldCallTimer:Lcom/android/incallui/CallTimer;

    return-void
.end method

.method private controlBannerPosition(ZZZ)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->controlSingleCallLayoutTopMargin()V

    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    iget-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtVtCall:Z

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->controlPrimaryCallBannerTopMargin(ZZZZ)V

    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    invoke-interface {v0, p1, p3, v1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->controlPrimaryName(ZZZ)V

    return-void
.end method

.method private createCallCardInfoByDialpad(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Z)Lcom/android/incallui/model/CallCardInfo;
    .locals 3

    invoke-static {}, Lcom/android/incallui/model/CallCardInfo;->createEmptyCardInfo()Lcom/android/incallui/model/CallCardInfo;

    move-result-object v0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    return-object v0

    :cond_1
    invoke-static {p1, p2}, Lcom/android/incallui/model/CallCardInfo;->createCallCardInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    if-eqz v1, :cond_2

    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mDigitsMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_2
    invoke-static {p2, p1}, Lcom/android/incallui/CallCardPresenter;->getNameForCall(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method private getCallConnectedTime(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;)J
    .locals 10

    if-nez p2, :cond_0

    const-wide/16 v8, -0x1

    return-wide v8

    :cond_0
    invoke-virtual {p2}, Lcom/android/incallui/Call;->getConnectTimeMillis()J

    move-result-wide v6

    invoke-virtual {p2}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p2}, Lcom/android/incallui/Call;->getChildCallIds()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/android/incallui/CallList;->getCallById(Ljava/lang/String;)Lcom/android/incallui/Call;

    move-result-object v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "CallCardPresenter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "getCallConnectedTime: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " was not found in call list"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v0}, Lcom/android/incallui/CallList;->getCallById(Ljava/lang/String;)Lcom/android/incallui/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/Call;->getConnectTimeMillis()J

    move-result-wide v4

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    move-wide v6, v4

    goto :goto_0

    :cond_3
    const-wide/16 v8, 0x64

    cmp-long v3, v6, v8

    if-gez v3, :cond_4

    const-wide/16 v6, 0x0

    :cond_4
    return-wide v6
.end method

.method private getCallToDisplay(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;Z)Lcom/android/incallui/Call;
    .locals 1

    invoke-virtual {p1}, Lcom/android/incallui/CallList;->getActiveCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eq v0, p2, :cond_0

    return-object v0

    :cond_0
    if-nez p3, :cond_2

    invoke-virtual {p1}, Lcom/android/incallui/CallList;->getDisconnectingCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eq v0, p2, :cond_1

    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/android/incallui/CallList;->getDisconnectedCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eq v0, p2, :cond_2

    return-object v0

    :cond_2
    invoke-virtual {p1}, Lcom/android/incallui/CallList;->getBackgroundCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eq v0, p2, :cond_3

    return-object v0

    :cond_3
    invoke-virtual {p1}, Lcom/android/incallui/CallList;->getSecondBackgroundCall()Lcom/android/incallui/Call;

    move-result-object v0

    return-object v0
.end method

.method private getHoldCallPromptWhenCrbt(Lcom/android/incallui/Call;)Ljava/lang/String;
    .locals 6

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00ae

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_0
    const-string/jumbo v1, ""

    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->getInstance()Lcom/android/incallui/ContactInfoCache;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/incallui/ContactInfoCache;->getInfo(Ljava/lang/String;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v2, v0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/android/incallui/model/YellowPageInfo;->isYellowPage()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/android/incallui/model/YellowPageInfo;->getYellowPageName()Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const v5, 0x7f0b00ac

    invoke-virtual {v3, v5, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_3
    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mContext:Landroid/content/Context;

    const v4, 0x7f0b00ad

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static getNameForCall(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/Call;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isVioceMailNumber:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isEmergencyNumber:Z

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->number:Ljava/lang/String;

    return-object v0

    :cond_3
    invoke-virtual {p1}, Lcom/android/incallui/Call;->getNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->name:Ljava/lang/String;

    return-object v0
.end method

.method private getVideoState(Lcom/android/incallui/Call;)I
    .locals 2

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getVideoState()I

    move-result v0

    invoke-virtual {p1}, Lcom/android/incallui/Call;->isVolteCallPlayingVideoCrbt()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method private handleAvatarAndPhotoByCallCardState(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)V
    .locals 11

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v6

    check-cast v6, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v6, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-static {}, Lcom/android/incallui/CallCardPresenter;->-getcom-android-incallui-CallCardPresenter$CallCardStateSwitchesValues()[I

    move-result-object v9

    iget-object v10, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v10}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    invoke-static {v0}, Lcom/android/incallui/model/CallCardInfo;->getCardInfoPhotoType(Landroid/graphics/drawable/Drawable;)I

    move-result v5

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/incallui/CallList;->hasLiveCall()Z

    move-result v3

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mLastCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v9}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isHasOnlyOneRingCall()Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v9}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isHasNonRingActiveCall()Z

    move-result v9

    if-eqz v9, :cond_2

    move v1, v3

    :goto_1
    iget-boolean v9, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    if-eqz v9, :cond_3

    const-string/jumbo v9, "incoming show big avatar"

    invoke-static {p0, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v7, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->controlBigAvatar(ZZ)V

    :goto_2
    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v9}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v2

    sget-object v9, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v9, :cond_7

    const/4 v4, 0x1

    :goto_3
    iget-boolean v9, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    if-eqz v9, :cond_c

    iget-boolean v9, p0, Lcom/android/incallui/CallCardPresenter;->mForceProcessIncoming:Z

    if-nez v9, :cond_8

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v10, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v9, v10, :cond_8

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/incallui/CallList;->hasActiveOrBackgroundVideoCall()Z

    move-result v9

    if-eqz v9, :cond_8

    const-string/jumbo v7, "has incoming call in video, hide small avatar for incoming"

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    invoke-direct {p0, v8, v8, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    :goto_4
    return-void

    :pswitch_1
    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v9, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_2
    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v9, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_3
    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v9, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_4
    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v9, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_4

    const-string/jumbo v9, "incoming to nonincoming, hide big avatar"

    invoke-static {p0, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->controlBigAvatar(ZZ)V

    goto :goto_2

    :cond_4
    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v10, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v9, v10, :cond_5

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->isSingleCallDisconnect()Z

    move-result v9

    if-eqz v9, :cond_6

    :cond_5
    const-string/jumbo v9, "current state idle, keep state, do nothing"

    invoke-static {p0, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    const-string/jumbo v9, "other state, hide big avatar"

    invoke-static {p0, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->controlBigAvatar(ZZ)V

    goto/16 :goto_2

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_8
    if-eq v5, v7, :cond_9

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/incallui/InCallApp;->isUseLockWallPaper()Z

    move-result v9

    if-eqz v9, :cond_a

    if-nez v5, :cond_a

    :cond_9
    const-string/jumbo v9, "has big avatar, hide small avatar for incoming"

    invoke-static {p0, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    invoke-direct {p0, v7, v8, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto :goto_4

    :cond_a
    const-string/jumbo v9, "no big avatar, show small avatar for incoming"

    invoke-static {p0, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v9, p0, Lcom/android/incallui/CallCardPresenter;->mForceProcessIncoming:Z

    if-eqz v9, :cond_b

    :goto_5
    invoke-interface {v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->getSingleCallerInfoTopSpace(I)I

    move-result v7

    invoke-interface {v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showSmallAvatar(I)V

    invoke-direct {p0, v8, v8, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :cond_b
    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getCallCount()I

    move-result v7

    goto :goto_5

    :cond_c
    if-eqz v1, :cond_d

    const-string/jumbo v7, "ring to nonring, call back make small avatar show"

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_d
    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v9}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isNonRingDoubleCall()Z

    move-result v9

    if-eqz v9, :cond_e

    const-string/jumbo v9, "hide small avatar for double nonring call"

    invoke-static {p0, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    invoke-direct {p0, v7, v2, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :cond_e
    iget-boolean v9, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallConference:Z

    if-eqz v9, :cond_f

    const-string/jumbo v9, "hide small avatar for conference"

    invoke-static {p0, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    invoke-direct {p0, v7, v8, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :cond_f
    if-eqz v2, :cond_11

    iget-boolean v9, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    if-eqz v9, :cond_11

    iget-boolean v9, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    if-eqz v9, :cond_10

    const-string/jumbo v9, "dialpad has pressed, don\'t show avatar for crbt"

    invoke-static {p0, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    invoke-direct {p0, v8, v7, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :cond_10
    const-string/jumbo v8, "dialpad has pressed, don\'t show avatar"

    invoke-static {p0, v8}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    invoke-direct {p0, v7, v7, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :cond_11
    iget-boolean v9, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    if-eqz v9, :cond_13

    const-string/jumbo v7, "current call is video, hide avatar"

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    if-eqz v2, :cond_12

    iget-boolean v7, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    :goto_6
    invoke-direct {p0, v7, v2, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :cond_12
    move v7, v8

    goto :goto_6

    :cond_13
    invoke-virtual {p1}, Lcom/android/incallui/InCallPresenter$InCallState;->isInDialingState()Z

    move-result v9

    if-nez v9, :cond_16

    invoke-virtual {p2}, Lcom/android/incallui/InCallPresenter$InCallState;->isInDialingState()Z

    move-result v9

    if-eqz v9, :cond_16

    iget-boolean v7, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    if-eqz v7, :cond_14

    const-string/jumbo v7, "from non dialing to dialing, hide avatar for crbt"

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    invoke-direct {p0, v8, v2, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :cond_14
    const-string/jumbo v7, "from non dialing to dialing, show small avatar"

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->controlSmallAvatarTranslateY()V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getCallCount()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->getSingleCallerInfoTopSpace(I)I

    move-result v7

    invoke-interface {v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showSmallAvatar(I)V

    if-eqz v2, :cond_15

    iget-boolean v8, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    :cond_15
    invoke-direct {p0, v8, v2, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :cond_16
    invoke-virtual {p2}, Lcom/android/incallui/InCallPresenter$InCallState;->isInDialingState()Z

    move-result v9

    if-eqz v9, :cond_17

    iget-boolean v9, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    if-eqz v9, :cond_17

    const-string/jumbo v7, "is dialing, hide avatar for crbt"

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    invoke-direct {p0, v8, v2, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :cond_17
    sget-object v9, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v10, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v9, v10, :cond_18

    const-string/jumbo v8, "no calls, hide small avatar"

    invoke-static {p0, v8}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    goto/16 :goto_4

    :cond_18
    const-string/jumbo v7, "other state, show small avatar"

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getCallCount()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->getSingleCallerInfoTopSpace(I)I

    move-result v7

    invoke-interface {v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showSmallAvatar(I)V

    invoke-direct {p0, v8, v2, v4}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private handleCallCardVideoViewByCallCardState()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-boolean v3, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/CallList;->getActiveOrBackgroundCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mDigitsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showVideoDigitsToField(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-interface {v2, v4}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showVideoDigitsToField(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-boolean v3, p0, Lcom/android/incallui/CallCardPresenter;->mIsLastVideoCall:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    invoke-interface {v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->initSingleVideoCallInfoRotation()V

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v3}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isSingleCall()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v4, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v3, v4, :cond_5

    :cond_4
    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v3}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v3, v4}, Lcom/android/incallui/CallCardPresenter;->getNameForCall(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSinglePhoneNumber(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v4, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_SECONDARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-direct {p0, v3}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-static {v3, v4}, Lcom/android/incallui/CallCardPresenter;->getNameForCall(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSinglePhoneNumber(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleCallCardViewByCallCardState()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v1, v2, :cond_1

    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v1, v2, :cond_3

    :cond_1
    invoke-interface {v0, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallInfoVisible(Z)V

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-direct {p0, v1}, Lcom/android/incallui/CallCardPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/incallui/CallAdapterUtils;->isUserUnLocked(Landroid/content/Context;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showUserLockedPrompt(Z)V

    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCrbtInDouble:Z

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCrbtHoldCallPrompt:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showCrbtPrompt(ZLjava/lang/String;)V

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isNonRingDoubleCall()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallInfoVisible(Z)V

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->updateDoubleDisplayInfo()V

    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isRingDoubleCall()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isSingleCall()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallInfoVisible(Z)V

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-direct {p0, v1}, Lcom/android/incallui/CallCardPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v1, v2, :cond_2

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    goto :goto_0
.end method

.method private handleCallTimerByCallCardState()V
    .locals 5

    const/4 v4, 0x3

    const-wide/16 v2, 0x3e8

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v0}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isRingDoubleCall()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v0}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isNonRingDoubleCall()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v0}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isNonRingDoubleCall()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/CallList;->hasActiveOrBackgroundVideoCall()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mVideoHoldCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallTimer;->start(J)Z

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mVideoCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallTimer;->start(J)Z

    :goto_0
    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateCallTime()V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateVideoCallTime()V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mDoublePrimaryCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallTimer;->start(J)Z

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateDoubleSecondaryCallTime()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    if-ne v0, v4, :cond_3

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateDoublePrimaryCallTime()V

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mDoubleSecondaryCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallTimer;->start(J)Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateDoublePrimaryCallTime()V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateDoubleSecondaryCallTime()V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v0, v1, :cond_6

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/CallList;->hasActiveOrBackgroundVideoCall()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mDoublePrimaryCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallTimer;->start(J)Z

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mDoubleSecondaryCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0}, Lcom/android/incallui/CallTimer;->cancel()V

    :goto_2
    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateCallTime()V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateVideoCallTime()V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mVideoCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallTimer;->start(J)Z

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    if-eq v0, v4, :cond_7

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_9

    :cond_7
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallTimer;->start(J)Z

    iget-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mVideoCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallTimer;->start(J)Z

    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mDoublePrimaryCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0}, Lcom/android/incallui/CallTimer;->cancel()V

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mDoubleSecondaryCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0}, Lcom/android/incallui/CallTimer;->cancel()V

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mVideoHoldCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0}, Lcom/android/incallui/CallTimer;->cancel()V

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->SINGLE_CALL_DIALING:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v0, v1, :cond_a

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateCallTime()V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->updateVideoCallTime()V

    goto :goto_3

    :cond_a
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0}, Lcom/android/incallui/CallTimer;->cancel()V

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mVideoCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0}, Lcom/android/incallui/CallTimer;->cancel()V

    goto :goto_3
.end method

.method private handleCallTypeByCallCardState()V
    .locals 9

    const/4 v8, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isRingDoubleCall()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v1, v2, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-direct {p0, v2}, Lcom/android/incallui/CallCardPresenter;->getVideoState(Lcom/android/incallui/Call;)I

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->getSessionModificationState()I

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v4

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v5

    invoke-interface/range {v0 .. v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallState(IIIZLandroid/telecom/DisconnectCause;I)V

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isRingDoubleCall()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isNonRingDoubleCall()Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_3
    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->hasActiveOrBackgroundVideoCall()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v2, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v2, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v1, v2, :cond_6

    :cond_4
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoHoldCallState(ILandroid/telecom/DisconnectCause;Z)V

    :cond_5
    :goto_0
    return-void

    :cond_6
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoHoldCallState(ILandroid/telecom/DisconnectCause;Z)V

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoublePrimaryCallState(ILandroid/telecom/DisconnectCause;Z)V

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleSecondaryCallState(ILandroid/telecom/DisconnectCause;Z)V

    goto :goto_0

    :cond_8
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v1, v2, :cond_9

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->hasActiveOrBackgroundVideoCall()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v2

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoublePrimaryCallState(ILandroid/telecom/DisconnectCause;Z)V

    goto :goto_0

    :cond_9
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isSingleCall()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v2}, Lcom/android/incallui/CallCardPresenter;->getVideoState(Lcom/android/incallui/Call;)I

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->getSessionModificationState()I

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v4

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v5

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getRedialTimes()I

    move-result v6

    invoke-interface/range {v0 .. v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallState(IIIZLandroid/telecom/DisconnectCause;I)V

    goto/16 :goto_0

    :cond_a
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v1, v2, :cond_b

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-direct {p0, v2}, Lcom/android/incallui/CallCardPresenter;->getVideoState(Lcom/android/incallui/Call;)I

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v3}, Lcom/android/incallui/Call;->getSessionModificationState()I

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v4

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v5

    invoke-interface/range {v0 .. v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallState(IIIZLandroid/telecom/DisconnectCause;I)V

    goto/16 :goto_0

    :cond_b
    new-instance v5, Landroid/telecom/DisconnectCause;

    invoke-direct {v5, v7}, Landroid/telecom/DisconnectCause;-><init>(I)V

    const/4 v1, 0x2

    move v2, v7

    move v3, v7

    move v4, v7

    invoke-interface/range {v0 .. v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallState(IIIZLandroid/telecom/DisconnectCause;I)V

    invoke-interface {v0, v7, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallElapsedTime(ZLjava/lang/String;)V

    goto/16 :goto_0
.end method

.method private handlePrimaryBannerByCallCardState()V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v0

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    sget-object v5, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne v4, v5, :cond_2

    const/4 v1, 0x1

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v5, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v4, v5, :cond_4

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/CallList;->hasActiveOrBackgroundVideoCall()Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ring call in video,show banner, mForceProcessIncoming = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/android/incallui/CallCardPresenter;->mForceProcessIncoming:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mForceProcessIncoming:Z

    if-eqz v4, :cond_3

    invoke-interface {v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showIncomingBanner()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-interface {v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideBanner(Z)V

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v4}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isHasRingCall()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mLastCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v4}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isHasRingCall()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_5

    const-string/jumbo v3, "ring call,show banner"

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showIncomingBanner()V

    goto :goto_1

    :cond_5
    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallConference:Z

    if-eqz v4, :cond_6

    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallVideoConference:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_6

    const-string/jumbo v3, "volte conference dial or ring to noring conference call"

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showIncomingBanner()V

    goto :goto_1

    :cond_6
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mLastInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-virtual {v4}, Lcom/android/incallui/InCallPresenter$InCallState;->isInDialingState()Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-virtual {v4}, Lcom/android/incallui/InCallPresenter$InCallState;->isInDialingState()Z

    move-result v4

    if-eqz v4, :cond_7

    const-string/jumbo v4, "dial, show banner"

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v3, v0, v1}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    invoke-interface {v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showDialBanner()V

    goto :goto_1

    :cond_7
    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsLastVideoCall:Z

    if-eqz v4, :cond_b

    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_b

    const-string/jumbo v4, "from video to non video, show banner"

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->getState()I

    move-result v4

    const/16 v5, 0x9

    if-eq v4, v5, :cond_8

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->getState()I

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_9

    :cond_8
    invoke-direct {p0, v3, v0, v1}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    :goto_2
    invoke-interface {v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showIncomingBanner()V

    goto :goto_1

    :cond_9
    if-eqz v0, :cond_a

    iget-boolean v3, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    :cond_a
    invoke-direct {p0, v3, v0, v1}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto :goto_2

    :cond_b
    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    if-eqz v4, :cond_c

    const-string/jumbo v4, "current is video, hide banner"

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideBanner(Z)V

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showVideoBanner(Z)V

    goto/16 :goto_1

    :cond_c
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mLastCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v5, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v4, v5, :cond_d

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v5, Lcom/android/incallui/CallCardPresenter$CallCardState;->SINGLE_CALL_ACTIVE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v4, v5, :cond_d

    const-string/jumbo v4, "auto answer call, same as dial call, show banner"

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v3, v0, v1}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    invoke-interface {v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showDialBanner()V

    goto/16 :goto_1

    :cond_d
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mLastCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v5, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v4, v5, :cond_e

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v4}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isNonRingDoubleCall()Z

    move-result v4

    if-eqz v4, :cond_e

    const-string/jumbo v3, "idle-> double call, show banner ready for call change to one"

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showIncomingBanner()V

    goto/16 :goto_1

    :cond_e
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    sget-object v5, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne v4, v5, :cond_1

    const-string/jumbo v4, "has no calls, hide banner"

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideBanner(Z)V

    invoke-interface {v2, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showVideoBanner(Z)V

    invoke-direct {p0, v3, v0, v3}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    goto/16 :goto_1
.end method

.method private handleSetImageByCallCardState()V
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v5

    check-cast v5, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v5, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/incallui/CallCardPresenter;->-getcom-android-incallui-CallCardPresenter$CallCardStateSwitchesValues()[I

    move-result-object v7

    iget-object v8, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v8}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "unknown call card state:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    :goto_0
    :pswitch_0
    invoke-static {v0}, Lcom/android/incallui/model/CallCardInfo;->getCardInfoPhotoType(Landroid/graphics/drawable/Drawable;)I

    move-result v4

    const/4 v3, 0x0

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/android/incallui/model/YellowPageInfo;->getCreditImg()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    xor-int/lit8 v3, v7, 0x1

    :cond_2
    if-ne v4, v10, :cond_3

    iget-boolean v7, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_4

    :cond_3
    invoke-interface {v5, v0, v3}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSmallAvatar(Landroid/graphics/drawable/Drawable;Z)V

    :cond_4
    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v7}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isHasRingCall()Z

    move-result v2

    if-ne v4, v10, :cond_5

    invoke-interface {v5, v0, v1, v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setCallCardImage(Landroid/graphics/drawable/Drawable;ZZ)V

    :goto_1
    return-void

    :pswitch_1
    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v7, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v6, v7, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-boolean v1, v7, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isEmergencyNumber:Z

    goto :goto_0

    :pswitch_2
    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v7, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v6, v7, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-boolean v1, v7, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isEmergencyNumber:Z

    goto :goto_0

    :pswitch_3
    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v7, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v6, v7, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-boolean v1, v7, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->isEmergencyNumber:Z

    goto :goto_0

    :cond_5
    invoke-interface {v5, v9, v1, v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setCallCardImage(Landroid/graphics/drawable/Drawable;ZZ)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private hasVideoDoubleDisplayInfo()Z
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    if-eqz v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v0

    if-eqz v0, :cond_2

    return v2

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_SECONDARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v0

    if-eqz v0, :cond_3

    return v2

    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method private isCallDisconnect(Lcom/android/incallui/Call;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_0

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v2

    const/16 v3, 0x9

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    return v1
.end method

.method private isCurrentCallConference()Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/incallui/CallCardPresenter;->-getcom-android-incallui-CallCardPresenter$CallCardStateSwitchesValues()[I

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v2}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown call card state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isConference(Lcom/android/incallui/Call;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isConference(Lcom/android/incallui/Call;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isConference(Lcom/android/incallui/Call;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private isCurrentCallVideoConference()Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/incallui/CallCardPresenter;->-getcom-android-incallui-CallCardPresenter$CallCardStateSwitchesValues()[I

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v2}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown call card state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isVideoConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isVideoConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isVideoConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private isDialpadPressed(Lcom/android/incallui/Call;)Z
    .locals 4

    const/4 v1, 0x0

    if-nez p1, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mDigitsMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private isNeedUpdateUI(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)Z
    .locals 5

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/Call;->getCallKeyForCallPresenter(Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/Call;->getCallKeyForCallPresenter(Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/Call;->getCallKeyForCallPresenter(Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v3

    if-ne p1, p2, :cond_0

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mLastRingCallKey:Ljava/lang/String;

    invoke-static {v4, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mLastPrimaryCallKey:Ljava/lang/String;

    invoke-static {v4, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mLastSecondaryCallKey:Ljava/lang/String;

    invoke-static {v4, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v0, v4, 0x1

    :goto_0
    iput-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mLastRingCallKey:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mLastPrimaryCallKey:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mLastSecondaryCallKey:Ljava/lang/String;

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSingleCallDisconnect()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getState()I

    move-result v2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getState()I

    move-result v2

    const/16 v3, 0x9

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    return v1
.end method

.method private updateContactCacheEntry()V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCache:Lcom/android/incallui/ContactInfoCache;

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/ContactInfoCache;->maybeUpdateContactInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCache:Lcom/android/incallui/ContactInfoCache;

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/ContactInfoCache;->maybeUpdateContactInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCache:Lcom/android/incallui/ContactInfoCache;

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/ContactInfoCache;->maybeUpdateContactInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-void
.end method

.method private updateContactEntry(Ljava/lang/String;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V
    .locals 1

    const-string/jumbo v0, "CCP.uCE"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    if-nez p1, :cond_0

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p2, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-direct {p0, v0}, Lcom/android/incallui/CallCardPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    :goto_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object p2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_5

    :cond_3
    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->updateDoubleDisplayInfo()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p2, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-direct {p0, v0}, Lcom/android/incallui/CallCardPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    goto :goto_0
.end method

.method private updateCurrentVideoCrbtCall()V
    .locals 2

    invoke-static {}, Lcom/android/incallui/CallCardPresenter;->-getcom-android-incallui-CallCardPresenter$CallCardStateSwitchesValues()[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unknown call card state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isPlayingVideoCrbt()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isVtCallPlayingVideoCrbt()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtVtCall:Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isPlayingVideoCrbt()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isVtCallPlayingVideoCrbt()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtVtCall:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateDoubleDisplayInfo()V
    .locals 13

    const/16 v10, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    const-string/jumbo v4, "updateDoubleDisplayInfo()"

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->hasVideoDoubleDisplayInfo()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleCallInfoVisible(Z)V

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->updateHasVideoDoubleDisplayInfo()V

    return-void

    :cond_1
    invoke-interface {v0, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoHoldCallInfoVisible(Z)V

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    if-eqz v4, :cond_7

    :cond_2
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v4, :cond_4

    invoke-interface {v0, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleCallInfoVisible(Z)V

    invoke-interface {v0, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoublePrimaryCallInfoVisible(Z)V

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v2

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v5

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->getState()I

    move-result v4

    if-eq v4, v10, :cond_3

    move v4, v7

    :goto_0
    invoke-direct {p0, v6, v9, v4}, Lcom/android/incallui/CallCardPresenter;->createCallCardInfoByDialpad(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Z)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v1

    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getPrimaryCallNum()I

    move-result v6

    invoke-interface/range {v0 .. v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoublePrimary(Lcom/android/incallui/model/CallCardInfo;ZZZZI)V

    iget-boolean v4, v1, Lcom/android/incallui/model/CallCardInfo;->isSuspect:Z

    invoke-interface {v0, v4}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->updateSuspectInfo(Z)V

    :goto_1
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v4, :cond_6

    invoke-interface {v0, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleSecondaryCallInfoVisible(Z)V

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v5

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v9}, Lcom/android/incallui/Call;->getState()I

    move-result v9

    if-eq v9, v10, :cond_5

    :goto_2
    invoke-direct {p0, v4, v6, v7}, Lcom/android/incallui/CallCardPresenter;->createCallCardInfoByDialpad(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Z)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v1

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v8

    iget-boolean v10, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getSecondaryCallNum()I

    move-result v12

    move-object v6, v0

    move-object v7, v1

    move v9, v3

    move v11, v5

    invoke-interface/range {v6 .. v12}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleSecondary(Lcom/android/incallui/model/CallCardInfo;ZZZZI)V

    :goto_3
    return-void

    :cond_3
    move v4, v8

    goto :goto_0

    :cond_4
    invoke-interface {v0, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoublePrimaryCallInfoVisible(Z)V

    goto :goto_1

    :cond_5
    move v7, v8

    goto :goto_2

    :cond_6
    invoke-interface {v0, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleSecondaryCallInfoVisible(Z)V

    goto :goto_3

    :cond_7
    invoke-interface {v0, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleCallInfoVisible(Z)V

    goto :goto_3
.end method

.method private updateHasVideoDoubleDisplayInfo()V
    .locals 8

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v5

    check-cast v5, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v5, :cond_0

    return-void

    :cond_0
    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v6, :cond_4

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v6, v7, :cond_1

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v6, v7, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    :goto_0
    if-eqz v2, :cond_2

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoHoldCallInfoVisible(Z)V

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v3

    invoke-static {v0}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v4

    invoke-static {v0, v2}, Lcom/android/incallui/model/CallCardInfo;->createCallCardInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v1

    invoke-interface {v5, v1, v3, v4}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoHoldInfo(Lcom/android/incallui/model/CallCardInfo;ZZ)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    goto :goto_0

    :cond_4
    const/4 v6, 0x0

    invoke-interface {v5, v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoHoldCallInfoVisible(Z)V

    goto :goto_1
.end method

.method private updateInfoByState()V
    .locals 4

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->isCurrentCallConference()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallConference:Z

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->isCurrentVideoCall()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->isCurrentCallVideoConference()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallVideoConference:Z

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->updateCurrentVideoCrbtCall()V

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->updateContactCacheEntry()V

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->handleCallCardViewByCallCardState()V

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsLastCallConference:Z

    iget-boolean v3, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallConference:Z

    if-eq v2, v3, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v2}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mLastCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v3, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v2, v3, :cond_4

    :goto_2
    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->handleSetImageByCallCardState()V

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mLastInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-direct {p0, v2, v3}, Lcom/android/incallui/CallCardPresenter;->handleAvatarAndPhotoByCallCardState(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)V

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->handlePrimaryBannerByCallCardState()V

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->handleCallCardVideoViewByCallCardState()V

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->handleCallTypeByCallCardState()V

    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->handleCallTimerByCallCardState()V

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mLastPrimaryCallId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallConference:Z

    iput-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsLastCallConference:Z

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iput-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mLastCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    iput-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsLastVideoCall:Z

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mLastPrimaryCallId:Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    goto :goto_2
.end method

.method private updateSingleDisplayInfo(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V
    .locals 10

    const-string/jumbo v4, "CallCardPresenter"

    const-string/jumbo v9, "updateSingleDisplayInfo: update single display"

    invoke-static {v4, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v0, :cond_0

    const-string/jumbo v4, "CallCardPresenter"

    const-string/jumbo v9, "updateSingleDisplayInfo: ui is null, return."

    invoke-static {v4, v9}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/CallUtils;->isThreeWayIncoming(Lcom/android/incallui/Call;)Z

    move-result v7

    const/4 v2, 0x0

    const/4 v6, 0x0

    if-eqz p1, :cond_3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    if-eqz v4, :cond_3

    :cond_1
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    :goto_0
    iput-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v2

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/CallUtils;->isVtConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v6

    :goto_1
    if-eqz v7, :cond_2

    const/4 v2, 0x0

    :cond_2
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/CallUtils;->isThreeWayOutgoing(Lcom/android/incallui/Call;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x0

    :cond_3
    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    if-eqz v4, :cond_7

    iget-object v8, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    :goto_2
    invoke-static {v8, p1}, Lcom/android/incallui/model/CallCardInfo;->createCallCardInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v1

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mDigitsMap:Ljava/util/HashMap;

    iget-object v9, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v9}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    :cond_4
    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getPrimaryCallNum()I

    move-result v4

    invoke-interface/range {v0 .. v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallInfo(Lcom/android/incallui/model/CallCardInfo;ZZIZZ)V

    iget-boolean v4, v1, Lcom/android/incallui/model/CallCardInfo;->isSuspect:Z

    invoke-interface {v0, v4}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->updateSuspectInfo(Z)V

    return-void

    :cond_5
    const/4 v4, 0x0

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v4}, Lcom/android/incallui/Call;->isConferenceCall()Z

    move-result v2

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v4}, Lcom/android/incallui/CallUtils;->isVtConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v6

    goto :goto_1

    :cond_7
    iget-object v8, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    goto :goto_2
.end method


# virtual methods
.method public activeHoldCall()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->unholdCall(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->unholdCall(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public activePrimaryCall()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->unholdCall(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public activeSecondaryCall()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->unholdCall(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public appendDigitsToField(C)V
    .locals 14

    const/4 v5, 0x0

    const/4 v2, 0x1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->getFirstCall()Lcom/android/incallui/Call;

    move-result-object v7

    if-eqz v7, :cond_5

    const/4 v9, 0x0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mDigitsMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/StringBuilder;

    if-nez v6, :cond_0

    const/4 v9, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mDigitsMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-lez v10, :cond_1

    add-int/lit8 v1, v10, -0x1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eq v1, v3, :cond_1

    const/16 v1, 0x20

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getCurrentCallCardState()Lcom/android/incallui/CallCardPresenter$CallCardState;

    move-result-object v11

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-eqz v0, :cond_5

    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    if-eqz v1, :cond_2

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showVideoDigitsToField(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->isCurrentCallConference()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "appendDigitsToField is ignored in conference."

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_3
    invoke-virtual {v11}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isSingleCall()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v1, v3}, Lcom/android/incallui/CallCardPresenter;->getNameForCall(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v12

    const/4 v4, 0x0

    if-eqz v9, :cond_4

    invoke-interface {v0, v12}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSinglePhoneNumber(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move v3, v2

    invoke-interface/range {v0 .. v5}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallName(Ljava/lang/String;ZZLjava/lang/String;Z)V

    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v1}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v1

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    sget-object v13, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne v3, v13, :cond_6

    :goto_0
    invoke-direct {p0, v5, v1, v2}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    :cond_5
    :goto_1
    return-void

    :cond_6
    move v2, v5

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v1}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v1

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    sget-object v13, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne v3, v13, :cond_8

    move v5, v2

    :cond_8
    invoke-direct {p0, v2, v1, v5}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    invoke-interface {v0, v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    goto :goto_1

    :cond_9
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v11, v1, :cond_a

    const/4 v4, 0x0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2, v2, v4}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoublePrimaryName(Ljava/lang/String;ZZLjava/lang/String;)V

    goto :goto_1

    :cond_a
    sget-object v1, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_SECONDARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v11, v1, :cond_5

    const/4 v4, 0x0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2, v2, v4}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleSecondaryName(Ljava/lang/String;ZZLjava/lang/String;)V

    goto :goto_1
.end method

.method public enablePrimaryHangUp()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v0}, Lcom/android/incallui/CallUtils;->isCDMAPhone(Lcom/android/incallui/Call;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCallCount()I
    .locals 4

    const/16 v3, 0xa

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_4

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    if-eq v1, v3, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_3

    :cond_2
    add-int/lit8 v0, v0, -0x1

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    if-ne v1, v3, :cond_4

    add-int/lit8 v0, v0, -0x1

    :cond_4
    return v0
.end method

.method public getCurrentCallCardState()Lcom/android/incallui/CallCardPresenter$CallCardState;
    .locals 3

    const/16 v2, 0x8

    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    if-ne v1, v2, :cond_0

    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_SECONDARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    if-ne v1, v2, :cond_4

    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_SECONDARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter$InCallState;->isInDialingState()Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->SINGLE_CALL_DIALING:Lcom/android/incallui/CallCardPresenter$CallCardState;

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->SINGLE_CALL_ACTIVE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    goto :goto_0

    :cond_7
    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    goto :goto_0
.end method

.method public getPrimaryCallNum()I
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getChildCallIds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getSecondaryCallNum()I
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getChildCallIds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method hideBanner(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string/jumbo v1, "incoming to nonincoming, hideBanner first"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideBanner(Z)V

    return-void
.end method

.method public hungUpPrimaryCall()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->disconnectCall(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public hungUpSecondaryCall()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->disconnectCall(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public init(Lcom/android/incallui/Call;)V
    .locals 4

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->getInstance()Lcom/android/incallui/ContactInfoCache;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCache:Lcom/android/incallui/ContactInfoCache;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v0

    invoke-static {v0}, Lcom/android/incallui/Call$State;->isRinging(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    :goto_0
    iget-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCache:Lcom/android/incallui/ContactInfoCache;

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/ContactInfoCache;->maybeUpdateContactInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    :cond_0
    :goto_1
    sget-object v0, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->getInCallState()Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/incallui/CallCardPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    return-void

    :cond_1
    iput-object p1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCache:Lcom/android/incallui/ContactInfoCache;

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/ContactInfoCache;->maybeUpdateContactInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    goto :goto_1
.end method

.method public isCurrentVideoCall()Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/incallui/CallCardPresenter;->-getcom-android-incallui-CallCardPresenter$CallCardStateSwitchesValues()[I

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v2}, Lcom/android/incallui/CallCardPresenter$CallCardState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown call card state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v1}, Lcom/android/incallui/CallCardPresenter;->isCallDisconnect(Lcom/android/incallui/Call;)Z

    move-result v1

    xor-int/lit8 v0, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-direct {p0, v1}, Lcom/android/incallui/CallCardPresenter;->isCallDisconnect(Lcom/android/incallui/Call;)Z

    move-result v1

    xor-int/lit8 v0, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isDialPadVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    return v0
.end method

.method public markNumberSettingGuide(Lcom/android/incallui/model/YellowPageInfo;Z)V
    .locals 7

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/incallui/model/YellowPageInfo;->getCid()I

    move-result v0

    invoke-virtual {p1}, Lcom/android/incallui/model/YellowPageInfo;->isMarkedSuspect()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mContext:Landroid/content/Context;

    invoke-static {v4, v0}, Landroid/provider/MiuiSettings$AntiSpam;->shouldShowGuidingDialog(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v5, "mark_guide_type"

    if-eqz p2, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v4, "mark_guide_yellowpage_cid"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v4, Landroid/content/ComponentName;

    const-string/jumbo v5, "com.miui.securitycenter"

    const-string/jumbo v6, "com.miui.antispam.ui.activity.MarkNumGuideActivity"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :try_start_0
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v4, 0x2

    goto :goto_0

    :catch_0
    move-exception v1

    const-string/jumbo v4, "CallCardPresenter"

    const-string/jumbo v5, "markNumberSettingGuide has error: "

    invoke-static {v4, v5, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public onDialPadVisibleChanged(Z)V
    .locals 8

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v3, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->isCurrentCallConference()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/incallui/CallList;->hasLiveCall()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    :cond_1
    const-string/jumbo v4, "onDialPadVisibleChanged is ignored in conference or no calls"

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_2
    iput-boolean p1, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    invoke-interface {v3, p1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDialPadVisible(Z)V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getCurrentCallCardState()Lcom/android/incallui/CallCardPresenter$CallCardState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isSingleCall()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCrbtCall:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    return-void

    :cond_3
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-direct {p0, v4, v5}, Lcom/android/incallui/CallCardPresenter;->handleAvatarAndPhotoByCallCardState(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)V

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    :cond_4
    iget-boolean v4, p0, Lcom/android/incallui/CallCardPresenter;->mDialPadVisible:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-static {v4, v5}, Lcom/android/incallui/CallCardPresenter;->getNameForCall(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSinglePhoneNumber(Ljava/lang/String;)V

    :cond_5
    :goto_0
    return-void

    :cond_6
    sget-object v4, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v2, v4, :cond_8

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_7

    return-void

    :cond_7
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-direct {p0, v4, v5, v6}, Lcom/android/incallui/CallCardPresenter;->createCallCardInfoByDialpad(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Z)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v0

    iget-object v4, v0, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-boolean v5, v0, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v6}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v6

    iget-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    invoke-interface {v3, v4, v5, v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoublePrimaryName(Ljava/lang/String;ZZLjava/lang/String;)V

    goto :goto_0

    :cond_8
    sget-object v4, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_SECONDARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v2, v4, :cond_5

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-direct {p0, v4}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_9

    return-void

    :cond_9
    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-direct {p0, v4, v5, v6}, Lcom/android/incallui/CallCardPresenter;->createCallCardInfoByDialpad(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Z)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v0

    iget-object v4, v0, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-boolean v5, v0, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-direct {p0, v6}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v6

    iget-object v7, v0, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    invoke-interface {v3, v4, v5, v6, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleSecondaryName(Ljava/lang/String;ZZLjava/lang/String;)V

    goto :goto_0
.end method

.method public onIncomingCall(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V
    .locals 1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/incallui/CallCardPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    return-void
.end method

.method public onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V
    .locals 10

    const-string/jumbo v7, "CCP.oSC"

    invoke-static {v7}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "onStateChange(): state = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " , callList = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v6

    check-cast v6, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v6, :cond_0

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_0
    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iput-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    const/4 v0, 0x0

    const/4 v1, 0x0

    sget-object v7, Lcom/android/incallui/InCallPresenter$InCallState;->INCOMING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v7, :cond_2

    const/4 v7, 0x1

    :goto_0
    iput-boolean v7, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    iget-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    iput-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mLastInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    iput-object p2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getIncomingCall()Lcom/android/incallui/Call;

    move-result-object v7

    iput-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/incallui/CallCardPresenter;->mIsCrbtInDouble:Z

    sget-object v7, Lcom/android/incallui/InCallPresenter$InCallState;->INCOMING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v7, :cond_3

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {p0, p3, v7, v8}, Lcom/android/incallui/CallCardPresenter;->getCallToDisplay(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;Z)Lcom/android/incallui/Call;

    move-result-object v0

    const/4 v7, 0x1

    invoke-direct {p0, p3, v0, v7}, Lcom/android/incallui/CallCardPresenter;->getCallToDisplay(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;Z)Lcom/android/incallui/Call;

    move-result-object v1

    :cond_1
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Ring call: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Primary call: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Secondary call: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p3, v0}, Lcom/android/incallui/CallCardPresenter;->getCallConnectedTime(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;)J

    move-result-wide v2

    invoke-direct {p0, p3, v1}, Lcom/android/incallui/CallCardPresenter;->getCallConnectedTime(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;)J

    move-result-wide v4

    const-wide/16 v8, -0x1

    cmp-long v7, v2, v8

    if-nez v7, :cond_8

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    :goto_2
    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getCurrentCallCardState()Lcom/android/incallui/CallCardPresenter$CallCardState;

    move-result-object v7

    iput-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/CallCardPresenter;->isNeedUpdateUI(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)Z

    move-result v7

    if-nez v7, :cond_c

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "=====>"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "ignore Ring State: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/incallui/CallCardPresenter;->mLastRingCallKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "ignore Primary State: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/incallui/CallCardPresenter;->mLastPrimaryCallKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "ignore Secondary State: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/incallui/CallCardPresenter;->mLastSecondaryCallKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_2
    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_3
    sget-object v7, Lcom/android/incallui/InCallPresenter$InCallState;->PENDING_OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-eq p2, v7, :cond_4

    sget-object v7, Lcom/android/incallui/InCallPresenter$InCallState;->OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v7, :cond_6

    :cond_4
    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getPendingOutgoingCall()Lcom/android/incallui/Call;

    move-result-object v0

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/incallui/CallList;->getActiveCall()Lcom/android/incallui/Call;

    move-result-object v7

    invoke-static {v7}, Lcom/android/incallui/CallUtils;->isVtConferenceCall(Lcom/android/incallui/Call;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_5

    const/4 v0, 0x0

    :cond_5
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-direct {p0, p3, v7, v8}, Lcom/android/incallui/CallCardPresenter;->getCallToDisplay(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;Z)Lcom/android/incallui/Call;

    move-result-object v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/incallui/Call;->isPlayingVideoCrbt()Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/CallCardPresenter;->getHoldCallPromptWhenCrbt(Lcom/android/incallui/Call;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/incallui/CallCardPresenter;->mCrbtHoldCallPrompt:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/incallui/CallCardPresenter;->mIsCrbtInDouble:Z

    goto/16 :goto_1

    :cond_6
    sget-object v7, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v7, :cond_7

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {p0, p3, v7, v8}, Lcom/android/incallui/CallCardPresenter;->getCallToDisplay(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;Z)Lcom/android/incallui/Call;

    move-result-object v0

    const/4 v7, 0x1

    invoke-direct {p0, p3, v0, v7}, Lcom/android/incallui/CallCardPresenter;->getCallToDisplay(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;Z)Lcom/android/incallui/Call;

    move-result-object v1

    goto/16 :goto_1

    :cond_7
    sget-object v7, Lcom/android/incallui/InCallPresenter$InCallState;->WAITING_FOR_ACCOUNT:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v7, :cond_1

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getWaitingForAccountCall()Lcom/android/incallui/Call;

    move-result-object v0

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-direct {p0, p3, v7, v8}, Lcom/android/incallui/CallCardPresenter;->getCallToDisplay(Lcom/android/incallui/CallList;Lcom/android/incallui/Call;Z)Lcom/android/incallui/Call;

    move-result-object v1

    goto/16 :goto_1

    :cond_8
    const-wide/16 v8, -0x1

    cmp-long v7, v4, v8

    if-nez v7, :cond_9

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    goto/16 :goto_2

    :cond_9
    cmp-long v7, v2, v4

    if-gtz v7, :cond_a

    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-nez v7, :cond_b

    :cond_a
    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    goto/16 :goto_2

    :cond_b
    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    goto/16 :goto_2

    :cond_c
    invoke-direct {p0}, Lcom/android/incallui/CallCardPresenter;->updateInfoByState()V

    invoke-static {}, Lcom/android/incallui/util/CallStatisticUtil;->getInstance()Lcom/android/incallui/util/CallStatisticUtil;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Lcom/android/incallui/util/CallStatisticUtil;->recordTimeOfInComingToInCall(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)V

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void
.end method

.method public onUiReady(Lcom/android/incallui/CallCardPresenter$CallCardUi;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiReady(Lcom/android/incallui/Ui;)V

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-direct {p0, v0}, Lcom/android/incallui/CallCardPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->addInCallDialPadListener(Lcom/android/incallui/InCallPresenter$InCallDialPadListener;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-direct {p0, v0}, Lcom/android/incallui/CallCardPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    goto :goto_0
.end method

.method public bridge synthetic onUiReady(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/CallCardPresenter;->onUiReady(Lcom/android/incallui/CallCardPresenter$CallCardUi;)V

    return-void
.end method

.method public onUiUnready(Lcom/android/incallui/CallCardPresenter$CallCardUi;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiUnready(Lcom/android/incallui/Ui;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeInCallDialPadListener(Lcom/android/incallui/InCallPresenter$InCallDialPadListener;)Z

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iput-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    iput-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsLastCallConference:Z

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mLastRingCallKey:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mLastPrimaryCallKey:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mLastSecondaryCallKey:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/incallui/CallCardPresenter;->mLastPrimaryCallId:Ljava/lang/String;

    sget-object v0, Lcom/android/incallui/CallCardPresenter$CallCardState;->IDLE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    iput-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    return-void
.end method

.method public bridge synthetic onUiUnready(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/CallCardPresenter;->onUiUnready(Lcom/android/incallui/CallCardPresenter$CallCardUi;)V

    return-void
.end method

.method public setForceProcessIncoming(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/CallCardPresenter;->mForceProcessIncoming:Z

    return-void
.end method

.method public showNotification(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingCall:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mRingContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    invoke-virtual {p0, v0, p2}, Lcom/android/incallui/CallCardPresenter;->markNumberSettingGuide(Lcom/android/incallui/model/YellowPageInfo;Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    invoke-virtual {p0, v0, p2}, Lcom/android/incallui/CallCardPresenter;->markNumberSettingGuide(Lcom/android/incallui/model/YellowPageInfo;Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondaryContactInfo:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v0, v0, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    invoke-virtual {p0, v0, p2}, Lcom/android/incallui/CallCardPresenter;->markNumberSettingGuide(Lcom/android/incallui/model/YellowPageInfo;Z)V

    :cond_2
    return-void
.end method

.method showSmallAvatar()V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "incoming to nonincoming, callback to show small avatar, isVideoCallActive="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    invoke-virtual {v2}, Lcom/android/incallui/CallCardPresenter$CallCardState;->isNonRingDoubleCall()Z

    move-result v2

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallConference:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getCallCount()I

    move-result v2

    invoke-interface {v0, v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->getSingleCallerInfoTopSpace(I)I

    move-result v2

    invoke-interface {v0, v2}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showSmallAvatar(I)V

    :goto_0
    iget-boolean v2, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallConference:Z

    iget-object v3, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-direct {p0, v3}, Lcom/android/incallui/CallCardPresenter;->isDialpadPressed(Lcom/android/incallui/Call;)Z

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentInCallState:Lcom/android/incallui/InCallPresenter$InCallState;

    sget-object v5, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-direct {p0, v2, v3, v1}, Lcom/android/incallui/CallCardPresenter;->controlBannerPosition(ZZZ)V

    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallConference:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentCallVideoConference:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "conference call show incoming banner"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showIncomingBanner()V

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-interface {v0, v1}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->hideSmallAvatar(Z)V

    goto :goto_0

    :cond_4
    iget-boolean v1, p0, Lcom/android/incallui/CallCardPresenter;->mIsCurrentVideoCall:Z

    if-nez v1, :cond_2

    const-string/jumbo v1, "not video call active, show answer incoming banner"

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->showAnswerIncomingBanner()V

    goto :goto_1
.end method

.method public updateCallTime()V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-nez v5, :cond_2

    :cond_0
    if-eqz v4, :cond_1

    const/4 v5, 0x0

    invoke-interface {v4, v5, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallElapsedTime(ZLjava/lang/String;)V

    :cond_1
    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v5}, Lcom/android/incallui/CallTimer;->cancel()V

    :goto_0
    return-void

    :cond_2
    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getState()I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_3

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getState()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_0

    :cond_3
    iget-boolean v5, p0, Lcom/android/incallui/CallCardPresenter;->mIsIncomingCall:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getState()I

    move-result v5

    invoke-static {v5}, Lcom/android/incallui/Call$State;->isDialing(I)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getState()I

    move-result v5

    const/16 v6, 0xc

    if-eq v5, v6, :cond_0

    :cond_4
    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getConnectTimeMillis()J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-nez v5, :cond_5

    return-void

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v0

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v4, v6, v5}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setSingleCallElapsedTime(ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public updateDoublePrimaryCallTime()V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    if-nez v5, :cond_2

    :cond_0
    if-eqz v4, :cond_1

    const/4 v5, 0x0

    invoke-interface {v4, v5, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoublePrimaryCallElapsedTime(ZLjava/lang/String;)V

    :cond_1
    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mDoublePrimaryCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v5}, Lcom/android/incallui/CallTimer;->cancel()V

    :goto_0
    return-void

    :cond_2
    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getState()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getConnectTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v0

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v4, v6, v5}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoublePrimaryCallElapsedTime(ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public updateDoubleSecondaryCallTime()V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-nez v5, :cond_2

    :cond_0
    if-eqz v4, :cond_1

    const/4 v5, 0x0

    invoke-interface {v4, v5, v7}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleSecondaryCallElapsedTime(ZLjava/lang/String;)V

    :cond_1
    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mDoubleSecondaryCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v5}, Lcom/android/incallui/CallTimer;->cancel()V

    :goto_0
    return-void

    :cond_2
    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getState()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v5}, Lcom/android/incallui/Call;->getConnectTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v0

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v4, v6, v5}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setDoubleSecondaryCallElapsedTime(ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public updateVideoCallTime()V
    .locals 9

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v6, v7, :cond_4

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    if-nez v0, :cond_5

    :cond_2
    if-eqz v1, :cond_3

    const/4 v6, 0x0

    invoke-interface {v1, v6, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoCallElapsedTime(ZLjava/lang/String;)V

    :cond_3
    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mVideoCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v6}, Lcom/android/incallui/CallTimer;->cancel()V

    :goto_1
    return-void

    :cond_4
    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_SINGLE_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->SINGLE_CALL_ACTIVE:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->SINGLE_CALL_DIALING:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->isVideoCall()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    goto :goto_0

    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v6

    invoke-static {v6}, Lcom/android/incallui/Call$State;->isMiuiDialing(I)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_6
    invoke-virtual {v0}, Lcom/android/incallui/Call;->getConnectTimeMillis()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v2

    const-wide/16 v6, 0x3e8

    div-long v6, v4, v6

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v1, v7, v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoCallElapsedTime(ZLjava/lang/String;)V

    goto :goto_1
.end method

.method public updateVideoHoldCallTime()V
    .locals 9

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/CallCardPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallCardPresenter$CallCardUi;

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->RING_DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mCurrentCallCardState:Lcom/android/incallui/CallCardPresenter$CallCardState;

    sget-object v7, Lcom/android/incallui/CallCardPresenter$CallCardState;->DOUBLE_PRIMARY_CALL:Lcom/android/incallui/CallCardPresenter$CallCardState;

    if-ne v6, v7, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mSecondary:Lcom/android/incallui/Call;

    :goto_0
    if-eqz v1, :cond_1

    if-nez v0, :cond_4

    :cond_1
    if-eqz v1, :cond_2

    const/4 v6, 0x0

    invoke-interface {v1, v6, v8}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoHoldCallElapsedTime(ZLjava/lang/String;)V

    :cond_2
    iget-object v6, p0, Lcom/android/incallui/CallCardPresenter;->mVideoHoldCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v6}, Lcom/android/incallui/CallTimer;->cancel()V

    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/CallCardPresenter;->mPrimary:Lcom/android/incallui/Call;

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/android/incallui/Call;->getConnectTimeMillis()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v2

    const-wide/16 v6, 0x3e8

    div-long v6, v4, v6

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v1, v7, v6}, Lcom/android/incallui/CallCardPresenter$CallCardUi;->setVideoHoldCallElapsedTime(ZLjava/lang/String;)V

    goto :goto_1
.end method
