.class public Lcom/android/incallui/InCallApp;
.super Lmiui/external/ApplicationDelegate;
.source "InCallApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/InCallApp$InCallApplication;,
        Lcom/android/incallui/InCallApp$NotificationBroadcastReceiver;,
        Lcom/android/incallui/InCallApp$SuppServFailedReceiver;
    }
.end annotation


# static fields
.field private static sMe:Lcom/android/incallui/InCallApp;


# instance fields
.field private mCallBackgroundType:I

.field private mCountryIso:Ljava/lang/String;

.field private mInCallBackground:Landroid/graphics/drawable/Drawable;

.field private mSuppServFailedReceiver:Lcom/android/incallui/InCallApp$SuppServFailedReceiver;


# direct methods
.method static synthetic -get0()Lcom/android/incallui/InCallApp;
    .locals 1

    sget-object v0, Lcom/android/incallui/InCallApp;->sMe:Lcom/android/incallui/InCallApp;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/incallui/InCallApp;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/InCallApp;->mCountryIso:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/incallui/InCallApp;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/InCallApp;->mInCallBackground:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/external/ApplicationDelegate;-><init>()V

    sput-object p0, Lcom/android/incallui/InCallApp;->sMe:Lcom/android/incallui/InCallApp;

    return-void
.end method

.method public static getInstance()Lcom/android/incallui/InCallApp;
    .locals 2

    sget-object v0, Lcom/android/incallui/InCallApp;->sMe:Lcom/android/incallui/InCallApp;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No InCallApp here!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/android/incallui/InCallApp;->sMe:Lcom/android/incallui/InCallApp;

    return-object v0
.end method

.method private registerSuppServMsgReceiver()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/incallui/InCallApp;->mSuppServFailedReceiver:Lcom/android/incallui/InCallApp$SuppServFailedReceiver;

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "org.codeaurora.ACTION_SUPP_SERVICE_FAILURE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/android/incallui/InCallApp$SuppServFailedReceiver;

    invoke-direct {v1, p0, v2}, Lcom/android/incallui/InCallApp$SuppServFailedReceiver;-><init>(Lcom/android/incallui/InCallApp;Lcom/android/incallui/InCallApp$SuppServFailedReceiver;)V

    iput-object v1, p0, Lcom/android/incallui/InCallApp;->mSuppServFailedReceiver:Lcom/android/incallui/InCallApp$SuppServFailedReceiver;

    iget-object v1, p0, Lcom/android/incallui/InCallApp;->mSuppServFailedReceiver:Lcom/android/incallui/InCallApp$SuppServFailedReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/InCallApp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method private unRegisterSuppServMsgReceiver()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/InCallApp;->mSuppServFailedReceiver:Lcom/android/incallui/InCallApp$SuppServFailedReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallApp;->mSuppServFailedReceiver:Lcom/android/incallui/InCallApp$SuppServFailedReceiver;

    invoke-virtual {p0, v0}, Lcom/android/incallui/InCallApp;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v1, p0, Lcom/android/incallui/InCallApp;->mSuppServFailedReceiver:Lcom/android/incallui/InCallApp$SuppServFailedReceiver;

    :cond_0
    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    invoke-super {p0, p1}, Lmiui/external/ApplicationDelegate;->attachBaseContext(Landroid/content/Context;)V

    return-void
.end method

.method public clear()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/InCallApp;->mInCallBackground:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lcom/android/incallui/InCallApp;->unRegisterSuppServMsgReceiver()V

    return-void
.end method

.method public getCountryIso()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallApp;->mCountryIso:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/incallui/InCallApp;->sMe:Lcom/android/incallui/InCallApp;

    invoke-static {v0}, Lcom/android/incallui/CallerInfo;->getCurrentCountryIso(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/InCallApp;->mCountryIso:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/InCallApp;->mCountryIso:Ljava/lang/String;

    return-object v0
.end method

.method public getInCallWallPaperDirectly(Z)Landroid/graphics/drawable/Drawable;
    .locals 2

    if-eqz p1, :cond_0

    const-string/jumbo v0, "update Lock Screen Wallpaper"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallApp;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/ImageUtils;->getLockWallpaper(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "update default image"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/InCallApp;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public isUseLockWallPaper()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/incallui/InCallApp;->mCallBackgroundType:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Lmiui/external/ApplicationDelegate;->onCreate()V

    invoke-static {}, Lcom/android/incallui/util/MiStatInterfaceUtil;->init()V

    invoke-static {p0}, Lcom/android/incallui/CallAdapterUtils;->createInCallNotificationChannel(Landroid/content/Context;)V

    const-string/jumbo v1, "persist.sys.miui_optimization"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lmiui/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "security"

    invoke-virtual {p0, v1}, Lcom/android/incallui/InCallApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManager;

    invoke-virtual {p0}, Lcom/android/incallui/InCallApp;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/security/SecurityManager;->grantRuntimePermission(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public preLoad()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/InCallApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Telephony;->getCallBackgroundType(Landroid/content/ContentResolver;)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/InCallApp;->mCallBackgroundType:I

    new-instance v0, Lcom/android/incallui/InCallApp$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/InCallApp$1;-><init>(Lcom/android/incallui/InCallApp;)V

    const-string/jumbo v1, "task_update_incall_wallpaper"

    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallApp$1;->withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/util/SimpleTask;->run()V

    invoke-direct {p0}, Lcom/android/incallui/InCallApp;->registerSuppServMsgReceiver()V

    return-void
.end method
