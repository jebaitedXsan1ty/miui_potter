.class Lcom/android/incallui/InCallActivity$MainHandler;
.super Lcom/android/incallui/BaseUIHandler;
.source "InCallActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/InCallActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MainHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseUIHandler",
        "<",
        "Lcom/android/incallui/InCallActivity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/incallui/InCallActivity;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/BaseUIHandler;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public doMainTask(Lcom/android/incallui/InCallActivity;Landroid/os/Message;)V
    .locals 1

    iget v0, p2, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/android/incallui/InCallActivity;->displayCallCard()V

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/android/incallui/InCallActivity;->-wrap0(Lcom/android/incallui/InCallActivity;)V

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/android/incallui/InCallActivity;->-wrap1(Lcom/android/incallui/InCallActivity;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1}, Lcom/android/incallui/InCallActivity;->showSmartCover()V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p1}, Lcom/android/incallui/InCallActivity;->displayCallButton()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic doMainTask(Ljava/lang/Object;Landroid/os/Message;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/InCallActivity;

    invoke-virtual {p0, p1, p2}, Lcom/android/incallui/InCallActivity$MainHandler;->doMainTask(Lcom/android/incallui/InCallActivity;Landroid/os/Message;)V

    return-void
.end method
