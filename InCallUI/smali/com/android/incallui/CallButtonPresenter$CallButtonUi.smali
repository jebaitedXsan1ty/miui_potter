.class public interface abstract Lcom/android/incallui/CallButtonPresenter$CallButtonUi;
.super Ljava/lang/Object;
.source "CallButtonPresenter.java"

# interfaces
.implements Lcom/android/incallui/Ui;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/CallButtonPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CallButtonUi"
.end annotation


# virtual methods
.method public abstract changeContentViewPaddingBottom(Z)V
.end method

.method public abstract displayToolPanel(Z)V
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract setVisible(Z)V
.end method

.method public abstract startRotationAnimator(I)V
.end method

.method public abstract updateAudioMode(I)V
.end method

.method public abstract updateToolLayoutButton()V
.end method
