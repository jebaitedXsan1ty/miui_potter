.class public Lcom/android/incallui/view/UnlockPhoneDialog;
.super Lcom/android/incallui/view/InCallDialog;
.source "UnlockPhoneDialog.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/InCallDialog;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/view/UnlockPhoneDialog;->configureDialog(Landroid/content/Context;I)V

    return-void
.end method

.method private configureDialog(Landroid/content/Context;I)V
    .locals 4

    const v3, 0x7f0b0078

    invoke-virtual {p0, v3}, Lcom/android/incallui/view/UnlockPhoneDialog;->setTitle(I)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f0b0079

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/UnlockPhoneDialog;->setMessage(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/android/incallui/view/UnlockPhoneDialog;->setPositiveButton(I)V

    const v0, 0x7f0b0073

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/UnlockPhoneDialog;->setNegativeButton(I)V

    invoke-virtual {p0}, Lcom/android/incallui/view/UnlockPhoneDialog;->setCancelListener()V

    invoke-virtual {p0}, Lcom/android/incallui/view/UnlockPhoneDialog;->setDismissListener()V

    return-void
.end method


# virtual methods
.method protected cancelDialog()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onDismissDialog()V

    return-void
.end method

.method protected dismissDialog()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onDismissDialog()V

    return-void
.end method

.method protected onNegative()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onDismissDialog()V

    return-void
.end method

.method protected onPositive()V
    .locals 2

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/InCallPresenter;->getInCallActivity()Lcom/android/incallui/InCallActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/incallui/InCallActivity;->hideAndDismissKeyguard()V

    :cond_0
    return-void
.end method
