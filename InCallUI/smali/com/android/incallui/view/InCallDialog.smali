.class public abstract Lcom/android/incallui/view/InCallDialog;
.super Ljava/lang/Object;
.source "InCallDialog.java"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/incallui/view/InCallDialog;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/incallui/view/InCallDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    return-void
.end method


# virtual methods
.method protected cancelDialog()V
    .locals 0

    return-void
.end method

.method public dismiss()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iput-object v1, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method protected dismissDialog()V
    .locals 0

    return-void
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNegative()V
    .locals 0

    return-void
.end method

.method protected onPositive()V
    .locals 0

    return-void
.end method

.method protected setCancelListener()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/android/incallui/view/InCallDialog$4;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/InCallDialog$4;-><init>(Lcom/android/incallui/view/InCallDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method protected setCancelable(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    return-void
.end method

.method protected setCanceledOnTouchOutside(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    return-void
.end method

.method protected setDismissListener()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/android/incallui/view/InCallDialog$5;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/InCallDialog$5;-><init>(Lcom/android/incallui/view/InCallDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method protected setMessage(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/InCallDialog;->setMessage(Ljava/lang/String;)V

    return-void
.end method

.method protected setMessage(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected setNegativeButton(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/InCallDialog;->setNegativeButton(Ljava/lang/String;)V

    return-void
.end method

.method protected setNegativeButton(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/android/incallui/view/InCallDialog$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/InCallDialog$2;-><init>(Lcom/android/incallui/view/InCallDialog;)V

    const/4 v2, -0x2

    invoke-virtual {v0, v2, p1, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method protected setPositiveButton(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/InCallDialog;->setPositiveButton(Ljava/lang/String;)V

    return-void
.end method

.method protected setPositiveButton(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    new-instance v1, Lcom/android/incallui/view/InCallDialog$1;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/InCallDialog$1;-><init>(Lcom/android/incallui/view/InCallDialog;)V

    const/4 v2, -0x1

    invoke-virtual {v0, v2, p1, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method protected setTitle(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/InCallDialog;->setTitle(Ljava/lang/String;)V

    return-void
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public show()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/InCallDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
