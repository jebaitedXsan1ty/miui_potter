.class Lcom/android/incallui/view/ArrowImageView$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ArrowImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/view/ArrowImageView;->playArrowRepeatTranslateAnimator(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field isAnimationCancel:Z

.field final synthetic this$0:Lcom/android/incallui/view/ArrowImageView;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/ArrowImageView;)V
    .locals 1

    iput-object p1, p0, Lcom/android/incallui/view/ArrowImageView$2;->this$0:Lcom/android/incallui/view/ArrowImageView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/view/ArrowImageView$2;->isAnimationCancel:Z

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/view/ArrowImageView$2;->isAnimationCancel:Z

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView$2;->this$0:Lcom/android/incallui/view/ArrowImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/ArrowImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView$2;->this$0:Lcom/android/incallui/view/ArrowImageView;

    invoke-static {v0}, Lcom/android/incallui/view/ArrowImageView;->-get0(Lcom/android/incallui/view/ArrowImageView;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/incallui/view/ArrowImageView$2;->isAnimationCancel:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView$2;->this$0:Lcom/android/incallui/view/ArrowImageView;

    invoke-static {v0}, Lcom/android/incallui/view/ArrowImageView;->-get0(Lcom/android/incallui/view/ArrowImageView;)Landroid/animation/AnimatorSet;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    iget-object v0, p0, Lcom/android/incallui/view/ArrowImageView$2;->this$0:Lcom/android/incallui/view/ArrowImageView;

    invoke-static {v0}, Lcom/android/incallui/view/ArrowImageView;->-get0(Lcom/android/incallui/view/ArrowImageView;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/view/ArrowImageView$2;->isAnimationCancel:Z

    return-void
.end method
