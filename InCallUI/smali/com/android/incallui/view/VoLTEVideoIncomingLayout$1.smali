.class Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;
.super Ljava/lang/Object;
.source "VoLTEVideoIncomingLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->setDialPadPreviewPosition(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

.field final synthetic val$isDialpad:Z


# direct methods
.method constructor <init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iput-boolean p2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->val$isDialpad:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/16 v2, 0x32

    const-string/jumbo v0, "VoLTEVideoInComingLayout"

    const-string/jumbo v1, "setDialPadPreviewPosition runnable"

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap4(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)V

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iget-boolean v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->val$isDialpad:Z

    invoke-static {v0, v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap2(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Z)V

    iget-boolean v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->val$isDialpad:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get7(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iget-boolean v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->val$isDialpad:Z

    invoke-static {v0, v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-set1(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Z)Z

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    const/4 v1, 0x2

    invoke-static {v0, v1, v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;II)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get7(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/android/incallui/util/Utils;->isInternationalBuild()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;II)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$1;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch
.end method
