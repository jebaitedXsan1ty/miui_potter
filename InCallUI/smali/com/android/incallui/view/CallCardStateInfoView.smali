.class public final Lcom/android/incallui/view/CallCardStateInfoView;
.super Lcom/android/incallui/view/IndexSortedView;
.source "CallCardStateInfoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/CallCardStateInfoView$1;,
        Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;
    }
.end annotation


# instance fields
.field private mCallStateLabel:Landroid/widget/TextView;

.field private mConferenceCallNumber:Landroid/widget/TextView;

.field private mConferenceMode:Landroid/widget/TextView;

.field private mConferenceSimCardInfo:Landroid/widget/ImageView;

.field private mDisconnectingListener:Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;

.field private mElapsedTime:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/CallCardStateInfoView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/view/CallCardStateInfoView;)Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mDisconnectingListener:Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/incallui/view/CallCardStateInfoView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/CallCardStateInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/incallui/view/CallCardStateInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/incallui/view/IndexSortedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/incallui/view/CallCardStateInfoView$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/view/CallCardStateInfoView$1;-><init>(Lcom/android/incallui/view/CallCardStateInfoView;)V

    iput-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x2

    filled-new-array {v1, v2, v0}, [I

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/CallCardStateInfoView;->configIndexFrontNoDivider([I)V

    new-array v0, v2, [I

    aput v1, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/CallCardStateInfoView;->configIndexBackNoDivider([I)V

    return-void
.end method

.method private createViewByIndex(I)Landroid/view/View;
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v6, -0x2

    const/4 v7, 0x1

    const/4 v5, 0x0

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    packed-switch p1, :pswitch_data_0

    new-instance v6, Ljava/lang/RuntimeException;

    const-string/jumbo v7, "error index"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    :pswitch_0
    new-instance v3, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/view/CallCardStateInfoView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/16 v6, 0x10

    iput v6, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    const v6, 0x7f090045

    invoke-virtual {p0, v6}, Lcom/android/incallui/view/CallCardStateInfoView;->getSize(I)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v5, v3

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v5

    :pswitch_1
    new-instance v4, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/incallui/view/CallCardStateInfoView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    if-ne p1, v7, :cond_1

    invoke-virtual {v4}, Landroid/widget/TextView;->setSingleLine()V

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setSelected(Z)V

    :goto_1
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v6, 0x7f09000c

    invoke-virtual {p0, v6}, Lcom/android/incallui/view/CallCardStateInfoView;->getSize(I)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v4, v8, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setAllCaps(Z)V

    if-ne p1, v9, :cond_2

    const v0, 0x7f06000c

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/incallui/view/CallCardStateInfoView;->getColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    if-ne v9, p1, :cond_0

    const v6, 0x7f0b0050

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    move-object v5, v4

    goto :goto_0

    :cond_1
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_1

    :cond_2
    const v0, 0x7f060002

    goto :goto_2

    :pswitch_2
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/incallui/view/CallCardStateInfoView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v6, 0x7f060001

    invoke-virtual {p0, v6}, Lcom/android/incallui/view/CallCardStateInfoView;->getColor(I)I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    const v6, 0x7f09000d

    invoke-virtual {p0, v6}, Lcom/android/incallui/view/CallCardStateInfoView;->getSize(I)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v1, v8, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    move-object v5, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private getViewByIndex(I)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "error index"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/CallCardStateInfoView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/incallui/view/CallCardStateInfoView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    if-nez v1, :cond_2

    invoke-direct {p0, p1}, Lcom/android/incallui/view/CallCardStateInfoView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceMode:Landroid/widget/TextView;

    if-nez v1, :cond_3

    invoke-direct {p0, p1}, Lcom/android/incallui/view/CallCardStateInfoView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceMode:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceCallNumber:Landroid/widget/TextView;

    if-nez v1, :cond_4

    invoke-direct {p0, p1}, Lcom/android/incallui/view/CallCardStateInfoView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceCallNumber:Landroid/widget/TextView;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private playElapsedTimeTranslateIn()V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iget-object v3, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v9, [F

    aput v10, v5, v7

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v3, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v9, [F

    const/high16 v6, 0x41500000    # 13.0f

    aput v6, v5, v7

    aput v10, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    new-instance v3, Lcom/android/incallui/view/CallCardStateInfoView$2;

    invoke-direct {v3, p0}, Lcom/android/incallui/view/CallCardStateInfoView$2;-><init>(Lcom/android/incallui/view/CallCardStateInfoView;)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-array v3, v9, [Landroid/animation/Animator;

    aput-object v1, v3, v7

    aput-object v2, v3, v8

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    const-wide/16 v4, 0x15e

    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method


# virtual methods
.method public addDisconnectingListener(Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mDisconnectingListener:Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;

    return-void
.end method

.method public getCallStateLabel()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method public getDisconnectingListener()Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mDisconnectingListener:Lcom/android/incallui/view/CallCardStateInfoView$DisconnectingListener;

    return-object v0
.end method

.method protected getDividerView(I)Landroid/view/View;
    .locals 5

    const/4 v4, -0x2

    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/incallui/view/CallCardStateInfoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/android/incallui/view/CallCardStateInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    float-to-int v0, v3

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    const/16 v3, 0x10

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v3, 0x7f02008f

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object v1
.end method

.method protected getTotalViewCount()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public removeViewByIndex(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/incallui/view/IndexSortedView;->removeViewByIndex(I)V

    return-void
.end method

.method public setCallCardStateLabel(Ljava/lang/String;I)V
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/16 v5, 0x65

    const/16 v2, 0x64

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0, v6}, Lcom/android/incallui/view/CallCardStateInfoView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v0, 0x9

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {p0, v6, v0}, Lcom/android/incallui/view/CallCardStateInfoView;->addIndexView(ILandroid/view/View;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    const v1, 0x7f060001

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    const v1, 0x7f09000d

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :cond_4
    if-eq p2, v7, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    const v1, 0x7f060002

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    const v1, 0x7f09000c

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setCallElapsedTime(ZLjava/lang/String;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    if-nez v0, :cond_0

    invoke-direct {p0, v2}, Lcom/android/incallui/view/CallCardStateInfoView;->createViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    invoke-virtual {p0, v2, v0}, Lcom/android/incallui/view/CallCardStateInfoView;->addIndexView(ILandroid/view/View;)V

    invoke-direct {p0}, Lcom/android/incallui/view/CallCardStateInfoView;->playElapsedTimeTranslateIn()V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    invoke-static {v0, p2}, Lcom/android/incallui/util/Utils;->measureWidthForTextView(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/CallCardStateInfoView;->removeViewByIndex(I)V

    iput-object v1, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mElapsedTime:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public setConferenceCallNumber(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceCallNumber:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceCallNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceCallNumber:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/CallCardStateInfoView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceCallNumber:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceCallNumber:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public setConferenceMode(Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x3

    if-eqz p1, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceMode:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceMode:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/CallCardStateInfoView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceMode:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceMode:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public setConferenceMultiSimIndicator(IZ)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->getViewByIndex(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/android/incallui/view/CallCardStateInfoView;->addIndexView(ILandroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/CallCardStateInfoView;->removeViewByIndex(I)V

    iput-object v2, p0, Lcom/android/incallui/view/CallCardStateInfoView;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    goto :goto_0
.end method
