.class public Lcom/android/incallui/view/VideoHoldCallInfoView;
.super Landroid/widget/LinearLayout;
.source "VideoHoldCallInfoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/VideoHoldCallInfoView$VideoHoldCallInfoOperationListener;
    }
.end annotation


# instance fields
.field private mCallAvatar:Landroid/widget/ImageView;

.field private mCallName:Landroid/widget/TextView;

.field private mCallState:Landroid/widget/TextView;

.field private mCallSwitch:Landroid/widget/Button;

.field private mCallTime:Landroid/widget/TextView;

.field private mInfoView:Landroid/view/View;

.field private mVideoHoldCallInfoOperationListener:Lcom/android/incallui/view/VideoHoldCallInfoView$VideoHoldCallInfoOperationListener;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/VideoHoldCallInfoView;)Lcom/android/incallui/view/VideoHoldCallInfoView$VideoHoldCallInfoOperationListener;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mVideoHoldCallInfoOperationListener:Lcom/android/incallui/view/VideoHoldCallInfoView$VideoHoldCallInfoOperationListener;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private showAndInitializeSecondaryCallInfo(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f0a00ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mInfoView:Landroid/view/View;

    const v0, 0x7f0a00cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallAvatar:Landroid/widget/ImageView;

    const v0, 0x7f0a00ce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallName:Landroid/widget/TextView;

    const v0, 0x7f0a00cf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallState:Landroid/widget/TextView;

    const v0, 0x7f0a00d0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallTime:Landroid/widget/TextView;

    const v0, 0x7f0a00d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallSwitch:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallSwitch:Landroid/widget/Button;

    new-instance v1, Lcom/android/incallui/view/VideoHoldCallInfoView$1;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/VideoHoldCallInfoView$1;-><init>(Lcom/android/incallui/view/VideoHoldCallInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallAvatar:Landroid/widget/ImageView;

    const v1, 0x7f020028

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallAvatar:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    invoke-direct {p0, p0}, Lcom/android/incallui/view/VideoHoldCallInfoView;->showAndInitializeSecondaryCallInfo(Landroid/view/View;)V

    return-void
.end method

.method public setVideoHold(Lcom/android/incallui/model/CallCardInfo;ZZ)V
    .locals 4

    const v3, 0x7f020028

    const/4 v2, 0x0

    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    iget-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v1}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    const/4 v1, 0x0

    iput-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    :cond_1
    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/VideoHoldCallInfoView;->setVideoHoldName(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->photo:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/android/incallui/view/VideoHoldCallInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->photo:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v2}, Lcom/android/incallui/ImageUtils;->getCircleAvatarBitmap(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallAvatar:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallAvatar:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallAvatar:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setVideoHoldCallElapsedTime(ZLjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallTime:Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallTime:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/incallui/AnimationUtils$Fade;->show(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallTime:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallTime:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVideoHoldCallInfoOperationListener(Lcom/android/incallui/view/VideoHoldCallInfoView$VideoHoldCallInfoOperationListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mVideoHoldCallInfoOperationListener:Lcom/android/incallui/view/VideoHoldCallInfoView$VideoHoldCallInfoOperationListener;

    return-void
.end method

.method public setVideoHoldCallState(ILandroid/telecom/DisconnectCause;Z)V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallState:Landroid/widget/TextView;

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, -0x1

    invoke-static {p1, p2, v4, v1}, Lcom/android/incallui/CallUtils;->getCallStateLabelFromState(ILandroid/telecom/DisconnectCause;ZI)Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/incallui/view/VideoHoldCallInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallState:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVideoHoldInfoView(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mInfoView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setVideoHoldName(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallName:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/VideoHoldCallInfoView;->mCallName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
