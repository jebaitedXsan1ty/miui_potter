.class public Lcom/android/incallui/view/EllipsisTextView;
.super Landroid/widget/TextView;
.source "EllipsisTextView.java"


# instance fields
.field private mCallTimer:Lcom/android/incallui/CallTimer;

.field private mDots:Ljava/lang/String;

.field private mDotsNum:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/EllipsisTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mDotsNum:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mDots:Ljava/lang/String;

    new-instance v0, Lcom/android/incallui/CallTimer;

    new-instance v1, Lcom/android/incallui/view/EllipsisTextView$1;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/EllipsisTextView$1;-><init>(Lcom/android/incallui/view/EllipsisTextView;)V

    invoke-direct {v0, v1}, Lcom/android/incallui/CallTimer;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mCallTimer:Lcom/android/incallui/CallTimer;

    return-void
.end method


# virtual methods
.method public setEllipsisText()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/incallui/view/EllipsisTextView;->mDots:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mDots:Ljava/lang/String;

    iget v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mDotsNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mDotsNum:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mDotsNum:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mDots:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mDots:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/EllipsisTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 4

    invoke-super {p0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mCallTimer:Lcom/android/incallui/CallTimer;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/android/incallui/CallTimer;->start(J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/EllipsisTextView;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v0}, Lcom/android/incallui/CallTimer;->cancel()V

    goto :goto_0
.end method
