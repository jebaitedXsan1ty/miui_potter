.class public Lcom/android/incallui/view/VolteServiceFailedDialog;
.super Lcom/android/incallui/view/InCallDialog;
.source "VolteServiceFailedDialog.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/incallui/view/InCallDialog;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0b00cc

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/VolteServiceFailedDialog;->setMessage(I)V

    const v0, 0x7f0b0072

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/VolteServiceFailedDialog;->setPositiveButton(I)V

    invoke-virtual {p0}, Lcom/android/incallui/view/VolteServiceFailedDialog;->setCancelListener()V

    invoke-virtual {p0}, Lcom/android/incallui/view/VolteServiceFailedDialog;->setDismissListener()V

    return-void
.end method


# virtual methods
.method protected cancelDialog()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onDismissDialog()V

    return-void
.end method

.method protected dismissDialog()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onDismissDialog()V

    return-void
.end method

.method protected onPositive()V
    .locals 1

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/InCallPresenter;->onDismissDialog()V

    return-void
.end method
