.class Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;
.super Landroid/widget/BaseAdapter;
.source "SimCardPickDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/view/SimCardPickDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectorAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final SMALL_SIM_SLOT_ICON:[I

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mOptionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/incallui/view/SimCardPickDialog;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/SimCardPickDialog;Landroid/view/LayoutInflater;)V
    .locals 2

    iput-object p1, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->this$0:Lcom/android/incallui/view/SimCardPickDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->mOptionList:Ljava/util/List;

    const v0, 0x7f0200a3

    const v1, 0x7f0200a4

    filled-new-array {v0, v1}, [I

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->SMALL_SIM_SLOT_ICON:[I

    iput-object p2, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public add(Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->mOptionList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->mOptionList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->mOptionList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->getItem(I)Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getPositionWithTypeAndId(II)I
    .locals 4

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->mOptionList:Ljava/util/List;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;

    invoke-virtual {v1}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getType()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v1}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getId()I

    move-result v3

    if-ne v3, p2, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, -0x1

    return v3
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const/4 v10, 0x0

    const/16 v9, 0xb

    const/16 v6, 0x8

    const/4 v7, 0x0

    if-nez p2, :cond_1

    iget-object v5, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f030013

    invoke-virtual {v5, v8, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;

    invoke-direct {v4, p0, v10}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;-><init>(Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;)V

    const v5, 0x7f0a0096

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mDisplayName:Landroid/widget/TextView;

    const v5, 0x7f0a0097

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v5, 0x7f0a0098

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mDescription:Landroid/widget/TextView;

    const v5, 0x7f0a0099

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mExtra:Landroid/widget/TextView;

    invoke-virtual {p2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->getItem(I)Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getType()I

    move-result v3

    invoke-virtual {v2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mDisplayName:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mDescription:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mDescription:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v6

    :goto_1
    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v8, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-ne v3, v9, :cond_3

    move v5, v7

    :goto_2
    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mExtra:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->isActive()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->setEnabled(Z)V

    if-ne v3, v9, :cond_0

    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->SMALL_SIM_SLOT_ICON:[I

    invoke-virtual {v2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getId()I

    move-result v8

    aget v6, v6, v8

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->isActive()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->isDefault()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->this$0:Lcom/android/incallui/view/SimCardPickDialog;

    invoke-static {v5}, Lcom/android/incallui/view/SimCardPickDialog;->-get0(Lcom/android/incallui/view/SimCardPickDialog;)Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/android/incallui/view/SimCardPickDialog$AccountInfo;->getLastTime()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const v8, 0x7f0b007d

    invoke-virtual {v5, v8, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mExtra:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mExtra:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->this$0:Lcom/android/incallui/view/SimCardPickDialog;

    invoke-static {v6}, Lcom/android/incallui/view/SimCardPickDialog;->-get2(Lcom/android/incallui/view/SimCardPickDialog;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mExtra:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_3
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;

    goto/16 :goto_0

    :cond_2
    move v5, v7

    goto :goto_1

    :cond_3
    const/4 v5, 0x4

    goto :goto_2

    :cond_4
    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mExtra:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mExtra:Landroid/widget/TextView;

    const v6, 0x7f0b0080

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, v4, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter$ViewHolder;->mExtra:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/android/incallui/view/SimCardPickDialog$SelectorAdapter;->this$0:Lcom/android/incallui/view/SimCardPickDialog;

    invoke-static {v6}, Lcom/android/incallui/view/SimCardPickDialog;->-get1(Lcom/android/incallui/view/SimCardPickDialog;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3
.end method
