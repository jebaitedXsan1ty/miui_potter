.class public Lcom/android/incallui/view/SingleVideoCallInfoView;
.super Landroid/widget/RelativeLayout;
.source "SingleVideoCallInfoView.java"

# interfaces
.implements Lcom/android/incallui/InCallPresenter$InCallOrientationListener;


# instance fields
.field private mBannerWidth:I

.field private mDTMFHeight:I

.field private mDTMFTextMarginTop:I

.field private mDTMFWidth:I

.field private mDialPadVisible:Z

.field private mHasHoldCall:Z

.field private mLastOrientation:I

.field private mRotationAnimatorSet:Landroid/animation/AnimatorSet;

.field private mTimeHeight:I

.field private mTimeTextMarginTop:I

.field private mTimeWidth:I

.field private mVideoCallDTMF:Landroid/widget/TextView;

.field private mVideoCallElapsedTime:Landroid/widget/TextView;

.field private mVideoHoldCallHeight:I

.field private mVideoHoldCallPaddingTop:I

.field private mVideoSessionStateLabel:Landroid/widget/TextView;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/SingleVideoCallInfoView;)I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFHeight:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/incallui/view/SingleVideoCallInfoView;)I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mLastOrientation:I

    return v0
.end method

.method static synthetic -get2(Lcom/android/incallui/view/SingleVideoCallInfoView;)I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeHeight:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/incallui/view/SingleVideoCallInfoView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/incallui/view/SingleVideoCallInfoView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/incallui/view/SingleVideoCallInfoView;I)I
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFHeight:I

    return p1
.end method

.method static synthetic -set1(Lcom/android/incallui/view/SingleVideoCallInfoView;I)I
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeHeight:I

    return p1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mLastOrientation:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mLastOrientation:I

    return-void
.end method

.method private showAndInitializeSecondaryCallInfo(Landroid/view/View;)V
    .locals 2

    const v1, 0x7f0a00a4

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    const v1, 0x7f0a00a5

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    const v1, 0x7f0a00a3

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoSessionStateLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFTextMarginTop:I

    const v1, 0x7f090019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeTextMarginTop:I

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallPaddingTop:I

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallHeight:I

    const v1, 0x7f09001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFWidth:I

    const v1, 0x7f09001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeWidth:I

    invoke-virtual {p0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->getScreenSize()Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mBannerWidth:I

    return-void
.end method


# virtual methods
.method public getScreenSize()Landroid/graphics/Point;
    .locals 5

    invoke-virtual {p0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    return-object v1
.end method

.method public initRotation()V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mLastOrientation:I

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->startRotationAnimator(I)V

    iput v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mLastOrientation:I

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeOrientationListener(Lcom/android/incallui/InCallPresenter$InCallOrientationListener;)V

    return-void
.end method

.method public onDeviceOrientationChanged(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onDeviceOrientationChanged orientation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mLastOrientation:I

    if-eq v0, p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->startRotationAnimator(I)V

    iput p1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mLastOrientation:I

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    invoke-direct {p0, p0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->showAndInitializeSecondaryCallInfo(Landroid/view/View;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/android/incallui/InCallPresenter;->addOrientationListener(Lcom/android/incallui/InCallPresenter$InCallOrientationListener;)V

    iget-object v2, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/android/incallui/view/SingleVideoCallInfoView$1;

    invoke-direct {v2, p0}, Lcom/android/incallui/view/SingleVideoCallInfoView$1;-><init>(Lcom/android/incallui/view/SingleVideoCallInfoView;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-object v2, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/android/incallui/view/SingleVideoCallInfoView$2;

    invoke-direct {v2, p0}, Lcom/android/incallui/view/SingleVideoCallInfoView$2;-><init>(Lcom/android/incallui/view/SingleVideoCallInfoView;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method public setCallElapsedTime(ZLjava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/incallui/AnimationUtils$Fade;->show(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setDialPadVisible(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDialPadVisible:Z

    iget-object v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setHasHoldCall(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mHasHoldCall:Z

    invoke-virtual {p0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->setVideoBannerContentPosition()V

    return-void
.end method

.method public setVideoBannerContentPosition()V
    .locals 6

    iget v3, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeTextMarginTop:I

    iget v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFTextMarginTop:I

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mLastOrientation:I

    const/16 v5, 0x5a

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mLastOrientation:I

    const/16 v5, 0x10e

    if-ne v4, v5, :cond_1

    :cond_0
    iget-boolean v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mHasHoldCall:Z

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallPaddingTop:I

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallHeight:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    :cond_1
    iget-boolean v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mHasHoldCall:Z

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallPaddingTop:I

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallHeight:I

    add-int/2addr v4, v5

    :goto_0
    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    return-void

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public showDigitsToField(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDialPadVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showVideoCallSessionLabel(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoSessionStateLabel:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoSessionStateLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoSessionStateLabel:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public startRotationAnimator(I)V
    .locals 12

    const/16 v11, 0x11

    const/4 v10, 0x5

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "startRotationAnimator end="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    new-array v4, v9, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    const/4 v6, 0x1

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoSessionStateLabel:Landroid/widget/TextView;

    const/4 v6, 0x2

    aput-object v5, v4, v6

    invoke-static {p1, v7, v4}, Lcom/android/incallui/AnimationUtils;->startRotationAnimator(II[Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v4

    iput-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mBannerWidth:I

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFWidth:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v0, v4

    const/4 v1, 0x0

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mBannerWidth:I

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeWidth:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v2, v4

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeTextMarginTop:I

    int-to-float v3, v4

    iget-boolean v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mHasHoldCall:Z

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallPaddingTop:I

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallHeight:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v3, v4

    :cond_1
    sparse-switch p1, :sswitch_data_0

    :goto_0
    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFHeight:I

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "startRotationAnimator dtmfMoveX="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", dtmfMoveY="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setX(F)V

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setY(F)V

    :cond_2
    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeHeight:I

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "startRotationAnimator timeMoveX="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", timeMoveY="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setX(F)V

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setY(F)V

    :cond_3
    return-void

    :sswitch_0
    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFTextMarginTop:I

    int-to-float v4, v4

    add-float v1, v8, v4

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0

    :sswitch_1
    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setGravity(I)V

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFHeight:I

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v0, v4

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFWidth:I

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeTextMarginTop:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    add-float v1, v8, v4

    iget-boolean v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mHasHoldCall:Z

    if-eqz v4, :cond_4

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallPaddingTop:I

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallHeight:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v1, v4

    :cond_4
    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeWidth:I

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v3, v4

    goto/16 :goto_0

    :sswitch_2
    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallDTMF:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoCallElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setGravity(I)V

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFHeight:I

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v0, v4

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFWidth:I

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mDTMFHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeTextMarginTop:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    add-float v1, v8, v4

    iget-boolean v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mHasHoldCall:Z

    if-eqz v4, :cond_5

    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallPaddingTop:I

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mVideoHoldCallHeight:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v1, v4

    :cond_5
    iget v4, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeWidth:I

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/android/incallui/view/SingleVideoCallInfoView;->mTimeHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v3, v4

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_0
        0x10e -> :sswitch_2
        0x168 -> :sswitch_0
    .end sparse-switch
.end method
