.class public Lcom/android/incallui/view/DoubleCallInfoView;
.super Landroid/widget/LinearLayout;
.source "DoubleCallInfoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;
    }
.end annotation


# instance fields
.field private mDialPadVisible:Z

.field private mDoubleCallInfoOperationListener:Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;

.field private mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

.field private mPrimaryAvatar:Landroid/widget/ImageView;

.field private mPrimaryCallName:Landroid/widget/TextView;

.field private mPrimaryCallState:Landroid/widget/TextView;

.field private mPrimaryCallTime:Landroid/widget/TextView;

.field private mPrimaryConfController:Landroid/widget/ImageView;

.field private mPrimaryHungUp:Landroid/view/View;

.field private mPrimaryInfoView:Landroid/view/View;

.field private mPrimarySimCardInfo:Landroid/widget/ImageView;

.field private mSecondaryAvatar:Landroid/widget/ImageView;

.field private mSecondaryCallName:Landroid/widget/TextView;

.field private mSecondaryCallState:Landroid/widget/TextView;

.field private mSecondaryCallTime:Landroid/widget/TextView;

.field private mSecondaryConfController:Landroid/widget/ImageView;

.field private mSecondaryHungUp:Landroid/view/View;

.field private mSecondaryInfoView:Landroid/view/View;

.field private mSecondarySimCardInfo:Landroid/widget/ImageView;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/DoubleCallInfoView;)Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mDoubleCallInfoOperationListener:Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private showAndInitializeSecondaryCallInfo(Landroid/view/View;)V
    .locals 3

    const v2, 0x7f020028

    const v0, 0x7f0a007d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryInfoView:Landroid/view/View;

    const v0, 0x7f0a0081

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    const v0, 0x7f0a0082

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimarySimCardInfo:Landroid/widget/ImageView;

    const v0, 0x7f0a0083

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallState:Landroid/widget/TextView;

    const v0, 0x7f0a0084

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallTime:Landroid/widget/TextView;

    const v0, 0x7f0a007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryHungUp:Landroid/view/View;

    const v0, 0x7f0a007e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryAvatar:Landroid/widget/ImageView;

    const v0, 0x7f0a0085

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryConfController:Landroid/widget/ImageView;

    const v0, 0x7f0a0086

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryInfoView:Landroid/view/View;

    const v0, 0x7f0a008a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    const v0, 0x7f0a008b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondarySimCardInfo:Landroid/widget/ImageView;

    const v0, 0x7f0a008c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallState:Landroid/widget/TextView;

    const v0, 0x7f0a008d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallTime:Landroid/widget/TextView;

    const v0, 0x7f0a0088

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryHungUp:Landroid/view/View;

    const v0, 0x7f0a0087

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryAvatar:Landroid/widget/ImageView;

    const v0, 0x7f0a008e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryConfController:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryInfoView:Landroid/view/View;

    new-instance v1, Lcom/android/incallui/view/DoubleCallInfoView$1;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/DoubleCallInfoView$1;-><init>(Lcom/android/incallui/view/DoubleCallInfoView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryInfoView:Landroid/view/View;

    new-instance v1, Lcom/android/incallui/view/DoubleCallInfoView$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/DoubleCallInfoView$2;-><init>(Lcom/android/incallui/view/DoubleCallInfoView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryConfController:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/incallui/view/DoubleCallInfoView$3;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/DoubleCallInfoView$3;-><init>(Lcom/android/incallui/view/DoubleCallInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryConfController:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/incallui/view/DoubleCallInfoView$4;

    invoke-direct {v1, p0}, Lcom/android/incallui/view/DoubleCallInfoView$4;-><init>(Lcom/android/incallui/view/DoubleCallInfoView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/android/incallui/view/DoubleCallInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0012

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    invoke-direct {p0, p0}, Lcom/android/incallui/view/DoubleCallInfoView;->showAndInitializeSecondaryCallInfo(Landroid/view/View;)V

    return-void
.end method

.method public setDialPadVisible(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mDialPadVisible:Z

    return-void
.end method

.method public setDoublePrimary(Lcom/android/incallui/model/CallCardInfo;ZZZZI)V
    .locals 7

    const v4, 0x7f020028

    const/16 v6, 0x8

    const/4 v3, 0x0

    const/4 v5, 0x0

    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    iget-boolean v2, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v2}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iput-boolean v5, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    :cond_1
    iget v2, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimarySimCardInfo:Landroid/widget/ImageView;

    iget v3, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimarySimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    if-nez p2, :cond_5

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->photo:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/android/incallui/view/DoubleCallInfoView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->photo:Landroid/graphics/drawable/Drawable;

    invoke-static {v2, v3}, Lcom/android/incallui/ImageUtils;->getCircleAvatarBitmap(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_1
    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-boolean v3, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iget-object v4, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    invoke-virtual {p0, v2, v3, p5, v4}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoublePrimaryName(Ljava/lang/String;ZZLjava/lang/String;)V

    if-eqz p4, :cond_6

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryHungUp:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryHungUp:Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryHungUp:Landroid/view/View;

    new-instance v3, Lcom/android/incallui/view/DoubleCallInfoView$5;

    invoke-direct {v3, p0}, Lcom/android/incallui/view/DoubleCallInfoView$5;-><init>(Lcom/android/incallui/view/DoubleCallInfoView;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mDoubleCallInfoOperationListener:Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mDoubleCallInfoOperationListener:Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;

    invoke-interface {v2}, Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;->enablePrimaryHangUp()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryHungUp:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setEnabled(Z)V

    :cond_2
    :goto_2
    if-eqz p2, :cond_7

    if-lez p6, :cond_7

    xor-int/lit8 v1, p4, 0x1

    :goto_3
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryConfController:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v2}, Lcom/android/incallui/view/DoubleCallInfoView;->showDoubleConferenceControl(ZLandroid/widget/ImageView;)V

    return-void

    :cond_3
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimarySimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryHungUp:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public setDoublePrimaryCallElapsedTime(ZLjava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallTime:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallTime:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/incallui/AnimationUtils$Fade;->show(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallTime:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallTime:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setDoublePrimaryCallInfoVisible(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryInfoView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setDoublePrimaryCallState(ILandroid/telecom/DisconnectCause;Z)V
    .locals 8

    const v7, 0x800005

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, -0x1

    invoke-static {p1, p2, v4, v1}, Lcom/android/incallui/CallUtils;->getCallStateLabelFromState(ILandroid/telecom/DisconnectCause;ZI)Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/incallui/view/DoubleCallInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    if-eq v6, p1, :cond_3

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryConfController:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryConfController:Landroid/widget/ImageView;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getGravity()I

    move-result v1

    if-eq v1, v7, :cond_1

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallState:Landroid/widget/TextView;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryConfController:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryConfController:Landroid/widget/ImageView;

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    goto :goto_1
.end method

.method public setDoublePrimaryName(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p3, :cond_2

    iget-boolean v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mDialPadVisible:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    invoke-static {p1, p2}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/4 v0, 0x0

    if-eqz p2, :cond_1

    const/4 v0, 0x3

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextDirection(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {p1, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/android/incallui/view/DoubleCallInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, p4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mPrimaryCallName:Landroid/widget/TextView;

    invoke-static {p1, p2}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setDoubleSecondary(Lcom/android/incallui/model/CallCardInfo;ZZZZI)V
    .locals 8

    const v7, 0x7f020028

    const/16 v6, 0x8

    const/4 v3, 0x0

    const/4 v5, 0x0

    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    iget-boolean v2, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v2}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iput-boolean v5, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    :cond_1
    iget v2, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondarySimCardInfo:Landroid/widget/ImageView;

    iget v3, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondarySimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-boolean v3, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iget-object v4, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    invoke-virtual {p0, v2, v3, p5, v4}, Lcom/android/incallui/view/DoubleCallInfoView;->setDoubleSecondaryName(Ljava/lang/String;ZZLjava/lang/String;)V

    if-nez p2, :cond_4

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->photo:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/android/incallui/view/DoubleCallInfoView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->photo:Landroid/graphics/drawable/Drawable;

    invoke-static {v2, v3}, Lcom/android/incallui/ImageUtils;->getCircleAvatarBitmap(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_1
    if-eqz p4, :cond_5

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryHungUp:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryHungUp:Landroid/view/View;

    new-instance v3, Lcom/android/incallui/view/DoubleCallInfoView$6;

    invoke-direct {v3, p0}, Lcom/android/incallui/view/DoubleCallInfoView$6;-><init>(Lcom/android/incallui/view/DoubleCallInfoView;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    if-eqz p2, :cond_6

    if-lez p6, :cond_6

    xor-int/lit8 v1, p4, 0x1

    :goto_3
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryConfController:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v2}, Lcom/android/incallui/view/DoubleCallInfoView;->showDoubleConferenceControl(ZLandroid/widget/ImageView;)V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondarySimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryHungUp:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public setDoubleSecondaryCallElapsedTime(ZLjava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallTime:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallTime:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/incallui/AnimationUtils$Fade;->show(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallTime:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallTime:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setDoubleSecondaryCallInfoVisible(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryInfoView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setDoubleSecondaryCallState(ILandroid/telecom/DisconnectCause;Z)V
    .locals 8

    const v7, 0x800005

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, -0x1

    invoke-static {p1, p2, v4, v1}, Lcom/android/incallui/CallUtils;->getCallStateLabelFromState(ILandroid/telecom/DisconnectCause;ZI)Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/incallui/view/DoubleCallInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    if-eq v6, p1, :cond_3

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryConfController:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryConfController:Landroid/widget/ImageView;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getGravity()I

    move-result v1

    if-eq v1, v7, :cond_1

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallState:Landroid/widget/TextView;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallState:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryAvatar:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryConfController:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryConfController:Landroid/widget/ImageView;

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    goto :goto_1
.end method

.method public setDoubleSecondaryName(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p3, :cond_2

    iget-boolean v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mDialPadVisible:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    invoke-static {p1, p2}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/4 v0, 0x0

    if-eqz p2, :cond_1

    const/4 v0, 0x3

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextDirection(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {p1, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/android/incallui/view/DoubleCallInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, p4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mSecondaryCallName:Landroid/widget/TextView;

    invoke-static {p1, p2}, Lcom/android/incallui/util/Utils;->localizeNumber(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setOnDoubleCallInfoOperationListener(Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/DoubleCallInfoView;->mDoubleCallInfoOperationListener:Lcom/android/incallui/view/DoubleCallInfoView$DoubleCallInfoOperationListener;

    return-void
.end method

.method public showDoubleConferenceControl(ZLandroid/widget/ImageView;)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f02008c

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
