.class Lcom/android/incallui/view/SingleVideoCallInfoView$2;
.super Ljava/lang/Object;
.source "SingleVideoCallInfoView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/view/SingleVideoCallInfoView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/SingleVideoCallInfoView;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/SingleVideoCallInfoView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView$2;->this$0:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView$2;->this$0:Lcom/android/incallui/view/SingleVideoCallInfoView;

    iget-object v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView$2;->this$0:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-static {v1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->-get4(Lcom/android/incallui/view/SingleVideoCallInfoView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->-set1(Lcom/android/incallui/view/SingleVideoCallInfoView;I)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "mTimeHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView$2;->this$0:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-static {v1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->-get2(Lcom/android/incallui/view/SingleVideoCallInfoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView$2;->this$0:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-static {v0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->-get2(Lcom/android/incallui/view/SingleVideoCallInfoView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView$2;->this$0:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-static {v0}, Lcom/android/incallui/view/SingleVideoCallInfoView;->-get4(Lcom/android/incallui/view/SingleVideoCallInfoView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-object v0, p0, Lcom/android/incallui/view/SingleVideoCallInfoView$2;->this$0:Lcom/android/incallui/view/SingleVideoCallInfoView;

    iget-object v1, p0, Lcom/android/incallui/view/SingleVideoCallInfoView$2;->this$0:Lcom/android/incallui/view/SingleVideoCallInfoView;

    invoke-static {v1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->-get1(Lcom/android/incallui/view/SingleVideoCallInfoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SingleVideoCallInfoView;->startRotationAnimator(I)V

    :cond_0
    return-void
.end method
