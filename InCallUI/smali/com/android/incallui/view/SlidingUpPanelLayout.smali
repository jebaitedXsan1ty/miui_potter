.class public Lcom/android/incallui/view/SlidingUpPanelLayout;
.super Landroid/view/ViewGroup;
.source "SlidingUpPanelLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;,
        Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;,
        Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;,
        Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-incallui-view-SlidingUpPanelLayout$SlideStateSwitchesValues:[I

.field private static final DEFAULT_ATTRS:[I

.field public static final DIFF_SLIDE_OFFSET:F

.field private static final TAG:Ljava/lang/String;

.field private static mDragSlop:I


# instance fields
.field private mAnchorPoint:F

.field private mCanSlide:Z

.field private mCoveredFadeColor:I

.field private final mCoveredFadePaint:Landroid/graphics/Paint;

.field private mDeviceHeight:I

.field private mDragHelperCallback:Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;

.field private mDragReleaseView:Landroid/view/View;

.field private mDragView:Landroid/view/View;

.field private mDragViewResId:I

.field private mFirstLayout:Z

.field private mInitialMotionX:F

.field private mInitialMotionY:F

.field private mIsSlidingEnabled:Z

.field private mIsSlidingUp:Z

.field private mIsUnableToDrag:Z

.field private mIsUsingDragViewTouchEvents:Z

.field private mMainView:Landroid/view/View;

.field private mMinFlingVelocity:I

.field private mOverlayContent:Z

.field private mPanelHeight:I

.field private mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

.field private mParalaxOffset:I

.field private final mScrollTouchSlop:I

.field private final mShadowDrawable:Landroid/graphics/drawable/Drawable;

.field private mShadowHeight:I

.field private mSlideOffset:F

.field private mSlideRange:I

.field private mSlideState:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

.field private mSlideableView:Landroid/view/View;

.field private final mTmpRect:Landroid/graphics/Rect;

.field private final mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragReleaseView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/view/SlidingUpPanelLayout;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/incallui/view/SlidingUpPanelLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingUp:Z

    return v0
.end method

.method static synthetic -get3(Lcom/android/incallui/view/SlidingUpPanelLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsUnableToDrag:Z

    return v0
.end method

.method static synthetic -get4(Lcom/android/incallui/view/SlidingUpPanelLayout;)I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mMinFlingVelocity:I

    return v0
.end method

.method static synthetic -get5(Lcom/android/incallui/view/SlidingUpPanelLayout;)F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    return v0
.end method

.method static synthetic -get6(Lcom/android/incallui/view/SlidingUpPanelLayout;)I
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideRange:I

    return v0
.end method

.method static synthetic -get7(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideState:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    return-object v0
.end method

.method static synthetic -get8(Lcom/android/incallui/view/SlidingUpPanelLayout;)Lcom/android/incallui/view/ViewDragHelper;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    return-object v0
.end method

.method private static synthetic -getcom-android-incallui-view-SlidingUpPanelLayout$SlideStateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->-com-android-incallui-view-SlidingUpPanelLayout$SlideStateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->-com-android-incallui-view-SlidingUpPanelLayout$SlideStateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->values()[Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->COLLAPSED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    invoke-virtual {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->EXPANDED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    invoke-virtual {v1}, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    sput-object v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->-com-android-incallui-view-SlidingUpPanelLayout$SlideStateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/incallui/view/SlidingUpPanelLayout;Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;)Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideState:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/view/SlidingUpPanelLayout;)I
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getSlidingTop()I

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/incallui/view/SlidingUpPanelLayout;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/incallui/view/SlidingUpPanelLayout;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->onPanelDragged(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->TAG:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100af

    const/4 v2, 0x0

    aput v1, v0, v2

    sput-object v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->DEFAULT_ATTRS:[I

    const v0, 0x3e6147b0    # 0.22000003f

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sput v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->DIFF_SLIDE_OFFSET:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v6, 0x0

    const/4 v8, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v6, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCoveredFadeColor:I

    const/16 v4, 0x190

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mMinFlingVelocity:I

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCoveredFadePaint:Landroid/graphics/Paint;

    iput v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelHeight:I

    iput v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowHeight:I

    iput v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mParalaxOffset:I

    iput-boolean v6, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mOverlayContent:Z

    iput v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragViewResId:I

    iput-object v9, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragReleaseView:Landroid/view/View;

    sget-object v4, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->COLLAPSED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    iput-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideState:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    const/4 v4, 0x0

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mAnchorPoint:F

    iput-boolean v5, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->isInEditMode()Z

    move-result v4

    if-eqz v4, :cond_0

    iput-object v9, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowDrawable:Landroid/graphics/drawable/Drawable;

    iput v6, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mScrollTouchSlop:I

    iput-object v9, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    return-void

    :cond_0
    if-eqz p2, :cond_4

    sget-object v4, Lcom/android/incallui/view/SlidingUpPanelLayout;->DEFAULT_ATTRS:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    const/16 v4, 0x30

    if-eq v2, v4, :cond_1

    const/16 v4, 0x50

    if-eq v2, v4, :cond_1

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "gravity must be set to either top or bottom"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    const/16 v4, 0x50

    if-ne v2, v4, :cond_8

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingUp:Z

    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    sget-object v4, Lcom/android/incallui/R$styleable;->SlidingUpPanelLayout:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3, v6, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelHeight:I

    invoke-virtual {v3, v5, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowHeight:I

    const/4 v4, 0x2

    invoke-virtual {v3, v4, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mParalaxOffset:I

    const/4 v4, 0x4

    const/16 v7, 0x190

    invoke-virtual {v3, v4, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mMinFlingVelocity:I

    const/4 v4, 0x3

    invoke-virtual {v3, v4, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCoveredFadeColor:I

    const/4 v4, 0x5

    invoke-virtual {v3, v4, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragViewResId:I

    const/4 v4, 0x6

    invoke-virtual {v3, v4, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mOverlayContent:Z

    :cond_3
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->density:F

    iget v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelHeight:I

    if-ne v4, v8, :cond_5

    const/high16 v4, 0x42880000    # 68.0f

    mul-float/2addr v4, v1

    add-float/2addr v4, v10

    float-to-int v4, v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelHeight:I

    :cond_5
    iget v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowHeight:I

    if-ne v4, v8, :cond_6

    const/high16 v4, 0x40800000    # 4.0f

    mul-float/2addr v4, v1

    add-float/2addr v4, v10

    float-to-int v4, v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowHeight:I

    :cond_6
    iget v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mParalaxOffset:I

    if-ne v4, v8, :cond_7

    const/4 v4, 0x0

    mul-float/2addr v4, v1

    float-to-int v4, v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mParalaxOffset:I

    :cond_7
    iget v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowHeight:I

    if-lez v4, :cond_a

    iget-boolean v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingUp:Z

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f02000d

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowDrawable:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {p0, v6}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setWillNotDraw(Z)V

    new-instance v4, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;

    invoke-direct {v4, p0}, Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;-><init>(Lcom/android/incallui/view/SlidingUpPanelLayout;)V

    iput-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragHelperCallback:Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragHelperCallback:Lcom/android/incallui/view/SlidingUpPanelLayout$DragHelperCallback;

    invoke-static {p0, v10, v4}, Lcom/android/incallui/view/ViewDragHelper;->create(Landroid/view/ViewGroup;FLcom/android/incallui/view/ViewDragHelper$Callback;)Lcom/android/incallui/view/ViewDragHelper;

    move-result-object v4

    iput-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v4}, Lcom/android/incallui/view/ViewDragHelper;->getMinVelocity()F

    move-result v4

    float-to-int v4, v4

    mul-int/lit8 v4, v4, 0x1e

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mMinFlingVelocity:I

    iput-boolean v5, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCanSlide:Z

    iput-boolean v5, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingEnabled:Z

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mScrollTouchSlop:I

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v4}, Lcom/android/incallui/view/ViewDragHelper;->getTouchSlop()I

    move-result v4

    sput v4, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragSlop:I

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDeviceHeight:I

    return-void

    :cond_8
    move v4, v6

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f02000e

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    :cond_a
    iput-object v9, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method private collapsePanel(Landroid/view/View;I)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, p2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->smoothSlideTo(FI)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private expandPanel(Landroid/view/View;IF)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p3, p2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->smoothSlideTo(FI)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getSlidingTop()I
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingUp:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private static hasOpaqueBackground(Landroid/view/View;)Z
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private isDragViewUnder(II)Z
    .locals 9

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getDragView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v5, "isDragViewUnder: dragView = null"

    invoke-direct {p0, v5}, Lcom/android/incallui/view/SlidingUpPanelLayout;->log(Ljava/lang/String;)V

    return v6

    :cond_0
    new-array v4, v7, [I

    invoke-virtual {v0, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    new-array v1, v7, [I

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getLocationOnScreen([I)V

    aget v7, v1, v6

    add-int v2, v7, p1

    aget v7, v1, v5

    add-int v3, v7, p2

    aget v7, v4, v6

    if-lt v2, v7, :cond_2

    aget v7, v4, v6

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    if-ge v2, v7, :cond_2

    aget v7, v4, v5

    if-lt v3, v7, :cond_2

    aget v7, v4, v5

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v7, v8

    if-ge v3, v7, :cond_1

    :goto_0
    return v5

    :cond_1
    move v5, v6

    goto :goto_0

    :cond_2
    move v5, v6

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onPanelDragged(I)V
    .locals 6

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getSlidingTop()I

    move-result v1

    sub-int v2, p1, v1

    int-to-float v2, v2

    iget v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideRange:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onPanelDragged: mSlideOffset = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " newTop = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " topBound = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->log(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    cmpg-float v2, v2, v4

    if-gez v2, :cond_2

    iput v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->dispatchOnPanelSlide(Landroid/view/View;)V

    iget v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mParalaxOffset:I

    if-lez v2, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getCurrentParalaxOffset()I

    move-result v0

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mMainView:Landroid/view/View;

    int-to-float v3, v0

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationY(F)V

    :cond_1
    return-void

    :cond_2
    iget v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_0

    iput v5, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    goto :goto_0
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public collapsePanel()Z
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->collapsePanel(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public collapsePanelManual()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    sget-object v0, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->COLLAPSED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    iput-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideState:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->requestLayout()V

    return-void
.end method

.method public computeScroll()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/ViewDragHelper;->continueSettling(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCanSlide:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v0}, Lcom/android/incallui/view/ViewDragHelper;->abort()V

    return-void

    :cond_0
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method dispatchOnPanelCollapsed(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    invoke-interface {v0, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;->onPanelCollapsed(Landroid/view/View;)V

    :cond_0
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->sendAccessibilityEvent(I)V

    return-void
.end method

.method dispatchOnPanelExpanded(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    invoke-interface {v0, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;->onPanelExpanded(Landroid/view/View;)V

    :cond_0
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->sendAccessibilityEvent(I)V

    return-void
.end method

.method dispatchOnPanelSlide(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    iget v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragView:Landroid/view/View;

    invoke-interface {v0, p1, v1, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;->onPanelSlide(Landroid/view/View;FLandroid/view/View;)V

    :cond_0
    return-void
.end method

.method dispatchOnPanelViewExpandReleased(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    invoke-interface {v0, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;->onPanelViewExpandReleased(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method dispatchPanelCollapsedReleased(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    invoke-interface {v0, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;->onPanelViewCollapsedReleased(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v0, v4, 0xff

    if-eqz v0, :cond_0

    const/4 v4, 0x5

    if-ne v0, v4, :cond_1

    :cond_0
    sget-object v4, Lcom/android/incallui/view/SlidingUpPanelLayout;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "dispatchTouchEvent "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    return v4

    :pswitch_0
    iput v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionX:F

    iput v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionY:F

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v4}, Lcom/android/incallui/view/ViewDragHelper;->getViewDragState()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    const-string/jumbo v4, "dispatchTouchEvent: dragState = STATE_SETTLING"

    invoke-direct {p0, v4}, Lcom/android/incallui/view/SlidingUpPanelLayout;->log(Ljava/lang/String;)V

    const/4 v4, 0x1

    return v4

    :cond_2
    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v4, p1}, Lcom/android/incallui/view/ViewDragHelper;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    if-nez v4, :cond_0

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v2

    iget-boolean v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingUp:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    iget v5, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowHeight:I

    sub-int v3, v4, v5

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    return-void

    :cond_2
    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v3

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    iget v5, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mShadowHeight:I

    add-int v0, v4, v5

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 11

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;

    const/4 v7, 0x2

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->save(I)I

    move-result v6

    const/4 v2, 0x0

    iget-boolean v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCanSlide:Z

    if-eqz v7, :cond_1

    iget-boolean v7, v4, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->slideable:Z

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    if-eqz v7, :cond_1

    iget-boolean v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mOverlayContent:Z

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    iget-boolean v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingUp:Z

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mTmpRect:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mTmpRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget-object v9, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, v7, Landroid/graphics/Rect;->bottom:I

    :goto_0
    iget-object v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mTmpRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    :cond_0
    iget v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    cmpg-float v7, v7, v10

    if-gez v7, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v5

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    if-eqz v2, :cond_2

    iget v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCoveredFadeColor:I

    const/high16 v8, -0x1000000

    and-int/2addr v7, v8

    ushr-int/lit8 v0, v7, 0x18

    int-to-float v7, v0

    iget v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    sub-float v8, v10, v8

    mul-float/2addr v7, v8

    float-to-int v3, v7

    shl-int/lit8 v7, v3, 0x18

    iget v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCoveredFadeColor:I

    const v9, 0xffffff

    and-int/2addr v8, v9

    or-int v1, v7, v8

    iget-object v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCoveredFadePaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mTmpRect:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCoveredFadePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    return v5

    :cond_3
    iget-object v7, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mTmpRect:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mTmpRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    iget-object v9, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getBottom()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    iput v8, v7, Landroid/graphics/Rect;->top:I

    goto :goto_0
.end method

.method public expandPanel()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->expandPanel(F)Z

    move-result v0

    return v0
.end method

.method public expandPanel(F)Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideState:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    sget-object v1, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->EXPANDED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    if-ne v0, v1, :cond_1

    :cond_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->isPaneVisible()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->showPanel()V

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-direct {p0, v0, v2, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->expandPanel(Landroid/view/View;IF)Z

    move-result v0

    return v0
.end method

.method public expandPanelManual()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    sget-object v0, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->EXPANDED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    iput-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideState:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->requestLayout()V

    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    new-instance v0, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;

    invoke-direct {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;-><init>()V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getCurrentParalaxOffset()I
    .locals 4

    iget v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mParalaxOffset:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v2, v3, v2

    mul-float/2addr v1, v2

    float-to-int v0, v1

    iget-boolean v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingUp:Z

    if-eqz v1, :cond_0

    neg-int v0, v0

    :cond_0
    return v0
.end method

.method getDragView()Landroid/view/View;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragView:Landroid/view/View;

    :cond_0
    return-object v0
.end method

.method public getSlideOffset()F
    .locals 1

    iget v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    return v0
.end method

.method public isExpanded()Z
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideState:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    sget-object v1, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->EXPANDED:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaneVisible()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildCount()I

    move-result v3

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    return v2

    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    iget v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragViewResId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragViewResId:I

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragView:Landroid/view/View;

    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v10

    and-int/lit16 v0, v10, 0xff

    iget-boolean v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCanSlide:Z

    if-eqz v10, :cond_0

    iget-boolean v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingEnabled:Z

    xor-int/lit8 v10, v10, 0x1

    if-nez v10, :cond_0

    iget-boolean v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsUnableToDrag:Z

    if-eqz v10, :cond_1

    if-eqz v0, :cond_1

    :cond_0
    iget-object v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v10}, Lcom/android/incallui/view/ViewDragHelper;->cancel()V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v10

    return v10

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    const/4 v7, 0x0

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_0
    :pswitch_0
    iget-object v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v10, p1}, Lcom/android/incallui/view/ViewDragHelper;->shouldInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    return v13

    :pswitch_1
    iput-boolean v13, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsUnableToDrag:Z

    iput v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionX:F

    iput v9, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionY:F

    float-to-int v10, v8

    float-to-int v11, v9

    invoke-direct {p0, v10, v11}, Lcom/android/incallui/view/SlidingUpPanelLayout;->isDragViewUnder(II)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-boolean v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsUsingDragViewTouchEvents:Z

    xor-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_2

    const/4 v7, 0x1

    goto :goto_0

    :pswitch_2
    iput v8, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionX:F

    iput v9, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionY:F

    goto :goto_0

    :pswitch_3
    iget v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionX:F

    sub-float v10, v8, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionY:F

    sub-float v10, v9, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-boolean v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsUsingDragViewTouchEvents:Z

    if-eqz v10, :cond_4

    iget v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mScrollTouchSlop:I

    int-to-float v10, v10

    cmpl-float v10, v1, v10

    if-lez v10, :cond_3

    iget v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mScrollTouchSlop:I

    int-to-float v10, v10

    cmpg-float v10, v2, v10

    if-gez v10, :cond_3

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v10

    return v10

    :cond_3
    iget v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mScrollTouchSlop:I

    int-to-float v10, v10

    cmpl-float v10, v2, v10

    if-lez v10, :cond_4

    float-to-int v10, v8

    float-to-int v11, v9

    invoke-direct {p0, v10, v11}, Lcom/android/incallui/view/SlidingUpPanelLayout;->isDragViewUnder(II)Z

    move-result v7

    :cond_4
    sget v10, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragSlop:I

    int-to-float v10, v10

    cmpl-float v10, v2, v10

    if-lez v10, :cond_5

    cmpl-float v10, v1, v2

    if-lez v10, :cond_5

    iget-object v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v10}, Lcom/android/incallui/view/ViewDragHelper;->cancel()V

    iput-boolean v13, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsUnableToDrag:Z

    return v13

    :cond_5
    :try_start_0
    iget-object v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v10, p1}, Lcom/android/incallui/view/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "onInterceptTouchEvent: ACTION_MOVE error "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/android/incallui/view/SlidingUpPanelLayout;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_4
    :try_start_1
    iget-object v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v10, p1}, Lcom/android/incallui/view/ViewDragHelper;->processTouchEvent(Landroid/view/MotionEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iget v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionX:F

    sub-float v10, v8, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mInitialMotionY:F

    sub-float v10, v9, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getDragView()Landroid/view/View;

    move-result-object v3

    cmpl-float v10, v4, v12

    if-nez v10, :cond_7

    cmpl-float v10, v5, v12

    if-nez v10, :cond_7

    :cond_6
    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->isExpanded()Z

    move-result v10

    if-nez v10, :cond_9

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->collapsePanel()Z

    goto/16 :goto_0

    :catch_1
    move-exception v6

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "onInterceptTouchEvent: ACTION_UP error "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/android/incallui/view/SlidingUpPanelLayout;->log(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    sget v10, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragSlop:I

    int-to-float v10, v10

    cmpg-float v10, v5, v10

    if-ltz v10, :cond_6

    mul-float v10, v4, v4

    mul-float v11, v5, v5

    add-float/2addr v10, v11

    sget v11, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragSlop:I

    sget v12, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragSlop:I

    mul-int/2addr v11, v12

    int-to-float v11, v11

    cmpg-float v10, v10, v11

    if-gez v10, :cond_2

    float-to-int v10, v8

    float-to-int v11, v9

    invoke-direct {p0, v10, v11}, Lcom/android/incallui/view/SlidingUpPanelLayout;->isDragViewUnder(II)Z

    move-result v10

    if-eqz v10, :cond_2

    if-eqz v3, :cond_8

    invoke-virtual {v3, v13}, Landroid/view/View;->playSoundEffect(I)V

    :cond_8
    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->isExpanded()Z

    move-result v10

    if-nez v10, :cond_a

    iget v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mAnchorPoint:F

    invoke-virtual {p0, v10}, Lcom/android/incallui/view/SlidingUpPanelLayout;->expandPanel(F)Z

    goto/16 :goto_0

    :cond_9
    iget v10, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mAnchorPoint:F

    invoke-virtual {p0, v10}, Lcom/android/incallui/view/SlidingUpPanelLayout;->expandPanel(F)Z

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->collapsePanel()Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 14

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingLeft()I

    move-result v9

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v10

    invoke-direct {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getSlidingTop()I

    move-result v11

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildCount()I

    move-result v2

    iget-boolean v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    if-eqz v12, :cond_0

    invoke-static {}, Lcom/android/incallui/view/SlidingUpPanelLayout;->-getcom-android-incallui-view-SlidingUpPanelLayout$SlideStateSwitchesValues()[I

    move-result-object v12

    iget-object v13, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideState:Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;

    invoke-virtual {v13}, Lcom/android/incallui/view/SlidingUpPanelLayout$SlideState;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    const/high16 v12, 0x3f800000    # 1.0f

    iput v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    :cond_0
    :goto_0
    const/4 v7, 0x0

    :goto_1
    if-ge v7, v2, :cond_8

    invoke-virtual {p0, v7}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_2

    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :pswitch_0
    iget-boolean v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCanSlide:Z

    if-eqz v12, :cond_1

    const/4 v12, 0x0

    :goto_3
    iput v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    goto :goto_0

    :cond_1
    const/high16 v12, 0x3f800000    # 1.0f

    goto :goto_3

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget-boolean v12, v8, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->slideable:Z

    if-eqz v12, :cond_3

    iget v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelHeight:I

    sub-int v12, v3, v12

    iput v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideRange:I

    :cond_3
    iget-boolean v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingUp:Z

    if-eqz v12, :cond_6

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "slidingTop = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " , mSlideRange = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideRange:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string/jumbo v13, " , mSlideOffset = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/android/incallui/view/SlidingUpPanelLayout;->log(Ljava/lang/String;)V

    iget-boolean v12, v8, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->slideable:Z

    if-eqz v12, :cond_5

    iget v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideRange:I

    int-to-float v12, v12

    iget v13, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    mul-float/2addr v12, v13

    float-to-int v12, v12

    add-int v6, v11, v12

    :cond_4
    :goto_4
    add-int v1, v6, v3

    move v4, v9

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    add-int v5, v9, v12

    invoke-virtual {v0, v9, v6, v5, v1}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    :cond_5
    move v6, v10

    goto :goto_4

    :cond_6
    iget-boolean v12, v8, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->slideable:Z

    if-eqz v12, :cond_7

    iget v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideRange:I

    int-to-float v12, v12

    iget v13, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideOffset:F

    mul-float/2addr v12, v13

    float-to-int v12, v12

    sub-int v6, v11, v12

    :goto_5
    iget-boolean v12, v8, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->slideable:Z

    if-nez v12, :cond_4

    iget-boolean v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mOverlayContent:Z

    xor-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_4

    iget v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelHeight:I

    add-int/2addr v6, v12

    goto :goto_4

    :cond_7
    move v6, v10

    goto :goto_5

    :cond_8
    iget-boolean v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    if-eqz v12, :cond_9

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->updateObscuredViewVisibility()V

    :cond_9
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 17

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    const/high16 v15, 0x40000000    # 2.0f

    if-eq v13, v15, :cond_0

    new-instance v15, Ljava/lang/IllegalStateException;

    const-string/jumbo v16, "Width must have an exact value or MATCH_PARENT"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_0
    const/high16 v15, 0x40000000    # 2.0f

    if-eq v6, v15, :cond_1

    new-instance v15, Ljava/lang/IllegalStateException;

    const-string/jumbo v16, "Height must have an exact value or MATCH_PARENT"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v15

    sub-int v15, v7, v15

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingBottom()I

    move-result v16

    sub-int v9, v15, v16

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingLeft()I

    move-result v15

    sub-int v15, v14, v15

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingRight()I

    move-result v16

    sub-int v10, v15, v16

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelHeight:I

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildCount()I

    move-result v2

    const/4 v15, 0x2

    if-le v2, v15, :cond_3

    sget-object v15, Lcom/android/incallui/view/SlidingUpPanelLayout;->TAG:Ljava/lang/String;

    const-string/jumbo v16, "onMeasure: More than two child views are not supported."

    invoke-static/range {v15 .. v16}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_0
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCanSlide:Z

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v2, :cond_c

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;

    move v5, v9

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    const/4 v15, 0x0

    iput-boolean v15, v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->dimWhenOffset:Z

    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_3
    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    invoke-virtual {v15}, Landroid/view/View;->getVisibility()I

    move-result v15

    const/16 v16, 0x8

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    const/4 v12, 0x0

    goto :goto_0

    :cond_4
    const/4 v15, 0x1

    if-ne v8, v15, :cond_6

    const/4 v15, 0x1

    iput-boolean v15, v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->slideable:Z

    const/4 v15, 0x1

    iput-boolean v15, v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->dimWhenOffset:Z

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCanSlide:Z

    :goto_3
    iget v15, v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->width:I

    const/16 v16, -0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_8

    const/high16 v15, -0x80000000

    invoke-static {v10, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    :goto_4
    iget v15, v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->height:I

    const/16 v16, -0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    const/high16 v15, -0x80000000

    invoke-static {v5, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    :goto_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    if-ne v1, v15, :cond_5

    invoke-static {v3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelHeight:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideRange:I

    :cond_5
    invoke-virtual {v1, v4, v3}, Landroid/view/View;->measure(II)V

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mOverlayContent:Z

    if-nez v15, :cond_7

    sub-int v5, v9, v12

    :cond_7
    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mMainView:Landroid/view/View;

    goto :goto_3

    :cond_8
    iget v15, v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->width:I

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v10, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_4

    :cond_9
    iget v15, v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->width:I

    const/high16 v16, 0x40000000    # 2.0f

    invoke-static/range {v15 .. v16}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_4

    :cond_a
    iget v15, v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->height:I

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_b

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v5, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto :goto_5

    :cond_b
    iget v15, v11, Lcom/android/incallui/view/SlidingUpPanelLayout$LayoutParams;->height:I

    const/high16 v16, 0x40000000    # 2.0f

    invoke-static/range {v15 .. v16}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto :goto_5

    :cond_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v7}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    if-eq p2, p4, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mFirstLayout:Z

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public resetDragState()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    invoke-virtual {v0}, Lcom/android/incallui/view/ViewDragHelper;->cancel()V

    return-void
.end method

.method setAllChildrenVisible()V
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v2, v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setDragView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragView:Landroid/view/View;

    return-void
.end method

.method public setOverlayed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mOverlayContent:Z

    return-void
.end method

.method public setPanelHeight(I)V
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelHeight:I

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->requestLayout()V

    return-void
.end method

.method public setPanelSlideListener(Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mPanelSlideListener:Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;

    return-void
.end method

.method public setSlidingEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingEnabled:Z

    return-void
.end method

.method public setSlidingUpLayoutPaddingTop(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0, v0, p1, v1, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setPadding(IIII)V

    return-void
.end method

.method public setTouchDragView(Landroid/view/View;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setDragView(Landroid/view/View;)V

    invoke-virtual {p0, p2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setSlidingEnabled(Z)V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mDragReleaseView:Landroid/view/View;

    :cond_0
    return-void
.end method

.method public showPanel()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->requestLayout()V

    return-void
.end method

.method smoothSlideTo(FI)Z
    .locals 6

    const/4 v5, 0x0

    iget-boolean v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mCanSlide:Z

    if-nez v2, :cond_0

    return v5

    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getSlidingTop()I

    move-result v0

    iget-boolean v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mIsSlidingUp:Z

    if-eqz v2, :cond_1

    int-to-float v2, v0

    iget v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideRange:I

    int-to-float v3, v3

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    float-to-int v1, v2

    :goto_0
    iget-object v2, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mViewDragHelper:Lcom/android/incallui/view/ViewDragHelper;

    iget-object v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    iget-object v4, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v2, v3, v4, v1}, Lcom/android/incallui/view/ViewDragHelper;->smoothSlideViewTo(Landroid/view/View;II)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setAllChildrenVisible()V

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    const/4 v2, 0x1

    return v2

    :cond_1
    int-to-float v2, v0

    iget v3, p0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideRange:I

    int-to-float v3, v3

    mul-float/2addr v3, p1

    sub-float/2addr v2, v3

    float-to-int v1, v2

    goto :goto_0

    :cond_2
    return v5
.end method

.method updateObscuredViewVisibility()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildCount()I

    move-result v15

    if-nez v15, :cond_0

    return-void

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingLeft()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getWidth()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingRight()I

    move-result v16

    sub-int v11, v15, v16

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingTop()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getHeight()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getPaddingBottom()I

    move-result v16

    sub-int v2, v15, v16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-static {v15}, Lcom/android/incallui/view/SlidingUpPanelLayout;->hasOpaqueBackground(Landroid/view/View;)Z

    move-result v15

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getLeft()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getRight()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getTop()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/incallui/view/SlidingUpPanelLayout;->mSlideableView:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getBottom()I

    move-result v1

    :goto_0
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v15

    invoke-static {v9, v15}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v15

    invoke-static {v13, v15}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v15

    invoke-static {v11, v15}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v15

    invoke-static {v2, v15}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-lt v5, v8, :cond_2

    if-lt v7, v12, :cond_2

    if-gt v6, v10, :cond_2

    if-gt v4, v1, :cond_2

    const/4 v14, 0x4

    :goto_1
    invoke-virtual {v3, v14}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    const/4 v1, 0x0

    const/4 v12, 0x0

    const/4 v10, 0x0

    const/4 v8, 0x0

    goto :goto_0

    :cond_2
    const/4 v14, 0x0

    goto :goto_1
.end method
