.class public abstract Lcom/android/incallui/view/IndexSortedView;
.super Landroid/widget/LinearLayout;
.source "IndexSortedView.java"


# instance fields
.field private dividerMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private hideBelowDividerSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private hideSelfDividerSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalViewsCount:I

.field private viewMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/incallui/view/IndexSortedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/incallui/view/IndexSortedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/IndexSortedView;->setOrientation(I)V

    invoke-direct {p0}, Lcom/android/incallui/view/IndexSortedView;->initData()V

    return-void
.end method

.method private controlDividerToShow(ZIILandroid/view/View;)V
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/IndexSortedView;->hideSelfDividerSet:Ljava/util/HashSet;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/view/IndexSortedView;->hideBelowDividerSet:Ljava/util/HashSet;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private initData()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/view/IndexSortedView;->getTotalViewCount()I

    move-result v0

    iput v0, p0, Lcom/android/incallui/view/IndexSortedView;->mTotalViewsCount:I

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v1, p0, Lcom/android/incallui/view/IndexSortedView;->viewMap:Landroid/util/SparseArray;

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v1, p0, Lcom/android/incallui/view/IndexSortedView;->dividerMap:Landroid/util/SparseArray;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lcom/android/incallui/view/IndexSortedView;->hideSelfDividerSet:Ljava/util/HashSet;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lcom/android/incallui/view/IndexSortedView;->hideBelowDividerSet:Ljava/util/HashSet;

    return-void
.end method

.method private refreshDividerVisibility()V
    .locals 5

    const/4 v2, 0x1

    const/4 v0, -0x1

    const/4 v1, 0x0

    :goto_0
    iget v4, p0, Lcom/android/incallui/view/IndexSortedView;->mTotalViewsCount:I

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/android/incallui/view/IndexSortedView;->dividerMap:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_0

    invoke-direct {p0, v2, v0, v1, v3}, Lcom/android/incallui/view/IndexSortedView;->controlDividerToShow(ZIILandroid/view/View;)V

    const/4 v2, 0x0

    move v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public addIndexView(ILandroid/view/View;)V
    .locals 6

    if-gez p1, :cond_0

    new-instance v4, Ljava/lang/RuntimeException;

    const-string/jumbo v5, "index must be > 0 !"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/view/IndexSortedView;->viewMap:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    return-void

    :cond_1
    const/4 v2, 0x0

    add-int/lit8 v1, p1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    iget-object v4, p0, Lcom/android/incallui/view/IndexSortedView;->viewMap:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_4

    invoke-virtual {p0, v3}, Lcom/android/incallui/view/IndexSortedView;->indexOfChild(Landroid/view/View;)I

    move-result v4

    add-int/lit8 v2, v4, 0x1

    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/incallui/view/IndexSortedView;->getDividerView(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v0, v2}, Lcom/android/incallui/view/IndexSortedView;->addView(Landroid/view/View;I)V

    iget-object v4, p0, Lcom/android/incallui/view/IndexSortedView;->dividerMap:Landroid/util/SparseArray;

    invoke-virtual {v4, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    :cond_3
    invoke-virtual {p0, p2, v2}, Lcom/android/incallui/view/IndexSortedView;->addView(Landroid/view/View;I)V

    iget-object v4, p0, Lcom/android/incallui/view/IndexSortedView;->viewMap:Landroid/util/SparseArray;

    invoke-virtual {v4, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/incallui/view/IndexSortedView;->refreshDividerVisibility()V

    return-void

    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public clearAllView()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/view/IndexSortedView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/view/IndexSortedView;->viewMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/android/incallui/view/IndexSortedView;->dividerMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    invoke-virtual {p0}, Lcom/android/incallui/view/IndexSortedView;->removeAllViewsInLayout()V

    return-void
.end method

.method public varargs configIndexBackNoDivider([I)V
    .locals 5

    const/4 v1, 0x0

    array-length v2, p1

    :goto_0
    if-ge v1, v2, :cond_0

    aget v0, p1, v1

    iget-object v3, p0, Lcom/android/incallui/view/IndexSortedView;->hideBelowDividerSet:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/view/IndexSortedView;->refreshDividerVisibility()V

    return-void
.end method

.method public varargs configIndexFrontNoDivider([I)V
    .locals 5

    const/4 v1, 0x0

    array-length v2, p1

    :goto_0
    if-ge v1, v2, :cond_0

    aget v0, p1, v1

    iget-object v3, p0, Lcom/android/incallui/view/IndexSortedView;->hideSelfDividerSet:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/incallui/view/IndexSortedView;->refreshDividerVisibility()V

    return-void
.end method

.method public getColor(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/view/IndexSortedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method protected abstract getDividerView(I)Landroid/view/View;
.end method

.method public getSize(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/view/IndexSortedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected abstract getTotalViewCount()I
.end method

.method public removeViewByIndex(I)V
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/view/IndexSortedView;->viewMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v2, p0, Lcom/android/incallui/view/IndexSortedView;->dividerMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/incallui/view/IndexSortedView;->removeViewInLayout(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/incallui/view/IndexSortedView;->viewMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/incallui/view/IndexSortedView;->removeViewInLayout(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/incallui/view/IndexSortedView;->dividerMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/android/incallui/view/IndexSortedView;->requestLayout()V

    invoke-direct {p0}, Lcom/android/incallui/view/IndexSortedView;->refreshDividerVisibility()V

    return-void
.end method
