.class public Lcom/android/incallui/view/VoLTEVideoIncomingLayout$TextureVideoViewOutlineProvider;
.super Landroid/view/ViewOutlineProvider;
.source "VoLTEVideoIncomingLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/view/VoLTEVideoIncomingLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TextureVideoViewOutlineProvider"
.end annotation


# instance fields
.field private mRadius:F

.field final synthetic this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;


# direct methods
.method public constructor <init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;F)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$TextureVideoViewOutlineProvider;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-direct {p0}, Landroid/view/ViewOutlineProvider;-><init>()V

    iput p2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$TextureVideoViewOutlineProvider;->mRadius:F

    return-void
.end method


# virtual methods
.method public getOutline(Landroid/view/View;Landroid/graphics/Outline;)V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    invoke-direct {v1, v5, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iget v2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$TextureVideoViewOutlineProvider;->mRadius:F

    invoke-virtual {p2, v1, v2}, Landroid/graphics/Outline;->setRoundRect(Landroid/graphics/Rect;F)V

    return-void
.end method
