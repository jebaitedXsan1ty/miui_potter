.class public Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
.super Ljava/lang/Object;
.source "AnimationCircleLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/view/AnimationCircleLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CircleAnimationBuilder"
.end annotation


# instance fields
.field private duration:J

.field private listener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

.field private needTranslateY:Z

.field private originR:F

.field private originX:F

.field private originY:F

.field private targetR:F

.field private targetX:F

.field private targetY:F

.field final synthetic this$0:Lcom/android/incallui/view/AnimationCircleLayout;


# direct methods
.method public constructor <init>(Lcom/android/incallui/view/AnimationCircleLayout;)V
    .locals 6

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originX:F

    iput v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originY:F

    iput v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originR:F

    iput v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->targetX:F

    iput v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->targetY:F

    iput v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->targetR:F

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->needTranslateY:Z

    invoke-virtual {p1}, Lcom/android/incallui/view/AnimationCircleLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p1}, Lcom/android/incallui/view/AnimationCircleLayout;->getMeasuredHeight()I

    move-result v0

    int-to-float v2, v1

    div-float/2addr v2, v3

    iput v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originX:F

    int-to-float v2, v0

    div-float/2addr v2, v3

    iput v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originY:F

    mul-int v2, v0, v0

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originR:F

    const-wide/16 v2, 0x12c

    iput-wide v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->duration:J

    return-void
.end method


# virtual methods
.method public configAnimationParam()V
    .locals 7

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    iget v1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originX:F

    iget v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originY:F

    iget v3, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originR:F

    iget v4, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->targetX:F

    iget v5, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->targetY:F

    iget v6, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->targetR:F

    invoke-static/range {v0 .. v6}, Lcom/android/incallui/view/AnimationCircleLayout;->-wrap0(Lcom/android/incallui/view/AnimationCircleLayout;FFFFFF)V

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    iget-wide v2, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->duration:J

    invoke-static {v0, v2, v3}, Lcom/android/incallui/view/AnimationCircleLayout;->-set3(Lcom/android/incallui/view/AnimationCircleLayout;J)J

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    iget-boolean v1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->needTranslateY:Z

    invoke-static {v0, v1}, Lcom/android/incallui/view/AnimationCircleLayout;->-set4(Lcom/android/incallui/view/AnimationCircleLayout;Z)Z

    iget-object v0, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->this$0:Lcom/android/incallui/view/AnimationCircleLayout;

    iget-object v1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->listener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    invoke-static {v0, v1}, Lcom/android/incallui/view/AnimationCircleLayout;->-set5(Lcom/android/incallui/view/AnimationCircleLayout;Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;)Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    return-void
.end method

.method public setCircleChangeDuration(I)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 2

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->duration:J

    return-object p0
.end method

.method public setNoNeedTranslateY()Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->needTranslateY:Z

    return-object p0
.end method

.method public setOnAnimationListener(Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->listener:Lcom/android/incallui/view/AnimationCircleLayout$OnAnimationListener;

    return-object p0
.end method

.method public setOriginR(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originR:F

    return-object p0
.end method

.method public setOriginX(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originX:F

    return-object p0
.end method

.method public setOriginY(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->originY:F

    return-object p0
.end method

.method public setTargetR(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->targetR:F

    return-object p0
.end method

.method public setTargetX(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->targetX:F

    return-object p0
.end method

.method public setTargetY(F)Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;
    .locals 0

    iput p1, p0, Lcom/android/incallui/view/AnimationCircleLayout$CircleAnimationBuilder;->targetY:F

    return-object p0
.end method
