.class Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;
.super Ljava/lang/Object;
.source "VoLTEVideoIncomingLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->initIncomingPosition(ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

.field final synthetic val$number:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iput-object p2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->val$number:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap4(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iget-object v2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get2(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap2(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Z)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap3(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get6(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/view/TextureView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/TextureView;->setClipToOutline(Z)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get6(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/view/TextureView;

    move-result-object v1

    new-instance v2, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$TextureVideoViewOutlineProvider;

    iget-object v3, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    const/high16 v4, 0x41c00000    # 24.0f

    invoke-direct {v2, v3, v4}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$TextureVideoViewOutlineProvider;-><init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;F)V

    invoke-virtual {v1, v2}, Landroid/view/TextureView;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get6(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/view/TextureView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get4(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get3(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get6(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/view/TextureView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iget-object v2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get4(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)F

    move-result v2

    invoke-static {v1, v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-set3(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;F)F

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iget-object v2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get3(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)F

    move-result v2

    invoke-static {v1, v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-set2(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;F)F

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get6(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/view/TextureView;

    move-result-object v1

    new-instance v2, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2$1;

    invoke-direct {v2, p0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2$1;-><init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;)V

    invoke-virtual {v1, v2}, Landroid/view/TextureView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get6(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/view/TextureView;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/TextureView;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iget-object v2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$2;->val$number:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-wrap1(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Ljava/lang/String;)V

    return-void
.end method
