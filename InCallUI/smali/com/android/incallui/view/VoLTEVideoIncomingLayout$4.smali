.class Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;
.super Lcom/android/incallui/util/SimpleTask;
.source "VoLTEVideoIncomingLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->setVideoCrbtTitle(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/util/SimpleTask",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

.field final synthetic val$number:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    iput-object p2, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->val$number:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/incallui/util/SimpleTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->doInBackground()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get0(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->val$number:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/incallui/util/Utils;->getOperator(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get5(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get5(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-virtual {v1}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo p1, ""

    :cond_0
    aput-object p1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/view/VoLTEVideoIncomingLayout$4;->this$0:Lcom/android/incallui/view/VoLTEVideoIncomingLayout;

    invoke-static {v0}, Lcom/android/incallui/view/VoLTEVideoIncomingLayout;->-get5(Lcom/android/incallui/view/VoLTEVideoIncomingLayout;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    return-void
.end method
