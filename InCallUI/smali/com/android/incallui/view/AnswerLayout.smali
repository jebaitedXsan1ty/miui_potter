.class public abstract Lcom/android/incallui/view/AnswerLayout;
.super Landroid/widget/FrameLayout;
.source "AnswerLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/view/AnswerLayout$SlideEndListener;
    }
.end annotation


# instance fields
.field public mSlideEndListener:Lcom/android/incallui/view/AnswerLayout$SlideEndListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public clearAll()V
    .locals 0

    return-void
.end method

.method public configureMessageDialog(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public resetAnswerUi()V
    .locals 0

    return-void
.end method

.method public setSlideEndListener(Lcom/android/incallui/view/AnswerLayout$SlideEndListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/view/AnswerLayout;->mSlideEndListener:Lcom/android/incallui/view/AnswerLayout$SlideEndListener;

    return-void
.end method

.method public setVideoCall(Z)V
    .locals 0

    return-void
.end method

.method public showAnswerUi(Z)V
    .locals 0

    return-void
.end method

.method public showTextButton(Z)V
    .locals 0

    return-void
.end method
