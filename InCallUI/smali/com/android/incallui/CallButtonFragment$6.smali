.class Lcom/android/incallui/CallButtonFragment$6;
.super Ljava/lang/Object;
.source "CallButtonFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/CallButtonFragment;->showAudioSelectDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/CallButtonFragment;


# direct methods
.method constructor <init>(Lcom/android/incallui/CallButtonFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonFragment;->cancelDialog()V

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0, v1}, Lcom/android/incallui/CallButtonFragment;->-wrap0(Lcom/android/incallui/CallButtonFragment;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0, v1}, Lcom/android/incallui/CallButtonPresenter;->setAudioMode(I)V

    const-string/jumbo v0, "voice_call"

    const-string/jumbo v1, "voice_call_audio_mode_bluetooth"

    invoke-static {v0, v1}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p2, v2, :cond_3

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0, v3}, Lcom/android/incallui/CallButtonFragment;->-wrap0(Lcom/android/incallui/CallButtonFragment;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0, v2}, Lcom/android/incallui/CallButtonFragment;->-wrap0(Lcom/android/incallui/CallButtonFragment;I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/incallui/CallButtonPresenter;->setAudioMode(I)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0, v3}, Lcom/android/incallui/CallButtonFragment;->-wrap1(Lcom/android/incallui/CallButtonFragment;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "voice_call"

    const-string/jumbo v1, "voice_call_audio_mode_headset"

    invoke-static {v0, v1}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "voice_call"

    const-string/jumbo v1, "voice_call_audio_mode_handset"

    invoke-static {v0, v1}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    if-ne p2, v1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/android/incallui/CallButtonFragment;->-wrap0(Lcom/android/incallui/CallButtonFragment;I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$6;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonPresenter;->onSpeakerClick()V

    const-string/jumbo v0, "voice_call"

    const-string/jumbo v1, "voice_call_audio_mode_speaker"

    invoke-static {v0, v1}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
