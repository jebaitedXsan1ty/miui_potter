.class public Lcom/android/incallui/InCallUiStateNotifier;
.super Ljava/lang/Object;
.source "InCallUiStateNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;
    }
.end annotation


# static fields
.field private static sInCallUiStateNotifier:Lcom/android/incallui/InCallUiStateNotifier;


# instance fields
.field private mInCallUiStateNotifierListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;",
            ">;"
        }
    .end annotation
.end field

.field private mIsInBackground:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/InCallUiStateNotifier;->mInCallUiStateNotifierListeners:Ljava/util/List;

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/android/incallui/InCallUiStateNotifier;
    .locals 2

    const-class v1, Lcom/android/incallui/InCallUiStateNotifier;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/incallui/InCallUiStateNotifier;->sInCallUiStateNotifier:Lcom/android/incallui/InCallUiStateNotifier;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/incallui/InCallUiStateNotifier;

    invoke-direct {v0}, Lcom/android/incallui/InCallUiStateNotifier;-><init>()V

    sput-object v0, Lcom/android/incallui/InCallUiStateNotifier;->sInCallUiStateNotifier:Lcom/android/incallui/InCallUiStateNotifier;

    :cond_0
    sget-object v0, Lcom/android/incallui/InCallUiStateNotifier;->sInCallUiStateNotifier:Lcom/android/incallui/InCallUiStateNotifier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isUiShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/InCallUiStateNotifier;->mIsInBackground:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private notifyOnUiShowing(Z)V
    .locals 3

    iget-object v2, p0, Lcom/android/incallui/InCallUiStateNotifier;->mInCallUiStateNotifierListeners:Ljava/util/List;

    invoke-static {v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/incallui/InCallUiStateNotifier;->mInCallUiStateNotifierListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;

    invoke-interface {v0, p1}, Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;->onUiShowing(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;)V
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/incallui/InCallUiStateNotifier;->mInCallUiStateNotifierListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addListener(Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;Z)V
    .locals 1

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/android/incallui/InCallUiStateNotifier;->isUiShowing()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;->onUiShowing(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/InCallUiStateNotifier;->mInCallUiStateNotifierListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onUiShowing(Z)V
    .locals 4

    invoke-direct {p0}, Lcom/android/incallui/InCallUiStateNotifier;->isUiShowing()Z

    move-result v1

    xor-int/lit8 v2, p1, 0x1

    iput-boolean v2, p0, Lcom/android/incallui/InCallUiStateNotifier;->mIsInBackground:Z

    invoke-direct {p0}, Lcom/android/incallui/InCallUiStateNotifier;->isUiShowing()Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onUiShowing wasShowing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " isShowing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    if-eq v1, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/incallui/InCallUiStateNotifier;->notifyOnUiShowing(Z)V

    :cond_0
    return-void
.end method

.method public removeListener(Lcom/android/incallui/InCallUiStateNotifier$InCallUiStateNotifierListener;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/InCallUiStateNotifier;->mInCallUiStateNotifierListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "Can\'t remove null listener"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public tearDown()V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/InCallUiStateNotifier;->mInCallUiStateNotifierListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
