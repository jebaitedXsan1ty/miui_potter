.class public Lcom/android/incallui/CallToolsFragment;
.super Lcom/android/incallui/BaseFragment;
.source "CallToolsFragment.java"

# interfaces
.implements Lcom/android/incallui/CallToolsPresenter$CallToolsUi;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/CallToolsFragment$1;,
        Lcom/android/incallui/CallToolsFragment$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseFragment",
        "<",
        "Lcom/android/incallui/CallToolsPresenter;",
        "Lcom/android/incallui/CallToolsPresenter$CallToolsUi;",
        ">;",
        "Lcom/android/incallui/CallToolsPresenter$CallToolsUi;"
    }
.end annotation


# instance fields
.field private isInternationalVersion:Z

.field private isNeedAnim:Z

.field private isNeedLaunchShow:Z

.field private mAddCallButton:Landroid/widget/Button;

.field private mBottomPanel:Landroid/widget/LinearLayout;

.field private mBottomPanelMarginBottom:F

.field private mCallRecordTimer:Landroid/widget/Chronometer;

.field mCallRecorderToolsListener:Lcom/android/incallui/recorder/CallRecorderToolsListener;

.field private mContactButton:Landroid/widget/Button;

.field private mHideAnimatorSet:Landroid/animation/AnimatorSet;

.field private mHoldButton:Landroid/widget/ToggleButton;

.field private mInCallToolPanel:Landroid/view/View;

.field private mInCallToolPanelContent:Landroid/view/View;

.field private mManualClickAutoRecord:Z

.field private mMergeButton:Landroid/widget/Button;

.field private mMessageButton:Landroid/widget/Button;

.field private mMuteButton:Landroid/widget/ToggleButton;

.field private mNotesButton:Landroid/widget/Button;

.field private mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

.field private mPauseVideoButton:Landroid/widget/ToggleButton;

.field private mRecordStartButton:Landroid/widget/ToggleButton;

.field private mRecordStartTxt:Landroid/widget/TextView;

.field private mRotationAnimatorSet:Landroid/animation/AnimatorSet;

.field private mShowAnimatorSet:Landroid/animation/AnimatorSet;

.field private mSwapButton:Landroid/widget/Button;

.field private mSwitchCameraButton:Landroid/widget/Button;

.field private mSwitchToVideoButton:Landroid/widget/Button;

.field private mSwitchToVoiceButton:Landroid/widget/Button;

.field private mToolpadVisible:Z

.field private mTopPanel:Landroid/widget/LinearLayout;

.field private mTopPanelMarginBottom:F

.field private mVideoBottomPanelMarginBottom:F

.field private mVideoConfBottomPanelMarginBottom:F

.field private mVideoTopPanelMarginBottom:F


# direct methods
.method static synthetic -get0(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mHoldButton:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mMuteButton:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/ToggleButton;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartButton:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/incallui/CallToolsFragment;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchCameraButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/incallui/CallToolsFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/CallToolsFragment;->mManualClickAutoRecord:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/CallToolsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/CallToolsFragment;->checkPermissionOrPerformRecord()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/incallui/CallToolsFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/CallToolsFragment;->onRecordButtonClick(Z)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/incallui/CallToolsFragment;ZZ[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/incallui/CallToolsFragment;->videoCallStateRecord(ZZ[Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/android/incallui/BaseFragment;-><init>()V

    iput-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->mToolpadVisible:Z

    iput-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->mManualClickAutoRecord:Z

    iput-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->isNeedLaunchShow:Z

    iput-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->isNeedAnim:Z

    invoke-static {}, Lcom/android/incallui/util/Utils;->isInternationalBuild()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->isInternationalVersion:Z

    new-instance v0, Lcom/android/incallui/CallToolsFragment$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/CallToolsFragment$1;-><init>(Lcom/android/incallui/CallToolsFragment;)V

    iput-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecorderToolsListener:Lcom/android/incallui/recorder/CallRecorderToolsListener;

    new-instance v0, Lcom/android/incallui/CallToolsFragment$2;

    invoke-direct {v0, p0}, Lcom/android/incallui/CallToolsFragment$2;-><init>(Lcom/android/incallui/CallToolsFragment;)V

    iput-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    return-void
.end method

.method private checkPermissionOrPerformRecord()V
    .locals 6

    invoke-static {}, Lcom/android/incallui/util/PermissionGrantHelper;->getInstance()Lcom/android/incallui/util/PermissionGrantHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/android/incallui/CallToolsFragment$3;

    invoke-direct {v2, p0}, Lcom/android/incallui/CallToolsFragment$3;-><init>(Lcom/android/incallui/CallToolsFragment;)V

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "android.permission.WRITE_EXTERNAL_STORAGE"

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string/jumbo v4, "android.permission.READ_EXTERNAL_STORAGE"

    const/4 v5, 0x1

    aput-object v4, v3, v5

    const-string/jumbo v4, "android.permission.RECORD_AUDIO"

    const/4 v5, 0x2

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/util/PermissionGrantHelper;->checkPermissions(Landroid/app/Activity;Lcom/android/incallui/util/PermissionGrantHelper$OnPermissionGrantedListener;[Ljava/lang/String;)V

    return-void
.end method

.method private clearAnimatorSet()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    iput-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    iput-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    :cond_1
    return-void
.end method

.method private onRecordButtonClick(Z)V
    .locals 6

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v1

    const v4, 0x7f0b002b

    invoke-static {v1, v4}, Lcom/android/incallui/util/Utils;->checkUserUnlocked(Landroid/content/Context;I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartButton:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    iput-boolean v3, p0, Lcom/android/incallui/CallToolsFragment;->mManualClickAutoRecord:Z

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->isSipCall()Z

    move-result v0

    if-eqz v0, :cond_1

    const v1, 0x7f0b002c

    invoke-static {v1}, Lcom/android/incallui/util/Utils;->displayMsg(I)V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartButton:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    iput-boolean v3, p0, Lcom/android/incallui/CallToolsFragment;->mManualClickAutoRecord:Z

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecordTimer:Landroid/widget/Chronometer;

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0032

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Chronometer;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartTxt:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecordTimer:Landroid/widget/Chronometer;

    if-nez p1, :cond_3

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/Chronometer;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartButton:Landroid/widget/ToggleButton;

    invoke-virtual {v1, p1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->getInstance()Lcom/android/incallui/recorder/CallRecorderTools;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/incallui/recorder/CallRecorderTools;->onRecordClick(Z)V

    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method private varargs videoCallStateRecord(ZZ[Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsPresenter;->isVtConferenceCall()Z

    move-result v1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsPresenter;->isVideoCall()Z

    move-result v0

    invoke-static {p1, p2, v1, v0, p3}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordVideoCallCountEvent(ZZZZ[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public changeCallToolMode()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/incallui/CallToolsFragment;->clearAnimatorSet()V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iput-object v2, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "changeCallToolMode to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsPresenter;->getCallToolMode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->initCallToolsView()V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->mToolpadVisible:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->hideToolPanel()V

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->showToolPanel()V

    :cond_2
    return-void
.end method

.method protected createPresenter()Lcom/android/incallui/CallToolsPresenter;
    .locals 1

    new-instance v0, Lcom/android/incallui/CallToolsPresenter;

    invoke-direct {v0}, Lcom/android/incallui/CallToolsPresenter;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic createPresenter()Lcom/android/incallui/Presenter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->createPresenter()Lcom/android/incallui/CallToolsPresenter;

    move-result-object v0

    return-object v0
.end method

.method public enableAddCall(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mAddCallButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public enableHold(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mHoldButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p1}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    return-void
.end method

.method public enableMerge(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mMergeButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public enableMute(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mMuteButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p1}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    return-void
.end method

.method public enablePauseVideo(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p1}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    return-void
.end method

.method public enableSwitchCamera(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchCameraButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public enableSwitchToVideoButton(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchToVideoButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public enableSwitchToVoiceButton(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchToVoiceButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchToVoiceButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public getToolPanelHideTime()I
    .locals 1

    const/16 v0, 0x64

    return v0
.end method

.method protected getUi()Lcom/android/incallui/CallToolsPresenter$CallToolsUi;
    .locals 0

    return-object p0
.end method

.method protected bridge synthetic getUi()Lcom/android/incallui/Ui;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getUi()Lcom/android/incallui/CallToolsPresenter$CallToolsUi;

    move-result-object v0

    return-object v0
.end method

.method public getVideoBottomPanelHeight()F
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mBottomPanel:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mBottomPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    int-to-float v0, v1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->getCallToolMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget v1, p0, Lcom/android/incallui/CallToolsFragment;->mBottomPanelMarginBottom:F

    add-float/2addr v0, v1

    goto :goto_0

    :pswitch_1
    iget v1, p0, Lcom/android/incallui/CallToolsFragment;->mVideoBottomPanelMarginBottom:F

    add-float/2addr v0, v1

    goto :goto_0

    :pswitch_2
    iget v1, p0, Lcom/android/incallui/CallToolsFragment;->mVideoConfBottomPanelMarginBottom:F

    add-float/2addr v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getVideoTopPanelHeight()F
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mTopPanel:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mTopPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    int-to-float v0, v1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/CallToolsPresenter;->getCallToolMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget v1, p0, Lcom/android/incallui/CallToolsFragment;->mTopPanelMarginBottom:F

    add-float/2addr v0, v1

    goto :goto_0

    :pswitch_1
    iget v1, p0, Lcom/android/incallui/CallToolsFragment;->mVideoTopPanelMarginBottom:F

    add-float/2addr v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public hideToolPanel()V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-boolean v3, p0, Lcom/android/incallui/CallToolsFragment;->mToolpadVisible:Z

    if-nez v3, :cond_0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    if-nez v3, :cond_1

    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x64

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v4, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v4}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/InCallApp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900a3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v9, [F

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v5, v7

    aput v10, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v9, [F

    aput v10, v5, v7

    int-to-float v6, v2

    aput v6, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    new-array v4, v9, [Landroid/animation/Animator;

    aput-object v0, v4, v7

    aput-object v1, v4, v8

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v4, Lcom/android/incallui/CallToolsFragment$5;

    invoke-direct {v4, p0}, Lcom/android/incallui/CallToolsFragment$5;-><init>(Lcom/android/incallui/CallToolsFragment;)V

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_1
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_2
    iput-boolean v7, p0, Lcom/android/incallui/CallToolsFragment;->mToolpadVisible:Z

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public initCallToolsView()V
    .locals 9

    const v8, 0x7f0a0048

    const v7, 0x7f09002e

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    if-eqz v3, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v3}, Lcom/android/incallui/CallToolsPresenter;->getCallToolMode()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "initCallToolsView mode="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, ", mInCallToolPanelContent"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanel:Landroid/view/View;

    const v6, 0x7f0a0044

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    :goto_0
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a0050

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mAddCallButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mAddCallButton:Landroid/widget/Button;

    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a0049

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ToggleButton;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHoldButton:Landroid/widget/ToggleButton;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHoldButton:Landroid/widget/ToggleButton;

    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v6}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a0051

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mMergeButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mMergeButton:Landroid/widget/Button;

    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a004a

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mSwapButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mSwapButton:Landroid/widget/Button;

    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a0047

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mTopPanel:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a004d

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mBottomPanel:Landroid/widget/LinearLayout;

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v3}, Lcom/android/incallui/CallToolsPresenter;->initVtView()V

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v3}, Lcom/android/incallui/CallToolsPresenter;->initCallTool()V

    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->getInstance()Lcom/android/incallui/recorder/CallRecorderTools;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/recorder/CallRecorderTools;->checkRecordTime()V

    return-void

    :cond_2
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanel:Landroid/view/View;

    const v6, 0x7f0a0045

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanel:Landroid/view/View;

    const v6, 0x7f0a0042

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanel:Landroid/view/View;

    const v6, 0x7f0a0043

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    goto/16 :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f090030

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/CallToolsFragment;->mTopPanelMarginBottom:F

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f09002d

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/CallToolsFragment;->mBottomPanelMarginBottom:F

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a004b

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mNotesButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mNotesButton:Landroid/widget/Button;

    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a004e

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mContactButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mContactButton:Landroid/widget/Button;

    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mContactButton:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a004f

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchToVideoButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchToVideoButton:Landroid/widget/Button;

    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a0052

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ToggleButton;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartButton:Landroid/widget/ToggleButton;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartButton:Landroid/widget/ToggleButton;

    invoke-virtual {v3, v4}, Landroid/widget/ToggleButton;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartButton:Landroid/widget/ToggleButton;

    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v6}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v6, 0x7f0a0053

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartTxt:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartTxt:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v4, 0x7f0a0054

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Chronometer;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecordTimer:Landroid/widget/Chronometer;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecordTimer:Landroid/widget/Chronometer;

    invoke-virtual {v3, v5}, Landroid/widget/Chronometer;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v4, 0x7f0a004c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mMessageButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mMessageButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ToggleButton;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mMuteButton:Landroid/widget/ToggleButton;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mMuteButton:Landroid/widget/ToggleButton;

    iget-object v4, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    :pswitch_1
    iget-object v6, p0, Lcom/android/incallui/CallToolsFragment;->mTopPanel:Landroid/widget/LinearLayout;

    iget-boolean v3, p0, Lcom/android/incallui/CallToolsFragment;->isInternationalVersion:Z

    if-eqz v3, :cond_5

    move v3, v4

    :goto_2
    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f090031

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/CallToolsFragment;->mVideoTopPanelMarginBottom:F

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/CallToolsFragment;->mVideoBottomPanelMarginBottom:F

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/android/incallui/CallToolsFragment;->mVideoConfBottomPanelMarginBottom:F

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v5, 0x7f0a00bd

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchToVoiceButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchToVoiceButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v5, 0x7f0a00be

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ToggleButton;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    iget-object v5, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v5}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v3, v4}, Landroid/widget/ToggleButton;->setChecked(Z)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v5, 0x7f0a00bf

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchCameraButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchCameraButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v3, p0, Lcom/android/incallui/CallToolsFragment;->isInternationalVersion:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ToggleButton;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mMuteButton:Landroid/widget/ToggleButton;

    :goto_3
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mMuteButton:Landroid/widget/ToggleButton;

    iget-object v4, p0, Lcom/android/incallui/CallToolsFragment;->mOnEffectiveClickListener:Lcom/android/incallui/view/OnEffectiveClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    :cond_5
    move v3, v5

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v5, 0x7f0a00c1

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ToggleButton;

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mMuteButton:Landroid/widget/ToggleButton;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const v5, 0x7f0a00c0

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public isInCallToolPanelContentInit()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPauseVideoChecked()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isToolPanelVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->mToolpadVisible:Z

    return v0
.end method

.method public onCallRecordingStarted(J)V
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartButton:Landroid/widget/ToggleButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    iput-boolean v2, p0, Lcom/android/incallui/CallToolsFragment;->mManualClickAutoRecord:Z

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartTxt:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecordTimer:Landroid/widget/Chronometer;

    invoke-virtual {v0, v2}, Landroid/widget/Chronometer;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecordTimer:Landroid/widget/Chronometer;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    const-wide/16 v4, 0x1f4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/widget/Chronometer;->setBase(J)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecordTimer:Landroid/widget/Chronometer;

    invoke-virtual {v0}, Landroid/widget/Chronometer;->start()V

    return-void
.end method

.method public onCallRecordingStoped()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mRecordStartTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecordTimer:Landroid/widget/Chronometer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Chronometer;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecordTimer:Landroid/widget/Chronometer;

    invoke-virtual {v0}, Landroid/widget/Chronometer;->stop()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f03000a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanel:Landroid/view/View;

    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->getInstance()Lcom/android/incallui/recorder/CallRecorderTools;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecorderToolsListener:Lcom/android/incallui/recorder/CallRecorderToolsListener;

    invoke-virtual {v0, v1}, Lcom/android/incallui/recorder/CallRecorderTools;->addCallRecorderToolsListener(Lcom/android/incallui/recorder/CallRecorderToolsListener;)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanel:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onDestroyView()V

    invoke-direct {p0}, Lcom/android/incallui/CallToolsFragment;->clearAnimatorSet()V

    invoke-static {}, Lcom/android/incallui/recorder/CallRecorderTools;->getInstance()Lcom/android/incallui/recorder/CallRecorderTools;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mCallRecorderToolsListener:Lcom/android/incallui/recorder/CallRecorderToolsListener;

    invoke-virtual {v0, v1}, Lcom/android/incallui/recorder/CallRecorderTools;->removeCallRecorderToolsListener(Lcom/android/incallui/recorder/CallRecorderToolsListener;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onResume()V

    iget-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->isNeedLaunchShow:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->isNeedLaunchShow:Z

    iget-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->isNeedAnim:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->showToolPanel()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/incallui/CallToolsFragment;->setVisible(Z)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mMergeButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwapButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsPresenter;->isVideoCall()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchCameraButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchCameraButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setHold(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mHoldButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    return-void
.end method

.method public setMute(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->isInCallToolPanelContentInit()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mMuteButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    return-void
.end method

.method public setNeedLaunchShow(Z)V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->showToolPanel()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/incallui/CallToolsFragment;->setVisible(Z)V

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/android/incallui/CallToolsFragment;->isNeedLaunchShow:Z

    iput-boolean p1, p0, Lcom/android/incallui/CallToolsFragment;->isNeedAnim:Z

    goto :goto_0
.end method

.method public setPauseVideoButton(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public setSwitchCameraButton(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchCameraButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchCameraButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setSelected(Z)V

    :cond_0
    return-void
.end method

.method public setVisible(Z)V
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, "CTF.sV"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->mToolpadVisible:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->isNeedLaunchShow:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->mToolpadVisible:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setVisible true, mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallToolsPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallToolsPresenter;->getCallToolMode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->i(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->initCallToolsView()V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanel:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/android/incallui/CallToolsFragment;->mToolpadVisible:Z

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanel:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanel:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public showAddCall(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mAddCallButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showContactButton(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mContactButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showHold(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mHoldButton:Landroid/widget/ToggleButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ToggleButton;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showMerge(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mMergeButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showMessageButton(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mMessageButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showNotesButton(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mNotesButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showSwap(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mSwapButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showSwitchToVideoButton(Z)V
    .locals 2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchToVideoButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showToolPanel()V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-boolean v3, p0, Lcom/android/incallui/CallToolsFragment;->mToolpadVisible:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/android/incallui/CallToolsFragment;->isNeedLaunchShow:Z

    if-eqz v3, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/android/incallui/CallToolsFragment;->initCallToolsView()V

    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v4, 0x15e

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v4, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v4}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/InCallApp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900a3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v9, [F

    aput v10, v5, v7

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mInCallToolPanelContent:Landroid/view/View;

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v9, [F

    int-to-float v6, v2

    aput v6, v5, v7

    aput v10, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    new-array v4, v9, [Landroid/animation/Animator;

    aput-object v0, v4, v7

    aput-object v1, v4, v8

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v4, Lcom/android/incallui/CallToolsFragment$4;

    invoke-direct {v4, p0}, Lcom/android/incallui/CallToolsFragment$4;-><init>(Lcom/android/incallui/CallToolsFragment;)V

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_2
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mHideAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_3
    iget-object v3, p0, Lcom/android/incallui/CallToolsFragment;->mShowAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public startRotationAnimator(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    const/16 v0, 0x8

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mMuteButton:Landroid/widget/ToggleButton;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchCameraButton:Landroid/widget/Button;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mSwitchToVoiceButton:Landroid/widget/Button;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mPauseVideoButton:Landroid/widget/ToggleButton;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mAddCallButton:Landroid/widget/Button;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mHoldButton:Landroid/widget/ToggleButton;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mMergeButton:Landroid/widget/Button;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/incallui/CallToolsFragment;->mSwapButton:Landroid/widget/Button;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const/16 v1, 0x12c

    invoke-static {p1, v1, v0}, Lcom/android/incallui/AnimationUtils;->startRotationAnimator(II[Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/CallToolsFragment;->mRotationAnimatorSet:Landroid/animation/AnimatorSet;

    return-void
.end method

.method public stopManualAutoRecordIfNotStarted()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/incallui/CallToolsFragment;->mManualClickAutoRecord:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/android/incallui/CallToolsFragment;->mManualClickAutoRecord:Z

    invoke-direct {p0, v1}, Lcom/android/incallui/CallToolsFragment;->onRecordButtonClick(Z)V

    :cond_0
    return-void
.end method
