.class Lcom/android/incallui/InCallApp$SuppServFailedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "InCallApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/InCallApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuppServFailedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/InCallApp;


# direct methods
.method private constructor <init>(Lcom/android/incallui/InCallApp;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/InCallApp$SuppServFailedReceiver;->this$0:Lcom/android/incallui/InCallApp;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/incallui/InCallApp;Lcom/android/incallui/InCallApp$SuppServFailedReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/InCallApp$SuppServFailedReceiver;-><init>(Lcom/android/incallui/InCallApp;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, -0x1

    if-eqz p2, :cond_1

    const-string/jumbo v1, "org.codeaurora.ACTION_SUPP_SERVICE_FAILURE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "supp_serv_failure"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_0

    const-string/jumbo v1, "SuppServFailedReceiver: Not support type of SuppService."

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/incallui/InCallPresenter;->onSuppServiceFailed(I)V

    invoke-static {}, Lcom/android/incallui/util/TimeOutUtil;->getInstance()Lcom/android/incallui/util/TimeOutUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/incallui/util/TimeOutUtil;->onSuppServiceFailed(I)V

    :cond_1
    return-void
.end method
