.class Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;
.super Ljava/lang/Object;
.source "ContactInfoCache.java"

# interfaces
.implements Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/ContactInfoCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FindYellowPageInfoCallBack"
.end annotation


# instance fields
.field private final mCall:Lcom/android/incallui/Call;

.field private final mIsIncoming:Z

.field final synthetic this$0:Lcom/android/incallui/ContactInfoCache;


# direct methods
.method public constructor <init>(Lcom/android/incallui/ContactInfoCache;Lcom/android/incallui/Call;ZLjava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/incallui/Call;",
            "Z",
            "Ljava/util/Set",
            "<",
            "Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->this$0:Lcom/android/incallui/ContactInfoCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->mCall:Lcom/android/incallui/Call;

    iput-boolean p3, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->mIsIncoming:Z

    invoke-static {p1, p4}, Lcom/android/incallui/ContactInfoCache;->-set0(Lcom/android/incallui/ContactInfoCache;Ljava/util/Set;)Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public onQueryComplete(ILjava/lang/Object;Lcom/android/incallui/CallerInfo;)V
    .locals 10

    const/4 v9, 0x0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->this$0:Lcom/android/incallui/ContactInfoCache;

    invoke-static {v0}, Lcom/android/incallui/ContactInfoCache;->-get3(Lcom/android/incallui/ContactInfoCache;)Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->-get0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "YellowPage: the callerInfo or the YellowPageCallbacks should not be null"

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p3, Lcom/android/incallui/CallerInfo;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->-get0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "YellowPage: the yellowPagePhone is null."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->-get0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "YellowPage: the yellowPagePhone is non-null."

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->-get0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "YellowPage: mYellowPageCallBack:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->this$0:Lcom/android/incallui/ContactInfoCache;

    invoke-static {v3}, Lcom/android/incallui/ContactInfoCache;->-get3(Lcom/android/incallui/ContactInfoCache;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->mCall:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getNumberPresentation()I

    move-result v4

    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->this$0:Lcom/android/incallui/ContactInfoCache;

    iget-object v1, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->this$0:Lcom/android/incallui/ContactInfoCache;

    invoke-static {v1}, Lcom/android/incallui/ContactInfoCache;->-get1(Lcom/android/incallui/ContactInfoCache;)Landroid/content/Context;

    move-result-object v1

    iget-boolean v5, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->mIsIncoming:Z

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/android/incallui/ContactInfoCache;->-wrap0(Lcom/android/incallui/ContactInfoCache;Landroid/content/Context;Ljava/lang/String;Lcom/android/incallui/CallerInfo;IZ)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v6

    packed-switch p1, :pswitch_data_0

    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->-get0()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Not supported query token"

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->this$0:Lcom/android/incallui/ContactInfoCache;

    invoke-static {v0}, Lcom/android/incallui/ContactInfoCache;->-get2(Lcom/android/incallui/ContactInfoCache;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->this$0:Lcom/android/incallui/ContactInfoCache;

    invoke-static {v0}, Lcom/android/incallui/ContactInfoCache;->-get3(Lcom/android/incallui/ContactInfoCache;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    invoke-interface {v7, v2, v6}, Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;->onContactInfoComplete(Ljava/lang/String;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)V

    goto :goto_1

    :pswitch_1
    iget-boolean v0, p3, Lcom/android/incallui/CallerInfo;->isCachedPhotoCurrent:Z

    if-eqz v0, :cond_3

    iget-object v0, v6, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/ContactInfoCache$FindYellowPageInfoCallBack;->this$0:Lcom/android/incallui/ContactInfoCache;

    iget-object v1, v6, Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;->photo:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v9, v2}, Lcom/android/incallui/ContactInfoCache;->onImageLoadComplete(ILandroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;Ljava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
