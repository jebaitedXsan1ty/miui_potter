.class public Lcom/android/incallui/smartcover/a1/A1CoverFragment;
.super Lcom/android/incallui/BaseFragment;
.source "A1CoverFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/incallui/smartcover/a1/A1CoverPresenter$CoverCardUi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseFragment",
        "<",
        "Lcom/android/incallui/smartcover/a1/A1CoverPresenter;",
        "Lcom/android/incallui/smartcover/a1/A1CoverPresenter$CoverCardUi;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/android/incallui/smartcover/a1/A1CoverPresenter$CoverCardUi;"
    }
.end annotation


# instance fields
.field private mCallInfoLayout:Landroid/widget/LinearLayout;

.field private mCity:Landroid/widget/TextView;

.field private mDivider1:Landroid/view/View;

.field private mDivider2:Landroid/view/View;

.field private mHungUp:Landroid/view/View;

.field private mIsIncoming:Z

.field private mName:Landroid/widget/TextView;

.field private mNumber:Landroid/widget/TextView;

.field private mSlideLayout:Lcom/android/incallui/view/HorizontalSlideLayout;

.field private mTime:Landroid/widget/TextView;

.field private mTransitionDrawable:Landroid/graphics/drawable/TransitionDrawable;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/smartcover/a1/A1CoverFragment;)Landroid/graphics/drawable/TransitionDrawable;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mTransitionDrawable:Landroid/graphics/drawable/TransitionDrawable;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/incallui/smartcover/a1/A1CoverFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showSlideLayout(Z)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/BaseFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mIsIncoming:Z

    return-void
.end method

.method private clearViews()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showCallInfoAndName(Z)V

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showSlideLayout(Z)V

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showHungup(Z)V

    return-void
.end method

.method private initInCall()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mTransitionDrawable:Landroid/graphics/drawable/TransitionDrawable;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showCallInfoAndName(Z)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showSlideLayout(Z)V

    return-void
.end method

.method private initIncomingView()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mTransitionDrawable:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showCallInfoAndName(Z)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showHungup(Z)V

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showSlideLayout(Z)V

    return-void
.end method

.method private showCallInfoAndName(Z)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mName:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mCallInfoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mName:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mCallInfoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showName(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showNumber(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showCityInfo(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showTimeInfo(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showCityInfo(Ljava/lang/String;)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mCity:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mCity:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mDivider1:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mCity:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mCity:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mDivider1:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private showHungup(Z)V
    .locals 3

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mHungUp:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mHungUp:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mHungUp:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mHungUp:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mHungUp:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mHungUp:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private showName(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showNumber(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showSlideLayout(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mSlideLayout:Lcom/android/incallui/view/HorizontalSlideLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/HorizontalSlideLayout;->showPanel()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mSlideLayout:Lcom/android/incallui/view/HorizontalSlideLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/HorizontalSlideLayout;->hidePanel()V

    goto :goto_0
.end method

.method private showTimeInfo(Ljava/lang/String;)V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mTime:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mTime:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mNumber:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mCity:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget-object v3, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mDivider2:Landroid/view/View;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mTime:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public clearSmartCoverView()V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->clearViews()V

    return-void
.end method

.method public configCallElapsedTime(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showTimeInfo(Ljava/lang/String;)V

    return-void
.end method

.method public configSmartCover(Lcom/android/incallui/model/CallCardInfo;ZZI)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    iget-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v1}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iput-boolean v3, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v5, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    if-nez p3, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/incallui/util/Utils;->localizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    :cond_1
    iget-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mIsIncoming:Z

    if-nez v1, :cond_2

    iput-boolean v4, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mIsIncoming:Z

    invoke-direct {p0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->initIncomingView()V

    :cond_2
    invoke-direct {p0, v5}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showTimeInfo(Ljava/lang/String;)V

    :cond_3
    :goto_0
    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showName(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showNumber(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showCityInfo(Ljava/lang/String;)V

    return-void

    :cond_4
    iget-boolean v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mIsIncoming:Z

    if-eqz v1, :cond_3

    iput-boolean v3, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mIsIncoming:Z

    invoke-direct {p0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->initInCall()V

    goto :goto_0
.end method

.method public configSmartCoverCallButton(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->showHungup(Z)V

    return-void
.end method

.method protected bridge synthetic createPresenter()Lcom/android/incallui/Presenter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->createPresenter()Lcom/android/incallui/smartcover/a1/A1CoverPresenter;

    move-result-object v0

    return-object v0
.end method

.method protected createPresenter()Lcom/android/incallui/smartcover/a1/A1CoverPresenter;
    .locals 1

    new-instance v0, Lcom/android/incallui/smartcover/a1/A1CoverPresenter;

    invoke-direct {v0}, Lcom/android/incallui/smartcover/a1/A1CoverPresenter;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic getUi()Lcom/android/incallui/Ui;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->getUi()Lcom/android/incallui/smartcover/a1/A1CoverPresenter$CoverCardUi;

    move-result-object v0

    return-object v0
.end method

.method protected getUi()Lcom/android/incallui/smartcover/a1/A1CoverPresenter$CoverCardUi;
    .locals 0

    return-object p0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mHungUp:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mSlideLayout:Lcom/android/incallui/view/HorizontalSlideLayout;

    new-instance v1, Lcom/android/incallui/smartcover/a1/A1CoverFragment$1;

    invoke-direct {v1, p0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment$1;-><init>(Lcom/android/incallui/smartcover/a1/A1CoverFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/HorizontalSlideLayout;->setOnSlideFinishListener(Lcom/android/incallui/view/HorizontalSlideLayout$OnSlideFinishListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/a1/A1CoverPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/smartcover/a1/A1CoverPresenter;->endCallClicked()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a0013
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    const/high16 v5, 0x7f030000

    const/4 v6, 0x0

    invoke-virtual {p1, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v5, 0x7f0a0004

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v4, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {}, Lcom/android/incallui/util/SmartCoverUtil;->getDisplayWindowSizeInSmartCover()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v5, v2, Landroid/graphics/Rect;->left:I

    iput v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v5, v2, Landroid/graphics/Rect;->top:I

    iput v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v5, v2, Landroid/graphics/Rect;->left:I

    iput v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v5

    iput v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v5, v4, v5

    iget v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v5, v6

    iput v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    return-object v3
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    const v1, 0x7f0a0004

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a0005

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mName:Landroid/widget/TextView;

    const v1, 0x7f0a0007

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mNumber:Landroid/widget/TextView;

    const v1, 0x7f0a0009

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mCity:Landroid/widget/TextView;

    const v1, 0x7f0a000b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mTime:Landroid/widget/TextView;

    const v1, 0x7f0a0006

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mCallInfoLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0a0008

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mDivider1:Landroid/view/View;

    const v1, 0x7f0a000a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mDivider2:Landroid/view/View;

    const v1, 0x7f0a000c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/view/HorizontalSlideLayout;

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mSlideLayout:Lcom/android/incallui/view/HorizontalSlideLayout;

    const v1, 0x7f0a0013

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mHungUp:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v1, p0, Lcom/android/incallui/smartcover/a1/A1CoverFragment;->mTransitionDrawable:Landroid/graphics/drawable/TransitionDrawable;

    return-void
.end method
