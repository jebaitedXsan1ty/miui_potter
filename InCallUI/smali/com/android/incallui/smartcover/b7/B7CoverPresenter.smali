.class public Lcom/android/incallui/smartcover/b7/B7CoverPresenter;
.super Lcom/android/incallui/Presenter;
.source "B7CoverPresenter.java"

# interfaces
.implements Lcom/android/incallui/InCallPresenter$InCallStateListener;
.implements Lcom/android/incallui/InCallPresenter$IncomingCallListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/smartcover/b7/B7CoverPresenter$1;,
        Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/Presenter",
        "<",
        "Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;",
        ">;",
        "Lcom/android/incallui/InCallPresenter$InCallStateListener;",
        "Lcom/android/incallui/InCallPresenter$IncomingCallListener;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

.field private mCallTimer:Lcom/android/incallui/CallTimer;

.field private mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

.field private mCurrentCall:Lcom/android/incallui/Call;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/smartcover/b7/B7CoverPresenter;)Lcom/android/incallui/Call;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/incallui/smartcover/b7/B7CoverPresenter;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/smartcover/b7/B7CoverPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->updateSmartCoverInfo()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/incallui/Presenter;-><init>()V

    new-instance v0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$1;-><init>(Lcom/android/incallui/smartcover/b7/B7CoverPresenter;)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    new-instance v0, Lcom/android/incallui/CallTimer;

    new-instance v1, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$2;-><init>(Lcom/android/incallui/smartcover/b7/B7CoverPresenter;)V

    invoke-direct {v0, v1}, Lcom/android/incallui/CallTimer;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    return-void
.end method

.method private updateCallEntry()V
    .locals 4

    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->getInstance()Lcom/android/incallui/ContactInfoCache;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v3, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/ContactInfoCache;->maybeUpdateContactInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-void
.end method

.method private updateSingleDisplayInfo(Lcom/android/incallui/model/CallCardInfo;)V
    .locals 7

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;

    if-nez v0, :cond_0

    sget-object v1, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "updateSingleDisplayInfo called but ui is null!"

    invoke-static {v1, v4}, Lcom/android/incallui/Log;->w(Ljava/lang/Object;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v3

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isThreeWayIncoming(Lcom/android/incallui/Call;)Z

    move-result v6

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-eqz v1, :cond_2

    iget-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    if-eqz v1, :cond_3

    const/4 v2, 0x0

    :goto_0
    if-eqz v6, :cond_1

    const/4 v2, 0x0

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isThreeWayOutgoing(Lcom/android/incallui/Call;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    :cond_2
    move-object v1, p1

    move v5, v4

    invoke-interface/range {v0 .. v5}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->setPrimary(Lcom/android/incallui/model/CallCardInfo;ZZZZ)V

    return-void

    :cond_3
    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v1}, Lcom/android/incallui/CallUtils;->isConference(Lcom/android/incallui/Call;)Z

    move-result v2

    goto :goto_0
.end method

.method private updateSmartCoverInfo()V
    .locals 7

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v5, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-static {v5, v6}, Lcom/android/incallui/model/CallCardInfo;->createCallCardInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v0

    iget-boolean v5, v0, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    if-eqz v5, :cond_1

    invoke-interface {v2}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->initAnswerOperLayout()V

    invoke-interface {v2}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->showTextButton()V

    invoke-interface {v2, v3}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->showAnswerUi(Z)V

    invoke-interface {v2, v4}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->showHangUpButton(Z)V

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->updateSingleDisplayInfo(Lcom/android/incallui/model/CallCardInfo;)V

    return-void

    :cond_1
    invoke-interface {v2, v4}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->showAnswerUi(Z)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/incallui/CallList;->getOutgoingOrActiveCall()Lcom/android/incallui/Call;

    move-result-object v1

    if-eqz v1, :cond_2

    :goto_1
    invoke-interface {v2, v3}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->showHangUpButton(Z)V

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1
.end method


# virtual methods
.method public endCallClicked()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->disconnectCall(Ljava/lang/String;)V

    return-void
.end method

.method public onAnswer(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/incallui/TelecomAdapter;->answerCall(Ljava/lang/String;I)V

    return-void
.end method

.method public onDecline()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/TelecomAdapter;->rejectCall(Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method public onIncomingCall(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V
    .locals 1

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    return-void
.end method

.method public onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getFirstCall()Lcom/android/incallui/Call;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-direct {p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->updateCallEntry()V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->updateSmartCoverInfo()V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->updateTimeAndTelocation()V

    return-void
.end method

.method public bridge synthetic onUiReady(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->onUiReady(Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;)V

    return-void
.end method

.method public onUiReady(Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiReady(Lcom/android/incallui/Ui;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/InCallPresenter;->getPotentialStateFromCallList(Lcom/android/incallui/CallList;)Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v1

    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-virtual {p0, v2, v1, v0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/android/incallui/InCallPresenter;->addListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/android/incallui/InCallPresenter;->addIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    return-void
.end method

.method public bridge synthetic onUiUnready(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->onUiUnready(Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;)V

    return-void
.end method

.method public onUiUnready(Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiUnready(Lcom/android/incallui/Ui;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    iput-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iput-object v1, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-void
.end method

.method public updateTimeAndTelocation()V
    .locals 12

    const-wide/16 v10, 0x3e8

    const/4 v9, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v5

    check-cast v5, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;

    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getState()I

    move-result v6

    invoke-static {v6}, Lcom/android/incallui/Call$State;->isRinging(I)Z

    move-result v4

    :goto_0
    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v6, :cond_3

    :cond_0
    if-eqz v5, :cond_1

    invoke-interface {v5, v8}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->setPrimaryCallElapsedTime(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v6, :cond_6

    invoke-interface {v5, v9}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->showTelocation(Z)V

    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v6}, Lcom/android/incallui/CallTimer;->cancel()V

    :goto_2
    return-void

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getState()I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_4

    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getState()I

    move-result v6

    const/16 v7, 0x8

    if-ne v6, v7, :cond_0

    :cond_4
    if-nez v4, :cond_0

    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v6}, Lcom/android/incallui/CallTimer;->isRunning()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCallTimer:Lcom/android/incallui/CallTimer;

    invoke-virtual {v6, v10, v11}, Lcom/android/incallui/CallTimer;->start(J)Z

    :cond_5
    iget-object v6, p0, Lcom/android/incallui/smartcover/b7/B7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v6}, Lcom/android/incallui/Call;->getConnectTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v0

    div-long v6, v2, v10

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->setPrimaryCallElapsedTime(Ljava/lang/String;)V

    invoke-interface {v5, v9}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->showTelocation(Z)V

    goto :goto_2

    :cond_6
    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/android/incallui/smartcover/b7/B7CoverPresenter$AnswerUi;->showTelocation(Z)V

    goto :goto_1
.end method
