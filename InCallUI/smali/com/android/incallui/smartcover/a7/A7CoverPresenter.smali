.class public Lcom/android/incallui/smartcover/a7/A7CoverPresenter;
.super Lcom/android/incallui/Presenter;
.source "A7CoverPresenter.java"

# interfaces
.implements Lcom/android/incallui/InCallPresenter$InCallStateListener;
.implements Lcom/android/incallui/InCallPresenter$IncomingCallListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/smartcover/a7/A7CoverPresenter$1;,
        Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/Presenter",
        "<",
        "Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;",
        ">;",
        "Lcom/android/incallui/InCallPresenter$InCallStateListener;",
        "Lcom/android/incallui/InCallPresenter$IncomingCallListener;"
    }
.end annotation


# instance fields
.field private mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

.field private mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

.field private mCurrentCall:Lcom/android/incallui/Call;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/smartcover/a7/A7CoverPresenter;)Lcom/android/incallui/Call;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/incallui/smartcover/a7/A7CoverPresenter;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/smartcover/a7/A7CoverPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->updateSmartCoverInfo()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/Presenter;-><init>()V

    new-instance v0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter$1;-><init>(Lcom/android/incallui/smartcover/a7/A7CoverPresenter;)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    return-void
.end method

.method private updateCallEntry()V
    .locals 4

    invoke-static {}, Lcom/android/incallui/ContactInfoCache;->getInstance()Lcom/android/incallui/ContactInfoCache;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iget-object v2, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    iget-object v3, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mContactInfoCacheCallback:Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/ContactInfoCache;->maybeUpdateContactInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;Lcom/android/incallui/ContactInfoCache$ContactInfoCacheCallback;)Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-void
.end method

.method private updateSmartCoverInfo()V
    .locals 8

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v5

    check-cast v5, Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;

    if-nez v5, :cond_0

    return-void

    :cond_0
    iget-object v6, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iget-object v7, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    invoke-static {v6, v7}, Lcom/android/incallui/model/CallCardInfo;->createCallCardInfo(Lcom/android/incallui/Call;Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;)Lcom/android/incallui/model/CallCardInfo;

    move-result-object v0

    iget-object v6, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v6}, Lcom/android/incallui/CallUtils;->isGenericConference(Lcom/android/incallui/Call;)Z

    move-result v4

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-eqz v6, :cond_2

    iget-boolean v6, v0, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    if-eqz v6, :cond_4

    const/4 v3, 0x0

    :goto_0
    iget-object v6, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v6}, Lcom/android/incallui/CallUtils;->isThreeWayIncoming(Lcom/android/incallui/Call;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v3, 0x0

    :cond_1
    iget-object v6, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v6}, Lcom/android/incallui/CallUtils;->isThreeWayOutgoing(Lcom/android/incallui/Call;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v3, 0x0

    :cond_2
    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->getPrimaryCallNum()I

    move-result v6

    invoke-interface {v5, v0, v3, v4, v6}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;->configSmartCover(Lcom/android/incallui/model/CallCardInfo;ZZI)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v6

    invoke-static {v6}, Lcom/android/incallui/InCallPresenter;->getPotentialStateFromCallList(Lcom/android/incallui/CallList;)Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v2

    iget-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    sget-object v6, Lcom/android/incallui/InCallPresenter$InCallState;->OUTGOING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-eq v2, v6, :cond_3

    sget-object v6, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-eq v2, v6, :cond_3

    const/4 v1, 0x0

    :cond_3
    if-eqz v1, :cond_5

    const/4 v6, 0x1

    :goto_1
    invoke-interface {v5, v6}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;->configSmartCoverEndCallSlide(Z)V

    return-void

    :cond_4
    iget-object v6, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-static {v6}, Lcom/android/incallui/CallUtils;->isConference(Lcom/android/incallui/Call;)Z

    move-result v3

    goto :goto_0

    :cond_5
    const/4 v6, 0x0

    goto :goto_1
.end method


# virtual methods
.method public endCallSlided()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/TelecomAdapter;->disconnectCall(Ljava/lang/String;)V

    return-void
.end method

.method public getPrimaryCallNum()I
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getChildCallIds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onAnswer(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/incallui/TelecomAdapter;->answerCall(Ljava/lang/String;I)V

    return-void
.end method

.method public onDecline()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {v1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/incallui/TelecomAdapter;->rejectCall(Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method public onIncomingCall(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/Call;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onIncomingCall(): state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " , call = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    return-void
.end method

.method public onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStateChange(): state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , callList = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p3}, Lcom/android/incallui/CallList;->getFirstCall()Lcom/android/incallui/Call;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    invoke-virtual {p3}, Lcom/android/incallui/CallList;->hasLiveCall()Z

    move-result v1

    if-nez v1, :cond_1

    iput-object v3, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iput-object v3, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->updateCallEntry()V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->updateSmartCoverInfo()V

    goto :goto_0
.end method

.method public bridge synthetic onUiReady(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->onUiReady(Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;)V

    return-void
.end method

.method public onUiReady(Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiReady(Lcom/android/incallui/Ui;)V

    const-string/jumbo v2, "onUiReady"

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/InCallPresenter;->getPotentialStateFromCallList(Lcom/android/incallui/CallList;)Lcom/android/incallui/InCallPresenter$InCallState;

    move-result-object v1

    sget-object v2, Lcom/android/incallui/InCallPresenter$InCallState;->NO_CALLS:Lcom/android/incallui/InCallPresenter$InCallState;

    invoke-virtual {p0, v2, v1, v0}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->onStateChange(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/CallList;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/android/incallui/InCallPresenter;->addListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/android/incallui/InCallPresenter;->addIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    return-void
.end method

.method public bridge synthetic onUiUnready(Lcom/android/incallui/Ui;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->onUiUnready(Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;)V

    return-void
.end method

.method public onUiUnready(Lcom/android/incallui/smartcover/a7/A7CoverPresenter$CoverCardUi;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/android/incallui/Presenter;->onUiUnready(Lcom/android/incallui/Ui;)V

    const-string/jumbo v0, "onUiUnready"

    invoke-static {p0, v0}, Lcom/android/incallui/Log;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeListener(Lcom/android/incallui/InCallPresenter$InCallStateListener;)V

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/InCallPresenter;->removeIncomingCallListener(Lcom/android/incallui/InCallPresenter$IncomingCallListener;)V

    iput-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCurrentCall:Lcom/android/incallui/Call;

    iput-object v1, p0, Lcom/android/incallui/smartcover/a7/A7CoverPresenter;->mCacheEntry:Lcom/android/incallui/ContactInfoCache$ContactCacheEntry;

    return-void
.end method
