.class public Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;
.super Lcom/android/incallui/BaseFragment;
.source "SmartCoverAnswerFragment.java"

# interfaces
.implements Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;
.implements Lcom/android/incallui/OnAnimatorChanged;
.implements Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/BaseFragment",
        "<",
        "Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;",
        "Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;",
        ">;",
        "Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;",
        "Lcom/android/incallui/OnAnimatorChanged;",
        "Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isCallInfoAnimFinished:Z

.field private isFirstShow:Z

.field private isMarkInfoNeedToShow:Z

.field private isShowingAnswerUI:Z

.field private mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

.field private mAnswerImg:Lcom/android/incallui/view/CircleImageView;

.field private mAnswerOperView:Landroid/view/View;

.field private mArrowList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mBounceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/incallui/view/CircleImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mCallCardConferenceManageButton:Landroid/widget/ImageView;

.field private mCallCardExtraDivider:Landroid/widget/ImageView;

.field private mCallCardExtraInfo:Landroid/widget/LinearLayout;

.field private mCallCardExtraTitle:Landroid/widget/TextView;

.field private mCallCardMarkInfo:Landroid/widget/LinearLayout;

.field private mCallCardNumberSep:Landroid/view/View;

.field private mCallCardOptionalInfo:Landroid/widget/LinearLayout;

.field private mCallCardTelocation:Landroid/widget/TextView;

.field private mCallProviderInfo:Landroid/widget/TextView;

.field private mCallStateLabel:Landroid/widget/TextView;

.field private mCallStateLabelDivider:Landroid/widget/ImageView;

.field private mCallerNameSet:Landroid/animation/AnimatorSet;

.field private mCircleAnimatorSet:Landroid/animation/AnimatorSet;

.field private mConferenceCallMarkSep:Landroid/view/View;

.field private mConferenceCallNumber:Landroid/widget/TextView;

.field private mConferenceSimCardInfo:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mDefaultPanelHeight:I

.field private mElapsedTime:Landroid/widget/TextView;

.field private mEndBtn:Landroid/widget/Button;

.field protected mHandler:Landroid/os/Handler;

.field private mLabelAndNumber:Landroid/view/View;

.field private mMarkCount:Landroid/widget/TextView;

.field private mMarkDivider:Landroid/widget/ImageView;

.field private mMarkProvider:Landroid/widget/ImageView;

.field private mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

.field private mMarkTitle:Landroid/widget/TextView;

.field private mOperateLayout:Landroid/widget/LinearLayout;

.field private mPhoneNumber:Landroid/widget/TextView;

.field private mPostDialTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

.field private mPrimaryCallBanner:Landroid/widget/LinearLayout;

.field private mPrimaryName:Landroid/widget/TextView;

.field private mProviderInfo:Landroid/view/View;

.field private mProviderLabel:Landroid/widget/TextView;

.field private mProviderNumber:Landroid/widget/TextView;

.field private mRejectArrow:Lcom/android/incallui/view/ArrowImageView;

.field private mRejectImg:Lcom/android/incallui/view/CircleImageView;

.field private mRootView:Landroid/view/View;

.field private mSimCardInfo:Landroid/widget/ImageView;

.field private mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

.field private mSubscriptionId:Landroid/widget/TextView;

.field private mTranslateInList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/incallui/view/CircleImageView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mLabelAndNumber:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isCallInfoAnimFinished:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->playMarkInfoAnimator()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/incallui/BaseFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    iput-boolean v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isCallInfoAnimFinished:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mDefaultPanelHeight:I

    iput-boolean v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isShowingAnswerUI:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isFirstShow:Z

    new-instance v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$1;-><init>(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private getDefaultPanelHeight()I
    .locals 3

    iget v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mDefaultPanelHeight:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mDefaultPanelHeight:I

    invoke-static {}, Landroid/content/res/MiuiConfiguration;->getScaleMode()I

    move-result v0

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mDefaultPanelHeight:I

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mDefaultPanelHeight:I

    :cond_0
    sget-object v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getDefaultPanelHeight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mDefaultPanelHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mDefaultPanelHeight:I

    return v0
.end method

.method private hideOperateItems()V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerOperView:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->stopCircleBounceAnimator()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setMaxTransparent()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/CircleImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/CircleImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/ArrowImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/ArrowImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/android/incallui/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private playArrowAnimator(Landroid/view/View;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/ArrowImageView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/incallui/view/ArrowImageView;->playArrowMoveUpAnimator()V

    :cond_2
    return-void
.end method

.method private playCallerInfoTranslateIn()V
    .locals 14

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isCallInfoAnimFinished:Z

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mLabelAndNumber:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardOptionalInfo:Landroid/widget/LinearLayout;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallerNameSet:Landroid/animation/AnimatorSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    sget-object v6, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v7, v12, [F

    aput v10, v7, v9

    aput v13, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    sget-object v6, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v7, v12, [F

    const/high16 v8, 0x43480000    # 200.0f

    aput v8, v7, v9

    aput v10, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    new-instance v5, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$5;

    invoke-direct {v5, p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$5;-><init>(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;)V

    invoke-virtual {v3, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mLabelAndNumber:Landroid/view/View;

    sget-object v6, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v7, v12, [F

    aput v10, v7, v9

    aput v13, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mLabelAndNumber:Landroid/view/View;

    sget-object v6, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v7, v12, [F

    const/high16 v8, 0x43480000    # 200.0f

    aput v8, v7, v9

    aput v10, v7, v11

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    new-instance v5, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$6;

    invoke-direct {v5, p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$6;-><init>(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;)V

    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v2, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallerNameSet:Landroid/animation/AnimatorSet;

    const-wide/16 v6, 0x320

    invoke-virtual {v5, v6, v7}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallerNameSet:Landroid/animation/AnimatorSet;

    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallerNameSet:Landroid/animation/AnimatorSet;

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080002

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallerNameSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    iget-object v5, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallerNameSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method private playMarkInfoAnimator()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardOptionalInfo:Landroid/widget/LinearLayout;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/16 v2, 0xc8

    invoke-static {v0, v1, v2, v3, v3}, Lcom/android/incallui/AnimationUtils$Translation;->translationIn(Landroid/view/View;Landroid/util/Property;III)V

    return-void
.end method

.method private setMaxTransparent()V
    .locals 3

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/view/CircleImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/android/incallui/view/CircleImageView;->setAlpha(F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setMultiSimIndicator(IZ)V
    .locals 3

    const/16 v2, 0x8

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    :goto_0
    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSimCardInfo:Landroid/widget/ImageView;

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private stopCallerInfoTranslateIn()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallerNameSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallerNameSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mLabelAndNumber:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardOptionalInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardOptionalInfo:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private updatePhoneNumberField(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setPrimaryPhoneNumber(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPostDialTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public cancelArrowAnimator(Landroid/view/View;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/ArrowImageView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/incallui/view/ArrowImageView;->cancelAllAnimator()V

    :cond_2
    return-void
.end method

.method public bridge synthetic createPresenter()Lcom/android/incallui/Presenter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->createPresenter()Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;
    .locals 1

    new-instance v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;

    invoke-direct {v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;-><init>()V

    return-object v0
.end method

.method public getTranslateInAnimatorSet(Lcom/android/incallui/view/CircleImageView;)Landroid/animation/ObjectAnimator;
    .locals 17

    invoke-virtual/range {p1 .. p1}, Lcom/android/incallui/view/CircleImageView;->getAnimatorImageHeight()I

    move-result v11

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    const v14, 0x438b8000    # 279.0f

    const/4 v15, 0x0

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    neg-int v14, v11

    int-to-float v14, v14

    const v15, 0x3e99999a    # 0.3f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_4

    const/4 v14, 0x5

    :goto_1
    int-to-float v14, v14

    const/high16 v15, 0x3f000000    # 0.5f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_5

    const/4 v14, -0x3

    :goto_2
    int-to-float v14, v14

    const v15, 0x3f19999a    # 0.6f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_6

    const/4 v14, 0x2

    :goto_3
    int-to-float v14, v14

    const v15, 0x3f333333    # 0.7f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_7

    const/high16 v14, -0x40800000    # -1.0f

    :goto_4
    const v15, 0x3f4ccccd    # 0.8f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    rem-int/lit8 v14, v2, 0x2

    if-eqz v14, :cond_1

    :cond_1
    const/4 v14, 0x0

    int-to-float v14, v14

    const v15, 0x3f666666    # 0.9f

    invoke-static {v15, v14}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v9

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x0

    invoke-static {v14, v15}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v10

    sget-object v14, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/16 v15, 0x8

    new-array v15, v15, [Landroid/animation/Keyframe;

    const/16 v16, 0x0

    aput-object v3, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    const/16 v16, 0x2

    aput-object v5, v15, v16

    const/16 v16, 0x3

    aput-object v6, v15, v16

    const/16 v16, 0x4

    aput-object v7, v15, v16

    const/16 v16, 0x5

    aput-object v8, v15, v16

    const/16 v16, 0x6

    aput-object v9, v15, v16

    const/16 v16, 0x7

    aput-object v10, v15, v16

    invoke-static {v14, v15}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v12

    const/4 v14, 0x1

    new-array v14, v14, [Landroid/animation/PropertyValuesHolder;

    const/4 v15, 0x0

    aput-object v12, v14, v15

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_8

    const/16 v14, 0x640

    :goto_5
    int-to-long v14, v14

    invoke-virtual {v13, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v14, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$4;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v14, v0, v1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$4;-><init>(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;Lcom/android/incallui/view/CircleImageView;)V

    invoke-virtual {v13, v14}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_9

    const-wide/16 v14, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    :cond_2
    :goto_6
    return-object v13

    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_0

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v14, 0x3

    goto/16 :goto_1

    :cond_5
    const/4 v14, -0x2

    goto/16 :goto_2

    :cond_6
    const/4 v14, 0x1

    goto/16 :goto_3

    :cond_7
    const/high16 v14, -0x41000000    # -0.5f

    goto/16 :goto_4

    :cond_8
    const/16 v14, 0x4b0

    goto :goto_5

    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_2

    const-wide/16 v14, 0xfa

    invoke-virtual {v13, v14, v15}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    goto :goto_6
.end method

.method protected bridge synthetic getUi()Lcom/android/incallui/Ui;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getUi()Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;

    move-result-object v0

    return-object v0
.end method

.method protected getUi()Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter$AnswerUi;
    .locals 0

    return-object p0
.end method

.method public initAnswerOperLayout()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerOperView:Landroid/view/View;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a001c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerOperView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0020

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/CircleImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/CircleImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/ArrowImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerOperView:Landroid/view/View;

    const v1, 0x7f0a0022

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/ArrowImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectArrow:Lcom/android/incallui/view/ArrowImageView;

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onAnimatorPause()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->pauseCircleBounceAnimator()V

    return-void
.end method

.method public onAnimatorResume()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->playCircleBounceAnimator()V

    return-void
.end method

.method public onAnswer(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;

    invoke-virtual {v0, p1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->onAnswer(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->endCallClicked()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a002c
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/incallui/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v3, 0x7f030016

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v2, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {}, Lcom/android/incallui/util/SmartCoverUtil;->getDisplayWindowSizeInSmartCover()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    iget v4, v1, Landroid/graphics/Rect;->left:I

    iget v5, v1, Landroid/graphics/Rect;->top:I

    iget v6, v1, Landroid/graphics/Rect;->right:I

    sub-int v6, v2, v6

    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v7, v0, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    return-object v3
.end method

.method public onDecline()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerPresenter;->onDecline()V

    return-void
.end method

.method public onDestroyView()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/incallui/AnimatorObservable;->createInstacne()Lcom/android/incallui/AnimatorObservable;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/AnimatorObservable;->unRegisterListener(Lcom/android/incallui/OnAnimatorChanged;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    iput-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    :cond_0
    iput-boolean v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    iput-boolean v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isCallInfoAnimFinished:Z

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onDestroyView()V

    return-void
.end method

.method public onPanelCollapsed(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "onPanelCollapsed()"

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->log(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->onViewTouchUp(Landroid/view/View;)V

    return-void
.end method

.method public onPanelExpanded(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v1, "onPanelExpanded()"

    invoke-direct {p0, v1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setMaxTransparent()V

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->onDecline()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    if-ne p1, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->onAnswer(I)V

    goto :goto_0
.end method

.method public onPanelSlide(Landroid/view/View;FLandroid/view/View;)V
    .locals 6

    const v5, 0x3ec28f5c    # 0.38f

    cmpg-float v4, p2, v5

    if-ltz v4, :cond_0

    const v4, 0x3f19999a    # 0.6f

    cmpl-float v4, p2, v4

    if-lez v4, :cond_1

    :cond_0
    return-void

    :cond_1
    sub-float v4, p2, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v4, Lcom/android/incallui/view/SlidingUpPanelLayout;->DIFF_SLIDE_OFFSET:F

    div-float v2, v0, v4

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_2

    if-ne v3, p3, :cond_2

    const/high16 v4, 0x3f800000    # 1.0f

    mul-float/2addr v4, v2

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public onPanelViewCollapsedReleased(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "onPanelViewCollapsedReleased()"

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onPanelViewExpandReleased(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "onPanelViewExpandReleased()"

    invoke-direct {p0, v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lcom/android/incallui/BaseFragment;->onStart()V

    invoke-static {}, Lcom/android/incallui/AnimatorObservable;->createInstacne()Lcom/android/incallui/AnimatorObservable;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/incallui/AnimatorObservable;->registerListener(Lcom/android/incallui/OnAnimatorChanged;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/incallui/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a0018

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/SlidingUpPanelLayout;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, v9}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setSlidingEnabled(Z)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, v8}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setOverlayed(Z)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, p0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setPanelSlideListener(Lcom/android/incallui/view/SlidingUpPanelLayout$PanelSlideListener;)V

    const v0, 0x7f0a0057

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSimCardInfo:Landroid/widget/ImageView;

    const v0, 0x7f0a00b2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceSimCardInfo:Landroid/widget/ImageView;

    const v0, 0x7f0a0029

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPhoneNumber:Landroid/widget/TextView;

    const v0, 0x7f0a0005

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    const v0, 0x7f0a009b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mLabelAndNumber:Landroid/view/View;

    const v0, 0x7f0a00a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardNumberSep:Landroid/view/View;

    const v0, 0x7f0a00a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardTelocation:Landroid/widget/TextView;

    const v0, 0x7f0a00b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallStateLabel:Landroid/widget/TextView;

    const v0, 0x7f0a002a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mElapsedTime:Landroid/widget/TextView;

    const v0, 0x7f0a00b7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mProviderInfo:Landroid/view/View;

    const v0, 0x7f0a00b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mProviderLabel:Landroid/widget/TextView;

    const v0, 0x7f0a00b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mProviderNumber:Landroid/widget/TextView;

    const v0, 0x7f0a00a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSubscriptionId:Landroid/widget/TextView;

    const v0, 0x7f0a00b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallStateLabelDivider:Landroid/widget/ImageView;

    const v0, 0x7f0a009a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    const v0, 0x7f0a00b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceCallMarkSep:Landroid/view/View;

    const v0, 0x7f0a00b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceCallNumber:Landroid/widget/TextView;

    const v0, 0x7f0a00a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$2;

    invoke-direct {v1, p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$2;-><init>(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v9}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->showConferenceControl(Z)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a009c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardOptionalInfo:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a00a9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardMarkInfo:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a00ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallProviderInfo:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a00af

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardExtraInfo:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a00aa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a00ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkCount:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a00ab

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkDivider:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a00ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkProvider:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a00b0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardExtraDivider:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a00b1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardExtraTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRootView:Landroid/view/View;

    const v1, 0x7f0a002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mEndBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mEndBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0d0012

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    new-instance v6, Landroid/text/style/TextAppearanceSpan;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f0d0011

    invoke-direct {v6, v0, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    new-instance v7, Landroid/text/style/TextAppearanceSpan;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f0d0014

    invoke-direct {v7, v0, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {v7}, Landroid/text/style/TextAppearanceSpan;->getFamily()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Landroid/text/style/TextAppearanceSpan;->getTextStyle()I

    move-result v2

    invoke-virtual {v7}, Landroid/text/style/TextAppearanceSpan;->getTextSize()I

    move-result v3

    invoke-virtual {v6}, Landroid/text/style/TextAppearanceSpan;->getTextColor()Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v6}, Landroid/text/style/TextAppearanceSpan;->getLinkTextColor()Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    iput-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPostDialTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardOptionalInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    iput-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    return-void

    :cond_0
    move v0, v9

    goto :goto_0
.end method

.method public onViewMove(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->getSlideOffset()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3fe6666666666666L    # 0.7

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->cancelArrowAnimator(Landroid/view/View;)V

    return-void
.end method

.method public onViewTouchDown(Landroid/view/View;Z)V
    .locals 5

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/view/CircleImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/android/incallui/view/CircleImageView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, p1, v3}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setSlidingView(Landroid/view/View;Z)V

    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->playArrowAnimator(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public onViewTouchUp(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/view/CircleImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/android/incallui/view/CircleImageView;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v1, v3}, Lcom/android/incallui/view/CircleImageView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v4, v3}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setSlidingView(Landroid/view/View;Z)V

    invoke-virtual {p0, p1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->cancelArrowAnimator(Landroid/view/View;)V

    return-void
.end method

.method public pauseCircleBounceAnimator()V
    .locals 6

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/view/CircleImageView;

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    invoke-virtual {v3, v2}, Lcom/android/incallui/view/CircleImageView;->getBounceAnimatorSet(I)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->pause()V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public playCircleBounceAnimator()V
    .locals 5

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/view/CircleImageView;

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/android/incallui/view/CircleImageView;->getBounceAnimatorSet(I)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isPaused()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->resume()V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public playCircleTranslateInAnimator()V
    .locals 8

    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [Landroid/animation/ObjectAnimator;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {p0, v3}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getTranslateInAnimatorSet(Lcom/android/incallui/view/CircleImageView;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    aput-object v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v5, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$3;

    invoke-direct {v5, p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment$3;-><init>(Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;)V

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v6, 0x320

    invoke-virtual {v4, v6, v7}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    iget-object v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public setCallState(ILandroid/telecom/DisconnectCause;I)V
    .locals 8

    const v7, 0x800005

    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v0, 0x0

    invoke-static {p1, p2, v5, p3}, Lcom/android/incallui/CallUtils;->getCallStateLabelFromState(ILandroid/telecom/DisconnectCause;ZI)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setSingleCallState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "DisconnectCause: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/incallui/Log;->v(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/android/incallui/Call$State;->isDialing(I)Z

    move-result v3

    if-nez v3, :cond_0

    const/16 v3, 0xa

    if-ne p1, v3, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const/4 v2, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mProviderInfo:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_2
    const/16 v3, 0x9

    if-ne p1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getGravity()I

    move-result v3

    if-eq v3, v7, :cond_1

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallStateLabel:Landroid/widget/TextView;

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallStateLabel:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_1
.end method

.method public setPrimary(Lcom/android/incallui/model/CallCardInfo;ZZIZZ)V
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p6, :cond_5

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isMtImsConference:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0b00a5

    :goto_0
    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0, v1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->showConferenceControl(Z)V

    :cond_0
    :goto_2
    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iget-boolean v2, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iget-object v4, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    move-object v0, p0

    move v3, p5

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setPrimaryName(Ljava/lang/String;ZZLjava/lang/String;Z)V

    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->leftPostDialString:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->updatePhoneNumberField(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setPrimaryTelocation(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p1, Lcom/android/incallui/model/CallCardInfo;->extraInfo:Ljava/lang/String;

    iget-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->markTitle:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/incallui/model/CallCardInfo;->markCount:Ljava/lang/String;

    iget-object v4, p1, Lcom/android/incallui/model/CallCardInfo;->markProviderIcon:Landroid/graphics/Bitmap;

    iget-object v5, p1, Lcom/android/incallui/model/CallCardInfo;->company:Ljava/lang/String;

    iget-boolean v6, p1, Lcom/android/incallui/model/CallCardInfo;->isIncoming:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setYellowPageInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;Z)V

    return-void

    :cond_1
    const v0, 0x7f0b00a4

    goto :goto_0

    :cond_2
    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v0}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->leftPostDialString:Ljava/lang/String;

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->telocation:Ljava/lang/String;

    iput-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->phoneNumber:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    invoke-static {p2, p3, v0}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, p4}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setPrimaryCallNum(I)V

    if-lez p4, :cond_4

    iget-boolean v0, p1, Lcom/android/incallui/model/CallCardInfo;->isVideoConference:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_3
    invoke-virtual {p0, v0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->showConferenceControl(Z)V

    iget v0, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    invoke-direct {p0, v0, v3}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setMultiSimIndicator(IZ)V

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    if-nez p2, :cond_6

    if-eqz p3, :cond_7

    :cond_6
    invoke-static {p2, p3, v1}, Lcom/android/incallui/CallUtils;->getConferenceString(ZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/incallui/model/CallCardInfo;->name:Ljava/lang/String;

    iput-boolean v1, p1, Lcom/android/incallui/model/CallCardInfo;->nameIsNumber:Z

    iput-object v2, p1, Lcom/android/incallui/model/CallCardInfo;->phoneTag:Ljava/lang/String;

    if-nez p3, :cond_0

    invoke-virtual {p0, p4}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setPrimaryCallNum(I)V

    invoke-virtual {p0, v3}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->showConferenceControl(Z)V

    iget v0, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    invoke-direct {p0, v0, v3}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setMultiSimIndicator(IZ)V

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->showConferenceControl(Z)V

    iget v0, p1, Lcom/android/incallui/model/CallCardInfo;->simIndicatorResId:I

    invoke-direct {p0, v0, v1}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->setMultiSimIndicator(IZ)V

    goto :goto_2
.end method

.method public setPrimaryCallElapsedTime(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mElapsedTime:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/incallui/AnimationUtils$Fade;->show(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mElapsedTime:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mElapsedTime:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPrimaryCallNum(I)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-gtz p1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceCallNumber:Landroid/widget/TextView;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceCallMarkSep:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceCallNumber:Landroid/widget/TextView;

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/incallui/util/Utils;->localizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceCallMarkSep:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPrimaryName(Ljava/lang/String;ZZLjava/lang/String;Z)V
    .locals 6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p3, :cond_1

    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {p1, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0b0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, p4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkTagTextAppearanceSpan:Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/4 v0, 0x0

    if-eqz p2, :cond_2

    const/4 v0, 0x3

    :cond_2
    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextDirection(I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryName:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setPrimaryPhoneNumber(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPhoneNumber:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPhoneNumber:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPhoneNumber:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPhoneNumber:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextDirection(I)V

    goto :goto_0
.end method

.method public setPrimaryTelocation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Telephony;->isTelocationEnable(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mContext:Landroid/content/Context;

    invoke-static {v0, p2}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getLocation(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardTelocation:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardNumberSep:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardTelocation:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardNumberSep:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardTelocation:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method setSlidingView(Landroid/view/View;Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, p1, p2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setTouchDragView(Landroid/view/View;Z)V

    return-void
.end method

.method public setYellowPageInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    if-eqz p6, :cond_8

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iput-boolean v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardMarkInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkCount:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkCount:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    if-eqz p4, :cond_3

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkProvider:Landroid/widget/ImageView;

    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkProvider:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iput-boolean v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardMarkInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallProviderInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallProviderInfo:Landroid/widget/TextView;

    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iput-boolean v4, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardExtraInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardExtraTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardExtraTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardExtraDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_4
    iget-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isMarkInfoNeedToShow:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isCallInfoAnimFinished:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardOptionalInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->playMarkInfoAnimator()V

    :cond_1
    :goto_5
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkCount:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkCount:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkProvider:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mMarkProvider:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardMarkInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallProviderInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardExtraInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardExtraDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardOptionalInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_5
.end method

.method public showAnswerUi(Z)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isFirstShow:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isShowingAnswerUI:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isShowingAnswerUI:Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->getDefaultPanelHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setPanelHeight(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setSlidingUpLayoutPaddingTop(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->collapsePanelManual()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mOperateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->playCallerInfoTranslateIn()V

    invoke-virtual {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->playCircleTranslateInAnimator()V

    :goto_0
    iput-boolean v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isFirstShow:Z

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isFirstShow:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isShowingAnswerUI:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    return-void

    :cond_2
    iput-boolean v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->isShowingAnswerUI:Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0, v2}, Lcom/android/incallui/view/SlidingUpPanelLayout;->setSlidingUpLayoutPaddingTop(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mSlidingUpLayout:Lcom/android/incallui/view/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/android/incallui/view/SlidingUpPanelLayout;->expandPanelManual()V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->stopCallerInfoTranslateIn()V

    invoke-direct {p0}, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->hideOperateItems()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public showConferenceControl(Z)V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    const v1, 0x800013

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    const v1, 0x7f02008c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceCallNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mPrimaryCallBanner:Landroid/widget/LinearLayout;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCallCardConferenceManageButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceCallMarkSep:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mConferenceCallNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showHangUpButton(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mEndBtn:Landroid/widget/Button;

    const v1, 0x7f0200ac

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mEndBtn:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mEndBtn:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public showTextButton(Z)V
    .locals 2

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mTranslateInList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerImg:Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mRejectArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mArrowList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mAnswerArrow:Lcom/android/incallui/view/ArrowImageView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public stopCircleBounceAnimator()V
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mBounceList:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/view/CircleImageView;

    invoke-virtual {v0}, Lcom/android/incallui/view/CircleImageView;->releaseBounceAnimatorSet()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->end()V

    iput-object v3, p0, Lcom/android/incallui/smartcover/x7/SmartCoverAnswerFragment;->mCircleAnimatorSet:Landroid/animation/AnimatorSet;

    :cond_1
    return-void
.end method
