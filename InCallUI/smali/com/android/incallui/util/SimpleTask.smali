.class public abstract Lcom/android/incallui/util/SimpleTask;
.super Ljava/lang/Object;
.source "SimpleTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/util/SimpleTask$InternalHandler;,
        Lcom/android/incallui/util/SimpleTask$SimpleTaskResult;,
        Lcom/android/incallui/util/SimpleTask$Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final synthetic -com-android-incallui-util-SimpleTask$StatusSwitchesValues:[I

.field private static mPendingTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/android/incallui/util/SimpleTask;",
            ">;>;"
        }
    .end annotation
.end field

.field private static mRunningTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/incallui/util/SimpleTask;",
            ">;"
        }
    .end annotation
.end field

.field private static sHandler:Lcom/android/incallui/util/SimpleTask$InternalHandler;


# instance fields
.field private isNeedPost:Z

.field private mAsyncTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "TResult;>;"
        }
    .end annotation
.end field

.field private mStartTime:J

.field private volatile mStatus:Lcom/android/incallui/util/SimpleTask$Status;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/incallui/util/SimpleTask$Status;"
        }
    .end annotation
.end field

.field private mTag:Ljava/lang/String;


# direct methods
.method private static synthetic -getcom-android-incallui-util-SimpleTask$StatusSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/incallui/util/SimpleTask;->-com-android-incallui-util-SimpleTask$StatusSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/incallui/util/SimpleTask;->-com-android-incallui-util-SimpleTask$StatusSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/incallui/util/SimpleTask$Status;->values()[Lcom/android/incallui/util/SimpleTask$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/incallui/util/SimpleTask$Status;->FINISHED:Lcom/android/incallui/util/SimpleTask$Status;

    invoke-virtual {v1}, Lcom/android/incallui/util/SimpleTask$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/incallui/util/SimpleTask$Status;->NEW:Lcom/android/incallui/util/SimpleTask$Status;

    invoke-virtual {v1}, Lcom/android/incallui/util/SimpleTask$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/incallui/util/SimpleTask$Status;->PENDING:Lcom/android/incallui/util/SimpleTask$Status;

    invoke-virtual {v1}, Lcom/android/incallui/util/SimpleTask$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/incallui/util/SimpleTask$Status;->RUNNING:Lcom/android/incallui/util/SimpleTask$Status;

    invoke-virtual {v1}, Lcom/android/incallui/util/SimpleTask$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    sput-object v0, Lcom/android/incallui/util/SimpleTask;->-com-android-incallui-util-SimpleTask$StatusSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -wrap0(Lcom/android/incallui/util/SimpleTask;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/incallui/util/SimpleTask;->finish()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/incallui/util/SimpleTask;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/util/SimpleTask;->timeOut(Ljava/lang/Object;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/incallui/util/SimpleTask;->mRunningTasks:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/incallui/util/SimpleTask;->mPendingTasks:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/android/incallui/util/SimpleTask$Status;->NEW:Lcom/android/incallui/util/SimpleTask$Status;

    iput-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mStatus:Lcom/android/incallui/util/SimpleTask$Status;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/incallui/util/SimpleTask;->isNeedPost:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/incallui/util/SimpleTask;->mStartTime:J

    new-instance v0, Lcom/android/incallui/util/SimpleTask$1;

    invoke-direct {v0, p0}, Lcom/android/incallui/util/SimpleTask$1;-><init>(Lcom/android/incallui/util/SimpleTask;)V

    iput-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mAsyncTask:Landroid/os/AsyncTask;

    return-void
.end method

.method public static destroyTask(Ljava/lang/String;)V
    .locals 2

    sget-object v1, Lcom/android/incallui/util/SimpleTask;->mRunningTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/incallui/util/SimpleTask;->mRunningTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/util/SimpleTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/incallui/util/SimpleTask;->cancel(Z)Z

    :cond_0
    sget-object v1, Lcom/android/incallui/util/SimpleTask;->mPendingTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/incallui/util/SimpleTask;->mPendingTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public static dump(Ljava/io/PrintWriter;)V
    .locals 8

    const-string/jumbo v4, "------- Dump SimpleTask -------"

    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    sget-object v4, Lcom/android/incallui/util/SimpleTask;->mRunningTasks:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/util/SimpleTask;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SimpleTask:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/incallui/util/SimpleTask;->getStatus()Lcom/android/incallui/util/SimpleTask$Status;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " Tag: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/incallui/util/SimpleTask;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/incallui/util/SimpleTask;->getStartTime()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object v4, Lcom/android/incallui/util/SimpleTask;->mPendingTasks:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayDeque;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/incallui/util/SimpleTask;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SimpleTask:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/incallui/util/SimpleTask;->getStatus()Lcom/android/incallui/util/SimpleTask$Status;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " Tag: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/incallui/util/SimpleTask;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/incallui/util/SimpleTask;->getStartTime()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string/jumbo v4, "------- Dump SimpleTask -------"

    invoke-virtual {p0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method private execute()V
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/android/incallui/util/SimpleTask;->mRunningTasks:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/android/incallui/util/SimpleTask;->insertTask(Ljava/lang/String;Lcom/android/incallui/util/SimpleTask;)V

    sget-object v1, Lcom/android/incallui/util/SimpleTask$Status;->RUNNING:Lcom/android/incallui/util/SimpleTask$Status;

    iput-object v1, p0, Lcom/android/incallui/util/SimpleTask;->mStatus:Lcom/android/incallui/util/SimpleTask$Status;

    iget-object v1, p0, Lcom/android/incallui/util/SimpleTask;->mAsyncTask:Landroid/os/AsyncTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v0}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/android/incallui/util/SimpleTask;->insertPendingTask(Ljava/lang/String;Lcom/android/incallui/util/SimpleTask;)V

    sget-object v0, Lcom/android/incallui/util/SimpleTask$Status;->PENDING:Lcom/android/incallui/util/SimpleTask$Status;

    iput-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mStatus:Lcom/android/incallui/util/SimpleTask$Status;

    goto :goto_0
.end method

.method private finish()V
    .locals 6

    iget-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/incallui/util/SimpleTask;->removeTask(Ljava/lang/String;)V

    const-string/jumbo v0, "SimpleTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Finish: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/incallui/util/SimpleTask;->mStartTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " used time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/incallui/util/SimpleTask;->mStartTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/incallui/util/SimpleTask$Status;->FINISHED:Lcom/android/incallui/util/SimpleTask$Status;

    iput-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mStatus:Lcom/android/incallui/util/SimpleTask$Status;

    return-void
.end method

.method private static getHandler()Landroid/os/Handler;
    .locals 2

    const-class v1, Lcom/android/incallui/util/SimpleTask;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/incallui/util/SimpleTask;->sHandler:Lcom/android/incallui/util/SimpleTask$InternalHandler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/incallui/util/SimpleTask$InternalHandler;

    invoke-direct {v0}, Lcom/android/incallui/util/SimpleTask$InternalHandler;-><init>()V

    sput-object v0, Lcom/android/incallui/util/SimpleTask;->sHandler:Lcom/android/incallui/util/SimpleTask$InternalHandler;

    :cond_0
    sget-object v0, Lcom/android/incallui/util/SimpleTask;->sHandler:Lcom/android/incallui/util/SimpleTask$InternalHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static insertPendingTask(Ljava/lang/String;Lcom/android/incallui/util/SimpleTask;)V
    .locals 2

    sget-object v1, Lcom/android/incallui/util/SimpleTask;->mPendingTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayDeque;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->offer(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->offer(Ljava/lang/Object;)Z

    sget-object v1, Lcom/android/incallui/util/SimpleTask;->mPendingTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static insertTask(Ljava/lang/String;Lcom/android/incallui/util/SimpleTask;)V
    .locals 1

    sget-object v0, Lcom/android/incallui/util/SimpleTask;->mRunningTasks:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static removeTask(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/android/incallui/util/SimpleTask;->mRunningTasks:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/incallui/util/SimpleTask;->mRunningTasks:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {p0}, Lcom/android/incallui/util/SimpleTask;->scheduleNext(Ljava/lang/String;)V

    return-void
.end method

.method private static scheduleNext(Ljava/lang/String;)V
    .locals 4

    sget-object v2, Lcom/android/incallui/util/SimpleTask;->mPendingTasks:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayDeque;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->size()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/util/SimpleTask;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/incallui/util/SimpleTask;->getStatus()Lcom/android/incallui/util/SimpleTask$Status;

    move-result-object v2

    sget-object v3, Lcom/android/incallui/util/SimpleTask$Status;->PENDING:Lcom/android/incallui/util/SimpleTask$Status;

    if-ne v2, v3, :cond_1

    invoke-direct {v0}, Lcom/android/incallui/util/SimpleTask;->execute()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/android/incallui/util/SimpleTask;->scheduleNext(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/android/incallui/util/SimpleTask;->mPendingTasks:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private timeOut(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/incallui/util/SimpleTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mStatus:Lcom/android/incallui/util/SimpleTask$Status;

    sget-object v1, Lcom/android/incallui/util/SimpleTask$Status;->FINISHED:Lcom/android/incallui/util/SimpleTask$Status;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/incallui/util/SimpleTask;->cancel(Z)Z

    iget-boolean v0, p0, Lcom/android/incallui/util/SimpleTask;->isNeedPost:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/incallui/util/SimpleTask;->onPostExecute(Ljava/lang/Object;)V

    :cond_0
    const-string/jumbo v0, "SimpleTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "TimeOut: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/incallui/util/SimpleTask;->mStartTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " used time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/incallui/util/SimpleTask;->mStartTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/incallui/util/SimpleTask$Status;->FINISHED:Lcom/android/incallui/util/SimpleTask$Status;

    iput-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mStatus:Lcom/android/incallui/util/SimpleTask$Status;

    :cond_1
    return-void
.end method


# virtual methods
.method public final cancel(Z)Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mAsyncTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, p1}, Landroid/os/AsyncTask;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method protected abstract doInBackground()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation
.end method

.method public final getStartTime()J
    .locals 2

    iget-wide v0, p0, Lcom/android/incallui/util/SimpleTask;->mStartTime:J

    return-wide v0
.end method

.method public final getStatus()Lcom/android/incallui/util/SimpleTask$Status;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/android/incallui/util/SimpleTask$Status;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mStatus:Lcom/android/incallui/util/SimpleTask$Status;

    return-object v0
.end method

.method public final getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method public final isCancelled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mAsyncTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    return v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    return-void
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "A tag must be provided for not conflicting with other tasks!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/util/SimpleTask;->mStatus:Lcom/android/incallui/util/SimpleTask$Status;

    sget-object v1, Lcom/android/incallui/util/SimpleTask$Status;->NEW:Lcom/android/incallui/util/SimpleTask$Status;

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/android/incallui/util/SimpleTask;->-getcom-android-incallui-util-SimpleTask$StatusSwitchesValues()[I

    move-result-object v0

    iget-object v1, p0, Lcom/android/incallui/util/SimpleTask;->mStatus:Lcom/android/incallui/util/SimpleTask$Status;

    invoke-virtual {v1}, Lcom/android/incallui/util/SimpleTask$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/incallui/util/SimpleTask;->mStartTime:J

    const-string/jumbo v0, "SimpleTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Start: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/incallui/util/SimpleTask;->mStartTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/incallui/util/SimpleTask;->execute()V

    return-void

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot execute task: the task is already in pending."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot execute task: the task is already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public run(JZLjava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZTResult;)V"
        }
    .end annotation

    iput-boolean p3, p0, Lcom/android/incallui/util/SimpleTask;->isNeedPost:Z

    invoke-static {}, Lcom/android/incallui/util/SimpleTask;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/incallui/util/SimpleTask$SimpleTaskResult;

    invoke-direct {v2, p0, p4}, Lcom/android/incallui/util/SimpleTask$SimpleTaskResult;-><init>(Lcom/android/incallui/util/SimpleTask;Ljava/lang/Object;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {}, Lcom/android/incallui/util/SimpleTask;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    invoke-virtual {p0}, Lcom/android/incallui/util/SimpleTask;->run()V

    return-void
.end method

.method public withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/android/incallui/util/SimpleTask",
            "<TResult;>;"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/incallui/util/SimpleTask;->mTag:Ljava/lang/String;

    return-object p0
.end method
