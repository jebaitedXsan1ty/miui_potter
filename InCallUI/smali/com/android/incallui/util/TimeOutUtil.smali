.class public Lcom/android/incallui/util/TimeOutUtil;
.super Ljava/lang/Object;
.source "TimeOutUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/util/TimeOutUtil$1;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/android/incallui/util/TimeOutUtil;


# instance fields
.field private mCacheId:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static synthetic -get0(Lcom/android/incallui/util/TimeOutUtil;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/incallui/util/TimeOutUtil;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/incallui/util/TimeOutUtil;->disconnectCall(ILjava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/incallui/util/TimeOutUtil;

    invoke-direct {v0}, Lcom/android/incallui/util/TimeOutUtil;-><init>()V

    sput-object v0, Lcom/android/incallui/util/TimeOutUtil;->sInstance:Lcom/android/incallui/util/TimeOutUtil;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    new-instance v0, Lcom/android/incallui/util/TimeOutUtil$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/incallui/util/TimeOutUtil$1;-><init>(Lcom/android/incallui/util/TimeOutUtil;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private clearMessages(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method private disconnectCall(ILjava/lang/String;)V
    .locals 8

    iget-object v1, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/incallui/CallList;->getCallById(Ljava/lang/String;)Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/incallui/TelecomAdapter;->getInstance()Lcom/android/incallui/TelecomAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/android/incallui/TelecomAdapter;->disconnectCall(Ljava/lang/String;)V

    const/4 v1, 0x2

    if-ge p1, v1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    add-int/lit8 v3, p1, 0x1

    const/16 v4, 0x64

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v3, v5, p2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    add-int/lit8 v3, p1, 0x1

    int-to-long v4, v3

    const-wide/16 v6, 0x5dc

    mul-long/2addr v4, v6

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    const-string/jumbo v1, "TimeOutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Have tried disconnect call three times, not try again!!! callId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getInstance()Lcom/android/incallui/util/TimeOutUtil;
    .locals 1

    sget-object v0, Lcom/android/incallui/util/TimeOutUtil;->sInstance:Lcom/android/incallui/util/TimeOutUtil;

    return-object v0
.end method


# virtual methods
.method public onCallDisconnect(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/android/incallui/util/TimeOutUtil;->clearMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public onIncomingCallStateChange(Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/incallui/CallList;->getCallById(Ljava/lang/String;)Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    invoke-static {v1}, Lcom/android/incallui/Call$State;->isRinging(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    const-string/jumbo v1, "TimeOutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "clearAllMessages incomingCallId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/incallui/util/TimeOutUtil;->clearMessages(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/android/incallui/util/CallStatisticUtil;->getInstance()Lcom/android/incallui/util/CallStatisticUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/incallui/util/CallStatisticUtil;->onIncomingCallStateChange(Lcom/android/incallui/Call;)V

    :cond_1
    return-void
.end method

.method public onSuppServiceFailed(I)V
    .locals 2

    invoke-static {}, Lcom/android/incallui/util/Utils$SuppService;->values()[Lcom/android/incallui/util/Utils$SuppService;

    move-result-object v1

    aget-object v0, v1, p1

    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->SWITCH:Lcom/android/incallui/util/Utils$SuppService;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/android/incallui/util/Utils$SuppService;->REJECT:Lcom/android/incallui/util/Utils$SuppService;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/incallui/util/TimeOutUtil;->clearMessages(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public startAnswerCallTimeOut(Ljava/lang/String;)V
    .locals 4

    const-string/jumbo v0, "TimeOutUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startAnswerCallTimeOut callId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallPresenter;->setAnswering(Z)V

    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    invoke-static {}, Lcom/android/incallui/util/CallStatisticUtil;->getInstance()Lcom/android/incallui/util/CallStatisticUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/incallui/util/CallStatisticUtil;->startAnswerCallTimeOut(Ljava/lang/String;)V

    return-void
.end method

.method public startEndCallTimeOut(Ljava/lang/String;)V
    .locals 5

    const-string/jumbo v0, "TimeOutUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startEndCallTimeOut callId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public startRejectCallTimeOut(Ljava/lang/String;)V
    .locals 4

    const-string/jumbo v0, "TimeOutUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startRejectCallTimeOut callId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/incallui/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mCacheId:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/android/incallui/InCallPresenter;->getInstance()Lcom/android/incallui/InCallPresenter;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/incallui/InCallPresenter;->setRejecting(Z)V

    iget-object v0, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/incallui/util/TimeOutUtil;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x66

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    invoke-static {}, Lcom/android/incallui/util/CallStatisticUtil;->getInstance()Lcom/android/incallui/util/CallStatisticUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/incallui/util/CallStatisticUtil;->startRejectCallTimeOut(Ljava/lang/String;)V

    return-void
.end method
