.class public Lcom/android/incallui/util/CallStatisticUtil;
.super Ljava/lang/Object;
.source "CallStatisticUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/util/CallStatisticUtil$1;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/android/incallui/util/CallStatisticUtil;


# instance fields
.field private mAnswerRejectCache:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDisconnectingTimeCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mUserRetryTimes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mUserWaitTime:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0(Lcom/android/incallui/util/CallStatisticUtil;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/incallui/util/CallStatisticUtil;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mDisconnectingTimeCache:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/incallui/util/CallStatisticUtil;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/incallui/util/CallStatisticUtil;->removeAnswerRejectCache(Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/incallui/util/CallStatisticUtil;

    invoke-direct {v0}, Lcom/android/incallui/util/CallStatisticUtil;-><init>()V

    sput-object v0, Lcom/android/incallui/util/CallStatisticUtil;->sInstance:Lcom/android/incallui/util/CallStatisticUtil;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/incallui/util/CallStatisticUtil$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/incallui/util/CallStatisticUtil$1;-><init>(Lcom/android/incallui/util/CallStatisticUtil;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mDisconnectingTimeCache:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserRetryTimes:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserWaitTime:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/incallui/util/CallStatisticUtil;
    .locals 1

    sget-object v0, Lcom/android/incallui/util/CallStatisticUtil;->sInstance:Lcom/android/incallui/util/CallStatisticUtil;

    return-object v0
.end method

.method private removeAnswerRejectCache(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserWaitTime:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserRetryTimes:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private updateRetryTimes(Ljava/lang/String;)V
    .locals 4

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserRetryTimes:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserRetryTimes:Ljava/util/HashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserWaitTime:Ljava/util/HashMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserRetryTimes:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserRetryTimes:Ljava/util/HashMap;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public onCallDisconnect(Ljava/lang/String;Z)V
    .locals 6

    const-wide/16 v4, 0x32

    invoke-static {p2}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordIsVideoCrbt(Z)V

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mDisconnectingTimeCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mDisconnectingTimeCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    div-long/2addr v0, v4

    mul-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordTimeOfDisconnectingToDisconnectedCountEvent(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mDisconnectingTimeCache:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    return-void
.end method

.method public onIncomingCallStateChange(Lcom/android/incallui/Call;)V
    .locals 4

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserWaitTime:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserRetryTimes:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/android/incallui/util/CallStatisticUtil;->recordTimeAndTimesOfAnswer(Ljava/lang/String;JI)V

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/android/incallui/util/CallStatisticUtil;->removeAnswerRejectCache(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/android/incallui/Call;->getState()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserWaitTime:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mUserRetryTimes:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/android/incallui/util/CallStatisticUtil;->recordTimeAndTimesOfReject(Ljava/lang/String;JI)V

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x66

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public recordDisconnectCause(Landroid/telecom/DisconnectCause;)V
    .locals 1

    invoke-virtual {p1}, Landroid/telecom/DisconnectCause;->getCode()I

    move-result v0

    invoke-static {v0}, Lcom/android/incallui/CallUtils;->translateDisconnectCauseToString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordDisconnectCauseCountEvent(Ljava/lang/String;)V

    return-void
.end method

.method public recordTimeAndTimesOfAnswer(Ljava/lang/String;JI)V
    .locals 4

    const-wide/16 v2, 0x32

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    div-long/2addr v0, v2

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordTimeOfAnswer(Ljava/lang/String;)V

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordTimesOfIncomingToActive(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    return-void
.end method

.method public recordTimeAndTimesOfReject(Ljava/lang/String;JI)V
    .locals 4

    const-wide/16 v2, 0x32

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    div-long/2addr v0, v2

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordTimeOfReject(Ljava/lang/String;)V

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordTimesOfIncomingToDisconnected(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    return-void
.end method

.method public recordTimeOfInComingToInCall(Lcom/android/incallui/InCallPresenter$InCallState;Lcom/android/incallui/InCallPresenter$InCallState;)V
    .locals 6

    sget-object v1, Lcom/android/incallui/InCallPresenter$InCallState;->INCOMING:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p1, v1, :cond_0

    sget-object v1, Lcom/android/incallui/InCallPresenter$InCallState;->INCALL:Lcom/android/incallui/InCallPresenter$InCallState;

    if-ne p2, v1, :cond_0

    invoke-static {}, Lcom/android/incallui/CallList;->getInstance()Lcom/android/incallui/CallList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/incallui/CallList;->getFirstCall()Lcom/android/incallui/Call;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/android/incallui/Call;->getCallCreationTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/incallui/util/MiStatInterfaceUtil;->recordTimeOfInComingToInCallCountEvent(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public startAnswerCallTimeOut(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/android/incallui/util/CallStatisticUtil;->updateRetryTimes(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public startEndCall(Ljava/lang/String;)V
    .locals 4

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mDisconnectingTimeCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mDisconnectingTimeCache:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public startRejectCallTimeOut(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/android/incallui/util/CallStatisticUtil;->updateRetryTimes(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mAnswerRejectCache:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/incallui/util/CallStatisticUtil;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x66

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method
