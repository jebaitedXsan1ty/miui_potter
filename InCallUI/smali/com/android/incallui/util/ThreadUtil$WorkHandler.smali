.class Lcom/android/incallui/util/ThreadUtil$WorkHandler;
.super Landroid/os/HandlerThread;
.source "ThreadUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/util/ThreadUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WorkHandler"
.end annotation


# static fields
.field private static INSTANCE:Lcom/android/incallui/util/ThreadUtil$WorkHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/incallui/util/ThreadUtil$WorkHandler;

    invoke-direct {v0}, Lcom/android/incallui/util/ThreadUtil$WorkHandler;-><init>()V

    sput-object v0, Lcom/android/incallui/util/ThreadUtil$WorkHandler;->INSTANCE:Lcom/android/incallui/util/ThreadUtil$WorkHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string/jumbo v0, "WorkHandler"

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/android/incallui/util/ThreadUtil$WorkHandler;->start()V

    return-void
.end method

.method public static getInstance()Lcom/android/incallui/util/ThreadUtil$WorkHandler;
    .locals 1

    sget-object v0, Lcom/android/incallui/util/ThreadUtil$WorkHandler;->INSTANCE:Lcom/android/incallui/util/ThreadUtil$WorkHandler;

    return-object v0
.end method
