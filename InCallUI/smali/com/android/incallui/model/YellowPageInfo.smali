.class public Lcom/android/incallui/model/YellowPageInfo;
.super Ljava/lang/Object;
.source "YellowPageInfo.java"


# instance fields
.field private mCid:I

.field private mCreditImg:Ljava/lang/String;

.field private mIsAntispam:Z

.field private mIsMarkedSuspect:Z

.field private mIsUserMarked:Z

.field private mIsYellowPage:Z

.field private mMarkedCount:I

.field private mProviderIcon:Landroid/graphics/Bitmap;

.field private mProviderName:Ljava/lang/String;

.field private mSlogan:Ljava/lang/String;

.field private mTag:Ljava/lang/String;

.field private mYpId:J

.field private mYpName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lmiui/yellowpage/YellowPagePhone;)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->isAntispam()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->getNumberType()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mIsAntispam:Z

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->isUserMarked()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mIsUserMarked:Z

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->isYellowPage()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mIsYellowPage:Z

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->isMarkedSuspect()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mIsMarkedSuspect:Z

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->getMarkedCount()I

    move-result v1

    iput v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mMarkedCount:I

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->getCid()I

    move-result v1

    iput v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mCid:I

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->getYellowPageId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/incallui/model/YellowPageInfo;->mYpId:J

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->getYellowPageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mYpName:Ljava/lang/String;

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->getTag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mTag:Ljava/lang/String;

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->getSlogan()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mSlogan:Ljava/lang/String;

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->getCreditImg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mCreditImg:Ljava/lang/String;

    invoke-static {}, Lcom/android/incallui/InCallApp;->getInstance()Lcom/android/incallui/InCallApp;

    move-result-object v1

    invoke-virtual {p1}, Lmiui/yellowpage/YellowPagePhone;->getProviderId()I

    move-result v2

    invoke-static {v1, v2}, Lmiui/yellowpage/YellowPageUtils;->getProvider(Landroid/content/Context;I)Lmiui/yellowpage/YellowPageProvider;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmiui/yellowpage/YellowPageProvider;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mProviderIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Lmiui/yellowpage/YellowPageProvider;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/incallui/model/YellowPageInfo;->mProviderName:Ljava/lang/String;

    :cond_1
    return-void
.end method


# virtual methods
.method public getCid()I
    .locals 1

    iget v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mCid:I

    return v0
.end method

.method public getCreditImg()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mCreditImg:Ljava/lang/String;

    return-object v0
.end method

.method public getMarkedCount()I
    .locals 1

    iget v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mMarkedCount:I

    return v0
.end method

.method public getProviderIcon()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mProviderIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mProviderName:Ljava/lang/String;

    return-object v0
.end method

.method public getSlogan()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mSlogan:Ljava/lang/String;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method public getYellowPageId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mYpId:J

    return-wide v0
.end method

.method public getYellowPageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mYpName:Ljava/lang/String;

    return-object v0
.end method

.method public isAntispam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mIsAntispam:Z

    return v0
.end method

.method public isMarkedSuspect()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mIsMarkedSuspect:Z

    return v0
.end method

.method public isUserMarked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mIsUserMarked:Z

    return v0
.end method

.method public isYellowPage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/incallui/model/YellowPageInfo;->mIsYellowPage:Z

    return v0
.end method
