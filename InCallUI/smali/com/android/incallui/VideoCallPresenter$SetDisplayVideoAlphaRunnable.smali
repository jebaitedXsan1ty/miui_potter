.class Lcom/android/incallui/VideoCallPresenter$SetDisplayVideoAlphaRunnable;
.super Ljava/lang/Object;
.source "VideoCallPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/VideoCallPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetDisplayVideoAlphaRunnable"
.end annotation


# instance fields
.field private mCall:Lcom/android/incallui/Call;

.field final synthetic this$0:Lcom/android/incallui/VideoCallPresenter;


# direct methods
.method public constructor <init>(Lcom/android/incallui/VideoCallPresenter;Lcom/android/incallui/Call;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/VideoCallPresenter$SetDisplayVideoAlphaRunnable;->this$0:Lcom/android/incallui/VideoCallPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/incallui/VideoCallPresenter$SetDisplayVideoAlphaRunnable;->mCall:Lcom/android/incallui/Call;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter$SetDisplayVideoAlphaRunnable;->this$0:Lcom/android/incallui/VideoCallPresenter;

    invoke-virtual {v1}, Lcom/android/incallui/VideoCallPresenter;->getUi()Lcom/android/incallui/Ui;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter$SetDisplayVideoAlphaRunnable;->mCall:Lcom/android/incallui/Call;

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Lcom/android/incallui/VideoCallPresenter$VideoCallUi;->setDisplayVideoAlpha(F)V

    iget-object v1, p0, Lcom/android/incallui/VideoCallPresenter$SetDisplayVideoAlphaRunnable;->mCall:Lcom/android/incallui/Call;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/incallui/Call;->setDelayDisplayVideoShown(Z)V

    return-void
.end method
