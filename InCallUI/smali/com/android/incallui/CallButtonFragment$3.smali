.class Lcom/android/incallui/CallButtonFragment$3;
.super Lcom/android/incallui/util/SimpleTask;
.source "CallButtonFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/CallButtonFragment;->checkToShowPanel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/util/SimpleTask",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/CallButtonFragment;


# direct methods
.method constructor <init>(Lcom/android/incallui/CallButtonFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-direct {p0}, Lcom/android/incallui/util/SimpleTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected doInBackground()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonPresenter;->isServiceNumber()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallButtonFragment$3;->doInBackground()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0}, Lcom/android/incallui/CallButtonFragment;->-get3(Lcom/android/incallui/CallButtonFragment;)Lcom/android/incallui/InCallActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonPresenter;->isVideoCall()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonFragment;->getPresenter()Lcom/android/incallui/Presenter;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/CallButtonPresenter;

    invoke-virtual {v0}, Lcom/android/incallui/CallButtonPresenter;->isVideoCall()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0}, Lcom/android/incallui/CallButtonFragment;->-get1(Lcom/android/incallui/CallButtonFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0}, Lcom/android/incallui/CallButtonFragment;->-get3(Lcom/android/incallui/CallButtonFragment;)Lcom/android/incallui/InCallActivity;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/android/incallui/InCallActivity;->displayToolPanel(ZZ)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0}, Lcom/android/incallui/CallButtonFragment;->-get3(Lcom/android/incallui/CallButtonFragment;)Lcom/android/incallui/InCallActivity;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Lcom/android/incallui/InCallActivity;->displayDialpad(ZZ)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0, v1}, Lcom/android/incallui/CallButtonFragment;->-wrap2(Lcom/android/incallui/CallButtonFragment;Z)V

    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0}, Lcom/android/incallui/CallButtonFragment;->-get3(Lcom/android/incallui/CallButtonFragment;)Lcom/android/incallui/InCallActivity;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/android/incallui/InCallActivity;->displayDialpad(ZZ)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0}, Lcom/android/incallui/CallButtonFragment;->-get3(Lcom/android/incallui/CallButtonFragment;)Lcom/android/incallui/InCallActivity;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Lcom/android/incallui/InCallActivity;->displayToolPanel(ZZ)V

    iget-object v0, p0, Lcom/android/incallui/CallButtonFragment$3;->this$0:Lcom/android/incallui/CallButtonFragment;

    invoke-static {v0, v2}, Lcom/android/incallui/CallButtonFragment;->-wrap2(Lcom/android/incallui/CallButtonFragment;Z)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/incallui/CallButtonFragment$3;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
