.class Lcom/android/incallui/recorder/CallRecorder$3;
.super Ljava/lang/Object;
.source "CallRecorder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/recorder/CallRecorder;->notifyRecording(Landroid/content/Context;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/recorder/CallRecorder;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$duration:J

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/incallui/recorder/CallRecorder;Landroid/content/Context;Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/CallRecorder$3;->this$0:Lcom/android/incallui/recorder/CallRecorder;

    iput-object p2, p0, Lcom/android/incallui/recorder/CallRecorder$3;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/incallui/recorder/CallRecorder$3;->val$path:Ljava/lang/String;

    iput-wide p4, p0, Lcom/android/incallui/recorder/CallRecorder$3;->val$duration:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    new-instance v0, Lcom/android/incallui/recorder/CallRecorder$3$1;

    iget-object v2, p0, Lcom/android/incallui/recorder/CallRecorder$3;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/incallui/recorder/CallRecorder$3;->val$path:Ljava/lang/String;

    iget-wide v4, p0, Lcom/android/incallui/recorder/CallRecorder$3;->val$duration:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/incallui/recorder/CallRecorder$3$1;-><init>(Lcom/android/incallui/recorder/CallRecorder$3;Landroid/content/Context;Ljava/lang/String;J)V

    const-string/jumbo v1, "task_notify_record"

    invoke-virtual {v0, v1}, Lcom/android/incallui/recorder/CallRecorder$3$1;->withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;

    move-result-object v0

    const-wide/16 v2, 0x1388

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/android/incallui/util/SimpleTask;->run(JZLjava/lang/Object;)V

    return-void
.end method
