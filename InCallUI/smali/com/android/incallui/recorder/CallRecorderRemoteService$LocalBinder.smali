.class public final Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;
.super Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;
.source "CallRecorderRemoteService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/recorder/CallRecorderRemoteService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LocalBinder"
.end annotation


# instance fields
.field private reference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/incallui/recorder/CallRecorderRemoteService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/incallui/recorder/CallRecorderRemoteService;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;->reference:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public checkCallRecordTime(Ljava/lang/String;)J
    .locals 8

    const-wide/16 v6, -0x1

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;->getService()Lcom/android/incallui/recorder/CallRecorderRemoteService;

    move-result-object v2

    if-nez v2, :cond_0

    const-string/jumbo v3, "CallRecorderRemoteService"

    const-string/jumbo v4, "checkCallRecordTime service is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-wide v6

    :cond_0
    invoke-static {v2}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get1(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {v2}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/recorder/CallRecorder;->isCallRecording()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/incallui/recorder/CallRecorder;->getRecordingTimeInMillis()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->onCallRecordTimeCheck(J)V

    return-wide v0

    :cond_1
    return-wide v6

    :cond_2
    return-wide v6
.end method

.method public getService()Lcom/android/incallui/recorder/CallRecorderRemoteService;
    .locals 1

    iget-object v0, p0, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;->reference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/incallui/recorder/CallRecorderRemoteService;

    return-object v0
.end method

.method public isBound()Z
    .locals 3

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;->getService()Lcom/android/incallui/recorder/CallRecorderRemoteService;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v1, "CallRecorderRemoteService"

    const-string/jumbo v2, "isBound service is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    return v1

    :cond_0
    invoke-static {v0}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get0(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Z

    move-result v1

    return v1
.end method

.method public registerCallback(Lcom/android/incallui/recorder/IRecorderCallback;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;->getService()Lcom/android/incallui/recorder/CallRecorderRemoteService;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v1, "CallRecorderRemoteService"

    const-string/jumbo v2, "registerCallback service is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {v0}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get3(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    :cond_1
    return-void
.end method

.method public startCallRecord(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;->getService()Lcom/android/incallui/recorder/CallRecorderRemoteService;

    move-result-object v4

    if-nez v4, :cond_0

    const-string/jumbo v5, "CallRecorderRemoteService"

    const-string/jumbo v6, "startCallRecord service is null"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-static {v4, p1}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-set0(Lcom/android/incallui/recorder/CallRecorderRemoteService;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v4}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;

    move-result-object v5

    if-nez v5, :cond_1

    const-string/jumbo v5, "CallRecorderRemoteService"

    const-string/jumbo v6, "can\'t record now"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v4}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/incallui/recorder/CallRecorder;->isCallRecording()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v4}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Lcom/android/incallui/recorder/CallRecorder;->startCallRecord(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :goto_0
    return-void

    :cond_2
    invoke-static {v4}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/incallui/recorder/CallRecorder;->getRecordingTimeInMillis()J

    move-result-wide v2

    const-string/jumbo v5, "CallRecorderRemoteService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "get duration when start record:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4, v2, v3}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->onCallRecordTimeCheck(J)V

    goto :goto_0
.end method

.method public stopCallRecord()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;->getService()Lcom/android/incallui/recorder/CallRecorderRemoteService;

    move-result-object v1

    if-nez v1, :cond_0

    const-string/jumbo v2, "CallRecorderRemoteService"

    const-string/jumbo v3, "stopCallRecord service is null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/recorder/CallRecorder;->isCallRecording()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get2(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Lcom/android/incallui/recorder/CallRecorder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/incallui/recorder/CallRecorder;->stopCallRecord()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v2, "CallRecorderRemoteService"

    const-string/jumbo v3, "stop call record error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public unregisterCallback(Lcom/android/incallui/recorder/IRecorderCallback;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/incallui/recorder/CallRecorderRemoteService$LocalBinder;->getService()Lcom/android/incallui/recorder/CallRecorderRemoteService;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v1, "CallRecorderRemoteService"

    const-string/jumbo v2, "unregisterCallback service is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {v0}, Lcom/android/incallui/recorder/CallRecorderRemoteService;->-get3(Lcom/android/incallui/recorder/CallRecorderRemoteService;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_1
    return-void
.end method
