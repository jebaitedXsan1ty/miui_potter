.class Lcom/android/incallui/recorder/RecorderNotificationMgr$1;
.super Ljava/lang/Object;
.source "RecorderNotificationMgr.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/recorder/RecorderNotificationMgr;->updateCallRecordNotification(ZLjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/incallui/recorder/RecorderNotificationMgr;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$message:Ljava/lang/String;

.field final synthetic val$success:Z


# direct methods
.method constructor <init>(Lcom/android/incallui/recorder/RecorderNotificationMgr;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/recorder/RecorderNotificationMgr$1;->this$0:Lcom/android/incallui/recorder/RecorderNotificationMgr;

    iput-boolean p2, p0, Lcom/android/incallui/recorder/RecorderNotificationMgr$1;->val$success:Z

    iput-object p3, p0, Lcom/android/incallui/recorder/RecorderNotificationMgr$1;->val$message:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/incallui/recorder/RecorderNotificationMgr$1;->val$fileName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    new-instance v0, Lcom/android/incallui/recorder/RecorderNotificationMgr$1$1;

    iget-boolean v1, p0, Lcom/android/incallui/recorder/RecorderNotificationMgr$1;->val$success:Z

    iget-object v2, p0, Lcom/android/incallui/recorder/RecorderNotificationMgr$1;->val$message:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/incallui/recorder/RecorderNotificationMgr$1;->val$fileName:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/android/incallui/recorder/RecorderNotificationMgr$1$1;-><init>(Lcom/android/incallui/recorder/RecorderNotificationMgr$1;ZLjava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "task_update_record_notification"

    invoke-virtual {v0, v1}, Lcom/android/incallui/recorder/RecorderNotificationMgr$1$1;->withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;

    move-result-object v0

    const-wide/16 v2, 0x1388

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/android/incallui/util/SimpleTask;->run(JZLjava/lang/Object;)V

    return-void
.end method
