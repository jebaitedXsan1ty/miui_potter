.class public abstract Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;
.super Landroid/os/Binder;
.source "ICallRecorderCommand.java"

# interfaces
.implements Lcom/android/incallui/recorder/ICallRecorderCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/incallui/recorder/ICallRecorderCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/incallui/recorder/ICallRecorderCommand$Stub$Proxy;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string/jumbo v0, "com.android.incallui.recorder.ICallRecorderCommand"

    invoke-virtual {p0, p0, v0}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/android/incallui/recorder/ICallRecorderCommand;
    .locals 2

    const/4 v1, 0x0

    if-nez p0, :cond_0

    return-object v1

    :cond_0
    const-string/jumbo v1, "com.android.incallui.recorder.ICallRecorderCommand"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/android/incallui/recorder/ICallRecorderCommand;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/android/incallui/recorder/ICallRecorderCommand;

    return-object v0

    :cond_1
    new-instance v1, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub$Proxy;

    invoke-direct {v1, p0}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v8, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    return v7

    :sswitch_0
    const-string/jumbo v7, "com.android.incallui.recorder.ICallRecorderCommand"

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return v8

    :sswitch_1
    const-string/jumbo v7, "com.android.incallui.recorder.ICallRecorderCommand"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;->startCallRecord(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v8

    :sswitch_2
    const-string/jumbo v7, "com.android.incallui.recorder.ICallRecorderCommand"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;->stopCallRecord()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v8

    :sswitch_3
    const-string/jumbo v7, "com.android.incallui.recorder.ICallRecorderCommand"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;->checkCallRecordTime(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    return v8

    :sswitch_4
    const-string/jumbo v7, "com.android.incallui.recorder.ICallRecorderCommand"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/android/incallui/recorder/IRecorderCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/incallui/recorder/IRecorderCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;->registerCallback(Lcom/android/incallui/recorder/IRecorderCallback;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v8

    :sswitch_5
    const-string/jumbo v7, "com.android.incallui.recorder.ICallRecorderCommand"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/android/incallui/recorder/IRecorderCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/incallui/recorder/IRecorderCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;->unregisterCallback(Lcom/android/incallui/recorder/IRecorderCallback;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    return v8

    :sswitch_6
    const-string/jumbo v7, "com.android.incallui.recorder.ICallRecorderCommand"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/incallui/recorder/ICallRecorderCommand$Stub;->isBound()Z

    move-result v6

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v6, :cond_0

    move v7, v8

    :goto_0
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    return v8

    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
