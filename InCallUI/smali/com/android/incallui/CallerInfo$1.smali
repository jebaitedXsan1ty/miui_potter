.class final Lcom/android/incallui/CallerInfo$1;
.super Lcom/android/incallui/util/SimpleTask;
.source "CallerInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/incallui/CallerInfo;->doYellowPageQuery(Landroid/content/Context;Ljava/lang/String;Lcom/android/incallui/CallerInfo;ZLcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;ILjava/lang/Object;)Lcom/android/incallui/CallerInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/incallui/util/SimpleTask",
        "<",
        "Lcom/android/incallui/model/YellowPageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$cookie:Ljava/lang/Object;

.field final synthetic val$isIncoming:Z

.field final synthetic val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

.field final synthetic val$number:Ljava/lang/String;

.field final synthetic val$previousResult:Lcom/android/incallui/CallerInfo;

.field final synthetic val$token:I


# direct methods
.method constructor <init>(Lcom/android/incallui/CallerInfo;Landroid/content/Context;Ljava/lang/String;Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;ILjava/lang/Object;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/incallui/CallerInfo$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    iput-object p2, p0, Lcom/android/incallui/CallerInfo$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/incallui/CallerInfo$1;->val$number:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/incallui/CallerInfo$1;->val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

    iput p5, p0, Lcom/android/incallui/CallerInfo$1;->val$token:I

    iput-object p6, p0, Lcom/android/incallui/CallerInfo$1;->val$cookie:Ljava/lang/Object;

    iput-boolean p7, p0, Lcom/android/incallui/CallerInfo$1;->val$isIncoming:Z

    invoke-direct {p0}, Lcom/android/incallui/util/SimpleTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected doInBackground()Lcom/android/incallui/model/YellowPageInfo;
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/android/incallui/CallerInfo$1;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/incallui/CallerInfo$1;->val$number:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/incallui/CallerInfo$1;->val$context:Landroid/content/Context;

    invoke-static {v4}, Lmiui/yellowpage/YellowPageUtils;->isYellowPageEnable(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v2, v3, v4}, Lmiui/yellowpage/YellowPageUtils;->getPhoneInfo(Landroid/content/Context;Ljava/lang/String;Z)Lmiui/yellowpage/YellowPagePhone;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    new-instance v2, Lcom/android/incallui/model/YellowPageInfo;

    invoke-direct {v2, v1}, Lcom/android/incallui/model/YellowPageInfo;-><init>(Lmiui/yellowpage/YellowPagePhone;)V

    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string/jumbo v2, "doYellowPageQuery: getPhoneInfo has error"

    invoke-static {p0, v2}, Lcom/android/incallui/Log;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-object v5
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/incallui/CallerInfo$1;->doInBackground()Lcom/android/incallui/model/YellowPageInfo;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/android/incallui/model/YellowPageInfo;)V
    .locals 9

    iget-object v0, p0, Lcom/android/incallui/CallerInfo$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    const/4 v1, 0x2

    iput v1, v0, Lcom/android/incallui/CallerInfo;->queryState:I

    iget-object v0, p0, Lcom/android/incallui/CallerInfo$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    iput-object p1, v0, Lcom/android/incallui/CallerInfo;->yellowPageInfo:Lcom/android/incallui/model/YellowPageInfo;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/incallui/model/YellowPageInfo;->isYellowPage()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/incallui/CallerInfo$1$1;

    iget-object v2, p0, Lcom/android/incallui/CallerInfo$1;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/incallui/CallerInfo$1;->val$number:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/android/incallui/CallerInfo$1;->val$isIncoming:Z

    iget-object v6, p0, Lcom/android/incallui/CallerInfo$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    iget-object v7, p0, Lcom/android/incallui/CallerInfo$1;->val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

    iget-object v8, p0, Lcom/android/incallui/CallerInfo$1;->val$cookie:Ljava/lang/Object;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/android/incallui/CallerInfo$1$1;-><init>(Lcom/android/incallui/CallerInfo$1;Landroid/content/Context;Lcom/android/incallui/model/YellowPageInfo;Ljava/lang/String;ZLcom/android/incallui/CallerInfo;Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;Ljava/lang/Object;)V

    const-string/jumbo v1, "task_query_yellow_page_avatar"

    invoke-virtual {v0, v1}, Lcom/android/incallui/CallerInfo$1$1;->withTag(Ljava/lang/String;)Lcom/android/incallui/util/SimpleTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/incallui/util/SimpleTask;->run()V

    :cond_0
    iget-object v0, p0, Lcom/android/incallui/CallerInfo$1;->val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/incallui/CallerInfo$1;->val$listener:Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;

    iget v1, p0, Lcom/android/incallui/CallerInfo$1;->val$token:I

    iget-object v2, p0, Lcom/android/incallui/CallerInfo$1;->val$cookie:Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/incallui/CallerInfo$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    invoke-interface {v0, v1, v2, v3}, Lcom/android/incallui/CallerInfoAsyncQuery$OnQueryCompleteListener;->onQueryComplete(ILjava/lang/Object;Lcom/android/incallui/CallerInfo;)V

    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/incallui/model/YellowPageInfo;

    invoke-virtual {p0, p1}, Lcom/android/incallui/CallerInfo$1;->onPostExecute(Lcom/android/incallui/model/YellowPageInfo;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/android/incallui/CallerInfo$1;->val$previousResult:Lcom/android/incallui/CallerInfo;

    const/4 v1, 0x1

    iput v1, v0, Lcom/android/incallui/CallerInfo;->queryState:I

    return-void
.end method
