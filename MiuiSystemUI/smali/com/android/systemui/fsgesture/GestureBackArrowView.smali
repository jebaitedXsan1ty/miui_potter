.class public Lcom/android/systemui/fsgesture/GestureBackArrowView;
.super Landroid/view/View;
.source "GestureBackArrowView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/fsgesture/GestureBackArrowView$1;
    }
.end annotation


# static fields
.field private static final CUBIC_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;


# instance fields
.field private mArrow:Landroid/graphics/Bitmap;

.field private mArrowAnimator:Landroid/animation/ValueAnimator;

.field private mArrowDstRect:Landroid/graphics/Rect;

.field private mArrowHeight:I

.field private mArrowNeedDraw:Z

.field private mArrowPaint:Landroid/graphics/Paint;

.field private mArrowShown:Z

.field private mArrowWidth:I

.field private mBackDstRect:Landroid/graphics/Rect;

.field private mBackHeight:I

.field private mBackWidth:I

.field private mBgPaint:Landroid/graphics/Paint;

.field private mCurArrowAlpha:I

.field private mCurrentY:F

.field private mDisplayWidth:I

.field private mExpectBackHeight:F

.field private mLeftBackground:Landroid/graphics/Bitmap;

.field private mPosition:I

.field private mReadyFinish:Z

.field private mRightBackground:Landroid/graphics/Bitmap;

.field private mScale:F

.field private mStartX:F


# direct methods
.method static synthetic -get0(Lcom/android/systemui/fsgesture/GestureBackArrowView;)Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/systemui/fsgesture/GestureBackArrowView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowNeedDraw:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/systemui/fsgesture/GestureBackArrowView;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mCurArrowAlpha:I

    return p1
.end method

.method static synthetic -set2(Lcom/android/systemui/fsgesture/GestureBackArrowView;F)F
    .locals 0

    iput p1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    return p1
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/systemui/fsgesture/GestureBackArrowView$1;

    invoke-direct {v0}, Lcom/android/systemui/fsgesture/GestureBackArrowView$1;-><init>()V

    sput-object v0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->CUBIC_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/android/systemui/fsgesture/GestureBackArrowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/android/systemui/fsgesture/GestureBackArrowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/systemui/fsgesture/GestureBackArrowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;III)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;III)V
    .locals 7

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    iput p5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mPosition:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBgPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBgPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBgPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setDither(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p0}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020083

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mLeftBackground:Landroid/graphics/Bitmap;

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v3, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mLeftBackground:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mLeftBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mLeftBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mRightBackground:Landroid/graphics/Bitmap;

    iget v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mPosition:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020082

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrow:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrow:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowHeight:I

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrow:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowWidth:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackDstRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowDstRect:Landroid/graphics/Rect;

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mLeftBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackHeight:I

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mLeftBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackWidth:I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mRightBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackHeight:I

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mRightBackground:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackWidth:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private startArrowAnimating(Z)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowAnimator:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [I

    iget v2, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mCurArrowAlpha:I

    aput v2, v1, v0

    if-eqz p1, :cond_1

    const/16 v0, 0xff

    :cond_1
    const/4 v2, 0x1

    aput v0, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowAnimator:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/android/systemui/fsgesture/GestureBackArrowView;->CUBIC_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/systemui/fsgesture/GestureBackArrowView$2;

    invoke-direct {v1, p0, p1}, Lcom/android/systemui/fsgesture/GestureBackArrowView$2;-><init>(Lcom/android/systemui/fsgesture/GestureBackArrowView;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method


# virtual methods
.method onActionDown(FFF)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    iput p3, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mExpectBackHeight:F

    iput p1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mCurrentY:F

    :goto_0
    iput p2, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mStartX:F

    iget-object v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iput-boolean v1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowShown:Z

    iput-boolean v1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowNeedDraw:Z

    return-void

    :cond_0
    iget v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackHeight:I

    int-to-float v0, v0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mExpectBackHeight:F

    const/high16 v0, 0x41a00000    # 20.0f

    sub-float v0, p1, v0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mCurrentY:F

    goto :goto_0
.end method

.method onActionMove(F)V
    .locals 1

    const/high16 v0, 0x41a00000    # 20.0f

    div-float v0, p1, v0

    iput v0, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    invoke-virtual {p0}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->invalidate()V

    return-void
.end method

.method onActionUp(FLandroid/animation/Animator$AnimatorListener;)V
    .locals 4

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowAnimator:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    const/high16 v1, 0x41a00000    # 20.0f

    div-float v1, p1, v1

    iput v1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    const/4 v1, 0x2

    new-array v1, v1, [F

    iget v2, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    const/4 v3, 0x0

    aput v2, v1, v3

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v2, v1, v3

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lcom/android/systemui/fsgesture/GestureBackArrowView$3;

    invoke-direct {v1, p0}, Lcom/android/systemui/fsgesture/GestureBackArrowView$3;-><init>(Lcom/android/systemui/fsgesture/GestureBackArrowView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    if-eqz p2, :cond_1

    invoke-virtual {v0, p2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_1
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/high16 v9, 0x40000000    # 2.0f

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mLeftBackground:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mPosition:I

    packed-switch v5, :pswitch_data_0

    :goto_0
    iget-object v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackDstRect:Landroid/graphics/Rect;

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mCurrentY:F

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mExpectBackHeight:F

    div-float/2addr v7, v9

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mCurrentY:F

    iget v8, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mExpectBackHeight:F

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-virtual {v5, v2, v6, v3, v7}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackDstRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBgPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v12, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-boolean v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mReadyFinish:Z

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowShown:Z

    if-nez v5, :cond_0

    iput-boolean v10, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowNeedDraw:Z

    invoke-direct {p0, v10}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->startArrowAnimating(Z)V

    iput-boolean v10, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowShown:Z

    :cond_0
    :goto_1
    iget-boolean v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowNeedDraw:Z

    if-eqz v5, :cond_1

    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    float-to-double v6, v5

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpl-double v5, v6, v8

    if-lez v5, :cond_1

    iget-object v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowDstRect:Landroid/graphics/Rect;

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mCurrentY:F

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowHeight:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mCurrentY:F

    iget v8, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowHeight:I

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-virtual {v5, v0, v6, v1, v7}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrow:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowDstRect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v12, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    return-void

    :pswitch_0
    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mStartX:F

    float-to-int v2, v5

    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackWidth:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mStartX:F

    add-float/2addr v5, v6

    float-to-int v3, v5

    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mStartX:F

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowWidth:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    div-float/2addr v6, v9

    add-float/2addr v5, v6

    float-to-int v0, v5

    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mStartX:F

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowWidth:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    div-float/2addr v6, v9

    add-float/2addr v5, v6

    float-to-int v1, v5

    goto/16 :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mRightBackground:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mDisplayWidth:I

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mStartX:F

    add-float/2addr v6, v7

    float-to-int v6, v6

    sub-int v2, v5, v6

    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mDisplayWidth:I

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mStartX:F

    float-to-int v6, v6

    sub-int v3, v5, v6

    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mDisplayWidth:I

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mStartX:F

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackWidth:I

    int-to-float v7, v7

    iget v8, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowWidth:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    div-float/2addr v7, v9

    add-float/2addr v6, v7

    float-to-int v6, v6

    sub-int v0, v5, v6

    iget v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mDisplayWidth:I

    iget v6, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mStartX:F

    iget v7, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mBackWidth:I

    int-to-float v7, v7

    iget v8, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowWidth:I

    int-to-float v8, v8

    sub-float/2addr v7, v8

    div-float/2addr v7, v9

    add-float/2addr v6, v7

    float-to-int v6, v6

    sub-int v1, v5, v6

    goto/16 :goto_0

    :cond_2
    iget-boolean v5, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowShown:Z

    if-eqz v5, :cond_0

    invoke-direct {p0, v11}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->startArrowAnimating(Z)V

    iput-boolean v11, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mArrowShown:Z

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method reset()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mScale:F

    const/high16 v0, -0x3b860000    # -1000.0f

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p0, v0, v2, v1}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->onActionDown(FFF)V

    invoke-virtual {p0}, Lcom/android/systemui/fsgesture/GestureBackArrowView;->invalidate()V

    return-void
.end method

.method setDisplayWidth(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mDisplayWidth:I

    return-void
.end method

.method setReadyFinish(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/fsgesture/GestureBackArrowView;->mReadyFinish:Z

    return-void
.end method
