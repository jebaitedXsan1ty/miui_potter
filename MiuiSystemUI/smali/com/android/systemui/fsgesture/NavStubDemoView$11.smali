.class Lcom/android/systemui/fsgesture/NavStubDemoView$11;
.super Ljava/lang/Object;
.source "NavStubDemoView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/fsgesture/NavStubDemoView;->startRecentTaskAnim()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

.field final synthetic val$finalScale:F

.field final synthetic val$initX:I

.field final synthetic val$initY:I


# direct methods
.method constructor <init>(Lcom/android/systemui/fsgesture/NavStubDemoView;FII)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

    iput p2, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->val$finalScale:F

    iput p3, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->val$initX:I

    iput p4, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->val$initY:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8

    const/4 v6, 0x0

    iget-object v5, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v5, v4}, Lcom/android/systemui/fsgesture/NavStubDemoView;->-set2(Lcom/android/systemui/fsgesture/NavStubDemoView;F)F

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, v3

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v4, v5

    float-to-int v0, v4

    iget-object v4, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

    invoke-static {v4}, Lcom/android/systemui/fsgesture/NavStubDemoView;->-get9(Lcom/android/systemui/fsgesture/NavStubDemoView;)Landroid/view/View;

    move-result-object v4

    invoke-static {v0, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

    invoke-static {v4}, Lcom/android/systemui/fsgesture/NavStubDemoView;->-get17(Lcom/android/systemui/fsgesture/NavStubDemoView;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    rsub-int v5, v0, 0xff

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

    invoke-static {v4}, Lcom/android/systemui/fsgesture/NavStubDemoView;->-get18(Lcom/android/systemui/fsgesture/NavStubDemoView;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

    invoke-static {v5}, Lcom/android/systemui/fsgesture/NavStubDemoView;->-get18(Lcom/android/systemui/fsgesture/NavStubDemoView;)I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->val$finalScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    add-float v2, v4, v5

    iget-object v4, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

    iget v5, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->val$initX:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->val$initX:I

    int-to-float v6, v6

    const/4 v7, 0x0

    sub-float v6, v7, v6

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/android/systemui/fsgesture/NavStubDemoView;->-set5(Lcom/android/systemui/fsgesture/NavStubDemoView;I)I

    iget-object v4, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

    iget v5, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->val$initY:I

    int-to-float v5, v5

    iget v6, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->val$initY:I

    int-to-float v6, v6

    sub-float v6, v2, v6

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v4, v5}, Lcom/android/systemui/fsgesture/NavStubDemoView;->-set6(Lcom/android/systemui/fsgesture/NavStubDemoView;I)I

    iget-object v4, p0, Lcom/android/systemui/fsgesture/NavStubDemoView$11;->this$0:Lcom/android/systemui/fsgesture/NavStubDemoView;

    invoke-virtual {v4}, Lcom/android/systemui/fsgesture/NavStubDemoView;->invalidate()V

    return-void
.end method
