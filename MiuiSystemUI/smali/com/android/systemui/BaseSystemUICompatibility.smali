.class public Lcom/android/systemui/BaseSystemUICompatibility;
.super Ljava/lang/Object;
.source "BaseSystemUICompatibility.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static is24HourFormat(Landroid/content/Context;I)Z
    .locals 1

    invoke-static {p0, p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;I)Z

    move-result v0

    return v0
.end method

.method public static visibilityChanged(ZLcom/android/internal/statusbar/IStatusBarService;)V
    .locals 3

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v1, v2}, Lcom/android/internal/statusbar/IStatusBarService;->onPanelRevealed(ZI)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/android/internal/statusbar/IStatusBarService;->onPanelHidden()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
