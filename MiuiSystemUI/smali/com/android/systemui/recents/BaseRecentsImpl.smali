.class public abstract Lcom/android/systemui/recents/BaseRecentsImpl;
.super Ljava/lang/Object;
.source "BaseRecentsImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/recents/BaseRecentsImpl$1;,
        Lcom/android/systemui/recents/BaseRecentsImpl$2;,
        Lcom/android/systemui/recents/BaseRecentsImpl$3;,
        Lcom/android/systemui/recents/BaseRecentsImpl$4;,
        Lcom/android/systemui/recents/BaseRecentsImpl$5;,
        Lcom/android/systemui/recents/BaseRecentsImpl$6;,
        Lcom/android/systemui/recents/BaseRecentsImpl$H;,
        Lcom/android/systemui/recents/BaseRecentsImpl$TaskStackListenerImpl;
    }
.end annotation


# static fields
.field public static mTaskBarHeight:I

.field protected static sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;


# instance fields
.field private isShowing:Z

.field jobExecutor:Ljava/util/concurrent/ExecutorService;

.field private mAm:Landroid/app/ActivityManager;

.field private mCleaningToast:Landroid/widget/Toast;

.field protected mContext:Landroid/content/Context;

.field private mCurType:Ljava/lang/String;

.field mDraggingInRecents:Z

.field protected mDummyStackView:Lcom/android/systemui/recents/views/TaskStackView;

.field mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

.field private mForceImmersiveNavBarListener:Landroid/database/ContentObserver;

.field private mFsgReceiver:Landroid/content/BroadcastReceiver;

.field private mFsgSizeChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

.field private mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

.field protected mHandler:Landroid/os/Handler;

.field mHeaderBar:Lcom/android/systemui/recents/views/TaskViewHeader;

.field final mHeaderBarLock:Ljava/lang/Object;

.field private mIsEnter:Z

.field private mIsInAnotherPro:Z

.field private mIsSizeReset:Z

.field protected mLastToggleTime:J

.field mLaunchedWhileDocking:Z

.field mNavBarHeight:I

.field mNavBarWidth:I

.field private mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

.field private mOneKeyCleaning:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mScreenWidth:I

.field private mShowGestureBackAnimationListener:Landroid/database/ContentObserver;

.field mStatusBarHeight:I

.field mTaskStackBounds:Landroid/graphics/Rect;

.field mTaskStackListener:Lcom/android/systemui/recents/BaseRecentsImpl$TaskStackListenerImpl;

.field protected mThumbTransitionBitmapCache:Landroid/graphics/Bitmap;

.field mTmpTransform:Lcom/android/systemui/recents/views/TaskViewTransform;

.field protected mTriggeredFromAltTab:Z


# direct methods
.method static synthetic -get0(Lcom/android/systemui/recents/BaseRecentsImpl;)Landroid/app/ActivityManager;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mAm:Landroid/app/ActivityManager;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/recents/BaseRecentsImpl;)Lcom/android/systemui/fsgesture/GestureStubView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/recents/BaseRecentsImpl;)Lcom/android/systemui/fsgesture/GestureStubView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/recents/BaseRecentsImpl;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mIsInAnotherPro:Z

    return v0
.end method

.method static synthetic -get4(Lcom/android/systemui/recents/BaseRecentsImpl;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mIsSizeReset:Z

    return v0
.end method

.method static synthetic -get5(Lcom/android/systemui/recents/BaseRecentsImpl;)Lcom/android/systemui/statusbar/phone/NavStubView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/systemui/recents/BaseRecentsImpl;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mScreenWidth:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/systemui/recents/BaseRecentsImpl;Landroid/app/ActivityManager;)Landroid/app/ActivityManager;
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mAm:Landroid/app/ActivityManager;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/systemui/recents/BaseRecentsImpl;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mIsInAnotherPro:Z

    return p1
.end method

.method static synthetic -set2(Lcom/android/systemui/recents/BaseRecentsImpl;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mIsSizeReset:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/recents/BaseRecentsImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->changeVisibility()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/recents/BaseRecentsImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->clearNavStubWindow()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/systemui/recents/BaseRecentsImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->hideBackStubWindow()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/systemui/recents/BaseRecentsImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->showBackStubWindow()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/systemui/recents/BaseRecentsImpl;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/recents/BaseRecentsImpl;->updateFsgStubState(ZLjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v5, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mOneKeyCleaning:Z

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskStackBounds:Landroid/graphics/Rect;

    new-instance v2, Lcom/android/systemui/recents/views/TaskViewTransform;

    invoke-direct {v2}, Lcom/android/systemui/recents/views/TaskViewTransform;-><init>()V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTmpTransform:Lcom/android/systemui/recents/views/TaskViewTransform;

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHeaderBarLock:Ljava/lang/Object;

    new-instance v2, Lcom/android/systemui/recents/misc/DozeTrigger;

    new-instance v3, Lcom/android/systemui/recents/BaseRecentsImpl$1;

    invoke-direct {v3, p0}, Lcom/android/systemui/recents/BaseRecentsImpl$1;-><init>(Lcom/android/systemui/recents/BaseRecentsImpl;)V

    const/16 v4, 0xe1

    invoke-direct {v2, v4, v3}, Lcom/android/systemui/recents/misc/DozeTrigger;-><init>(ILjava/lang/Runnable;)V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

    invoke-static {v6}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->jobExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/android/systemui/recents/BaseRecentsImpl$2;

    invoke-direct {v2, p0}, Lcom/android/systemui/recents/BaseRecentsImpl$2;-><init>(Lcom/android/systemui/recents/BaseRecentsImpl;)V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFsgSizeChangeReceiver:Landroid/content/BroadcastReceiver;

    iput-boolean v5, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mIsInAnotherPro:Z

    new-instance v2, Lcom/android/systemui/recents/BaseRecentsImpl$3;

    invoke-direct {v2, p0}, Lcom/android/systemui/recents/BaseRecentsImpl$3;-><init>(Lcom/android/systemui/recents/BaseRecentsImpl;)V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Lcom/android/systemui/recents/BaseRecentsImpl$4;

    invoke-direct {v2, p0}, Lcom/android/systemui/recents/BaseRecentsImpl$4;-><init>(Lcom/android/systemui/recents/BaseRecentsImpl;)V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFsgReceiver:Landroid/content/BroadcastReceiver;

    iput-boolean v5, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->isShowing:Z

    new-instance v2, Lcom/android/systemui/recents/BaseRecentsImpl$5;

    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v2, p0, v3}, Lcom/android/systemui/recents/BaseRecentsImpl$5;-><init>(Lcom/android/systemui/recents/BaseRecentsImpl;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mForceImmersiveNavBarListener:Landroid/database/ContentObserver;

    new-instance v2, Lcom/android/systemui/recents/BaseRecentsImpl$6;

    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v2, p0, v3}, Lcom/android/systemui/recents/BaseRecentsImpl$6;-><init>(Lcom/android/systemui/recents/BaseRecentsImpl;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mShowGestureBackAnimationListener:Landroid/database/ContentObserver;

    iput-object p1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/systemui/recents/BaseRecentsImpl$H;

    invoke-direct {v2, p0, v7}, Lcom/android/systemui/recents/BaseRecentsImpl$H;-><init>(Lcom/android/systemui/recents/BaseRecentsImpl;Lcom/android/systemui/recents/BaseRecentsImpl$H;)V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    invoke-static {}, Lcom/android/systemui/recents/misc/ForegroundThread;->get()Lcom/android/systemui/recents/misc/ForegroundThread;

    new-instance v2, Lcom/android/systemui/recents/BaseRecentsImpl$TaskStackListenerImpl;

    invoke-direct {v2, p0}, Lcom/android/systemui/recents/BaseRecentsImpl$TaskStackListenerImpl;-><init>(Lcom/android/systemui/recents/BaseRecentsImpl;)V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskStackListener:Lcom/android/systemui/recents/BaseRecentsImpl$TaskStackListenerImpl;

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskStackListener:Lcom/android/systemui/recents/BaseRecentsImpl$TaskStackListenerImpl;

    invoke-virtual {v1, v2}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->registerTaskStackListener(Lcom/android/systemui/recents/misc/SystemServicesProxy$TaskStackListener;)V

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    new-instance v2, Lcom/android/systemui/recents/views/TaskStackView;

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/android/systemui/recents/views/TaskStackView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDummyStackView:Lcom/android/systemui/recents/views/TaskStackView;

    const v2, 0x7f030048

    invoke-virtual {v0, v2, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/recents/views/TaskViewHeader;

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHeaderBar:Lcom/android/systemui/recents/views/TaskViewHeader;

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->reloadResources()V

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->registerMiuiTaskResizeList(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d016c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mCleaningToast:Landroid/widget/Toast;

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->addFsgGestureWindow()V

    return-void
.end method

.method private addFsgGestureWindow()V
    .locals 23

    const/16 v17, 0x1

    :try_start_0
    const-string/jumbo v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/view/IWindowManager;->hasNavigationBar()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    :goto_0
    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/view/WindowManager;

    invoke-interface/range {v22 .. v22}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v15

    new-instance v19, Landroid/graphics/Point;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    move-object/from16 v0, v19

    iget v1, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, v19

    iget v2, v0, Landroid/graphics/Point;->x:I

    if-le v1, v2, :cond_2

    move-object/from16 v0, v19

    iget v1, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mScreenWidth:I

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "force_fsg_nav_bar"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mForceImmersiveNavBarListener:Landroid/database/ContentObserver;

    const/4 v5, 0x0

    const/4 v6, -0x1

    invoke-virtual {v1, v2, v5, v3, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "show_gesture_back_animation"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mShowGestureBackAnimationListener:Landroid/database/ContentObserver;

    const/4 v5, 0x0

    const/4 v6, -0x1

    invoke-virtual {v1, v2, v5, v3, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v4, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mReceiver:Landroid/content/BroadcastReceiver;

    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "com.android.systemui.fsgesture"

    invoke-virtual {v8, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFsgReceiver:Landroid/content/BroadcastReceiver;

    sget-object v7, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string/jumbo v9, "miui.permission.USE_INTERNAL_GENERAL_API"

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v12, Landroid/content/IntentFilter;

    invoke-direct {v12}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v1, "com.android.systemui.fsgsizechange"

    invoke-virtual {v12, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFsgSizeChangeReceiver:Landroid/content/BroadcastReceiver;

    sget-object v11, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string/jumbo v13, "miui.permission.USE_INTERNAL_GENERAL_API"

    const/4 v14, 0x0

    invoke-virtual/range {v9 .. v14}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v1, Lcom/android/systemui/statusbar/phone/NavStubView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/systemui/statusbar/phone/NavStubView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    const/16 v20, 0xd

    const-string/jumbo v1, "lithium"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v20, 0xe

    :cond_0
    move/from16 v0, v20

    int-to-float v1, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v0, v1

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Lcom/android/systemui/statusbar/phone/NavStubView;->getWindowParam(I)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->changeVisibility()V

    :cond_1
    return-void

    :cond_2
    move-object/from16 v0, v19

    iget v1, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mScreenWidth:I

    goto/16 :goto_1

    :catch_0
    move-exception v16

    goto/16 :goto_0
.end method

.method private calculateWindowStableInsets(Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 5

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getDisplayRect()Landroid/graphics/Rect;

    move-result-object v1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-static {v0, p1}, Lcom/android/systemui/Util;->inset(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->left:I

    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget v4, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->top:I

    iget v3, p2, Landroid/graphics/Rect;->right:I

    iget v4, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method private changeVisibility()V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "force_fsg_nav_bar"

    invoke-static {v1, v2}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mIsEnter:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mCurType:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/systemui/recents/BaseRecentsImpl;->setStateByTypeFrom(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/phone/NavStubView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->showBackStubWindow()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/phone/NavStubView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->hideBackStubWindow()V

    goto :goto_0
.end method

.method private clearNavStubWindow()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->clearGestureStub()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->clearGestureStub()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->isShowing:Z

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static consumeInstanceLoadPlan()Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;
    .locals 2

    sget-object v0, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    const/4 v1, 0x0

    sput-object v1, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    return-object v0
.end method

.method private getLaunchTargetTaskViewRect(Landroid/app/ActivityManager$RunningTaskInfo;Lcom/android/systemui/recents/views/TaskStackView;Landroid/graphics/Rect;Lcom/android/systemui/recents/model/Task;)Landroid/graphics/RectF;
    .locals 4

    invoke-virtual {p0, p2, p4, p3}, Lcom/android/systemui/recents/BaseRecentsImpl;->getThumbnailTransitionTransform(Lcom/android/systemui/recents/views/TaskStackView;Lcom/android/systemui/recents/model/Task;Landroid/graphics/Rect;)Lcom/android/systemui/recents/views/TaskViewTransform;

    move-result-object v1

    iget-object v0, v1, Lcom/android/systemui/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->top:F

    sget v3, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskBarHeight:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    return-object v0
.end method

.method private getWindowRectOverride(I)Landroid/graphics/Rect;
    .locals 7

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getDisplayRect()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getDockedDividerSize(Landroid/content/Context;)I

    move-result v5

    const/4 v1, 0x4

    move v0, p1

    invoke-static/range {v0 .. v5}, Lcom/android/internal/policy/DockedDividerUtils;->calculateBoundsForPosition(IILandroid/graphics/Rect;III)V

    return-object v2
.end method

.method private hideBackStubWindow()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v0}, Lcom/android/systemui/fsgesture/GestureStubView;->hideGestureStubDelay()V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v0}, Lcom/android/systemui/fsgesture/GestureStubView;->hideGestureStubDelay()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->isShowing:Z

    return-void
.end method

.method private initGestureStub(I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "show_gesture_back_animation"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Lcom/android/systemui/fsgesture/GestureStubView;

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/systemui/fsgesture/GestureStubView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v1, v3}, Lcom/android/systemui/fsgesture/GestureStubView;->setGestureStubPosition(I)V

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v1, v0}, Lcom/android/systemui/fsgesture/GestureStubView;->enableGestureBackAnimation(Z)V

    new-instance v1, Lcom/android/systemui/fsgesture/GestureStubView;

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/systemui/fsgesture/GestureStubView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v1, v4}, Lcom/android/systemui/fsgesture/GestureStubView;->setGestureStubPosition(I)V

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v1, v0}, Lcom/android/systemui/fsgesture/GestureStubView;->enableGestureBackAnimation(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private preloadIcon(I)V
    .locals 4

    new-instance v0, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;

    invoke-direct {v0}, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;-><init>()V

    iput p1, v0, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;->runningTaskId:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;->loadThumbnails:Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;->onlyLoadForCache:Z

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getTaskLoader()Lcom/android/systemui/recents/model/RecentsTaskLoader;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    invoke-virtual {v1, v2, v3, v0}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->loadTasks(Landroid/content/Context;Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;)V

    return-void
.end method

.method private reloadResources()V
    .locals 8

    const v4, 0x7f0b011a

    const v1, 0x7f0b0119

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v0, 0x1050177

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mStatusBarHeight:I

    const v0, 0x10500f8

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavBarHeight:I

    const v0, 0x10500fd

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavBarWidth:I

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    move v2, v1

    move v3, v1

    move v5, v1

    move v6, v4

    invoke-static/range {v0 .. v6}, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm;->getDimensionForDevice(Landroid/content/Context;IIIIII)I

    move-result v0

    sput v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskBarHeight:I

    return-void
.end method

.method private setStateByTypeFrom(Ljava/lang/String;)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    const-string/jumbo v0, "typefrom_demo"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/phone/NavStubView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->hideBackStubWindow()V

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "typefrom_keyguard"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "typefrom_home"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/NavStubView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->hideBackStubWindow()V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "typefrom_provision"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/phone/NavStubView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->showBackStubWindow()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/NavStubView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->showBackStubWindow()V

    goto :goto_0
.end method

.method private showBackStubWindow()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/android/systemui/recents/BaseRecentsImpl;->showBackStubWindow(I)V

    return-void
.end method

.method private showBackStubWindow(I)V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "force_fsg_nav_bar"

    invoke-static {v1, v2}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/recents/BaseRecentsImpl;->initGestureStub(I)V

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubLeft:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->showGestureStub()V

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mGestureStubRight:Lcom/android/systemui/fsgesture/GestureStubView;

    invoke-virtual {v1}, Lcom/android/systemui/fsgesture/GestureStubView;->showGestureStub()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->isShowing:Z

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->hideBackStubWindow()V

    goto :goto_0
.end method

.method private startRecentsActivity(Landroid/app/ActivityOptions;)V
    .locals 5

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "com.android.systemui"

    const-string/jumbo v3, "com.android.systemui.recents.RecentsActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v2, 0x10804000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v3

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    :goto_0
    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v2

    new-instance v3, Lcom/android/systemui/recents/events/activity/RecentsActivityStartingEvent;

    invoke-direct {v3}, Lcom/android/systemui/recents/events/activity/RecentsActivityStartingEvent;-><init>()V

    invoke-virtual {v2, v3}, Lcom/android/systemui/recents/events/RecentsEventBus;->send(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    :goto_1
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "RecentsImpl"

    const-string/jumbo v3, "startRecentsActivity"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private updateFsgStubState(ZLjava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mIsInAnotherPro:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "force_fsg_nav_bar"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mIsEnter:Z

    iput-object p2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mCurType:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mCurType:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/systemui/recents/BaseRecentsImpl;->setStateByTypeFrom(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mCurType:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    return-void

    :cond_3
    iput-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mCurType:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mIsEnter:Z

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mNavStubView:Lcom/android/systemui/statusbar/phone/NavStubView;

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/phone/NavStubView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->showBackStubWindow()V

    goto :goto_0
.end method

.method private updateHeaderBarLayout(Lcom/android/systemui/recents/model/TaskStack;Landroid/graphics/Rect;)V
    .locals 12

    const/4 v10, 0x0

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getDisplayRect()Landroid/graphics/Rect;

    move-result-object v1

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v6, v7}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getStableInsets(Landroid/graphics/Rect;)V

    if-eqz p2, :cond_5

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    :goto_0
    invoke-virtual {v6}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->hasDockedTask()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget v4, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    iput v10, v7, Landroid/graphics/Rect;->bottom:I

    :cond_0
    invoke-direct {p0, v7, v2}, Lcom/android/systemui/recents/BaseRecentsImpl;->calculateWindowStableInsets(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-virtual {v2, v10, v10}, Landroid/graphics/Rect;->offsetTo(II)V

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDummyStackView:Lcom/android/systemui/recents/views/TaskStackView;

    invoke-virtual {v3}, Lcom/android/systemui/recents/views/TaskStackView;->getStackAlgorithm()Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm;->setSystemInsets(Landroid/graphics/Rect;)Z

    if-eqz p1, :cond_4

    iget v3, v7, Landroid/graphics/Rect;->top:I

    iget v4, v7, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskStackBounds:Landroid/graphics/Rect;

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm;->getTaskStackBounds(Landroid/graphics/Rect;Landroid/graphics/Rect;IILandroid/graphics/Rect;)V

    invoke-virtual {v0}, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm;->reset()V

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskStackBounds:Landroid/graphics/Rect;

    invoke-static {p1}, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm$StackState;->getStackStateForStack(Lcom/android/systemui/recents/model/TaskStack;)Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm$StackState;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm;->initialize(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm$StackState;)V

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDummyStackView:Lcom/android/systemui/recents/views/TaskStackView;

    invoke-virtual {v3, p1, v10}, Lcom/android/systemui/recents/views/TaskStackView;->setTasks(Lcom/android/systemui/recents/model/TaskStack;Z)V

    invoke-virtual {v0}, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm;->getUntransformedTaskViewBounds()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v9

    iget-object v4, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHeaderBarLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHeaderBar:Lcom/android/systemui/recents/views/TaskViewHeader;

    invoke-virtual {v3}, Lcom/android/systemui/recents/views/TaskViewHeader;->getMeasuredWidth()I

    move-result v3

    if-ne v3, v9, :cond_1

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHeaderBar:Lcom/android/systemui/recents/views/TaskViewHeader;

    invoke-virtual {v3}, Lcom/android/systemui/recents/views/TaskViewHeader;->getMeasuredHeight()I

    move-result v3

    sget v5, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskBarHeight:I

    if-eq v3, v5, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHeaderBar:Lcom/android/systemui/recents/views/TaskViewHeader;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v9, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    sget v10, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskBarHeight:I

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v3, v5, v10}, Lcom/android/systemui/recents/views/TaskViewHeader;->measure(II)V

    :cond_2
    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHeaderBar:Lcom/android/systemui/recents/views/TaskViewHeader;

    sget v5, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskBarHeight:I

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11, v9, v5}, Lcom/android/systemui/recents/views/TaskViewHeader;->layout(IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v4

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mThumbTransitionBitmapCache:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mThumbTransitionBitmapCache:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-eq v3, v9, :cond_6

    :cond_3
    :goto_1
    sget v3, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskBarHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mThumbTransitionBitmapCache:Landroid/graphics/Bitmap;

    :cond_4
    return-void

    :cond_5
    invoke-virtual {v6}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getWindowRect()Landroid/graphics/Rect;

    move-result-object v2

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    :cond_6
    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mThumbTransitionBitmapCache:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget v4, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskBarHeight:I

    if-eq v3, v4, :cond_4

    goto :goto_1
.end method


# virtual methods
.method public cancelPreloadingRecents()V
    .locals 0

    return-void
.end method

.method public dockTopTask(IIILandroid/graphics/Rect;)V
    .locals 9

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/systemui/recents/misc/Utilities;->supportsMultiWindow()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    const v1, 0x7f0d033d

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v8

    invoke-virtual {v8, p1, p3, p4}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->moveTaskToDockedStack(IILandroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v0

    new-instance v2, Lcom/android/systemui/recents/events/activity/DockedTopTaskEvent;

    invoke-direct {v2, p2, p4}, Lcom/android/systemui/recents/events/activity/DockedTopTaskEvent;-><init>(ILandroid/graphics/Rect;)V

    invoke-virtual {v0, v2}, Lcom/android/systemui/recents/events/RecentsEventBus;->send(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    if-nez p2, :cond_2

    move v2, v4

    :goto_0
    const/4 v6, -0x1

    move-object v0, p0

    move v3, v1

    move v5, v1

    move v7, v1

    invoke-virtual/range {v0 .. v7}, Lcom/android/systemui/recents/BaseRecentsImpl;->showRecents(ZZZZZIZ)V

    :cond_1
    return-void

    :cond_2
    move v2, v1

    goto :goto_0
.end method

.method protected getHomeTransitionActivityOptions()Landroid/app/ActivityOptions;
    .locals 5

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    const v2, 0x7f04000d

    const v3, 0x7f04000e

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;IILandroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;

    move-result-object v0

    return-object v0
.end method

.method getThumbnailTransitionActivityOptions(Landroid/app/ActivityManager$RunningTaskInfo;Lcom/android/systemui/recents/views/TaskStackView;Landroid/graphics/Rect;)Landroid/app/ActivityOptions;
    .locals 11

    const/4 v7, 0x0

    new-instance v8, Lcom/android/systemui/recents/model/Task;

    invoke-direct {v8}, Lcom/android/systemui/recents/model/Task;-><init>()V

    invoke-virtual {p0, p2, v8, p3}, Lcom/android/systemui/recents/BaseRecentsImpl;->getThumbnailTransitionTransform(Lcom/android/systemui/recents/views/TaskStackView;Lcom/android/systemui/recents/model/Task;Landroid/graphics/Rect;)Lcom/android/systemui/recents/views/TaskViewTransform;

    move-result-object v10

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mThumbTransitionBitmapCache:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v9, v10, Lcom/android/systemui/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    iget v0, v9, Landroid/graphics/RectF;->top:F

    sget v1, Lcom/android/systemui/recents/BaseRecentsImpl;->mTaskBarHeight:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, v9, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDummyStackView:Lcom/android/systemui/recents/views/TaskStackView;

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mThumbTransitionBitmapCache:Landroid/graphics/Bitmap;

    iget v2, v9, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v9, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v5, v5

    iget-object v6, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    invoke-static/range {v0 .. v7}, Landroid/app/ActivityOptions;->makeThumbnailAspectScaleDownAnimation(Landroid/view/View;Landroid/graphics/Bitmap;IIIILandroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->getUnknownTransitionActivityOptions()Landroid/app/ActivityOptions;

    move-result-object v0

    return-object v0
.end method

.method getThumbnailTransitionTransform(Lcom/android/systemui/recents/views/TaskStackView;Lcom/android/systemui/recents/model/Task;Landroid/graphics/Rect;)Lcom/android/systemui/recents/views/TaskViewTransform;
    .locals 7

    const/4 v4, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/android/systemui/recents/views/TaskStackView;->getStack()Lcom/android/systemui/recents/model/TaskStack;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/systemui/recents/model/TaskStack;->getLaunchTarget()Lcom/android/systemui/recents/model/Task;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2, v1}, Lcom/android/systemui/recents/model/Task;->copyFrom(Lcom/android/systemui/recents/model/Task;)V

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/systemui/recents/views/TaskStackView;->updateLayoutAlgorithm(Z)V

    invoke-virtual {p1}, Lcom/android/systemui/recents/views/TaskStackView;->updateToInitialState()V

    invoke-virtual {p1}, Lcom/android/systemui/recents/views/TaskStackView;->getStackAlgorithm()Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/systemui/recents/views/TaskStackView;->getScroller()Lcom/android/systemui/recents/views/TaskStackViewScroller;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/systemui/recents/views/TaskStackViewScroller;->getStackScroll()F

    move-result v2

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTmpTransform:Lcom/android/systemui/recents/views/TaskViewTransform;

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm;->getStackTransformScreenCoordinates(Lcom/android/systemui/recents/model/Task;FLcom/android/systemui/recents/views/TaskViewTransform;Lcom/android/systemui/recents/views/TaskViewTransform;Landroid/graphics/Rect;)Lcom/android/systemui/recents/views/TaskViewTransform;

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTmpTransform:Lcom/android/systemui/recents/views/TaskViewTransform;

    return-object v0

    :cond_0
    invoke-virtual {v6, v0}, Lcom/android/systemui/recents/model/TaskStack;->getStackFrontMostTask(Z)Lcom/android/systemui/recents/model/Task;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/android/systemui/recents/model/Task;->copyFrom(Lcom/android/systemui/recents/model/Task;)V

    goto :goto_0
.end method

.method protected getUnknownTransitionActivityOptions()Landroid/app/ActivityOptions;
    .locals 5

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    const v2, 0x7f04000f

    const v3, 0x7f040010

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;IILandroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;

    move-result-object v0

    return-object v0
.end method

.method public hideRecents(ZZZ)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

    invoke-virtual {v0}, Lcom/android/systemui/recents/misc/DozeTrigger;->isDozing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->showNextTask()V

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

    invoke-virtual {v0}, Lcom/android/systemui/recents/misc/DozeTrigger;->stopDozing()V

    return-void

    :cond_0
    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/recents/events/activity/HideRecentsEvent;

    invoke-direct {v1, p1, p2, p3}, Lcom/android/systemui/recents/events/activity/HideRecentsEvent;-><init>(ZZZ)V

    invoke-virtual {v0, v1}, Lcom/android/systemui/recents/events/RecentsEventBus;->post(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    return-void
.end method

.method public onBootCompleted()V
    .locals 5

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getTaskLoader()Lcom/android/systemui/recents/model/RecentsTaskLoader;

    move-result-object v1

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->createLoadPlan(Landroid/content/Context;)Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    move-result-object v2

    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->preloadTasks(Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;IZ)V

    new-instance v0, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;

    invoke-direct {v0}, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;-><init>()V

    invoke-virtual {v1}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->getIconCacheSize()I

    move-result v3

    iput v3, v0, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;->numVisibleTasks:I

    invoke-virtual {v1}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->getThumbnailCacheSize()I

    move-result v3

    iput v3, v0, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;->numVisibleTaskThumbnails:I

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;->onlyLoadForCache:Z

    iget-object v3, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3, v2, v0}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->loadTasks(Landroid/content/Context;Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;Lcom/android/systemui/recents/model/RecentsTaskLoadPlan$Options;)V

    return-void
.end method

.method public final onBusEvent(Lcom/android/systemui/recents/events/activity/FsGestureEnterRecentsEvent;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x1

    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-virtual/range {v0 .. v7}, Lcom/android/systemui/recents/BaseRecentsImpl;->showRecents(ZZZZZIZ)V

    return-void
.end method

.method public final onBusEvent(Lcom/android/systemui/recents/events/activity/FsGesturePreloadRecentsEvent;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->preloadRecents()V

    return-void
.end method

.method public final onBusEvent(Lcom/android/systemui/recents/events/activity/FsGestureShowStateEvent;)V
    .locals 2

    iget-boolean v0, p1, Lcom/android/systemui/recents/events/activity/FsGestureShowStateEvent;->isEnter:Z

    iget-object v1, p1, Lcom/android/systemui/recents/events/activity/FsGestureShowStateEvent;->typeFrom:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/systemui/recents/BaseRecentsImpl;->updateFsgStubState(ZLjava/lang/String;)V

    return-void
.end method

.method public final onBusEvent(Lcom/android/systemui/recents/events/component/HideNavStubForBackWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->hideBackStubWindow()V

    return-void
.end method

.method public final onBusEvent(Lcom/android/systemui/recents/events/component/ShowNavStubForBackWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->showBackStubWindow()V

    return-void
.end method

.method public final onBusEvent(Lcom/android/systemui/recents/events/ui/DismissAllTaskViewsEndEvent;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mOneKeyCleaning:Z

    return-void
.end method

.method public final onBusEvent(Lcom/android/systemui/recents/events/ui/DismissAllTaskViewsEvent;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mOneKeyCleaning:Z

    return-void
.end method

.method public onConfigurationChanged()V
    .locals 1

    invoke-direct {p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->reloadResources()V

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDummyStackView:Lcom/android/systemui/recents/views/TaskStackView;

    invoke-virtual {v0}, Lcom/android/systemui/recents/views/TaskStackView;->reloadOnConfigurationChange()V

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mHeaderBar:Lcom/android/systemui/recents/views/TaskViewHeader;

    invoke-virtual {v0}, Lcom/android/systemui/recents/views/TaskViewHeader;->onConfigurationChanged()V

    return-void
.end method

.method public onDraggingInRecents(F)V
    .locals 2

    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/recents/events/ui/DraggingInRecentsEvent;

    invoke-direct {v1, p1}, Lcom/android/systemui/recents/events/ui/DraggingInRecentsEvent;-><init>(F)V

    invoke-virtual {v0, v1}, Lcom/android/systemui/recents/events/RecentsEventBus;->sendOntoMainThread(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    return-void
.end method

.method public onDraggingInRecentsEnded(F)V
    .locals 2

    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/recents/events/ui/DraggingInRecentsEndedEvent;

    invoke-direct {v1, p1}, Lcom/android/systemui/recents/events/ui/DraggingInRecentsEndedEvent;-><init>(F)V

    invoke-virtual {v0, v1}, Lcom/android/systemui/recents/events/RecentsEventBus;->sendOntoMainThread(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    return-void
.end method

.method public onStartScreenPinning(Landroid/content/Context;I)V
    .locals 3

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/Application;

    invoke-virtual {v2}, Lcom/android/systemui/Application;->getSystemUIApplication()Lcom/android/systemui/SystemUIApplication;

    move-result-object v0

    const-class v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0, v2}, Lcom/android/systemui/SystemUIApplication;->getComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->showScreenPinningRequest(IZ)V

    :cond_0
    return-void
.end method

.method public onVisibilityChanged(Landroid/content/Context;Z)V
    .locals 1

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->setRecentsVisibility(Landroid/content/Context;Z)V

    return-void
.end method

.method public preloadRecents()V
    .locals 9

    const/4 v8, 0x0

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v4

    iget-object v6, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/android/systemui/taskmanager/RecentTasksManager;->updateProtectedPkgs(Landroid/content/Context;)V

    new-instance v0, Lcom/android/systemui/recents/model/MutableBoolean;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Lcom/android/systemui/recents/model/MutableBoolean;-><init>(Z)V

    invoke-virtual {v4, v0}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->isRecentsActivityVisible(Lcom/android/systemui/recents/model/MutableBoolean;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getRunningTask()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    iget v3, v2, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    :goto_0
    invoke-static {}, Lcom/android/systemui/recents/Recents;->getTaskLoader()Lcom/android/systemui/recents/model/RecentsTaskLoader;

    move-result-object v1

    iget-object v6, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->createLoadPlan(Landroid/content/Context;)Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    move-result-object v6

    sput-object v6, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    sget-object v6, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    iget-boolean v7, v0, Lcom/android/systemui/recents/model/MutableBoolean;->value:Z

    xor-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;->preloadRawTasks(Z)V

    sget-object v6, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    iget-boolean v7, v0, Lcom/android/systemui/recents/model/MutableBoolean;->value:Z

    xor-int/lit8 v7, v7, 0x1

    invoke-virtual {v1, v6, v3, v7}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->preloadTasks(Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;IZ)V

    sget-object v6, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    invoke-virtual {v6}, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;->getTaskStack()Lcom/android/systemui/recents/model/TaskStack;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/systemui/recents/model/TaskStack;->getTaskCount()I

    move-result v6

    if-lez v6, :cond_0

    invoke-direct {p0, v3}, Lcom/android/systemui/recents/BaseRecentsImpl;->preloadIcon(I)V

    invoke-direct {p0, v5, v8}, Lcom/android/systemui/recents/BaseRecentsImpl;->updateHeaderBarLayout(Lcom/android/systemui/recents/model/TaskStack;Landroid/graphics/Rect;)V

    :cond_0
    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public showNextTask()V
    .locals 15

    const/4 v13, 0x0

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v7

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getTaskLoader()Lcom/android/systemui/recents/model/RecentsTaskLoader;

    move-result-object v4

    iget-object v12, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v12}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->createLoadPlan(Landroid/content/Context;)Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    move-result-object v5

    const/4 v12, -0x1

    invoke-virtual {v4, v5, v12, v13}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->preloadTasks(Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;IZ)V

    invoke-virtual {v5}, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;->getTaskStack()Lcom/android/systemui/recents/model/TaskStack;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/systemui/recents/model/TaskStack;->getTaskCount()I

    move-result v12

    if-nez v12, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v7}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getRunningTask()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v6

    if-nez v6, :cond_2

    return-void

    :cond_2
    invoke-static {v6}, Lcom/android/systemui/CompatibilityN;->getRunningTaskStackId(Landroid/app/ActivityManager$RunningTaskInfo;)I

    move-result v12

    invoke-static {v12}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->isHomeOrRecentsStack(I)Z

    move-result v2

    invoke-virtual {v0}, Lcom/android/systemui/recents/model/TaskStack;->getStackTasks()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v3, 0x0

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v1, v9, -0x1

    :goto_0
    const/4 v12, 0x1

    if-lt v1, v12, :cond_3

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/systemui/recents/model/Task;

    if-eqz v2, :cond_4

    add-int/lit8 v12, v1, -0x1

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/systemui/recents/model/Task;

    iget-object v12, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    const v13, 0x7f040013

    const v14, 0x7f04000c

    invoke-static {v12, v13, v14}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v3

    :cond_3
    :goto_1
    if-nez v11, :cond_6

    iget-object v12, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    const v13, 0x7f040014

    invoke-static {v12, v13}, Landroid/app/ActivityOptions;->makeCustomInPlaceAnimation(Landroid/content/Context;I)Landroid/app/ActivityOptions;

    move-result-object v12

    invoke-virtual {v7, v12}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->startInPlaceAnimationOnFrontMostApplication(Landroid/app/ActivityOptions;)V

    return-void

    :cond_4
    iget-object v12, v8, Lcom/android/systemui/recents/model/Task;->key:Lcom/android/systemui/recents/model/Task$TaskKey;

    iget v12, v12, Lcom/android/systemui/recents/model/Task$TaskKey;->id:I

    iget v13, v6, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    if-ne v12, v13, :cond_5

    add-int/lit8 v12, v1, -0x1

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/systemui/recents/model/Task;

    iget-object v12, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    const v13, 0x7f040016

    const v14, 0x7f040015

    invoke-static {v12, v13, v14}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v3

    goto :goto_1

    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_6
    iget-object v12, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    iget-object v13, v11, Lcom/android/systemui/recents/model/Task;->key:Lcom/android/systemui/recents/model/Task$TaskKey;

    iget-object v14, v11, Lcom/android/systemui/recents/model/Task;->title:Ljava/lang/String;

    invoke-virtual {v7, v12, v13, v14, v3}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->startActivityFromRecents(Landroid/content/Context;Lcom/android/systemui/recents/model/Task$TaskKey;Ljava/lang/String;Landroid/app/ActivityOptions;)Z

    return-void
.end method

.method public showRecents(ZZZZZIZ)V
    .locals 10

    iput-boolean p1, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTriggeredFromAltTab:Z

    iput-boolean p2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDraggingInRecents:Z

    iput-boolean p4, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLaunchedWhileDocking:Z

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

    invoke-virtual {v0}, Lcom/android/systemui/recents/misc/DozeTrigger;->isAsleep()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

    invoke-virtual {v0}, Lcom/android/systemui/recents/misc/DozeTrigger;->stopDozing()V

    :cond_0
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v9

    if-nez p4, :cond_4

    move v7, p2

    :goto_1
    new-instance v8, Lcom/android/systemui/recents/model/MutableBoolean;

    invoke-direct {v8, v7}, Lcom/android/systemui/recents/model/MutableBoolean;-><init>(Z)V

    iget-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mOneKeyCleaning:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mCleaningToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

    invoke-virtual {v0}, Lcom/android/systemui/recents/misc/DozeTrigger;->isDozing()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p1, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

    invoke-virtual {v0}, Lcom/android/systemui/recents/misc/DozeTrigger;->stopDozing()V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

    invoke-virtual {v0}, Lcom/android/systemui/recents/misc/DozeTrigger;->startDozing()V

    return-void

    :cond_4
    const/4 v7, 0x1

    goto :goto_1

    :cond_5
    if-nez v7, :cond_6

    :try_start_1
    invoke-virtual {v9, v8}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->isRecentsActivityVisible(Lcom/android/systemui/recents/model/MutableBoolean;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    :cond_6
    invoke-virtual {v9}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getRunningTask()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v1

    iget-boolean v0, v8, Lcom/android/systemui/recents/model/MutableBoolean;->value:Z

    if-nez v0, :cond_8

    move v2, p5

    :goto_2
    move-object v0, p0

    move v3, p3

    move/from16 v4, p6

    move/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/recents/BaseRecentsImpl;->startRecentsActivity(Landroid/app/ActivityManager$RunningTaskInfo;ZZIZ)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_7
    :goto_3
    return-void

    :cond_8
    const/4 v2, 0x1

    goto :goto_2

    :catch_0
    move-exception v6

    const-string/jumbo v0, "RecentsImpl"

    const-string/jumbo v2, "Failed to launch RecentsActivity"

    invoke-static {v0, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3
.end method

.method protected startRecentsActivity(Landroid/app/ActivityManager$RunningTaskInfo;ZZIZ)V
    .locals 17

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getTaskLoader()Lcom/android/systemui/recents/model/RecentsTaskLoader;

    move-result-object v6

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getConfiguration()Lcom/android/systemui/recents/RecentsConfiguration;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/systemui/recents/RecentsConfiguration;->getLaunchState()Lcom/android/systemui/recents/RecentsActivityLaunchState;

    move-result-object v3

    const-string/jumbo v15, "RecentsImpl"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "startRecentsActivity runningTask: "

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    if-eqz p1, :cond_6

    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v14}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v14

    :goto_0
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v15, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLaunchedWhileDocking:Z

    if-nez v14, :cond_7

    if-eqz p1, :cond_7

    move-object/from16 v0, p1

    iget v8, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLaunchedWhileDocking:Z

    if-nez v14, :cond_0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTriggeredFromAltTab:Z

    if-nez v14, :cond_0

    sget-object v14, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    if-nez v14, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v14}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->createLoadPlan(Landroid/content/Context;)Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    move-result-object v14

    sput-object v14, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLaunchedWhileDocking:Z

    if-nez v14, :cond_2

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTriggeredFromAltTab:Z

    if-nez v14, :cond_2

    sget-object v14, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    invoke-virtual {v14}, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;->hasTasks()Z

    move-result v14

    xor-int/lit8 v14, v14, 0x1

    if-eqz v14, :cond_3

    :cond_2
    sget-object v14, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    xor-int/lit8 v15, p2, 0x1

    invoke-virtual {v6, v14, v8, v15}, Lcom/android/systemui/recents/model/RecentsTaskLoader;->preloadTasks(Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;IZ)V

    :cond_3
    sget-object v14, Lcom/android/systemui/recents/BaseRecentsImpl;->sInstanceLoadPlan:Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;

    invoke-virtual {v14}, Lcom/android/systemui/recents/model/RecentsTaskLoadPlan;->getTaskStack()Lcom/android/systemui/recents/model/TaskStack;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/systemui/recents/model/TaskStack;->getTaskCount()I

    move-result v14

    if-lez v14, :cond_8

    const/4 v2, 0x1

    :goto_2
    if-eqz p1, :cond_9

    xor-int/lit8 v14, p2, 0x1

    if-eqz v14, :cond_9

    move v12, v2

    :goto_3
    if-nez v12, :cond_a

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLaunchedWhileDocking:Z

    xor-int/lit8 v14, v14, 0x1

    :goto_4
    iput-boolean v14, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedFromHome:Z

    if-nez v12, :cond_b

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLaunchedWhileDocking:Z

    :goto_5
    iput-boolean v14, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedFromApp:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLaunchedWhileDocking:Z

    iput-boolean v14, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedViaDockGesture:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDraggingInRecents:Z

    iput-boolean v14, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedViaDragGesture:Z

    iput v8, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedToTaskId:I

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTriggeredFromAltTab:Z

    iput-boolean v14, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedWithAltTab:Z

    move/from16 v0, p5

    iput-boolean v0, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedViaFsGesture:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/systemui/recents/BaseRecentsImpl;->preloadIcon(I)V

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/android/systemui/recents/BaseRecentsImpl;->getWindowRectOverride(I)Landroid/graphics/Rect;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v13}, Lcom/android/systemui/recents/BaseRecentsImpl;->updateHeaderBarLayout(Lcom/android/systemui/recents/model/TaskStack;Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDummyStackView:Lcom/android/systemui/recents/views/TaskStackView;

    invoke-virtual {v14}, Lcom/android/systemui/recents/views/TaskStackView;->computeStackVisibilityReport()Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm$VisibilityReport;

    move-result-object v11

    iget v14, v11, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm$VisibilityReport;->numVisibleTasks:I

    iput v14, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedNumVisibleTasks:I

    iget v14, v11, Lcom/android/systemui/recents/views/TaskStackLayoutAlgorithm$VisibilityReport;->numVisibleThumbnails:I

    iput v14, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedNumVisibleThumbnails:I

    iget-boolean v14, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedViaFsGesture:Z

    if-eqz v14, :cond_c

    iget-boolean v9, v3, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedFromHome:Z

    :goto_6
    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v14

    new-instance v15, Lcom/android/systemui/recents/events/activity/ActivitySetDummyTranslucentEvent;

    invoke-direct {v15, v9}, Lcom/android/systemui/recents/events/activity/ActivitySetDummyTranslucentEvent;-><init>(Z)V

    invoke-virtual {v14, v15}, Lcom/android/systemui/recents/events/RecentsEventBus;->send(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    if-eqz p3, :cond_4

    if-eqz p5, :cond_d

    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mContext:Landroid/content/Context;

    const/4 v15, -0x1

    const/16 v16, -0x1

    invoke-static/range {v14 .. v16}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/systemui/recents/BaseRecentsImpl;->startRecentsActivity(Landroid/app/ActivityOptions;)V

    if-eqz v2, :cond_5

    new-instance v4, Lcom/android/systemui/recents/model/Task;

    invoke-direct {v4}, Lcom/android/systemui/recents/model/Task;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDummyStackView:Lcom/android/systemui/recents/views/TaskStackView;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14, v13, v4}, Lcom/android/systemui/recents/BaseRecentsImpl;->getLaunchTargetTaskViewRect(Landroid/app/ActivityManager$RunningTaskInfo;Lcom/android/systemui/recents/views/TaskStackView;Landroid/graphics/Rect;Lcom/android/systemui/recents/model/Task;)Landroid/graphics/RectF;

    move-result-object v5

    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v14

    new-instance v15, Lcom/android/systemui/recents/events/activity/FsGestureLaunchTargetTaskViewRectEvent;

    invoke-direct {v15, v5, v4}, Lcom/android/systemui/recents/events/activity/FsGestureLaunchTargetTaskViewRectEvent;-><init>(Landroid/graphics/RectF;Lcom/android/systemui/recents/model/Task;)V

    invoke-virtual {v14, v15}, Lcom/android/systemui/recents/events/RecentsEventBus;->send(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    :cond_5
    return-void

    :cond_6
    const-string/jumbo v14, "null"

    goto/16 :goto_0

    :cond_7
    const/4 v8, -0x1

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v12, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v14, 0x0

    goto/16 :goto_4

    :cond_b
    const/4 v14, 0x1

    goto/16 :goto_5

    :cond_c
    const/4 v9, 0x0

    goto :goto_6

    :cond_d
    if-eqz v12, :cond_e

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDummyStackView:Lcom/android/systemui/recents/views/TaskStackView;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v14, v13}, Lcom/android/systemui/recents/BaseRecentsImpl;->getThumbnailTransitionActivityOptions(Landroid/app/ActivityManager$RunningTaskInfo;Lcom/android/systemui/recents/views/TaskStackView;Landroid/graphics/Rect;)Landroid/app/ActivityOptions;

    move-result-object v7

    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/systemui/recents/BaseRecentsImpl;->startRecentsActivity(Landroid/app/ActivityOptions;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLastToggleTime:J

    return-void

    :cond_e
    if-eqz v2, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->getHomeTransitionActivityOptions()Landroid/app/ActivityOptions;

    move-result-object v7

    goto :goto_7

    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/recents/BaseRecentsImpl;->getUnknownTransitionActivityOptions()Landroid/app/ActivityOptions;

    move-result-object v7

    goto :goto_7
.end method

.method public toggleRecents(I)V
    .locals 14

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mFastAltTabTrigger:Lcom/android/systemui/recents/misc/DozeTrigger;

    invoke-virtual {v0}, Lcom/android/systemui/recents/misc/DozeTrigger;->isDozing()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mDraggingInRecents:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLaunchedWhileDocking:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mTriggeredFromAltTab:Z

    :try_start_0
    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v13

    new-instance v9, Lcom/android/systemui/recents/model/MutableBoolean;

    const/4 v0, 0x1

    invoke-direct {v9, v0}, Lcom/android/systemui/recents/model/MutableBoolean;-><init>(Z)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLastToggleTime:J

    sub-long v10, v2, v4

    invoke-virtual {v13, v9}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->isRecentsActivityVisible(Lcom/android/systemui/recents/model/MutableBoolean;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getDebugFlags()Lcom/android/systemui/recents/RecentsDebugFlags;

    move-result-object v7

    invoke-static {}, Lcom/android/systemui/recents/Recents;->getConfiguration()Lcom/android/systemui/recents/RecentsConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/systemui/recents/RecentsConfiguration;->getLaunchState()Lcom/android/systemui/recents/RecentsActivityLaunchState;

    move-result-object v12

    iget-boolean v0, v12, Lcom/android/systemui/recents/RecentsActivityLaunchState;->launchedWithAltTab:Z

    if-nez v0, :cond_3

    invoke-virtual {v7}, Lcom/android/systemui/recents/RecentsDebugFlags;->isPagingEnabled()Z

    move-result v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapMinTime()I

    move-result v0

    int-to-long v2, v0

    cmp-long v0, v2, v10

    if-gez v0, :cond_1

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    int-to-long v2, v0

    cmp-long v0, v10, v2

    if-gez v0, :cond_1

    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v0

    new-instance v2, Lcom/android/systemui/recents/events/activity/LaunchNextTaskRequestEvent;

    invoke-direct {v2}, Lcom/android/systemui/recents/events/activity/LaunchNextTaskRequestEvent;-><init>()V

    invoke-virtual {v0, v2}, Lcom/android/systemui/recents/events/RecentsEventBus;->post(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    const-string/jumbo v0, "doubleTap"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/systemui/recents/misc/RecentsPushEventHelper;->sendSwitchAppEvent(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLastToggleTime:J

    :goto_1
    return-void

    :cond_1
    invoke-virtual {v7}, Lcom/android/systemui/recents/RecentsDebugFlags;->isPagingEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v0

    new-instance v2, Lcom/android/systemui/recents/events/activity/IterateRecentsEvent;

    invoke-direct {v2}, Lcom/android/systemui/recents/events/activity/IterateRecentsEvent;-><init>()V

    invoke-virtual {v0, v2}, Lcom/android/systemui/recents/events/RecentsEventBus;->post(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    const-string/jumbo v0, "RecentsImpl"

    const-string/jumbo v2, "Failed to launch RecentsActivity"

    invoke-static {v0, v2, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    return-void

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v0

    new-instance v2, Lcom/android/systemui/recents/events/activity/ToggleRecentsEvent;

    invoke-direct {v2}, Lcom/android/systemui/recents/events/activity/ToggleRecentsEvent;-><init>()V

    invoke-virtual {v0, v2}, Lcom/android/systemui/recents/events/RecentsEventBus;->post(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    const-string/jumbo v0, "hideRecents"

    const-string/jumbo v2, "clickRecentsKey"

    invoke-static {v0, v2}, Lcom/android/systemui/recents/misc/RecentsPushEventHelper;->sendRecentsEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0x15e

    cmp-long v0, v10, v2

    if-gez v0, :cond_4

    return-void

    :cond_4
    invoke-static {}, Lcom/android/systemui/recents/events/RecentsEventBus;->getDefault()Lcom/android/systemui/recents/events/RecentsEventBus;

    move-result-object v0

    new-instance v2, Lcom/android/systemui/recents/events/activity/ToggleRecentsEvent;

    invoke-direct {v2}, Lcom/android/systemui/recents/events/activity/ToggleRecentsEvent;-><init>()V

    invoke-virtual {v0, v2}, Lcom/android/systemui/recents/events/RecentsEventBus;->post(Lcom/android/systemui/recents/events/RecentsEventBus$Event;)V

    const-string/jumbo v0, "hideRecents"

    const-string/jumbo v2, "clickAltTabKey"

    invoke-static {v0, v2}, Lcom/android/systemui/recents/misc/RecentsPushEventHelper;->sendRecentsEvent(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLastToggleTime:J

    goto :goto_1

    :cond_5
    const-wide/16 v2, 0x15e

    cmp-long v0, v10, v2

    if-gez v0, :cond_6

    return-void

    :cond_6
    iget-boolean v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mOneKeyCleaning:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mCleaningToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_7
    invoke-virtual {v13}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->getRunningTask()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v1

    iget-boolean v2, v9, Lcom/android/systemui/recents/model/MutableBoolean;->value:Z

    const/4 v3, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/recents/BaseRecentsImpl;->startRecentsActivity(Landroid/app/ActivityManager$RunningTaskInfo;ZZIZ)V

    const-string/jumbo v0, "recentapps"

    invoke-virtual {v13, v0}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->sendCloseSystemWindows(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/systemui/recents/BaseRecentsImpl;->mLastToggleTime:J
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
