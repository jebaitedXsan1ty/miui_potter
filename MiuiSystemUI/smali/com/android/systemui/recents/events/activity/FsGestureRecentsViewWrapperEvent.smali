.class public Lcom/android/systemui/recents/events/activity/FsGestureRecentsViewWrapperEvent;
.super Lcom/android/systemui/recents/events/RecentsEventBus$Event;
.source "FsGestureRecentsViewWrapperEvent.java"


# instance fields
.field public final mBackGround:Landroid/view/View;

.field public final mDockBtn:Landroid/widget/TextView;

.field public final mMemoryAndClearContainer:Landroid/view/ViewGroup;

.field public final mRecentsView:Lcom/android/systemui/recents/views/RecentsView;

.field public final mTipView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/android/systemui/recents/views/RecentsView;Landroid/view/View;Landroid/widget/TextView;Landroid/view/ViewGroup;Landroid/widget/TextView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/recents/events/RecentsEventBus$Event;-><init>()V

    iput-object p1, p0, Lcom/android/systemui/recents/events/activity/FsGestureRecentsViewWrapperEvent;->mRecentsView:Lcom/android/systemui/recents/views/RecentsView;

    iput-object p2, p0, Lcom/android/systemui/recents/events/activity/FsGestureRecentsViewWrapperEvent;->mBackGround:Landroid/view/View;

    iput-object p3, p0, Lcom/android/systemui/recents/events/activity/FsGestureRecentsViewWrapperEvent;->mDockBtn:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/android/systemui/recents/events/activity/FsGestureRecentsViewWrapperEvent;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    iput-object p5, p0, Lcom/android/systemui/recents/events/activity/FsGestureRecentsViewWrapperEvent;->mTipView:Landroid/widget/TextView;

    return-void
.end method
