.class Lcom/android/systemui/recents/BaseRecentsImpl$2$1;
.super Ljava/lang/Object;
.source "BaseRecentsImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/recents/BaseRecentsImpl$2;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/systemui/recents/BaseRecentsImpl$2;Landroid/content/Intent;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iput-object p2, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->val$intent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const/4 v9, 0x0

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    if-nez v7, :cond_0

    return-void

    :cond_0
    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->val$intent:Landroid/content/Intent;

    const-string/jumbo v8, "isEnter"

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v2, 0x3f

    :try_start_0
    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-static {v7}, Lcom/android/systemui/recents/BaseRecentsImpl;->-get0(Lcom/android/systemui/recents/BaseRecentsImpl;)Landroid/app/ActivityManager;

    move-result-object v7

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v8, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->val$context:Landroid/content/Context;

    const-string/jumbo v9, "activity"

    invoke-virtual {v7, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager;

    invoke-static {v8, v7}, Lcom/android/systemui/recents/BaseRecentsImpl;->-set0(Lcom/android/systemui/recents/BaseRecentsImpl;Landroid/app/ActivityManager;)Landroid/app/ActivityManager;

    :cond_1
    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-static {v7}, Lcom/android/systemui/recents/BaseRecentsImpl;->-get0(Lcom/android/systemui/recents/BaseRecentsImpl;)Landroid/app/ActivityManager;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, -0x2

    invoke-virtual {v7, v8, v2, v9}, Landroid/app/ActivityManager;->getRecentTasksForUser(III)Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v7, v7, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v7, "com.tencent.mobileqq"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string/jumbo v7, "com.miui.gallery"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string/jumbo v7, "com.sina.weibo"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string/jumbo v7, "com.tencent.mm"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/android/systemui/recents/BaseRecentsImpl;->-set2(Lcom/android/systemui/recents/BaseRecentsImpl;Z)Z

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xad9

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xb3d

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xad9

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string/jumbo v7, "RecentsImpl"

    const-string/jumbo v8, "Failed to get recent tasks"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_4
    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    invoke-static {v7}, Lcom/android/systemui/recents/BaseRecentsImpl;->-get4(Lcom/android/systemui/recents/BaseRecentsImpl;)Z

    move-result v7

    if-eqz v7, :cond_3

    :try_start_1
    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/android/systemui/recents/BaseRecentsImpl;->-set2(Lcom/android/systemui/recents/BaseRecentsImpl;Z)Z

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xad9

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xb3d

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xb3d

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    iget-object v7, p0, Lcom/android/systemui/recents/BaseRecentsImpl$2$1;->this$1:Lcom/android/systemui/recents/BaseRecentsImpl$2;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl$2;->this$0:Lcom/android/systemui/recents/BaseRecentsImpl;

    iget-object v7, v7, Lcom/android/systemui/recents/BaseRecentsImpl;->mHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x12c

    invoke-virtual {v7, v4, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
