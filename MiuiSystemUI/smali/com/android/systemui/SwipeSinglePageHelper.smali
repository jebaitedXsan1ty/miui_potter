.class public Lcom/android/systemui/SwipeSinglePageHelper;
.super Ljava/lang/Object;
.source "SwipeSinglePageHelper.java"


# static fields
.field public static ALPHA_FADE_START:F

.field private static sLinearInterpolator:Landroid/view/animation/LinearInterpolator;


# instance fields
.field private DEFAULT_ESCAPE_ANIMATION_DURATION:I

.field private MAX_DISMISS_VELOCITY:I

.field private MAX_ESCAPE_ANIMATION_DURATION:I

.field private SWIPE_ESCAPE_VELOCITY:F

.field private mBlockWidth:I

.field private mCallback:Lcom/android/systemui/SwipeHelper$Callback;

.field private mCanCurrViewBeDimissed:Z

.field private mCurrAnimView:Landroid/view/View;

.field private mCurrView:Landroid/view/View;

.field private mDensityScale:F

.field private mDragging:Z

.field private mHandler:Landroid/os/Handler;

.field private mInitialTouchPos:F

.field private mIsShowBlock:Z

.field private mLongPressListener:Landroid/view/View$OnLongClickListener;

.field private mLongPressSent:Z

.field private mLongPressTimeout:J

.field private mMinAlpha:F

.field private mPagingTouchSlop:F

.field private mSwipeDirection:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mWatchLongPress:Ljava/lang/Runnable;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/SwipeSinglePageHelper;)Lcom/android/systemui/SwipeHelper$Callback;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/SwipeSinglePageHelper;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/SwipeSinglePageHelper;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/SwipeSinglePageHelper;)Landroid/view/View$OnLongClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressListener:Landroid/view/View$OnLongClickListener;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/systemui/SwipeSinglePageHelper;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressSent:Z

    return v0
.end method

.method static synthetic -set0(Lcom/android/systemui/SwipeSinglePageHelper;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressSent:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/SwipeSinglePageHelper;Landroid/view/View;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/SwipeSinglePageHelper;->updateAlphaFromOffset(Landroid/view/View;Z)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/android/systemui/SwipeSinglePageHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    const/4 v0, 0x0

    sput v0, Lcom/android/systemui/SwipeSinglePageHelper;->ALPHA_FADE_START:F

    return-void
.end method

.method public constructor <init>(ILcom/android/systemui/SwipeHelper$Callback;FFI)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->SWIPE_ESCAPE_VELOCITY:F

    const/16 v0, 0xc8

    iput v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->DEFAULT_ESCAPE_ANIMATION_DURATION:I

    const/16 v0, 0x190

    iput v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->MAX_ESCAPE_ANIMATION_DURATION:I

    const/16 v0, 0x7d0

    iput v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->MAX_DISMISS_VELOCITY:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mMinAlpha:F

    iput-object p2, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mHandler:Landroid/os/Handler;

    iput p1, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mSwipeDirection:I

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    iput p3, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mDensityScale:F

    iput p4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mPagingTouchSlop:F

    iput p5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mBlockWidth:I

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    iput-wide v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressTimeout:J

    return-void
.end method

.method private createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 4

    const/4 v3, 0x0

    iget v1, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mSwipeDirection:I

    if-nez v1, :cond_0

    const-string/jumbo v1, "translationX"

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [F

    aput p2, v2, v3

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v1, "translationY"

    goto :goto_0
.end method

.method private getAlphaForOffset(Landroid/view/View;)F
    .locals 6

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getSize(Landroid/view/View;)F

    move-result v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float v0, v4, v3

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getTranslation(Landroid/view/View;)F

    move-result v1

    sget v4, Lcom/android/systemui/SwipeSinglePageHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_1

    sget v4, Lcom/android/systemui/SwipeSinglePageHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    sub-float v4, v1, v4

    div-float/2addr v4, v0

    sub-float v2, v5, v4

    :cond_0
    :goto_0
    iget v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mMinAlpha:F

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    return v4

    :cond_1
    sget v4, Lcom/android/systemui/SwipeSinglePageHelper;->ALPHA_FADE_START:F

    sub-float v4, v5, v4

    mul-float/2addr v4, v3

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    sget v4, Lcom/android/systemui/SwipeSinglePageHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    add-float/2addr v4, v1

    div-float/2addr v4, v0

    add-float v2, v5, v4

    goto :goto_0
.end method

.method private getDelta(F)F
    .locals 6

    iget-object v2, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/android/systemui/SwipeSinglePageHelper;->getSize(Landroid/view/View;)F

    move-result v1

    const v2, 0x3e19999a    # 0.15f

    mul-float v0, v2, v1

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v1

    if-ltz v2, :cond_1

    const/4 v2, 0x0

    cmpl-float v2, p1, v2

    if-lez v2, :cond_0

    move p1, v0

    :goto_0
    return p1

    :cond_0
    neg-float p1, v0

    goto :goto_0

    :cond_1
    div-float v2, p1, v1

    float-to-double v2, v2

    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float p1, v0, v2

    goto :goto_0
.end method

.method private getPerpendicularVelocity(Landroid/view/VelocityTracker;)F
    .locals 1

    iget v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private getPos(Landroid/view/MotionEvent;)F
    .locals 1

    iget v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0
.end method

.method private getSize(Landroid/view/View;)F
    .locals 1

    iget v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_0
    int-to-float v0, v0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method private getTranslation(Landroid/view/View;)F
    .locals 1

    iget v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    goto :goto_0
.end method

.method private getVelocity(Landroid/view/VelocityTracker;)F
    .locals 1

    iget v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    goto :goto_0
.end method

.method public static invalidateGlobalRegion(Landroid/view/View;)V
    .locals 5

    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {p0, v0}, Lcom/android/systemui/SwipeSinglePageHelper;->invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/RectF;)V

    return-void
.end method

.method public static invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/RectF;)V
    .locals 6

    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    check-cast p0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget v0, p1, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p1, Landroid/graphics/RectF;->right:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private isLeftMove(Landroid/view/View;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setTranslation(Landroid/view/View;F)V
    .locals 1

    iget v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method

.method private updateAlphaFromOffset(Landroid/view/View;Z)V
    .locals 4

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->isLeftMove(Landroid/view/View;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    cmpl-float v1, v0, v2

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    :goto_0
    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    :goto_1
    invoke-static {p1}, Lcom/android/systemui/SwipeSinglePageHelper;->invalidateGlobalRegion(Landroid/view/View;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1
.end method


# virtual methods
.method public dismissChild(Landroid/view/View;F)V
    .locals 7

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/android/systemui/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v1

    cmpg-float v4, p2, v6

    if-ltz v4, :cond_0

    cmpl-float v4, p2, v6

    if-nez v4, :cond_1

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    cmpg-float v4, v4, v6

    if-gez v4, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getSize(Landroid/view/View;)F

    move-result v4

    neg-float v3, v4

    :goto_0
    iget v2, p0, Lcom/android/systemui/SwipeSinglePageHelper;->MAX_ESCAPE_ANIMATION_DURATION:I

    cmpl-float v4, p2, v6

    if-eqz v4, :cond_3

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    sub-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x447a0000    # 1000.0f

    mul-float/2addr v4, v5

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    div-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    :goto_1
    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    invoke-direct {p0, p1, v3}, Lcom/android/systemui/SwipeSinglePageHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    sget-object v4, Lcom/android/systemui/SwipeSinglePageHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    int-to-long v4, v2

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v4, Lcom/android/systemui/SwipeSinglePageHelper$4;

    invoke-direct {v4, p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper$4;-><init>(Lcom/android/systemui/SwipeSinglePageHelper;Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v4, Lcom/android/systemui/SwipeSinglePageHelper$5;

    invoke-direct {v4, p0, p1, v1}, Lcom/android/systemui/SwipeSinglePageHelper$5;-><init>(Lcom/android/systemui/SwipeSinglePageHelper;Landroid/view/View;Z)V

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    :cond_1
    cmpl-float v4, p2, v6

    if-nez v4, :cond_2

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    cmpl-float v4, v4, v6

    if-nez v4, :cond_2

    iget v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mSwipeDirection:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getSize(Landroid/view/View;)F

    move-result v3

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/android/systemui/SwipeSinglePageHelper;->DEFAULT_ESCAPE_ANIMATION_DURATION:I

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-boolean v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mDragging:Z

    return v4

    :pswitch_0
    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/android/systemui/SwipeHelper$Callback;->getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v2

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    if-eq v4, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/SwipeSinglePageHelper;->resetViewIfNeed()V

    :cond_1
    iput-object v2, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    iput-boolean v7, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mDragging:Z

    iput-boolean v7, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressSent:Z

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    iget-object v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v4, v5}, Lcom/android/systemui/SwipeHelper$Callback;->isBlockShow(Landroid/view/View;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mIsShowBlock:Z

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->clear()V

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    iget-object v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v4, v5}, Lcom/android/systemui/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    iget-object v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v4, v5}, Lcom/android/systemui/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCanCurrViewBeDimissed:Z

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v4

    iput v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mInitialTouchPos:F

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressListener:Landroid/view/View$OnLongClickListener;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mWatchLongPress:Ljava/lang/Runnable;

    if-nez v4, :cond_2

    new-instance v4, Lcom/android/systemui/SwipeSinglePageHelper$3;

    invoke-direct {v4, p0}, Lcom/android/systemui/SwipeSinglePageHelper$3;-><init>(Lcom/android/systemui/SwipeSinglePageHelper;)V

    iput-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mWatchLongPress:Ljava/lang/Runnable;

    :cond_2
    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mWatchLongPress:Ljava/lang/Runnable;

    iget-wide v6, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressTimeout:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressSent:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v3

    iget v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mInitialTouchPos:F

    sub-float v1, v3, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mPagingTouchSlop:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    iget-object v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v4, v5}, Lcom/android/systemui/SwipeHelper$Callback;->onBeginDrag(Landroid/view/View;)V

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mDragging:Z

    invoke-direct {p0, p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v4

    iget-object v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    invoke-direct {p0, v5}, Lcom/android/systemui/SwipeSinglePageHelper;->getTranslation(Landroid/view/View;)F

    move-result v5

    sub-float/2addr v4, v5

    iput v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mInitialTouchPos:F

    invoke-virtual {p0}, Lcom/android/systemui/SwipeSinglePageHelper;->removeLongPressCallback()V

    goto/16 :goto_0

    :pswitch_2
    iput-boolean v7, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mDragging:Z

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTranslationX()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_3

    iput-object v6, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    iput-object v6, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    :cond_3
    iput-boolean v7, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressSent:Z

    invoke-virtual {p0}, Lcom/android/systemui/SwipeSinglePageHelper;->removeLongPressCallback()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 18

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressSent:Z

    if-eqz v12, :cond_0

    const/4 v12, 0x1

    return v12

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mDragging:Z

    if-nez v12, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/SwipeSinglePageHelper;->removeLongPressCallback()V

    const/4 v12, 0x0

    return v12

    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_2
    :goto_0
    const/4 v12, 0x1

    return v12

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    if-eqz v12, :cond_2

    invoke-direct/range {p0 .. p1}, Lcom/android/systemui/SwipeSinglePageHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mInitialTouchPos:F

    sub-float v5, v12, v13

    const/4 v12, 0x0

    cmpl-float v12, v5, v12

    if-ltz v12, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v12, v13}, Lcom/android/systemui/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v12

    xor-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_4

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/systemui/SwipeSinglePageHelper;->getDelta(F)F

    move-result v5

    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v5}, Lcom/android/systemui/SwipeSinglePageHelper;->setTranslation(Landroid/view/View;F)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCanCurrViewBeDimissed:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/android/systemui/SwipeSinglePageHelper;->updateAlphaFromOffset(Landroid/view/View;Z)V

    goto :goto_0

    :cond_4
    const/4 v12, 0x0

    cmpg-float v12, v5, v12

    if-gez v12, :cond_5

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v12, v13}, Lcom/android/systemui/SwipeHelper$Callback;->canSetImportance(Landroid/view/View;)Z

    move-result v12

    xor-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_5

    const/high16 v5, -0x40800000    # -1.0f

    goto :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mBlockWidth:I

    neg-int v12, v12

    int-to-float v12, v12

    cmpg-float v12, v5, v12

    if-gez v12, :cond_3

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mBlockWidth:I

    int-to-float v12, v12

    add-float v7, v5, v12

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/systemui/SwipeSinglePageHelper;->getDelta(F)F

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mBlockWidth:I

    int-to-float v13, v13

    sub-float v5, v12, v13

    goto :goto_1

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    if-eqz v12, :cond_2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->MAX_DISMISS_VELOCITY:I

    int-to-float v12, v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mDensityScale:F

    mul-float v9, v12, v13

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v13, 0x3e8

    invoke-virtual {v12, v13, v9}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->SWIPE_ESCAPE_VELOCITY:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mDensityScale:F

    mul-float v8, v12, v13

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/systemui/SwipeSinglePageHelper;->getVelocity(Landroid/view/VelocityTracker;)F

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/systemui/SwipeSinglePageHelper;->getPerpendicularVelocity(Landroid/view/VelocityTracker;)F

    move-result v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/systemui/SwipeSinglePageHelper;->getTranslation(Landroid/view/View;)F

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    float-to-double v12, v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/systemui/SwipeSinglePageHelper;->getSize(Landroid/view/View;)F

    move-result v14

    float-to-double v14, v14

    const-wide v16, 0x3fd999999999999aL    # 0.4

    mul-double v14, v14, v16

    cmpl-double v12, v12, v14

    if-lez v12, :cond_7

    const/4 v3, 0x1

    :goto_2
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v12

    cmpl-float v12, v12, v8

    if-lez v12, :cond_b

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v12

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_b

    const/4 v12, 0x0

    cmpl-float v12, v11, v12

    if-lez v12, :cond_8

    const/4 v12, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/systemui/SwipeSinglePageHelper;->getTranslation(Landroid/view/View;)F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_9

    const/4 v13, 0x1

    :goto_4
    if-ne v12, v13, :cond_a

    const/4 v4, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v12, v13}, Lcom/android/systemui/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v12

    if-eqz v12, :cond_e

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/systemui/SwipeSinglePageHelper;->isLeftMove(Landroid/view/View;)Z

    move-result v12

    xor-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_e

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mIsShowBlock:Z

    if-eqz v12, :cond_d

    if-nez v4, :cond_c

    :cond_6
    move v6, v3

    :goto_6
    if-eqz v6, :cond_10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    if-eqz v4, :cond_f

    :goto_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v11}, Lcom/android/systemui/SwipeSinglePageHelper;->dismissChild(Landroid/view/View;F)V

    goto/16 :goto_0

    :cond_7
    const/4 v3, 0x0

    goto :goto_2

    :cond_8
    const/4 v12, 0x0

    goto :goto_3

    :cond_9
    const/4 v13, 0x0

    goto :goto_4

    :cond_a
    const/4 v4, 0x0

    goto :goto_5

    :cond_b
    const/4 v4, 0x0

    goto :goto_5

    :cond_c
    const/4 v6, 0x0

    goto :goto_6

    :cond_d
    if-eqz v4, :cond_6

    const/4 v6, 0x1

    goto :goto_6

    :cond_e
    const/4 v6, 0x0

    goto :goto_6

    :cond_f
    const/4 v11, 0x0

    goto :goto_7

    :cond_10
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v12, v13}, Lcom/android/systemui/SwipeHelper$Callback;->onDragCancelled(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v11}, Lcom/android/systemui/SwipeSinglePageHelper;->snapChild(Landroid/view/View;F)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public removeLongPressCallback()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mWatchLongPress:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mWatchLongPress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mWatchLongPress:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method public resetViewIfNeed()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/SwipeSinglePageHelper;->snapChild(Landroid/view/View;F)V

    :cond_0
    return-void
.end method

.method public resetViewIfNeed(I)V
    .locals 6

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTranslationX()F

    move-result v3

    const/4 v5, 0x0

    cmpg-float v3, v3, v5

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    iget-object v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    invoke-interface {v3, v5}, Lcom/android/systemui/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    invoke-interface {v3, v1}, Lcom/android/systemui/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    move-result v3

    iget v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mBlockWidth:I

    neg-int v5, v5

    int-to-float v5, v5

    cmpg-float v3, v3, v5

    if-gez v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    iget-object v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCurrAnimView:Landroid/view/View;

    invoke-interface {v3, v5}, Lcom/android/systemui/SwipeHelper$Callback;->canSetImportance(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mBlockWidth:I

    neg-int v3, v3

    :goto_0
    int-to-float v3, v3

    invoke-direct {p0, v1, v3}, Lcom/android/systemui/SwipeSinglePageHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-lez p1, :cond_2

    :goto_1
    int-to-long v4, p1

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v3, Lcom/android/systemui/SwipeSinglePageHelper$1;

    invoke-direct {v3, p0, v1, v2}, Lcom/android/systemui/SwipeSinglePageHelper$1;-><init>(Lcom/android/systemui/SwipeSinglePageHelper;Landroid/view/View;Z)V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v3, Lcom/android/systemui/SwipeSinglePageHelper$2;

    invoke-direct {v3, p0, v1, v2}, Lcom/android/systemui/SwipeSinglePageHelper$2;-><init>(Lcom/android/systemui/SwipeSinglePageHelper;Landroid/view/View;Z)V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    return-void

    :cond_1
    move v3, v4

    goto :goto_0

    :cond_2
    move p1, v4

    goto :goto_1
.end method

.method public setBlockWidth(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mBlockWidth:I

    return-void
.end method

.method public setDensityScale(F)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mDensityScale:F

    return-void
.end method

.method public setLongPressListener(Landroid/view/View$OnLongClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mLongPressListener:Landroid/view/View$OnLongClickListener;

    return-void
.end method

.method public setPagingTouchSlop(F)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mPagingTouchSlop:F

    return-void
.end method

.method public snapChild(Landroid/view/View;F)V
    .locals 6

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/android/systemui/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    invoke-interface {v4, v1}, Lcom/android/systemui/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    move-result v4

    iget v5, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mBlockWidth:I

    neg-int v5, v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mCallback:Lcom/android/systemui/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/android/systemui/SwipeHelper$Callback;->canSetImportance(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/android/systemui/SwipeSinglePageHelper;->mBlockWidth:I

    neg-int v4, v4

    :goto_0
    int-to-float v4, v4

    invoke-direct {p0, v1, v4}, Lcom/android/systemui/SwipeSinglePageHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const/16 v3, 0xfa

    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v4, Lcom/android/systemui/SwipeSinglePageHelper$6;

    invoke-direct {v4, p0, v1, v2}, Lcom/android/systemui/SwipeSinglePageHelper$6;-><init>(Lcom/android/systemui/SwipeSinglePageHelper;Landroid/view/View;Z)V

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v4, Lcom/android/systemui/SwipeSinglePageHelper$7;

    invoke-direct {v4, p0, v1, v2, p1}, Lcom/android/systemui/SwipeSinglePageHelper$7;-><init>(Lcom/android/systemui/SwipeSinglePageHelper;Landroid/view/View;ZLandroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
