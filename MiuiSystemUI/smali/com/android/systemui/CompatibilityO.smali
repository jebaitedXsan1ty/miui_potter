.class public Lcom/android/systemui/CompatibilityO;
.super Ljava/lang/Object;
.source "CompatibilityO.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addNotificationChannel(Landroid/app/NotificationManager;Ljava/lang/String;Ljava/lang/String;Landroid/app/Notification$Builder;)V
    .locals 2

    new-instance v0, Landroid/app/NotificationChannel;

    const/4 v1, 0x2

    invoke-direct {v0, p1, p2, v1}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-virtual {p0, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    invoke-virtual {p3, p1}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    return-void
.end method

.method public static addNotificationChannel(Landroid/app/NotificationManager;Ljava/lang/String;Ljava/lang/String;ZLandroid/app/Notification$Builder;)V
    .locals 2

    new-instance v0, Landroid/app/NotificationChannel;

    const/4 v1, 0x2

    invoke-direct {v0, p1, p2, v1}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-virtual {v0, p3}, Landroid/app/NotificationChannel;->enableLights(Z)V

    invoke-virtual {p0, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    invoke-virtual {p4, p1}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    return-void
.end method

.method public static getDeviceOwnerOrganizationName(Lcom/android/systemui/statusbar/policy/SecurityController;)Ljava/lang/CharSequence;
    .locals 1

    invoke-interface {p0}, Lcom/android/systemui/statusbar/policy/SecurityController;->getDeviceOwnerOrganizationName()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static getPrimaryVpnName(Lcom/android/systemui/statusbar/policy/SecurityController;)Ljava/lang/String;
    .locals 1

    invoke-interface {p0}, Lcom/android/systemui/statusbar/policy/SecurityController;->getPrimaryVpnName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getQsFooterEnterprisePrivacySettingsActionIntent()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.ENTERPRISE_PRIVACY_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static getQsFooterVpnSettingsActionIntent()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.VPN_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static getSecurityController(Landroid/content/Context;)Lcom/android/systemui/statusbar/policy/SecurityController;
    .locals 1

    invoke-static {p0}, Lcom/android/systemui/statusbar/policy/SecurityControllerImpl;->getInstance(Landroid/content/Context;)Lcom/android/systemui/statusbar/policy/SecurityControllerImpl;

    move-result-object v0

    return-object v0
.end method

.method public static getWorkProfileOrganizationName(Lcom/android/systemui/statusbar/policy/SecurityController;)Ljava/lang/CharSequence;
    .locals 1

    invoke-interface {p0}, Lcom/android/systemui/statusbar/policy/SecurityController;->getWorkProfileOrganizationName()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static getWorkProfileVpnName(Lcom/android/systemui/statusbar/policy/SecurityController;)Ljava/lang/String;
    .locals 1

    invoke-interface {p0}, Lcom/android/systemui/statusbar/policy/SecurityController;->getWorkProfileVpnName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasCACertInCurrentUser(Lcom/android/systemui/statusbar/policy/SecurityController;)Z
    .locals 1

    invoke-interface {p0}, Lcom/android/systemui/statusbar/policy/SecurityController;->hasCACertInCurrentUser()Z

    move-result v0

    return v0
.end method

.method public static hasCACertInWorkProfile(Lcom/android/systemui/statusbar/policy/SecurityController;)Z
    .locals 1

    invoke-interface {p0}, Lcom/android/systemui/statusbar/policy/SecurityController;->hasCACertInWorkProfile()Z

    move-result v0

    return v0
.end method

.method public static hasWorkProfile(Lcom/android/systemui/statusbar/policy/SecurityController;)Z
    .locals 1

    invoke-interface {p0}, Lcom/android/systemui/statusbar/policy/SecurityController;->hasWorkProfile()Z

    move-result v0

    return v0
.end method

.method public static isDeviceManaged(Lcom/android/systemui/statusbar/policy/SecurityController;)Z
    .locals 1

    invoke-interface {p0}, Lcom/android/systemui/statusbar/policy/SecurityController;->isDeviceManaged()Z

    move-result v0

    return v0
.end method

.method public static isNetworkLoggingEnabled(Lcom/android/systemui/statusbar/policy/SecurityController;)Z
    .locals 1

    invoke-interface {p0}, Lcom/android/systemui/statusbar/policy/SecurityController;->isNetworkLoggingEnabled()Z

    move-result v0

    return v0
.end method

.method public static setDefaultFocusHighlightEnabled(Landroid/view/View;Z)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setDefaultFocusHighlightEnabled(Z)V

    :cond_0
    return-void
.end method

.method public static showPictureInPictureMenu(Lcom/android/systemui/statusbar/BaseStatusBar;)V
    .locals 2

    const-class v1, Lcom/android/systemui/pip/PipUI;

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/BaseStatusBar;->getComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/pip/PipUI;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/systemui/pip/PipUI;->showPictureInPictureMenu()V

    :cond_0
    return-void
.end method
