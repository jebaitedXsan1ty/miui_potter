.class Lcom/android/systemui/taskmanager/TaskManagerView$8;
.super Ljava/lang/Object;
.source "TaskManagerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/taskmanager/TaskManagerView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/TaskManagerView;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView$8;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/16 v4, 0x12c

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$8;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->freeze()V

    const-string/jumbo v0, "systemui_taskmanager_kill_all"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$8;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->-set0(Lcom/android/systemui/taskmanager/TaskManagerView;I)I

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$8;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView$8;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->getFreeMemory()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/android/systemui/taskmanager/TaskManagerView;->-set2(Lcom/android/systemui/taskmanager/TaskManagerView;J)J

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$8;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->removeAllTask()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$8;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-static {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->-get2(Lcom/android/systemui/taskmanager/TaskManagerView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$8;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-static {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->-get2(Lcom/android/systemui/taskmanager/TaskManagerView;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
