.class Lcom/android/systemui/taskmanager/TaskItemView$1;
.super Ljava/lang/Object;
.source "TaskItemView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/taskmanager/TaskItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/TaskItemView;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/TaskItemView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskItemView$1;->this$0:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView$1;->this$0:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-static {v0}, Lcom/android/systemui/taskmanager/TaskItemView;->-wrap0(Lcom/android/systemui/taskmanager/TaskItemView;)Lcom/android/systemui/taskmanager/TasksView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskItemView$1;->this$0:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v1, v1, Lcom/android/systemui/taskmanager/TaskItemView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/TasksView;->toggleTaskItemLock(Lcom/android/systemui/taskmanager/TaskInfo;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView$1;->this$0:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskItemView;->refreshLockState()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskItemView$1;->this$0:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->flingToRest()V

    invoke-virtual {p1}, Landroid/view/View;->clearAccessibilityFocus()V

    return-void
.end method
