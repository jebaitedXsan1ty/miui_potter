.class public Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;
.super Landroid/widget/LinearLayout;
.source "ScreenView.java"

# interfaces
.implements Lcom/android/systemui/taskmanager/ScreenView$Indicator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/taskmanager/ScreenView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SeekBarIndicator"
.end annotation


# virtual methods
.method public fastOffset(I)Z
    .locals 2

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;->mLeft:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;->mRight:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;->mLeft:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;->mRight:I

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;->mLeft:I

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
