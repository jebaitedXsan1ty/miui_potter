.class public Lcom/android/systemui/taskmanager/ScreenshotLoadManager;
.super Ljava/lang/Object;
.source "ScreenshotLoadManager.java"


# instance fields
.field private mAccessLockedFakeScreenshotLand:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mAccessLockedFakeScreenshotPort:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDefaultScreenshot:Landroid/graphics/Bitmap;

.field private mForInTempStorage:[B

.field private mHandler:Landroid/os/Handler;

.field private mIsInited:Z

.field private mIsPort:Z

.field private mReusableAllBitmaps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mReusableBitmapCount:I

.field private mReusableFreeBitmaps:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mWorkThread:Landroid/os/HandlerThread;


# direct methods
.method static synthetic -wrap0(Lcom/android/systemui/taskmanager/ScreenshotLoadManager;Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->loadScreenshot(Lcom/android/systemui/taskmanager/TaskInfo;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableAllBitmaps:Ljava/util/List;

    const/16 v0, 0x4000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mForInTempStorage:[B

    return-void
.end method

.method private checkOrientation(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mIsPort:Z

    iget-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->release(Lcom/android/systemui/taskmanager/TaskInfo;)V

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mIsPort:Z

    iput-boolean v0, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    :cond_0
    return-void
.end method

.method private createAccessLockedFakeScreenshot()Landroid/graphics/Bitmap;
    .locals 9

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iget-object v4, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mContext:Landroid/content/Context;

    invoke-static {v4, v2}, Lmiui/util/ScreenshotUtils;->getActivityScreenshotSize(Landroid/content/Context;Landroid/graphics/Point;)V

    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v4, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f03006d

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iget v4, v2, Landroid/graphics/Point;->x:I

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, v2, Landroid/graphics/Point;->y:I

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v3, v7, v7, v4, v5}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {v3, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    return-object v0
.end method

.method private getAccessLockedFakeScreenshot()Landroid/graphics/Bitmap;
    .locals 2

    iget-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mIsPort:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mAccessLockedFakeScreenshotPort:Ljava/lang/ref/SoftReference;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->createAccessLockedFakeScreenshot()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mIsPort:Z

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mAccessLockedFakeScreenshotPort:Ljava/lang/ref/SoftReference;

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mAccessLockedFakeScreenshotPort:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mAccessLockedFakeScreenshotLand:Ljava/lang/ref/SoftReference;

    if-nez v1, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mAccessLockedFakeScreenshotLand:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_4
    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mAccessLockedFakeScreenshotLand:Ljava/lang/ref/SoftReference;

    goto :goto_1
.end method

.method private getActivityScreenshotFile(Lcom/android/systemui/taskmanager/TaskInfo;)Ljava/io/File;
    .locals 3

    iget-object v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    invoke-static {v1, v2}, Lmiui/util/ScreenshotUtils;->getActivityScreenshotFile(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    iput-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotContentIsPort:Z

    return-object v0

    :cond_0
    iget-object v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    xor-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lmiui/util/ScreenshotUtils;->getActivityScreenshotFile(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    xor-int/lit8 v1, v1, 0x1

    iput-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotContentIsPort:Z

    return-object v0

    :cond_1
    const/4 v1, 0x0

    return-object v1
.end method

.method private getUserActivityScreenshotFile(Lcom/android/systemui/taskmanager/TaskInfo;)Ljava/io/File;
    .locals 4

    iget-object v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mComponentName:Landroid/content/ComponentName;

    iget v3, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    iget v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->taskId:I

    if-ltz v1, :cond_0

    iget v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->taskId:I

    :goto_0
    invoke-static {v2, v3, v1}, Lmiui/securityspace/CrossUserUtils;->getComponentStringWithUserIdAndTaskId(Landroid/content/ComponentName;II)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    invoke-static {v1, v2}, Lmiui/util/ScreenshotUtils;->getActivityScreenshotFile(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    iput-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotContentIsPort:Z

    return-object v0

    :cond_0
    iget v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->persistentTaskId:I

    goto :goto_0

    :cond_1
    iget-object v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mComponentName:Landroid/content/ComponentName;

    iget v3, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    iget v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->taskId:I

    if-ltz v1, :cond_2

    iget v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->taskId:I

    :goto_1
    invoke-static {v2, v3, v1}, Lmiui/securityspace/CrossUserUtils;->getComponentStringWithUserIdAndTaskId(Landroid/content/ComponentName;II)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    xor-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lmiui/util/ScreenshotUtils;->getActivityScreenshotFile(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    xor-int/lit8 v1, v1, 0x1

    iput-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotContentIsPort:Z

    return-object v0

    :cond_2
    iget v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->persistentTaskId:I

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    return-object v1
.end method

.method private isAccessLocked(Lcom/android/systemui/taskmanager/TaskInfo;)Z
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "security"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManager;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lmiui/security/SecurityManager;->isAccessControlActived(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->packageName:Ljava/lang/String;

    iget v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    invoke-virtual {v0, v1, v2}, Lmiui/security/SecurityManager;->getApplicationAccessControlEnabledAsUser(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->packageName:Ljava/lang/String;

    iget v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    invoke-virtual {v0, v1, v2}, Lmiui/security/SecurityManager;->checkAccessControlPassAsUser(Ljava/lang/String;I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    return v1

    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method private loadScreenshot(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->checkOrientation(Lcom/android/systemui/taskmanager/TaskInfo;)V

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->getUserActivityScreenshotFile(Lcom/android/systemui/taskmanager/TaskInfo;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->getActivityScreenshotFile(Lcom/android/systemui/taskmanager/TaskInfo;)Ljava/io/File;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->isAccessLocked(Lcom/android/systemui/taskmanager/TaskInfo;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->getAccessLockedFakeScreenshot()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    iput-boolean v5, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mIsRealScreenshot:Z

    iget-boolean v2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mIsPort:Z

    iput-boolean v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotContentIsPort:Z

    :cond_1
    :goto_0
    iget-object v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->getDefaultScreenshot()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    iput-boolean v5, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mIsRealScreenshot:Z

    :cond_2
    return-void

    :cond_3
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    iput v4, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mForInTempStorage:[B

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableFreeBitmaps:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    iget-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableAllBitmaps:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableBitmapCount:I

    if-ge v2, v3, :cond_4

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableAllBitmaps:Ljava/util/List;

    iget-object v3, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableAllBitmaps:Ljava/util/List;

    iget-object v3, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iput-boolean v4, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mIsRealScreenshot:Z

    goto :goto_0
.end method

.method private releaseInner(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getCompoundedTasks()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskInfo;

    iget-object v3, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    iget-object v2, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    iput-object v4, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    iget-boolean v3, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mIsRealScreenshot:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableAllBitmaps:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableFreeBitmaps:Ljava/util/Queue;

    invoke-interface {v3, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private waitLoading(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 2

    monitor-enter p1

    :goto_0
    :try_start_0
    iget-boolean v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mIsLoadingScreenshot:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p1

    throw v1

    :cond_0
    monitor-exit p1

    return-void
.end method


# virtual methods
.method public destroyAll()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->releaseAll()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableFreeBitmaps:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableAllBitmaps:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public ensureLoadedScreenshot(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 1

    iget-object v0, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mIsLoadingScreenshot:Z

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->loadScreenshot(Lcom/android/systemui/taskmanager/TaskInfo;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->waitLoading(Lcom/android/systemui/taskmanager/TaskInfo;)V

    goto :goto_0
.end method

.method protected finalize()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mWorkThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mWorkThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    :cond_0
    return-void
.end method

.method public getDefaultScreenshot()Landroid/graphics/Bitmap;
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mDefaultScreenshot:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mDefaultScreenshot:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mDefaultScreenshot:Landroid/graphics/Bitmap;

    const v1, -0x55000001

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mDefaultScreenshot:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public initialize(ILandroid/content/Context;)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mIsInited:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mIsInited:Z

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableBitmapCount:I

    iput-object p2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, p1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mReusableFreeBitmaps:Ljava/util/Queue;

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "ScreenshotLoadThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mWorkThread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mWorkThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mWorkThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mHandler:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method public release(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 1

    iget-boolean v0, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mIsLoadingScreenshot:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->waitLoading(Lcom/android/systemui/taskmanager/TaskInfo;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->releaseInner(Lcom/android/systemui/taskmanager/TaskInfo;)V

    return-void
.end method

.method public releaseAll()V
    .locals 3

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mTasks:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mTasks:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->release(Lcom/android/systemui/taskmanager/TaskInfo;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setup(Ljava/util/ArrayList;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;Z)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->releaseAll()V

    iput-object p1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mTasks:Ljava/util/ArrayList;

    iput-boolean p2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mIsPort:Z

    return-void
.end method

.method public startLoad(I)V
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->startLoad(Lcom/android/systemui/taskmanager/TaskInfo;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public startLoad(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 6

    invoke-virtual {p1}, Lcom/android/systemui/taskmanager/TaskInfo;->getCompoundedTasks()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-direct {p0, v0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->checkOrientation(Lcom/android/systemui/taskmanager/TaskInfo;)V

    iget-object v2, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mIsLoadingScreenshot:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mIsLoadingScreenshot:Z

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;

    invoke-direct {v3, p0, v0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;-><init>(Lcom/android/systemui/taskmanager/ScreenshotLoadManager;Lcom/android/systemui/taskmanager/TaskInfo;)V

    const-wide/16 v4, 0x15

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    return-void
.end method
