.class Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;
.super Landroid/widget/BaseAdapter;
.source "MultiTaskView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/taskmanager/MultiTaskView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MultiTaskAdapter"
.end annotation


# instance fields
.field mTasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/systemui/taskmanager/MultiTaskView;


# direct methods
.method public constructor <init>(Lcom/android/systemui/taskmanager/MultiTaskView;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->mTasks:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->mTasks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskInfo;

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskInfo;->persistentTaskId:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v1, v1, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    new-instance v0, Lcom/android/systemui/taskmanager/PreviewIconView;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/MultiTaskView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/PreviewIconView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/MultiTaskView;->-get1(Lcom/android/systemui/taskmanager/MultiTaskView;)Lcom/android/systemui/taskmanager/TaskManagerView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->mTasks:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v6

    sub-int/2addr v6, p1

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/taskmanager/TaskInfo;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/taskmanager/PreviewIconView;->setup(Lcom/android/systemui/taskmanager/TaskManagerView;Lcom/android/systemui/taskmanager/TaskItemView;Lcom/android/systemui/taskmanager/TaskInfo;ZZ)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v1, v1, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v1, v1, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskViewTransforms:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/systemui/taskmanager/TaskViewTransform;

    invoke-direct {v2}, Lcom/android/systemui/taskmanager/TaskViewTransform;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/PreviewIconView;->setAlpha(F)V

    return-object v0

    :cond_0
    move v4, v5

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v1, v1, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/PreviewIconView;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/MultiTaskView;->-get1(Lcom/android/systemui/taskmanager/MultiTaskView;)Lcom/android/systemui/taskmanager/TaskManagerView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v2, v2, Lcom/android/systemui/taskmanager/MultiTaskView;->mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->mTasks:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/MultiTaskView$MultiTaskAdapter;->getCount()I

    move-result v6

    sub-int/2addr v6, p1

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/systemui/taskmanager/TaskInfo;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/taskmanager/PreviewIconView;->setup(Lcom/android/systemui/taskmanager/TaskManagerView;Lcom/android/systemui/taskmanager/TaskItemView;Lcom/android/systemui/taskmanager/TaskInfo;ZZ)V

    goto :goto_1

    :cond_2
    move v4, v5

    goto :goto_2
.end method
