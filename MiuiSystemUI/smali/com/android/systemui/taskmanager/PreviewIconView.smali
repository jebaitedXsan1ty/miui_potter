.class public Lcom/android/systemui/taskmanager/PreviewIconView;
.super Landroid/view/View;
.source "PreviewIconView.java"


# instance fields
.field mIsShowLockImg:Z

.field mIsShowShadow:Z

.field mLockBitmap:Landroid/graphics/Bitmap;

.field mPaint:Landroid/graphics/Paint;

.field mScreenshotDrawable:Lcom/android/systemui/taskmanager/RotatableDrawable;

.field mSepBitmap:Landroid/graphics/Bitmap;

.field mShadowHeight:I

.field mTask:Lcom/android/systemui/taskmanager/TaskInfo;

.field mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

.field mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/taskmanager/PreviewIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/taskmanager/PreviewIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/systemui/taskmanager/RotatableDrawable;

    invoke-direct {v0}, Lcom/android/systemui/taskmanager/RotatableDrawable;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mScreenshotDrawable:Lcom/android/systemui/taskmanager/RotatableDrawable;

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowShadow:Z

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowLockImg:Z

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201c8

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mSepBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201c5

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mLockBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0075

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mShadowHeight:I

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/PreviewIconView;->setImportantForAccessibility(I)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 10

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotRatio()F

    move-result v8

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v8, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    const/4 v0, 0x0

    invoke-static {v8, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mScreenshotDrawable:Lcom/android/systemui/taskmanager/RotatableDrawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v8

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/RotatableDrawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mScreenshotDrawable:Lcom/android/systemui/taskmanager/RotatableDrawable;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v5, v5, v1, v2}, Lcom/android/systemui/taskmanager/RotatableDrawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mScreenshotDrawable:Lcom/android/systemui/taskmanager/RotatableDrawable;

    invoke-virtual {v0, p1}, Lcom/android/systemui/taskmanager/RotatableDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowShadow:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mSepBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mSepBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mSepBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {v1, v5, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mShadowHeight:I

    rsub-int/lit8 v3, v3, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getWidth()I

    move-result v4

    invoke-direct {v2, v5, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v1, v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mPaint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v6, v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v9, v0

    sub-float v0, v9, v6

    mul-float/2addr v0, v8

    add-float v7, v6, v0

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowLockImg:Z

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mLockBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v0, v2

    int-to-float v0, v0

    sub-float/2addr v0, v7

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    sub-float/2addr v2, v7

    iget-object v3, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_3
    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mLockBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    add-float/2addr v0, v7

    goto :goto_0
.end method

.method public getTaskItemView()Lcom/android/systemui/taskmanager/TaskItemView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

    return-object v0
.end method

.method public loadScreenshot()V
    .locals 5

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mScreenshotDrawable:Lcom/android/systemui/taskmanager/RotatableDrawable;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TaskInfo;->getScreenshot()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenRotation()I

    move-result v2

    iget-object v3, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v3, v3, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotContentIsPort:Z

    iget-object v4, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-boolean v4, v4, Lcom/android/systemui/taskmanager/TaskInfo;->mIsRealScreenshot:Z

    xor-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/systemui/taskmanager/RotatableDrawable;->setup(Landroid/graphics/Bitmap;IZZ)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->invalidate()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    iget-object v5, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v5}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotRatio()F

    move-result v4

    invoke-static {}, Lmiui/content/res/IconCustomizer;->getCustomizedIconWidth()I

    move-result v3

    invoke-static {}, Lmiui/content/res/IconCustomizer;->getCustomizedIconHeight()I

    move-result v2

    int-to-float v5, v3

    iget-object v6, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v6}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotWidth()I

    move-result v6

    sub-int/2addr v6, v3

    int-to-float v6, v6

    mul-float/2addr v6, v4

    add-float v1, v5, v6

    int-to-float v5, v2

    iget-object v6, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v6}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotHeight()I

    move-result v6

    sub-int/2addr v6, v2

    int-to-float v6, v6

    mul-float/2addr v6, v4

    add-float v0, v5, v6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/android/systemui/taskmanager/PreviewIconView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getTasksView()Lcom/android/systemui/taskmanager/TasksView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/TasksView;->setTouchedDownChild(Lcom/android/systemui/taskmanager/TaskItemView;)V

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public releaseScreenshot()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mScreenshotDrawable:Lcom/android/systemui/taskmanager/RotatableDrawable;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/RotatableDrawable;->releaseBitmap()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskInfo;->releaseScreenshot()V

    return-void
.end method

.method public setIsShowLockImg(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowLockImg:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowLockImg:Z

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setIsShowShadow(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowShadow:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowShadow:Z

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/PreviewIconView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setup(Lcom/android/systemui/taskmanager/TaskManagerView;Lcom/android/systemui/taskmanager/TaskItemView;Lcom/android/systemui/taskmanager/TaskInfo;ZZ)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskManagerView:Lcom/android/systemui/taskmanager/TaskManagerView;

    iput-object p2, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTaskItemView:Lcom/android/systemui/taskmanager/TaskItemView;

    iput-object p3, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iput-boolean p4, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowShadow:Z

    iput-boolean p5, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mIsShowLockImg:Z

    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
