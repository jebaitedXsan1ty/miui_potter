.class public Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;
.super Ljava/lang/Object;
.source "MultiTaskViewAlgorithm.java"


# static fields
.field static px:[F

.field static xp:[F


# instance fields
.field mBetweenAffiliationOffset:I

.field mInitialScrollP:F

.field mMaxScrollP:F

.field mMinScrollP:F

.field mStackRect:Landroid/graphics/Rect;

.field mStackVisibleRect:Landroid/graphics/Rect;

.field mTaskProgressMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field mTaskRect:Landroid/graphics/Rect;

.field mViewRect:Landroid/graphics/Rect;

.field mWithinAffiliationOffset:I

.field scrollOffset:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mViewRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskRect:Landroid/graphics/Rect;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskProgressMap:Ljava/util/HashMap;

    invoke-static {}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->initializeCurve()V

    return-void
.end method

.method public static initializeCurve()V
    .locals 16

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->xp:[F

    if-eqz v9, :cond_0

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    if-eqz v9, :cond_0

    return-void

    :cond_0
    const/16 v9, 0xfb

    new-array v9, v9, [F

    sput-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->xp:[F

    const/16 v9, 0xfb

    new-array v9, v9, [F

    sput-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    const/16 v9, 0xfb

    new-array v2, v9, [F

    const v6, 0x3b83126f    # 0.004f

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0xfa

    if-gt v8, v9, :cond_1

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v9, v10

    aput v9, v2, v8

    const v9, 0x3b83126f    # 0.004f

    add-float/2addr v7, v9

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    const/16 v9, 0xfb

    new-array v0, v9, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v9, v0, v10

    const/4 v8, 0x1

    :goto_1
    const/16 v9, 0xfa

    if-ge v8, v9, :cond_2

    aget v9, v2, v8

    add-int/lit8 v10, v8, -0x1

    aget v10, v2, v10

    sub-float/2addr v9, v10

    float-to-double v10, v9

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    const-wide v12, 0x3f70624de0000000L    # 0.004000000189989805

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    add-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v9, v10

    aput v9, v0, v8

    aget v9, v0, v8

    add-float/2addr v4, v9

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    const/4 v10, 0x0

    const/4 v11, 0x0

    aput v10, v9, v11

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    const/high16 v10, 0x3f800000    # 1.0f

    const/16 v11, 0xfa

    aput v10, v9, v11

    const/4 v8, 0x1

    :goto_2
    const/16 v9, 0xfa

    if-gt v8, v9, :cond_3

    aget v9, v0, v8

    div-float/2addr v9, v4

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    add-float/2addr v3, v9

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    aput v3, v9, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_3
    const/4 v8, 0x0

    const/4 v3, 0x0

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->xp:[F

    const/4 v10, 0x0

    const/4 v11, 0x0

    aput v10, v9, v11

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->xp:[F

    const/high16 v10, 0x3f800000    # 1.0f

    const/16 v11, 0xfa

    aput v10, v9, v11

    const/4 v5, 0x0

    :goto_3
    const/16 v9, 0xfa

    if-ge v5, v9, :cond_7

    :goto_4
    const/16 v9, 0xfa

    if-ge v8, v9, :cond_4

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    aget v9, v9, v8

    cmpl-float v9, v9, v3

    if-lez v9, :cond_5

    :cond_4
    if-nez v8, :cond_6

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->xp:[F

    const/4 v10, 0x0

    aput v10, v9, v5

    :goto_5
    const v9, 0x3b83126f    # 0.004f

    add-float/2addr v3, v9

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_6
    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    add-int/lit8 v10, v8, -0x1

    aget v9, v9, v10

    sub-float v9, v3, v9

    sget-object v10, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    aget v10, v10, v8

    sget-object v11, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    add-int/lit8 v12, v8, -0x1

    aget v11, v11, v12

    sub-float/2addr v10, v11

    div-float v1, v9, v10

    add-int/lit8 v9, v8, -0x1

    int-to-float v9, v9

    add-float/2addr v9, v1

    const v10, 0x3b83126f    # 0.004f

    mul-float v7, v9, v10

    sget-object v9, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->xp:[F

    aput v7, v9, v5

    goto :goto_5

    :cond_7
    return-void
.end method

.method public static scaleRectAboutCenter(Landroid/graphics/Rect;F)V
    .locals 5

    const/high16 v4, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    neg-int v2, v0

    neg-int v3, v1

    invoke-virtual {p0, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    iget v2, p0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    :cond_0
    return-void
.end method


# virtual methods
.method computeMinMaxScroll(I)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskProgressMap:Ljava/util/HashMap;

    invoke-virtual {v14}, Ljava/util/HashMap;->clear()V

    const/4 v14, 0x1

    move/from16 v0, p1

    if-ge v0, v14, :cond_0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMaxScrollP:F

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMinScrollP:F

    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskRect:Landroid/graphics/Rect;

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->screenYToCurveProgress(I)F

    move-result v3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mWithinAffiliationOffset:I

    sub-int/2addr v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->screenYToCurveProgress(I)F

    move-result v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->curveProgressToScale(F)F

    move-result v11

    const/high16 v14, 0x3f800000    # 1.0f

    sub-float/2addr v14, v11

    int-to-float v15, v13

    mul-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    float-to-int v12, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mWithinAffiliationOffset:I

    sub-int/2addr v14, v15

    add-int/2addr v14, v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->screenYToCurveProgress(I)F

    move-result v10

    sub-float v9, v3, v10

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mBetweenAffiliationOffset:I

    sub-int/2addr v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->screenYToCurveProgress(I)F

    move-result v14

    sub-float v5, v3, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v14, v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->screenYToCurveProgress(I)F

    move-result v14

    sub-float v8, v3, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    sub-int/2addr v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->screenYToCurveProgress(I)F

    move-result v14

    sub-float v6, v3, v14

    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    :goto_0
    move/from16 v0, p1

    if-ge v1, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskProgressMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v14, p1, -0x1

    if-ge v1, v14, :cond_1

    move v7, v5

    add-float/2addr v4, v5

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/high16 v14, 0x3f800000    # 1.0f

    sub-float/2addr v14, v8

    sub-float/2addr v14, v6

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->scrollOffset:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->scrollOffset:F

    sub-float v14, v4, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMaxScrollP:F

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMinScrollP:F

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMaxScrollP:F

    invoke-static {v14, v15}, Ljava/lang/Math;->max(FF)F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mInitialScrollP:F

    return-void
.end method

.method public computeRects(IILandroid/graphics/Rect;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mViewRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v7, v7, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    invoke-virtual {v3, p3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v3, p3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mViewRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    const v4, 0x3f733333    # 0.95f

    invoke-static {v3, v4}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->scaleRectAboutCenter(Landroid/graphics/Rect;F)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    add-int v1, v3, v4

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int v5, v1, v2

    iget-object v6, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v0

    invoke-virtual {v3, v1, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    iput v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mWithinAffiliationOffset:I

    iget-object v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3e99999a    # 0.3f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mBetweenAffiliationOffset:I

    return-void
.end method

.method curveProgressToScale(F)F
    .locals 4

    const v3, 0x3f666666    # 0.9f

    const/4 v2, 0x0

    cmpg-float v2, p1, v2

    if-gez v2, :cond_0

    return v3

    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, p1, v2

    if-lez v2, :cond_1

    const v2, 0x3fa66666    # 1.3f

    return v2

    :cond_1
    const v1, 0x3ecccccc    # 0.39999998f

    const v2, 0x3ecccccc    # 0.39999998f

    mul-float/2addr v2, p1

    add-float v0, v3, v2

    return v0
.end method

.method curveProgressToScreenY(F)I
    .locals 8

    const/4 v6, 0x0

    cmpg-float v6, p1, v6

    if-ltz v6, :cond_0

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v6, p1, v6

    if-lez v6, :cond_1

    :cond_0
    iget-object v6, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, p1

    float-to-int v7, v7

    add-int/2addr v6, v7

    return v6

    :cond_1
    const/high16 v6, 0x437a0000    # 250.0f

    mul-float v3, p1, v6

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v1, v6

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v0, v6

    const/4 v5, 0x0

    const/16 v6, 0xfa

    if-ge v1, v6, :cond_2

    if-eq v0, v1, :cond_2

    int-to-float v6, v1

    sub-float v6, v3, v6

    sub-int v7, v0, v1

    int-to-float v7, v7

    div-float v2, v6, v7

    sget-object v6, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->xp:[F

    aget v6, v6, v0

    sget-object v7, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->xp:[F

    aget v7, v7, v1

    sub-float/2addr v6, v7

    mul-float v5, v6, v2

    :cond_2
    sget-object v6, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->xp:[F

    aget v6, v6, v1

    add-float v4, v6, v5

    iget-object v6, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v4

    float-to-int v7, v7

    add-int/2addr v6, v7

    return v6
.end method

.method public getStackTransform(FFLcom/android/systemui/taskmanager/TaskViewTransform;Lcom/android/systemui/taskmanager/TaskViewTransform;)Lcom/android/systemui/taskmanager/TaskViewTransform;
    .locals 10

    const/4 v9, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    sub-float v3, p1, p2

    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v8, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    cmpl-float v6, v3, v7

    if-lez v6, :cond_0

    invoke-virtual {p3}, Lcom/android/systemui/taskmanager/TaskViewTransform;->reset()V

    iget-object v6, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->rect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    return-object p3

    :cond_0
    cmpg-float v6, v3, v8

    if-gez v6, :cond_1

    if-eqz p4, :cond_1

    iget v6, p4, Lcom/android/systemui/taskmanager/TaskViewTransform;->p:F

    invoke-static {v6, v8}, Ljava/lang/Float;->compare(FF)I

    move-result v6

    if-gtz v6, :cond_1

    invoke-virtual {p3}, Lcom/android/systemui/taskmanager/TaskViewTransform;->reset()V

    iget-object v6, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->rect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    return-object p3

    :cond_1
    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->curveProgressToScale(F)F

    move-result v4

    sub-float v6, v7, v4

    iget-object v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    float-to-int v5, v6

    const/4 v1, 0x0

    const/16 v0, 0x64

    iput v4, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->scale:F

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->curveProgressToScreenY(F)I

    move-result v6

    iget-object v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v7

    sub-int/2addr v6, v5

    iput v6, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->translationY:I

    const/16 v6, 0x64

    int-to-float v6, v6

    mul-float/2addr v6, v2

    add-float/2addr v6, v8

    invoke-static {v8, v6}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->translationZ:F

    iget-object v6, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->rect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v6, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->rect:Landroid/graphics/Rect;

    iget v7, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->translationY:I

    invoke-virtual {v6, v9, v7}, Landroid/graphics/Rect;->offset(II)V

    iget-object v6, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->rect:Landroid/graphics/Rect;

    iget v7, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->scale:F

    invoke-static {v6, v7}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->scaleRectAboutCenter(Landroid/graphics/Rect;F)V

    const/4 v6, 0x1

    iput-boolean v6, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->visible:Z

    iput v3, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->p:F

    iget v6, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMaxScrollP:F

    sub-float/2addr v6, p2

    iget v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMaxScrollP:F

    iget v8, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mMinScrollP:F

    sub-float/2addr v7, v8

    div-float/2addr v6, v7

    iput v6, p3, Lcom/android/systemui/taskmanager/TaskViewTransform;->alpha:F

    return-object p3
.end method

.method public getStackTransform(IFLcom/android/systemui/taskmanager/TaskViewTransform;Lcom/android/systemui/taskmanager/TaskViewTransform;)Lcom/android/systemui/taskmanager/TaskViewTransform;
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskProgressMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Lcom/android/systemui/taskmanager/TaskViewTransform;->reset()V

    return-object p3

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mTaskProgressMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->getStackTransform(FFLcom/android/systemui/taskmanager/TaskViewTransform;Lcom/android/systemui/taskmanager/TaskViewTransform;)Lcom/android/systemui/taskmanager/TaskViewTransform;

    move-result-object v0

    return-object v0
.end method

.method screenYToCurveProgress(I)F
    .locals 8

    iget-object v6, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, p1, v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->mStackVisibleRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    div-float v1, v6, v7

    const/4 v6, 0x0

    cmpg-float v6, v1, v6

    if-ltz v6, :cond_0

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v6, v1, v6

    if-lez v6, :cond_1

    :cond_0
    return v1

    :cond_1
    const/high16 v6, 0x437a0000    # 250.0f

    mul-float v5, v1, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v3, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    const/4 v0, 0x0

    const/16 v6, 0xfa

    if-ge v3, v6, :cond_2

    if-eq v2, v3, :cond_2

    int-to-float v6, v3

    sub-float v6, v5, v6

    sub-int v7, v2, v3

    int-to-float v7, v7

    div-float v4, v6, v7

    sget-object v6, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    aget v6, v6, v2

    sget-object v7, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    aget v7, v7, v3

    sub-float/2addr v6, v7

    mul-float v0, v6, v4

    :cond_2
    sget-object v6, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->px:[F

    aget v6, v6, v3

    add-float/2addr v6, v0

    return v6
.end method
