.class public Lcom/android/systemui/taskmanager/RecentTasksManager;
.super Ljava/lang/Object;
.source "RecentTasksManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;,
        Lcom/android/systemui/taskmanager/RecentTasksManager$ThumbnailLoadedCallback;
    }
.end annotation


# static fields
.field static final DEBUG:Z

.field private static sNeedForceClosePkgs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSomeDefaultNoKillPkgs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static sTtsEngines:Landroid/speech/tts/TtsEngines;


# instance fields
.field mBgHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mDefaultThumbnailBackground:Landroid/graphics/Bitmap;

.field private mHandler:Landroid/os/Handler;

.field private mIconDpi:I

.field private mIsCleaning:Z

.field private mIsLoadThumbnail:Z

.field private mIsLoadedAllPrevious:Z

.field private mMainHandler:Landroid/os/Handler;

.field private mNoKillProcesses:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mNotCompoundPkgs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPm:Landroid/content/pm/PackageManager;

.field final mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

.field private mTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTasksLoadTime:J

.field private mThumbnailLoadedCallback:Lcom/android/systemui/taskmanager/RecentTasksManager$ThumbnailLoadedCallback;

.field private mThumbnailLoader:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field mTopRunningPkg:Ljava/lang/String;

.field private mWebAppDAO:Lmiui/browser/webapps/WebAppDAO;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/taskmanager/RecentTasksManager;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/taskmanager/RecentTasksManager;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/taskmanager/RecentTasksManager;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/taskmanager/RecentTasksManager;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/systemui/taskmanager/RecentTasksManager;)Lcom/android/systemui/taskmanager/RecentTasksManager$ThumbnailLoadedCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mThumbnailLoadedCallback:Lcom/android/systemui/taskmanager/RecentTasksManager$ThumbnailLoadedCallback;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->DEBUG:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->DEBUG:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->sNeedForceClosePkgs:Ljava/util/List;

    sget-object v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->sNeedForceClosePkgs:Ljava/util/List;

    const-string/jumbo v1, "com.miui.player"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 11

    const/16 v10, 0xa0

    const/16 v9, 0x140

    const/16 v7, 0xf0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mNotCompoundPkgs:Ljava/util/List;

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mNoKillProcesses:Ljava/util/HashSet;

    const/4 v8, 0x0

    iput-object v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mWebAppDAO:Lmiui/browser/webapps/WebAppDAO;

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIsCleaning:Z

    iput-object p1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIsLoadThumbnail:Z

    new-instance v8, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    invoke-direct {v8}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;-><init>()V

    iput-object v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f0a0001

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v2, v8, Landroid/util/DisplayMetrics;->densityDpi:I

    if-eqz v4, :cond_6

    const/16 v8, 0x78

    if-ne v2, v8, :cond_3

    iput v10, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIconDpi:I

    :cond_0
    :goto_0
    if-eqz v4, :cond_7

    :goto_1
    iput v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIconDpi:I

    const v7, 0x1050002

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const v7, 0x1050001

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v7, 0x7f020330

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget-boolean v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIsLoadThumbnail:Z

    if-eqz v7, :cond_1

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v3, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mDefaultThumbnailBackground:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mDefaultThumbnailBackground:Landroid/graphics/Bitmap;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    :cond_1
    iget-boolean v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIsLoadThumbnail:Z

    if-eqz v7, :cond_2

    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    iput-object v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mHandler:Landroid/os/Handler;

    :cond_2
    iget-object v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lmiui/browser/webapps/WebAppDAO;->getInstance(Landroid/content/Context;)Lmiui/browser/webapps/WebAppDAO;

    move-result-object v7

    iput-object v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mWebAppDAO:Lmiui/browser/webapps/WebAppDAO;

    new-instance v7, Landroid/speech/tts/TtsEngines;

    iget-object v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/speech/tts/TtsEngines;-><init>(Landroid/content/Context;)V

    sput-object v7, Lcom/android/systemui/taskmanager/RecentTasksManager;->sTtsEngines:Landroid/speech/tts/TtsEngines;

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->addNotCompoundPkgs()V

    iget-object v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    iput-object v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mPm:Landroid/content/pm/PackageManager;

    return-void

    :cond_3
    if-ne v2, v10, :cond_4

    iput v7, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIconDpi:I

    goto :goto_0

    :cond_4
    if-ne v2, v7, :cond_5

    iput v9, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIconDpi:I

    goto :goto_0

    :cond_5
    if-ne v2, v9, :cond_0

    iput v9, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIconDpi:I

    goto :goto_0

    :cond_6
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v8, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIconDpi:I

    goto :goto_0

    :cond_7
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->densityDpi:I

    goto :goto_1
.end method

.method private addNotCompoundPkgs()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mNotCompoundPkgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mNotCompoundPkgs:Ljava/util/List;

    const-string/jumbo v1, "com.android.browser"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private static varargs addSomeNoKillPkgs(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x0

    array-length v2, p1

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    if-eqz v0, :cond_0

    sget-object v3, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "com.android.deskclock"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "com.android.providers.media"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "com.android.contacts"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "com.android.mms"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "android"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "com.lbe.security.miui"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "com.miui.miwallpaper"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "com.miui.miwallpaper"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "com.google.android.marvin.talkback"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    const-string/jumbo v2, "com.miui.contentextension"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private doClear(Ljava/util/List;ZI)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZI)V"
        }
    .end annotation

    new-instance v0, Lcom/android/systemui/taskmanager/RecentTasksManager$2;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/android/systemui/taskmanager/RecentTasksManager$2;-><init>(Lcom/android/systemui/taskmanager/RecentTasksManager;ILjava/util/List;Z)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mBgHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public static getFormatedMemory(J)Ljava/lang/String;
    .locals 12

    const-wide/16 v8, 0x400

    const-wide/16 v10, 0xa

    div-long v4, p0, v8

    cmp-long v6, v4, v8

    if-gez v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " M"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    :cond_0
    mul-long v6, v4, v10

    div-long v2, v6, v8

    div-long v0, v2, v10

    rem-long v6, v2, v10

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    rem-long v8, v2, v10

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " G"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " G"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private getProcessByPackageName(Landroid/app/ActivityManager;Ljava/lang/String;I)Landroid/app/ActivityManager$RunningAppProcessInfo;
    .locals 9

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    if-ne p3, v5, :cond_0

    iget-object v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    const/4 v5, 0x0

    array-length v7, v6

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v2, v6, v5

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object v4, v0

    return-object v0

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    return-object v4
.end method

.method public static getToastMsg(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x0

    sub-long v2, p3, p1

    long-to-int v2, v2

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/4 v0, 0x0

    const/16 v2, 0x2800

    if-le v1, v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    int-to-long v4, v1

    invoke-static {v4, v5}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getFormatedMemory(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {p3, p4}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getFormatedMemory(J)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    const v4, 0x1108008d

    invoke-virtual {v2, v4, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1108008e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getTtsEngines(Landroid/content/Context;)Landroid/speech/tts/TtsEngines;
    .locals 2

    const-class v1, Lcom/android/systemui/taskmanager/RecentTasksManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->sTtsEngines:Landroid/speech/tts/TtsEngines;

    if-nez v0, :cond_0

    new-instance v0, Landroid/speech/tts/TtsEngines;

    invoke-direct {v0, p0}, Landroid/speech/tts/TtsEngines;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->sTtsEngines:Landroid/speech/tts/TtsEngines;

    :cond_0
    sget-object v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->sTtsEngines:Landroid/speech/tts/TtsEngines;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private killProcess(Landroid/app/ActivityManager;Ljava/lang/String;II)V
    .locals 8

    const/4 v0, 0x0

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v4

    :try_start_0
    iget-object v5, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p2, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    :try_start_1
    sget-object v5, Lcom/android/systemui/taskmanager/RecentTasksManager;->sNeedForceClosePkgs:Ljava/util/List;

    if-eqz v5, :cond_0

    sget-object v5, Lcom/android/systemui/taskmanager/RecentTasksManager;->sNeedForceClosePkgs:Ljava/util/List;

    invoke-interface {v5, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    if-eqz v0, :cond_3

    iget v5, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-static {v5, p2}, Landroid/miui/AppOpsUtils;->getApplicationAutoStart(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_3

    :cond_1
    sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v6, 0x0

    aput p3, v5, v6

    const-string/jumbo v6, "RecentTasksManager"

    const/4 v7, 0x1

    invoke-interface {v4, v5, v6, v7}, Landroid/app/IActivityManager;->killPids([ILjava/lang/String;Z)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-interface {v4, p2, p4}, Landroid/app/IActivityManager;->forceStopPackage(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    :cond_3
    :try_start_3
    invoke-interface {v4, p2, p4}, Landroid/app/IActivityManager;->killBackgroundProcesses(Ljava/lang/String;I)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    const-string/jumbo v5, "RecentTasksLoader"

    const-string/jumbo v6, "RecentTasksManager killPid "

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private declared-synchronized loadRecentTasks(I)V
    .locals 27

    monitor-enter p0

    const/16 v25, 0x0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->cancelLoadingThumbnails()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasksLoadTime:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/ActivityManager;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v2

    const/4 v3, 0x6

    move/from16 v0, p1

    invoke-virtual {v10, v0, v3, v2}, Landroid/app/ActivityManager;->getRecentTasksForUser(III)Ljava/util/List;

    move-result-object v23

    if-nez v23, :cond_0

    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, p1

    if-ge v2, v0, :cond_3

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIsLoadedAllPrevious:Z

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.category.HOME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mPm:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v8

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v18

    if-eqz v18, :cond_1

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/app/ActivityManager$RunningTaskInfo;

    move-object/from16 v0, v24

    iget v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    move/from16 v25, v0

    move-object/from16 v0, v24

    iget-object v2, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTopRunningPkg:Ljava/lang/String;

    :cond_1
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v19

    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_1
    move/from16 v0, v19

    if-ge v13, v0, :cond_9

    move/from16 v0, p1

    if-ge v14, v0, :cond_9

    move-object/from16 v0, v23

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/app/ActivityManager$RecentTaskInfo;

    move-object/from16 v0, v22

    iget-object v2, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v2

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    if-eqz v2, :cond_4

    :cond_2
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    move-object/from16 v0, v22

    iget v3, v0, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    move-object/from16 v0, v22

    iget v4, v0, Landroid/app/ActivityManager$RecentTaskInfo;->persistentId:I

    move-object/from16 v0, v22

    iget-object v5, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    move-object/from16 v0, v22

    iget-object v6, v0, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    move-object/from16 v0, v22

    iget-object v7, v0, Landroid/app/ActivityManager$RecentTaskInfo;->description:Ljava/lang/CharSequence;

    invoke-static/range {v22 .. v22}, Lcom/android/systemui/taskmanager/RecentTasksUserId;->getRecentUserId(Landroid/app/ActivityManager$RecentTaskInfo;)I

    move-result v9

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Lcom/android/systemui/taskmanager/RecentTasksManager;->createTaskDescription(IILandroid/content/Intent;Landroid/content/ComponentName;Ljava/lang/CharSequence;Landroid/content/pm/ActivityInfo;I)Lcom/android/systemui/taskmanager/TaskInfo;

    move-result-object v15

    if-eqz v15, :cond_2

    iget v2, v15, Lcom/android/systemui/taskmanager/TaskInfo;->persistentTaskId:I

    move/from16 v0, v25

    if-ne v2, v0, :cond_5

    const/4 v2, 0x1

    iput-boolean v2, v15, Lcom/android/systemui/taskmanager/TaskInfo;->mTopRunning:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    :try_start_1
    iget-object v2, v15, Lcom/android/systemui/taskmanager/TaskInfo;->packageName:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/android/systemui/taskmanager/RecentTasksUserId;->getRecentUserId(Landroid/app/ActivityManager$RecentTaskInfo;)I

    move-result v3

    invoke-static {v2, v3}, Lmiui/process/ProcessManager;->isLockedApplication(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    iput-boolean v2, v15, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    :goto_3
    const/4 v11, 0x0

    :try_start_2
    invoke-virtual {v15}, Lcom/android/systemui/taskmanager/TaskInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mNotCompoundPkgs:Ljava/util/List;

    move-object/from16 v0, v16

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const/16 v17, 0x0

    :goto_4
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v17

    if-ge v0, v2, :cond_8

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static/range {v22 .. v22}, Lcom/android/systemui/taskmanager/RecentTasksUserId;->getRecentUserId(Landroid/app/ActivityManager$RecentTaskInfo;)I

    move-result v2

    move-object/from16 v0, v21

    iget v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;->userId:I

    if-ne v2, v3, :cond_7

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    move-object/from16 v0, v21

    iget v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;->taskIndex:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/android/systemui/taskmanager/TaskInfo;

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/android/systemui/taskmanager/TaskInfo;->addCompoundedTask(Lcom/android/systemui/taskmanager/TaskInfo;)V

    :cond_7
    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    :cond_8
    if-nez v11, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v21, Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;

    invoke-direct/range {v21 .. v21}, Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;-><init>()V

    invoke-virtual {v15}, Lcom/android/systemui/taskmanager/TaskInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v21

    iput v14, v0, Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;->taskIndex:I

    invoke-static/range {v22 .. v22}, Lcom/android/systemui/taskmanager/RecentTasksUserId;->getRecentUserId(Landroid/app/ActivityManager$RecentTaskInfo;)I

    move-result v2

    move-object/from16 v0, v21

    iput v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager$TaskPackageInfo;->userId:I

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIsLoadThumbnail:Z

    if-eqz v2, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadThumbnailsInBackground(Ljava/util/ArrayList;)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/systemui/taskmanager/RecentTasksManager;->updateProtectedPkgs(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :catch_0
    move-exception v12

    goto/16 :goto_3
.end method

.method private loadRecentTasksIfNull()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadRecentTasks()V

    :cond_0
    return-void
.end method

.method private loadThumbnailsInBackground(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    sget-boolean v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "RecentTasksLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Showing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " tasks"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadThumbnail(Lcom/android/systemui/taskmanager/TaskInfo;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    new-instance v0, Lcom/android/systemui/taskmanager/RecentTasksManager$1;

    invoke-direct {v0, p0, p1}, Lcom/android/systemui/taskmanager/RecentTasksManager$1;-><init>(Lcom/android/systemui/taskmanager/RecentTasksManager;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mThumbnailLoader:Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mThumbnailLoader:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method

.method private showCleanEndMsg(I)V
    .locals 10

    const/4 v1, -0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v6, Lmiui/widget/CircleProgressBar;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-direct {v6, v2}, Lmiui/widget/CircleProgressBar;-><init>(Landroid/content/Context;)V

    new-array v2, v5, [I

    const v3, 0x7f02002b

    aput v3, v2, v4

    new-array v3, v5, [I

    const v8, 0x7f02002c

    aput v8, v3, v4

    const/4 v8, 0x0

    invoke-virtual {v6, v2, v3, v8}, Lmiui/widget/CircleProgressBar;->setDrawablesForLevels([I[I[I)V

    invoke-static {}, Lmiui/util/HardwareInfo;->getTotalPhysicalMemory()J

    move-result-wide v2

    const-wide/16 v8, 0x400

    div-long/2addr v2, v8

    long-to-int v2, v2

    invoke-virtual {v6, v2}, Lmiui/widget/CircleProgressBar;->setMax(I)V

    invoke-virtual {v6}, Lmiui/widget/CircleProgressBar;->getMax()I

    move-result v2

    sub-int/2addr v2, p1

    invoke-virtual {v6, v2}, Lmiui/widget/CircleProgressBar;->setProgress(I)V

    iget-object v2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d6

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/16 v1, 0x51

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0178

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    const v1, 0x7f0e001c

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    const/16 v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    invoke-interface {v7, v6, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Lcom/android/systemui/taskmanager/RecentTasksManager$3;

    invoke-direct {v1, p0, v6, v7, p1}, Lcom/android/systemui/taskmanager/RecentTasksManager$3;-><init>(Lcom/android/systemui/taskmanager/RecentTasksManager;Lmiui/widget/CircleProgressBar;Landroid/view/WindowManager;I)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v6, v1, v2, v3}, Lmiui/widget/CircleProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static updateProtectedPkgs(Landroid/content/Context;)V
    .locals 11

    const-class v8, Lcom/android/systemui/taskmanager/RecentTasksManager;

    monitor-enter v8

    :try_start_0
    sget-object v7, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    new-instance v7, Landroid/content/Intent;

    const-string/jumbo v9, "android.intent.action.MAIN"

    invoke-direct {v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, "android.intent.category.HOME"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v7, v4, v9}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v7

    iget-object v0, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v9, "default_input_method"

    invoke-static {v7, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    const/16 v7, 0x2f

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    const/4 v9, 0x0

    invoke-virtual {v1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :cond_0
    const/4 v3, 0x0

    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v9, "current_live_wallpaper_packagename"

    const/4 v10, -0x2

    invoke-static {v7, v9, v10}, Landroid/provider/MiuiSettings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    :cond_3
    const/4 v5, 0x0

    invoke-static {p0}, Lcom/android/systemui/taskmanager/Utils;->isTalkBackMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-static {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getTtsEngines(Landroid/content/Context;)Landroid/speech/tts/TtsEngines;

    move-result-object v7

    invoke-virtual {v7}, Landroid/speech/tts/TtsEngines;->getDefaultEngine()Ljava/lang/String;

    move-result-object v5

    :cond_4
    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v0, v7, v9

    const/4 v9, 0x1

    aput-object v2, v7, v9

    const/4 v9, 0x2

    aput-object v3, v7, v9

    const/4 v9, 0x3

    aput-object v5, v7, v9

    invoke-static {p0, v7}, Lcom/android/systemui/taskmanager/RecentTasksManager;->addSomeNoKillPkgs(Landroid/content/Context;[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v8

    return-void

    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7
.end method


# virtual methods
.method public cancelLoadingThumbnails()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mThumbnailLoader:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mThumbnailLoader:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mThumbnailLoader:Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method createTaskDescription(IILandroid/content/Intent;Landroid/content/ComponentName;Ljava/lang/CharSequence;Landroid/content/pm/ActivityInfo;I)Lcom/android/systemui/taskmanager/TaskInfo;
    .locals 18

    new-instance v15, Landroid/content/Intent;

    move-object/from16 v0, p3

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    if-eqz p4, :cond_0

    move-object/from16 v0, p4

    invoke-virtual {v15, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :cond_0
    if-nez p6, :cond_1

    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "android.intent.category.HOME"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mPm:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object p6

    :cond_1
    invoke-virtual {v15}, Landroid/content/Intent;->getFlags()I

    move-result v3

    const v4, -0x200001

    and-int/2addr v3, v4

    const/high16 v4, 0x10000000

    or-int/2addr v3, v4

    invoke-virtual {v15, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mPm:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    move/from16 v0, p7

    invoke-virtual {v3, v15, v4, v0}, Landroid/content/pm/PackageManager;->resolveActivityAsUser(Landroid/content/Intent;II)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    if-eqz v6, :cond_a

    iget-object v14, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    const/16 v16, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mWebAppDAO:Lmiui/browser/webapps/WebAppDAO;

    invoke-virtual {v3, v14}, Lmiui/browser/webapps/WebAppDAO;->get(Landroid/content/pm/ActivityInfo;)Lmiui/browser/webapps/WebAppInfo;

    move-result-object v17

    if-eqz v17, :cond_2

    move-object/from16 v0, v17

    iget-object v0, v0, Lmiui/browser/webapps/WebAppInfo;->mLabel:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lmiui/browser/webapps/WebAppInfo;->getIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    :cond_2
    if-nez v16, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v14, v3}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v16

    :cond_3
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-nez v3, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getFullResDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    :cond_4
    if-nez v13, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mPm:Landroid/content/pm/PackageManager;

    const-wide/32 v8, 0x2932e00

    invoke-static {v3, v6, v4, v8, v9}, Lmiui/maml/util/AppIconsHelper;->getIconDrawable(Landroid/content/Context;Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;J)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    :cond_5
    if-eqz v16, :cond_9

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_9

    if-eqz v13, :cond_9

    sget-boolean v3, Lcom/android/systemui/taskmanager/RecentTasksManager;->DEBUG:Z

    if-eqz v3, :cond_6

    const-string/jumbo v3, "RecentTasksLoader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "creating activity desc for id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", label="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v2, Lcom/android/systemui/taskmanager/TaskInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    iget-object v8, v14, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v15}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    const/4 v11, 0x0

    move/from16 v4, p1

    move/from16 v5, p2

    move-object/from16 v7, p3

    move-object/from16 v9, p5

    move/from16 v12, p7

    invoke-direct/range {v2 .. v12}, Lcom/android/systemui/taskmanager/TaskInfo;-><init>(Lcom/android/systemui/taskmanager/ScreenshotLoadManager;IILandroid/content/pm/ResolveInfo;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/CharSequence;Landroid/content/ComponentName;ZI)V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/android/systemui/taskmanager/TaskInfo;->setLabel(Ljava/lang/CharSequence;)V

    invoke-static/range {p7 .. p7}, Lmiui/securityspace/XSpaceUserHandle;->isXSpaceUserId(I)Z

    move-result v3

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v13}, Lmiui/securityspace/XSpaceUserHandle;->getXSpaceIcon(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    :cond_7
    invoke-virtual {v2, v13}, Lcom/android/systemui/taskmanager/TaskInfo;->setIcon(Landroid/graphics/drawable/Drawable;)V

    if-eqz p6, :cond_8

    move-object/from16 v0, p6

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v15}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, p6

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v15}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x0

    return-object v3

    :cond_8
    return-object v2

    :cond_9
    sget-boolean v3, Lcom/android/systemui/taskmanager/RecentTasksManager;->DEBUG:Z

    if-eqz v3, :cond_a

    const-string/jumbo v3, "RecentTasksLoader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "SKIPPING item "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const/4 v3, 0x0

    return-object v3
.end method

.method getFullResDefaultActivityIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10d0000

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getFullResIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method getFullResIcon(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    :try_start_0
    iget v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIconDpi:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getFullResDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    return-object v1
.end method

.method getRecentTasks()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasksLoadTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7d0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadRecentTasks()V

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v0, :cond_1

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->setup(Ljava/util/ArrayList;Z)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadRecentTasksIfNull()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isCleaning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mIsCleaning:Z

    return v0
.end method

.method public isTaskActive(I)Z
    .locals 6

    iget-object v4, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "activity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/16 v4, 0x15

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget v4, v2, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    if-ne v4, p1, :cond_0

    const/4 v4, 0x1

    return v4

    :cond_1
    const/4 v4, 0x0

    return v4
.end method

.method public loadRecentTasks()V
    .locals 1

    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadRecentTasks(I)V

    return-void
.end method

.method loadThumbnail(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 4

    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    iget v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->persistentTaskId:I

    invoke-static {v1, v2}, Lcom/android/systemui/SystemUICompatibility;->loadThumbnail(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sget-boolean v1, Lcom/android/systemui/taskmanager/RecentTasksManager;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "RecentTasksLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Loaded bitmap for task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-enter p1

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/android/systemui/taskmanager/TaskInfo;->setThumbnail(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p1

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mDefaultThumbnailBackground:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v1}, Lcom/android/systemui/taskmanager/TaskInfo;->setThumbnail(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p1

    throw v1
.end method

.method public removeAllTask()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/systemui/taskmanager/RecentTasksManager;->removeAllTask(ZZLjava/util/List;I)V

    return-void
.end method

.method public removeAllTask(ZZLjava/util/List;I)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    const-string/jumbo v13, "RecentTasksLoader"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "removeAllTask, cleanByRecents="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    invoke-static {}, Lmiui/util/HardwareInfo;->getFreeMemory()J

    move-result-wide v14

    const-wide/16 v16, 0x400

    div-long v14, v14, v16

    long-to-int v7, v14

    :cond_0
    const v13, 0x7fffffff

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadRecentTasks(I)V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/android/systemui/recents/Recents;->getSystemServices()Lcom/android/systemui/recents/misc/SystemServicesProxy;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/systemui/recents/misc/SystemServicesProxy;->hasDockedTask()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v10

    const/4 v13, 0x3

    invoke-interface {v10, v13}, Landroid/app/IActivityManager;->getStackInfo(I)Landroid/app/ActivityManager$StackInfo;

    move-result-object v4

    iget-object v5, v4, Landroid/app/ActivityManager$StackInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v5, :cond_2

    iget-boolean v13, v4, Landroid/app/ActivityManager$StackInfo;->visible:Z

    if-eqz v13, :cond_2

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    const/4 v13, 0x1

    invoke-interface {v10, v13}, Landroid/app/IActivityManager;->getStackInfo(I)Landroid/app/ActivityManager$StackInfo;

    move-result-object v8

    iget-object v9, v8, Landroid/app/ActivityManager$StackInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v9, :cond_3

    iget-boolean v13, v8, Landroid/app/ActivityManager$StackInfo;->visible:Z

    if-eqz v13, :cond_3

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p4

    invoke-direct {v0, v11, v1, v2}, Lcom/android/systemui/taskmanager/RecentTasksManager;->doClear(Ljava/util/List;ZI)V

    if-eqz p1, :cond_4

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/systemui/taskmanager/RecentTasksManager;->showCleanEndMsg(I)V

    :cond_4
    return-void

    :catch_0
    move-exception v6

    const-string/jumbo v13, "RecentTasksLoader"

    const-string/jumbo v14, "getProtectedTaskPkg"

    invoke-static {v13, v14, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removeTask(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 10

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadRecentTasksIfNull()V

    iget-object v6, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mContext:Landroid/content/Context;

    const-string/jumbo v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iget-object v6, p1, Lcom/android/systemui/taskmanager/TaskInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v6, Lcom/android/systemui/taskmanager/RecentTasksManager;->sSomeDefaultNoKillPkgs:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    return-void

    :cond_0
    iget v6, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    invoke-direct {p0, v0, v2, v6}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getProcessByPackageName(Landroid/app/ActivityManager;Ljava/lang/String;I)Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v6, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mTasks:Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/systemui/taskmanager/TaskInfo;

    iget v6, v4, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    iget v7, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    if-ne v6, v7, :cond_1

    iget-object v7, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    const/4 v6, 0x0

    array-length v8, v7

    :goto_0
    if-ge v6, v8, :cond_1

    aget-object v1, v7, v6

    iget-object v9, v4, Lcom/android/systemui/taskmanager/TaskInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    return-void

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    iget v6, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    iget v7, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    invoke-direct {p0, v0, v2, v6, v7}, Lcom/android/systemui/taskmanager/RecentTasksManager;->killProcess(Landroid/app/ActivityManager;Ljava/lang/String;II)V

    :cond_4
    return-void
.end method

.method public setBgHandler(Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mBgHandler:Landroid/os/Handler;

    return-void
.end method

.method public setMainHandler(Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mMainHandler:Landroid/os/Handler;

    return-void
.end method

.method public toggleLockTask(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 4

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/RecentTasksManager;->loadRecentTasksIfNull()V

    :try_start_0
    iget-object v1, p1, Lcom/android/systemui/taskmanager/TaskInfo;->packageName:Ljava/lang/String;

    iget v2, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    iget-boolean v3, p1, Lcom/android/systemui/taskmanager/TaskInfo;->isLocked:Z

    invoke-static {v1, v2, v3}, Lmiui/process/ProcessManager;->updateApplicationLockedState(Ljava/lang/String;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
