.class public Lcom/android/systemui/taskmanager/ScreenView;
.super Landroid/view/ViewGroup;
.source "ScreenView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/taskmanager/ScreenView$1;,
        Lcom/android/systemui/taskmanager/ScreenView$ArrowIndicator;,
        Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;,
        Lcom/android/systemui/taskmanager/ScreenView$Indicator;,
        Lcom/android/systemui/taskmanager/ScreenView$SavedState;,
        Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;,
        Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;,
        Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;,
        Lcom/android/systemui/taskmanager/ScreenView$SlideBar;,
        Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;
    }
.end annotation


# static fields
.field protected static final INDICATOR_MEASURE_SPEC:I

.field private static final MIN_DISTENCE_ONLY_HORIZONTAL_SCROLL:F

.field static final MULTI_SCREEN_FLING_COEFFICIENT_VARIANT:D

.field protected static final SEEK_POINT_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

.field private static final SMOOTHING_CONSTANT:F


# instance fields
.field protected final DEFAULT_CAMERA_DISTANCE:F

.field protected final DEFAULT_ROTATE_CAMERA_DISTANCE:F

.field private final PREVIEW_MODE_MAX_SCREEN_WIDTH:F

.field hasInitCurrentScreen:Z

.field protected mActivePointerId:I

.field private mAllowLongPress:Z

.field private mArrowLeft:Lcom/android/systemui/taskmanager/ScreenView$ArrowIndicator;

.field private mArrowLeftOffResId:I

.field private mArrowLeftOnResId:I

.field private mArrowRight:Lcom/android/systemui/taskmanager/ScreenView$ArrowIndicator;

.field private mArrowRightOffResId:I

.field private mArrowRightOnResId:I

.field protected mAutoHideTimer:Ljava/lang/Runnable;

.field private mCanClickWhenScrolling:Z

.field protected mChildScreenHeight:I

.field protected mChildScreenWidth:I

.field protected mColumnCountPerScreen:I

.field private mComputeScrollTime:J

.field private mConfirmHorizontalScrollRatio:F

.field protected mCurrentScreen:I

.field private mEnableReverseDrawingMode:Z

.field mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

.field protected mHeightMeasureSpec:I

.field private mIndicatorCount:I

.field protected mIsSlideBarAutoHide:Z

.field protected mLastMotionX:F

.field protected mLastMotionY:F

.field protected mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mMaximumVelocity:I

.field protected mMultiScreenScroll:Z

.field protected mNextScreen:I

.field protected mOverScrollRatio:F

.field private mOvershootTension:F

.field private mPreviewModeFooter:Landroid/view/View;

.field private mPreviewModeHeader:Landroid/view/View;

.field protected mRowCountPerScreen:I

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field protected mScreenAlignScrollOffset:I

.field protected mScreenAlignment:I

.field private mScreenCounter:I

.field private mScreenLayoutMode:I

.field protected mScreenPaddingBottom:I

.field protected mScreenPaddingTop:I

.field protected mScreenSeekBar:Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;

.field private mScreenSnapDuration:I

.field private mScreenTransitionType:I

.field protected mScreenWidth:I

.field private mScrollInterpolator:Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;

.field protected mScrollLeftBound:I

.field private mScrollOverMode:I

.field protected mScrollRightBound:I

.field protected mScrollWholeScreen:Z

.field protected mScroller:Landroid/widget/Scroller;

.field private mSeekPointResId:I

.field protected mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

.field private mSmoothingTime:F

.field private mSnapScreenOnceNotification:Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;

.field private mTouchDownTime:J

.field private mTouchDownX:F

.field private mTouchDownY:F

.field private mTouchIntercepted:Z

.field private mTouchSlop:I

.field private mTouchState:I

.field private mTouchX:F

.field protected mVisibleRange:I

.field protected mWidthMeasureSpec:I


# direct methods
.method static synthetic -get0(Lcom/android/systemui/taskmanager/ScreenView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mMaximumVelocity:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/systemui/taskmanager/ScreenView;)F
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOvershootTension:F

    return v0
.end method

.method static synthetic -get2(Lcom/android/systemui/taskmanager/ScreenView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/systemui/taskmanager/ScreenView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    return v0
.end method

.method static synthetic -wrap0(Lcom/android/systemui/taskmanager/ScreenView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->startHideSlideBar()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x0

    const/4 v2, -0x1

    const-wide v4, 0x3fbc71c71c71c71cL    # 0.1111111111111111

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, Lcom/android/systemui/taskmanager/ScreenView;->INDICATOR_MEASURE_SPEC:I

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    sput-object v0, Lcom/android/systemui/taskmanager/ScreenView;->SEEK_POINT_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide v2, 0x3949f623d5a8a733L    # 1.0E-32

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    sput-wide v0, Lcom/android/systemui/taskmanager/ScreenView;->MULTI_SCREEN_FLING_COEFFICIENT_VARIANT:D

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    sput v0, Lcom/android/systemui/taskmanager/ScreenView;->MIN_DISTENCE_ONLY_HORIZONTAL_SCROLL:F

    const-wide/high16 v0, 0x3fe8000000000000L    # 0.75

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x3f90624dd2f1a9fcL    # 0.016

    div-double v0, v2, v0

    double-to-float v0, v0

    sput v0, Lcom/android/systemui/taskmanager/ScreenView;->SMOOTHING_CONSTANT:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v5, 0x0

    const/high16 v4, -0x80000000

    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0201ef

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowLeftOnResId:I

    const v0, 0x7f0201f0

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowLeftOffResId:I

    const v0, 0x7f0201f1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowRightOnResId:I

    const v0, 0x7f0201f2

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowRightOffResId:I

    const v0, 0x7f0201f5

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSeekPointResId:I

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->hasInitCurrentScreen:Z

    new-instance v0, Lcom/android/systemui/taskmanager/ScreenView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/ScreenView$1;-><init>(Lcom/android/systemui/taskmanager/ScreenView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAutoHideTimer:Ljava/lang/Runnable;

    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->PREVIEW_MODE_MAX_SCREEN_WIDTH:F

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignment:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenHeight:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mRowCountPerScreen:I

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mColumnCountPerScreen:I

    iput-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeHeader:Landroid/view/View;

    iput-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeFooter:Landroid/view/View;

    iput v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    iput v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOverScrollRatio:F

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollWholeScreen:Z

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    iput-boolean v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAllowLongPress:Z

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mConfirmHorizontalScrollRatio:F

    const/16 v0, 0x12c

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSnapDuration:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenTransitionType:I

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mEnableReverseDrawingMode:Z

    const v0, 0x3fa66666    # 1.3f

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOvershootTension:F

    new-instance v0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;-><init>(Lcom/android/systemui/taskmanager/ScreenView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x44a00000    # 1280.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_CAMERA_DISTANCE:F

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4094000000000000L    # 1280.0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->initScreenView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/taskmanager/ScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    const/4 v5, 0x0

    const/high16 v4, -0x80000000

    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const v0, 0x7f0201ef

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowLeftOnResId:I

    const v0, 0x7f0201f0

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowLeftOffResId:I

    const v0, 0x7f0201f1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowRightOnResId:I

    const v0, 0x7f0201f2

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowRightOffResId:I

    const v0, 0x7f0201f5

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSeekPointResId:I

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->hasInitCurrentScreen:Z

    new-instance v0, Lcom/android/systemui/taskmanager/ScreenView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/ScreenView$1;-><init>(Lcom/android/systemui/taskmanager/ScreenView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAutoHideTimer:Ljava/lang/Runnable;

    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->PREVIEW_MODE_MAX_SCREEN_WIDTH:F

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignment:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenHeight:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mRowCountPerScreen:I

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mColumnCountPerScreen:I

    iput-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeHeader:Landroid/view/View;

    iput-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeFooter:Landroid/view/View;

    iput v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    iput v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOverScrollRatio:F

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollWholeScreen:Z

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    iput-boolean v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAllowLongPress:Z

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mConfirmHorizontalScrollRatio:F

    const/16 v0, 0x12c

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSnapDuration:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenTransitionType:I

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mEnableReverseDrawingMode:Z

    const v0, 0x3fa66666    # 1.3f

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOvershootTension:F

    new-instance v0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;-><init>(Lcom/android/systemui/taskmanager/ScreenView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x44a00000    # 1280.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_CAMERA_DISTANCE:F

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4094000000000000L    # 1280.0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->initScreenView()V

    return-void
.end method

.method private CheckInRangeOfView(Landroid/view/View;II)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    new-array v0, v3, [I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v1, v0, v4

    aget v2, v0, v5

    if-lt p2, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, v1

    if-le p2, v3, :cond_1

    :cond_0
    return v4

    :cond_1
    if-lt p3, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v3, v2

    if-gt p3, v3, :cond_0

    return v5
.end method

.method private createSeekPoint()Landroid/widget/ImageView;
    .locals 2

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSeekPointResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object v0
.end method

.method private findActualView(Landroid/view/ViewGroup;FF)Landroid/view/View;
    .locals 5

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    float-to-int v3, p2

    float-to-int v4, p3

    invoke-direct {p0, v1, v3, v4}, Lcom/android/systemui/taskmanager/ScreenView;->CheckInRangeOfView(Landroid/view/View;II)Z

    move-result v3

    if-eqz v3, :cond_1

    instance-of v3, v1, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {p0, v1, p2, p3}, Lcom/android/systemui/taskmanager/ScreenView;->findActualView(Landroid/view/ViewGroup;FF)Landroid/view/View;

    move-result-object v3

    return-object v3

    :cond_0
    return-object v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    return-object v3
.end method

.method private initScreenView()V
    .locals 5

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setAlwaysDrawnWithCacheEnabled(Z)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setClipToPadding(Z)V

    new-instance v1, Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;-><init>(Lcom/android/systemui/taskmanager/ScreenView;)V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollInterpolator:Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;

    new-instance v1, Landroid/widget/Scroller;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollInterpolator:Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;

    invoke-direct {v1, v2, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setMaximumSnapVelocity(I)V

    new-instance v1, Landroid/view/ScaleGestureDetector;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;-><init>(Lcom/android/systemui/taskmanager/ScreenView;Lcom/android/systemui/taskmanager/ScreenView$ScaleDetectorListener;)V

    invoke-direct {v1, v2, v3}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    return-void
.end method

.method private onTouchEventUnique(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    invoke-virtual {v0, p1}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_0
    return-void
.end method

.method private refreshScrollBound()Z
    .locals 6

    const/4 v2, 0x1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    neg-int v3, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOverScrollRatio:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    iget-boolean v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollWholeScreen:Z

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenLayoutX(I)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOverScrollRatio:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    :goto_0
    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOverScrollRatio:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    int-to-float v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    float-to-int v3, v3

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    if-ne v0, v3, :cond_0

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    if-eq v1, v3, :cond_2

    :cond_0
    :goto_1
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    div-int/2addr v3, v4

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    mul-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOverScrollRatio:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    if-lt v3, v4, :cond_0

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    if-gt v3, v4, :cond_0

    const/4 v2, 0x0

    goto :goto_1
.end method

.method private scrolledFarEnough(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mLastMotionX:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mLastMotionY:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mConfirmHorizontalScrollRatio:F

    mul-float/2addr v3, v1

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchSlop:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    mul-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private showSlideBar()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mIsSlideBarAutoHide:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAutoHideTimer:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/ScreenView$SlideBar;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/ScreenView$SlideBar;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

    invoke-virtual {v0, v2}, Lcom/android/systemui/taskmanager/ScreenView$SlideBar;->setVisibility(I)V

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAutoHideTimer:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/systemui/taskmanager/ScreenView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    return-void
.end method

.method private snapByVelocity(I)V
    .locals 5

    iget v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getCurrentScreen()Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->isScrollable()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mMaximumVelocity:I

    const/16 v4, 0x3e8

    invoke-virtual {v2, v4, v3, p1}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->getXVelocity(III)F

    move-result v2

    float-to-int v1, v2

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->getXFlingDirection(F)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/android/systemui/taskmanager/ScreenView;->snapByVelocity(II)V

    return-void
.end method

.method private startHideSlideBar()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mIsSlideBarAutoHide:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/ScreenView$SlideBar;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/taskmanager/ScreenView$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/ScreenView$2;-><init>(Lcom/android/systemui/taskmanager/ScreenView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private startOverScrollFling(I)V
    .locals 9

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0, v3}, Landroid/widget/Scroller;->forceFinished(Z)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->setFriction(F)V

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollOverMode:I

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    move v3, p1

    move v4, v2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->invalidate()V

    return-void
.end method

.method private throwRemoveIndicatorException()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ScreenView doesn\'t support remove indicator directly."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private updateArrowIndicatorResource(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowLeft:Lcom/android/systemui/taskmanager/ScreenView$ArrowIndicator;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowLeft:Lcom/android/systemui/taskmanager/ScreenView$ArrowIndicator;

    if-gtz p1, :cond_1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowLeftOffResId:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/systemui/taskmanager/ScreenView$ArrowIndicator;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowRight:Lcom/android/systemui/taskmanager/ScreenView$ArrowIndicator;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    iget v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    sub-int/2addr v0, v2

    if-lt p1, v0, :cond_2

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowRightOffResId:I

    :goto_1
    invoke-virtual {v1, v0}, Lcom/android/systemui/taskmanager/ScreenView$ArrowIndicator;->setImageResource(I)V

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowLeftOnResId:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mArrowRightOnResId:I

    goto :goto_1
.end method

.method private updateIndicatorPositions(IZ)V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getWidth()I

    move-result v14

    if-lez v14, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getWidth()I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getHeight()I

    move-result v11

    const/4 v3, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/systemui/taskmanager/ScreenView;->mIndicatorCount:I

    if-ge v3, v14, :cond_3

    add-int v14, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v1, v10, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    const/4 v14, -0x1

    if-eq v1, v14, :cond_0

    and-int/lit8 v2, v1, 0x7

    and-int/lit8 v13, v1, 0x70

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    iget v7, v10, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    :goto_1
    sparse-switch v13, :sswitch_data_0

    iget v8, v10, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    :cond_0
    :goto_2
    if-nez p2, :cond_2

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v14

    if-lez v14, :cond_2

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v14

    if-lez v14, :cond_2

    move-object v14, v5

    check-cast v14, Lcom/android/systemui/taskmanager/ScreenView$Indicator;

    add-int v15, p1, v7

    invoke-interface {v14, v15}, Lcom/android/systemui/taskmanager/ScreenView$Indicator;->fastOffset(I)Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-virtual {v5}, Landroid/view/View;->invalidate()V

    :cond_1
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :pswitch_1
    iget v7, v10, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto :goto_1

    :pswitch_2
    sub-int v14, v12, v9

    div-int/lit8 v14, v14, 0x2

    iget v15, v10, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v14, v15

    iget v15, v10, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int v7, v14, v15

    goto :goto_1

    :pswitch_3
    sub-int v14, v12, v9

    iget v15, v10, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int v7, v14, v15

    goto :goto_1

    :sswitch_0
    iget v8, v10, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto :goto_2

    :sswitch_1
    sub-int v14, v11, v6

    div-int/lit8 v14, v14, 0x2

    iget v15, v10, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v14, v15

    iget v15, v10, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int v8, v14, v15

    goto :goto_2

    :sswitch_2
    sub-int v14, v11, v6

    iget v15, v10, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int v8, v14, v15

    goto :goto_2

    :cond_2
    add-int v14, p1, v7

    add-int v15, p1, v7

    add-int/2addr v15, v9

    add-int v16, v8, v6

    move/from16 v0, v16

    invoke-virtual {v5, v14, v8, v15, v0}, Landroid/view/View;->layout(IIII)V

    goto :goto_3

    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x30 -> :sswitch_0
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method private updateScreenOffset()V
    .locals 2

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignment:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateSeekPoints(I)V
    .locals 5

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSeekBar:Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSeekBar:Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;

    invoke-virtual {v3, v1}, Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    if-lt v1, p1, :cond_1

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    add-int/2addr v3, p1

    if-ge v1, v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    :cond_2
    return-void
.end method

.method private updateSlidePointPosition(I)V
    .locals 7

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

    if-eqz v5, :cond_0

    if-lez v0, :cond_0

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

    invoke-virtual {v5}, Lcom/android/systemui/taskmanager/ScreenView$SlideBar;->getSlideWidth()I

    move-result v2

    div-int v5, v2, v0

    iget v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    mul-int/2addr v5, v6

    const/16 v6, 0x30

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    mul-int v1, v5, v0

    if-gt v1, v2, :cond_1

    const/4 v4, 0x0

    :goto_0
    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

    add-int v6, v4, v3

    invoke-virtual {v5, v4, v6}, Lcom/android/systemui/taskmanager/ScreenView$SlideBar;->setPosition(II)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->isHardwareAccelerated()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSlideBar:Lcom/android/systemui/taskmanager/ScreenView$SlideBar;

    invoke-virtual {v5}, Lcom/android/systemui/taskmanager/ScreenView$SlideBar;->invalidate()V

    :cond_0
    return-void

    :cond_1
    sub-int v5, v2, v3

    mul-int/2addr v5, p1

    sub-int v6, v1, v2

    div-int v4, v5, v6

    goto :goto_0
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 4

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    if-gez p2, :cond_1

    move p2, v0

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSeekBar:Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSeekBar:Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->createSeekPoint()Landroid/widget/ImageView;

    move-result-object v2

    sget-object v3, Lcom/android/systemui/taskmanager/ScreenView;->SEEK_POINT_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1, v2, p2, v3}, Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_1
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 11

    const/4 v10, 0x0

    const/high16 v9, -0x80000000

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getFinalX()I

    move-result v5

    iget-object v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrX()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-gt v5, v7, :cond_0

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    iget v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget-wide v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mComputeScrollTime:J

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrX()I

    move-result v5

    iput v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    int-to-float v5, v5

    iput v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchX:F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mComputeScrollTime:J

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollOverMode:I

    if-nez v5, :cond_2

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    if-gt v5, v6, :cond_1

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    if-ge v5, v6, :cond_2

    :cond_1
    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    sub-int/2addr v5, v2

    int-to-float v5, v5

    iget-wide v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mComputeScrollTime:J

    sub-long/2addr v6, v0

    long-to-float v6, v6

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v6, v7

    div-float/2addr v5, v6

    float-to-int v4, v5

    invoke-direct {p0, v4}, Lcom/android/systemui/taskmanager/ScreenView;->startOverScrollFling(I)V

    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    long-to-float v5, v6

    const v6, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v5, v6

    iput v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSmoothingTime:F

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrY()I

    move-result v5

    iput v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollY:I

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->postInvalidateOnAnimation()V

    :cond_3
    :goto_0
    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    invoke-direct {p0, v5, v8}, Lcom/android/systemui/taskmanager/ScreenView;->updateIndicatorPositions(IZ)V

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    invoke-direct {p0, v5}, Lcom/android/systemui/taskmanager/ScreenView;->updateSlidePointPosition(I)V

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    invoke-direct {p0, v5}, Lcom/android/systemui/taskmanager/ScreenView;->updateArrowIndicatorResource(I)V

    return-void

    :cond_4
    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollOverMode:I

    if-eq v5, v7, :cond_5

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    if-eq v5, v9, :cond_9

    :cond_5
    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollOverMode:I

    if-ne v5, v7, :cond_8

    const/4 v5, 0x2

    iput v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollOverMode:I

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    if-ne v3, v9, :cond_6

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    if-gt v5, v6, :cond_7

    const/4 v3, 0x0

    :cond_6
    :goto_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v8, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/ScreenView;->snapToScreen(I)V

    return-void

    :cond_7
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v5

    add-int/lit8 v3, v5, -0x1

    goto :goto_1

    :cond_8
    iput v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollOverMode:I

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v8, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/ScreenView;->setCurrentScreenInner(I)V

    iput v9, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSnapScreenOnceNotification:Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSnapScreenOnceNotification:Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;

    invoke-interface {v5, p0}, Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;->onSnapEnd(Lcom/android/systemui/taskmanager/ScreenView;)V

    iput-object v10, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSnapScreenOnceNotification:Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;

    goto :goto_0

    :cond_9
    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    goto :goto_0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 3

    const/4 v2, 0x1

    const/16 v0, 0x11

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->snapToScreen(I)V

    return v2

    :cond_0
    const/16 v0, 0x42

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->snapToScreen(I)V

    return v2

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 1

    invoke-virtual {p0, p2}, Lcom/android/systemui/taskmanager/ScreenView;->updateChildStaticTransformation(Landroid/view/View;)V

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0
.end method

.method protected findChild(FF)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v1, v4}, Lcom/android/systemui/taskmanager/ScreenView;->isTransformedTouchPointInView(FFLandroid/view/View;Landroid/graphics/PointF;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method protected getChildDrawingOrder(II)I
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mEnableReverseDrawingMode:Z

    if-eqz v0, :cond_0

    sub-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    return v0

    :cond_0
    return p2
.end method

.method public getCurrentScreen()Landroid/view/View;
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreen(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentScreenIndex()I
    .locals 2

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    return v0

    :cond_0
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    return v0
.end method

.method protected getDefaultScreenIndex()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getScreen(I)Landroid/view/View;
    .locals 1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getScreenCount()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    return v0
.end method

.method protected getScreenLayoutX(I)I
    .locals 7

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    if-ge v4, v5, :cond_0

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    iget v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    mul-int/2addr v5, v6

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    mul-int/2addr v5, p1

    add-int/2addr v4, v5

    return v4

    :cond_0
    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    div-int v2, p1, v4

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    rem-int v4, p1, v4

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mColumnCountPerScreen:I

    rem-int v0, v4, v5

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getWidth()I

    move-result v5

    mul-int/2addr v5, v2

    add-int/2addr v4, v5

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    mul-int/2addr v5, v0

    add-int/2addr v4, v5

    return v4

    :cond_1
    const/4 v3, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_3

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    add-int/2addr v4, v3

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    iget v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingRight:I

    add-int/2addr v5, v6

    mul-int/2addr v5, p1

    iget v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    div-int/2addr v5, v6

    add-int/2addr v4, v5

    return v4
.end method

.method protected final getScreenScrollX(I)I
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->isScrollable()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    return v0

    :cond_0
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    neg-int v0, v0

    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenVisualX(I)I

    move-result v0

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getScreenSnapMaxDuration()I
    .locals 2

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSnapDuration:I

    int-to-float v0, v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method protected final getScreenVisualX(I)I
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenLayoutX(I)I

    move-result v0

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    sub-int/2addr v0, v1

    return v0
.end method

.method protected getTouchState()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    return v0
.end method

.method protected final isScrollable()Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->computeScroll()V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->showSlideBar()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    const/4 v4, 0x2

    if-eq v4, v3, :cond_1

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->onTouchEventUnique(Landroid/view/MotionEvent;)V

    :cond_1
    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-eq v3, v6, :cond_5

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    const/4 v4, 0x6

    if-eq v3, v4, :cond_5

    :goto_1
    return v1

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->onTouchEventUnique(Landroid/view/MotionEvent;)V

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-ne v3, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->scrolledFarEnough(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, p1, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setTouchState(Landroid/view/MotionEvent;I)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    invoke-virtual {v3}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->getVerticalGesture()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->onVerticalGesture(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x4

    invoke-virtual {p0, p1, v3}, Lcom/android/systemui/taskmanager/ScreenView;->setTouchState(Landroid/view/MotionEvent;I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    invoke-virtual {v3}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->recycle()V

    iput-boolean v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchIntercepted:Z

    iput v2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollOverMode:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchDownTime:J

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mLastMotionX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mLastMotionY:F

    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_3

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAllowLongPress:Z

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getFinalX()I

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrX()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    sget v4, Lcom/android/systemui/taskmanager/ScreenView;->MIN_DISTENCE_ONLY_HORIZONTAL_SCROLL:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    invoke-virtual {p0, p1, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setTouchState(Landroid/view/MotionEvent;I)V

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchDownX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchDownY:F

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual {p0, p1, v2}, Lcom/android/systemui/taskmanager/ScreenView;->setTouchState(Landroid/view/MotionEvent;I)V

    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 32

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/systemui/taskmanager/ScreenView;->setFrame(IIII)Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/taskmanager/ScreenView;->updateIndicatorPositions(IZ)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v9

    if-gtz v9, :cond_0

    return-void

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->refreshScrollBound()Z

    move-result v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    move/from16 v25, v0

    const/high16 v26, -0x80000000

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getDefaultScreenIndex()I

    move-result v10

    :goto_0
    if-lt v10, v9, :cond_1

    add-int/lit8 v10, v9, -0x1

    :cond_1
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollWholeScreen:Z

    move/from16 v25, v0

    if-eqz v25, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    move/from16 v25, v0

    rem-int v25, v10, v25

    if-lez v25, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    move/from16 v26, v0

    rem-int v25, v25, v26

    sub-int v10, v10, v25

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->hasInitCurrentScreen:Z

    move/from16 v25, v0

    if-nez v25, :cond_7

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/android/systemui/taskmanager/ScreenView;->setCurrentScreen(I)V

    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/ScreenView;->hasInitCurrentScreen:Z

    :cond_3
    :goto_1
    const/4 v14, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeHeader:Landroid/view/View;

    move-object/from16 v25, v0

    if-eqz v25, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    move/from16 v26, v0

    div-int v19, v25, v26

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeFooter:Landroid/view/View;

    move-object/from16 v25, v0

    if-eqz v25, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    move/from16 v26, v0

    div-int v17, v25, v26

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeHeader:Landroid/view/View;

    move-object/from16 v25, v0

    if-eqz v25, :cond_b

    const/16 v18, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeFooter:Landroid/view/View;

    move-object/from16 v25, v0

    if-eqz v25, :cond_c

    const/16 v16, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    const v26, 0x3e4ccccd    # 0.2f

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v14, v0

    sub-int v25, v9, v18

    sub-int v15, v25, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    move/from16 v25, v0

    sub-int v25, v25, v19

    sub-int v21, v25, v17

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    sub-float v25, v25, v26

    add-int/lit8 v26, v15, -0x1

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    div-float v22, v25, v26

    int-to-float v0, v14

    move/from16 v25, v0

    cmpg-float v25, v22, v25

    if-gez v25, :cond_4

    move/from16 v0, v22

    float-to-int v14, v0

    :cond_4
    add-int/lit8 v25, v15, -0x1

    mul-int v25, v25, v14

    sub-int v25, v21, v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    div-int/lit8 v25, v25, 0x2

    add-int v20, v19, v25

    :cond_5
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingTop:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingTop:I

    move/from16 v26, v0

    add-int v13, v25, v26

    const/4 v11, 0x0

    :goto_7
    if-ge v11, v9, :cond_12

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v25

    const/16 v26, 0x8

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_11

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenLayoutX(I)I

    move-result v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_0

    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    move/from16 v25, v0

    const/16 v26, 0x4

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    move/from16 v25, v0

    add-int v25, v25, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenHeight:I

    move/from16 v26, v0

    add-int v26, v26, v13

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v7, v12, v13, v0, v1}, Landroid/view/View;->layout(IIII)V

    :goto_9
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    :cond_6
    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    move/from16 v25, v0

    move/from16 v0, v25

    if-ne v10, v0, :cond_8

    if-eqz v6, :cond_3

    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/android/systemui/taskmanager/ScreenView;->setCurrentScreen(I)V

    goto/16 :goto_1

    :cond_9
    const/16 v19, 0x0

    goto/16 :goto_2

    :cond_a
    const/16 v17, 0x0

    goto/16 :goto_3

    :cond_b
    const/16 v18, 0x0

    goto/16 :goto_4

    :cond_c
    const/16 v16, 0x0

    goto/16 :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    move/from16 v25, v0

    const/16 v26, 0x4

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_5

    sub-int v25, p4, p2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mColumnCountPerScreen:I

    move/from16 v27, v0

    mul-int v26, v26, v27

    sub-int v25, v25, v26

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingRight:I

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    sub-int v25, p5, p3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenHeight:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mRowCountPerScreen:I

    move/from16 v27, v0

    mul-int v26, v26, v27

    sub-int v25, v25, v26

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingTop:I

    goto/16 :goto_6

    :pswitch_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    move/from16 v25, v0

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v26

    sub-int v25, v25, v26

    div-int/lit8 v25, v25, 0x2

    add-int v12, v12, v25

    goto/16 :goto_8

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeHeader:Landroid/view/View;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    if-ne v7, v0, :cond_e

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    move/from16 v26, v0

    add-int v12, v25, v26

    goto/16 :goto_8

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPreviewModeFooter:Landroid/view/View;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    if-ne v7, v0, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    move/from16 v26, v0

    add-int v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    move/from16 v26, v0

    add-int v25, v25, v26

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v26

    add-int v26, v26, v17

    div-int/lit8 v26, v26, 0x2

    sub-int v12, v25, v26

    goto/16 :goto_8

    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    move/from16 v26, v0

    add-int v25, v25, v26

    add-int v25, v25, v20

    sub-int v26, v11, v18

    mul-int v26, v26, v14

    add-int v12, v25, v26

    goto/16 :goto_8

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getHeight()I

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingTop:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingTop:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingBottom:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingBottom:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v26

    sub-int v25, v25, v26

    div-int/lit8 v25, v25, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingTop:I

    move/from16 v26, v0

    add-int v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingTop:I

    move/from16 v26, v0

    add-int v13, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-double v0, v0

    move-wide/from16 v26, v0

    int-to-double v0, v11

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x3fe0000000000000L    # 0.5

    add-double v28, v28, v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    move/from16 v25, v0

    div-int v25, v25, v9

    move/from16 v0, v25

    int-to-double v0, v0

    move-wide/from16 v30, v0

    mul-double v28, v28, v30

    add-double v26, v26, v28

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v25

    div-int/lit8 v25, v25, 0x2

    move/from16 v0, v25

    int-to-double v0, v0

    move-wide/from16 v28, v0

    sub-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-int v12, v0

    goto/16 :goto_8

    :pswitch_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    move/from16 v25, v0

    div-int v24, v11, v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    move/from16 v25, v0

    rem-int v25, v11, v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mColumnCountPerScreen:I

    move/from16 v26, v0

    rem-int v8, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    move/from16 v25, v0

    rem-int v25, v11, v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mColumnCountPerScreen:I

    move/from16 v26, v0

    div-int v23, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingTop:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingTop:I

    move/from16 v26, v0

    add-int v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenHeight:I

    move/from16 v26, v0

    mul-int v26, v26, v23

    add-int v13, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    move/from16 v25, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getWidth()I

    move-result v26

    mul-int v26, v26, v24

    add-int v25, v25, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    move/from16 v26, v0

    mul-int v26, v26, v8

    add-int v12, v25, v26

    goto/16 :goto_8

    :cond_10
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v25

    add-int v25, v25, v12

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v26

    add-int v26, v26, v13

    move/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v7, v12, v13, v0, v1}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_9

    :cond_11
    new-instance v25, Ljava/lang/RuntimeException;

    const-string/jumbo v26, "child screen can\'t set visible as GONE."

    invoke-direct/range {v25 .. v26}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v25

    :cond_12
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 13

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mWidthMeasureSpec:I

    iput p2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mHeightMeasureSpec:I

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v3

    const/4 v4, 0x0

    :goto_0
    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mIndicatorCount:I

    if-ge v4, v11, :cond_0

    add-int v11, v4, v3

    invoke-virtual {p0, v11}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingRight:I

    add-int/2addr v11, v12

    iget v12, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v11, v12}, Lcom/android/systemui/taskmanager/ScreenView;->getChildMeasureSpec(III)I

    move-result v2

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingTop:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingTop:I

    add-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingBottom:I

    add-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingBottom:I

    add-int/2addr v11, v12

    iget v12, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v11, v12}, Lcom/android/systemui/taskmanager/ScreenView;->getChildMeasureSpec(III)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    invoke-static {v8, v11}, Ljava/lang/Math;->max(II)I

    move-result v8

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x1

    iput v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_1

    invoke-virtual {p0, v4}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingRight:I

    add-int/2addr v11, v12

    iget v12, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v11, v12}, Lcom/android/systemui/taskmanager/ScreenView;->getChildMeasureSpec(III)I

    move-result v2

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingTop:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingTop:I

    add-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingBottom:I

    add-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingBottom:I

    add-int/2addr v11, v12

    iget v12, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v11, v12}, Lcom/android/systemui/taskmanager/ScreenView;->getChildMeasureSpec(III)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    invoke-static {v7, v11}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    invoke-static {v6, v11}, Ljava/lang/Math;->max(II)I

    move-result v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v8

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingRight:I

    add-int/2addr v11, v12

    add-int/2addr v9, v11

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingTop:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingTop:I

    add-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingBottom:I

    add-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingBottom:I

    add-int/2addr v11, v12

    add-int/2addr v8, v11

    invoke-static {v9, p1}, Lcom/android/systemui/taskmanager/ScreenView;->resolveSize(II)I

    move-result v11

    invoke-static {v8, p2}, Lcom/android/systemui/taskmanager/ScreenView;->resolveSize(II)I

    move-result v12

    invoke-virtual {p0, v11, v12}, Lcom/android/systemui/taskmanager/ScreenView;->setMeasuredDimension(II)V

    if-lez v3, :cond_3

    iput v7, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    iput v6, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenHeight:I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingLeft:I

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingRight:I

    sub-int/2addr v11, v12

    iput v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingTop:I

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mPaddingBottom:I

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingTop:I

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenPaddingBottom:I

    sub-int v10, v11, v12

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    if-lez v11, :cond_2

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    const/4 v12, 0x4

    if-eq v11, v12, :cond_4

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    div-int/2addr v11, v12

    const/4 v12, 0x1

    invoke-static {v12, v11}, Ljava/lang/Math;->max(II)I

    move-result v11

    iput v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    div-int/2addr v11, v12

    iput v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    :cond_2
    :goto_2
    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOverScrollRatio:F

    invoke-virtual {p0, v11}, Lcom/android/systemui/taskmanager/ScreenView;->setOverScrollRatio(F)V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->updateScreenOffset()V

    :cond_3
    return-void

    :cond_4
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    iput v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenHeight:I

    div-int v11, v10, v11

    iput v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mRowCountPerScreen:I

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    div-int/2addr v11, v12

    iput v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mColumnCountPerScreen:I

    iget v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mRowCountPerScreen:I

    iget v12, p0, Lcom/android/systemui/taskmanager/ScreenView;->mColumnCountPerScreen:I

    mul-int/2addr v11, v12

    iput v11, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    goto :goto_2
.end method

.method protected onPinchIn(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onPinchOut(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    move-object v0, p1

    check-cast v0, Lcom/android/systemui/taskmanager/ScreenView$SavedState;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/ScreenView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v1, v0, Lcom/android/systemui/taskmanager/ScreenView$SavedState;->currentScreen:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, v0, Lcom/android/systemui/taskmanager/ScreenView$SavedState;->currentScreen:I

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setCurrentScreen(I)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/android/systemui/taskmanager/ScreenView$SavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/ScreenView$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    iput v1, v0, Lcom/android/systemui/taskmanager/ScreenView$SavedState;->currentScreen:I

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 18

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchIntercepted:Z

    if-eqz v12, :cond_0

    invoke-direct/range {p0 .. p1}, Lcom/android/systemui/taskmanager/ScreenView;->onTouchEventUnique(Landroid/view/MotionEvent;)V

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v12

    and-int/lit16 v12, v12, 0xff

    packed-switch v12, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchIntercepted:Z

    const/4 v12, 0x1

    return v12

    :pswitch_1
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-nez v12, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_2

    invoke-direct/range {p0 .. p1}, Lcom/android/systemui/taskmanager/ScreenView;->scrolledFarEnough(Landroid/view/MotionEvent;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v12, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v12}, Lcom/android/systemui/taskmanager/ScreenView;->setTouchState(Landroid/view/MotionEvent;I)V

    :cond_2
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v10

    const/4 v12, -0x1

    if-ne v10, v12, :cond_3

    const/4 v12, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v12}, Lcom/android/systemui/taskmanager/ScreenView;->setTouchState(Landroid/view/MotionEvent;I)V

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v10

    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/MotionEvent;->getX(I)F

    move-result v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mLastMotionX:F

    sub-float v3, v12, v11

    move-object/from16 v0, p0

    iput v11, v0, Lcom/android/systemui/taskmanager/ScreenView;->mLastMotionX:F

    const/4 v12, 0x0

    cmpl-float v12, v3, v12

    if-eqz v12, :cond_6

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchX:F

    add-float/2addr v12, v3

    move-object/from16 v0, p0

    iput v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchX:F

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchX:F

    float-to-int v5, v12

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchX:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    int-to-float v13, v13

    cmpg-float v12, v12, v13

    if-gez v12, :cond_5

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    int-to-float v12, v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchX:F

    sub-float v4, v12, v13

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    int-to-double v12, v12

    float-to-double v14, v4

    const-wide v16, 0x3febd70a40000000L    # 0.8700000047683716

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    sub-double/2addr v12, v14

    double-to-int v5, v12

    :cond_4
    :goto_1
    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v12}, Lcom/android/systemui/taskmanager/ScreenView;->scrollTo(II)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchX:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    int-to-float v13, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_4

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchX:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    int-to-float v13, v13

    sub-float v4, v12, v13

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    int-to-double v12, v12

    float-to-double v14, v4

    const-wide v16, 0x3febd70a40000000L    # 0.8700000047683716

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    add-double/2addr v12, v14

    double-to-int v5, v12

    goto :goto_1

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->awakenScrollBars()Z

    goto/16 :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_b

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mCanClickWhenScrolling:Z

    if-eqz v12, :cond_8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchDownTime:J

    sub-long/2addr v12, v14

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v14

    int-to-long v14, v14

    cmp-long v12, v12, v14

    if-gez v12, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchDownX:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v13

    int-to-float v13, v13

    cmpg-float v12, v12, v13

    if-gez v12, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchDownY:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v13

    int-to-float v13, v13

    cmpg-float v12, v12, v13

    if-gez v12, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/android/systemui/taskmanager/ScreenView;->findChild(FF)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Landroid/view/View;->isClickable()Z

    move-result v12

    if-eqz v12, :cond_8

    const/4 v6, 0x0

    instance-of v12, v2, Landroid/view/ViewGroup;

    if-eqz v12, :cond_7

    move-object v12, v2

    check-cast v12, Landroid/view/ViewGroup;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v13

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v14

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13, v14}, Lcom/android/systemui/taskmanager/ScreenView;->findActualView(Landroid/view/ViewGroup;FF)Landroid/view/View;

    move-result-object v6

    :cond_7
    if-eqz v6, :cond_a

    invoke-virtual {v6}, Landroid/view/View;->isClickable()Z

    move-result v12

    if-eqz v12, :cond_a

    invoke-virtual {v6}, Landroid/view/View;->performClick()Z

    move-result v7

    :cond_8
    :goto_2
    if-nez v7, :cond_9

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/systemui/taskmanager/ScreenView;->snapByVelocity(I)V

    :cond_9
    :goto_3
    const/4 v12, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v12}, Lcom/android/systemui/taskmanager/ScreenView;->setTouchState(Landroid/view/MotionEvent;I)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v2}, Landroid/view/View;->performClick()Z

    goto :goto_2

    :cond_b
    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-nez v12, :cond_9

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/ScreenView;->performClick()Z

    goto :goto_3

    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v12

    const v13, 0xff00

    and-int/2addr v12, v13

    shr-int/lit8 v10, v12, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v9

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    if-ne v9, v12, :cond_1

    if-nez v10, :cond_c

    const/4 v8, 0x1

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/view/MotionEvent;->getX(I)F

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mLastMotionX:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/systemui/taskmanager/ScreenView;->mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    invoke-virtual {v12, v13}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->init(I)V

    goto/16 :goto_0

    :cond_c
    const/4 v8, 0x0

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onVerticalGesture(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onViewRemoved(Landroid/view/View;)V
    .locals 1

    instance-of v0, p1, Lcom/android/systemui/taskmanager/ScreenView$Indicator;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mIndicatorCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mIndicatorCount:I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenCounter:I

    goto :goto_0
.end method

.method public removeAllScreens()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/systemui/taskmanager/ScreenView;->removeScreensInLayout(II)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->requestLayout()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->invalidate()V

    return-void
.end method

.method public removeScreen(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    new-instance v0, Ljava/security/InvalidParameterException;

    const-string/jumbo v1, "The view specified by the index must be a screen."

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    if-lt p1, v0, :cond_1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignment:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->setCurrentScreen(I)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSeekBar:Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSeekBar:Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;

    invoke-virtual {v0, p1}, Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;->removeViewAt(I)V

    :cond_3
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    return-void

    :cond_4
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->snapToScreen(I)V

    goto :goto_0
.end method

.method public removeScreensInLayout(II)V
    .locals 1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSeekBar:Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSeekBar:Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/taskmanager/ScreenView$SeekBarIndicator;->removeViewsInLayout(II)V

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->removeViewsInLayout(II)V

    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1

    instance-of v0, p1, Lcom/android/systemui/taskmanager/ScreenView$Indicator;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->throwRemoveIndicatorException()V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public removeViewAt(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->throwRemoveIndicatorException()V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    return-void
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .locals 1

    instance-of v0, p1, Lcom/android/systemui/taskmanager/ScreenView$Indicator;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->throwRemoveIndicatorException()V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public removeViews(II)V
    .locals 2

    add-int v0, p1, p2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->throwRemoveIndicatorException()V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->removeViews(II)V

    return-void
.end method

.method public removeViewsInLayout(II)V
    .locals 2

    add-int v0, p1, p2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->throwRemoveIndicatorException()V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->removeViewsInLayout(II)V

    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->snapToScreen(I)V

    const/4 v1, 0x1

    return v1

    :cond_1
    const/4 v1, 0x0

    return v1

    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    move-result v1

    return v1
.end method

.method protected resetTransformation(Landroid/view/View;)V
    .locals 2

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setRotationX(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setRotationY(F)V

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_CAMERA_DISTANCE:F

    invoke-virtual {p1, v0}, Landroid/view/View;->setCameraDistance(F)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    return-void
.end method

.method public scrollTo(II)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->isScrollable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSmoothingTime:F

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    :cond_0
    return-void
.end method

.method public scrollToScreen(I)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollWholeScreen:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    rem-int v0, p1, v0

    sub-int/2addr p1, v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenScrollX(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->scrollTo(II)V

    return-void
.end method

.method public setCanClickWhenScrolling(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCanClickWhenScrolling:Z

    return-void
.end method

.method public setCurrentScreen(I)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollWholeScreen:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    rem-int v0, p1, v0

    sub-int/2addr p1, v0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->setCurrentScreenInner(I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->scrollToScreen(I)V

    return-void

    :cond_0
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignment:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto :goto_0
.end method

.method protected setCurrentScreenInner(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->updateSeekPoints(I)V

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    return-void
.end method

.method public setMaximumSnapVelocity(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mMaximumVelocity:I

    return-void
.end method

.method public setMultiScreenScroll(Z)V
    .locals 4

    iput-boolean p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mMultiScreenScroll:Z

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mMultiScreenScroll:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x5dc

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSnapDuration:I

    new-instance v0, Landroid/widget/Scroller;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-direct {v0, v1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    :cond_0
    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 3

    iput-object p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setOverScrollRatio(F)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOverScrollRatio:F

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->requestLayout()V

    return-void
.end method

.method public setOvershootTension(F)V
    .locals 1

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mOvershootTension:F

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollInterpolator:Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollInterpolator:Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;

    invoke-static {v0, p1}, Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;->-set0(Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;F)F

    :cond_0
    return-void
.end method

.method public setScreenAlignment(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignment:I

    return-void
.end method

.method public setScreenLayoutMode(I)V
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenLayoutMode:I

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setScreenSnapDuration(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSnapDuration:I

    return-void
.end method

.method public setScreenTransitionType(I)V
    .locals 6

    const/16 v5, 0x12c

    const/16 v4, 0x10e

    const/16 v3, 0x14a

    const v2, 0x3fa66666    # 1.3f

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenTransitionType:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenTransitionType:I

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenTransitionType:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/ScreenView;->setOvershootTension(F)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/ScreenView;->setScreenSnapDuration(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setOvershootTension(F)V

    invoke-virtual {p0, v4}, Lcom/android/systemui/taskmanager/ScreenView;->setScreenSnapDuration(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/ScreenView;->setOvershootTension(F)V

    invoke-virtual {p0, v5}, Lcom/android/systemui/taskmanager/ScreenView;->setScreenSnapDuration(I)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setOvershootTension(F)V

    invoke-virtual {p0, v3}, Lcom/android/systemui/taskmanager/ScreenView;->setScreenSnapDuration(I)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setOvershootTension(F)V

    invoke-virtual {p0, v3}, Lcom/android/systemui/taskmanager/ScreenView;->setScreenSnapDuration(I)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->setOvershootTension(F)V

    invoke-virtual {p0, v4}, Lcom/android/systemui/taskmanager/ScreenView;->setScreenSnapDuration(I)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/ScreenView;->setOvershootTension(F)V

    invoke-virtual {p0, v3}, Lcom/android/systemui/taskmanager/ScreenView;->setScreenSnapDuration(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
    .end packed-switch
.end method

.method public setScrollWholeScreen(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollWholeScreen:Z

    return-void
.end method

.method protected setTouchState(Landroid/view/MotionEvent;I)V
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    iput p2, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-interface {v4, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-nez v1, :cond_2

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    iput-boolean v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAllowLongPress:Z

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mGestureVelocityTracker:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->recycle()V

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->showSlideBar()V

    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    :cond_3
    iget-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAllowLongPress:Z

    if-eqz v1, :cond_4

    iput-boolean v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mAllowLongPress:Z

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/ScreenView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    :cond_4
    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchState:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mActivePointerId:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mLastMotionX:F

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    int-to-float v1, v1

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mTouchX:F

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    long-to-float v1, v2

    const v2, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSmoothingTime:F

    goto :goto_1
.end method

.method public setVisibility(I)V
    .locals 0

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView;->showSlideBar()V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method protected snapByVelocity(II)V
    .locals 13

    const/4 v10, 0x2

    const/4 v12, 0x1

    if-ne p2, v10, :cond_1

    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget v9, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollRightBound:I

    if-lt v8, v9, :cond_1

    :cond_0
    neg-int v8, p1

    invoke-direct {p0, v8}, Lcom/android/systemui/taskmanager/ScreenView;->startOverScrollFling(I)V

    return-void

    :cond_1
    if-ne p2, v12, :cond_2

    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget v9, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollLeftBound:I

    if-le v8, v9, :cond_0

    :cond_2
    iget-boolean v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollWholeScreen:Z

    if-eqz v8, :cond_6

    iget v3, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    :goto_0
    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    mul-int v4, v8, v3

    const/4 v1, -0x1

    iget v5, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    const/4 v8, 0x3

    if-eq p2, v8, :cond_5

    if-eq p2, v12, :cond_3

    if-ne p2, v10, :cond_9

    :cond_3
    iget-boolean v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->mMultiScreenScroll:Z

    if-eqz v8, :cond_7

    int-to-float v8, p1

    const/high16 v9, 0x447a0000    # 1000.0f

    div-float v2, v8, v9

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v8

    float-to-double v8, v8

    const-wide v10, 0x3fbc71c71c71c71cL    # 0.1111111111111111

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    sget-wide v10, Lcom/android/systemui/taskmanager/ScreenView;->MULTI_SCREEN_FLING_COEFFICIENT_VARIANT:D

    div-double v6, v8, v10

    double-to-int v1, v6

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    const-wide v10, 0x3949f623d5a8a733L    # 1.0E-32

    mul-double/2addr v8, v10

    double-to-float v0, v8

    if-ne p2, v12, :cond_4

    neg-float v0, v0

    :cond_4
    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget v9, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    add-int/2addr v8, v9

    int-to-float v8, v8

    add-float/2addr v8, v0

    shr-int/lit8 v9, v4, 0x1

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    float-to-int v5, v8

    :cond_5
    :goto_1
    const/4 v8, 0x0

    invoke-virtual {p0, v5, v8, v12, v1}, Lcom/android/systemui/taskmanager/ScreenView;->snapToScreen(IIZI)V

    return-void

    :cond_6
    const/4 v3, 0x1

    goto :goto_0

    :cond_7
    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    if-ne p2, v12, :cond_8

    neg-int v3, v3

    :cond_8
    add-int v5, v8, v3

    goto :goto_1

    :cond_9
    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    iget v9, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenAlignScrollOffset:I

    add-int/2addr v8, v9

    shr-int/lit8 v9, v4, 0x1

    add-int/2addr v8, v9

    iget v9, p0, Lcom/android/systemui/taskmanager/ScreenView;->mChildScreenWidth:I

    div-int v5, v8, v9

    goto :goto_1
.end method

.method public snapToScreen(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->snapToScreen(IIZ)V

    return-void
.end method

.method protected snapToScreen(IIZ)V
    .locals 6

    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/taskmanager/ScreenView;->snapToScreen(IIZLcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;I)V

    return-void
.end method

.method protected snapToScreen(IIZI)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/taskmanager/ScreenView;->snapToScreen(IIZLcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;I)V

    return-void
.end method

.method protected snapToScreen(IIZLcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;I)V
    .locals 8

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    if-gtz v0, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollWholeScreen:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mVisibleRange:I

    rem-int/2addr v1, v4

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    :cond_1
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mCurrentScreen:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v7

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSnapScreenOnceNotification:Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSnapScreenOnceNotification:Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;

    invoke-interface {v0, p0}, Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;->onSnapCancelled(Lcom/android/systemui/taskmanager/ScreenView;)V

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_3
    iput-object p4, p0, Lcom/android/systemui/taskmanager/ScreenView;->mSnapScreenOnceNotification:Lcom/android/systemui/taskmanager/ScreenView$SnapScreenOnceNotification;

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    if-eqz p3, :cond_4

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollInterpolator:Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;

    invoke-virtual {v0, v7, p2}, Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;->setDistance(II)V

    :goto_0
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mNextScreen:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenScrollX(I)I

    move-result v6

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    sub-int v3, v6, v0

    if-nez v3, :cond_5

    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollInterpolator:Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/ScreenView$ScreenViewOvershootInterpolator;->disableSettle()V

    goto :goto_0

    :cond_5
    if-gez p5, :cond_7

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSnapDuration:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenWidth:I

    div-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getScreenSnapMaxDuration()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p5

    if-lez p2, :cond_6

    int-to-float v0, p5

    int-to-float v1, p2

    const v4, 0x451c4000    # 2500.0f

    div-float/2addr v1, v4

    div-float/2addr v0, v1

    const v1, 0x3ecccccd    # 0.4f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    add-int/2addr p5, v0

    :cond_6
    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenSnapDuration:I

    invoke-static {v0, p5}, Ljava/lang/Math;->max(II)I

    move-result p5

    :cond_7
    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    move v4, v2

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->invalidate()V

    return-void
.end method

.method protected updateChildStaticTransformation(Landroid/view/View;)V
    .locals 13

    const/high16 v10, 0x40000000    # 2.0f

    const v12, 0x3e99999a    # 0.3f

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    instance-of v9, p1, Lcom/android/systemui/taskmanager/ScreenView$Indicator;

    if-eqz v9, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    int-to-float v1, v9

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    int-to-float v0, v9

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView;->getMeasuredWidth()I

    move-result v9

    int-to-float v9, v9

    div-float v4, v9, v10

    div-float v3, v1, v10

    div-float v2, v0, v10

    iget v9, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScrollX:I

    int-to-float v9, v9

    add-float/2addr v9, v4

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v10

    int-to-float v10, v10

    sub-float/2addr v9, v10

    sub-float/2addr v9, v3

    div-float v5, v9, v1

    iget v9, p0, Lcom/android/systemui/taskmanager/ScreenView;->mScreenTransitionType:I

    packed-switch v9, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->resetTransformation(Landroid/view/View;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->resetTransformation(Landroid/view/View;)V

    goto :goto_0

    :pswitch_3
    cmpl-float v9, v5, v8

    if-eqz v9, :cond_1

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v9

    cmpl-float v9, v9, v11

    if-lez v9, :cond_2

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->resetTransformation(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v9

    sub-float v9, v11, v9

    const v10, 0x3f333333    # 0.7f

    mul-float/2addr v9, v10

    add-float/2addr v9, v12

    invoke-virtual {p1, v9}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v11}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v11}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationX(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationY(F)V

    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_CAMERA_DISTANCE:F

    invoke-virtual {p1, v8}, Landroid/view/View;->setCameraDistance(F)V

    goto :goto_0

    :pswitch_4
    cmpl-float v9, v5, v8

    if-eqz v9, :cond_3

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v9

    cmpl-float v9, v9, v11

    if-lez v9, :cond_4

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->resetTransformation(Landroid/view/View;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v11}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v11}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v11}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotY(F)V

    neg-float v9, v5

    const/high16 v10, 0x41f00000    # 30.0f

    mul-float/2addr v9, v10

    invoke-virtual {p1, v9}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationX(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationY(F)V

    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_CAMERA_DISTANCE:F

    invoke-virtual {p1, v8}, Landroid/view/View;->setCameraDistance(F)V

    goto :goto_0

    :pswitch_5
    cmpl-float v9, v5, v8

    if-eqz v9, :cond_5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v9

    cmpl-float v9, v9, v11

    if-lez v9, :cond_6

    :cond_5
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->resetTransformation(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1, v11}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v11}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v11}, Landroid/view/View;->setScaleY(F)V

    cmpg-float v9, v5, v8

    if-gez v9, :cond_7

    move v1, v8

    :cond_7
    invoke-virtual {p1, v1}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationX(F)V

    const/high16 v8, -0x3d4c0000    # -90.0f

    mul-float/2addr v8, v5

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationY(F)V

    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-virtual {p1, v8}, Landroid/view/View;->setCameraDistance(F)V

    goto/16 :goto_0

    :pswitch_6
    cmpl-float v9, v5, v8

    if-eqz v9, :cond_8

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v9

    cmpl-float v9, v9, v11

    if-lez v9, :cond_9

    :cond_8
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->resetTransformation(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_9
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v9

    sub-float v9, v11, v9

    invoke-virtual {p1, v9}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationY(F)V

    mul-float v9, v1, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v10

    mul-float/2addr v10, v1

    mul-float/2addr v10, v12

    sub-float/2addr v9, v10

    invoke-virtual {p1, v9}, Landroid/view/View;->setTranslationX(F)V

    mul-float v9, v12, v5

    add-float v6, v11, v9

    invoke-virtual {p1, v6}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v6}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationX(F)V

    neg-float v8, v5

    const/high16 v9, 0x42340000    # 45.0f

    mul-float/2addr v8, v9

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationY(F)V

    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-virtual {p1, v8}, Landroid/view/View;->setCameraDistance(F)V

    goto/16 :goto_0

    :pswitch_7
    cmpg-float v9, v5, v8

    if-gtz v9, :cond_a

    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->resetTransformation(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_a
    sub-float v9, v11, v5

    invoke-virtual {p1, v9}, Landroid/view/View;->setAlpha(F)V

    sub-float v9, v11, v5

    const v10, 0x3ecccccd    # 0.4f

    mul-float/2addr v9, v10

    const v10, 0x3f19999a    # 0.6f

    add-float v7, v10, v9

    sub-float v9, v11, v7

    mul-float/2addr v9, v1

    const/high16 v10, 0x40400000    # 3.0f

    mul-float/2addr v9, v10

    invoke-virtual {p1, v9}, Landroid/view/View;->setTranslationX(F)V

    sub-float v9, v11, v7

    mul-float/2addr v9, v0

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v9, v10

    invoke-virtual {p1, v9}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v7}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v7}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationX(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationY(F)V

    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_CAMERA_DISTANCE:F

    invoke-virtual {p1, v8}, Landroid/view/View;->setCameraDistance(F)V

    goto/16 :goto_0

    :pswitch_8
    cmpl-float v9, v5, v8

    if-eqz v9, :cond_b

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v9

    cmpl-float v9, v9, v11

    if-lez v9, :cond_c

    :cond_b
    invoke-virtual {p0, p1}, Lcom/android/systemui/taskmanager/ScreenView;->resetTransformation(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_c
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v9

    sub-float v9, v11, v9

    invoke-virtual {p1, v9}, Landroid/view/View;->setAlpha(F)V

    mul-float v9, v1, v5

    invoke-virtual {p1, v9}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, v11}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v11}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setPivotY(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationX(F)V

    neg-float v8, v5

    const/high16 v9, 0x42b40000    # 90.0f

    mul-float/2addr v8, v9

    invoke-virtual {p1, v8}, Landroid/view/View;->setRotationY(F)V

    iget v8, p0, Lcom/android/systemui/taskmanager/ScreenView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-virtual {p1, v8}, Landroid/view/View;->setCameraDistance(F)V

    goto/16 :goto_0

    :pswitch_9
    invoke-virtual {p0, p1, v5}, Lcom/android/systemui/taskmanager/ScreenView;->updateChildStaticTransformationByScreen(Landroid/view/View;F)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method protected updateChildStaticTransformationByScreen(Landroid/view/View;F)V
    .locals 0

    return-void
.end method
