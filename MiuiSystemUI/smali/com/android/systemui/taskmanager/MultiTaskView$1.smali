.class Lcom/android/systemui/taskmanager/MultiTaskView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "MultiTaskView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/taskmanager/MultiTaskView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/MultiTaskView;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/MultiTaskView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getMultiTaskCount()I

    move-result v0

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    neg-float v1, p4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/MultiTaskView;->startScrolling(I)V

    return v2

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 11

    const/high16 v10, 0x41200000    # 10.0f

    const/4 v9, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v4, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mMultiTaskViewAlgorithm:Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;

    float-to-int v5, v2

    invoke-virtual {v4, v5}, Lcom/android/systemui/taskmanager/MultiTaskViewAlgorithm;->screenYToCurveProgress(I)F

    move-result v0

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget v4, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mLastP:F

    sub-float v5, v4, v0

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-static {v4}, Lcom/android/systemui/taskmanager/MultiTaskView;->-wrap0(Lcom/android/systemui/taskmanager/MultiTaskView;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/high16 v4, 0x3f800000    # 1.0f

    :goto_0
    mul-float v3, v5, v4

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget v1, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget v5, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    add-float/2addr v5, v3

    iput v5, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v5, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-static {v5}, Lcom/android/systemui/taskmanager/MultiTaskView;->-wrap1(Lcom/android/systemui/taskmanager/MultiTaskView;)F

    move-result v5

    invoke-static {}, Lcom/android/systemui/taskmanager/MultiTaskView;->-get0()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget v6, v6, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    iget-object v7, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v7}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v7

    invoke-static {}, Lcom/android/systemui/taskmanager/MultiTaskView;->-get0()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget v4, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    iget-object v5, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-static {v5}, Lcom/android/systemui/taskmanager/MultiTaskView;->-wrap1(Lcom/android/systemui/taskmanager/MultiTaskView;)F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    div-float v5, v3, v10

    add-float/2addr v5, v1

    iput v5, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iput v0, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mLastP:F

    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v4}, Lcom/android/systemui/taskmanager/MultiTaskView;->invalidate()V

    const/4 v4, 0x1

    return v4

    :cond_1
    const v4, 0x3f19999a    # 0.6f

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget v4, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    iget-object v5, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v5}, Lcom/android/systemui/taskmanager/MultiTaskView;->getTopScroll()F

    move-result v5

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_3

    cmpg-float v4, v2, v9

    if-gez v4, :cond_0

    cmpg-float v4, v3, v9

    if-gez v4, :cond_0

    :cond_3
    iget-object v4, p0, Lcom/android/systemui/taskmanager/MultiTaskView$1;->this$0:Lcom/android/systemui/taskmanager/MultiTaskView;

    div-float v5, v3, v10

    add-float/2addr v5, v1

    iput v5, v4, Lcom/android/systemui/taskmanager/MultiTaskView;->mScroll:F

    goto :goto_1
.end method
