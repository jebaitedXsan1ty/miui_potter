.class public Lcom/android/systemui/taskmanager/TaskInfo;
.super Ljava/lang/Object;
.source "TaskInfo.java"


# instance fields
.field private final MAX_COMPOUNDED_COUNT:I

.field final description:Ljava/lang/CharSequence;

.field final intent:Landroid/content/Intent;

.field isLocked:Z

.field final mComponentName:Landroid/content/ComponentName;

.field private mCompoundedTasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field volatile mIsLoadingScreenshot:Z

.field volatile mIsRealScreenshot:Z

.field private mLabel:Ljava/lang/CharSequence;

.field volatile mScreenshot:Landroid/graphics/Bitmap;

.field mScreenshotContentIsPort:Z

.field mScreenshotIsForPort:Z

.field mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

.field private mThumbnail:Landroid/graphics/Bitmap;

.field mTopRunning:Z

.field final mUserId:I

.field final packageName:Ljava/lang/String;

.field final persistentTaskId:I

.field final resolveInfo:Landroid/content/pm/ResolveInfo;

.field final taskId:I


# direct methods
.method public constructor <init>(Lcom/android/systemui/taskmanager/ScreenshotLoadManager;IILandroid/content/pm/ResolveInfo;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/CharSequence;Landroid/content/ComponentName;ZI)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    iput v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->MAX_COMPOUNDED_COUNT:I

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    iput-object p4, p0, Lcom/android/systemui/taskmanager/TaskInfo;->resolveInfo:Landroid/content/pm/ResolveInfo;

    iput-object p5, p0, Lcom/android/systemui/taskmanager/TaskInfo;->intent:Landroid/content/Intent;

    iput p2, p0, Lcom/android/systemui/taskmanager/TaskInfo;->taskId:I

    iput p3, p0, Lcom/android/systemui/taskmanager/TaskInfo;->persistentTaskId:I

    iput-object p7, p0, Lcom/android/systemui/taskmanager/TaskInfo;->description:Ljava/lang/CharSequence;

    iput-object p6, p0, Lcom/android/systemui/taskmanager/TaskInfo;->packageName:Ljava/lang/String;

    iput-object p8, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mComponentName:Landroid/content/ComponentName;

    iput-boolean p9, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mTopRunning:Z

    iput p10, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mCompoundedTasks:Ljava/util/List;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mCompoundedTasks:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public addCompoundedTask(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mCompoundedTasks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mCompoundedTasks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public getComponentName()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mComponentName:Landroid/content/ComponentName;

    return-object v0
.end method

.method public getCompoundedTasks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/taskmanager/TaskInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mCompoundedTasks:Ljava/util/List;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getLabel()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mLabel:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getScreenshot()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    invoke-virtual {v0, p0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->ensureLoadedScreenshot(Lcom/android/systemui/taskmanager/TaskInfo;)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public isCompoundTask()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mCompoundedTasks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseScreenshot()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mTopRunning:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    invoke-virtual {v0, p0}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->release(Lcom/android/systemui/taskmanager/TaskInfo;)V

    :cond_0
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mIcon:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setLabel(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mLabel:Ljava/lang/CharSequence;

    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskInfo;->mThumbnail:Landroid/graphics/Bitmap;

    return-void
.end method
