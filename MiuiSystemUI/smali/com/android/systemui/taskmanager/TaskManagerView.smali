.class public Lcom/android/systemui/taskmanager/TaskManagerView;
.super Landroid/widget/FrameLayout;
.source "TaskManagerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/taskmanager/TaskManagerView$1;,
        Lcom/android/systemui/taskmanager/TaskManagerView$2;,
        Lcom/android/systemui/taskmanager/TaskManagerView$3;,
        Lcom/android/systemui/taskmanager/TaskManagerView$4;,
        Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;,
        Lcom/android/systemui/taskmanager/TaskManagerView$TaskManagerHandler;
    }
.end annotation


# static fields
.field private static final ACTIVITY_SCREENSHOT_SCALE:F

.field private static isdDisplayOled:Z


# instance fields
.field private mAdBackground:Landroid/view/View;

.field private mAdButton:Landroid/view/View;

.field mAnimatorsNeedForceEnd:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private mArgbEvaluator:Landroid/animation/ArgbEvaluator;

.field private mBackground:Landroid/view/View;

.field private mBgHandler:Landroid/os/Handler;

.field private mCleaningToast:Landroid/widget/Toast;

.field private mClearAnimView:Lcom/android/systemui/taskmanager/CircleAndTickAnimView;

.field private mClearEndAnimatorCounter:I

.field mClickListenerForQuit:Landroid/view/View$OnClickListener;

.field mClickListenerForToggle:Landroid/view/View$OnClickListener;

.field mCloseRunnable:Ljava/lang/Runnable;

.field private mCurrentScreenshot:Landroid/graphics/Bitmap;

.field private mExitAnimSet:Landroid/animation/AnimatorSet;

.field private mExitScrollAnimator:Landroid/animation/ValueAnimator;

.field private mFadeoutAnimator:Landroid/animation/Animator;

.field private mFastBlurColor:I

.field private mFirstTaskIsTopRunning:Z

.field private mFreeAtFirst:J

.field private mFreeMemoryShow:J

.field private mHandler:Landroid/os/Handler;

.field private mHasPreLoad:Z

.field mImgPreviewClickListener:Landroid/view/View$OnClickListener;

.field private mIsAttachedToWindow:Z

.field private mIsFirstEnterTalkBack:Z

.field private mIsFreeze:Z

.field private mIsFromHome:Z

.field private mIsShow:Z

.field private mItemViewFactory:Lcom/android/systemui/taskmanager/TaskItemViewFactory;

.field private mLastPreLoadTime:J

.field private mMainContainer:Landroid/view/ViewGroup;

.field mMemoryAndClearContainer:Landroid/view/ViewGroup;

.field private mNavigationBarBackground:Landroid/graphics/drawable/Drawable;

.field mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

.field private mNeedHideTotalCount:I

.field private mOpeningTaskInScreenshot:Z

.field mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

.field mRecentsReceiver:Lcom/android/systemui/taskmanager/RecentsReceiver;

.field private mScreenRotation:I

.field private mScreenshotHeight:I

.field private mScreenshotMode:I

.field private mScreenshotModeAnimator:Landroid/animation/ValueAnimator;

.field private mScreenshotWidth:I

.field private mSeparatorForMemoryInfo:Landroid/view/View;

.field private mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mShowTaskByAnimationSet:Landroid/animation/AnimatorSet;

.field private mShowTime:J

.field mTasksView:Lcom/android/systemui/taskmanager/TasksView;

.field private mThemeId:Ljava/lang/String;

.field private mThemeName:Ljava/lang/String;

.field private mToggleButton:Landroid/widget/ImageView;

.field private mTotalMemory:J

.field private mTxtMemoryContainer:Landroid/view/ViewGroup;

.field private mTxtMemoryInfo1:Landroid/widget/TextView;

.field private mTxtMemoryInfo2:Landroid/widget/TextView;

.field private mTxtNoRecentApps:Landroid/widget/TextView;

.field private mUseBlurBackground:Z


# direct methods
.method static synthetic -get0(Lcom/android/systemui/taskmanager/TaskManagerView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/taskmanager/TaskManagerView;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFreeAtFirst:J

    return-wide v0
.end method

.method static synthetic -get2(Lcom/android/systemui/taskmanager/TaskManagerView;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/taskmanager/TaskManagerView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsShow:Z

    return v0
.end method

.method static synthetic -get4(Lcom/android/systemui/taskmanager/TaskManagerView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/systemui/taskmanager/TaskManagerView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    return v0
.end method

.method static synthetic -get6(Lcom/android/systemui/taskmanager/TaskManagerView;)Lcom/android/systemui/statusbar/phone/PhoneStatusBar;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/systemui/taskmanager/TaskManagerView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mToggleButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/systemui/taskmanager/TaskManagerView;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearEndAnimatorCounter:I

    return p1
.end method

.method static synthetic -set1(Lcom/android/systemui/taskmanager/TaskManagerView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCurrentScreenshot:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic -set2(Lcom/android/systemui/taskmanager/TaskManagerView;J)J
    .locals 1

    iput-wide p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFreeAtFirst:J

    return-wide p1
.end method

.method static synthetic -set3(Lcom/android/systemui/taskmanager/TaskManagerView;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/taskmanager/TaskManagerView;I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->checkNeedHide()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->doClearAnim()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->endAnimatorForClear()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->refreshUi()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/systemui/taskmanager/TaskManagerView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/TaskManagerView;->sendVisibilityChangedBroadcast(Z)V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->showFirstTask()V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/systemui/taskmanager/TaskManagerView;Lcom/android/systemui/taskmanager/PreviewIconView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/taskmanager/TaskManagerView;->showTaskByAnimation(Lcom/android/systemui/taskmanager/PreviewIconView;)V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/systemui/taskmanager/TaskManagerView;ZZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/taskmanager/TaskManagerView;->show(ZZZ)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    const-string/jumbo v1, "oled"

    const-string/jumbo v2, "ro.display.type"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/android/systemui/taskmanager/TaskManagerView;->isdDisplayOled:Z

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110b0013

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    sput v1, Lcom/android/systemui/taskmanager/TaskManagerView;->ACTIVITY_SCREENSHOT_SCALE:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/taskmanager/TaskManagerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/taskmanager/TaskManagerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/systemui/taskmanager/TaskItemViewFactory;

    invoke-direct {v0}, Lcom/android/systemui/taskmanager/TaskItemViewFactory;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mItemViewFactory:Lcom/android/systemui/taskmanager/TaskItemViewFactory;

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsAttachedToWindow:Z

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotModeAnimator:Landroid/animation/ValueAnimator;

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mUseBlurBackground:Z

    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mArgbEvaluator:Landroid/animation/ArgbEvaluator;

    iput-boolean v4, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFirstEnterTalkBack:Z

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mHasPreLoad:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mAnimatorsNeedForceEnd:Ljava/util/List;

    new-instance v0, Lcom/android/systemui/taskmanager/RecentsReceiver;

    invoke-direct {v0}, Lcom/android/systemui/taskmanager/RecentsReceiver;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentsReceiver:Lcom/android/systemui/taskmanager/RecentsReceiver;

    new-instance v0, Lcom/android/systemui/taskmanager/TaskManagerView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$1;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mImgPreviewClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/systemui/taskmanager/TaskManagerView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$2;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClickListenerForQuit:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/systemui/taskmanager/TaskManagerView$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$3;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClickListenerForToggle:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/systemui/taskmanager/TaskManagerView$4;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$4;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCloseRunnable:Ljava/lang/Runnable;

    invoke-static {}, Lmiui/util/HardwareInfo;->getTotalPhysicalMemory()J

    move-result-wide v0

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTotalMemory:J

    const-string/jumbo v0, "TaskManagerView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getTotalPhysicalMemory in TaskManagerViewInit:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTotalMemory:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x8

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    new-instance v0, Lcom/android/systemui/taskmanager/TaskManagerView$TaskManagerHandler;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$TaskManagerHandler;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mHandler:Landroid/os/Handler;

    const-string/jumbo v0, "TaskManagerView"

    invoke-static {v0}, Lcom/android/systemui/taskmanager/Utils;->getBgHandler(Ljava/lang/String;)Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mBgHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d016c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCleaningToast:Landroid/widget/Toast;

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private checkNeedHide()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNeedHideTotalCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNeedHideTotalCount:I

    iget v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNeedHideTotalCount:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCloseRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {p0, v2}, Lcom/android/systemui/taskmanager/TaskManagerView;->show(Z)V

    :cond_0
    return-void
.end method

.method private doClearAnim()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->refreshMemoryInfo()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearAnimView:Lcom/android/systemui/taskmanager/CircleAndTickAnimView;

    new-instance v1, Lcom/android/systemui/taskmanager/TaskManagerView$20;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$20;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->animatorStart(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    new-instance v1, Lcom/android/systemui/taskmanager/TaskManagerView$21;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$21;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/TasksView;->playClearAllAnimator(Ljava/lang/Runnable;)V

    return-void
.end method

.method private endAnimatorForClear()V
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearEndAnimatorCounter:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearEndAnimatorCounter:I

    iget v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearEndAnimatorCounter:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->endForClear()V

    :cond_0
    return-void
.end method

.method private endForClear()V
    .locals 4

    new-instance v0, Lcom/android/systemui/taskmanager/TaskManagerView$7;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$7;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/systemui/taskmanager/TaskManagerView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private getFastBlurColor()I
    .locals 15

    const/high16 v14, -0x1000000

    const/4 v13, 0x0

    const/4 v12, 0x1

    iget-boolean v8, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mHasPreLoad:Z

    if-eqz v8, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mLastPreLoadTime:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x7d0

    cmp-long v8, v8, v10

    if-gtz v8, :cond_0

    iget v8, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    return v8

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget-boolean v8, Lcom/android/systemui/taskmanager/TaskManagerView;->isdDisplayOled:Z

    if-eqz v8, :cond_4

    const v8, 0x11060011

    :goto_0
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iget-object v8, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCurrentScreenshot:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCurrentScreenshot:Landroid/graphics/Bitmap;

    iget-object v9, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCurrentScreenshot:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0xa

    iget-object v10, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCurrentScreenshot:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0xa

    invoke-static {v8, v9, v10, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_1
    :try_start_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    :goto_2
    if-gt v7, v12, :cond_1

    if-le v5, v12, :cond_7

    :cond_1
    div-int/lit8 v7, v7, 0x2

    div-int/lit8 v5, v5, 0x2

    if-ge v7, v12, :cond_2

    const/4 v7, 0x1

    :cond_2
    if-ge v5, v12, :cond_3

    const/4 v5, 0x1

    :cond_3
    const/4 v8, 0x1

    invoke-static {v0, v7, v5, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    goto :goto_2

    :cond_4
    const v8, 0x11060010

    goto :goto_0

    :cond_5
    iget-object v8, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    const v9, 0x3dcccccd    # 0.1f

    invoke-static {v8, v9, v13, v13, v12}, Lmiui/util/ScreenshotUtils;->getScreenshot(Landroid/content/Context;FIIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_6

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v6, v8, v12}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    :cond_6
    or-int v8, v3, v14

    return v8

    :cond_7
    :try_start_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Bitmap;->getPixel(II)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    return v2

    :catch_0
    move-exception v4

    :try_start_2
    const-string/jumbo v8, "TaskManagerView"

    const-string/jumbo v9, "getFastBlurColor"

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    or-int v8, v3, v14

    return v8

    :catchall_0
    move-exception v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    throw v8
.end method

.method private getHomes()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v6, "android.intent.action.MAIN"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "android.intent.category.HOME"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v6, 0x10000

    invoke-virtual {v2, v0, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private getIsScreenshotModePersist()Z
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->isForceScreenShotMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmiui/util/ScreenshotUtils;->disallowTaskManagerScreenshotMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const-string/jumbo v0, "persist.sys.screenshot_mode"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private initScreenshotModePersistIfNeed()V
    .locals 3

    const-string/jumbo v1, "persist.sys.screenshot_mode"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    invoke-static {}, Lmiui/util/MiuiFeatureUtils;->isLiteMode()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    or-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->setScreenshotModePersist(Z)V

    :cond_0
    return-void
.end method

.method private isForceScreenShotMode()Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/systemui/taskmanager/Utils;->isTalkBackMode(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private isMemInfoShow()Z
    .locals 4

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "miui_recents_show_mem_info"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v1

    return v1
.end method

.method private isUserSetup()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "device_provisioned"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "user_setup_complete"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private refreshUi()V
    .locals 5

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v1}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v4, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    if-eqz v0, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/android/systemui/taskmanager/TasksView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtNoRecentApps:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v1, Lcom/android/systemui/taskmanager/TaskManagerView$17;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$17;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v3, v2

    goto :goto_2
.end method

.method private sendVisibilityChangedBroadcast(Z)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.SYSTEM_UI_VISIBILITY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "is_show"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void
.end method

.method private setScreenshotModePersist(Z)V
    .locals 2

    const-string/jumbo v1, "persist.sys.screenshot_mode"

    if-eqz p1, :cond_0

    const-string/jumbo v0, "1"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "0"

    goto :goto_0
.end method

.method private show(ZZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->show(ZZZZ)V

    return-void
.end method

.method private showFirstTask()V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/TasksView;->getScreen(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v0, v1, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v1, v0, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->getMultiTaskCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/PreviewIconView;

    invoke-direct {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->showTaskByAnimation(Lcom/android/systemui/taskmanager/PreviewIconView;)V

    return-void
.end method

.method private showTask(Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    const-string/jumbo v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v1, 0x0

    iget v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    if-ne v6, v10, :cond_0

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getHandler()Landroid/os/Handler;

    move-result-object v7

    new-instance v8, Lcom/android/systemui/taskmanager/TaskManagerView$6;

    invoke-direct {v8, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$6;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-static {v6, v9, v9, v7, v8}, Landroid/app/ActivityOptions;->makeCustomAnimation(Landroid/content/Context;IILandroid/os/Handler;Landroid/app/ActivityOptions$OnAnimationStartedListener;)Landroid/app/ActivityOptions;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->isAttachedToWindow()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v7, "window"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager$LayoutParams;

    iput v11, v4, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    invoke-interface {v5, p0, v4}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-boolean v6, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mTopRunning:Z

    if-eqz v6, :cond_2

    iput v10, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNeedHideTotalCount:I

    :goto_0
    return-void

    :cond_2
    iget v6, p1, Lcom/android/systemui/taskmanager/TaskInfo;->taskId:I

    if-ltz v6, :cond_3

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    iget v7, p1, Lcom/android/systemui/taskmanager/TaskInfo;->taskId:I

    invoke-virtual {v6, v7}, Lcom/android/systemui/taskmanager/RecentTasksManager;->isTaskActive(I)Z

    move-result v6

    if-eqz v6, :cond_3

    iget v6, p1, Lcom/android/systemui/taskmanager/TaskInfo;->taskId:I

    invoke-virtual {v0, v6, v10, v1}, Landroid/app/ActivityManager;->moveTaskToFront(IILandroid/os/Bundle;)V

    iput v11, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNeedHideTotalCount:I

    goto :goto_0

    :cond_3
    const/4 v6, 0x2

    iput v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNeedHideTotalCount:I

    iget-object v3, p1, Lcom/android/systemui/taskmanager/TaskInfo;->intent:Landroid/content/Intent;

    const v6, 0x10104000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :try_start_0
    iget v6, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    invoke-static {v6}, Lmiui/securityspace/XSpaceUserHandle;->isXSpaceUserId(I)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/os/UserHandle;

    iget v8, p1, Lcom/android/systemui/taskmanager/TaskInfo;->mUserId:I

    invoke-direct {v7, v8}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v6, v3, v1, v7}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    :goto_1
    const-string/jumbo v6, "systemui_taskmanager_toggleapp"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string/jumbo v6, "TaskManagerView"

    const-string/jumbo v7, "start activity failed."

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_4
    :try_start_1
    iget-object v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v6, v3, v1, v7}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private showTaskByAnimation(Lcom/android/systemui/taskmanager/PreviewIconView;)V
    .locals 26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/systemui/taskmanager/TasksView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/view/ViewPropertyAnimator;->cancel()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    invoke-virtual/range {v19 .. v20}, Lcom/android/systemui/taskmanager/TasksView;->setScaleX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v19, v0

    const/high16 v20, 0x3f800000    # 1.0f

    invoke-virtual/range {v19 .. v20}, Lcom/android/systemui/taskmanager/TasksView;->setScaleY(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/android/systemui/taskmanager/TasksView;->setTranslationX(F)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getTaskItemView()Lcom/android/systemui/taskmanager/TaskItemView;

    move-result-object v18

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/systemui/taskmanager/PreviewIconView;->mTask:Lcom/android/systemui/taskmanager/TaskInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mOpeningTaskInScreenshot:Z

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v8, v0, [I

    invoke-virtual/range {v18 .. v18}, Lcom/android/systemui/taskmanager/TaskItemView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/view/ViewPropertyAnimator;->cancel()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string/jumbo v20, "window"

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/view/WindowManager;

    invoke-interface/range {v19 .. v19}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    new-instance v16, Landroid/graphics/Point;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    new-instance v12, Landroid/util/TypedValue;

    invoke-direct {v12}, Landroid/util/TypedValue;-><init>()V

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x110b0013

    const/16 v21, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v12, v2}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    invoke-virtual {v12}, Landroid/util/TypedValue;->getFloat()F

    move-result v4

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v19

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v20

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/systemui/taskmanager/Utils;->isScreenPort(Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_2

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v4

    const/high16 v20, 0x3f000000    # 0.5f

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v15, v0, :cond_2

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x10500f8

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Point;->y:I

    :cond_0
    :goto_0
    new-instance v19, Landroid/animation/AnimatorSet;

    invoke-direct/range {v19 .. v19}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTaskByAnimationSet:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTaskByAnimationSet:Landroid/animation/AnimatorSet;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f100009

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/android/systemui/taskmanager/PreviewIconView;->getLocationOnScreen([I)V

    const/16 v19, 0x0

    aget v20, v8, v19

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getScaleX()F

    move-result v22

    mul-float v21, v21, v22

    const/high16 v22, 0x40000000    # 2.0f

    div-float v21, v21, v22

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    aput v20, v8, v19

    const/16 v19, 0x1

    aget v20, v8, v19

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getScaleY()F

    move-result v22

    mul-float v21, v21, v22

    const/high16 v22, 0x40000000    # 2.0f

    div-float v21, v21, v22

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    aput v20, v8, v19

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    div-int/lit8 v19, v19, 0x2

    const/16 v20, 0x0

    aget v20, v8, v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v10, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    div-int/lit8 v19, v19, 0x2

    const/16 v20, 0x1

    aget v20, v8, v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v11, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getWidth()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v19, v19, v20

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getScaleX()F

    move-result v20

    div-float v13, v19, v20

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v19, v19, v20

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getScaleY()F

    move-result v20

    div-float v14, v19, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aget v20, v8, v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/view/ViewGroup;->setPivotX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    aget v20, v8, v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/view/ViewGroup;->setPivotY(F)V

    invoke-virtual/range {p1 .. p1}, Lcom/android/systemui/taskmanager/PreviewIconView;->getTaskItemView()Lcom/android/systemui/taskmanager/TaskItemView;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v9, v0, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    iget-object v0, v9, Lcom/android/systemui/taskmanager/MultiTaskView;->mPreviewIconViewList:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/systemui/taskmanager/PreviewIconView;

    move-object/from16 v0, p1

    if-eq v6, v0, :cond_1

    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/android/systemui/taskmanager/PreviewIconView;->setAlpha(F)V

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/android/systemui/taskmanager/Utils;->isScreenPort(Landroid/content/Context;)Z

    move-result v19

    if-nez v19, :cond_0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v4

    const/high16 v20, 0x3f000000    # 0.5f

    add-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v15, v0, :cond_0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x10500f8

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Point;->x:I

    goto/16 :goto_0

    :cond_3
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/android/systemui/taskmanager/MultiTaskView;->setIsShowLockImg(Z)V

    invoke-virtual {v9}, Lcom/android/systemui/taskmanager/MultiTaskView;->forceFinished()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTaskByAnimationSet:Landroid/animation/AnimatorSet;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v21, v0

    sget-object v22, Lcom/android/systemui/taskmanager/TaskManagerView;->TRANSLATION_X:Landroid/util/Property;

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    div-float v24, v10, v13

    const/16 v25, 0x0

    aput v24, v23, v25

    invoke-static/range {v21 .. v23}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v21

    const/16 v22, 0x0

    aput-object v21, v20, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v21, v0

    sget-object v22, Lcom/android/systemui/taskmanager/TaskManagerView;->TRANSLATION_Y:Landroid/util/Property;

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput v11, v23, v24

    invoke-static/range {v21 .. v23}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v21

    const/16 v22, 0x1

    aput-object v21, v20, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v21, v0

    sget-object v22, Lcom/android/systemui/taskmanager/TaskManagerView;->SCALE_X:Landroid/util/Property;

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput v13, v23, v24

    invoke-static/range {v21 .. v23}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v21

    const/16 v22, 0x2

    aput-object v21, v20, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v21, v0

    sget-object v22, Lcom/android/systemui/taskmanager/TaskManagerView;->SCALE_Y:Landroid/util/Property;

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput v14, v23, v24

    invoke-static/range {v21 .. v23}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v21

    const/16 v22, 0x3

    aput-object v21, v20, v22

    invoke-virtual/range {v19 .. v20}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTaskByAnimationSet:Landroid/animation/AnimatorSet;

    move-object/from16 v19, v0

    new-instance v20, Lcom/android/systemui/taskmanager/TaskManagerView$5;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView$5;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual/range {v19 .. v20}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTaskByAnimationSet:Landroid/animation/AnimatorSet;

    move-object/from16 v19, v0

    new-instance v20, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct/range {v20 .. v20}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual/range {v19 .. v20}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTaskByAnimationSet:Landroid/animation/AnimatorSet;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/animation/AnimatorSet;->start()V

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskItemView;->mTitleInScreenshotMode:Landroid/widget/TextView;

    move-object/from16 v19, v0

    sget-object v20, Lcom/android/systemui/taskmanager/TaskManagerView;->ALPHA:Landroid/util/Property;

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aput v22, v21, v23

    invoke-static/range {v19 .. v21}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f100009

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v20

    div-int/lit8 v20, v20, 0x2

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/animation/ObjectAnimator;->start()V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->showTask(Lcom/android/systemui/taskmanager/TaskInfo;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCloseRunnable:Ljava/lang/Runnable;

    move-object/from16 v20, v0

    const-wide/16 v22, 0x1f4

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-wide/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->freeze()V

    :goto_2
    return-void

    :cond_4
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->show(Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->showTask(Lcom/android/systemui/taskmanager/TaskInfo;)V

    goto :goto_2
.end method

.method private startScreenshotFadeAnimator()V
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TasksView;->startScreenshotModeAnimating()V

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotModeAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotModeAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotModeAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotModeAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/systemui/taskmanager/TaskManagerView$18;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$18;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotModeAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/systemui/taskmanager/TaskManagerView$19;

    invoke-direct {v1, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$19;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotModeAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private updateActivityScreenshotSize()V
    .locals 2

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lmiui/util/ScreenshotUtils;->getActivityScreenshotSize(Landroid/content/Context;Landroid/graphics/Point;)V

    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotWidth:I

    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotHeight:I

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFreeze:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public freeze()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFreeze:Z

    return-void
.end method

.method public getFreeMemory()J
    .locals 6

    const-wide/16 v2, 0x0

    const-string/jumbo v1, "sagit"

    sget-object v4, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "jason"

    sget-object v4, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/miui/daemon/performance/PerfShielderManager;->getFreeMemory()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    :goto_0
    const-string/jumbo v1, "TaskManagerView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "getFreeMemory:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v4, 0x400

    div-long v4, v2, v4

    return-wide v4

    :catch_0
    move-exception v0

    const-string/jumbo v1, "TaskManagerView"

    const-string/jumbo v4, "getFreeMemory"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {}, Landroid/os/Process;->getFreeMemory()J

    move-result-wide v2

    goto :goto_0

    :cond_1
    invoke-static {}, Lmiui/util/HardwareInfo;->getFreeMemory()J

    move-result-wide v2

    goto :goto_0
.end method

.method public getItemViewFactory()Lcom/android/systemui/taskmanager/TaskItemViewFactory;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mItemViewFactory:Lcom/android/systemui/taskmanager/TaskItemViewFactory;

    return-object v0
.end method

.method public getModeAnimatingOriginalRatio()F
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotModeAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getScreenRotation()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenRotation:I

    return v0
.end method

.method public getScreenshotHeight()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotHeight:I

    return v0
.end method

.method public getScreenshotMode()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    return v0
.end method

.method public getScreenshotRatio()F
    .locals 2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    return v1

    :pswitch_2
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getModeAnimatingOriginalRatio()F

    move-result v0

    sub-float v0, v1, v0

    return v0

    :pswitch_3
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getModeAnimatingOriginalRatio()F

    move-result v0

    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getScreenshotWidth()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotWidth:I

    return v0
.end method

.method public getTasksView()Lcom/android/systemui/taskmanager/TasksView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    return-object v0
.end method

.method public gotoIconMode()V
    .locals 3

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->isForceScreenShotMode()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->setScreenshotModePersist(Z)V

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->startScreenshotFadeAnimator()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "ScreenshotMode"

    const-string/jumbo v2, "gotoIconMode"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "systemui_taskmanager_toggle_icon_screenshot"

    invoke-static {v1, v0}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    return-void
.end method

.method public gotoScreenshotMode()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lmiui/util/ScreenshotUtils;->disallowTaskManagerScreenshotMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->setScreenshotModePersist(Z)V

    const/4 v1, 0x4

    iput v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->startScreenshotFadeAnimator()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "ScreenshotMode"

    const-string/jumbo v2, "gotoScreenshotMode"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "systemui_taskmanager_toggle_icon_screenshot"

    invoke-static {v1, v0}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    return-void
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isHome()Z
    .locals 7

    const/4 v6, 0x0

    iget-boolean v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mHasPreLoad:Z

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mLastPreLoadTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFromHome:Z

    return v2

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    return v6

    :cond_2
    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getHomes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFromHome:Z

    iget-boolean v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFromHome:Z

    return v2
.end method

.method public isQucikSearh()Z
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    return v4

    :cond_1
    const-string/jumbo v3, "com.android.quicksearchbox.SearchActivity"

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    return v2
.end method

.method public isShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsShow:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 6

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    const-string/jumbo v0, "TaskManagerView"

    const-string/jumbo v1, "onAttachedToWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsAttachedToWindow:Z

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v0, "android.intent.action.PHONE_STATE"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "com.android.systemui.closeRecents"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "com.android.systemui.taskmanager.Clear"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentsReceiver:Lcom/android/systemui/taskmanager/RecentsReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    const-string/jumbo v0, "TaskManagerView"

    const-string/jumbo v1, "onDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsAttachedToWindow:Z

    iget-boolean v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsShow:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "TaskManagerView"

    const-string/jumbo v1, "Unexpect detach at TaskManagerView."

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentsReceiver:Lcom/android/systemui/taskmanager/RecentsReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 6

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->initScreenshotModePersistIfNeed()V

    invoke-direct {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getIsScreenshotModePersist()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    iput v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    new-instance v1, Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v4}, Lcom/android/systemui/taskmanager/RecentTasksManager;-><init>(Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    const v1, 0x7f0f01c9

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0f01cb

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/TasksView;

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v1, p0}, Lcom/android/systemui/taskmanager/TasksView;->setTaskManagerView(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    const v1, 0x7f0f01cd

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtNoRecentApps:Landroid/widget/TextView;

    const v1, 0x7f0f012e

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtMemoryContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0f012f

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtMemoryInfo1:Landroid/widget/TextView;

    const v1, 0x7f0f0131

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtMemoryInfo2:Landroid/widget/TextView;

    const v1, 0x7f0f0130

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mSeparatorForMemoryInfo:Landroid/view/View;

    const v1, 0x7f0f012d

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearAnimView:Lcom/android/systemui/taskmanager/CircleAndTickAnimView;

    const v1, 0x7f0f01ce

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mToggleButton:Landroid/widget/ImageView;

    const v1, 0x7f0f005e

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mBackground:Landroid/view/View;

    const v1, 0x7f0f01ca

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mAdBackground:Landroid/view/View;

    const v1, 0x7f0f01cc

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mAdButton:Landroid/view/View;

    const v1, 0x7f0f012c

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mBackground:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v1, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mBackground:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, v4, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mBackground:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iput-boolean v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mUseBlurBackground:Z

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/TasksView;->setLoader(Lcom/android/systemui/taskmanager/RecentTasksManager;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearAnimView:Lcom/android/systemui/taskmanager/CircleAndTickAnimView;

    const v2, 0x7f0201b8

    const v3, 0x7f0201ba

    const v4, 0x7f0201b9

    const v5, 0x7f0201bb

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setDrawables(IIII)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClickListenerForQuit:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/TasksView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClickListenerForQuit:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearAnimView:Lcom/android/systemui/taskmanager/CircleAndTickAnimView;

    new-instance v2, Lcom/android/systemui/taskmanager/TaskManagerView$8;

    invoke-direct {v2, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$8;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearAnimView:Lcom/android/systemui/taskmanager/CircleAndTickAnimView;

    new-instance v2, Lcom/android/systemui/taskmanager/TaskManagerView$9;

    invoke-direct {v2, p0}, Lcom/android/systemui/taskmanager/TaskManagerView$9;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual {v1, v2}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mToggleButton:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClickListenerForToggle:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->setImportantForAccessibility(I)V

    return-void

    :cond_1
    const/4 v1, 0x3

    goto/16 :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->show(Z)V

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public refreshMemoryAndClearContainerAlpha()V
    .locals 5

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v3}, Lcom/android/systemui/taskmanager/TasksView;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v3, v2}, Lcom/android/systemui/taskmanager/TasksView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/taskmanager/TaskItemView;

    iget-object v3, v1, Lcom/android/systemui/taskmanager/TaskItemView;->mMultiTaskView:Lcom/android/systemui/taskmanager/MultiTaskView;

    invoke-virtual {v3}, Lcom/android/systemui/taskmanager/MultiTaskView;->getMemoryAndClearContainerAlpha()F

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getAlpha()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method public refreshMemoryInfo()V
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFreeMemoryShow:J

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getFreeMemory()J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFreeMemoryShow:J

    iget-wide v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFreeMemoryShow:J

    invoke-static {v6, v7}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getFormatedMemory(J)Ljava/lang/String;

    move-result-object v0

    iget-wide v6, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTotalMemory:J

    invoke-static {v6, v7}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getFormatedMemory(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtMemoryInfo1:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    new-array v6, v11, [Ljava/lang/Object;

    aput-object v0, v6, v4

    aput-object v2, v6, v10

    const v7, 0x7f0d025a

    invoke-virtual {v5, v7, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtMemoryInfo2:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    new-array v6, v11, [Ljava/lang/Object;

    aput-object v0, v6, v4

    aput-object v2, v6, v10

    const v7, 0x7f0d025b

    invoke-virtual {v5, v7, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtMemoryInfo1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtMemoryInfo2:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    :goto_0
    iget-object v5, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mSeparatorForMemoryInfo:Landroid/view/View;

    if-eqz v1, :cond_1

    const/16 v3, 0x8

    :goto_1
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearAnimView:Lcom/android/systemui/taskmanager/CircleAndTickAnimView;

    iget-object v5, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    new-array v6, v11, [Ljava/lang/Object;

    aput-object v0, v6, v4

    aput-object v2, v6, v10

    const v4, 0x7f0d025d

    invoke-virtual {v5, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method

.method public refreshMemoryInfo(I)V
    .locals 4

    iget-wide v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFreeMemoryShow:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFreeMemoryShow:J

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->refreshMemoryInfo()V

    return-void
.end method

.method public setFirstTaskIsTopRunning(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFirstTaskIsTopRunning:Z

    return-void
.end method

.method public setScreenshotMode(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    return-void
.end method

.method public setService(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V
    .locals 2

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/RecentTasksManager;->setMainHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/RecentTasksManager;->setBgHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentsReceiver:Lcom/android/systemui/taskmanager/RecentsReceiver;

    invoke-virtual {v0, p0}, Lcom/android/systemui/taskmanager/RecentsReceiver;->setTaskManagerView(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Please don\'t call this method directly, call show() method instead."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public show(Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->show(ZZ)V

    return-void
.end method

.method public show(ZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->show(ZZZ)V

    return-void
.end method

.method public show(ZZZZ)V
    .locals 29

    const-string/jumbo v22, "TaskManagerView"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v24, "show("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string/jumbo v24, "), mIsShow:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsShow:Z

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->isUserSetup()Z

    move-result v22

    if-nez v22, :cond_0

    const-string/jumbo v22, "TaskManagerView"

    const-string/jumbo v23, "the device has\'not set up"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsShow:Z

    move/from16 v22, v0

    move/from16 v0, p1

    move/from16 v1, v22

    if-ne v0, v1, :cond_1

    return-void

    :cond_1
    if-eqz p1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getVisibility()I

    move-result v22

    if-eqz v22, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFadeoutAnimator:Landroid/animation/Animator;

    move-object/from16 v22, v0

    if-eqz v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFadeoutAnimator:Landroid/animation/Animator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/Animator;->isRunning()Z

    move-result v22

    if-nez v22, :cond_5

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    if-eqz v22, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v22

    if-nez v22, :cond_5

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitScrollAnimator:Landroid/animation/ValueAnimator;

    move-object/from16 v22, v0

    if-eqz v22, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitScrollAnimator:Landroid/animation/ValueAnimator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v22

    if-nez v22, :cond_5

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTaskByAnimationSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    if-eqz v22, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTaskByAnimationSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v22

    if-eqz v22, :cond_6

    :cond_5
    if-eqz p2, :cond_6

    const-string/jumbo v22, "TaskManagerView"

    const-string/jumbo v23, "Fadeout isn\'t finished, so ignore"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_6
    if-eqz p1, :cond_7

    xor-int/lit8 v22, p2, 0x1

    if-eqz v22, :cond_7

    new-instance v22, Ljava/lang/RuntimeException;

    const-string/jumbo v23, "Unsupported operation."

    invoke-direct/range {v22 .. v23}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v22

    :cond_7
    if-eqz p1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/RecentTasksManager;->isCleaning()Z

    move-result v22

    if-eqz v22, :cond_8

    const-string/jumbo v22, "TaskManagerView"

    const-string/jumbo v23, "show TaskManagerView When Cleaning , just try to skip this show action"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCleaningToast:Landroid/widget/Toast;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/Toast;->show()V

    return-void

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsAttachedToWindow:Z

    move/from16 v22, v0

    if-nez v22, :cond_9

    return-void

    :cond_9
    if-eqz p1, :cond_25

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->updateActivityScreenshotSize()V

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mOpeningTaskInScreenshot:Z

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getIsScreenshotModePersist()Z

    move-result v22

    if-eqz v22, :cond_d

    const/16 v22, 0x1

    :goto_0
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v22

    const-string/jumbo v23, "window"

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/WindowManager;

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/view/WindowManager$LayoutParams;

    const/16 v22, 0xe

    move/from16 v0, v22

    iput v0, v9, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v9}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/Display;->getRotation()I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenRotation:I

    const/16 v16, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    move-object/from16 v22, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x6

    move-object/from16 v0, v22

    move/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->initialize(ILandroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/RecentTasksManager;->getRecentTasks()Ljava/util/ArrayList;

    move-result-object v20

    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->isHome()Z

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFromHome:Z

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v22

    if-lez v22, :cond_e

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/systemui/taskmanager/TaskInfo;

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mTopRunning:Z

    move/from16 v22, v0

    :goto_1
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mFirstTaskIsTopRunning:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFromHome:Z

    move/from16 v22, v0

    if-nez v22, :cond_11

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFirstTaskIsTopRunning:Z

    move/from16 v22, v0

    if-eqz v22, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCurrentScreenshot:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    if-nez v22, :cond_a

    const/16 v10, 0x7530

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    sget v23, Lcom/android/systemui/taskmanager/TaskManagerView;->ACTIVITY_SCREENSHOT_SCALE:F

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-static {v0, v1, v2, v10, v3}, Lmiui/util/ScreenshotUtils;->getScreenshot(Landroid/content/Context;FIIZ)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mCurrentScreenshot:Landroid/graphics/Bitmap;

    :cond_a
    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/systemui/taskmanager/TaskInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mCurrentScreenshot:Landroid/graphics/Bitmap;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshot:Landroid/graphics/Bitmap;

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/systemui/taskmanager/TaskInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v23

    move-object/from16 v0, v23

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v23, v0

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_f

    const/16 v23, 0x1

    move/from16 v24, v23

    :goto_2
    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/android/systemui/taskmanager/TaskInfo;

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotIsForPort:Z

    move/from16 v0, v24

    move-object/from16 v1, v22

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskInfo;->mScreenshotContentIsPort:Z

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->isQucikSearh()Z

    move-result v22

    if-eqz v22, :cond_10

    const-string/jumbo v22, "from"

    const-string/jumbo v23, "Home"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-interface {v14, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    const/4 v11, 0x0

    const/4 v8, 0x0

    :goto_4
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v8, v0, :cond_12

    move-object/from16 v0, v20

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/TaskInfo;->isCompoundTask()Z

    move-result v22

    if-eqz v22, :cond_c

    add-int/lit8 v11, v11, 0x1

    :cond_c
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_d
    const/16 v22, 0x3

    goto/16 :goto_0

    :cond_e
    const/16 v22, 0x0

    goto/16 :goto_1

    :cond_f
    const/16 v23, 0x0

    move/from16 v24, v23

    goto :goto_2

    :cond_10
    const-string/jumbo v22, "from"

    const-string/jumbo v23, "App"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-interface {v14, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_11
    const-string/jumbo v22, "from"

    const-string/jumbo v23, "Home"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-interface {v14, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_12
    const-string/jumbo v22, "multi_task_num"

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-interface {v14, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v22, "systemui_taskmanager_show"

    move-object/from16 v0, v22

    invoke-static {v0, v14}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v22

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mRecentTasksMgr:Lcom/android/systemui/taskmanager/RecentTasksManager;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/systemui/taskmanager/RecentTasksManager;->mScreenshotManager:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    move-object/from16 v22, v0

    const/16 v23, 0x4

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->startLoad(I)V

    :cond_13
    new-instance v4, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v4, v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;Ljava/util/List;)V

    new-instance v22, Lcom/android/systemui/taskmanager/TaskManagerView$10;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView$10;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTxtMemoryContainer:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->isMemInfoShow()Z

    move-result v22

    if-eqz v22, :cond_1f

    const/16 v22, 0x0

    :goto_5
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/android/systemui/taskmanager/TasksView;->setAdapter(Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TasksView;->hasInitCurrentScreen:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/high16 v23, 0x3f800000    # 1.0f

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setAlpha(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/high16 v23, 0x3f800000    # 1.0f

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setScaleX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/high16 v23, 0x3f800000    # 1.0f

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setScaleY(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setTranslationX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setTranslationY(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->setTranslationX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setPivotX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setPivotY(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->setTranslationY(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setTranslationY(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mClearAnimView:Lcom/android/systemui/taskmanager/CircleAndTickAnimView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/CircleAndTickAnimView;->stopAnimator()V

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-super {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    move/from16 v22, v0

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mToggleButton:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f0201c7

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-eqz v20, :cond_21

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->isEmpty()Z

    move-result v22

    xor-int/lit8 v22, v22, 0x1

    if-eqz v22, :cond_21

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lmiui/util/ScreenshotUtils;->disallowTaskManagerScreenshotMode(Landroid/content/Context;)Z

    move-result v22

    xor-int/lit8 v22, v22, 0x1

    if-eqz v22, :cond_21

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->isForceScreenShotMode()Z

    move-result v22

    xor-int/lit8 v22, v22, 0x1

    if-eqz v22, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mToggleButton:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getFreeMemory()J

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/systemui/taskmanager/TaskManagerView;->mFreeMemoryShow:J

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->refreshUi()V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    move/from16 v22, v0

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/TasksView;->show()V

    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getFastBlurColor()I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mUseBlurBackground:Z

    move/from16 v22, v0

    if-eqz v22, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mBackground:Landroid/view/View;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    if-eqz v22, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    if-eqz v22, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarBackground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFromHome:Z

    move/from16 v22, v0

    if-nez v22, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    move/from16 v22, v0

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_22

    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    move/from16 v23, v0

    const v24, 0xffffff

    and-int v23, v23, v24

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->setBackgroundColor(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    const-string/jumbo v23, "backgroundColor"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    move/from16 v25, v0

    const v26, 0xffffff

    and-int v25, v25, v26

    const/16 v26, 0x0

    aput v25, v24, v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    move/from16 v25, v0

    const/16 v26, 0x1

    aput v25, v24, v26

    invoke-static/range {v22 .. v24}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mArgbEvaluator:Landroid/animation/ArgbEvaluator;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    new-instance v23, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/ObjectAnimator;->start()V

    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->getLightSuit()Lcom/android/systemui/statusbar/phone/NavigationBarView$DrawableSuit;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->switchSuit(Lcom/android/systemui/statusbar/phone/NavigationBarView$DrawableSuit;Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->isForceImmersive()Z

    move-result v22

    if-eqz v22, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenRotation:I

    move/from16 v22, v0

    if-eqz v22, :cond_18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenRotation:I

    move/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_19

    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0b0067

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x10500f8

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v27

    add-int v26, v26, v27

    invoke-virtual/range {v22 .. v26}, Landroid/view/ViewGroup;->setPadding(IIII)V

    :cond_19
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->requestLayout()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/TasksView;->clearAnimation()V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    move/from16 v22, v0

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    const/high16 v23, 0x40100000    # 2.25f

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->setScaleX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    const/high16 v23, 0x40100000    # 2.25f

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->setScaleY(F)V

    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/TasksView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    const/high16 v23, 0x3f800000    # 1.0f

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    const/high16 v23, 0x3f800000    # 1.0f

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    new-instance v23, Lmiui/view/animation/QuarticEaseOutInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/QuarticEaseOutInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    new-instance v23, Lcom/android/systemui/taskmanager/TaskManagerView$11;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView$11;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    const-wide/16 v24, 0x10e

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewPropertyAnimator;->start()V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFromHome:Z

    move/from16 v22, v0

    if-nez v22, :cond_1b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    move/from16 v22, v0

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_23

    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->setCurrentScreen(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewGroup;->setAlpha(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    const/high16 v23, 0x3f800000    # 1.0f

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    new-instance v23, Lmiui/view/animation/QuarticEaseOutInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/QuarticEaseOutInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    const-wide/16 v24, 0x10e

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_1c
    :goto_9
    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->setFocusable(Z)V

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->setFocusableInTouchMode(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->requestFocus()Z

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->unfreeze()V

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mHasPreLoad:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/android/systemui/taskmanager/Utils;->isTalkBackMode(Landroid/content/Context;)Z

    move-result v22

    if-eqz v22, :cond_1e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFirstEnterTalkBack:Z

    move/from16 v22, v0

    if-eqz v22, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v15

    const-string/jumbo v22, "pref_task_manager_is_first_enter_talkback"

    const/16 v23, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-interface {v15, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFirstEnterTalkBack:Z

    :cond_1d
    invoke-virtual {v4}, Lcom/android/systemui/taskmanager/TaskManagerView$TaskAdapter;->getCount()I

    move-result v6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFirstEnterTalkBack:Z

    move/from16 v22, v0

    if-eqz v22, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    const/16 v25, 0x0

    aput-object v24, v23, v25

    const v24, 0x7f110002

    move-object/from16 v0, v22

    move/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v6, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    :goto_a
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFirstEnterTalkBack:Z

    move/from16 v22, v0

    if-eqz v22, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v15

    invoke-interface {v15}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    const-string/jumbo v23, "pref_task_manager_is_first_enter_talkback"

    const/16 v24, 0x0

    invoke-interface/range {v22 .. v24}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFirstEnterTalkBack:Z

    :cond_1e
    :goto_b
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsShow:Z

    if-eqz p1, :cond_34

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTime:J

    :goto_c
    return-void

    :cond_1f
    const/16 v22, 0x4

    goto/16 :goto_5

    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mToggleButton:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const v23, 0x7f0201c3

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mToggleButton:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x4

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    :cond_22
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFirstTaskIsTopRunning:Z

    move/from16 v22, v0

    xor-int/lit8 v22, v22, 0x1

    if-nez v22, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->setBackgroundColor(I)V

    goto/16 :goto_8

    :cond_23
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFirstTaskIsTopRunning:Z

    move/from16 v22, v0

    xor-int/lit8 v22, v22, 0x1

    if-nez v22, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->setCurrentScreen(I)V

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_1c

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0b0063

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotWidth()I

    move-result v23

    mul-int/lit8 v24, v17, 0x2

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    const/high16 v24, 0x40100000    # 2.25f

    mul-float v23, v23, v24

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->setTranslationX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/TasksView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    new-instance v23, Lmiui/view/animation/QuarticEaseOutInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/QuarticEaseOutInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    const-wide/16 v24, 0x10e

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_9

    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    const/16 v25, 0x0

    aput-object v24, v23, v25

    const v24, 0x7f110001

    move-object/from16 v0, v22

    move/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v6, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_a

    :cond_25
    const/4 v8, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mAnimatorsNeedForceEnd:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v8, v0, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mAnimatorsNeedForceEnd:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/animation/Animator;

    invoke-virtual {v5}, Landroid/animation/Animator;->isRunning()Z

    move-result v22

    if-eqz v22, :cond_26

    invoke-virtual {v5}, Landroid/animation/Animator;->end()V

    :cond_26
    add-int/lit8 v8, v8, 0x1

    goto :goto_d

    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mAnimatorsNeedForceEnd:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->clear()V

    if-nez p2, :cond_2b

    const/16 v22, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-super {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/TasksView;->destroyAllScreenshot()V

    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/TasksView;->removeAllScreens()V

    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const v23, 0x7f0d025c

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    if-eqz v22, :cond_2a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    if-eqz v22, :cond_29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->setBackgroundColor(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->getBarTransitions()Lcom/android/systemui/statusbar/phone/BarTransitions;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/BarTransitions;->getBackgroundColor()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    const-string/jumbo v23, "backgroundColor"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    move/from16 v25, v0

    const/16 v26, 0x0

    aput v25, v24, v26

    invoke-static {v12}, Landroid/graphics/Color;->alpha(I)I

    move-result v25

    if-eqz v25, :cond_33

    :goto_f
    const/16 v25, 0x1

    aput v12, v24, v25

    invoke-static/range {v22 .. v24}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mArgbEvaluator:Landroid/animation/ArgbEvaluator;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    new-instance v23, Lmiui/view/animation/SineEaseOutInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/SineEaseOutInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/systemui/taskmanager/TaskManagerView$15;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView$15;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mNavigationBarColorAnimator:Landroid/animation/ObjectAnimator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/ObjectAnimator;->start()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->refreshDisableFlags()V

    :cond_2a
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->sendVisibilityChangedBroadcast(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->showStatusBar(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mToggleButton:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x4

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_b

    :cond_2b
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->freeze()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mOpeningTaskInScreenshot:Z

    move/from16 v22, v0

    if-nez v22, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMainContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    sget-object v23, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    aput v25, v24, v26

    invoke-static/range {v22 .. v24}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mFadeoutAnimator:Landroid/animation/Animator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFadeoutAnimator:Landroid/animation/Animator;

    move-object/from16 v22, v0

    const-wide/16 v24, 0x10e

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFadeoutAnimator:Landroid/animation/Animator;

    move-object/from16 v22, v0

    new-instance v23, Lmiui/view/animation/SineEaseOutInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/SineEaseOutInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFadeoutAnimator:Landroid/animation/Animator;

    move-object/from16 v22, v0

    const-wide/16 v24, 0x1e

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setStartDelay(J)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_2c

    if-eqz p4, :cond_2e

    :cond_2c
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v22

    move-object/from16 v0, v22

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v22, v0

    const/high16 v23, 0x43c80000    # 400.0f

    mul-float v7, v23, v22

    new-instance v22, Landroid/animation/AnimatorSet;

    invoke-direct/range {v22 .. v22}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    const-wide/16 v24, 0xfa

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    new-instance v23, Lmiui/view/animation/CubicEaseInOutInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/CubicEaseInOutInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v24, v0

    sget-object v25, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [F

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput v7, v26, v27

    invoke-static/range {v24 .. v26}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v24

    const/16 v25, 0x0

    aput-object v24, v23, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    move-object/from16 v24, v0

    sget-object v25, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [F

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput v7, v26, v27

    invoke-static/range {v24 .. v26}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v24

    const/16 v25, 0x1

    aput-object v24, v23, v25

    invoke-virtual/range {v22 .. v23}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/AnimatorSet;->start()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFadeoutAnimator:Landroid/animation/Animator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/Animator;->start()V

    :cond_2d
    :goto_10
    new-instance v23, Lcom/android/systemui/taskmanager/TaskManagerView$14;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView$14;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mOpeningTaskInScreenshot:Z

    move/from16 v22, v0

    if-eqz v22, :cond_32

    const/16 v22, 0x0

    :goto_11
    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-wide/from16 v2, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/systemui/taskmanager/TaskManagerView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_e

    :cond_2e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFromHome:Z

    move/from16 v22, v0

    if-nez v22, :cond_2f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFirstTaskIsTopRunning:Z

    move/from16 v22, v0

    xor-int/lit8 v22, v22, 0x1

    if-nez v22, :cond_2f

    if-eqz p3, :cond_30

    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    const/high16 v23, 0x3f800000    # 1.0f

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->setScaleX(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    const/high16 v23, 0x3f800000    # 1.0f

    invoke-virtual/range {v22 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->setScaleY(F)V

    new-instance v22, Landroid/animation/AnimatorSet;

    invoke-direct/range {v22 .. v22}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    const-wide/16 v24, 0xfa

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    new-instance v23, Lmiui/view/animation/SineEaseOutInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/SineEaseOutInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v24, v0

    sget-object v25, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [F

    move-object/from16 v26, v0

    const v27, 0x401e6667    # 2.4750001f

    const/16 v28, 0x0

    aput v27, v26, v28

    invoke-static/range {v24 .. v26}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v24

    const/16 v25, 0x0

    aput-object v24, v23, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v24, v0

    sget-object v25, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [F

    move-object/from16 v26, v0

    const v27, 0x401e6667    # 2.4750001f

    const/16 v28, 0x0

    aput v27, v26, v28

    invoke-static/range {v24 .. v26}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v24

    const/16 v25, 0x1

    aput-object v24, v23, v25

    invoke-virtual/range {v22 .. v23}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitAnimSet:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/AnimatorSet;->start()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFadeoutAnimator:Landroid/animation/Animator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/Animator;->start()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mMemoryAndClearContainer:Landroid/view/ViewGroup;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    const-wide/16 v24, 0x96

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    new-instance v23, Lmiui/view/animation/QuarticEaseOutInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/QuarticEaseOutInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_10

    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TasksView;->mScroller:Landroid/widget/Scroller;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Landroid/widget/Scroller;->forceFinished(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/TasksView;->getCurrentScreenIndex()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_31

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [I

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/android/systemui/taskmanager/TasksView;->getScrollX()I

    move-result v23

    const/16 v24, 0x0

    aput v23, v22, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/android/systemui/taskmanager/TasksView;->getScreenScrollX(I)I

    move-result v23

    const/16 v24, 0x1

    aput v23, v22, v24

    invoke-static/range {v22 .. v22}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitScrollAnimator:Landroid/animation/ValueAnimator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitScrollAnimator:Landroid/animation/ValueAnimator;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/systemui/taskmanager/TaskManagerView$12;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView$12;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitScrollAnimator:Landroid/animation/ValueAnimator;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/systemui/taskmanager/TaskManagerView$13;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView$13;-><init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitScrollAnimator:Landroid/animation/ValueAnimator;

    move-object/from16 v22, v0

    new-instance v23, Lmiui/view/animation/CubicEaseInInterpolator;

    invoke-direct/range {v23 .. v23}, Lmiui/view/animation/CubicEaseInInterpolator;-><init>()V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitScrollAnimator:Landroid/animation/ValueAnimator;

    move-object/from16 v22, v0

    const-wide/16 v24, 0x15e

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mExitScrollAnimator:Landroid/animation/ValueAnimator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :cond_31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/systemui/taskmanager/TasksView;->getScreenCount()I

    move-result v22

    if-lez v22, :cond_2d

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->showFirstTask()V

    return-void

    :cond_32
    const/16 v22, 0x12c

    goto/16 :goto_11

    :cond_33
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mFastBlurColor:I

    move/from16 v25, v0

    const v26, 0xffffff

    and-int v12, v25, v26

    goto/16 :goto_f

    :cond_34
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mShowTime:J

    move-wide/from16 v24, v0

    sub-long v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mScreenshotMode:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mThemeId:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mThemeName:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-static/range {v22 .. v25}, Lcom/android/systemui/AnalyticsHelper;->trackTaskManagerShow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c
.end method

.method startApplicationDetailsActivity(Ljava/lang/String;I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "com.android.settings"

    const-string/jumbo v2, "com.android.settings.applications.InstalledAppDetailsTop"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "package"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-static {p2}, Lmiui/securityspace/XSpaceUserHandle;->isXSpaceUserId(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "is_xspace_app"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    invoke-direct {p0, v4, v3, v3}, Lcom/android/systemui/taskmanager/TaskManagerView;->show(ZZZ)V

    return-void

    :cond_0
    const-string/jumbo v1, "is_xspace_app"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "miui.intent.action.APP_MANAGER_APPLICATION_DETAIL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "package_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.extra.USER_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x10800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public unfreeze()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView;->mIsFreeze:Z

    return-void
.end method
