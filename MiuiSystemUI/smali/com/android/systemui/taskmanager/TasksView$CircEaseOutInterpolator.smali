.class Lcom/android/systemui/taskmanager/TasksView$CircEaseOutInterpolator;
.super Ljava/lang/Object;
.source "TasksView.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/taskmanager/TasksView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CircEaseOutInterpolator"
.end annotation


# static fields
.field public static final Instance:Lcom/android/systemui/taskmanager/TasksView$CircEaseOutInterpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/systemui/taskmanager/TasksView$CircEaseOutInterpolator;

    invoke-direct {v0}, Lcom/android/systemui/taskmanager/TasksView$CircEaseOutInterpolator;-><init>()V

    sput-object v0, Lcom/android/systemui/taskmanager/TasksView$CircEaseOutInterpolator;->Instance:Lcom/android/systemui/taskmanager/TasksView$CircEaseOutInterpolator;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 2

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr p1, v1

    mul-float v0, p1, p1

    sub-float v0, v1, v0

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method
