.class public Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;
.super Ljava/lang/Object;
.source "ScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/taskmanager/ScreenView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GestureVelocityTracker"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;
    }
.end annotation


# instance fields
.field private final DEFAULT_VERTICAL_GESTURE_CONFIRM_DIST:I

.field private mCounter:I

.field private mPointerId:I

.field private mTx:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

.field private mTy:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mVerticalGestureConfirmed:Z

.field final synthetic this$0:Lcom/android/systemui/taskmanager/ScreenView;


# direct methods
.method public constructor <init>(Lcom/android/systemui/taskmanager/ScreenView;)V
    .locals 2

    const/4 v1, -0x1

    iput-object p1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;-><init>(Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTx:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    new-instance v0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    invoke-direct {v0, p0}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;-><init>(Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;)V

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTy:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mPointerId:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mCounter:I

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->DEFAULT_VERTICAL_GESTURE_CONFIRM_DIST:I

    return-void
.end method

.method private reset()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTx:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->reset()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTy:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->reset()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mPointerId:I

    iput v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mCounter:I

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVerticalGestureConfirmed:Z

    return-void
.end method

.method private trackPoint(FLcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;)V
    .locals 3

    const/high16 v2, 0x40400000    # 3.0f

    const/4 v1, 0x0

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->start:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iput p1, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->start:F

    return-void

    :cond_0
    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iput p1, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    return-void

    :cond_1
    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    iget v1, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->start:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_3

    :goto_0
    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->start:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    iput v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    :cond_2
    :goto_1
    iput p1, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    return-void

    :cond_3
    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    iget v1, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->start:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    goto :goto_0

    :cond_4
    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    iget v1, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    iget v1, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_5

    :goto_2
    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    iput v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->start:F

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    iput v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    goto :goto_1

    :cond_5
    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    iget v1, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    goto :goto_2
.end method


# virtual methods
.method public addMovement(Landroid/view/MotionEvent;)V
    .locals 6

    const/4 v5, -0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v0, v4, 0xff

    const/4 v4, 0x1

    if-eq v0, v4, :cond_0

    const/4 v4, 0x3

    if-ne v0, v4, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mCounter:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mCounter:I

    iget-object v4, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v4, :cond_2

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v4

    iput-object v4, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_2
    iget-object v4, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mPointerId:I

    if-eq v4, v5, :cond_3

    iget v4, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mPointerId:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    if-eq v3, v5, :cond_4

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    :cond_3
    :goto_0
    iget-object v4, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTx:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    invoke-direct {p0, v1, v4}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->trackPoint(FLcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;)V

    iget-object v4, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTy:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    invoke-direct {p0, v2, v4}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->trackPoint(FLcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;)V

    return-void

    :cond_4
    iput v5, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mPointerId:I

    goto :goto_0
.end method

.method public getCounter()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mCounter:I

    return v0
.end method

.method public getFlingDirection(Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;F)I
    .locals 5

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v4, 0x3

    const/high16 v2, 0x43960000    # 300.0f

    cmpl-float v2, p2, v2

    if-lez v2, :cond_6

    iget v2, p1, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    iget v2, p1, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    iget v3, p1, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->start:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget v2, p1, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    iget v3, p1, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    iget-boolean v0, v0, Lcom/android/systemui/taskmanager/ScreenView;->mMultiScreenScroll:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-static {v0}, Lcom/android/systemui/taskmanager/ScreenView;->-get2(Lcom/android/systemui/taskmanager/ScreenView;)I

    move-result v0

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/ScreenView;->getCurrentScreen()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    if-ge v0, v2, :cond_2

    return v4

    :cond_2
    return v1

    :cond_3
    iget v1, p1, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    iget v2, p1, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->fold:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_5

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    iget-boolean v1, v1, Lcom/android/systemui/taskmanager/ScreenView;->mMultiScreenScroll:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/ScreenView;->-get2(Lcom/android/systemui/taskmanager/ScreenView;)I

    move-result v1

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-virtual {v2}, Lcom/android/systemui/taskmanager/ScreenView;->getCurrentScreen()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    if-le v1, v2, :cond_4

    return v4

    :cond_4
    return v0

    :cond_5
    return v4

    :cond_6
    const/4 v0, 0x4

    return v0
.end method

.method public getVerticalGesture()I
    .locals 4

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVerticalGestureConfirmed:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->getCounter()I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    :cond_0
    return v3

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/ScreenView;->-get0(Lcom/android/systemui/taskmanager/ScreenView;)I

    move-result v1

    const/16 v2, 0x3e8

    invoke-virtual {p0, v2, v1, v3}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->getYVelocity(III)F

    move-result v0

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->this$0:Lcom/android/systemui/taskmanager/ScreenView;

    invoke-static {v1}, Lcom/android/systemui/taskmanager/ScreenView;->-get0(Lcom/android/systemui/taskmanager/ScreenView;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_3

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTy:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    iget v1, v1, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->start:F

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTy:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    iget v2, v2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->DEFAULT_VERTICAL_GESTURE_CONFIRM_DIST:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVerticalGestureConfirmed:Z

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTy:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    iget v1, v1, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->start:F

    iget-object v2, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTy:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    iget v2, v2, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;->prev:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    const/16 v1, 0xa

    :goto_0
    return v1

    :cond_2
    const/16 v1, 0xb

    goto :goto_0

    :cond_3
    return v3
.end method

.method public getXFlingDirection(F)I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mTx:Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;

    invoke-virtual {p0, v0, p1}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->getFlingDirection(Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker$Tracker;F)I

    move-result v0

    return v0
.end method

.method public getXVelocity(III)F
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    int-to-float v1, p2

    invoke-virtual {v0, p1, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    return v0
.end method

.method public getYVelocity(III)F
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    int-to-float v1, p2

    invoke-virtual {v0, p1, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p3}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v0

    return v0
.end method

.method public init(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    :goto_0
    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->reset()V

    iput p1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mPointerId:I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0
.end method

.method public recycle()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v1, p0, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/taskmanager/ScreenView$GestureVelocityTracker;->reset()V

    return-void
.end method
