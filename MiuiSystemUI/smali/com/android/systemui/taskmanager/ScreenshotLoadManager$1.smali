.class Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;
.super Ljava/lang/Object;
.source "ScreenshotLoadManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->startLoad(Lcom/android/systemui/taskmanager/TaskInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

.field final synthetic val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/ScreenshotLoadManager;Lcom/android/systemui/taskmanager/TaskInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;->this$0:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    iput-object p2, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;->this$0:Lcom/android/systemui/taskmanager/ScreenshotLoadManager;

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-static {v0, v1}, Lcom/android/systemui/taskmanager/ScreenshotLoadManager;->-wrap0(Lcom/android/systemui/taskmanager/ScreenshotLoadManager;Lcom/android/systemui/taskmanager/TaskInfo;)V

    iget-object v1, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/android/systemui/taskmanager/TaskInfo;->mIsLoadingScreenshot:Z

    iget-object v0, p0, Lcom/android/systemui/taskmanager/ScreenshotLoadManager$1;->val$taskInfo:Lcom/android/systemui/taskmanager/TaskInfo;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskInfo;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
