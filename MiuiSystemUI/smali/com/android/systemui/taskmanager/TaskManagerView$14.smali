.class Lcom/android/systemui/taskmanager/TaskManagerView$14;
.super Ljava/lang/Object;
.source "TaskManagerView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/taskmanager/TaskManagerView;->show(ZZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/taskmanager/TaskManagerView;


# direct methods
.method constructor <init>(Lcom/android/systemui/taskmanager/TaskManagerView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/taskmanager/TaskManagerView$14;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$14;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->-wrap0(Lcom/android/systemui/taskmanager/TaskManagerView;I)V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$14;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TaskManagerView;->getScreenshotMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$14;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TasksView;->destroyAllScreenshot()V

    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$14;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/systemui/taskmanager/TaskManagerView;->-set1(Lcom/android/systemui/taskmanager/TaskManagerView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/taskmanager/TaskManagerView$14;->this$0:Lcom/android/systemui/taskmanager/TaskManagerView;

    iget-object v0, v0, Lcom/android/systemui/taskmanager/TaskManagerView;->mTasksView:Lcom/android/systemui/taskmanager/TasksView;

    invoke-virtual {v0}, Lcom/android/systemui/taskmanager/TasksView;->removeAllScreens()V

    return-void
.end method
