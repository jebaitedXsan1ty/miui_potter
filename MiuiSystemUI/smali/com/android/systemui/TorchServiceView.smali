.class public Lcom/android/systemui/TorchServiceView;
.super Landroid/widget/FrameLayout;
.source "TorchServiceView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/TorchServiceView$1;,
        Lcom/android/systemui/TorchServiceView$2;,
        Lcom/android/systemui/TorchServiceView$3;,
        Lcom/android/systemui/TorchServiceView$4;,
        Lcom/android/systemui/TorchServiceView$5;
    }
.end annotation


# static fields
.field public static final FLASH_DEVICES:[Ljava/lang/String;


# instance fields
.field private mBgHandler:Landroid/os/Handler;

.field private mCamera:Landroid/hardware/Camera;

.field private mCameraWakeLock:Landroid/os/PowerManager$WakeLock;

.field private final mCloseRunnable:Ljava/lang/Runnable;

.field private mDelayOpen:Ljava/lang/Runnable;

.field private mFlashDevice:Ljava/lang/String;

.field private mFlashMode:I

.field private final mFlashRunable:Ljava/lang/Runnable;

.field private mFlashing:Z

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mLastCallState:I

.field private final mOpenRunnable:Ljava/lang/Runnable;

.field private mParams:Landroid/hardware/Camera$Parameters;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mStatusDetecting:Ljava/lang/Runnable;

.field private mSurfaceView:Landroid/view/SurfaceView;

.field private mValueOn:I

.field private telephonyManager:Lmiui/telephony/TelephonyManager;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/TorchServiceView;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mCloseRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/TorchServiceView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/TorchServiceView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mFlashDevice:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/TorchServiceView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/TorchServiceView;->mFlashMode:I

    return v0
.end method

.method static synthetic -get4(Lcom/android/systemui/TorchServiceView;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mFlashRunable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/systemui/TorchServiceView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/TorchServiceView;->mLastCallState:I

    return v0
.end method

.method static synthetic -get6(Lcom/android/systemui/TorchServiceView;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mOpenRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/systemui/TorchServiceView;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mStatusDetecting:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get8(Lcom/android/systemui/TorchServiceView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/TorchServiceView;->mValueOn:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/systemui/TorchServiceView;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/TorchServiceView;->mLastCallState:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/TorchServiceView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/TorchServiceView;->flashWhenRing(Z)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/TorchServiceView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/TorchServiceView;->setFlashModeInternal(I)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/systemui/TorchServiceView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/TorchServiceView;->setFlashMode(I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "/sys/class/leds/flashlight/brightness"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "/sys/class/leds/spotlight/brightness"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/TorchServiceView;->FLASH_DEVICES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/TorchServiceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/systemui/TorchServiceView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/TorchServiceView$1;-><init>(Lcom/android/systemui/TorchServiceView;)V

    iput-object v0, p0, Lcom/android/systemui/TorchServiceView;->mFlashRunable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/systemui/TorchServiceView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/TorchServiceView$2;-><init>(Lcom/android/systemui/TorchServiceView;)V

    iput-object v0, p0, Lcom/android/systemui/TorchServiceView;->mOpenRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/systemui/TorchServiceView$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/TorchServiceView$3;-><init>(Lcom/android/systemui/TorchServiceView;)V

    iput-object v0, p0, Lcom/android/systemui/TorchServiceView;->mCloseRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/systemui/TorchServiceView$4;

    invoke-direct {v0, p0}, Lcom/android/systemui/TorchServiceView$4;-><init>(Lcom/android/systemui/TorchServiceView;)V

    iput-object v0, p0, Lcom/android/systemui/TorchServiceView;->mDelayOpen:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/systemui/TorchServiceView$5;

    invoke-direct {v0, p0}, Lcom/android/systemui/TorchServiceView$5;-><init>(Lcom/android/systemui/TorchServiceView;)V

    iput-object v0, p0, Lcom/android/systemui/TorchServiceView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0}, Lcom/android/systemui/TorchServiceView;->fetchFlashDeviceConfig()V

    return-void
.end method

.method private fetchFlashDeviceConfig()V
    .locals 5

    iget-object v3, p0, Lcom/android/systemui/TorchServiceView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-nez v3, :cond_0

    const v3, 0x7f100007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, p0, Lcom/android/systemui/TorchServiceView;->mValueOn:I

    const v3, 0x7f0d0162

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/systemui/TorchServiceView;->mFlashDevice:Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/android/systemui/TorchServiceView;->mFlashDevice:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/android/systemui/TorchServiceView;->FLASH_DEVICES:[Ljava/lang/String;

    array-length v3, v3

    if-ne v0, v3, :cond_1

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/systemui/TorchServiceView;->mFlashDevice:Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    sget-object v3, Lcom/android/systemui/TorchServiceView;->FLASH_DEVICES:[Ljava/lang/String;

    add-int/lit8 v1, v0, 0x1

    aget-object v3, v3, v0

    iput-object v3, p0, Lcom/android/systemui/TorchServiceView;->mFlashDevice:Ljava/lang/String;

    move v0, v1

    goto :goto_0
.end method

.method private flashWhenRing(Z)V
    .locals 4

    const-string/jumbo v0, "TorchServiceView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "flashWhenRing: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/TorchServiceView;->mFlashing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/systemui/TorchServiceView;->mFlashing:Z

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mOpenRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/systemui/TorchServiceView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mCloseRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/systemui/TorchServiceView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mFlashRunable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/systemui/TorchServiceView;->removeCallbacks(Ljava/lang/Runnable;)Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mFlashRunable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/systemui/TorchServiceView;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    iput-boolean p1, p0, Lcom/android/systemui/TorchServiceView;->mFlashing:Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mDelayOpen:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/systemui/TorchServiceView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget v0, p0, Lcom/android/systemui/TorchServiceView;->mFlashMode:I

    invoke-direct {p0, v0}, Lcom/android/systemui/TorchServiceView;->setFlashMode(I)V

    goto :goto_0
.end method

.method private declared-synchronized setFlashMode(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/systemui/TorchServiceView;->setFlashModeInternal(I)V

    iput p1, p0, Lcom/android/systemui/TorchServiceView;->mFlashMode:I

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "torch_state"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized setFlashModeInternal(I)V
    .locals 10

    const/4 v7, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lmiui/os/Build;->hasCameraFlash(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-nez v6, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mFlashDevice:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    if-nez v6, :cond_1

    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v6

    iput-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v6, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    if-nez p1, :cond_4

    :try_start_2
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mDelayOpen:Ljava/lang/Runnable;

    invoke-virtual {p0, v6}, Lcom/android/systemui/TorchServiceView;->removeCallbacks(Ljava/lang/Runnable;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v6

    iput-object v6, p0, Lcom/android/systemui/TorchServiceView;->mParams:Landroid/hardware/Camera$Parameters;

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mParams:Landroid/hardware/Camera$Parameters;

    const-string/jumbo v7, "off"

    invoke-virtual {v6, v7}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    iget-object v7, p0, Lcom/android/systemui/TorchServiceView;->mParams:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v6, v7}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->release()V

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/TorchServiceView;->invalidate()V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCameraWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCameraWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_2
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mSurfaceView:Landroid/view/SurfaceView;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {p0, v6}, Lcom/android/systemui/TorchServiceView;->removeView(Landroid/view/View;)V

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/systemui/TorchServiceView;->mSurfaceView:Landroid/view/SurfaceView;

    :cond_3
    const/16 v6, 0x8

    invoke-virtual {p0, v6}, Lcom/android/systemui/TorchServiceView;->setVisibility(I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->release()V

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_7
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :catchall_1
    move-exception v6

    :try_start_8
    iget-object v7, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v7}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v7, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v7}, Landroid/hardware/Camera;->release()V

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    throw v6

    :cond_4
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mSurfaceView:Landroid/view/SurfaceView;

    if-nez v6, :cond_5

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/android/systemui/TorchServiceView;->setVisibility(I)V

    new-instance v6, Landroid/view/SurfaceView;

    iget-object v7, p0, Lcom/android/systemui/TorchServiceView;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/android/systemui/TorchServiceView;->mSurfaceView:Landroid/view/SurfaceView;

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mSurfaceView:Landroid/view/SurfaceView;

    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-virtual {p0, v6, v7, v8}, Lcom/android/systemui/TorchServiceView;->addView(Landroid/view/View;II)V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mDelayOpen:Ljava/lang/Runnable;

    invoke-virtual {p0, v6}, Lcom/android/systemui/TorchServiceView;->post(Ljava/lang/Runnable;)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    monitor-exit p0

    return-void

    :cond_5
    :try_start_9
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v6

    iput-object v6, p0, Lcom/android/systemui/TorchServiceView;->mParams:Landroid/hardware/Camera$Parameters;

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mParams:Landroid/hardware/Camera$Parameters;

    const-string/jumbo v7, "torch"

    invoke-virtual {v6, v7}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mParams:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/Camera$Size;

    iget v6, v4, Landroid/hardware/Camera$Size;->height:I

    iget v7, v4, Landroid/hardware/Camera$Size;->width:I

    mul-int/2addr v6, v7

    iget v7, v1, Landroid/hardware/Camera$Size;->height:I

    iget v8, v1, Landroid/hardware/Camera$Size;->width:I

    mul-int/2addr v7, v8

    if-ge v6, v7, :cond_6

    move-object v1, v4

    goto :goto_2

    :cond_7
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mParams:Landroid/hardware/Camera$Parameters;

    iget v7, v1, Landroid/hardware/Camera$Size;->width:I

    iget v8, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v6, v7, v8}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    iget-object v7, p0, Lcom/android/systemui/TorchServiceView;->mParams:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v6, v7}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    iget-object v7, p0, Lcom/android/systemui/TorchServiceView;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v7}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->startPreview()V

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mContext:Landroid/content/Context;

    const-string/jumbo v7, "power"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    const-string/jumbo v6, "TorchServiceView"

    const/4 v7, 0x1

    invoke-virtual {v2, v7, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v6

    iput-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCameraWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mCameraWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto/16 :goto_1

    :cond_8
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mStatusDetecting:Ljava/lang/Runnable;

    if-nez v6, :cond_9

    new-instance v6, Lcom/android/systemui/TorchServiceView$7;

    invoke-direct {v6, p0}, Lcom/android/systemui/TorchServiceView$7;-><init>(Lcom/android/systemui/TorchServiceView;)V

    iput-object v6, p0, Lcom/android/systemui/TorchServiceView;->mStatusDetecting:Ljava/lang/Runnable;

    :cond_9
    if-ne p1, v7, :cond_a

    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mStatusDetecting:Ljava/lang/Runnable;

    const-wide/16 v8, 0x3e8

    invoke-virtual {p0, v6, v8, v9}, Lcom/android/systemui/TorchServiceView;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_3
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mBgHandler:Landroid/os/Handler;

    new-instance v7, Lcom/android/systemui/TorchServiceView$8;

    invoke-direct {v7, p0, p1}, Lcom/android/systemui/TorchServiceView$8;-><init>(Lcom/android/systemui/TorchServiceView;I)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    :cond_a
    iget-object v6, p0, Lcom/android/systemui/TorchServiceView;->mStatusDetecting:Ljava/lang/Runnable;

    invoke-virtual {p0, v6}, Lcom/android/systemui/TorchServiceView;->removeCallbacks(Ljava/lang/Runnable;)Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 6

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "torch"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/systemui/TorchServiceView;->mHandlerThread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/systemui/TorchServiceView;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/systemui/TorchServiceView;->mBgHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mFlashDevice:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/systemui/TorchServiceView;->setFlashMode(I)V

    :cond_0
    new-instance v3, Landroid/content/IntentFilter;

    const-string/jumbo v0, "miui.intent.action.TOGGLE_TORCH"

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/16 v0, -0x3e8

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->setPriority(I)V

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/TorchServiceView;->mReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v0, Lcom/android/systemui/TorchServiceView$6;

    invoke-direct {v0, p0}, Lcom/android/systemui/TorchServiceView$6;-><init>(Lcom/android/systemui/TorchServiceView;)V

    iput-object v0, p0, Lcom/android/systemui/TorchServiceView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/TorchServiceView;->telephonyManager:Lmiui/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->telephonyManager:Lmiui/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/systemui/TorchServiceView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Lmiui/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/TorchServiceView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-direct {p0, v2}, Lcom/android/systemui/TorchServiceView;->flashWhenRing(Z)V

    invoke-direct {p0, v2}, Lcom/android/systemui/TorchServiceView;->setFlashMode(I)V

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->telephonyManager:Lmiui/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/systemui/TorchServiceView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v2}, Lmiui/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lcom/android/systemui/TorchServiceView;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method
