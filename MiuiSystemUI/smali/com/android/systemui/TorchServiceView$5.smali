.class Lcom/android/systemui/TorchServiceView$5;
.super Landroid/content/BroadcastReceiver;
.source "TorchServiceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/TorchServiceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/TorchServiceView;


# direct methods
.method constructor <init>(Lcom/android/systemui/TorchServiceView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/TorchServiceView$5;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    const-string/jumbo v2, "miui.intent.extra.IS_TOGGLE"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/systemui/TorchServiceView$5;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-static {v2}, Lcom/android/systemui/TorchServiceView;->-get3(Lcom/android/systemui/TorchServiceView;)I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string/jumbo v2, "TorchServiceView"

    const-string/jumbo v3, "onReceive mode=%d isToggle=%b"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/systemui/TorchServiceView$5;->this$0:Lcom/android/systemui/TorchServiceView;

    invoke-static {v2, v1}, Lcom/android/systemui/TorchServiceView;->-wrap2(Lcom/android/systemui/TorchServiceView;I)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "miui.intent.extra.IS_ENABLE"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
