.class public Lcom/android/systemui/OldmanHelper;
.super Ljava/lang/Object;
.source "OldmanHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;
    }
.end annotation


# static fields
.field private static final _allMobileMarkShownModes:[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

.field private static final _defMobileMarkShownMode:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->SLOT0_AUTO_HIDDEN:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    sput-object v0, Lcom/android/systemui/OldmanHelper;->_defMobileMarkShownMode:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    invoke-static {}, Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;->values()[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/OldmanHelper;->_allMobileMarkShownModes:[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMobileMarkShownMode(Landroid/content/Context;)Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "elder.systemui.mobile_mark_shown_mode"

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lcom/android/systemui/OldmanHelper;->_allMobileMarkShownModes:[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v1, Lcom/android/systemui/OldmanHelper;->_allMobileMarkShownModes:[Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    aget-object v1, v1, v0

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/android/systemui/OldmanHelper;->_defMobileMarkShownMode:Lcom/android/systemui/OldmanHelper$MobileMarkShownMode;

    goto :goto_0
.end method

.method public static isAllowStatusExpand(Landroid/content/Context;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-static {}, Lcom/android/systemui/OldmanHelper;->isOldmanMode()Z

    move-result v2

    if-nez v2, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "elder.systemui.allow_status_expand"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static isOldmanMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
