.class public Lcom/android/systemui/volume/VolumePanel;
.super Landroid/os/Handler;
.source "VolumePanel.java"


# instance fields
.field private final mVolumeController:Landroid/media/IVolumeController;

.field private final mVolumeDialog:Lmiui/view/VolumeDialog;

.field private final mVolumePanelDelegate:Lmiui/view/VolumeDialog$VolumePanelDelegate;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/volume/VolumePanel;)Landroid/media/IVolumeController;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumeController:Landroid/media/IVolumeController;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/media/IVolumeController;)V
    .locals 2

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Lcom/android/systemui/volume/VolumePanel$1;

    invoke-direct {v0, p0, p1}, Lcom/android/systemui/volume/VolumePanel$1;-><init>(Lcom/android/systemui/volume/VolumePanel;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumePanelDelegate:Lmiui/view/VolumeDialog$VolumePanelDelegate;

    new-instance v0, Lmiui/view/VolumeDialog;

    iget-object v1, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumePanelDelegate:Lmiui/view/VolumeDialog$VolumePanelDelegate;

    invoke-direct {v0, p1, v1}, Lmiui/view/VolumeDialog;-><init>(Landroid/content/Context;Lmiui/view/VolumeDialog$VolumePanelDelegate;)V

    iput-object v0, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumeDialog:Lmiui/view/VolumeDialog;

    iput-object p2, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumeController:Landroid/media/IVolumeController;

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumeDialog:Lmiui/view/VolumeDialog;

    invoke-virtual {v0}, Lmiui/view/VolumeDialog;->dismiss()V

    return-void
.end method

.method public postDismiss(J)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumeDialog:Lmiui/view/VolumeDialog;

    invoke-virtual {v0, p1, p2}, Lmiui/view/VolumeDialog;->dismiss(J)V

    return-void
.end method

.method public postDisplaySafeVolumeWarning(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumeDialog:Lmiui/view/VolumeDialog;

    invoke-virtual {v0, p1}, Lmiui/view/VolumeDialog;->showSafeWarningDialog(I)V

    return-void
.end method

.method public postLayoutDirection(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumeDialog:Lmiui/view/VolumeDialog;

    invoke-virtual {v0, p1}, Lmiui/view/VolumeDialog;->updateLayoutDirection(I)V

    return-void
.end method

.method public postMasterMuteChanged(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumeDialog:Lmiui/view/VolumeDialog;

    invoke-virtual {v0, p1}, Lmiui/view/VolumeDialog;->masterMuteChanged(I)V

    return-void
.end method

.method public postRemoteSliderVisibility(Z)V
    .locals 0

    return-void
.end method

.method public postRemoteVolumeChanged(Landroid/media/session/MediaController;I)V
    .locals 0

    return-void
.end method

.method public postVolumeChanged(II)V
    .locals 2

    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/systemui/volume/VolumePanel;->mVolumeDialog:Lmiui/view/VolumeDialog;

    invoke-virtual {v1, p1, p2}, Lmiui/view/VolumeDialog;->show(II)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
