.class Lcom/android/systemui/SwipeSinglePageHelper$3;
.super Ljava/lang/Object;
.source "SwipeSinglePageHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/SwipeSinglePageHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/SwipeSinglePageHelper;


# direct methods
.method constructor <init>(Lcom/android/systemui/SwipeSinglePageHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/SwipeSinglePageHelper$3;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper$3;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    invoke-static {v0}, Lcom/android/systemui/SwipeSinglePageHelper;->-get2(Lcom/android/systemui/SwipeSinglePageHelper;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper$3;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    invoke-static {v0}, Lcom/android/systemui/SwipeSinglePageHelper;->-get4(Lcom/android/systemui/SwipeSinglePageHelper;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper$3;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/systemui/SwipeSinglePageHelper;->-set0(Lcom/android/systemui/SwipeSinglePageHelper;Z)Z

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper$3;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    invoke-static {v0}, Lcom/android/systemui/SwipeSinglePageHelper;->-get2(Lcom/android/systemui/SwipeSinglePageHelper;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper$3;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    invoke-static {v0}, Lcom/android/systemui/SwipeSinglePageHelper;->-get3(Lcom/android/systemui/SwipeSinglePageHelper;)Landroid/view/View$OnLongClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/SwipeSinglePageHelper$3;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    invoke-static {v1}, Lcom/android/systemui/SwipeSinglePageHelper;->-get2(Lcom/android/systemui/SwipeSinglePageHelper;)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    :cond_0
    return-void
.end method
