.class public Lcom/android/systemui/statusbar/InCallNotificationView;
.super Landroid/widget/LinearLayout;
.source "InCallNotificationView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/InCallNotificationView$1;,
        Lcom/android/systemui/statusbar/InCallNotificationView$2;
    }
.end annotation


# instance fields
.field private mAnswerIcon:Landroid/widget/ImageView;

.field private mCallerInfo:Landroid/widget/TextView;

.field private mCallerName:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mEndCallIcon:Landroid/widget/ImageView;

.field private mHasAnswer:Z

.field private mIsVideoCall:Z

.field private mMiuiVoipManager:Lcom/miui/voiptalk/service/MiuiVoipManager;

.field private mPhoneNumber:Ljava/lang/String;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field public mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mSubId:I

.field private mTelephonyManager:Lmiui/telephony/TelephonyManager;

.field private mVoipPhoneStateReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/InCallNotificationView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/InCallNotificationView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mHasAnswer:Z

    return v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/InCallNotificationView;)Lcom/miui/voiptalk/service/MiuiVoipManager;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mMiuiVoipManager:Lcom/miui/voiptalk/service/MiuiVoipManager;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/statusbar/InCallNotificationView;)Lmiui/telephony/TelephonyManager;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/InCallNotificationView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mHasAnswer:Z

    return p1
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/systemui/statusbar/InCallNotificationView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/InCallNotificationView$1;-><init>(Lcom/android/systemui/statusbar/InCallNotificationView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/android/systemui/statusbar/InCallNotificationView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/InCallNotificationView$2;-><init>(Lcom/android/systemui/statusbar/InCallNotificationView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mVoipPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/InCallNotificationView;->setClipChildren(Z)V

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/InCallNotificationView;->setClipToPadding(Z)V

    iput-object p1, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mContext:Landroid/content/Context;

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/voiptalk/service/MiuiVoipManager;->getInstance(Landroid/content/Context;)Lcom/miui/voiptalk/service/MiuiVoipManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mMiuiVoipManager:Lcom/miui/voiptalk/service/MiuiVoipManager;

    return-void
.end method

.method private updateAnswerIcon()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mIsVideoCall:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mAnswerIcon:Landroid/widget/ImageView;

    const v1, 0x7f020314

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mAnswerIcon:Landroid/widget/ImageView;

    const v1, 0x7f02000a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mAnswerIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mMiuiVoipManager:Lcom/miui/voiptalk/service/MiuiVoipManager;

    invoke-virtual {v0}, Lcom/miui/voiptalk/service/MiuiVoipManager;->isVideoCall()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f020316

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    const v0, 0x7f020315

    goto :goto_1
.end method


# virtual methods
.method public hide()V
    .locals 3

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/InCallNotificationView;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mHasAnswer:Z

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/InCallNotificationView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v2}, Lmiui/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mVoipPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0f00ba

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/InCallNotificationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mCallerName:Landroid/widget/TextView;

    const v0, 0x7f0f00bb

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/InCallNotificationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mCallerInfo:Landroid/widget/TextView;

    const v0, 0x7f0f00b9

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/InCallNotificationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mEndCallIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f00bc

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/InCallNotificationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mAnswerIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mEndCallIcon:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/systemui/statusbar/InCallNotificationView$3;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/InCallNotificationView$3;-><init>(Lcom/android/systemui/statusbar/InCallNotificationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mAnswerIcon:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/systemui/statusbar/InCallNotificationView$4;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/InCallNotificationView$4;-><init>(Lcom/android/systemui/statusbar/InCallNotificationView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/android/systemui/statusbar/InCallNotificationView$5;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/InCallNotificationView$5;-><init>(Lcom/android/systemui/statusbar/InCallNotificationView;)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/InCallNotificationView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public show()V
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/InCallNotificationView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/InCallNotificationView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/InCallNotificationView;->updateAnswerIcon()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Lmiui/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mVoipPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    new-instance v3, Landroid/content/IntentFilter;

    const-string/jumbo v5, "com.miui.voip.action.CALL_STATE_CHANGED"

    invoke-direct {v3, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method public updateInfo(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    if-eqz p1, :cond_0

    const v2, 0x1020016

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f0f0000

    invoke-static {v2, v3}, Lmiui/util/ResourceMapper;->resolveReference(Landroid/content/res/Resources;I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mCallerName:Landroid/widget/TextView;

    if-nez v1, :cond_2

    const-string/jumbo v2, ""

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mCallerInfo:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string/jumbo v2, ""

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz p2, :cond_1

    const-string/jumbo v2, "phoneNumber"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mPhoneNumber:Ljava/lang/String;

    const-string/jumbo v2, "subId"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mSubId:I

    const-string/jumbo v2, "isVideoCall"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/InCallNotificationView;->mIsVideoCall:Z

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
