.class public Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;
.super Ljava/lang/Object;
.source "BrightnessMirrorController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$1;,
        Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;,
        Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;
    }
.end annotation


# static fields
.field public static final SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z


# instance fields
.field public TRANSITION_DURATION_IN:J

.field public TRANSITION_DURATION_OUT:J

.field private mBrightnessFileObserver:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

.field private mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

.field private mBrightnessModeObserver:Landroid/database/ContentObserver;

.field private mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

.field private mCurrentUserId:I

.field private mExpandView:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private final mInt2Cache:[I

.field private mManualScreenBrightness:I

.field private mOriginal:Landroid/view/View;

.field private mPowerManager:Landroid/os/IPowerManager;

.field private mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mStatusBarView:Landroid/view/View;

.field private mToggleManager:Lmiui/app/ToggleManager;

.field showMirrorRunnable:Ljava/lang/Runnable;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mCurrentUserId:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mExpandView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mManualScreenBrightness:I

    return v0
.end method

.method static synthetic -get6(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lcom/android/systemui/statusbar/phone/PhoneStatusBar;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mStatusBarView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get8(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)Lmiui/app/ToggleManager;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mToggleManager:Lmiui/app/ToggleManager;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mManualScreenBrightness:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->inAnimation(Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;Landroid/view/ViewPropertyAnimator;J)Landroid/view/ViewPropertyAnimator;
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->inAnimation(Landroid/view/ViewPropertyAnimator;J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap2(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->outAnimation(Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lmiui/os/DeviceFeature;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    sput-boolean v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V
    .locals 2

    const-wide/16 v0, 0xc8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->TRANSITION_DURATION_OUT:J

    iput-wide v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->TRANSITION_DURATION_IN:J

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mInt2Cache:[I

    const/16 v0, 0x80

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mManualScreenBrightness:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mCurrentUserId:I

    new-instance v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$1;-><init>(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessModeObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$2;-><init>(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->showMirrorRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private getBrightnessFile()Ljava/lang/String;
    .locals 5

    const-string/jumbo v0, "/sys/class/leds/lcd-backlight/brightness"

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v3, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x11090019

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    new-instance v3, Ljava/io/File;

    aget-object v4, v1, v2

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    aget-object v0, v1, v2

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private inAnimation(Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;
    .locals 2

    iget-wide v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->TRANSITION_DURATION_IN:J

    invoke-direct {p0, p1, v0, v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->inAnimation(Landroid/view/ViewPropertyAnimator;J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method private inAnimation(Landroid/view/ViewPropertyAnimator;J)Landroid/view/ViewPropertyAnimator;
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method private outAnimation(Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->TRANSITION_DURATION_OUT:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getMirror()Lcom/android/systemui/statusbar/phone/BrightnessPanel;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    return-object v0
.end method

.method public getScreenBrightness()I
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mPowerManager:Landroid/os/IPowerManager;

    const-string/jumbo v3, "getScreenBrightnessReal"

    const-class v4, Ljava/lang/Integer;

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4, v5}, Lmiui/util/ReflectionUtils;->tryCallMethod(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Object;)Lmiui/util/ObjectReference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_0
    return v1
.end method

.method public hideMirror()V
    .locals 3

    sget-boolean v1, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v1}, Lmiui/app/ToggleManager;->isBrightnessAutoMode()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mPowerManager:Landroid/os/IPowerManager;

    const/high16 v2, -0x40000000    # -2.0f

    invoke-interface {v1, v2}, Landroid/os/IPowerManager;->setTemporaryScreenAutoBrightnessAdjustmentSettingOverride(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->outAnimation(Landroid/view/ViewPropertyAnimator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$3;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$3;-><init>(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    sget-boolean v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->stopObserverBrightness()V

    :cond_0
    return-void
.end method

.method public setBrightnessPanel(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)V
    .locals 3

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    sget-boolean v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v0}, Lmiui/app/ToggleManager;->isBrightnessAutoMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->getScreenBrightness()I

    move-result v1

    sget v2, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setValue(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessPanel:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mManualScreenBrightness:I

    sget v2, Lmiui/app/ToggleManager;->MINIMUM_BACKLIGHT:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setValue(I)V

    goto :goto_0
.end method

.method public setCurrentUserId(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mCurrentUserId:I

    return-void
.end method

.method public setLocation()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mOriginal:Landroid/view/View;

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mInt2Cache:[I

    invoke-virtual {v4, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mInt2Cache:[I

    aget v4, v4, v7

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mOriginal:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v2, v4, v5

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mInt2Cache:[I

    aget v3, v4, v6

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getSeekBar()Landroid/widget/SeekBar;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mInt2Cache:[I

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->getLocationOnScreen([I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mInt2Cache:[I

    aget v4, v4, v7

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getSeekBar()Landroid/widget/SeekBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v0, v4, v5

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mInt2Cache:[I

    aget v1, v4, v6

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getTranslationY()F

    move-result v5

    int-to-float v6, v2

    add-float/2addr v5, v6

    int-to-float v6, v0

    sub-float/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setTranslationY(F)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getTranslationX()F

    move-result v5

    int-to-float v6, v3

    add-float/2addr v5, v6

    int-to-float v6, v1

    sub-float/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setTranslationX(F)V

    return-void
.end method

.method public setView(Lcom/android/systemui/statusbar/phone/StatusBarWindowView;)V
    .locals 5

    const/4 v4, 0x0

    const v0, 0x7f0f01c2

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/phone/StatusBarWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    const v1, 0x7f02001c

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setBackgroundResource(I)V

    const v0, 0x7f0f0172

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/phone/StatusBarWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mExpandView:Landroid/view/View;

    const v0, 0x7f0f015a

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/phone/StatusBarWindowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mStatusBarView:Landroid/view/View;

    sget-boolean v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->getBrightnessFile()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;-><init>(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessFileObserver:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/app/ToggleManager;->createInstance(Landroid/content/Context;)Lmiui/app/ToggleManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mToggleManager:Lmiui/app/ToggleManager;

    const-string/jumbo v0, "power"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/IPowerManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPowerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mPowerManager:Landroid/os/IPowerManager;

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v0}, Lmiui/app/ToggleManager;->isBrightnessAutoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->startObserverBrightness(J)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_brightness_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessModeObserver:Landroid/database/ContentObserver;

    iget v3, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mCurrentUserId:I

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_brightness"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessModeObserver:Landroid/database/ContentObserver;

    iget v3, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mCurrentUserId:I

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_brightness"

    iget v2, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mCurrentUserId:I

    const/16 v3, 0x80

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mManualScreenBrightness:I

    return-void
.end method

.method public showMirror(Landroid/view/View;)V
    .locals 6

    sget-boolean v1, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v1}, Lmiui/app/ToggleManager;->isBrightnessAutoMode()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mPowerManager:Landroid/os/IPowerManager;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-interface {v1, v2}, Landroid/os/IPowerManager;->setTemporaryScreenAutoBrightnessAdjustmentSettingOverride(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mOriginal:Landroid/view/View;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->showMirrorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->showMirrorRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x96

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string/jumbo v1, "brightness"

    invoke-static {v1}, Lcom/android/systemui/AnalyticsHelper;->trackClickLocation(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public startObserverBrightness(J)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessFileObserver:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessFileObserver:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->startWatching(J)V

    :cond_0
    return-void
.end method

.method public stopObserverBrightness()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessFileObserver:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessFileObserver:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController$BrightnessFileObserver;->stopWatching()V

    :cond_0
    return-void
.end method

.method public updateResources(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getPaddingStart()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getPaddingEnd()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getPaddingStart()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getPaddingEnd()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->mBrightnessMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
