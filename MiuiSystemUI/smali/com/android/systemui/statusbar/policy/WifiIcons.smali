.class Lcom/android/systemui/statusbar/policy/WifiIcons;
.super Ljava/lang/Object;
.source "WifiIcons.java"


# static fields
.field static final WIFI_LEVEL_COUNT:I

.field static final WIFI_SIGNAL_STRENGTH:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const v6, 0x7f0202bd

    const/4 v5, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [[I

    const v1, 0x7f0202bf

    const v2, 0x7f0202c2

    const v3, 0x7f0202c5

    const v4, 0x7f0202c8

    filled-new-array {v6, v1, v2, v3, v4}, [I

    move-result-object v1

    aput-object v1, v0, v5

    const v1, 0x7f0202c1

    const v2, 0x7f0202c4

    const v3, 0x7f0202c7

    const v4, 0x7f0202ca

    filled-new-array {v6, v1, v2, v3, v4}, [I

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/statusbar/policy/WifiIcons;->WIFI_SIGNAL_STRENGTH:[[I

    sget-object v0, Lcom/android/systemui/statusbar/policy/WifiIcons;->WIFI_SIGNAL_STRENGTH:[[I

    aget-object v0, v0, v5

    array-length v0, v0

    sput v0, Lcom/android/systemui/statusbar/policy/WifiIcons;->WIFI_LEVEL_COUNT:I

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
