.class final Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;
.super Landroid/os/Handler;
.source "BaseNetworkController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/BaseNetworkController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WorkHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14

    const/16 v13, 0x8

    const/4 v12, 0x7

    const/4 v11, 0x6

    const/4 v10, 0x5

    const/4 v9, 0x0

    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v5, 0x0

    const/4 v2, 0x0

    :goto_1
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneCount:I

    if-ge v2, v7, :cond_2

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneType:[I

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v8

    invoke-virtual {v8, v2}, Lmiui/telephony/TelephonyManager;->getPhoneTypeForSlot(I)I

    move-result v8

    aput v8, v7, v2

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mLastPhoneType:[I

    aget v7, v7, v2

    iget-object v8, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v8, v8, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneType:[I

    aget v8, v8, v2

    if-eq v7, v8, :cond_1

    const/4 v5, 0x1

    :cond_1
    const-string/jumbo v7, "StatusBar.NetworkController"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "phone type slot:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " type:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v9, v9, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPhoneType:[I

    aget v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    if-eqz v5, :cond_0

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v10}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_1
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/systemui/VirtualSimUtils;->getVirtualSimCarrierName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v11}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v7, v11, v8, v9, v6}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const-string/jumbo v8, "connectivity"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-boolean v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mWifiConnected:Z

    if-eqz v7, :cond_3

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v4

    :goto_2
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-boolean v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsActiveNetworkMetered:Z

    if-eq v7, v4, :cond_0

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iput-boolean v4, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mIsActiveNetworkMetered:Z

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v12}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    :pswitch_3
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    const-string/jumbo v8, "connectivity"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v13}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mHandler:Landroid/os/Handler;

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v7, v13, v8, v9, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :pswitch_4
    iget-object v7, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$WorkHandler;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v7, v7, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "status_bar_real_carrier"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v9, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    invoke-static {v8, v9, v7}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
