.class Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;
.super Landroid/content/BroadcastReceiver;
.source "OldmanStatusPaneCommonReceivers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v1, p2}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-wrap1(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const-string/jumbo v1, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v1, p2}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-wrap4(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string/jumbo v1, "android.intent.action.LEAVE_INCALL_SCREEN_DURING_CALL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "android.intent.action.ENTER_INCALL_SCREEN_DURING_CALL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v1, p2}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-wrap3(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V

    goto :goto_0

    :cond_6
    const-string/jumbo v1, "android.intent.action.ALARM_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v1, p2}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-wrap0(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V

    goto :goto_0

    :cond_7
    const-string/jumbo v1, "android.intent.action.SYNC_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v1, p2}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-wrap5(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V

    goto :goto_0

    :cond_8
    const-string/jumbo v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$4;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v1, p2}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-wrap2(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;Landroid/content/Intent;)V

    goto :goto_0
.end method
