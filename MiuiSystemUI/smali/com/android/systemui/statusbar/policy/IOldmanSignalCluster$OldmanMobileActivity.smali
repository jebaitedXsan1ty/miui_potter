.class public final enum Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;
.super Ljava/lang/Enum;
.source "IOldmanSignalCluster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OldmanMobileActivity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

.field public static final enum IN:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

.field public static final enum INOUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

.field public static final enum OUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

.field public static final enum UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    const-string/jumbo v1, "UNK"

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    const-string/jumbo v1, "IN"

    invoke-direct {v0, v1, v3}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->IN:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    const-string/jumbo v1, "OUT"

    invoke-direct {v0, v1, v4}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->OUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    const-string/jumbo v1, "INOUT"

    invoke-direct {v0, v1, v5}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->INOUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->IN:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->OUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->INOUT:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->$VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;
    .locals 1

    const-class v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    return-object v0
.end method

.method public static values()[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;
    .locals 1

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;->$VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileActivity;

    return-object v0
.end method
