.class Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$1;
.super Ljava/lang/Object;
.source "OldmanStatusPaneCommonReceivers.java"

# interfaces
.implements Lmiui/provider/ExtraTelephony$QuietModeEnableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$1;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQuietModeEnableChange(Z)V
    .locals 3

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$1;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v2}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-get0(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    invoke-interface {v0, p1}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;->onQuietModeChanged(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method
