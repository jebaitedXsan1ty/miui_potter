.class public Lcom/android/systemui/statusbar/policy/ClockText;
.super Ljava/lang/Object;
.source "ClockText.java"

# interfaces
.implements Lcom/android/systemui/statusbar/policy/ILeftCorner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;
    }
.end annotation


# static fields
.field private static sNotch:Z

.field private static final sReceiverInfo:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCalendar:Lmiui/date/Calendar;

.field private mClock:Lcom/android/systemui/statusbar/policy/Clock;

.field private mContext:Landroid/content/Context;

.field private mDemoMode:Z

.field private mHandler:Landroid/os/Handler;

.field private mShowAmPm:Z

.field private mShowDate:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lcom/android/systemui/statusbar/policy/ClockText;->sReceiverInfo:Ljava/lang/ThreadLocal;

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/statusbar/policy/ClockText;->sNotch:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/systemui/statusbar/policy/Clock;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mShowAmPm:Z

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mClock:Lcom/android/systemui/statusbar/policy/Clock;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private showDemoModeClock()V
    .locals 6

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    if-nez v3, :cond_0

    new-instance v3, Lmiui/date/Calendar;

    invoke-direct {v3}, Lmiui/date/Calendar;-><init>()V

    iput-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/date/Calendar;->setTimeZone(Ljava/util/TimeZone;)Lmiui/date/Calendar;

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    const/16 v4, 0x12

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Lmiui/date/Calendar;->set(II)Lmiui/date/Calendar;

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    const/16 v4, 0x14

    const/16 v5, 0x24

    invoke-virtual {v3, v4, v5}, Lmiui/date/Calendar;->set(II)Lmiui/date/Calendar;

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mShowDate:Z

    if-eqz v3, :cond_2

    const v1, 0x7f0d0257

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/date/Calendar;->format(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/systemui/statusbar/policy/ClockText;->updateText(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v3, Lcom/android/systemui/statusbar/policy/ClockText;->sReceiverInfo:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;->getTimeFormat()I

    move-result v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    invoke-virtual {v3}, Lmiui/date/Calendar;->getTimeInMillis()J

    move-result-wide v4

    or-int/lit8 v3, v0, 0xc

    invoke-static {v4, v5, v3}, Lmiui/date/DateUtils;->formatDateTime(JI)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/systemui/statusbar/policy/ClockText;->updateText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mClock:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/Clock;->isClock()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mClock:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->setCornerText(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public exitDemomode()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mDemoMode:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/ClockText;->updateClock()V

    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public registerObservers()V
    .locals 3

    const/4 v2, 0x0

    sget-object v1, Lcom/android/systemui/statusbar/policy/ClockText;->sReceiverInfo:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;

    invoke-direct {v0, v2}, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;-><init>(Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;)V

    sget-object v1, Lcom/android/systemui/statusbar/policy/ClockText;->sReceiverInfo:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mContext:Landroid/content/Context;

    const/4 v2, -0x2

    invoke-static {v1, v2}, Lcom/android/systemui/SystemUICompatibility;->is24HourFormat(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x20

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;->setTimeFormat(I)V

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;->addView(Lcom/android/systemui/statusbar/policy/ClockText;)V

    return-void

    :cond_1
    const/16 v1, 0x10

    goto :goto_0
.end method

.method public setShowAmPm(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mShowAmPm:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mShowAmPm:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/ClockText;->updateClock()V

    :cond_0
    return-void
.end method

.method public setShowDate(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mShowDate:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mShowDate:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/ClockText;->updateClock()V

    :cond_0
    return-void
.end method

.method public showDemomode()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mDemoMode:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/ClockText;->updateClock()V

    return-void
.end method

.method public unregisterObservers()V
    .locals 2

    sget-object v1, Lcom/android/systemui/statusbar/policy/ClockText;->sReceiverInfo:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;->removeView(Lcom/android/systemui/statusbar/policy/ClockText;)V

    :cond_0
    return-void
.end method

.method public update()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/policy/ClockText;->updateClock()V

    return-void
.end method

.method updateClock()V
    .locals 6

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mDemoMode:Z

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/ClockText;->showDemoModeClock()V

    return-void

    :cond_0
    sget-object v3, Lcom/android/systemui/statusbar/policy/ClockText;->sReceiverInfo:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mShowDate:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    if-nez v3, :cond_1

    new-instance v3, Lmiui/date/Calendar;

    invoke-direct {v3}, Lmiui/date/Calendar;-><init>()V

    iput-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/date/Calendar;->setTimeZone(Ljava/util/TimeZone;)Lmiui/date/Calendar;

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lmiui/date/Calendar;->setTimeInMillis(J)Lmiui/date/Calendar;

    sget-boolean v3, Lcom/android/systemui/statusbar/policy/ClockText;->sNotch:Z

    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;->getTimeFormat()I

    move-result v3

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    const v1, 0x7f0d0258

    :goto_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mCalendar:Lmiui/date/Calendar;

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/date/Calendar;->format(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/systemui/statusbar/policy/ClockText;->updateText(Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const v1, 0x7f0d0257

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/policy/ClockText$ReceiverInfo;->getTimeFormat()I

    move-result v0

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/policy/ClockText;->mShowAmPm:Z

    if-eqz v3, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    or-int/lit8 v3, v0, 0xc

    invoke-static {v4, v5, v3}, Lmiui/date/DateUtils;->formatDateTime(JI)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/systemui/statusbar/policy/ClockText;->updateText(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    or-int/lit8 v3, v0, 0xc

    or-int/lit8 v3, v3, 0x40

    invoke-static {v4, v5, v3}, Lmiui/date/DateUtils;->formatDateTime(JI)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/systemui/statusbar/policy/ClockText;->updateText(Ljava/lang/String;)V

    goto :goto_1
.end method
