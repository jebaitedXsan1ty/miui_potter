.class Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;
.super Landroid/os/Handler;
.source "BaseNetworkController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/BaseNetworkController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/BaseNetworkController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string/jumbo v0, "StatusBar.NetworkController"

    const-string/jumbo v1, "delay msg, update Service State Changed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v2, v2, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempServiceState:[Landroid/telephony/ServiceState;

    iget v3, p1, Landroid/os/Message;->arg1:I

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mNetworkNameOriginal:[Ljava/lang/String;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v2, v2, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mTempNetworkName:[Ljava/lang/String;

    iget v3, p1, Landroid/os/Message;->arg1:I

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v2, v2, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    iget v3, p1, Landroid/os/Message;->arg1:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v0, v0, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v2, v2, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mDataNetType:[I

    iget v3, p1, Landroid/os/Message;->arg1:I

    aget v2, v2, v3

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v3, v3, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mServiceState:[Landroid/telephony/ServiceState;

    iget v4, p1, Landroid/os/Message;->arg1:I

    aget-object v3, v3, v4

    invoke-static {v2, v3}, Lcom/android/systemui/statusbar/NetworkTypeUtils;->getDataNetTypeFromServiceState(ILandroid/telephony/ServiceState;)I

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateServiceStateChanged(I)V

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "StatusBar.NetworkController"

    const-string/jumbo v1, "delay msg, update network name"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews()V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVirtualSimName:Ljava/lang/String;

    const-string/jumbo v0, "StatusBar.NetworkController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "delay msg, update vitrual sim network name: mVirtualSimName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v2, v2, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mVirtualSimName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v1, v1, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowSpn:[Z

    aget-boolean v1, v1, v5

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v2, v2, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mSpn:[Ljava/lang/String;

    aget-object v2, v2, v5

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v3, v3, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mShowPlmn:[Z

    aget-boolean v3, v3, v5

    iget-object v4, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget-object v4, v4, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->mPlmn:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateNetworkName(ZLjava/lang/String;ZLjava/lang/String;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    invoke-virtual {v0, v5}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews(I)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    invoke-static {v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->-wrap1(Lcom/android/systemui/statusbar/policy/BaseNetworkController;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/net/NetworkInfo;

    invoke-virtual {v1, v2, v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->updateConnectivity(ILandroid/net/NetworkInfo;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/policy/BaseNetworkController$1;->this$0:Lcom/android/systemui/statusbar/policy/BaseNetworkController;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/BaseNetworkController;->refreshViews()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
