.class public final enum Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;
.super Ljava/lang/Enum;
.source "IOldmanSignalCluster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OldmanMobileDataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field public static final enum E:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field public static final enum G:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field public static final enum G3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field public static final enum G4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field public static final enum H:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field public static final enum HP:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field public static final enum UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

.field public static final enum X1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const-string/jumbo v1, "UNK"

    invoke-direct {v0, v1, v3}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const-string/jumbo v1, "G4"

    invoke-direct {v0, v1, v4}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const-string/jumbo v1, "HP"

    invoke-direct {v0, v1, v5}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->HP:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const-string/jumbo v1, "H"

    invoke-direct {v0, v1, v6}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->H:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const-string/jumbo v1, "G3"

    invoke-direct {v0, v1, v7}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const-string/jumbo v1, "E"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->E:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const-string/jumbo v1, "X1"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->X1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    new-instance v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const-string/jumbo v1, "G"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->UNK:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G4:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->HP:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->H:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G3:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->E:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->X1:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->G:Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->$VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;
    .locals 1

    const-class v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    return-object v0
.end method

.method public static values()[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;
    .locals 1

    sget-object v0, Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;->$VALUES:[Lcom/android/systemui/statusbar/policy/IOldmanSignalCluster$OldmanMobileDataType;

    return-object v0
.end method
