.class public Lcom/android/systemui/statusbar/policy/TelephonyIcons;
.super Ljava/lang/Object;
.source "TelephonyIcons.java"


# static fields
.field static final DATA_SIGNAL_STRENGTH:[[I

.field public static final M_TELEPHONY_SIGNAL_STRENGTH:[[I

.field public static final TELEPHONY_SIGNAL_STRENGTH:[[I

.field static final TELEPHONY_SIGNAL_STRENGTH_ROAMING:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x6

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [[I

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    new-array v0, v3, [[I

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH_ROAMING:[[I

    new-array v0, v3, [[I

    new-array v1, v4, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->M_TELEPHONY_SIGNAL_STRENGTH:[[I

    sget-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_SIGNAL_STRENGTH:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f020271
        0x7f020275
        0x7f020279
        0x7f02027d
        0x7f020281
        0x7f020285
    .end array-data

    :array_1
    .array-data 4
        0x7f020271
        0x7f020275
        0x7f020279
        0x7f02027d
        0x7f020281
        0x7f020285
    .end array-data

    :array_2
    .array-data 4
        0x7f020273
        0x7f020277
        0x7f02027b
        0x7f02027f
        0x7f020283
        0x7f020287
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNetworkTypeName(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string/jumbo v0, ""

    return-object v0

    :pswitch_0
    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsUSAOperation:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, ""

    return-object v0

    :cond_0
    const-string/jumbo v0, "G"

    return-object v0

    :pswitch_1
    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsUSAOperation:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, ""

    return-object v0

    :cond_1
    const-string/jumbo v0, "E"

    return-object v0

    :pswitch_2
    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsUSAOperation:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, ""

    return-object v0

    :cond_2
    const-string/jumbo v0, "3G"

    return-object v0

    :pswitch_3
    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsUSAOperation:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "4G"

    return-object v0

    :cond_3
    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsMXOperation:Z

    if-eqz v0, :cond_4

    const-string/jumbo v0, "3G"

    return-object v0

    :cond_4
    const-string/jumbo v0, "H"

    return-object v0

    :pswitch_4
    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsUSAOperation:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsMXOperation:Z

    if-eqz v0, :cond_6

    :cond_5
    const-string/jumbo v0, "4G"

    return-object v0

    :cond_6
    const-string/jumbo v0, "H+"

    return-object v0

    :pswitch_5
    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsUSAOperation:Z

    if-nez v0, :cond_7

    const-string/jumbo v0, "PL"

    invoke-static {v0}, Lmiui/os/Build;->checkRegion(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_7
    const-string/jumbo v0, "LTE"

    return-object v0

    :cond_8
    sget-boolean v0, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsMXOperation:Z

    if-eqz v0, :cond_a

    :cond_9
    const-string/jumbo v0, "4G LTE"

    return-object v0

    :cond_a
    const-string/jumbo v0, "4G"

    return-object v0

    :pswitch_6
    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsUSAOperation:Z

    if-nez v0, :cond_b

    const-string/jumbo v0, "PL"

    invoke-static {v0}, Lmiui/os/Build;->checkRegion(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_b
    const-string/jumbo v0, "LTE+"

    return-object v0

    :cond_c
    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsIROperation:Z

    if-nez v0, :cond_d

    sget-boolean v0, Lcom/android/systemui/MCCUtils;->sIsNPOperation:Z

    if-eqz v0, :cond_e

    :cond_d
    const-string/jumbo v0, "4.5G"

    return-object v0

    :cond_e
    const-string/jumbo v0, "4G+"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
