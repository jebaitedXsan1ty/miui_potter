.class Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;
.super Landroid/content/BroadcastReceiver;
.source "OldmanStatusPaneCommonReceivers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mCalendar:Ljava/util/Calendar;

.field private mClockFormat:Ljava/text/SimpleDateFormat;

.field private mClockFormatString:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mCalendar:Ljava/util/Calendar;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mClockFormatString:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mClockFormat:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method private getSmallTime()Ljava/lang/CharSequence;
    .locals 11

    const/16 v8, 0xc

    const/4 v7, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v5}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-get1(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mCalendar:Ljava/util/Calendar;

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v5, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v6, "%02d:%02d"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    return-object v5

    :cond_0
    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mCalendar:Ljava/util/Calendar;

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v5, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v5, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-nez v2, :cond_1

    const/16 v2, 0xc

    :cond_1
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v6, "%d:%02d"

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private updateClock()V
    .locals 6

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mCalendar:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->getSmallTime()Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->this$0:Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;

    invoke-static {v3}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;->-get0(Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;

    invoke-interface {v0, v2}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$IOldmanStatusPaneCommonCallback;->onClockTextChanged(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "time-zone"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mCalendar:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mClockFormat:Ljava/text/SimpleDateFormat;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mClockFormat:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/statusbar/policy/OldmanStatusPaneCommonReceivers$2;->updateClock()V

    return-void
.end method
