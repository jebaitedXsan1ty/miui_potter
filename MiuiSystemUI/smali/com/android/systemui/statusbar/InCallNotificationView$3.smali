.class Lcom/android/systemui/statusbar/InCallNotificationView$3;
.super Ljava/lang/Object;
.source "InCallNotificationView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/InCallNotificationView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/InCallNotificationView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/InCallNotificationView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/InCallNotificationView$3;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView$3;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/InCallNotificationView;->-get3(Lcom/android/systemui/statusbar/InCallNotificationView;)Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManagerEx;->endCall()Z

    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView$3;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/InCallNotificationView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->exitFloatingNotification(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/InCallNotificationView$3;->this$0:Lcom/android/systemui/statusbar/InCallNotificationView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/InCallNotificationView;->-get2(Lcom/android/systemui/statusbar/InCallNotificationView;)Lcom/miui/voiptalk/service/MiuiVoipManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/voiptalk/service/MiuiVoipManager;->endCall()V

    goto :goto_0
.end method
