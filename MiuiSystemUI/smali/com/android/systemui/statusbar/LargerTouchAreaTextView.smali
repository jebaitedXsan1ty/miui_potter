.class public Lcom/android/systemui/statusbar/LargerTouchAreaTextView;
.super Landroid/widget/TextView;
.source "LargerTouchAreaTextView.java"


# instance fields
.field private mEnabled:Z

.field private mRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mRect:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mEnabled:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mRect:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mEnabled:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mRect:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mEnabled:Z

    return-void
.end method


# virtual methods
.method public enableLargerTouchArea()V
    .locals 3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mEnabled:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mRect:Landroid/graphics/Rect;

    invoke-direct {v1, v2, p0}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    invoke-super/range {p0 .. p5}, Landroid/widget/TextView;->onLayout(ZIIII)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mRect:Landroid/graphics/Rect;

    add-int v2, p2, p4

    const/4 v3, 0x0

    invoke-virtual {v1, v3, p3, v2, p5}, Landroid/graphics/Rect;->set(IIII)V

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mEnabled:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lcom/android/systemui/statusbar/LargerTouchAreaTextView;->mRect:Landroid/graphics/Rect;

    invoke-direct {v1, v2, p0}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    :cond_0
    return-void
.end method
