.class Lcom/android/systemui/statusbar/BaseStatusBar$3;
.super Ljava/lang/Object;
.source "BaseStatusBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/BaseStatusBar;->updateNotificationVetoButton(Landroid/view/View;Lcom/android/systemui/statusbar/ExpandedNotification;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

.field final synthetic val$n:Lcom/android/systemui/statusbar/ExpandedNotification;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/BaseStatusBar;Lcom/android/systemui/statusbar/ExpandedNotification;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iput-object p2, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iget-object v3, v3, Lcom/android/systemui/statusbar/BaseStatusBar;->mNotificationData:Lcom/android/systemui/statusbar/NotificationData;

    iget-object v4, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/NotificationData;->findByNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)Lcom/android/systemui/statusbar/NotificationData$Entry;

    move-result-object v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v3

    iget-object v3, v3, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "right_swipe"

    invoke-static {v3, v4}, Lcom/android/systemui/AnalyticsHelper;->trackNotificationRemove(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/systemui/analytics/RemoveEvent;

    invoke-direct {v1}, Lcom/android/systemui/analytics/RemoveEvent;-><init>()V

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotificationEvent()Lcom/android/systemui/analytics/NotificationEvent;

    move-result-object v3

    iput-object v3, v1, Lcom/android/systemui/analytics/RemoveEvent;->notificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/BaseStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/PanelView;->getNotificationContainer()Landroid/view/ViewGroup;

    move-result-object v3

    iget-object v4, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/android/systemui/analytics/RemoveEvent;->index:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iget-object v3, v3, Lcom/android/systemui/statusbar/BaseStatusBar;->mNotificationSession:Lcom/android/systemui/statusbar/phone/rank/ISession;

    invoke-interface {v3, v0}, Lcom/android/systemui/statusbar/phone/rank/ISession;->getSessionIndex(Lcom/android/systemui/statusbar/NotificationData$Entry;)I

    move-result v3

    iput v3, v1, Lcom/android/systemui/analytics/RemoveEvent;->sessionIndex:I

    const-string/jumbo v3, "statusbar"

    iput-object v3, v1, Lcom/android/systemui/analytics/RemoveEvent;->location:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Notification;->isGroupSummary()Z

    move-result v3

    iput-boolean v3, v1, Lcom/android/systemui/analytics/RemoveEvent;->groupNotification:Z

    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v3

    invoke-virtual {v3, v1}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-static {v3}, Lcom/android/systemui/Util;->isFoldTips(Lcom/android/systemui/statusbar/ExpandedNotification;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-static {v3}, Lcom/android/systemui/statusbar/BaseStatusBar;->-wrap1(Lcom/android/systemui/statusbar/BaseStatusBar;)V

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iget-object v3, v3, Lcom/android/systemui/statusbar/BaseStatusBar;->mNotificationData:Lcom/android/systemui/statusbar/NotificationData;

    invoke-virtual {v3, v5}, Lcom/android/systemui/statusbar/NotificationData;->size(Z)I

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iget-object v4, v4, Lcom/android/systemui/statusbar/BaseStatusBar;->mGroupManager:Lcom/android/systemui/statusbar/phone/NotificationGroupManager;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/NotificationGroupManager;->getFoldInvisibleCount()I

    move-result v4

    sub-int v2, v3, v4

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/BaseStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v3

    iget-boolean v3, v3, Lcom/android/systemui/statusbar/phone/PanelView;->mIsFoldOpened:Z

    if-eqz v3, :cond_2

    if-ne v2, v5, :cond_2

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    invoke-virtual {v3, v0}, Lcom/android/systemui/statusbar/BaseStatusBar;->closeFold(Lcom/android/systemui/statusbar/NotificationData$Entry;)V

    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    iget-object v4, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iget-object v4, v4, Lcom/android/systemui/statusbar/BaseStatusBar;->mBarService:Lcom/android/internal/statusbar/IStatusBarService;

    invoke-static {v3, v4}, Lcom/android/systemui/SystemUICompatibility;->onNotificationClear(Lcom/android/systemui/statusbar/ExpandedNotification;Lcom/android/internal/statusbar/IStatusBarService;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->this$0:Lcom/android/systemui/statusbar/BaseStatusBar;

    iget-object v3, v3, Lcom/android/systemui/statusbar/BaseStatusBar;->mGroupManager:Lcom/android/systemui/statusbar/phone/NotificationGroupManager;

    iget-object v4, p0, Lcom/android/systemui/statusbar/BaseStatusBar$3;->val$n:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/phone/NotificationGroupManager;->onSwipeClearNotification(Lcom/android/systemui/statusbar/ExpandedNotification;)V

    goto :goto_0
.end method
