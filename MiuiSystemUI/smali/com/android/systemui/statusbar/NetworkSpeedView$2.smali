.class Lcom/android/systemui/statusbar/NetworkSpeedView$2;
.super Landroid/content/BroadcastReceiver;
.source "NetworkSpeedView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/NetworkSpeedView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/NetworkSpeedView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$2;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    iget-object v2, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$2;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/NetworkSpeedView;->-get0(Lcom/android/systemui/statusbar/NetworkSpeedView;)Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    iget-object v3, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$2;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    :goto_0
    invoke-static {v3, v2}, Lcom/android/systemui/statusbar/NetworkSpeedView;->-set1(Lcom/android/systemui/statusbar/NetworkSpeedView;Z)Z

    iget-object v2, p0, Lcom/android/systemui/statusbar/NetworkSpeedView$2;->this$0:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/NetworkSpeedView;->-wrap4(Lcom/android/systemui/statusbar/NetworkSpeedView;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
