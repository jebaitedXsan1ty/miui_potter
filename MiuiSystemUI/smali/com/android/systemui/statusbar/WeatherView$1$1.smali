.class Lcom/android/systemui/statusbar/WeatherView$1$1;
.super Ljava/lang/Object;
.source "WeatherView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/WeatherView$1;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/WeatherView$1;

.field final synthetic val$action:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/WeatherView$1;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/WeatherView$1$1;->this$1:Lcom/android/systemui/statusbar/WeatherView$1;

    iput-object p2, p0, Lcom/android/systemui/statusbar/WeatherView$1$1;->val$action:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const-string/jumbo v0, "android.intent.action.LOCALE_CHANGED"

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView$1$1;->val$action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "android.intent.action.TIMEZONE_CHANGED"

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView$1$1;->val$action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView$1$1;->this$1:Lcom/android/systemui/statusbar/WeatherView$1;

    iget-object v0, v0, Lcom/android/systemui/statusbar/WeatherView$1;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/WeatherView;->-set0(Lcom/android/systemui/statusbar/WeatherView;Ljava/util/Calendar;)Ljava/util/Calendar;

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView$1$1;->this$1:Lcom/android/systemui/statusbar/WeatherView$1;

    iget-object v0, v0, Lcom/android/systemui/statusbar/WeatherView$1;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/WeatherView;->-wrap6(Lcom/android/systemui/statusbar/WeatherView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView$1$1;->this$1:Lcom/android/systemui/statusbar/WeatherView$1;

    iget-object v0, v0, Lcom/android/systemui/statusbar/WeatherView$1;->this$0:Lcom/android/systemui/statusbar/WeatherView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/WeatherView;->-wrap7(Lcom/android/systemui/statusbar/WeatherView;)V

    return-void
.end method
