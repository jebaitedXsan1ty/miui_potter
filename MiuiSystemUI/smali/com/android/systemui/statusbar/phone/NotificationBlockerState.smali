.class public abstract Lcom/android/systemui/statusbar/phone/NotificationBlockerState;
.super Ljava/lang/Object;
.source "NotificationBlockerState.java"


# instance fields
.field protected mInAnimator:Landroid/animation/Animator;

.field protected mOutAnimator:Landroid/animation/Animator;

.field protected mStateView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mStateView:Landroid/view/View;

    return-void
.end method


# virtual methods
.method protected abstract getInAnimator()Landroid/animation/Animator;
.end method

.method protected abstract getOutAnimator()Landroid/animation/Animator;
.end method

.method public inAnimation()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mInAnimator:Landroid/animation/Animator;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->getInAnimator()Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mInAnimator:Landroid/animation/Animator;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mInAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/android/systemui/statusbar/phone/NotificationBlockerState$1;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/NotificationBlockerState$1;-><init>(Lcom/android/systemui/statusbar/phone/NotificationBlockerState;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mInAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    return-void
.end method

.method public outAnimation()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mOutAnimator:Landroid/animation/Animator;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->getOutAnimator()Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mOutAnimator:Landroid/animation/Animator;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mOutAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/android/systemui/statusbar/phone/NotificationBlockerState$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/NotificationBlockerState$2;-><init>(Lcom/android/systemui/statusbar/phone/NotificationBlockerState;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mOutAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    return-void
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mInAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mInAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mOutAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mOutAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    :cond_1
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlockerState;->mStateView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
