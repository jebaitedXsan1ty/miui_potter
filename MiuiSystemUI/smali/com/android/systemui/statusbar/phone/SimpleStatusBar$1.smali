.class Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;
.super Ljava/lang/Object;
.source "SimpleStatusBar.java"

# interfaces
.implements Lcom/android/systemui/statusbar/policy/BatteryController$ChargingStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/SimpleStatusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChargingStateChanged(ZZ)V
    .locals 4

    const-string/jumbo v0, "SimpleStatusBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onChargingStateChanged() charging="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "; quickCharging="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v2, v3}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateQuickChargingDelayed(J)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v2, v3}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateQuickChargingDelayed(J)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateQuickCharging(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->-get2(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
