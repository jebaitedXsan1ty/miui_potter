.class Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;
.super Ljava/lang/Object;
.source "PhoneStatusBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;

.field final synthetic val$clearableViews:Ljava/util/ArrayList;

.field final synthetic val$sc:Lcom/android/systemui/SwipeHelper$Callback;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;Lcom/android/systemui/SwipeHelper$Callback;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;->this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;

    iput-object p2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;->val$sc:Lcom/android/systemui/SwipeHelper$Callback;

    iput-object p3, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;->val$clearableViews:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    :try_start_0
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;->val$sc:Lcom/android/systemui/SwipeHelper$Callback;

    const/4 v7, 0x1

    invoke-interface {v6, v7}, Lcom/android/systemui/SwipeHelper$Callback;->setViewRemoval(Z)V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;->val$clearableViews:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v3, 0x0

    invoke-static {v4}, Lcom/android/systemui/AnalyticsHelper;->trackNotificationClearAll(I)V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;->val$clearableViews:Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    add-int/lit8 v3, v3, 0x1

    const v6, 0x7f0f0006

    invoke-virtual {v0, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/systemui/statusbar/NotificationData$Entry;

    iget-object v5, v6, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "click_clear_button"

    invoke-static {v6, v7}, Lcom/android/systemui/AnalyticsHelper;->trackNotificationRemove(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;->this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;

    iget-object v6, v6, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-static {v6}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->-get3(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)Lcom/android/internal/statusbar/IStatusBarService;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/systemui/SystemUICompatibility;->onNotificationClear(Lcom/android/systemui/statusbar/ExpandedNotification;Lcom/android/internal/statusbar/IStatusBarService;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    :cond_1
    return-void
.end method
