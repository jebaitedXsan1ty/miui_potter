.class public Lcom/android/systemui/statusbar/phone/rank/PackageRank;
.super Ljava/lang/Object;
.source "PackageRank.java"

# interfaces
.implements Lcom/android/systemui/statusbar/phone/rank/IRank;


# static fields
.field public static PKG_MMS:Ljava/lang/String;

.field public static PKG_QQ:Ljava/lang/String;

.field public static PKG_WEICHAT:Ljava/lang/String;


# instance fields
.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "com.android.mms"

    sput-object v0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->PKG_MMS:Ljava/lang/String;

    const-string/jumbo v0, "com.tencent.mobileqq"

    sput-object v0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->PKG_QQ:Ljava/lang/String;

    const-string/jumbo v0, "com.tencent.mm"

    sput-object v0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->PKG_WEICHAT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I
    .locals 10

    const-wide/16 v8, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    iget-object v0, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    iget-object v1, p2, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->getScore(Lcom/android/systemui/statusbar/ExpandedNotification;)D

    move-result-wide v6

    add-double v2, v8, v6

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->getScore(Lcom/android/systemui/statusbar/ExpandedNotification;)D

    move-result-wide v6

    add-double v4, v8, v6

    cmpl-double v6, v2, v4

    if-eqz v6, :cond_1

    sub-double v6, v2, v4

    cmpl-double v6, v6, v8

    if-lez v6, :cond_0

    const/4 v6, 0x1

    :goto_0
    return v6

    :cond_0
    const/4 v6, -0x1

    goto :goto_0

    :cond_1
    invoke-static {p1, p2}, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->compareByTime(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I

    move-result v6

    return v6
.end method

.method public getPercent()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public getScore(Lcom/android/systemui/statusbar/ExpandedNotification;)D
    .locals 4

    const-wide/16 v2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-wide v2

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->mList:Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->mList:Ljava/util/List;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->mList:Ljava/util/List;

    sget-object v1, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->PKG_MMS:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->mList:Ljava/util/List;

    sget-object v1, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->PKG_QQ:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->mList:Ljava/util/List;

    sget-object v1, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->PKG_WEICHAT:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->mList:Ljava/util/List;

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    return-wide v0

    :cond_3
    return-wide v2
.end method
