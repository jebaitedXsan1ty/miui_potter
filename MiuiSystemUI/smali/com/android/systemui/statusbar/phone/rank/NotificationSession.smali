.class public Lcom/android/systemui/statusbar/phone/rank/NotificationSession;
.super Ljava/lang/Object;
.source "NotificationSession.java"

# interfaces
.implements Lcom/android/systemui/statusbar/phone/rank/ISession;


# instance fields
.field private mNotificationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/NotificationSession;->mNotificationList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getSessionIndex(Lcom/android/systemui/statusbar/NotificationData$Entry;)I
    .locals 3

    const/4 v2, -0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/NotificationSession;->mNotificationList:Ljava/util/List;

    iget-object v1, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->key:Landroid/os/IBinder;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ne v2, v0, :cond_1

    :cond_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/NotificationSession;->mNotificationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/rank/NotificationSession;->mNotificationList:Ljava/util/List;

    iget-object v2, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->key:Landroid/os/IBinder;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public sessionEnd()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/NotificationSession;->mNotificationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public sessionStart(Lcom/android/systemui/statusbar/NotificationData;)V
    .locals 5

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/NotificationData;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Lcom/android/systemui/statusbar/NotificationData;->get(I)Lcom/android/systemui/statusbar/NotificationData$Entry;

    move-result-object v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/rank/NotificationSession;->mNotificationList:Ljava/util/List;

    iget-object v4, v0, Lcom/android/systemui/statusbar/NotificationData$Entry;->key:Landroid/os/IBinder;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
