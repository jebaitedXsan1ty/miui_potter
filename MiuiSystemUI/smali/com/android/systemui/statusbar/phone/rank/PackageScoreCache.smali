.class public Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;
.super Ljava/lang/Object;
.source "PackageScoreCache.java"


# static fields
.field private static volatile mPackageScoreCache:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mLooper:Landroid/os/Looper;

.field private mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/systemui/statusbar/phone/rank/RankLruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/systemui/statusbar/phone/rank/PackageEntity;",
            ">;"
        }
    .end annotation
.end field

.field private mUiHandler:Landroid/os/Handler;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;)Lcom/android/systemui/statusbar/phone/rank/RankLruCache;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private constructor <init>(Landroid/os/HandlerThread;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "packageScoreCache"

    const-string/jumbo v1, "init packageScoreCache"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mLooper:Landroid/os/Looper;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mLooper:Landroid/os/Looper;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mHandler:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mUiHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    return-void
.end method

.method public static getInstance()Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;
    .locals 1

    sget-object v0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScoreCache:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;

    return-object v0
.end method

.method public static getInstance(Landroid/os/HandlerThread;)Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;
    .locals 2

    sget-object v0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScoreCache:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;

    if-nez v0, :cond_1

    const-class v1, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScoreCache:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;-><init>(Landroid/os/HandlerThread;)V

    sput-object v0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScoreCache:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScoreCache:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addClick(Ljava/lang/String;)Lcom/android/systemui/statusbar/phone/rank/PackageEntity;
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    invoke-virtual {v1, p1}, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->retrievePackage(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    invoke-virtual {v1, p1}, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;->addClickCount()V

    return-object v0
.end method

.method public addShow(Ljava/lang/String;)Lcom/android/systemui/statusbar/phone/rank/PackageEntity;
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    invoke-virtual {v1, p1}, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->retrievePackage(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    invoke-virtual {v1, p1}, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;->addShowCount()V

    return-object v0
.end method

.method public containsPkg(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPackageScore()Lcom/android/systemui/statusbar/phone/rank/RankLruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/android/systemui/statusbar/phone/rank/RankLruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/systemui/statusbar/phone/rank/PackageEntity;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    return-object v0
.end method

.method public retrievePackage(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mPackageScore:Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    new-instance v1, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

    invoke-direct {v1, p1}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1;

    invoke-direct {v1, p0, p1}, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1;-><init>(Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
