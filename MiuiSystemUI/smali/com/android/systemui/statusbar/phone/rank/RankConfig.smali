.class public Lcom/android/systemui/statusbar/phone/rank/RankConfig;
.super Ljava/lang/Object;
.source "RankConfig.java"


# static fields
.field private static volatile INSTANCE:Lcom/android/systemui/statusbar/phone/rank/RankConfig;

.field public static sNum:I

.field public static sRandomNum:I

.field public static sRankAlogrithmVersion:I

.field public static sSlot:I


# instance fields
.field private mRankList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/systemui/statusbar/phone/rank/IRank;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRankAlogrithmVersion:I

    const/4 v0, 0x0

    sput-object v0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->INSTANCE:Lcom/android/systemui/statusbar/phone/rank/RankConfig;

    return-void
.end method

.method private constructor <init>()V
    .locals 11

    const/4 v10, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    new-instance v8, Lcom/android/systemui/statusbar/phone/rank/GoogleRank;

    invoke-direct {v8}, Lcom/android/systemui/statusbar/phone/rank/GoogleRank;-><init>()V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    new-instance v8, Lcom/android/systemui/statusbar/phone/rank/PackageRank;

    invoke-direct {v8}, Lcom/android/systemui/statusbar/phone/rank/PackageRank;-><init>()V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    new-instance v8, Lcom/android/systemui/statusbar/phone/rank/ClickRank;

    invoke-direct {v8}, Lcom/android/systemui/statusbar/phone/rank/ClickRank;-><init>()V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    new-instance v8, Lcom/android/systemui/statusbar/phone/rank/PushRank;

    invoke-direct {v8}, Lcom/android/systemui/statusbar/phone/rank/PushRank;-><init>()V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    sput v7, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sNum:I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->shouldUpdateRankNum()Z

    move-result v7

    if-eqz v7, :cond_4

    const-string/jumbo v7, "persist.sys.notification_num"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget v9, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sNum:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v7, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v7, :cond_1

    sput v10, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    :goto_0
    const-string/jumbo v7, "persist.sys.notification_rank"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget v9, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v7, "persist.sys.notification_ver"

    const-string/jumbo v8, "1"

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    const-string/jumbo v7, "persist.sys.notification_device"

    const-string/jumbo v8, ""

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    const/16 v7, 0x3e8

    invoke-virtual {v5, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    sput v7, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sSlot:I

    const-string/jumbo v7, "persist.sys.notification_device"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget v9, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sSlot:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_1
    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    const/16 v7, 0x64

    invoke-virtual {v5, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    const-string/jumbo v7, "RankConfig"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "percent:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_3
    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_2

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/systemui/statusbar/phone/rank/IRank;

    invoke-interface {v6}, Lcom/android/systemui/statusbar/phone/rank/IRank;->getPercent()I

    move-result v7

    if-ge v4, v7, :cond_3

    sput v3, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    :cond_2
    const-string/jumbo v7, "RankConfig"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "percent:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " num:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget v9, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    invoke-interface {v6}, Lcom/android/systemui/statusbar/phone/rank/IRank;->getPercent()I

    move-result v7

    sub-int/2addr v4, v7

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    :try_start_0
    const-string/jumbo v7, "persist.sys.notification_rank"

    const-string/jumbo v8, ""

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    sput v7, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    sget v7, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    if-ltz v7, :cond_5

    sget v7, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lt v7, v8, :cond_6

    :cond_5
    :goto_5
    sput v10, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    const-string/jumbo v7, "persist.sys.notification_rank"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget v9, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_0
    move-exception v2

    sput v10, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    goto :goto_4

    :cond_6
    sget-boolean v7, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v7, :cond_0

    sget v7, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    if-eqz v7, :cond_0

    goto :goto_5

    :cond_7
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    sput v7, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sSlot:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v2

    sput v10, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sSlot:I

    goto/16 :goto_2
.end method

.method public static getInstance()Lcom/android/systemui/statusbar/phone/rank/RankConfig;
    .locals 2

    sget-object v0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->INSTANCE:Lcom/android/systemui/statusbar/phone/rank/RankConfig;

    if-nez v0, :cond_1

    const-class v1, Lcom/android/systemui/statusbar/phone/rank/RankConfig;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->INSTANCE:Lcom/android/systemui/statusbar/phone/rank/RankConfig;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;

    invoke-direct {v0}, Lcom/android/systemui/statusbar/phone/rank/RankConfig;-><init>()V

    sput-object v0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->INSTANCE:Lcom/android/systemui/statusbar/phone/rank/RankConfig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->INSTANCE:Lcom/android/systemui/statusbar/phone/rank/RankConfig;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private shouldUpdateRankNum()Z
    .locals 8

    const/4 v7, 0x1

    const-string/jumbo v4, "persist.sys.notification_ver"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "1"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    return v7

    :cond_0
    const-string/jumbo v4, "persist.sys.notification_num"

    const-string/jumbo v5, ""

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    sget v4, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sNum:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v4, :cond_1

    const/4 v4, 0x0

    return v4

    :catch_0
    move-exception v0

    const-string/jumbo v4, "RankConfig"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "error :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v7
.end method

.method public static updateConfig(I)V
    .locals 4

    invoke-static {}, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->getInstance()Lcom/android/systemui/statusbar/phone/rank/RankConfig;

    move-result-object v0

    if-ltz p0, :cond_0

    sget v1, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sNum:I

    if-lt p0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    sput p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    const-string/jumbo v1, "persist.sys.notification_rank"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getRankAlogrithm()Lcom/android/systemui/statusbar/phone/rank/IRank;
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->mRankList:Ljava/util/List;

    sget v1, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/rank/IRank;

    return-object v0
.end method
