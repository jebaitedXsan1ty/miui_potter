.class public Lcom/android/systemui/statusbar/phone/rank/PushRank;
.super Ljava/lang/Object;
.source "PushRank.java"

# interfaces
.implements Lcom/android/systemui/statusbar/phone/rank/IRank;


# instance fields
.field googleRank:Lcom/android/systemui/statusbar/phone/rank/GoogleRank;

.field packageRank:Lcom/android/systemui/statusbar/phone/rank/PackageRank;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/systemui/statusbar/phone/rank/PackageRank;

    invoke-direct {v0}, Lcom/android/systemui/statusbar/phone/rank/PackageRank;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PushRank;->packageRank:Lcom/android/systemui/statusbar/phone/rank/PackageRank;

    new-instance v0, Lcom/android/systemui/statusbar/phone/rank/GoogleRank;

    invoke-direct {v0}, Lcom/android/systemui/statusbar/phone/rank/GoogleRank;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/rank/PushRank;->googleRank:Lcom/android/systemui/statusbar/phone/rank/GoogleRank;

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I
    .locals 12

    const/4 v4, 0x1

    const/4 v5, -0x1

    const-wide/16 v10, 0x0

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/rank/PushRank;->packageRank:Lcom/android/systemui/statusbar/phone/rank/PackageRank;

    iget-object v7, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v6, v7}, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->getScore(Lcom/android/systemui/statusbar/ExpandedNotification;)D

    move-result-wide v6

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/rank/PushRank;->packageRank:Lcom/android/systemui/statusbar/phone/rank/PackageRank;

    iget-object v9, p2, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v8, v9}, Lcom/android/systemui/statusbar/phone/rank/PackageRank;->getScore(Lcom/android/systemui/statusbar/ExpandedNotification;)D

    move-result-wide v8

    sub-double v2, v6, v8

    cmpl-double v6, v2, v10

    if-eqz v6, :cond_1

    cmpl-double v6, v2, v10

    if-lez v6, :cond_0

    :goto_0
    return v4

    :cond_0
    move v4, v5

    goto :goto_0

    :cond_1
    iget-object v6, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v6}, Lcom/android/systemui/statusbar/ExpandedNotification;->getScoreForRank()D

    move-result-wide v6

    iget-object v8, p2, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v8}, Lcom/android/systemui/statusbar/ExpandedNotification;->getScoreForRank()D

    move-result-wide v8

    sub-double v0, v6, v8

    cmpl-double v6, v0, v10

    if-eqz v6, :cond_3

    cmpl-double v6, v0, v10

    if-lez v6, :cond_2

    :goto_1
    return v4

    :cond_2
    move v4, v5

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/rank/PushRank;->googleRank:Lcom/android/systemui/statusbar/phone/rank/GoogleRank;

    invoke-virtual {v4, p1, p2}, Lcom/android/systemui/statusbar/phone/rank/GoogleRank;->compare(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I

    move-result v4

    return v4
.end method

.method public getPercent()I
    .locals 1

    const/16 v0, 0x5a

    return v0
.end method
