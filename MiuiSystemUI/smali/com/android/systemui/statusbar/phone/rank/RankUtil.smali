.class public Lcom/android/systemui/statusbar/phone/rank/RankUtil;
.super Ljava/lang/Object;
.source "RankUtil.java"


# static fields
.field public static UNFLOD_LIMIT:I

.field public static sGap:J

.field public static sNewNotification:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/32 v0, 0x6ddd00

    sput-wide v0, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->sGap:J

    const-wide/16 v0, 0x2710

    sput-wide v0, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->sNewNotification:J

    const/4 v0, 0x3

    sput v0, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->UNFLOD_LIMIT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compareByTime(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I
    .locals 14

    const/4 v6, 0x1

    const/4 v7, -0x1

    const-wide/16 v12, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    iget-object v3, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v8

    iget-wide v8, v8, Landroid/app/Notification;->when:J

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v10

    iget-wide v10, v10, Landroid/app/Notification;->when:J

    sub-long v4, v8, v10

    iget-wide v8, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->firstWhen:J

    iget-wide v10, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->firstWhen:J

    sub-long v0, v8, v10

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/16 v10, 0x2710

    cmp-long v8, v8, v10

    if-lez v8, :cond_2

    cmp-long v8, v4, v12

    if-lez v8, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    move v6, v7

    goto :goto_0

    :cond_2
    cmp-long v8, v0, v12

    if-eqz v8, :cond_3

    cmp-long v8, v0, v12

    if-gtz v8, :cond_0

    move v6, v7

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->key:Landroid/os/IBinder;

    invoke-virtual {v6}, Ljava/lang/Object;->hashCode()I

    move-result v6

    iget-object v7, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->key:Landroid/os/IBinder;

    invoke-virtual {v7}, Ljava/lang/Object;->hashCode()I

    move-result v7

    sub-int/2addr v6, v7

    goto :goto_0
.end method

.method public static getGapSize(Lcom/android/systemui/statusbar/NotificationData$Entry;JJ)J
    .locals 7

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    iget-wide v2, v1, Landroid/app/Notification;->when:J

    sub-long v4, p1, v2

    div-long/2addr v4, p3

    return-wide v4
.end method
