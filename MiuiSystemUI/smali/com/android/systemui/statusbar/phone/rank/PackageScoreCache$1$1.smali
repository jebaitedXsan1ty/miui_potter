.class Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1$1;
.super Ljava/lang/Object;
.source "PackageScoreCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1;

.field final synthetic val$packageEntityFromDb:Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1;Ljava/lang/String;Lcom/android/systemui/statusbar/phone/rank/PackageEntity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1$1;->this$1:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1;

    iput-object p2, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1$1;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1$1;->val$packageEntityFromDb:Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1$1;->this$1:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1;->this$0:Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;->-get0(Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache;)Lcom/android/systemui/statusbar/phone/rank/RankLruCache;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1$1;->val$packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/phone/rank/RankLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1$1;->val$packageEntityFromDb:Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;->getClickMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;->setClickMap(Ljava/util/Map;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1$1;->val$packageEntityFromDb:Lcom/android/systemui/statusbar/phone/rank/PackageEntity;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;->getShowMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;->setShowMap(Ljava/util/Map;)V

    const-string/jumbo v1, "packageScoreCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "retrievePackage end:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/rank/PackageScoreCache$1$1;->val$packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ",showCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;->getShowSum()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ",clickCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/rank/PackageEntity;->getClickSum()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
