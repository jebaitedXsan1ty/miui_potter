.class public Lcom/android/systemui/statusbar/phone/rank/ClickRank;
.super Ljava/lang/Object;
.source "ClickRank.java"

# interfaces
.implements Lcom/android/systemui/statusbar/phone/rank/IRank;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I
    .locals 8

    iget-object v4, p1, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {p0, v4}, Lcom/android/systemui/statusbar/phone/rank/ClickRank;->getScore(Lcom/android/systemui/statusbar/ExpandedNotification;)D

    move-result-wide v0

    iget-object v4, p2, Lcom/android/systemui/statusbar/NotificationData$Entry;->notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-virtual {p0, v4}, Lcom/android/systemui/statusbar/phone/rank/ClickRank;->getScore(Lcom/android/systemui/statusbar/ExpandedNotification;)D

    move-result-wide v2

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_1

    sub-double v4, v0, v2

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, -0x1

    goto :goto_0

    :cond_1
    invoke-static {p1, p2}, Lcom/android/systemui/statusbar/phone/rank/RankUtil;->compareByTime(Lcom/android/systemui/statusbar/NotificationData$Entry;Lcom/android/systemui/statusbar/NotificationData$Entry;)I

    move-result v4

    return v4
.end method

.method public getPercent()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public getScore(Lcom/android/systemui/statusbar/ExpandedNotification;)D
    .locals 4

    invoke-static {p1}, Lcom/android/systemui/statusbar/NotificationSortHelper;->getNotificationPackageScore(Lcom/android/systemui/statusbar/ExpandedNotification;)I

    move-result v0

    int-to-double v0, v0

    const-wide/16 v2, 0x0

    add-double/2addr v0, v2

    return-wide v0
.end method
