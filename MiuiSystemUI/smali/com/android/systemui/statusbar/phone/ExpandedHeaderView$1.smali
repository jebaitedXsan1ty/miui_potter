.class Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$1;
.super Ljava/lang/Object;
.source "ExpandedHeaderView.java"

# interfaces
.implements Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$1;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onColorChanged(Lcom/android/systemui/statusbar/ColorSuits;)V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$1;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-static {v1, p1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->-set0(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;Lcom/android/systemui/statusbar/ColorSuits;)Lcom/android/systemui/statusbar/ColorSuits;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$1;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v1

    iget-boolean v1, v1, Lcom/android/systemui/statusbar/phone/PanelView;->mIsDetailOpened:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$1;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v1

    iget-boolean v0, v1, Lcom/android/systemui/statusbar/phone/PanelView;->mIsFoldOpened:Z

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$1;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->updateBackground(ZZ)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
