.class public Lcom/android/systemui/statusbar/phone/SnapScrollView;
.super Landroid/widget/FrameLayout;
.source "SnapScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;
    }
.end annotation


# static fields
.field private static sDragCount:I

.field private static sHintColor:I

.field private static sNeedShowToggleScrollbarHint:Z


# instance fields
.field private mActivePointerId:I

.field private mChildToScrollTo:Landroid/view/View;

.field private mEdgeGlowBottom:Landroid/widget/EdgeEffect;

.field private mEdgeGlowTop:Landroid/widget/EdgeEffect;

.field private mIsBeingDragged:Z

.field private mIsLayoutDirty:Z

.field private mItemHeight:I

.field private mLaidOut:Z

.field private mLastMotionY:I

.field private mLastScroll:J

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mOverflingDistance:I

.field private mOverscrollDistance:I

.field private mOvershootTension:F

.field private mScrollInterpolator:Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;

.field private mScroller:Landroid/widget/OverScroller;

.field private mSmoothScrollingEnabled:Z

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/SnapScrollView;)F
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mOvershootTension:F

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sHintColor:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sNeedShowToggleScrollbarHint:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsLayoutDirty:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mChildToScrollTo:Landroid/view/View;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mSmoothScrollingEnabled:Z

    iput v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mOvershootTension:F

    iput v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mItemHeight:I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->initScrollView()V

    return-void
.end method

.method private static clamp(III)I
    .locals 1

    const/4 v0, 0x0

    if-ge p1, p2, :cond_0

    if-gez p0, :cond_1

    :cond_0
    return v0

    :cond_1
    add-int v0, p1, p0

    if-le v0, p2, :cond_2

    sub-int v0, p2, p1

    return v0

    :cond_2
    return p0
.end method

.method private doScrollY(I)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mSmoothScrollingEnabled:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1, p1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->smoothScrollBy(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v1, p1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollBy(II)V

    goto :goto_0
.end method

.method private endDrag()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->recycleVelocityTracker()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_0
    return-void
.end method

.method private getScrollRange()I
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingTop:I

    sub-int/2addr v3, v4

    sub-int/2addr v2, v3

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_0
    return v1
.end method

.method private inChild(II)Z
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v3

    if-lez v3, :cond_1

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int/2addr v3, v1

    if-lt p2, v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v3

    sub-int/2addr v3, v1

    if-ge p2, v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt p1, v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge p1, v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    return v2
.end method

.method private initOrResetVelocityTracker()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0
.end method

.method private initScrollView()V
    .locals 6

    const/4 v5, 0x1

    new-instance v2, Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;-><init>(Lcom/android/systemui/statusbar/phone/SnapScrollView;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollInterpolator:Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;

    new-instance v2, Landroid/widget/OverScroller;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollInterpolator:Lcom/android/systemui/statusbar/phone/SnapScrollView$ScreenViewOvershootInterpolator;

    invoke-direct {v2, v3, v4}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->setFocusable(Z)V

    const/high16 v2, 0x40000

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->setDescendantFocusability(I)V

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->setWillNotDraw(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mMinimumVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mMaximumVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mOverscrollDistance:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    move-result v2

    iput v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mOverflingDistance:I

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "pref_need_show_toggle_scrollbar_hint"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sNeedShowToggleScrollbarHint:Z

    return-void
.end method

.method private initVelocityTrackerIfNotExists()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method

.method private isOffScreen(Landroid/view/View;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->isWithinDeltaOfScreen(Landroid/view/View;II)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private static isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isWithinDeltaOfScreen(Landroid/view/View;II)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, p2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollY()I

    move-result v2

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, p2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollY()I

    move-result v2

    add-int/2addr v2, p3

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const v4, 0xff00

    and-int/2addr v3, v4

    shr-int/lit8 v2, v3, 0x8

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    if-ne v1, v3, :cond_0

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastMotionY:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    iput v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private recycleVelocityTracker()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method

.method private scrollToChild(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2, v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollBy(II)V

    :cond_0
    return-void
.end method

.method private scrollToChildRect(Landroid/graphics/Rect;Z)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    if-eqz p2, :cond_2

    invoke-virtual {p0, v2, v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollBy(II)V

    :cond_0
    :goto_1
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v2, v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->smoothScrollBy(II)V

    goto :goto_1
.end method

.method private setNoNeedShowScrollBarHintIfNeeded()V
    .locals 4

    const/4 v3, 0x0

    sget v1, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sDragCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sDragCount:I

    sget-boolean v1, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sNeedShowToggleScrollbarHint:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sDragCount:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-boolean v3, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sNeedShowToggleScrollbarHint:Z

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "pref_need_show_toggle_scrollbar_hint"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method private springBack()V
    .locals 10

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v6

    iget v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mItemHeight:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    int-to-double v0, v0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v8

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mItemHeight:I

    int-to-double v8, v2

    div-double/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mItemHeight:I

    mul-int v7, v0, v1

    if-gez v7, :cond_0

    const/4 v7, 0x0

    :cond_0
    if-le v7, v6, :cond_1

    move v7, v6

    :cond_1
    move v6, v7

    move v5, v7

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    move v4, v3

    invoke-virtual/range {v0 .. v6}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->postInvalidateOnAnimation()V

    :cond_3
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "SnapScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "SnapScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "SnapScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "SnapScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public computeScroll()V
    .locals 14

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v12

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v13

    if-ne v3, v12, :cond_0

    if-eq v4, v13, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getOverScrollMode()I

    move-result v11

    if-eqz v11, :cond_3

    const/4 v0, 0x1

    if-ne v11, v0, :cond_4

    if-lez v6, :cond_4

    const/4 v10, 0x1

    :goto_0
    sub-int v1, v12, v3

    sub-int v2, v13, v4

    iget v8, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mOverflingDistance:I

    move-object v0, p0

    move v7, v5

    move v9, v5

    invoke-virtual/range {v0 .. v9}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->overScrollBy(IIIIIIIIZ)Z

    iget v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->onScrollChanged(IIII)V

    if-eqz v10, :cond_1

    if-gez v13, :cond_5

    if-ltz v4, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->awakenScrollBars()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->postInvalidateOnAnimation()V

    :cond_2
    return-void

    :cond_3
    const/4 v10, 0x1

    goto :goto_0

    :cond_4
    const/4 v10, 0x0

    goto :goto_0

    :cond_5
    if-le v13, v6, :cond_1

    if-gt v4, v6, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrVelocity()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    goto :goto_1
.end method

.method protected computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I
    .locals 10

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v7

    if-nez v7, :cond_0

    return v9

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollY()I

    move-result v5

    add-int v4, v5, v3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getVerticalFadingEdgeLength()I

    move-result v2

    iget v7, p1, Landroid/graphics/Rect;->top:I

    if-lez v7, :cond_1

    add-int/2addr v5, v2

    :cond_1
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v9}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v8

    if-ge v7, v8, :cond_2

    sub-int/2addr v4, v2

    :cond_2
    const/4 v6, 0x0

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    if-le v7, v4, :cond_5

    iget v7, p1, Landroid/graphics/Rect;->top:I

    if-le v7, v5, :cond_5

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    if-le v7, v3, :cond_4

    iget v7, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v5

    add-int/lit8 v6, v7, 0x0

    :goto_0
    invoke-virtual {p0, v9}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v0

    sub-int v1, v0, v4

    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    :cond_3
    :goto_1
    return v6

    :cond_4
    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v4

    add-int/lit8 v6, v7, 0x0

    goto :goto_0

    :cond_5
    iget v7, p1, Landroid/graphics/Rect;->top:I

    if-ge v7, v5, :cond_3

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    if-ge v7, v4, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    if-le v7, v3, :cond_6

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    sub-int v7, v4, v7

    rsub-int/lit8 v6, v7, 0x0

    :goto_2
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollY()I

    move-result v7

    neg-int v7, v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    goto :goto_1

    :cond_6
    iget v7, p1, Landroid/graphics/Rect;->top:I

    sub-int v7, v5, v7

    rsub-int/lit8 v6, v7, 0x0

    goto :goto_2
.end method

.method protected computeVerticalScrollOffset()I
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->computeVerticalScrollOffset()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected computeVerticalScrollRange()I
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v5

    iget v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingTop:I

    sub-int v0, v5, v6

    if-nez v1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0, v7}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    sub-int v5, v3, v0

    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-gez v4, :cond_2

    sub-int/2addr v3, v4

    :cond_1
    :goto_0
    return v3

    :cond_2
    if-le v4, v2, :cond_1

    sub-int v5, v4, v2

    add-int/2addr v3, v5

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    if-eqz v4, :cond_3

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v4}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingLeft:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingRight:I

    sub-int v3, v4, v5

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingLeft:I

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v5

    invoke-virtual {v4, v3, v5}, Landroid/widget/EdgeEffect;->setSize(II)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->postInvalidateOnAnimation()V

    :cond_0
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v4}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingLeft:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingRight:I

    sub-int v3, v4, v5

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v0

    neg-int v4, v3

    iget v5, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingLeft:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v5

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int/2addr v5, v0

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v4, 0x43340000    # 180.0f

    int-to-float v5, v3

    const/4 v6, 0x0

    invoke-virtual {p1, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v4, v3, v0}, Landroid/widget/EdgeEffect;->setSize(II)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v4, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->postInvalidateOnAnimation()V

    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_3
    return-void
.end method

.method public fling(I)V
    .locals 14

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mItemHeight:I

    if-gtz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingTop:I

    sub-int v12, v0, v1

    invoke-virtual {p0, v3}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v11

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    sub-int v4, v11, v12

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v8

    div-int/lit8 v10, v12, 0x2

    move v4, p1

    move v5, v3

    move v6, v3

    move v7, v3

    move v9, v3

    invoke-virtual/range {v0 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->postInvalidateOnAnimation()V

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mItemHeight:I

    div-int/2addr v0, v1

    div-int/lit16 v1, p1, 0x7d0

    add-int v13, v0, v1

    invoke-static {v3, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mItemHeight:I

    div-int/2addr v0, v1

    invoke-static {v13, v0}, Ljava/lang/Math;->min(II)I

    move-result v13

    iget v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mItemHeight:I

    mul-int/2addr v13, v0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    sub-int v4, v13, v4

    const/16 v5, 0x1f4

    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    goto :goto_0
.end method

.method protected getBottomFadingEdgeStrength()F
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    return v3

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getVerticalFadingEdgeLength()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int v0, v3, v4

    invoke-virtual {p0, v5}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    sub-int/2addr v3, v4

    sub-int v2, v3, v0

    if-ge v2, v1, :cond_1

    int-to-float v3, v2

    int-to-float v4, v1

    div-float/2addr v3, v4

    return v3

    :cond_1
    const/high16 v3, 0x3f800000    # 1.0f

    return v3
.end method

.method protected getTopFadingEdgeStrength()F
    .locals 3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getVerticalFadingEdgeLength()I

    move-result v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    if-ge v1, v0, :cond_1

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    int-to-float v1, v1

    int-to-float v2, v0

    div-float/2addr v1, v2

    return v1

    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    return v1
.end method

.method protected measureChild(Landroid/view/View;II)V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingLeft:I

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingRight:I

    add-int/2addr v3, v4

    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p2, v3, v4}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildMeasureSpec(III)I

    move-result v1

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingLeft:I

    iget v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingRight:I

    add-int/2addr v3, v4

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    add-int/2addr v3, p3

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-static {p2, v3, v4}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildMeasureSpec(III)I

    move-result v1

    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v3, v4

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLaidOut:Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLaidOut:Z

    return-void
.end method

.method protected onDrawVerticalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V
    .locals 2

    sget v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sHintColor:I

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p2, v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-super/range {p0 .. p6}, Landroid/widget/FrameLayout;->onDrawVerticalScrollBar(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;IIII)V

    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    return v5

    :pswitch_0
    iget-boolean v5, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    if-nez v5, :cond_0

    const/16 v5, 0x9

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v5, v4, v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getVerticalScrollFactor()F

    move-result v5

    mul-float/2addr v5, v4

    float-to-int v0, v5

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v3

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    sub-int v1, v2, v0

    if-gez v1, :cond_2

    const/4 v1, 0x0

    :cond_1
    :goto_0
    if-eq v1, v2, :cond_0

    iget v5, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    invoke-super {p0, v5, v1}, Landroid/widget/FrameLayout;->scrollTo(II)V

    const/4 v5, 0x1

    return v5

    :cond_2
    if-le v1, v3, :cond_1

    move v1, v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v1, Lcom/android/systemui/statusbar/phone/SnapScrollView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setScrollX(I)V

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollX(I)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v1, Lcom/android/systemui/statusbar/phone/SnapScrollView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    if-lez v1, :cond_0

    const/16 v1, 0x2000

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    :cond_0
    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    if-ge v1, v0, :cond_1

    const/16 v1, 0x1000

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v6, 0x2

    if-ne v0, v6, :cond_0

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    if-eqz v6, :cond_0

    return v8

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollY()I

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {p0, v8}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->canScrollVertically(I)Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_1

    return v7

    :cond_1
    and-int/lit16 v6, v0, 0xff

    packed-switch v6, :pswitch_data_0

    :cond_2
    :goto_0
    :pswitch_0
    iget-boolean v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    return v6

    :pswitch_1
    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    if-eq v1, v9, :cond_2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    if-ne v3, v9, :cond_3

    const-string/jumbo v6, "SnapScrollView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Invalid pointerId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " in onInterceptTouchEvent"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    float-to-int v4, v6

    iget v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastMotionY:I

    sub-int v6, v4, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v5

    iget v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTouchSlop:I

    if-le v5, v6, :cond_2

    iput-boolean v8, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    iput v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastMotionY:I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->initVelocityTrackerIfNotExists()V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v4, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    invoke-direct {p0, v6, v4}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->inChild(II)Z

    move-result v6

    if-nez v6, :cond_4

    iput-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->recycleVelocityTracker()V

    goto :goto_0

    :cond_4
    iput v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastMotionY:I

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    iput v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->initOrResetVelocityTracker()V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v6}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    iput-boolean v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    goto/16 :goto_0

    :pswitch_3
    iput-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    iput v9, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->recycleVelocityTracker()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->springBack()V

    goto/16 :goto_0

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    iput-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsLayoutDirty:Z

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mChildToScrollTo:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mChildToScrollTo:Landroid/view/View;

    invoke-static {v2, p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->isViewDescendantOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mChildToScrollTo:Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollToChild(Landroid/view/View;)V

    :cond_0
    iput-object v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mChildToScrollTo:Landroid/view/View;

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLaidOut:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLaidOut:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {p0, v4}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_0
    sub-int v2, p5, p3

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingTop:I

    sub-int/2addr v2, v3

    sub-int v2, v0, v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    if-le v2, v1, :cond_3

    iput v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    :cond_1
    :goto_1
    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    invoke-virtual {p0, v2, v3}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollTo(II)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    if-gez v2, :cond_1

    iput v4, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 8

    const/4 v7, 0x0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    if-nez v4, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v6

    if-lez v6, :cond_1

    invoke-virtual {p0, v7}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    if-ge v6, v3, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    iget v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingLeft:I

    iget v7, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingRight:I

    add-int/2addr v6, v7

    iget v7, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-static {p1, v6, v7}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildMeasureSpec(III)I

    move-result v2

    iget v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingTop:I

    sub-int/2addr v3, v6

    iget v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int/2addr v3, v6

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    :cond_1
    return-void
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 9

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    iget v7, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iget v8, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    iput p1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iput p2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->invalidateParentIfNeeded()V

    iget v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    invoke-virtual {p0, v0, v1, v7, v8}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->onScrollChanged(IIII)V

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v6

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v6}, Landroid/widget/OverScroller;->springBack(IIIIII)Z

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->awakenScrollBars()Z

    return-void

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    goto :goto_0
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    const/16 p1, 0x82

    :cond_0
    :goto_0
    if-nez p2, :cond_2

    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, v2, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_3

    return v3

    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const/16 p1, 0x21

    goto :goto_0

    :cond_2
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, p2, p1}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->isOffScreen(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_4

    return v3

    :cond_4
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v1

    return v1
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ne p0, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2, p4}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->isWithinDeltaOfScreen(Landroid/view/View;II)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->computeScrollDeltaToGetChildRectOnScreen(Landroid/graphics/Rect;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->doScrollY(I)V

    :cond_2
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 24

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->initVelocityTrackerIfNotExists()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v12

    and-int/lit16 v2, v12, 0xff

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v2, 0x1

    return v2

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    return v2

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    if-eqz v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v20

    if-eqz v20, :cond_2

    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->abortAnimation()V

    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastMotionY:I

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    goto :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v13

    const/4 v2, -0x1

    if-ne v13, v2, :cond_4

    const-string/jumbo v2, "SnapScrollView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Invalid pointerId="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, " in onTouchEvent"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-int v0, v2

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastMotionY:I

    sub-int v4, v2, v23

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    if-nez v2, :cond_6

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTouchSlop:I

    if-le v2, v3, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v20

    if-eqz v20, :cond_5

    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_5
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    if-lez v4, :cond_a

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTouchSlop:I

    sub-int/2addr v4, v2

    :cond_6
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    if-eqz v2, :cond_0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastMotionY:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    move/from16 v18, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getOverScrollMode()I

    move-result v19

    if-eqz v19, :cond_b

    const/4 v2, 0x1

    move/from16 v0, v19

    if-ne v0, v2, :cond_c

    if-lez v8, :cond_c

    const/4 v14, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mOverscrollDistance:I

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->overScrollBy(IIIIIIIIZ)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    :cond_7
    if-eqz v14, :cond_0

    add-int v21, v18, v4

    if-gez v21, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    int-to-float v3, v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v3, v5

    invoke-virtual {v2, v3}, Landroid/widget/EdgeEffect;->onPull(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    :cond_8
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->postInvalidateOnAnimation()V

    goto/16 :goto_0

    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mTouchSlop:I

    add-int/2addr v4, v2

    goto/16 :goto_1

    :cond_b
    const/4 v14, 0x1

    goto :goto_2

    :cond_c
    const/4 v14, 0x0

    goto :goto_2

    :cond_d
    move/from16 v0, v21

    if-le v0, v8, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    int-to-float v3, v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v3, v5

    invoke-virtual {v2, v3}, Landroid/widget/EdgeEffect;->onPull(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->onRelease()V

    goto :goto_3

    :pswitch_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    if-eqz v2, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->setNoNeedShowScrollBarHintIfNeeded()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mMaximumVelocity:I

    int-to-float v2, v2

    const/16 v3, 0x3e8

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v2

    float-to-int v0, v2

    move/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v2

    if-lez v2, :cond_e

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(I)I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mMinimumVelocity:I

    if-le v2, v3, :cond_f

    move/from16 v0, v16

    neg-int v2, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->fling(I)V

    :cond_e
    :goto_4
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->endDrag()V

    goto/16 :goto_0

    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->springBack()V

    goto :goto_4

    :pswitch_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsBeingDragged:Z

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v2

    if-lez v2, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->springBack()V

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    invoke-direct/range {p0 .. p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->endDrag()V

    goto/16 :goto_0

    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastMotionY:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    goto/16 :goto_0

    :pswitch_6
    invoke-direct/range {p0 .. p1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastMotionY:I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v5

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    return v4

    :cond_1
    sparse-switch p1, :sswitch_data_0

    return v4

    :sswitch_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingTop:I

    sub-int v1, v2, v3

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    add-int/2addr v2, v1

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getScrollRange()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    if-eq v0, v2, :cond_2

    invoke-virtual {p0, v4, v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->smoothScrollTo(II)V

    return v5

    :cond_2
    return v4

    :sswitch_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingTop:I

    sub-int v1, v2, v3

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    sub-int/2addr v2, v1

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    if-eq v0, v2, :cond_3

    invoke-virtual {p0, v4, v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->smoothScrollTo(II)V

    return v5

    :cond_3
    return v4

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
    .end sparse-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsLayoutDirty:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollToChild(Landroid/view/View;)V

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    return-void

    :cond_0
    iput-object p2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mChildToScrollTo:Landroid/view/View;

    goto :goto_0
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    invoke-direct {p0, p2, p3}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollToChildRect(Landroid/graphics/Rect;Z)Z

    move-result v0

    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->recycleVelocityTracker()V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    return-void
.end method

.method public requestLayout()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mIsLayoutDirty:Z

    invoke-super {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    return-void
.end method

.method public scrollTo(II)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingRight:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingLeft:I

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-static {p1, v1, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->clamp(III)I

    move-result p1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingTop:I

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-static {p2, v1, v2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->clamp(III)I

    move-result p2

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    if-ne p1, v1, :cond_0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    if-eq p2, v1, :cond_1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    :cond_1
    return-void
.end method

.method public setItemHeight(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mItemHeight:I

    return-void
.end method

.method public setOverScrollMode(I)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/widget/EdgeEffect;

    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    new-instance v1, Landroid/widget/EdgeEffect;

    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOverScrollMode(I)V

    return-void

    :cond_1
    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowTop:Landroid/widget/EdgeEffect;

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mEdgeGlowBottom:Landroid/widget/EdgeEffect;

    goto :goto_0
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public showScrollBarHint()V
    .locals 1

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sNeedShowToggleScrollbarHint:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    sput v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sHintColor:I

    :goto_0
    const/16 v0, 0x4b0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->awakenScrollBars(I)Z

    return-void

    :cond_0
    const v0, 0x40ffffff    # 7.9999995f

    sput v0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->sHintColor:I

    goto :goto_0
.end method

.method public final smoothScrollBy(II)V
    .locals 11

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildCount()I

    move-result v6

    if-nez v6, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastScroll:J

    sub-long v2, v6, v8

    const-wide/16 v6, 0xfa

    cmp-long v6, v2, v6

    if-lez v6, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getHeight()I

    move-result v6

    iget v7, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingBottom:I

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mPaddingTop:I

    sub-int v1, v6, v7

    invoke-virtual {p0, v10}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int v6, v0, v1

    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v5, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    add-int v6, v5, p2

    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-static {v10, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    sub-int p2, v6, v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    iget v7, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    invoke-virtual {v6, v7, v5, v10, p2}, Landroid/widget/OverScroller;->startScroll(IIII)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->postInvalidateOnAnimation()V

    :goto_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mLastScroll:J

    return-void

    :cond_1
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v6}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScroller:Landroid/widget/OverScroller;

    invoke-virtual {v6}, Landroid/widget/OverScroller;->abortAnimation()V

    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->scrollBy(II)V

    goto :goto_0
.end method

.method public final smoothScrollTo(II)V
    .locals 2

    iget v0, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollX:I

    sub-int v0, p1, v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/SnapScrollView;->mScrollY:I

    sub-int v1, p2, v1

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/phone/SnapScrollView;->smoothScrollBy(II)V

    return-void
.end method
