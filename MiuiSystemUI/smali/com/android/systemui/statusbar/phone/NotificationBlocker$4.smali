.class Lcom/android/systemui/statusbar/phone/NotificationBlocker$4;
.super Ljava/lang/Object;
.source "NotificationBlocker.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/NotificationBlocker;->updateBlockerLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/NotificationBlocker;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/NotificationBlocker;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker$4;->this$0:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker$4;->this$0:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationBlocker$4;->this$0:Lcom/android/systemui/statusbar/phone/NotificationBlocker;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/NotificationBlocker;->requestLayout()V

    return-void
.end method
