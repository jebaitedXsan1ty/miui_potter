.class public Lcom/android/systemui/statusbar/phone/NotificationsView;
.super Landroid/widget/LinearLayout;
.source "NotificationsView.java"


# instance fields
.field private mScrollView:Landroid/widget/ScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0f0189

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationsView;->mScrollView:Landroid/widget/ScrollView;

    return-void
.end method
