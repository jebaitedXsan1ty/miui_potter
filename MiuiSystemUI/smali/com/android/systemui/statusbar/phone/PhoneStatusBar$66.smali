.class Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;
.super Ljava/lang/Object;
.source "PhoneStatusBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->clearAllNotification()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field final synthetic val$clearableViews:Ljava/util/ArrayList;

.field final synthetic val$list:Landroid/view/ViewGroup;

.field final synthetic val$sc:Lcom/android/systemui/SwipeHelper$Callback;

.field final synthetic val$snapshot:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;Lcom/android/systemui/SwipeHelper$Callback;Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iput-object p2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$sc:Lcom/android/systemui/SwipeHelper$Callback;

    iput-object p3, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$snapshot:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$clearableViews:Ljava/util/ArrayList;

    iput-object p5, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$list:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const/4 v12, 0x0

    const/16 v0, 0xa

    const/16 v3, 0x8c

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$sc:Lcom/android/systemui/SwipeHelper$Callback;

    invoke-interface {v8, v12}, Lcom/android/systemui/SwipeHelper$Callback;->setViewRemoval(Z)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    new-instance v9, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$sc:Lcom/android/systemui/SwipeHelper$Callback;

    iget-object v11, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$clearableViews:Ljava/util/ArrayList;

    invoke-direct {v9, p0, v10, v11}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$1;-><init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;Lcom/android/systemui/SwipeHelper$Callback;Ljava/util/ArrayList;)V

    iput-object v9, v8, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mPostCollapseCleanup:Ljava/lang/Runnable;

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$snapshot:Ljava/util/ArrayList;

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v7

    mul-int/lit8 v6, v7, 0x8

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$snapshot:Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-static {v8}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->-get14(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)Lcom/android/systemui/statusbar/BaseStatusBar$H;

    move-result-object v8

    new-instance v9, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$2;

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$list:Landroid/view/ViewGroup;

    invoke-direct {v9, p0, v10, v1, v6}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$2;-><init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;Landroid/view/ViewGroup;Landroid/view/View;I)V

    int-to-long v10, v5

    invoke-virtual {v8, v9, v10, v11}, Lcom/android/systemui/statusbar/BaseStatusBar$H;->postDelayed(Ljava/lang/Runnable;J)Z

    add-int/lit8 v8, v3, -0xa

    const/16 v9, 0x32

    invoke-static {v9, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/2addr v5, v3

    goto :goto_0

    :cond_0
    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-static {v8}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->-get14(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)Lcom/android/systemui/statusbar/BaseStatusBar$H;

    move-result-object v8

    new-instance v9, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$3;

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;->val$clearableViews:Ljava/util/ArrayList;

    invoke-direct {v9, p0, v10}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66$3;-><init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar$66;Ljava/util/ArrayList;)V

    add-int/lit16 v10, v5, 0xe1

    int-to-long v10, v10

    invoke-virtual {v8, v9, v10, v11}, Lcom/android/systemui/statusbar/BaseStatusBar$H;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
