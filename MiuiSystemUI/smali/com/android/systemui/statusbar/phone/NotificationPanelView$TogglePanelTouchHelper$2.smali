.class Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "NotificationPanelView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->fling(FZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mExpandableTogglesLayoutHeight:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mTogglePanelExpanded:Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->requestLayout()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mExpandableTogglesLayoutHeight:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mTogglePanelExpanded:Z

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mTogglePanelExpandAdjusting:Z

    return-void
.end method
