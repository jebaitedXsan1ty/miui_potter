.class public Lcom/android/systemui/statusbar/phone/FlowStatusBar;
.super Landroid/widget/FrameLayout;
.source "FlowStatusBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;,
        Lcom/android/systemui/statusbar/phone/FlowStatusBar$2;,
        Lcom/android/systemui/statusbar/phone/FlowStatusBar$3;,
        Lcom/android/systemui/statusbar/phone/FlowStatusBar$4;,
        Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;
    }
.end annotation


# instance fields
.field private mAction1:Ljava/lang/String;

.field private mAction2:Ljava/lang/String;

.field private mBgHandler:Landroid/os/Handler;

.field private mBgThread:Landroid/os/HandlerThread;

.field private mCharSequence1:Ljava/lang/CharSequence;

.field private mContent:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private mDataUsageAvaliable:Z

.field private mEnabled:Z

.field mFlowClickListener:Landroid/view/View$OnClickListener;

.field private mFlowContent:Landroid/widget/TextView;

.field private mFlowPurchase:Landroid/widget/TextView;

.field private final mFlowStatusShowObserver:Landroid/database/ContentObserver;

.field private mHandler:Landroid/os/Handler;

.field private mIcon:Landroid/graphics/Bitmap;

.field private mIconUri:Ljava/lang/String;

.field private mIntent1:Landroid/content/Intent;

.field private mIntent2:Landroid/content/Intent;

.field private mLastVisible:Z

.field private mNeedPurchase:Z

.field private mNetworkUri:Landroid/net/Uri;

.field private mPieView:Landroid/widget/ImageView;

.field private mPortrait:Z

.field mPurchaseClickListener:Landroid/view/View$OnClickListener;

.field private final mResolver:Landroid/content/ContentResolver;

.field mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mText1:Ljava/lang/String;

.field private mText2:Ljava/lang/String;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mDataUsageAvaliable:Z

    return v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mEnabled:Z

    return v0
.end method

.method static synthetic -get3(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIntent1:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIntent2:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mLastVisible:Z

    return v0
.end method

.method static synthetic -get7(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mPortrait:Z

    return v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/phone/FlowStatusBar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mEnabled:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/systemui/statusbar/phone/FlowStatusBar;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mLastVisible:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->readDataUsageDetail()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->setFlowStatusContent()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mLastVisible:Z

    new-instance v2, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;-><init>(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/systemui/statusbar/phone/FlowStatusBar$2;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar$2;-><init>(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowClickListener:Landroid/view/View$OnClickListener;

    new-instance v2, Lcom/android/systemui/statusbar/phone/FlowStatusBar$3;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar$3;-><init>(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mPurchaseClickListener:Landroid/view/View$OnClickListener;

    new-instance v2, Lcom/android/systemui/statusbar/phone/FlowStatusBar$4;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/android/systemui/statusbar/phone/FlowStatusBar$4;-><init>(Lcom/android/systemui/statusbar/phone/FlowStatusBar;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowStatusShowObserver:Landroid/database/ContentObserver;

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mPortrait:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->initNetworkAssistantProviderUri()V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private initNetworkAssistantProviderUri()V
    .locals 1

    const-string/jumbo v0, "content://com.miui.networkassistant.provider/datausage_noti_status"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mNetworkUri:Landroid/net/Uri;

    return-void
.end method

.method private readDataUsageDetail()V
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x1

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sBootCompleted:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "PhoneStatusBar/FlowStatusBar"

    const-string/jumbo v1, "not boot complete yet\uff01"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v7, 0x0

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mNetworkUri:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "text1"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mText1:Ljava/lang/String;

    const-string/jumbo v0, "text2"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mText2:Ljava/lang/String;

    const-string/jumbo v0, "action1"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mAction1:Ljava/lang/String;

    const-string/jumbo v0, "action2"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mAction2:Ljava/lang/String;

    const-string/jumbo v0, "icon"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIconUri:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mAction1:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIntent1:Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mAction2:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIntent2:Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIconUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "r"

    invoke-virtual {v0, v9, v1}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v10

    new-instance v0, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v0, v10}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIcon:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mText1:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mCharSequence1:Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v7, 0x1

    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_0
    if-eqz v7, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mText1:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIcon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIntent1:Landroid/content/Intent;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIntent2:Landroid/content/Intent;

    if-eqz v0, :cond_4

    move v0, v11

    :goto_1
    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mDataUsageAvaliable:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/provider/ExtraNetwork;->isTrafficPurchaseSupported(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mNeedPurchase:Z

    return-void

    :catch_0
    move-exception v8

    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v7, 0x0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    move v0, v12

    goto :goto_1

    :cond_5
    move v0, v12

    goto :goto_1
.end method

.method private setFlowStatusContent()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mCharSequence1:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowPurchase:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mText2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mPieView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "PhoneStatusBar/FlowStatusBar"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgThread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/phone/FlowStatusBar$WorkHandler;-><init>(Lcom/android/systemui/statusbar/phone/FlowStatusBar;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "status_bar_show_network_assistant"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowStatusShowObserver:Landroid/database/ContentObserver;

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowStatusShowObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v4}, Landroid/database/ContentObserver;->onChange(Z)V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    const/4 v0, 0x1

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mPortrait:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->updateVisibility()V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    iput-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgThread:Landroid/os/HandlerThread;

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowStatusShowObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onEditModeAnimationEnd(Z)V
    .locals 2

    xor-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowPurchase:Landroid/widget/TextView;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public onEditModeAnimationStart(Z)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowPurchase:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public onEditModeAnimationUpdate(F)V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mContent:Landroid/view/View;

    sget v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->EDIT_MODE_ALPHA:F

    sub-float v1, v2, v1

    mul-float/2addr v1, p1

    sub-float v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f0031

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mContent:Landroid/view/View;

    const v0, 0x7f0f0186

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowContent:Landroid/widget/TextView;

    const v0, 0x7f0f0185

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mPieView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f0187

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowPurchase:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowPurchase:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mPurchaseClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mFlowPurchase:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public updateVisibility()V
    .locals 2

    const/16 v1, 0x64

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mBgHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
