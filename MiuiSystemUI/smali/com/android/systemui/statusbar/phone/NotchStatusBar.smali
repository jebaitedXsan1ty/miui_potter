.class public Lcom/android/systemui/statusbar/phone/NotchStatusBar;
.super Landroid/widget/FrameLayout;
.source "NotchStatusBar.java"

# interfaces
.implements Lcom/android/systemui/statusbar/phone/IStatusBar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/NotchStatusBar$1;,
        Lcom/android/systemui/statusbar/phone/NotchStatusBar$2;,
        Lcom/android/systemui/statusbar/phone/NotchStatusBar$3;
    }
.end annotation


# static fields
.field private static sFilterColor:I


# instance fields
.field mBatteryChargingIcon:Landroid/widget/ImageView;

.field mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

.field mBatteryNum:Landroid/widget/TextView;

.field private mChargingStateListener:Lcom/android/systemui/statusbar/policy/BatteryController$ChargingStateListener;

.field private mClockShow:Z

.field private mDarkMode:Z

.field private mDemoMode:Z

.field mIcons:Landroid/view/View;

.field private mIconsDarkArea:Landroid/graphics/Rect;

.field private mIsExpand:Z

.field private mLayoutDirection:I

.field private final mMakeIconsInvisible:Landroid/animation/Animator$AnimatorListener;

.field mMoreIcon:Landroid/widget/ImageView;

.field mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

.field mNotificationIconCluster:Landroid/view/View;

.field mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

.field mNotificationMoreIcon:Landroid/widget/ImageView;

.field private mQuickCharging:Z

.field private mReturnToMultiMode:Z

.field mReturnToMultiModeText:Landroid/widget/TextView;

.field mSearchButton:Landroid/widget/ImageView;

.field mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mShowClock:Z

.field private mShowNetworkSpeed:Z

.field mSignalClusterContainer:Landroid/view/View;

.field mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

.field mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

.field mStatusBarIcons:Lcom/android/systemui/statusbar/phone/StatusBarIcons;

.field mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

.field mStatusIcons:Landroid/widget/LinearLayout;

.field private mUpdateQuickChargingTask:Ljava/lang/Runnable;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/NotchStatusBar;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/NotchStatusBar;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mUpdateQuickChargingTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/phone/NotchStatusBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->startSearchActivity()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->sFilterColor:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIsExpand:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mShowNetworkSpeed:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mShowClock:Z

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mQuickCharging:Z

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiMode:Z

    new-instance v0, Lcom/android/systemui/statusbar/phone/NotchStatusBar$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar$1;-><init>(Lcom/android/systemui/statusbar/phone/NotchStatusBar;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mChargingStateListener:Lcom/android/systemui/statusbar/policy/BatteryController$ChargingStateListener;

    new-instance v0, Lcom/android/systemui/statusbar/phone/NotchStatusBar$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar$2;-><init>(Lcom/android/systemui/statusbar/phone/NotchStatusBar;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mUpdateQuickChargingTask:Ljava/lang/Runnable;

    iput v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mLayoutDirection:I

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mClockShow:Z

    new-instance v0, Lcom/android/systemui/statusbar/phone/NotchStatusBar$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar$3;-><init>(Lcom/android/systemui/statusbar/phone/NotchStatusBar;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMakeIconsInvisible:Landroid/animation/Animator$AnimatorListener;

    return-void
.end method

.method private findQuietModeIconView()Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/statusbar/StatusBarIconView;

    const-string/jumbo v2, "quiet"

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/StatusBarIconView;->getSlot()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    return-object v2
.end method

.method private getIconsDarkArea()Landroid/graphics/Rect;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->getId()I

    move-result v1

    const v2, 0x7f0f015c

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->getId()I

    move-result v1

    const v2, 0x7f0f017f

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDarkMode()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->getId()I

    move-result v1

    const v2, 0x7f0f015c

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v0, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mDarkMode:Z

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->getId()I

    move-result v1

    const v2, 0x7f0f017f

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v0, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedDarkMode:Z

    goto :goto_0
.end method

.method private loadAnim(ILandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    :cond_0
    return-object v0
.end method

.method private refreshClockVisible()V
    .locals 0

    return-void
.end method

.method private startSearchActivity()V
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/android/systemui/SystemUICompatibility;->dismissKeyguardOnNextActivity()V

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SEARCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "com.android.quicksearchbox"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "qsb://query?close_web_page=true"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.extra.REFERRER"

    sget-object v3, Lcom/android/systemui/statusbar/SearchView;->PEFERRER_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->animateCollapseAndUnlock()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updateBatteryChargingIcon()V
    .locals 4

    const-string/jumbo v1, "SimpleStatusBar"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateBatteryChargingIcon() mQuickCharging="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mQuickCharging:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; mDarkMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mQuickCharging:Z

    if-eqz v1, :cond_0

    const v1, 0x7f020267

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryChargingIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryChargingIcon:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryChargingIcon:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateIcon(Landroid/widget/ImageView;I)V
    .locals 5

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {p1, v3, v4}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/android/systemui/statusbar/Icons;->get(Ljava/lang/Integer;Z)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v1, :cond_1

    sget-boolean v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sMiuiOptimizationDisabled:Z

    if-eqz v3, :cond_1

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x19

    if-le v3, v4, :cond_1

    sget v3, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->sFilterColor:I

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090025

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->sFilterColor:I

    :cond_0
    sget v3, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->sFilterColor:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_1
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public addIcon(Ljava/lang/String;IILcom/android/systemui/statusbar/ExpandedIcon;)V
    .locals 3

    new-instance v0, Lcom/android/systemui/statusbar/StatusBarIconView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/android/systemui/statusbar/StatusBarIconView;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/systemui/statusbar/ExpandedNotification;)V

    invoke-virtual {v0, p4}, Lcom/android/systemui/statusbar/StatusBarIconView;->set(Lcom/android/systemui/statusbar/ExpandedIcon;)Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->isDarkMode()Z

    move-result v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/StatusBarIconView;->updateDarkMode(Z)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x8

    const-string/jumbo v0, "demo_mode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SimpleStatusBar mDemoMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDemoMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", command = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDemoMode:Z

    if-nez v0, :cond_1

    const-string/jumbo v0, "enter"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDemoMode:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIconCluster:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, v4}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateLabelVisible(Z)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/NetworkSpeedView;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/phone/BatteryIndicator;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDemoMode:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "exit"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDemoMode:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->updateViewsInStatusBar()V

    goto :goto_0
.end method

.method public initDisplay(ZZ)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIsExpand:Z

    iput-boolean p2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mShowNetworkSpeed:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->updateDarkMode()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->updateViewsInStatusBar()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->updateQuickCharging()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f01ab

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setSlotId(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setCardSlot(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setNotchExpandedStatusbar(Z)V

    const v0, 0x7f0f01aa

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterContainer:Landroid/view/View;

    const v0, 0x7f0f01ae

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/NetworkSpeedView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/NetworkSpeedView;->canShow(Z)V

    const v0, 0x7f0f01ad

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    const v0, 0x7f0f01a6

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/IconMerger;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    const v0, 0x7f0f01a5

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIconCluster:Landroid/view/View;

    const v0, 0x7f0f01af

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f01a7

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationMoreIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f01a4

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIcons:Landroid/view/View;

    const v0, 0x7f0f01a8

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBarIcons:Lcom/android/systemui/statusbar/phone/StatusBarIcons;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBarIcons:Lcom/android/systemui/statusbar/phone/StatusBarIcons;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIcons:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0, v3, v1, v2}, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->setOverflowIndicator(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIcons:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationMoreIcon:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBarIcons:Lcom/android/systemui/statusbar/phone/StatusBarIcons;

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/phone/IconMerger;->setOverflowIndicator(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    const v0, 0x7f0f002b

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    const v0, 0x7f0f01a3

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    const v0, 0x7f0f002c

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryNum:Landroid/widget/TextView;

    const v0, 0x7f0f01b2

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    const v0, 0x7f0f01a9

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryChargingIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f01ac

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setSlotId(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setCardSlot(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setWifiNeedVisible(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setAirplaneModeNeedVisible(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setNotchExpandedStatusbar(Z)V

    new-instance v0, Lcom/android/systemui/statusbar/phone/NotchStatusBar$4;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar$4;-><init>(Lcom/android/systemui/statusbar/phone/NotchStatusBar;)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0f01b0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSearchButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSearchButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/systemui/statusbar/phone/NotchStatusBar$5;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar$5;-><init>(Lcom/android/systemui/statusbar/phone/NotchStatusBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/IconMerger;->forceLayout()V

    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRtlPropertiesChanged(I)V

    return-void
.end method

.method public removeIcon(Ljava/lang/String;II)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p3}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    return-void
.end method

.method public setBatteryController(Lcom/android/systemui/statusbar/policy/BatteryController;)V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addIconView(Lcom/android/systemui/statusbar/policy/BatteryController$BatteryStateChangeCallback;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addIconView(Lcom/android/systemui/statusbar/policy/BatteryController$BatteryStateChangeCallback;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryChargingIcon:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addChargingIconView(Landroid/widget/ImageView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryNum:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryNum:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addLabelView(Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mChargingStateListener:Lcom/android/systemui/statusbar/policy/BatteryController$ChargingStateListener;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addChargingStateListener(Lcom/android/systemui/statusbar/policy/BatteryController$ChargingStateListener;)V

    return-void
.end method

.method public setNetworkController(Lcom/android/systemui/statusbar/policy/NetworkController;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setNetworkController(Lcom/android/systemui/statusbar/policy/NetworkController;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setNetworkController(Lcom/android/systemui/statusbar/policy/NetworkController;)V

    return-void
.end method

.method public setSignalClusterViewId(II)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setViewId(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    invoke-virtual {v0, p2}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->setViewId(I)V

    return-void
.end method

.method public startQuietModeIconAnim(Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->findQuietModeIconView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const v1, 0x7f04000b

    invoke-direct {p0, v1, v2}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->loadAnim(ILandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_0
.end method

.method public updateDarkMode()V
    .locals 10

    const v3, 0x7f090025

    const v5, 0x7f090009

    const v9, 0x7f020216

    const v4, 0x7f090008

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->isDarkMode()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->getIconsDarkArea()Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    const-string/jumbo v2, "SimpleStatusBar"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateDarkMode() mDarkMode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v2, "SimpleStatusBar"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateDarkMode() mIconsDarkArea="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateDarkMode(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isMsim()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateDarkMode(Z)V

    :cond_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/NetworkSpeedView;->updateDarkMode(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->updateDarkMode(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/statusbar/StatusBarIconView;

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v1, v2, v6}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/StatusBarIconView;->updateDarkMode(Z)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v9}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationMoreIcon:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v9}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->updateIcon(Landroid/widget/ImageView;I)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->updateBatteryChargingIcon()V

    sget-boolean v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sMiuiOptimizationDisabled:Z

    if-eqz v2, :cond_4

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x19

    if-le v2, v6, :cond_4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryNum:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryNum:Landroid/widget/TextView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v2, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    :goto_1
    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_3

    :goto_2
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_3
    return-void

    :cond_2
    move v2, v4

    goto :goto_1

    :cond_3
    move v3, v4

    goto :goto_2

    :cond_4
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryNum:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryNum:Landroid/widget/TextView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v2, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v5

    :goto_4
    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_6

    :goto_5
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    :cond_5
    move v2, v4

    goto :goto_4

    :cond_6
    move v5, v4

    goto :goto_5
.end method

.method public updateIcon(Ljava/lang/String;IILcom/android/systemui/statusbar/ExpandedIcon;Lcom/android/systemui/statusbar/ExpandedIcon;)V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/StatusBarIconView;

    invoke-virtual {v0, p5}, Lcom/android/systemui/statusbar/StatusBarIconView;->set(Lcom/android/systemui/statusbar/ExpandedIcon;)Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->isDarkMode()Z

    move-result v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/StatusBarIconView;->updateDarkMode(Z)V

    return-void
.end method

.method public updateNotificationIcons(ZLjava/util/ArrayList;Landroid/widget/LinearLayout$LayoutParams;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/LinearLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->getId()I

    move-result v9

    const v10, 0x7f0f015c

    if-eq v9, v10, :cond_0

    return-void

    :cond_0
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v9, v9, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowNotificationIcons:Z

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/android/systemui/statusbar/phone/IconMerger;->setVisibility(I)V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_0
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9}, Lcom/android/systemui/statusbar/phone/IconMerger;->getChildCount()I

    move-result v9

    if-ge v4, v9, :cond_2

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v4}, Lcom/android/systemui/statusbar/phone/IconMerger;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v5}, Lcom/android/systemui/statusbar/phone/IconMerger;->removeView(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_5

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    if-nez v9, :cond_4

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v8, v4, p3}, Lcom/android/systemui/statusbar/phone/IconMerger;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9}, Lcom/android/systemui/statusbar/phone/IconMerger;->getChildCount()I

    move-result v2

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v2, :cond_8

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v4}, Lcom/android/systemui/statusbar/phone/IconMerger;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-ne v0, v3, :cond_6

    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v3}, Lcom/android/systemui/statusbar/phone/IconMerger;->removeView(Landroid/view/View;)V

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v3, v4}, Lcom/android/systemui/statusbar/phone/IconMerger;->addView(Landroid/view/View;I)V

    goto :goto_4

    :cond_7
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/android/systemui/statusbar/phone/IconMerger;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/android/systemui/statusbar/phone/IconMerger;->setForceShowingMore(Z)V

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_9

    const/4 v9, 0x0

    :goto_5
    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_8
    return-void

    :cond_9
    const/16 v9, 0x8

    goto :goto_5
.end method

.method public updateQuickCharging()V
    .locals 3

    new-instance v0, Lcom/android/systemui/statusbar/phone/NotchStatusBar$6;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar$6;-><init>(Lcom/android/systemui/statusbar/phone/NotchStatusBar;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/systemui/statusbar/phone/NotchStatusBar$6;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public updateQuickCharging(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mQuickCharging:Z

    const-string/jumbo v0, "SimpleStatusBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateQuickCharging() mQuickCharging="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mQuickCharging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->updateBatteryChargingIcon()V

    return-void
.end method

.method public updateQuickChargingDelayed(J)V
    .locals 3

    const-string/jumbo v0, "SimpleStatusBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateQuickChargingDelayed() delayMillis="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mUpdateQuickChargingTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, p1, p2}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public updateViewsInStatusBar()V
    .locals 10

    const/4 v9, 0x2

    const/4 v6, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mDemoMode:Z

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->refreshClockVisible()V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v0, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mDisabled:I

    and-int/lit8 v3, v0, 0x20

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->disable(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/phone/BatteryIndicator;->disable(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterContainer:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-virtual {v3, v5}, Lcom/android/systemui/statusbar/NetworkSpeedView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->refreshClockVisible()V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIconCluster:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isUnderKeyguard()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->getId()I

    move-result v3

    const v8, 0x7f0f015c

    if-ne v3, v8, :cond_9

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    sget-boolean v8, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowBatteryUnderKeyguard:Z

    xor-int/lit8 v8, v8, 0x1

    invoke-virtual {v3, v8}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->disable(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    sget-boolean v8, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowBatteryUnderKeyguard:Z

    xor-int/lit8 v8, v8, 0x1

    invoke-virtual {v3, v8}, Lcom/android/systemui/statusbar/phone/BatteryIndicator;->disable(Z)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterContainer:Landroid/view/View;

    sget-boolean v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowSignalUnderKeyguard:Z

    if-eqz v3, :cond_3

    move v3, v4

    :goto_1
    invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    sget-boolean v8, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowSignalUnderKeyguard:Z

    xor-int/lit8 v8, v8, 0x1

    invoke-virtual {v3, v8}, Lcom/android/systemui/statusbar/NetworkSpeedView;->requestHideByKeyguard(Z)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->refreshClockVisible()V

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    sget-boolean v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowStatusUnderKeyguard:Z

    if-eqz v3, :cond_4

    move v3, v4

    :goto_2
    invoke-virtual {v8, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v3, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowCarrierStyle:I

    if-eq v3, v9, :cond_5

    move v3, v6

    :goto_3
    invoke-virtual {v8, v3}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateLabelVisible(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isMsim()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v8, v8, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowCarrierStyle:I

    if-eq v8, v9, :cond_6

    :goto_4
    invoke-virtual {v3, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateLabelVisible(Z)V

    :cond_2
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v3, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedVisible:Z

    if-nez v3, :cond_7

    sget-boolean v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowNotificationUnderKeyguard:Z

    if-eqz v3, :cond_7

    const/high16 v3, 0x20000

    and-int/2addr v3, v0

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v1, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowNotificationIcons:Z

    :goto_5
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIconCluster:Landroid/view/View;

    if-eqz v1, :cond_8

    :goto_6
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    move v3, v5

    goto :goto_1

    :cond_4
    move v3, v5

    goto :goto_2

    :cond_5
    move v3, v4

    goto :goto_3

    :cond_6
    move v6, v4

    goto :goto_4

    :cond_7
    const/4 v1, 0x0

    goto :goto_5

    :cond_8
    move v4, v5

    goto :goto_6

    :cond_9
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->disable(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/phone/BatteryIndicator;->disable(Z)V

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterContainer:Landroid/view/View;

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiMode:Z

    if-nez v3, :cond_d

    move v3, v4

    :goto_7
    invoke-virtual {v8, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    iget-boolean v8, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiMode:Z

    if-nez v8, :cond_a

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mShowNetworkSpeed:Z

    xor-int/lit8 v6, v6, 0x1

    :cond_a
    invoke-virtual {v3, v6}, Lcom/android/systemui/statusbar/NetworkSpeedView;->requestHideByKeyguard(Z)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->refreshClockVisible()V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiMode:Z

    if-nez v3, :cond_e

    move v3, v4

    :goto_8
    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mIsExpand:Z

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiMode:Z

    if-nez v3, :cond_f

    move v3, v2

    :goto_9
    invoke-virtual {v6, v3}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateLabelVisible(Z)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isMsim()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiMode:Z

    if-nez v6, :cond_10

    :goto_a
    invoke-virtual {v3, v2}, Lcom/android/systemui/statusbar/NotchSignalClusterViewExpand;->updateLabelVisible(Z)V

    :cond_b
    const/high16 v3, 0x20000

    and-int/2addr v3, v0

    if-nez v3, :cond_11

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v1, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowNotificationIcons:Z

    :goto_b
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mNotificationIconCluster:Landroid/view/View;

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiMode:Z

    if-nez v3, :cond_12

    if-eqz v1, :cond_12

    move v3, v4

    :goto_c
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v3, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mSystemUiVisibility:I

    and-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_c

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiMode:Z

    if-eqz v3, :cond_13

    :cond_c
    const/4 v3, 0x0

    :goto_d
    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    iget-boolean v6, p0, Lcom/android/systemui/statusbar/phone/NotchStatusBar;->mReturnToMultiMode:Z

    if-eqz v6, :cond_14

    :goto_e
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_d
    move v3, v5

    goto :goto_7

    :cond_e
    move v3, v5

    goto :goto_8

    :cond_f
    move v3, v4

    goto :goto_9

    :cond_10
    move v2, v4

    goto :goto_a

    :cond_11
    const/4 v1, 0x0

    goto :goto_b

    :cond_12
    move v3, v5

    goto :goto_c

    :cond_13
    move v3, v7

    goto :goto_d

    :cond_14
    move v4, v5

    goto :goto_e
.end method
