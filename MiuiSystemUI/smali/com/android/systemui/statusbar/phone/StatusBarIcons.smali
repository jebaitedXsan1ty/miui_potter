.class public Lcom/android/systemui/statusbar/phone/StatusBarIcons;
.super Landroid/widget/LinearLayout;
.source "StatusBarIcons.java"


# instance fields
.field private mIcons:Landroid/view/View;

.field private mNotificationMoreIcon:Landroid/view/View;

.field private mStatusBar:Landroid/view/View;

.field private mStatusClock:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    new-instance v0, Lcom/android/systemui/statusbar/phone/StatusBarIcons$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/StatusBarIcons$1;-><init>(Lcom/android/systemui/statusbar/phone/StatusBarIcons;)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mStatusClock:Landroid/view/View;

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mStatusBar:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mStatusBar:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingStart()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mStatusBar:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingEnd()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mNotificationMoreIcon:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mIcons:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingStart()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mIcons:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingEnd()I

    move-result v3

    sub-int v1, v2, v3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->getMeasuredWidth()I

    move-result v2

    if-gt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->setMeasuredDimension(II)V

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mStatusClock:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    goto :goto_0
.end method

.method public setOverflowIndicator(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mStatusBar:Landroid/view/View;

    iput-object p2, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mStatusClock:Landroid/view/View;

    iput-object p3, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mIcons:Landroid/view/View;

    iput-object p4, p0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->mNotificationMoreIcon:Landroid/view/View;

    return-void
.end method
