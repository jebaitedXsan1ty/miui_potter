.class public Lcom/android/systemui/statusbar/phone/NotificationScrollView;
.super Landroid/widget/ScrollView;
.source "NotificationScrollView.java"


# instance fields
.field private mMarkAllSeenResult:Z

.field private mNotificationData:Lcom/android/systemui/statusbar/NotificationData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->mMarkAllSeenResult:Z

    return-void
.end method


# virtual methods
.method public getMarkAllSeenResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->mMarkAllSeenResult:Z

    return v0
.end method

.method public markAllSeen(I)V
    .locals 14

    const-wide/16 v12, 0x0

    const/4 v10, 0x1

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->mNotificationData:Lcom/android/systemui/statusbar/NotificationData;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/NotificationData;->size()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_6

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->mNotificationData:Lcom/android/systemui/statusbar/NotificationData;

    invoke-virtual {v4, v2}, Lcom/android/systemui/statusbar/NotificationData;->get(I)Lcom/android/systemui/statusbar/NotificationData$Entry;

    move-result-object v1

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    if-eqz v4, :cond_0

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-boolean v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->isSeen:Z

    if-nez v4, :cond_3

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getY()F

    move-result v5

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v4

    add-float/2addr v4, v5

    iget-object v5, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getHeight()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->getHeight()I

    move-result v5

    add-int/2addr v5, p1

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    iput-boolean v10, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->isSeen:Z

    iget-wide v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->seeTime:J

    cmp-long v4, v4, v12

    if-nez v4, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->seeTime:J

    :cond_2
    iput-boolean v10, p0, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->mMarkAllSeenResult:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/android/systemui/AdTracker;->trackShow(Landroid/content/Context;Lcom/android/systemui/statusbar/NotificationData$Entry;)V

    invoke-static {}, Lcom/android/systemui/AnalyticsHelper;->recordNotificationReadNum()V

    :cond_3
    const/4 v3, 0x0

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getY()F

    move-result v5

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v4

    add-float/2addr v4, v5

    iget-object v5, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getHeight()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->getHeight()I

    move-result v5

    add-int/2addr v5, p1

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_4

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getY()F

    move-result v5

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getY()F

    move-result v4

    add-float/2addr v4, v5

    iget-object v5, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getHeight()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v4, v5

    int-to-float v5, p1

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    const/4 v3, 0x1

    :cond_4
    iget-boolean v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->isLastSeen:Z

    if-eq v4, v3, :cond_0

    if-eqz v3, :cond_5

    iput-boolean v10, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->isLastSeen:Z

    iget-wide v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->lastVisibleTime:J

    cmp-long v4, v4, v12

    if-nez v4, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->lastVisibleTime:J

    goto/16 :goto_1

    :cond_5
    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->isLastSeen:Z

    iget-object v4, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->messageList:Ljava/util/List;

    new-instance v5, Lcom/android/systemui/analytics/ExposeMessage;

    iget-wide v6, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->lastVisibleTime:J

    const-string/jumbo v8, "statusbar"

    iget-object v9, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {p0, v9}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->indexOfChild(Landroid/view/View;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/systemui/analytics/ExposeMessage;-><init>(JLjava/lang/String;I)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-wide v12, v1, Lcom/android/systemui/statusbar/NotificationData$Entry;->lastVisibleTime:J

    goto/16 :goto_1

    :cond_6
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    invoke-virtual {p0, p2}, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->markAllSeen(I)V

    return-void
.end method

.method public setMarkAllSeenResult(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->mMarkAllSeenResult:Z

    return-void
.end method

.method public setNotificationData(Lcom/android/systemui/statusbar/NotificationData;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NotificationScrollView;->mNotificationData:Lcom/android/systemui/statusbar/NotificationData;

    return-void
.end method
