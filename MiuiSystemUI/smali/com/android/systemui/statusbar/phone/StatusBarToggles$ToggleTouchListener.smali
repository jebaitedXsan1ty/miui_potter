.class Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;
.super Ljava/lang/Object;
.source "StatusBarToggles.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/StatusBarToggles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ToggleTouchListener"
.end annotation


# instance fields
.field mStarted:Z

.field mToggleAlphaAnimator:Landroid/animation/ObjectAnimator;

.field mToggleView:Landroid/view/View;

.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/StatusBarToggles;


# direct methods
.method public constructor <init>(Lcom/android/systemui/statusbar/phone/StatusBarToggles;Landroid/view/View;)V
    .locals 4

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->this$0:Lcom/android/systemui/statusbar/phone/StatusBarToggles;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mToggleView:Landroid/view/View;

    const-string/jumbo v0, "transitionAlpha"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p2, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mToggleAlphaAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mToggleAlphaAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mToggleAlphaAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e800000    # 0.25f
    .end array-data
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v1

    :pswitch_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mToggleView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mToggleAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mStarted:Z

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mStarted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mToggleAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/StatusBarToggles$ToggleTouchListener;->mStarted:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
