.class Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;
.super Landroid/os/Handler;
.source "FlowStatusBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/FlowStatusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v2, v1}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->setVisibility(I)V

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-wrap1(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)V

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-get6(Lcom/android/systemui/statusbar/phone/FlowStatusBar;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    invoke-static {v1, v0}, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->-set1(Lcom/android/systemui/statusbar/phone/FlowStatusBar;Z)Z

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->updateNotificationBackground()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->onNotificationSizeChanged()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FlowStatusBar$1;->this$0:Lcom/android/systemui/statusbar/phone/FlowStatusBar;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/FlowStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->onFlowStatusBarVisibilityChanged()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/16 v1, 0x8

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x186a0
        :pswitch_0
    .end packed-switch
.end method
