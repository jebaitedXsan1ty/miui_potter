.class public Lcom/android/systemui/statusbar/phone/UpdateAppsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UpdateAppsReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private sendBroadcastToDestPackage(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.UPDATE_MIUI_APPS_LIB"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const-string/jumbo v2, "miui.permission.USE_INTERNAL_GENERAL_API"

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    return-void
.end method

.method private sendBroadcastToIndex(Landroid/content/Context;I)V
    .locals 1

    invoke-static {}, Lcom/android/systemui/download/DownloadExtraPackage;->getDestPackages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/phone/UpdateAppsReceiver;->sendBroadcastToDestPackage(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/systemui/download/DownloadExtraPackage;->addIndex()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    invoke-static {}, Lcom/android/systemui/download/DownloadExtraPackage;->getIndex()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/phone/UpdateAppsReceiver;->sendBroadcastToIndex(Landroid/content/Context;I)V

    return-void
.end method
