.class public Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;
.super Landroid/widget/LinearLayout;
.source "ExpandableTogglesLayout.java"


# instance fields
.field private mPaddingBetweenRows:I

.field private mRowCount:I

.field private mRowHeight:I

.field private mRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mToggleViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRows:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mToggleViews:Ljava/util/List;

    iput v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowHeight:I

    iput v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowCount:I

    iput v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mPaddingBetweenRows:I

    return-void
.end method

.method private static getRowCount(II)I
    .locals 1

    add-int/lit8 v0, p0, -0x1

    div-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public getCollapsedHeight()I
    .locals 3

    const-string/jumbo v0, "ExpandableTogglesLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getCollapsedHeight() mRowHeight"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowHeight:I

    return v0
.end method

.method public getExpandedHeight()I
    .locals 4

    iget v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowHeight:I

    iget v2, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowCount:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mPaddingBetweenRows:I

    iget v3, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowCount:I

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v2, v3

    add-int v0, v1, v2

    const-string/jumbo v1, "ExpandableTogglesLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getExpandedHeight() ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getHeightDiff()I
    .locals 4

    const-string/jumbo v0, "ExpandableTogglesLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getExpandedHeight() ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getExpandedHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getCollapsedHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getExpandedHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getCollapsedHeight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getToggleViewAt(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mToggleViews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const-string/jumbo v0, "ExpandableTogglesLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onFinishInflate() getOrientation()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getOrientation()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setToggles(Ljava/util/List;IIIII)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;IIIII)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->removeAllViews()V

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRows:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->clear()V

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mToggleViews:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->clear()V

    move/from16 v0, p5

    iput v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowHeight:I

    move/from16 v0, p6

    iput v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mPaddingBetweenRows:I

    if-gez p2, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v10, p2}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getRowCount(II)I

    move-result v10

    iput v10, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowCount:I

    const/4 v1, 0x0

    :goto_0
    iget v10, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowCount:I

    if-ge v1, v10, :cond_4

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    const v11, 0x7f030062

    const/4 v12, 0x0

    invoke-virtual {v10, v11, p0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout$LayoutParams;

    iget v10, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRowHeight:I

    iput v10, v8, Landroid/widget/LinearLayout$LayoutParams;->height:I

    if-lez v1, :cond_1

    iget v10, v8, Landroid/widget/LinearLayout$LayoutParams;->height:I

    add-int v10, v10, p6

    iput v10, v8, Landroid/widget/LinearLayout$LayoutParams;->height:I

    :cond_1
    invoke-virtual {p0, v7, v8}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mRows:Ljava/util/List;

    invoke-interface {v10, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v3, 0x64

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v10, 0x42c80000    # 100.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v2, v11, v12, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    move/from16 v0, p4

    int-to-float v10, v0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v6, v11, v12, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    new-instance v10, Landroid/widget/Space;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v10, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v4, 0x0

    :goto_1
    if-ge v4, p2, :cond_3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    const v11, 0x7f030061

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v7, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    move/from16 v0, p3

    iput v0, v5, Landroid/widget/LinearLayout$LayoutParams;->width:I

    invoke-virtual {v9, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->mToggleViews:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v10, p2, -0x1

    if-ge v4, v10, :cond_2

    new-instance v10, Landroid/widget/Space;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v10, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    new-instance v10, Landroid/widget/Space;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v10, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_4
    return-void
.end method
