.class Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;
.super Ljava/lang/Object;
.source "NotificationPanelView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->createExpandAnimator(FI)Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

.field final synthetic val$targetY:F

.field final synthetic val$togglePanelTargetHeight:I


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;FI)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iput p2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->val$targetY:F

    iput p3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->val$togglePanelTargetHeight:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->val$targetY:F

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v2, v3}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->-set0(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;F)F

    iget v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->val$togglePanelTargetHeight:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    invoke-static {v3}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->-get0(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;)F

    move-result v3

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    invoke-static {v2, v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->-wrap1(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;->this$1:Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mContainer:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v5

    sub-float/2addr v4, v5

    invoke-static {v2, v0, v3, v4}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->-wrap2(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;IFF)V

    return-void
.end method
