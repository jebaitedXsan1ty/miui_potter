.class public Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;
.super Ljava/lang/Object;
.source "OLEDScreenHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/OLEDScreenHelper$1;,
        Lcom/android/systemui/statusbar/phone/OLEDScreenHelper$2;
    }
.end annotation


# static fields
.field private static final mIsOLEDScreen:Z


# instance fields
.field private mDirection:I

.field private mExpandedStatusBar:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private mInterval:I

.field private mNavigationBarRot0:Landroid/view/ViewGroup;

.field private mNavigationBarRot90:Landroid/view/ViewGroup;

.field private mPixels:I

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mStatusBar:Landroid/view/View;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mDirection:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mInterval:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mDirection:I

    return p1
.end method

.method static synthetic -set1(Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mInterval:I

    return p1
.end method

.method static synthetic -set2(Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mPixels:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->update()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "oled"

    const-string/jumbo v1, "ro.display.type"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mIsOLEDScreen:Z

    return-void
.end method

.method public constructor <init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper$1;-><init>(Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper$2;-><init>(Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    const v0, 0x1d4c0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mInterval:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mPixels:I

    return-void
.end method

.method private generateRandomDirection()I
    .locals 4

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    return v0
.end method

.method private isScreenOn(Landroid/content/Context;)Z
    .locals 2

    const-string/jumbo v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v1

    return v1
.end method

.method private resetView(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    return-void
.end method

.method private resetViewGroup(Landroid/view/ViewGroup;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->resetView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private update()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mStatusBar:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->updateView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mExpandedStatusBar:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->updateView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mNavigationBarRot0:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->updateViewGroup(Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mNavigationBarRot90:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->updateViewGroup(Landroid/view/ViewGroup;)V

    return-void
.end method

.method private updateView(Landroid/view/View;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mDirection:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v2, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mPixels:I

    int-to-float v2, v2

    sub-float v2, v0, v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    :pswitch_1
    iget v2, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mPixels:I

    int-to-float v2, v2

    sub-float v2, v1, v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    :pswitch_2
    iget v2, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mPixels:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    :pswitch_3
    iget v2, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mPixels:I

    int-to-float v2, v2

    add-float/2addr v2, v1

    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateViewGroup(Landroid/view/ViewGroup;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->isShown()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->updateView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public bind()V
    .locals 0

    return-void
.end method

.method public init()V
    .locals 5

    const-string/jumbo v0, "OLEDScreenHelper"

    const-string/jumbo v1, "mIsOLEDScreen=%b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    sget-boolean v3, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mIsOLEDScreen:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mSimpleStatusbar:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mStatusBar:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedSimpleStatusbar:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mExpandedStatusBar:Landroid/view/View;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v0

    const v1, 0x7f0f008d

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mNavigationBarRot0:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getNavigationBarView()Lcom/android/systemui/statusbar/phone/NavigationBarView;

    move-result-object v0

    const v1, 0x7f0f0096

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/NavigationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mNavigationBarRot90:Landroid/view/ViewGroup;

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->start()V

    return-void
.end method

.method public onConfigurationChanged()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->stop()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->start()V

    return-void
.end method

.method public start()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->isScreenOn(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->start(Z)V

    return-void
.end method

.method public start(Z)V
    .locals 5

    const/16 v4, 0x2711

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mIsOLEDScreen:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->generateRandomDirection()I

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mDirection:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mInterval:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mIsOLEDScreen:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x2711

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mStatusBar:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->resetView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mExpandedStatusBar:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->resetView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mNavigationBarRot0:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->resetViewGroup(Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->mNavigationBarRot90:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->resetViewGroup(Landroid/view/ViewGroup;)V

    :cond_0
    return-void
.end method

.method public unbind()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/OLEDScreenHelper;->stop()V

    return-void
.end method
