.class public Lcom/android/systemui/statusbar/phone/FloatNotificationView;
.super Lcom/android/systemui/statusbar/policy/NotificationRowLayout;
.source "FloatNotificationView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;
    }
.end annotation


# instance fields
.field private mEdgeSwipeHelper:Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;

.field mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/phone/FloatNotificationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/phone/FloatNotificationView;->setLayoutTransitionsEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/FloatNotificationView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    int-to-float v0, v2

    new-instance v2, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;

    invoke-direct {v2, p0, v0}, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;-><init>(Lcom/android/systemui/statusbar/phone/FloatNotificationView;F)V

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView;->mEdgeSwipeHelper:Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;

    return-void
.end method


# virtual methods
.method public canChildBeDismissed(Landroid/view/View;)Z
    .locals 2

    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/android/systemui/statusbar/OptimizedFloatNotificationView;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/android/systemui/statusbar/OptimizedFloatNotificationView;

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/OptimizedFloatNotificationView;->getRow()Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v1

    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v1

    return v1
.end method

.method public onChildDismissed(Landroid/view/View;)V
    .locals 2

    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/android/systemui/statusbar/OptimizedFloatNotificationView;

    if-eqz v1, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/android/systemui/statusbar/OptimizedFloatNotificationView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/OptimizedFloatNotificationView;->getRow()Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->onChildDismissed(Landroid/view/View;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->onChildDismissed(Landroid/view/View;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView;->mEdgeSwipeHelper:Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;

    invoke-virtual {v1, p1}, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    :goto_1
    return v1

    :pswitch_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->removeFloatNotificationExitRunnable()V

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView;->mEdgeSwipeHelper:Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;

    invoke-virtual {v3, p1}, Lcom/android/systemui/statusbar/phone/FloatNotificationView$EdgeSwipeHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-super {p0, p1}, Lcom/android/systemui/statusbar/policy/NotificationRowLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    :goto_1
    return v3

    :pswitch_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/android/systemui/statusbar/phone/FloatNotificationView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const v3, 0x7f0f0007

    invoke-virtual {v1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/systemui/statusbar/phone/FloatNotificationInfo;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/FloatNotificationInfo;->getFloatWhen()J

    move-result-wide v10

    sub-long v6, v8, v10

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/FloatNotificationInfo;->getFloatTime()I

    move-result v3

    int-to-long v10, v3

    sub-long v4, v10, v6

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/FloatNotificationView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    const-wide/16 v12, 0x3e8

    cmp-long v3, v4, v12

    if-gez v3, :cond_1

    const/16 v3, 0x3e8

    :goto_2
    invoke-virtual {v10, v3}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->postDelayedFloatNotificationExit(I)V

    goto :goto_0

    :cond_1
    long-to-int v3, v4

    goto :goto_2

    :cond_2
    const/4 v3, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
