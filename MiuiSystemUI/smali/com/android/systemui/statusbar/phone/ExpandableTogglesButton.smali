.class public Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;
.super Landroid/view/View;
.source "ExpandableTogglesButton.java"


# instance fields
.field private mCenter:I

.field private mFirstRow:Landroid/graphics/Bitmap;

.field private mFirstRowY:F

.field private mPaint:Landroid/graphics/Paint;

.field private mRowHeight:I

.field private mSecondRow:Landroid/graphics/Bitmap;

.field private mThirdRow:Landroid/graphics/Bitmap;

.field private mThirdRowY:F

.field private mVerticalPadding:F


# direct methods
.method static synthetic -set0(Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;I)I
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mCenter:I

    return p1
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mVerticalPadding:F

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0202e2

    invoke-virtual {p0, v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->setBitmap(Landroid/content/res/Resources;I)V

    new-instance v0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton$1;-><init>(Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mRowHeight:I

    div-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mFirstRow:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mFirstRowY:F

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mSecondRow:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mCenter:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mThirdRow:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mThirdRowY:F

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method setBitmap(Landroid/content/res/Resources;I)V
    .locals 1

    invoke-static {p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mFirstRow:Landroid/graphics/Bitmap;

    invoke-static {p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mSecondRow:Landroid/graphics/Bitmap;

    invoke-static {p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mThirdRow:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mFirstRow:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mRowHeight:I

    return-void
.end method

.method setRowHeightPercent(I)V
    .locals 3

    int-to-float v0, p1

    iget v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mCenter:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mVerticalPadding:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mRowHeight:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iget v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mVerticalPadding:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mRowHeight:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mFirstRowY:F

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mFirstRowY:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->mThirdRowY:F

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->invalidate()V

    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-super {p0, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
