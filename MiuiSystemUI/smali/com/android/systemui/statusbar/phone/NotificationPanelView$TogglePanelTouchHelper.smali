.class Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;
.super Ljava/lang/Object;
.source "NotificationPanelView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/phone/NotificationPanelView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TogglePanelTouchHelper"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;
    }
.end annotation


# instance fields
.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mDeltaY:F

.field private mExpandTogglesButtonListener:Landroid/view/View$OnTouchListener;

.field mExpandableTogglesLayoutHeight:I

.field mTogglePanelExpandAdjusting:Z

.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;)F
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mDeltaY:F

    return v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;F)F
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mDeltaY:F

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->onClick()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->setExpandTogglesButtonState(I)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;IFF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->setViewsPosition(IFF)V

    return-void
.end method

.method private constructor <init>(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mTogglePanelExpandAdjusting:Z

    new-instance v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$1;-><init>(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mExpandTogglesButtonListener:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/systemui/statusbar/phone/NotificationPanelView;Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;-><init>(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)V

    return-void
.end method

.method private createExpandAnimator(FI)Landroid/animation/ValueAnimator;
    .locals 4

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mNotificationStack:Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;->getY()F

    move-result v1

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v3, 0x1

    aput p1, v2, v3

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v2, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;

    invoke-direct {v2, p0, p1, p2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$3;-><init>(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;FI)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    return-object v0
.end method

.method private onClick()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mAnimaing:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mTogglePanelExpanded:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1, v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->fling(FZ)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iput-boolean v2, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mTogglePanelExpanded:Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v1, v3}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->fling(FZ)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iput-boolean v3, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mTogglePanelExpanded:Z

    goto :goto_0
.end method

.method private setExpandTogglesButtonState(I)V
    .locals 4

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v1

    sub-int v1, p1, v1

    mul-int/lit8 v1, v1, 0x64

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v3}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v3

    sub-int/2addr v2, v3

    div-int v0, v1, v2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v1, v1, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getExpandTogglesButton()Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->setRowHeightPercent(I)V

    return-void
.end method

.method private setViewsPosition(IFF)V
    .locals 4

    iput p1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mExpandableTogglesLayoutHeight:I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get1(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap8(Lcom/android/systemui/statusbar/phone/NotificationPanelView;Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mNotificationStack:Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;

    float-to-double v2, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;->setY(F)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mNotificationStackBg:Landroid/view/View;

    float-to-double v2, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mNotificationStack:Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;

    float-to-double v2, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;->setStackHeight(F)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)V

    return-void
.end method


# virtual methods
.method adjustTogglePanelHeight(FFF)V
    .locals 6

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mTogglePanelExpandAdjusting:Z

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v3}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, p1

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v4, p1

    invoke-static {p3, v3, v4}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap0(FFF)F

    move-result p3

    add-float v2, p1, p3

    add-float v1, p2, p3

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->setExpandTogglesButtonState(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v3, v3, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mContainer:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v1

    invoke-direct {p0, v0, v1, v3}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->setViewsPosition(IFF)V

    const-string/jumbo v3, "expandable_toggle"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "TogglePanelTouchHelper.adjustTogglePanelHeight() mLargeQSContainerHeight="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get5(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";mSmallQSContainerHeight="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get9(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";mLargeTogglePanelHeight="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";mSmallTogglePanelHeight="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";mQSContainer.getY()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v5, v5, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mQSContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getY()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";mNotificationStack.getY()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v5, v5, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mNotificationStack:Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;->getY()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v3, "expandable_toggle"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "TogglePanelTouchHelper.adjustTogglePanelHeight() mNotificationStack.getY()="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v5, v5, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mNotificationStack:Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;->getY()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method endMotionEvent(Landroid/view/MotionEvent;)V
    .locals 7

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    iget v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mExpandableTogglesLayoutHeight:I

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v5

    if-eq v4, v5, :cond_2

    iget v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mExpandableTogglesLayoutHeight:I

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v5

    if-ne v4, v5, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_5

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get13(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Landroid/view/VelocityTracker;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get13(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Landroid/view/VelocityTracker;

    move-result-object v4

    const/16 v5, 0x3e8

    invoke-virtual {v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get13(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Landroid/view/VelocityTracker;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    :cond_0
    const/4 v4, 0x0

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_4

    :goto_1
    invoke-virtual {p0, v1, v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->fling(FZ)V

    :goto_2
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get13(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Landroid/view/VelocityTracker;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get13(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Landroid/view/VelocityTracker;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->recycle()V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2, v6}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-set2(Lcom/android/systemui/statusbar/phone/NotificationPanelView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap2(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)V

    return-void

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_1

    :cond_5
    iget v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mExpandableTogglesLayoutHeight:I

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v5}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v5

    if-ne v4, v5, :cond_6

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iput-boolean v3, v2, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mTogglePanelExpanded:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mTogglePanelExpandAdjusting:Z

    goto :goto_2

    :cond_6
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iput-boolean v2, v4, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mTogglePanelExpanded:Z

    iput-boolean v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mTogglePanelExpandAdjusting:Z

    goto :goto_2
.end method

.method fling(FZ)V
    .locals 7

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mQSContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get5(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v1

    int-to-float v1, v1

    add-float v3, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v6

    :goto_0
    const-string/jumbo v0, "expandable_toggle"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "TogglePanelTouchHelper.fling() targetY="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";togglePanelTargetHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";mLargeQSContainerHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get5(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";mSmallQSContainerHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get9(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";mLargeTogglePanelHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";mSmallTogglePanelHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";mQSContainer.getY()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mQSContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getY()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";mQSContainer.getY()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mQSContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getY()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3, v6}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->createExpandAnimator(FI)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get2(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Lcom/android/systemui/statusbar/FlingAnimationUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mNotificationStack:Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/stack/NotificationStackScrollLayout;->getY()F

    move-result v2

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mQSContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v4

    int-to-float v5, v4

    move v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/FlingAnimationUtils;->apply(Landroid/animation/Animator;FFFF)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper$2;-><init>(Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mQSContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get9(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v1

    int-to-float v1, v1

    add-float v3, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v6

    goto/16 :goto_0
.end method

.method getExpandTogglesButtonListener()Landroid/view/View$OnTouchListener;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mExpandTogglesButtonListener:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method isTogglePanelExpandAnimating()Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2, v5}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-set1(Lcom/android/systemui/statusbar/phone/NotificationPanelView;Z)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    return v5

    :pswitch_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap3(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2, p1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap5(Lcom/android/systemui/statusbar/phone/NotificationPanelView;Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get4(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)F

    move-result v2

    sub-float v0, v1, v2

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get12(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v3}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get11(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)F

    move-result v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mNotificationYWhenDown:F

    invoke-virtual {v2, v3, v4, v0}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->adjustTogglePanelHeight(FFF)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2, p1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap5(Lcom/android/systemui/statusbar/phone/NotificationPanelView;Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get12(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->endMotionEvent(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method setExpandStatus(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get1(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get6(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap8(Lcom/android/systemui/statusbar/phone/NotificationPanelView;Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getExpandTogglesButton()Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->setRowHeightPercent(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iput-boolean p1, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mTogglePanelExpanded:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get1(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)Lcom/android/systemui/statusbar/phone/ExpandableTogglesLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-get10(Lcom/android/systemui/statusbar/phone/NotificationPanelView;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->-wrap8(Lcom/android/systemui/statusbar/phone/NotificationPanelView;Landroid/view/View;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->this$0:Lcom/android/systemui/statusbar/phone/NotificationPanelView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/NotificationPanelView;->mHeader:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->getExpandTogglesButton()Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/ExpandableTogglesButton;->setRowHeightPercent(I)V

    goto :goto_0
.end method

.method stopAnimation()V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationPanelView$TogglePanelTouchHelper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    return-void
.end method
