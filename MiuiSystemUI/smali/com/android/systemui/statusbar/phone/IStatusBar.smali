.class public interface abstract Lcom/android/systemui/statusbar/phone/IStatusBar;
.super Ljava/lang/Object;
.source "IStatusBar.java"


# virtual methods
.method public abstract addIcon(Ljava/lang/String;IILcom/android/systemui/statusbar/ExpandedIcon;)V
.end method

.method public abstract removeIcon(Ljava/lang/String;II)V
.end method

.method public abstract setBatteryController(Lcom/android/systemui/statusbar/policy/BatteryController;)V
.end method

.method public abstract setNetworkController(Lcom/android/systemui/statusbar/policy/NetworkController;)V
.end method

.method public abstract startQuietModeIconAnim(Z)V
.end method

.method public abstract updateIcon(Ljava/lang/String;IILcom/android/systemui/statusbar/ExpandedIcon;Lcom/android/systemui/statusbar/ExpandedIcon;)V
.end method

.method public abstract updateNotificationIcons(ZLjava/util/ArrayList;Landroid/widget/LinearLayout$LayoutParams;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/LinearLayout$LayoutParams;",
            ")V"
        }
    .end annotation
.end method

.method public abstract updateViewsInStatusBar()V
.end method
