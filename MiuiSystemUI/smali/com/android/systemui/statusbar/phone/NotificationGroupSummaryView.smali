.class public Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;
.super Landroid/widget/LinearLayout;
.source "NotificationGroupSummaryView.java"


# instance fields
.field private mChild0:Landroid/view/View;

.field private mChild1:Landroid/view/View;

.field private mChild2:Landroid/view/View;

.field private mContainer:Landroid/view/View;

.field private mExpandTips:Landroid/view/View;

.field private mIcon:Landroid/widget/ImageView;

.field private mOverflow:Landroid/widget/TextView;

.field private mParent:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

.field private mSubtext0:Landroid/widget/TextView;

.field private mSubtext1:Landroid/widget/TextView;

.field private mSubtext2:Landroid/widget/TextView;

.field private mSubtitle0:Landroid/widget/TextView;

.field private mSubtitle1:Landroid/widget/TextView;

.field private mSubtitle2:Landroid/widget/TextView;

.field private mTime:Landroid/widget/DateTimeView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mOverflow:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtitle2:Landroid/widget/TextView;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private resolveText(Landroid/app/Notification;)Ljava/lang/CharSequence;
    .locals 3

    iget-object v1, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string/jumbo v2, "android.text"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string/jumbo v2, "android.bigText"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private resolveTime(Landroid/app/Notification;)J
    .locals 4

    iget-wide v0, p1, Landroid/app/Notification;->when:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p1, Landroid/app/Notification;->when:J

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method private resolveTitle(Landroid/app/Notification;)Ljava/lang/CharSequence;
    .locals 3

    iget-object v1, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string/jumbo v2, "android.title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string/jumbo v2, "android.title.big"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private updateChildrenSummary()V
    .locals 15

    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mParent:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v10}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getGroupChildren()Ljava/util/ArrayList;

    move-result-object v1

    new-array v0, v14, [Landroid/view/View;

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mChild0:Landroid/view/View;

    aput-object v10, v0, v11

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mChild1:Landroid/view/View;

    aput-object v10, v0, v12

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mChild2:Landroid/view/View;

    aput-object v10, v0, v13

    new-array v7, v14, [Landroid/widget/TextView;

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtitle0:Landroid/widget/TextView;

    aput-object v10, v7, v11

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtitle1:Landroid/widget/TextView;

    aput-object v10, v7, v12

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtitle2:Landroid/widget/TextView;

    aput-object v10, v7, v13

    new-array v6, v14, [Landroid/widget/TextView;

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtext0:Landroid/widget/TextView;

    aput-object v10, v6, v11

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtext1:Landroid/widget/TextView;

    aput-object v10, v6, v12

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtext2:Landroid/widget/TextView;

    aput-object v10, v6, v13

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    :cond_0
    :goto_0
    array-length v10, v7

    if-ge v2, v10, :cond_4

    if-ge v3, v5, :cond_4

    add-int/lit8 v10, v5, -0x1

    sub-int/2addr v10, v3

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/systemui/statusbar/NotificationData$Entry;

    iget-object v10, v10, Lcom/android/systemui/statusbar/NotificationData$Entry;->row:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v10}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getNotification()Lcom/android/systemui/statusbar/ExpandedNotification;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->resolveTitle(Landroid/app/Notification;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-direct {p0, v4}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->resolveText(Landroid/app/Notification;)Ljava/lang/CharSequence;

    move-result-object v8

    add-int/lit8 v3, v3, 0x1

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    :cond_1
    if-eqz v9, :cond_2

    aget-object v10, v7, v2

    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    xor-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_2

    aget-object v10, v7, v2

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    if-eqz v8, :cond_3

    aget-object v10, v6, v2

    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    xor-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_3

    aget-object v10, v6, v2

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    aget-object v10, v0, v2

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    :goto_1
    array-length v10, v0

    if-ge v2, v10, :cond_5

    aget-object v10, v0, v2

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    return-void
.end method

.method private updateIcon()V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mParent:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getPublicLayout()Landroid/view/View;

    move-result-object v1

    const v2, 0x1020006

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method private updateOverflow()V
    .locals 8

    const/4 v7, 0x1

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mParent:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getGroupChildrenCount()I

    move-result v3

    add-int/lit8 v1, v3, -0x3

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mOverflow:Landroid/widget/TextView;

    if-lez v1, :cond_3

    move v3, v4

    :goto_0
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setVisibility(I)V

    if-lez v1, :cond_1

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    const v6, 0x7f0d0329

    invoke-virtual {v3, v6, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mOverflow:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mOverflow:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f110009

    invoke-virtual {v3, v5, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mOverflow:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mOverflow:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mOverflow:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mOverflow:Landroid/widget/TextView;

    new-instance v4, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView$1;

    invoke-direct {v4, p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView$1;-><init>(Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void

    :cond_3
    const/16 v3, 0x8

    goto :goto_0
.end method

.method private updateTime()V
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mTime:Landroid/widget/DateTimeView;

    invoke-virtual {v0}, Landroid/widget/DateTimeView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mTime:Landroid/widget/DateTimeView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mParent:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getNotification()Lcom/android/systemui/statusbar/ExpandedNotification;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/ExpandedNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->resolveTime(Landroid/app/Notification;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/widget/DateTimeView;->setTime(J)V

    :cond_0
    return-void
.end method

.method private updateVisibility()V
    .locals 8

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x3

    new-array v1, v3, [Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mChild0:Landroid/view/View;

    aput-object v3, v1, v4

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mChild1:Landroid/view/View;

    const/4 v6, 0x1

    aput-object v3, v1, v6

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mChild2:Landroid/view/View;

    const/4 v6, 0x2

    aput-object v3, v1, v6

    const/4 v2, 0x0

    array-length v6, v1

    move v3, v4

    :goto_0
    if-ge v3, v6, :cond_0

    aget-object v0, v1, v3

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    const/4 v2, 0x1

    :cond_0
    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mContainer:Landroid/view/View;

    if-eqz v2, :cond_2

    move v3, v4

    :goto_1
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mExpandTips:Landroid/view/View;

    if-nez v2, :cond_3

    :goto_2
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move v3, v5

    goto :goto_1

    :cond_3
    move v4, v5

    goto :goto_2
.end method


# virtual methods
.method public getNaturalHeight()I
    .locals 5

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b002f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v3, -0x2

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getMeasuredWidth()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v4, -0x80000000

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->measure(II)V

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->getMeasuredHeight()I

    move-result v3

    return v3
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0f0030

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f004c

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f0f0098

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DateTimeView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mTime:Landroid/widget/DateTimeView;

    const v0, 0x7f0f00c2

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mChild0:Landroid/view/View;

    const v0, 0x7f0f00c5

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mChild1:Landroid/view/View;

    const v0, 0x7f0f00c8

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mChild2:Landroid/view/View;

    const v0, 0x7f0f00c3

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtitle0:Landroid/widget/TextView;

    const v0, 0x7f0f00c6

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtitle1:Landroid/widget/TextView;

    const v0, 0x7f0f00ca

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtitle2:Landroid/widget/TextView;

    const v0, 0x7f0f00c4

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtext0:Landroid/widget/TextView;

    const v0, 0x7f0f00c7

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtext1:Landroid/widget/TextView;

    const v0, 0x7f0f00cb

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mSubtext2:Landroid/widget/TextView;

    const v0, 0x7f0f00c9

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mOverflow:Landroid/widget/TextView;

    const v0, 0x7f0f00c1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mContainer:Landroid/view/View;

    const v0, 0x7f0f00cc

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mExpandTips:Landroid/view/View;

    return-void
.end method

.method public refresh()V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->updateIcon()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->updateTitle()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->updateTime()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->updateChildrenSummary()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->updateOverflow()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->updateVisibility()V

    return-void
.end method

.method public setParent(Lcom/android/systemui/statusbar/ExpandableNotificationRow;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mParent:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    return-void
.end method

.method public updateTitle()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mParent:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getGroupChildrenCount()I

    move-result v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const v5, 0x7f110008

    invoke-virtual {v3, v5, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mParent:Lcom/android/systemui/statusbar/ExpandableNotificationRow;

    invoke-virtual {v5}, Lcom/android/systemui/statusbar/ExpandableNotificationRow;->getAppName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v1, v4, v7

    const v5, 0x7f0d0328

    invoke-virtual {v3, v5, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/NotificationGroupSummaryView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
