.class Lcom/android/systemui/statusbar/phone/TogglesContainer$6;
.super Ljava/lang/Object;
.source "TogglesContainer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/TogglesContainer;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$6;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$6;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpanded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$6;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, p0, v2, v3}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$6;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mToggleManager:Lmiui/app/ToggleManager;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lmiui/app/ToggleManager;->performToggle(I)Z

    goto :goto_0
.end method
