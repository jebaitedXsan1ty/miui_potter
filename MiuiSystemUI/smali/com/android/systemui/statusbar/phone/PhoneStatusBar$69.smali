.class Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;
.super Ljava/lang/Object;
.source "PhoneStatusBar.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->setImportanceDialog(Lcom/android/systemui/statusbar/ExpandedNotification;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field final synthetic val$filterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

.field final synthetic val$notification:Lcom/android/systemui/statusbar/ExpandedNotification;

.field final synthetic val$pkg:Ljava/lang/String;

.field final synthetic val$userFold:Z

.field final synthetic val$value:I


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;ZLjava/lang/String;ILcom/android/systemui/statusbar/ExpandedNotification;Lcom/android/systemui/statusbar/policy/FilterInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iput-boolean p2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$userFold:Z

    iput-object p3, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$pkg:Ljava/lang/String;

    iput p4, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$value:I

    iput-object p5, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    iput-object p6, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$filterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$userFold:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$pkg:Ljava/lang/String;

    iget v2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$value:I

    invoke-static {v0, v1, v2}, Lcom/android/systemui/statusbar/phone/rank/ImportanceUtil;->setImportance(Landroid/content/Context;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$pkg:Ljava/lang/String;

    iget v3, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$value:I

    invoke-static {v0, v1, v2, v3}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->-wrap12(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;Lcom/android/systemui/statusbar/ExpandedNotification;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$notification:Lcom/android/systemui/statusbar/ExpandedNotification;

    invoke-static {v0, v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->-wrap32(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;Lcom/android/systemui/statusbar/ExpandedNotification;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$69;->val$filterInfo:Lcom/android/systemui/statusbar/policy/FilterInfo;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->blockAndTrackNotification(Lcom/android/systemui/statusbar/policy/FilterInfo;)V

    goto :goto_0
.end method
