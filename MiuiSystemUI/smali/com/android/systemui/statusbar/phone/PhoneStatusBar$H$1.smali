.class Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;
.super Ljava/lang/Object;
.source "PhoneStatusBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;

.field final synthetic val$adjust:Z

.field final synthetic val$pkg:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;ZLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;

    iput-boolean p2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->val$adjust:Z

    iput-object p3, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->val$pkg:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->-get29(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)Lmiui/app/AlertDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    new-instance v0, Lmiui/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mContext:Landroid/content/Context;

    sget v3, Lmiui/R$style;->Theme_Light_Dialog_Alert:I

    invoke-direct {v0, v2, v3}, Lmiui/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmiui/app/AlertDialog$Builder;->setCancelable(Z)Lmiui/app/AlertDialog$Builder;

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->val$adjust:Z

    if-eqz v2, :cond_1

    const v2, 0x7f0d0267

    :goto_0
    invoke-virtual {v0, v2}, Lmiui/app/AlertDialog$Builder;->setTitle(I)Lmiui/app/AlertDialog$Builder;

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->val$adjust:Z

    if-eqz v2, :cond_2

    const v2, 0x7f0d0268

    :goto_1
    invoke-virtual {v0, v2}, Lmiui/app/AlertDialog$Builder;->setMessage(I)Lmiui/app/AlertDialog$Builder;

    const v2, 0x7f0d026b

    invoke-virtual {v0, v2, v4}, Lmiui/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    new-instance v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1$1;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->val$pkg:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->val$adjust:Z

    invoke-direct {v2, p0, v3, v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1$1;-><init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;Ljava/lang/String;Z)V

    const v3, 0x7f0d026c

    invoke-virtual {v0, v3, v2}, Lmiui/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiui/app/AlertDialog$Builder;

    invoke-virtual {v0}, Lmiui/app/AlertDialog$Builder;->create()Lmiui/app/AlertDialog;

    move-result-object v1

    new-instance v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1$2;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1$2;-><init>(Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;)V

    invoke-virtual {v1, v2}, Lmiui/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v1}, Lmiui/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d3

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v1}, Lmiui/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addExtraFlags(I)V

    invoke-virtual {v1}, Lmiui/app/AlertDialog;->show()V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-static {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->-get14(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;)Lcom/android/systemui/statusbar/BaseStatusBar$H;

    move-result-object v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;

    iget-object v3, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v3, v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mAutoHideSpecialModeToggleRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/BaseStatusBar$H;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H$1;->this$1:Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar$H;->this$0:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-static {v2, v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->-set12(Lcom/android/systemui/statusbar/phone/PhoneStatusBar;Lmiui/app/AlertDialog;)Lmiui/app/AlertDialog;

    return-void

    :cond_1
    const v2, 0x7f0d0269

    goto :goto_0

    :cond_2
    const v2, 0x7f0d026a

    goto :goto_1
.end method
