.class Lcom/android/systemui/statusbar/phone/TogglesContainer$4;
.super Ljava/lang/Object;
.source "TogglesContainer.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/TogglesContainer;->startAnimatorIfNeed(Landroid/widget/ImageView;IZ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field imageChanged:Z

.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

.field final synthetic val$id:I

.field final synthetic val$imageView:Landroid/widget/ImageView;

.field final synthetic val$isOpen:Z


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;Landroid/widget/ImageView;IZ)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iput-object p2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$imageView:Landroid/widget/ImageView;

    iput p3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$id:I

    iput-boolean p4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$isOpen:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->imageChanged:Z

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v2, 0x3f000000    # 0.5f

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$imageView:Landroid/widget/ImageView;

    sub-float v3, v4, v1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$imageView:Landroid/widget/ImageView;

    sub-float v3, v4, v1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->imageChanged:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$id:I

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$isOpen:Z

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    invoke-static {v4}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->-get0(Lcom/android/systemui/statusbar/phone/TogglesContainer;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lmiui/app/ToggleManager;->getImageDrawable(IZLandroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    invoke-static {v3}, Lcom/android/systemui/statusbar/phone/TogglesContainer;->-get0(Lcom/android/systemui/statusbar/phone/TogglesContainer;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/ColorSuits;->enableImageColorDye(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/ColorSuits;->getImageColor()I

    move-result v2

    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/ColorSuits;->getImageColor()I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_1
    const-string/jumbo v2, "SystemUI.toggleContainer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateToggleImage for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$id:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " status:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$id:I

    invoke-static {v4}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " last:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v4, v4, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    iget v5, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$id:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " isOpen:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$isOpen:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$imageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->imageChanged:Z

    :cond_2
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$imageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$4;->val$imageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    goto/16 :goto_0
.end method
