.class public Lcom/android/systemui/statusbar/phone/SimpleStatusBar;
.super Landroid/widget/FrameLayout;
.source "SimpleStatusBar.java"

# interfaces
.implements Lcom/android/systemui/statusbar/phone/IStatusBar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;,
        Lcom/android/systemui/statusbar/phone/SimpleStatusBar$2;,
        Lcom/android/systemui/statusbar/phone/SimpleStatusBar$3;
    }
.end annotation


# static fields
.field private static sFilterColor:I

.field private static sNotch:Z


# instance fields
.field mBatteryChargingIcon:Landroid/widget/ImageView;

.field mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

.field mBatteryNum:Landroid/widget/TextView;

.field private mChargingStateListener:Lcom/android/systemui/statusbar/policy/BatteryController$ChargingStateListener;

.field private mClockShow:Z

.field private mDarkMode:Z

.field private mDemoMode:Z

.field mIcons:Landroid/view/View;

.field private mIconsDarkArea:Landroid/graphics/Rect;

.field private mIsExpand:Z

.field private mLayoutDirection:I

.field private final mMakeIconsInvisible:Landroid/animation/Animator$AnimatorListener;

.field mMoreIcon:Landroid/widget/ImageView;

.field mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

.field mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

.field mNotificationIconCluster:Landroid/view/View;

.field mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

.field mNotificationMoreIcon:Landroid/widget/ImageView;

.field private mQuickCharging:Z

.field private mReturnToMultiMode:Z

.field mReturnToMultiModeText:Landroid/widget/TextView;

.field mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mShowClock:Z

.field private mShowNetworkSpeed:Z

.field mSignalClusterContainer:Landroid/view/View;

.field mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

.field mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

.field mStatusBarIcons:Lcom/android/systemui/statusbar/phone/StatusBarIcons;

.field mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

.field mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

.field mStatusIcons:Landroid/widget/LinearLayout;

.field private mUpdateQuickChargingTask:Ljava/lang/Runnable;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mClockShow:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mUpdateQuickChargingTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    const/4 v0, 0x0

    sput v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sFilterColor:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIsExpand:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mShowNetworkSpeed:Z

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mShowClock:Z

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mQuickCharging:Z

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    new-instance v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$1;-><init>(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mChargingStateListener:Lcom/android/systemui/statusbar/policy/BatteryController$ChargingStateListener;

    new-instance v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$2;-><init>(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mUpdateQuickChargingTask:Ljava/lang/Runnable;

    iput v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mLayoutDirection:I

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mClockShow:Z

    new-instance v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$3;-><init>(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMakeIconsInvisible:Landroid/animation/Animator$AnimatorListener;

    return-void
.end method

.method private findQuietModeIconView()Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/statusbar/StatusBarIconView;

    const-string/jumbo v2, "quiet"

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/StatusBarIconView;->getSlot()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    return-object v2
.end method

.method private getIconsDarkArea()Landroid/graphics/Rect;
    .locals 3

    invoke-static {}, Lcom/android/systemui/recents/misc/Utilities;->isAndroidNorNewer()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getId()I

    move-result v1

    const v2, 0x7f0f015c

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-object v0, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getId()I

    move-result v1

    const v2, 0x7f0f017f

    if-ne v1, v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDarkMode()Z
    .locals 3

    invoke-static {}, Lcom/android/systemui/recents/misc/Utilities;->isAndroidNorNewer()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getId()I

    move-result v1

    const v2, 0x7f0f015c

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v0, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mDarkMode:Z

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getId()I

    move-result v1

    const v2, 0x7f0f017f

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v0, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedDarkMode:Z

    goto :goto_0
.end method

.method private loadAnim(ILandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;
    .locals 2

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    :cond_0
    return-object v0
.end method

.method private setReturnToMultiModeTextPosition()V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    instance-of v1, v1, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    const v2, 0x7f0d033b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method

.method private updateBatteryChargingIcon()V
    .locals 4

    sget-boolean v1, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v1, :cond_0

    const-string/jumbo v1, "SimpleStatusBar"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateBatteryChargingIcon() mQuickCharging="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mQuickCharging:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; mDarkMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mQuickCharging:Z

    if-eqz v1, :cond_1

    const v1, 0x7f020267

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryChargingIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateIcon(Landroid/widget/ImageView;I)V

    :cond_0
    return-void

    :cond_1
    const v1, 0x7f02022e

    goto :goto_0
.end method

.method private updateIcon(Landroid/widget/ImageView;I)V
    .locals 5

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {p1, v3, v4}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/android/systemui/statusbar/Icons;->get(Ljava/lang/Integer;Z)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v1, :cond_1

    sget-boolean v3, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sMiuiOptimizationDisabled:Z

    if-eqz v3, :cond_1

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x19

    if-le v3, v4, :cond_1

    sget v3, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sFilterColor:I

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090025

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sFilterColor:I

    :cond_0
    sget v3, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sFilterColor:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_1
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public addIcon(Ljava/lang/String;IILcom/android/systemui/statusbar/ExpandedIcon;)V
    .locals 3

    sget-boolean v1, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v1, :cond_0

    new-instance v0, Lcom/android/systemui/statusbar/StatusBarIconView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/android/systemui/statusbar/StatusBarIconView;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/systemui/statusbar/ExpandedNotification;)V

    invoke-virtual {v0, p4}, Lcom/android/systemui/statusbar/StatusBarIconView;->set(Lcom/android/systemui/statusbar/ExpandedIcon;)Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->isDarkMode()Z

    move-result v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/StatusBarIconView;->updateDarkMode(Z)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public disable(III)V
    .locals 8

    const-wide/16 v6, 0xaf

    const/high16 v3, 0x100000

    const/high16 v4, 0x3fc00000    # 1.5f

    const/4 v2, 0x0

    const/4 v1, 0x0

    and-int v0, p1, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    and-int v0, p2, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p3

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMakeIconsInvisible:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method public dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0x8

    const-string/jumbo v0, "demo_mode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SimpleStatusBar mDemoMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDemoMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", command = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDemoMode:Z

    if-nez v0, :cond_1

    const-string/jumbo v0, "enter"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDemoMode:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIconCluster:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, v4}, Lcom/android/systemui/statusbar/SignalClusterView;->updateLabelVisible(Z)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/NetworkSpeedView;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/phone/BatteryIndicator;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDemoMode:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "exit"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDemoMode:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateViewsInStatusBar()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, p1, p2}, Lcom/android/systemui/statusbar/SignalClusterView;->dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public getMarqueScrollView()Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    return-object v0
.end method

.method public initDisplay(ZZ)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIsExpand:Z

    iput-boolean p2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mShowNetworkSpeed:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateDarkMode()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateViewsInStatusBar()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateQuickCharging()V

    return-void
.end method

.method public isQuickCharging()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mQuickCharging:Z

    return v0
.end method

.method public onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 9

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0f01ab

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/SignalClusterView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/SignalClusterView;->setSlotId(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/SignalClusterView;->setCardSlot(I)V

    const v0, 0x7f0f01aa

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterContainer:Landroid/view/View;

    const v0, 0x7f0f01ae

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/NetworkSpeedView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    const v0, 0x7f0f01ad

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    const v0, 0x7f0f01a6

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/IconMerger;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    const v0, 0x7f0f01a5

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIconCluster:Landroid/view/View;

    const v0, 0x7f0f01af

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f01a7

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationMoreIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f01a4

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    const v0, 0x7f0f01bd

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    const v0, 0x7f0f01a8

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/StatusBarIcons;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBarIcons:Lcom/android/systemui/statusbar/phone/StatusBarIcons;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBarIcons:Lcom/android/systemui/statusbar/phone/StatusBarIcons;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->setOverflowIndicator(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationMoreIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBarIcons:Lcom/android/systemui/statusbar/phone/StatusBarIcons;

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/systemui/statusbar/phone/IconMerger;->setOverflowIndicator(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    const v0, 0x7f0f002b

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    const v0, 0x7f0f01a3

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    const v0, 0x7f0f002c

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryNum:Landroid/widget/TextView;

    const v0, 0x7f0f01b2

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    invoke-static {}, Lcom/android/systemui/recents/misc/Utilities;->isAndroidNorNewer()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->setReturnToMultiModeTextPosition()V

    const v0, 0x7f0f01a9

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryChargingIcon:Landroid/widget/ImageView;

    const v0, 0x7f0f01ac

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/SignalClusterView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, v7}, Lcom/android/systemui/statusbar/SignalClusterView;->setSlotId(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, v7}, Lcom/android/systemui/statusbar/SignalClusterView;->setCardSlot(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/SignalClusterView;->setWifiNeedVisible(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/SignalClusterView;->setAirplaneModeNeedVisible(Z)V

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0f01c1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/NotchSignalClusterView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->setSimpleStatusBar(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->setSlotId(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->setCardSlot(I)V

    :cond_1
    new-instance v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$4;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$4;-><init>(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)V

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-virtual {v0, v6}, Lcom/android/systemui/statusbar/NetworkSpeedView;->canShow(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v0, v8}, Lcom/android/systemui/statusbar/phone/IconMerger;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    invoke-virtual {v0, v8}, Lcom/android/systemui/statusbar/phone/BatteryIndicator;->setVisibility(I)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-virtual {v0, v7}, Lcom/android/systemui/statusbar/NetworkSpeedView;->canShow(Z)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/IconMerger;->forceLayout()V

    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRtlPropertiesChanged(I)V

    iget v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mLayoutDirection:I

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :goto_0
    iput p1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mLayoutDirection:I

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBarIcons:Lcom/android/systemui/statusbar/phone/StatusBarIcons;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/StatusBarIcons;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public refreshClockVisible()V
    .locals 8

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getId()I

    move-result v2

    const v5, 0x7f0f015c

    if-ne v2, v5, :cond_7

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mDisabled:I

    const/high16 v5, 0x800000

    and-int/2addr v2, v5

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowPrompt:Z

    xor-int/lit8 v1, v2, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v2, v4}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->setSimple(Z)V

    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isUnderKeyguard()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->setShowDate(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v2, v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowCarrierStyle:I

    if-nez v2, :cond_8

    const/4 v1, 0x1

    :cond_1
    :goto_2
    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    :cond_2
    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mClockShow:Z

    if-eq v2, v1, :cond_5

    if-eqz v1, :cond_9

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->setVisibility(I)V

    :cond_3
    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    const-string/jumbo v6, "transitionAlpha"

    const/4 v2, 0x2

    new-array v7, v2, [F

    if-eqz v1, :cond_a

    move v2, v3

    :goto_3
    int-to-float v2, v2

    aput v2, v7, v3

    if-eqz v1, :cond_4

    move v3, v4

    :cond_4
    int-to-float v2, v3

    aput v2, v7, v4

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v2, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$6;

    invoke-direct {v2, p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$6;-><init>(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mClockShow:Z

    :cond_5
    return-void

    :cond_6
    const/4 v1, 0x0

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getId()I

    move-result v2

    const v5, 0x7f0f017f

    if-ne v2, v5, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->setShowDate(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->setSimple(Z)V

    const/4 v1, 0x1

    goto :goto_1

    :cond_8
    const/4 v1, 0x0

    goto :goto_2

    :cond_9
    sget-boolean v2, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isUnderKeyguard()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mClockShow:Z

    return-void

    :cond_a
    move v2, v4

    goto :goto_3
.end method

.method public removeIcon(Ljava/lang/String;II)V
    .locals 1

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p3}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    :cond_0
    return-void
.end method

.method public setBatteryController(Lcom/android/systemui/statusbar/policy/BatteryController;)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addIconView(Lcom/android/systemui/statusbar/policy/BatteryController$BatteryStateChangeCallback;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addIconView(Lcom/android/systemui/statusbar/policy/BatteryController$BatteryStateChangeCallback;)V

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryChargingIcon:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addChargingIconView(Landroid/widget/ImageView;)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryNum:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addLabelView(Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mChargingStateListener:Lcom/android/systemui/statusbar/policy/BatteryController$ChargingStateListener;

    invoke-virtual {p1, v0}, Lcom/android/systemui/statusbar/policy/BatteryController;->addChargingStateListener(Lcom/android/systemui/statusbar/policy/BatteryController$ChargingStateListener;)V

    return-void
.end method

.method public setIconsPadding(I)V
    .locals 4

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingStart()I

    move-result v1

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIcons:Landroid/view/View;

    invoke-virtual {v3, v1, v2, p1, v0}, Landroid/view/View;->setPaddingRelative(IIII)V

    return-void
.end method

.method public setNetworkController(Lcom/android/systemui/statusbar/policy/NetworkController;)V
    .locals 1

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/SignalClusterView;->setNetworkController(Lcom/android/systemui/statusbar/policy/NetworkController;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/SignalClusterView;->setNetworkController(Lcom/android/systemui/statusbar/policy/NetworkController;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->setNetworkController(Lcom/android/systemui/statusbar/policy/NetworkController;)V

    goto :goto_0
.end method

.method public setReturnToMultiMode(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateDarkMode()V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateViewsInStatusBar()V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->setClickable(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->setClickable(Z)V

    goto :goto_0
.end method

.method public setSignalClusterViewId(II)V
    .locals 1

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/SignalClusterView;->setViewId(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    invoke-virtual {v0, p2}, Lcom/android/systemui/statusbar/SignalClusterView;->setViewId(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->setViewId(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    invoke-virtual {v0, p2}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->setViewId2(I)V

    goto :goto_0
.end method

.method public startQuietModeIconAnim(Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->findQuietModeIconView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const v1, 0x7f04000b

    invoke-direct {p0, v1, v2}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->loadAnim(ILandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_0
.end method

.method public updateDarkMode()V
    .locals 10

    const v3, 0x7f090025

    const v5, 0x7f090009

    const v9, 0x7f020216

    const v4, 0x7f090008

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->isDarkMode()Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getIconsDarkArea()Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    const-string/jumbo v2, "SimpleStatusBar"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateDarkMode() mDarkMode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v2, "SimpleStatusBar"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "updateDarkMode() mIconsDarkArea="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v2, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/SignalClusterView;->updateDarkMode(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v2}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isMsim()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/SignalClusterView;->updateDarkMode(Z)V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/NetworkSpeedView;->updateDarkMode(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusClock:Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/policy/widget/MarqueScrollView;->updateDarkMode(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->updateDarkMode(Z)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_1
    if-ltz v0, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/systemui/statusbar/StatusBarIconView;

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v1, v2, v6}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/StatusBarIconView;->updateDarkMode(Z)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateDarkMode(Z)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v9}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationMoreIcon:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v9}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateIcon(Landroid/widget/ImageView;I)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateBatteryChargingIcon()V

    sget-boolean v2, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sMiuiOptimizationDisabled:Z

    if-eqz v2, :cond_5

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x19

    if-le v2, v6, :cond_5

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryNum:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryNum:Landroid/widget/TextView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v2, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    :goto_2
    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_4

    :goto_3
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_4
    return-void

    :cond_3
    move v2, v4

    goto :goto_2

    :cond_4
    move v3, v4

    goto :goto_3

    :cond_5
    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryNum:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryNum:Landroid/widget/TextView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v2, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v5

    :goto_5
    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v6, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDarkMode:Z

    iget-object v8, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v6, v7, v8}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_7

    :goto_6
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_4

    :cond_6
    move v2, v4

    goto :goto_5

    :cond_7
    move v5, v4

    goto :goto_6
.end method

.method public updateIcon(Ljava/lang/String;IILcom/android/systemui/statusbar/ExpandedIcon;Lcom/android/systemui/statusbar/ExpandedIcon;)V
    .locals 3

    sget-boolean v1, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/StatusBarIconView;

    invoke-virtual {v0, p5}, Lcom/android/systemui/statusbar/StatusBarIconView;->set(Lcom/android/systemui/statusbar/ExpandedIcon;)Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->isDarkMode()Z

    move-result v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIconsDarkArea:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2}, Lcom/android/systemui/Util;->isDarkModeWithDarkArea(Landroid/view/View;ZLandroid/graphics/Rect;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/StatusBarIconView;->updateDarkMode(Z)V

    :cond_0
    return-void
.end method

.method public updateNotificationIcons(ZLjava/util/ArrayList;Landroid/widget/LinearLayout$LayoutParams;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/widget/LinearLayout$LayoutParams;",
            ")V"
        }
    .end annotation

    sget-boolean v9, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v9, :cond_8

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getId()I

    move-result v9

    const v10, 0x7f0f015c

    if-eq v9, v10, :cond_0

    return-void

    :cond_0
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v9, v9, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowNotificationIcons:Z

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/android/systemui/statusbar/phone/IconMerger;->setVisibility(I)V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_0
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9}, Lcom/android/systemui/statusbar/phone/IconMerger;->getChildCount()I

    move-result v9

    if-ge v4, v9, :cond_2

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v4}, Lcom/android/systemui/statusbar/phone/IconMerger;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v5}, Lcom/android/systemui/statusbar/phone/IconMerger;->removeView(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_5

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    if-nez v9, :cond_4

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v8, v4, p3}, Lcom/android/systemui/statusbar/phone/IconMerger;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9}, Lcom/android/systemui/statusbar/phone/IconMerger;->getChildCount()I

    move-result v2

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v2, :cond_8

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v4}, Lcom/android/systemui/statusbar/phone/IconMerger;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-ne v0, v3, :cond_6

    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v3}, Lcom/android/systemui/statusbar/phone/IconMerger;->removeView(Landroid/view/View;)V

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    invoke-virtual {v9, v3, v4}, Lcom/android/systemui/statusbar/phone/IconMerger;->addView(Landroid/view/View;I)V

    goto :goto_4

    :cond_7
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/android/systemui/statusbar/phone/IconMerger;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIcons:Lcom/android/systemui/statusbar/phone/IconMerger;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/android/systemui/statusbar/phone/IconMerger;->setForceShowingMore(Z)V

    iget-object v10, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_9

    const/4 v9, 0x0

    :goto_5
    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_8
    return-void

    :cond_9
    const/16 v9, 0x8

    goto :goto_5
.end method

.method public updateQuickCharging()V
    .locals 3

    new-instance v0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$5;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$5;-><init>(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar$5;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public updateQuickCharging(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mQuickCharging:Z

    const-string/jumbo v0, "SimpleStatusBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateQuickCharging() mQuickCharging="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mQuickCharging:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->updateBatteryChargingIcon()V

    return-void
.end method

.method public updateQuickChargingDelayed(J)V
    .locals 3

    const-string/jumbo v0, "SimpleStatusBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateQuickChargingDelayed() delayMillis="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mUpdateQuickChargingTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, p1, p2}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public updateViewsInStatusBar()V
    .locals 11

    const/4 v10, 0x2

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mDemoMode:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->refreshClockVisible()V

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v0, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mDisabled:I

    and-int/lit8 v4, v0, 0x20

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->disable(Z)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/phone/BatteryIndicator;->disable(Z)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterContainer:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    invoke-virtual {v4, v6}, Lcom/android/systemui/statusbar/NetworkSpeedView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->refreshClockVisible()V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIconCluster:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isUnderKeyguard()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->getId()I

    move-result v4

    const v9, 0x7f0f015c

    if-ne v4, v9, :cond_9

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    sget-boolean v9, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowBatteryUnderKeyguard:Z

    xor-int/lit8 v9, v9, 0x1

    invoke-virtual {v4, v9}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->disable(Z)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    sget-boolean v9, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowBatteryUnderKeyguard:Z

    xor-int/lit8 v9, v9, 0x1

    invoke-virtual {v4, v9}, Lcom/android/systemui/statusbar/phone/BatteryIndicator;->disable(Z)V

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterContainer:Landroid/view/View;

    sget-boolean v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowSignalUnderKeyguard:Z

    if-eqz v4, :cond_3

    move v4, v5

    :goto_1
    invoke-virtual {v9, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    sget-boolean v9, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowSignalUnderKeyguard:Z

    xor-int/lit8 v9, v9, 0x1

    invoke-virtual {v4, v9}, Lcom/android/systemui/statusbar/NetworkSpeedView;->requestHideByKeyguard(Z)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->refreshClockVisible()V

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    sget-boolean v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowStatusUnderKeyguard:Z

    if-eqz v4, :cond_4

    move v4, v5

    :goto_2
    invoke-virtual {v9, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    sget-boolean v4, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v4, :cond_2

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowCarrierStyle:I

    if-eq v4, v10, :cond_5

    move v4, v7

    :goto_3
    invoke-virtual {v9, v4}, Lcom/android/systemui/statusbar/SignalClusterView;->updateLabelVisible(Z)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isMsim()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v9, v9, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowCarrierStyle:I

    if-eq v9, v10, :cond_6

    :goto_4
    invoke-virtual {v4, v7}, Lcom/android/systemui/statusbar/SignalClusterView;->updateLabelVisible(Z)V

    :cond_2
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v4, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mExpandedVisible:Z

    if-nez v4, :cond_7

    sget-boolean v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sShowNotificationUnderKeyguard:Z

    if-eqz v4, :cond_7

    const/high16 v4, 0x20000

    and-int/2addr v4, v0

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v1, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowNotificationIcons:Z

    :goto_5
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIconCluster:Landroid/view/View;

    if-eqz v1, :cond_8

    :goto_6
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    move v4, v6

    goto :goto_1

    :cond_4
    move v4, v6

    goto :goto_2

    :cond_5
    move v4, v5

    goto :goto_3

    :cond_6
    move v7, v5

    goto :goto_4

    :cond_7
    const/4 v1, 0x0

    goto :goto_5

    :cond_8
    move v5, v6

    goto :goto_6

    :cond_9
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusBattery:Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/phone/BatteryStatusIconView;->disable(Z)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mBatteryIndicator:Lcom/android/systemui/statusbar/phone/BatteryIndicator;

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/phone/BatteryIndicator;->disable(Z)V

    sget-boolean v4, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-nez v4, :cond_a

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterContainer:Landroid/view/View;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-nez v4, :cond_d

    move v4, v5

    :goto_7
    invoke-virtual {v9, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_a
    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNetworkSpeedView:Lcom/android/systemui/statusbar/NetworkSpeedView;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-nez v4, :cond_e

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mShowNetworkSpeed:Z

    xor-int/lit8 v4, v4, 0x1

    :goto_8
    invoke-virtual {v9, v4}, Lcom/android/systemui/statusbar/NetworkSpeedView;->requestHideByKeyguard(Z)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->refreshClockVisible()V

    iget-object v9, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mStatusIcons:Landroid/widget/LinearLayout;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-nez v4, :cond_f

    move v4, v5

    :goto_9
    invoke-virtual {v9, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowCarrierStyle:I

    if-eq v4, v7, :cond_10

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mIsExpand:Z

    if-eqz v4, :cond_11

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowCarrierStyle:I

    if-nez v4, :cond_11

    const/4 v2, 0x1

    :goto_a
    sget-boolean v4, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->sNotch:Z

    if-eqz v4, :cond_12

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotchSignalClusterView:Lcom/android/systemui/statusbar/NotchSignalClusterView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    invoke-virtual {v4, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateLabelVisible(Z)V

    :cond_b
    :goto_b
    const/high16 v4, 0x20000

    and-int/2addr v4, v0

    if-nez v4, :cond_15

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v1, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowNotificationIcons:Z

    :goto_c
    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mNotificationIconCluster:Landroid/view/View;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-nez v4, :cond_16

    if-eqz v1, :cond_16

    move v4, v5

    :goto_d
    invoke-virtual {v7, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mMoreIcon:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mSystemUiVisibility:I

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_c

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-eqz v4, :cond_17

    :cond_c
    const/4 v4, 0x0

    :goto_e
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-eqz v4, :cond_19

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v4

    if-eqz v4, :cond_18

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget-boolean v4, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mShowPrompt:Z

    xor-int/lit8 v3, v4, 0x1

    :goto_f
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiModeText:Landroid/widget/TextView;

    if-eqz v3, :cond_1a

    :goto_10
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_d
    move v4, v6

    goto :goto_7

    :cond_e
    move v4, v7

    goto :goto_8

    :cond_f
    move v4, v6

    goto :goto_9

    :cond_10
    const/4 v2, 0x1

    goto :goto_a

    :cond_11
    const/4 v2, 0x0

    goto :goto_a

    :cond_12
    iget-object v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-nez v4, :cond_13

    move v4, v2

    :goto_11
    invoke-virtual {v7, v4}, Lcom/android/systemui/statusbar/SignalClusterView;->updateLabelVisible(Z)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v4}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isMsim()Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mSignalClusterView2:Lcom/android/systemui/statusbar/SignalClusterView;

    iget-boolean v7, p0, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->mReturnToMultiMode:Z

    if-nez v7, :cond_14

    :goto_12
    invoke-virtual {v4, v2}, Lcom/android/systemui/statusbar/SignalClusterView;->updateLabelVisible(Z)V

    goto :goto_b

    :cond_13
    move v4, v5

    goto :goto_11

    :cond_14
    move v2, v5

    goto :goto_12

    :cond_15
    const/4 v1, 0x0

    goto :goto_c

    :cond_16
    move v4, v6

    goto :goto_d

    :cond_17
    move v4, v8

    goto :goto_e

    :cond_18
    const/4 v3, 0x1

    goto :goto_f

    :cond_19
    const/4 v3, 0x0

    goto :goto_f

    :cond_1a
    move v5, v6

    goto :goto_10
.end method
