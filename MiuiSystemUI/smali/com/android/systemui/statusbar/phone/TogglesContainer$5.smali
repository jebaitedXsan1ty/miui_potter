.class Lcom/android/systemui/statusbar/phone/TogglesContainer$5;
.super Ljava/lang/Object;
.source "TogglesContainer.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/TogglesContainer;->startAnimatorIfNeed(Landroid/widget/ImageView;IZ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

.field final synthetic val$id:I

.field final synthetic val$imageView:Landroid/widget/ImageView;

.field final synthetic val$isOpen:Z


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/TogglesContainer;IZLandroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iput p2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$id:I

    iput-boolean p3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$isOpen:Z

    iput-object p4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$imageView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$id:I

    iget-boolean v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$isOpen:Z

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$imageView:Landroid/widget/ImageView;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/android/systemui/statusbar/phone/TogglesContainer$5$1;-><init>(Lcom/android/systemui/statusbar/phone/TogglesContainer$5;IZLandroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4

    const-string/jumbo v0, "SystemUI.toggleContainer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateToggleImage start for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$id:I

    invoke-static {v2}, Lmiui/app/ToggleManager;->getStatus(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " last:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v2, v2, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$id:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " isOpen:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$isOpen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->this$0:Lcom/android/systemui/statusbar/phone/TogglesContainer;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/TogglesContainer;->mLastIsOpenMap:Ljava/util/HashMap;

    iget v1, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$id:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/phone/TogglesContainer$5;->val$isOpen:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
