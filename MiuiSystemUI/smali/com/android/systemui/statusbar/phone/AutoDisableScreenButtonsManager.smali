.class public Lcom/android/systemui/statusbar/phone/AutoDisableScreenButtonsManager;
.super Ljava/lang/Object;
.source "AutoDisableScreenButtonsManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static disableButtonsStatus(Landroid/content/Context;Z)V
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_buttons_state"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static enableRotationAction(Landroid/content/Context;Z)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/android/systemui/statusbar/phone/AutoDisableScreenButtonsManager;->showTouchAssistant(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/systemui/statusbar/phone/AutoDisableScreenButtonsManager;->disableButtonsStatus(Landroid/content/Context;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, v0}, Lcom/android/systemui/statusbar/phone/AutoDisableScreenButtonsManager;->setTouchAssistantTemporary(Landroid/content/Context;Z)V

    invoke-static {p0, v0}, Lcom/android/systemui/statusbar/phone/AutoDisableScreenButtonsManager;->disableButtonsStatus(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public static getRotationSettingStatus(Landroid/content/Context;)Z
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "enable_auto_disable_screen_rotation"

    const/4 v2, 0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    return v0
.end method

.method private static getTouchAssistantTemporary(Landroid/content/Context;)Z
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "touch_assistant_temporary"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    return v0
.end method

.method public static isPad()Z
    .locals 2

    const-string/jumbo v0, "is_pad"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static setTouchAssistantTemporary(Landroid/content/Context;Z)V
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "touch_assistant_temporary"

    const/4 v2, -0x2

    invoke-static {v0, v1, p1, v2}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    return-void
.end method

.method private static showTouchAssistant(Landroid/content/Context;)V
    .locals 5

    const/4 v4, -0x2

    const/4 v3, 0x0

    invoke-static {p0, v4, v3}, Landroid/provider/MiuiSettings$System;->isTouchAssistantTemporaryForUser(Landroid/content/Context;IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p0}, Lcom/android/systemui/statusbar/phone/AutoDisableScreenButtonsManager;->getTouchAssistantTemporary(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v3, 0x1

    invoke-static {p0, v3}, Lcom/android/systemui/statusbar/phone/AutoDisableScreenButtonsManager;->setTouchAssistantTemporary(Landroid/content/Context;Z)V

    :try_start_0
    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v3, "com.miui.touchassistant"

    const-string/jumbo v4, "com.miui.touchassistant.CoreService"

    invoke-direct {v1, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "com.miui.touchassistant.SHOW_FLOATING_WINDOW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    new-instance v3, Landroid/os/UserHandle;

    const/4 v4, -0x2

    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
