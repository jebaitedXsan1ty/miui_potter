.class Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$4;
.super Ljava/lang/Object;
.source "ExpandedHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$4;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-static {}, Lcom/android/systemui/Util;->isFold()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$4;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PanelView;->resetFoldState()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView$4;->this$0:Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;

    iget-object v0, v0, Lcom/android/systemui/statusbar/phone/ExpandedHeaderView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->getPanelView()Lcom/android/systemui/statusbar/phone/PanelView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/PanelView;->closeDetail()V

    goto :goto_0
.end method
