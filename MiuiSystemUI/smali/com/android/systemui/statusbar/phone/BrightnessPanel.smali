.class public Lcom/android/systemui/statusbar/phone/BrightnessPanel;
.super Landroid/widget/LinearLayout;
.source "BrightnessPanel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;
    }
.end annotation


# instance fields
.field private mBrightnessMax:Landroid/widget/ImageView;

.field private mBrightnessMin:Landroid/widget/ImageView;

.field private mMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

.field private mMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

.field private final mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSlider:Landroid/widget/SeekBar;

.field private mToggleManager:Lmiui/app/ToggleManager;


# direct methods
.method static synthetic -get0(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/phone/BrightnessPanel;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)Lmiui/app/ToggleManager;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mToggleManager:Lmiui/app/ToggleManager;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel$1;-><init>(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/app/ToggleManager;->createInstance(Landroid/content/Context;)Lmiui/app/ToggleManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mToggleManager:Lmiui/app/ToggleManager;

    return-void
.end method


# virtual methods
.method public dyeColor(Lcom/android/systemui/statusbar/ColorSuits;)V
    .locals 7

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/android/systemui/statusbar/ColorSuits;->enableImageColorDye(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mBrightnessMin:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ColorSuits;->getImageColor()I

    move-result v5

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mBrightnessMax:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ColorSuits;->getImageColor()I

    move-result v5

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v4, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v4, :cond_1

    move-object v2, v0

    check-cast v2, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ColorSuits;->getImageColor()I

    move-result v5

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_1
    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v4, v0, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v4, :cond_2

    move-object v3, v0

    check-cast v3, Landroid/graphics/drawable/StateListDrawable;

    const v4, 0x101009e

    const v5, 0x10100a7

    filled-new-array {v4, v5}, [I

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawableIndex([I)I

    move-result v1

    if-ltz v1, :cond_2

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/systemui/statusbar/ColorSuits;->getImageColor()I

    move-result v5

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_2
    return-void
.end method

.method public getSeekBar()Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0f0182

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    sget v1, Lmiui/app/ToggleManager;->RANGE:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    sget-boolean v0, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v0}, Lmiui/app/ToggleManager;->isBrightnessAutoMode()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "screen_brightness"

    const/16 v3, 0x80

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const v0, 0x7f0f0181

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mBrightnessMin:Landroid/widget/ImageView;

    const v0, 0x7f0f0183

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mBrightnessMax:Landroid/widget/ImageView;

    return-void
.end method

.method public refreshBrightness()V
    .locals 3

    sget-boolean v1, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->SUPPORT_AUTO_BRIGHTNESS_OPTIMIZE:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v2}, Lmiui/app/ToggleManager;->getCurBrightness()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mToggleManager:Lmiui/app/ToggleManager;

    invoke-virtual {v1}, Lmiui/app/ToggleManager;->isBrightnessAutoMode()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Lmiui/app/ToggleManager;->USE_SCREEN_AUTO_BRIGHTNESS_ADJUSTMENT:Z

    xor-int/lit8 v0, v1, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    xor-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMirror(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    iget-object v1, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setValue(I)V

    :cond_0
    return-void
.end method

.method public setMirrorController(Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;)V
    .locals 1

    iput-object p1, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirrorController:Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;

    invoke-virtual {v0, p0}, Lcom/android/systemui/statusbar/policy/BrightnessMirrorController;->setBrightnessPanel(Lcom/android/systemui/statusbar/phone/BrightnessPanel;)V

    return-void
.end method

.method public setValue(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mSlider:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mMirror:Lcom/android/systemui/statusbar/phone/BrightnessPanel;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setValue(I)V

    :cond_0
    return-void
.end method

.method public updateResources(Z)V
    .locals 7

    const/4 v6, 0x0

    sget-boolean v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sTogglesInListStyle:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b008c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b008d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0, v2, v6, v3, v6}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setPaddingRelative(IIII)V

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0086

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0087

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0088

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b007e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    invoke-virtual {p0, v2, v6, v3, v1}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setPaddingRelative(IIII)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0089

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b008a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b008b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v4, p0, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b007f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/phone/BrightnessPanel;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method
