.class Lcom/android/systemui/statusbar/StatusBar$2;
.super Landroid/content/BroadcastReceiver;
.source "StatusBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/StatusBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/StatusBar;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/StatusBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/StatusBar$2;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    const/4 v11, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v7, "android.intent.action.USER_PRESENT"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v7

    iget-object v8, p0, Lcom/android/systemui/statusbar/StatusBar$2;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget v8, v8, Lcom/android/systemui/statusbar/StatusBar;->mCurrentUserId:I

    const/4 v9, 0x1

    const/4 v10, 0x5

    invoke-interface {v7, v9, v10, v8}, Landroid/app/IActivityManager;->getRecentTasks(III)Landroid/content/pm/ParceledListSlice;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_1

    iget-object v7, p0, Lcom/android/systemui/statusbar/StatusBar$2;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    invoke-static {v7}, Lcom/android/systemui/statusbar/StatusBar;->-get1(Lcom/android/systemui/statusbar/StatusBar;)Landroid/os/UserManager;

    move-result-object v8

    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager$RecentTaskInfo;

    iget v7, v7, Landroid/app/ActivityManager$RecentTaskInfo;->userId:I

    invoke-virtual {v8, v7}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/systemui/statusbar/StatusBar$2;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v7, v7, Lcom/android/systemui/statusbar/StatusBar;->mContext:Landroid/content/Context;

    const v8, 0x7f0d01be

    invoke-static {v7, v8, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v7

    const v8, 0x102000b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v7, 0x7f02025d

    invoke-virtual {v4, v7, v11, v11, v11}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/StatusBar$2;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v7, v7, Lcom/android/systemui/statusbar/StatusBar;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b00bc

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_0
    if-eqz v6, :cond_1

    iget-object v7, p0, Lcom/android/systemui/statusbar/StatusBar$2;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    iget-object v7, v7, Lcom/android/systemui/statusbar/StatusBar;->mIconPolicy:Lcom/android/systemui/statusbar/phone/PhoneStatusBarPolicy;

    invoke-virtual {v6}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/android/systemui/statusbar/phone/PhoneStatusBarPolicy;->profileChanged(I)V

    :cond_1
    const-string/jumbo v7, "sagit"

    sget-object v8, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/android/systemui/statusbar/StatusBar$2;->this$0:Lcom/android/systemui/statusbar/StatusBar;

    invoke-static {v7, p1}, Lcom/android/systemui/statusbar/StatusBar;->-wrap0(Lcom/android/systemui/statusbar/StatusBar;Landroid/content/Context;)V

    :cond_2
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method
