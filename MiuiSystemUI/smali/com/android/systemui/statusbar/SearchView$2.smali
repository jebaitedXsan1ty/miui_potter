.class Lcom/android/systemui/statusbar/SearchView$2;
.super Ljava/lang/Object;
.source "SearchView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/statusbar/SearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/statusbar/SearchView;


# direct methods
.method constructor <init>(Lcom/android/systemui/statusbar/SearchView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/SearchView$2;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView$2;->this$0:Lcom/android/systemui/statusbar/SearchView;

    iget-object v1, v1, Lcom/android/systemui/statusbar/SearchView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isChangeToSearching()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView$2;->this$0:Lcom/android/systemui/statusbar/SearchView;

    iget-object v1, v1, Lcom/android/systemui/statusbar/SearchView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    invoke-virtual {v1}, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->isModifyStatusBarExpandedBg()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView$2;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/SearchView;->-wrap5(Lcom/android/systemui/statusbar/SearchView;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView$2;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/SearchView;->-wrap0(Lcom/android/systemui/statusbar/SearchView;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x258

    :goto_1
    sput v1, Lcom/android/systemui/statusbar/SearchView;->sDuartion:I

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v2, v1, v3

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    aput v2, v1, v3

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    sget v2, Lcom/android/systemui/statusbar/SearchView;->sDuartion:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/systemui/statusbar/SearchView$2$1;

    invoke-direct {v1, p0}, Lcom/android/systemui/statusbar/SearchView$2$1;-><init>(Lcom/android/systemui/statusbar/SearchView$2;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    iget-object v1, p0, Lcom/android/systemui/statusbar/SearchView$2;->this$0:Lcom/android/systemui/statusbar/SearchView;

    invoke-static {v1}, Lcom/android/systemui/statusbar/SearchView;->-wrap5(Lcom/android/systemui/statusbar/SearchView;)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x3e8

    goto :goto_1
.end method
