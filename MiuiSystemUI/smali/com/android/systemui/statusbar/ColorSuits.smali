.class public Lcom/android/systemui/statusbar/ColorSuits;
.super Ljava/lang/Object;
.source "ColorSuits.java"


# static fields
.field public static final CLEAR:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final CLOUDY:Lcom/android/systemui/statusbar/ColorSuits;

.field protected static final CLOUDYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

.field public static final CLOUDY_HOT:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final DEFAULT:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final FOG:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final HAIL:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final HAZE:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final OVERCAST:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final RAIN:Lcom/android/systemui/statusbar/ColorSuits;

.field protected static final RAINSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

.field public static final RAIN_1:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final RAIN_2:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final SAND:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final SNOW:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final SUNNY:Lcom/android/systemui/statusbar/ColorSuits;

.field protected static final SUNNYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

.field public static final SUNNY_1:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final SUNNY_2:Lcom/android/systemui/statusbar/ColorSuits;

.field public static final SUNNY_HOT:Lcom/android/systemui/statusbar/ColorSuits;

.field private static sBgDrawableRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static sCurrentBgRes:I

.field private static sLandBgDrawableRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static sLandCanvasBitmap:Landroid/graphics/Bitmap;

.field private static sLandCurrentBgRes:I

.field private static sPoritalCanvasBitmap:Landroid/graphics/Bitmap;


# instance fields
.field private mBgColor:I

.field private mBgRes:I

.field private mImageColor:I

.field private mLandBgRes:I

.field private mTextColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const v9, -0xab4970

    const v8, -0xb65149

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f02004f

    const v2, -0xb0575c

    const v3, -0xaa4a50

    const v4, -0xbc6066

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->DEFAULT:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f02004c

    const v2, -0x8d8755

    const v3, -0x7d7740

    const v4, -0x938d53

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->CLEAR:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020059

    const v2, -0xb4597d

    const v3, -0xbc567f

    invoke-direct {v0, v1, v2, v9, v3}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SUNNY:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f02005a

    const v2, -0xa2512c

    const v3, -0x974422

    const v4, -0xad5632

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SUNNY_1:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f02005b

    const v2, -0x96433c

    const v3, -0xa34d46

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SUNNY_2:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f02005c

    const v2, -0x105ebd

    const v3, -0xa58b8

    const v4, -0x1767c9

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SUNNY_HOT:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f02004d

    const v2, -0xaf4c4b

    const v3, -0xa94140

    const v4, -0xba4f4e

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->CLOUDY:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f02004e

    const v2, -0xa5fc8

    const v3, -0x85dc7

    const v4, -0x1771d9

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->CLOUDY_HOT:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020053

    const v2, -0x866427

    const v3, -0x7b591e

    const v4, -0x98702a

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->OVERCAST:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020052

    const v2, -0x85755f

    const v3, -0x74634b

    const v4, -0x8c765e

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->HAZE:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020050

    const v2, -0x7b5f4e

    const v3, -0x6b4e3b

    const v4, -0x7f604c

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->FOG:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020051

    const v2, -0x805646

    const v3, -0x754635

    const v4, -0x885a48

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->HAIL:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020054

    const v2, -0xb6547c

    const v3, -0xc35d85

    invoke-direct {v0, v1, v2, v9, v3}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->RAIN:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020055

    const v2, -0xb46f37

    const v3, -0xa96229

    const v4, -0xc27436

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->RAIN_1:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020056

    const v2, -0x96433c

    const v3, -0xa34d46

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->RAIN_2:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020058

    const v2, -0x7d594b

    const v3, -0x704939

    const v4, -0x795648

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SNOW:Lcom/android/systemui/statusbar/ColorSuits;

    new-instance v0, Lcom/android/systemui/statusbar/ColorSuits;

    const v1, 0x7f020057

    const v2, -0x265b98

    const v3, -0x1d4f8b

    const v4, -0x285c9b

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/systemui/statusbar/ColorSuits;-><init>(IIII)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SAND:Lcom/android/systemui/statusbar/ColorSuits;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/systemui/statusbar/ColorSuits;

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->SUNNY:Lcom/android/systemui/statusbar/ColorSuits;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->SUNNY_1:Lcom/android/systemui/statusbar/ColorSuits;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->SUNNY_1:Lcom/android/systemui/statusbar/ColorSuits;

    aput-object v1, v0, v7

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SUNNYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/systemui/statusbar/ColorSuits;

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->RAIN:Lcom/android/systemui/statusbar/ColorSuits;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->RAIN_1:Lcom/android/systemui/statusbar/ColorSuits;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->RAIN_1:Lcom/android/systemui/statusbar/ColorSuits;

    aput-object v1, v0, v7

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->RAINSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    new-array v0, v7, [Lcom/android/systemui/statusbar/ColorSuits;

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->CLOUDY:Lcom/android/systemui/statusbar/ColorSuits;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->OVERCAST:Lcom/android/systemui/statusbar/ColorSuits;

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->CLOUDYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    return-void
.end method

.method private constructor <init>(IIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/systemui/statusbar/ColorSuits;->mBgRes:I

    iput p2, p0, Lcom/android/systemui/statusbar/ColorSuits;->mBgColor:I

    iput p3, p0, Lcom/android/systemui/statusbar/ColorSuits;->mImageColor:I

    iput p4, p0, Lcom/android/systemui/statusbar/ColorSuits;->mTextColor:I

    return-void
.end method

.method private getBitmap(III)Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x1

    if-ne p3, v0, :cond_3

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sPoritalCanvasBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sPoritalCanvasBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-eq v0, p1, :cond_2

    :cond_0
    :goto_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sPoritalCanvasBitmap:Landroid/graphics/Bitmap;

    :cond_1
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sPoritalCanvasBitmap:Landroid/graphics/Bitmap;

    return-object v0

    :cond_2
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sPoritalCanvasBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, p2, :cond_1

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandCanvasBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandCanvasBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-eq v0, p1, :cond_6

    :cond_4
    :goto_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandCanvasBitmap:Landroid/graphics/Bitmap;

    :cond_5
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandCanvasBitmap:Landroid/graphics/Bitmap;

    return-object v0

    :cond_6
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandCanvasBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, p2, :cond_5

    goto :goto_1
.end method

.method private maskingImage(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;III)Landroid/graphics/drawable/Drawable;
    .locals 10

    const/4 v9, 0x0

    invoke-direct {p0, p3, p4, p5}, Lcom/android/systemui/statusbar/ColorSuits;->getBitmap(III)Landroid/graphics/Bitmap;

    move-result-object v4

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/Paint;

    const/4 v5, 0x1

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    new-instance v5, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    const/4 v5, 0x0

    invoke-virtual {p2, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v9, v9, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p2, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f800000    # 1.0f

    mul-float/2addr v5, v6

    int-to-float v6, p3

    div-float v3, v5, v6

    int-to-float v5, p4

    mul-float/2addr v5, v3

    float-to-int v1, v5

    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sub-int/2addr v6, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-direct {v5, v9, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v9, v9, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, p1, v5, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v5, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    return-object v5
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sBgDrawableRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sBgDrawableRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    :cond_0
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandBgDrawableRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandBgDrawableRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    :cond_1
    return-void
.end method

.method public enableImageColorDye(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public enableTextColorDye(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public getBgColor()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/ColorSuits;->mBgColor:I

    return v0
.end method

.method public getBgDrawable(Landroid/content/Context;Landroid/graphics/drawable/Drawable;III)Landroid/graphics/drawable/Drawable;
    .locals 8

    sget v0, Lcom/android/systemui/statusbar/ColorSuits;->sCurrentBgRes:I

    iget v2, p0, Lcom/android/systemui/statusbar/ColorSuits;->mBgRes:I

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sBgDrawableRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sBgDrawableRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    mul-int/2addr v0, p4

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    mul-int/2addr v2, p3

    if-ne v0, v2, :cond_0

    return-object v6

    :cond_0
    if-lez p3, :cond_1

    if-gtz p4, :cond_2

    :cond_1
    return-object p2

    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/android/systemui/statusbar/ColorSuits;->mBgRes:I

    invoke-static {v0, v2}, Lmiui/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_3

    return-object p2

    :cond_3
    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/android/systemui/statusbar/ColorSuits;->maskingImage(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;III)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    const-class v2, Lcom/android/systemui/statusbar/ColorSuits;

    monitor-enter v2

    :try_start_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v7}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sBgDrawableRef:Ljava/lang/ref/WeakReference;

    iget v0, p0, Lcom/android/systemui/statusbar/ColorSuits;->mBgRes:I

    sput v0, Lcom/android/systemui/statusbar/ColorSuits;->sCurrentBgRes:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    return-object v7

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public getImageColor()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/ColorSuits;->mImageColor:I

    return v0
.end method

.method public getTextColor()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/ColorSuits;->mTextColor:I

    return v0
.end method

.method public getThemeBgDrawable(Landroid/content/Context;Landroid/graphics/drawable/Drawable;IIII)Landroid/graphics/drawable/Drawable;
    .locals 9

    const/4 v3, 0x0

    iput p3, p0, Lcom/android/systemui/statusbar/ColorSuits;->mLandBgRes:I

    sget v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandCurrentBgRes:I

    iget v2, p0, Lcom/android/systemui/statusbar/ColorSuits;->mLandBgRes:I

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandBgDrawableRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandBgDrawableRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    mul-int/2addr v0, p5

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    mul-int/2addr v2, p4

    if-ne v0, v2, :cond_0

    return-object v7

    :cond_0
    if-lez p4, :cond_1

    if-gtz p5, :cond_2

    :cond_1
    return-object p2

    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p4, p5, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v7, v3, v3, p4, p5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v7, v6}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object v0, p0

    move-object v2, p2

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/android/systemui/statusbar/ColorSuits;->maskingImage(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;III)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    const-class v2, Lcom/android/systemui/statusbar/ColorSuits;

    monitor-enter v2

    :try_start_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v8}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandBgDrawableRef:Ljava/lang/ref/WeakReference;

    iget v0, p0, Lcom/android/systemui/statusbar/ColorSuits;->mLandBgRes:I

    sput v0, Lcom/android/systemui/statusbar/ColorSuits;->sLandCurrentBgRes:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    return-object v8

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
