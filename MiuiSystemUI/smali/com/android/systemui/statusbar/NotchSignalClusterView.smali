.class public Lcom/android/systemui/statusbar/NotchSignalClusterView;
.super Lcom/android/systemui/statusbar/BaseSignalClusterView;
.source "NotchSignalClusterView.java"


# instance fields
.field protected mCardNum:I

.field private mIconsPaddingEndOld:I

.field private mMobileSignalUpgradeId:I

.field protected mNotchMobile:Landroid/view/View;

.field protected mNotchSignal:Landroid/widget/ImageView;

.field protected mNotchSignal2:Landroid/widget/ImageView;

.field mSimpleStatusBar:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/BaseSignalClusterView;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardNum:I

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/systemui/statusbar/BaseSignalClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardNum:I

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeId:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/systemui/statusbar/BaseSignalClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardNum:I

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeId:I

    return-void
.end method

.method private changeNotchStrength(I)I
    .locals 3

    const/4 v2, 0x0

    const v1, 0x7f020293

    if-ne p1, v1, :cond_1

    const p1, 0x7f020295

    :cond_0
    :goto_0
    return p1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    aget-object v1, v1, v2

    array-length v1, v1

    if-ge v0, v1, :cond_0

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    aget-object v1, v1, v2

    aget v1, v1, v0

    if-ne v1, p1, :cond_2

    sget-object v1, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->M_TELEPHONY_SIGNAL_STRENGTH:[[I

    aget-object v1, v1, v2

    aget p1, v1, v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setIconsPaddingEnd()V
    .locals 3

    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b013d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    rsub-int/lit8 v0, v1, 0x19

    :goto_0
    iget v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIconsPaddingEndOld:I

    if-eq v1, v0, :cond_1

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIconsPaddingEndOld:I

    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mSimpleStatusBar:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    invoke-virtual {v1, v0}, Lcom/android/systemui/statusbar/phone/SimpleStatusBar;->setIconsPadding(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0051

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public apply()V
    .locals 10

    const v7, 0x7f020283

    const/4 v8, 0x2

    const/4 v9, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v0, 0x0

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardNum:I

    if-ne v4, v8, :cond_0

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchSignal:Landroid/widget/ImageView;

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchSignal2:Landroid/widget/ImageView;

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    if-nez v4, :cond_2

    return-void

    :cond_2
    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiVisible:Z

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifi:Landroid/widget/ImageView;

    iget v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiStrengthId:I

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    iget v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiActivityId:I

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    iget v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiActivityId:I

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_9

    :cond_3
    move v4, v6

    :goto_0
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiDescription:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    iget v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardNum:I

    if-ge v4, v8, :cond_13

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileVisible:[Z

    aget-boolean v4, v4, v5

    if-eqz v4, :cond_d

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIsAirplaneMode:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_d

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobile:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    aget v7, v7, v5

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityId:[I

    aget v7, v7, v5

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    iget v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeId:I

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeDescription:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileDescription:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeId:[I

    aget v4, v4, v5

    invoke-static {v4}, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateMobileType(Ljava/lang/String;)Z

    move-result v0

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiVisible:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeId:[I

    aget v4, v4, v5

    if-nez v4, :cond_b

    :cond_4
    move v4, v6

    :goto_2
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    if-eqz v0, :cond_c

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    :goto_3
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_4
    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIsAirplaneMode:Z

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    iget v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mAirplaneIconId:I

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    :goto_5
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileVisibleCdma:[Z

    aget-boolean v4, v4, v5

    if-eqz v4, :cond_10

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIsAirplaneMode:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroupCdma:Landroid/view/ViewGroup;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileEvdo:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthIdEvdo:[I

    aget v7, v7, v5

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileCdma:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthIdCdma:[I

    aget v7, v7, v5

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityCdma:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityIdCdma:[I

    aget v7, v7, v5

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeCdma:Landroid/widget/ImageView;

    iget v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeId:I

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityCdma:Landroid/widget/ImageView;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiVisible:Z

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityIdCdma:[I

    aget v4, v4, v5

    if-nez v4, :cond_f

    :cond_5
    move v4, v6

    :goto_6
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_7
    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiNeedVisible:Z

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_6
    sget-boolean v4, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-eqz v4, :cond_12

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIsSimMissing:[Z

    aget-boolean v4, v4, v5

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_12

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWcdmaCardSlot:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardSlot:I

    if-nez v4, :cond_11

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWcdmaCardSlot:Landroid/widget/ImageView;

    const v7, 0x7f02020e

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    :cond_7
    :goto_8
    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIsAirplaneMode:Z

    if-eqz v4, :cond_1e

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    iget v5, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mAirplaneIconId:I

    invoke-virtual {p0, v4, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    :goto_9
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mSimpleStatusBar:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    if-eqz v4, :cond_8

    invoke-direct {p0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->setIconsPaddingEnd()V

    :cond_8
    return-void

    :cond_9
    move v4, v5

    goto/16 :goto_0

    :cond_a
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_1

    :cond_b
    move v4, v5

    goto/16 :goto_2

    :cond_c
    move v4, v6

    goto/16 :goto_3

    :cond_d
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_4

    :cond_e
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_f
    move v4, v5

    goto :goto_6

    :cond_10
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroupCdma:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_7

    :cond_11
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWcdmaCardSlot:Landroid/widget/ImageView;

    const v7, 0x7f020210

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    goto :goto_8

    :cond_12
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWcdmaCardSlot:Landroid/widget/ImageView;

    const/4 v7, 0x4

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_8

    :cond_13
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileVisible:[Z

    aget-boolean v4, v4, v5

    if-eqz v4, :cond_1d

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIsAirplaneMode:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1d

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    aget v7, v7, v5

    invoke-direct {p0, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->changeNotchStrength(I)I

    move-result v7

    aput v7, v4, v5

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchSignal:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    aget v7, v7, v5

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    aget v7, v7, v9

    invoke-direct {p0, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->changeNotchStrength(I)I

    move-result v7

    aput v7, v4, v9

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchSignal2:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    aget v7, v7, v9

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    iget v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeId:I

    invoke-virtual {p0, v4, v7}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityId:[I

    aget v4, v4, v5

    if-eqz v4, :cond_15

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityId:[I

    aget v4, v4, v5

    :goto_a
    invoke-virtual {p0, v7, v4}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeDescription:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileDescription:[Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeId:[I

    aget v4, v4, v5

    invoke-static {v4}, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeId:[I

    aget v4, v4, v9

    invoke-static {v4}, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->getNetworkTypeName(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_16

    move-object v3, v2

    :goto_b
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_19

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateMobileType(Ljava/lang/String;)Z

    move-result v0

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiVisible:Z

    if-nez v4, :cond_14

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeId:[I

    aget v4, v4, v5

    if-nez v4, :cond_17

    :cond_14
    move v4, v6

    :goto_c
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    if-eqz v0, :cond_18

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    :goto_d
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_e
    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiNeedVisible:Z

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_8

    :cond_15
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityId:[I

    aget v4, v4, v9

    goto :goto_a

    :cond_16
    move-object v3, v1

    goto :goto_b

    :cond_17
    move v4, v5

    goto :goto_c

    :cond_18
    move v4, v6

    goto :goto_d

    :cond_19
    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateMobileType(Ljava/lang/String;)Z

    move-result v0

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiVisible:Z

    if-nez v4, :cond_1a

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeId:[I

    aget v4, v4, v9

    if-nez v4, :cond_1b

    :cond_1a
    move v4, v6

    :goto_f
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    if-eqz v0, :cond_1c

    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    :goto_10
    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_e

    :cond_1b
    move v4, v5

    goto :goto_f

    :cond_1c
    move v4, v6

    goto :goto_10

    :cond_1d
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_e

    :cond_1e
    iget-object v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_9
.end method

.method public dispatchDemoCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    const v5, 0x7f020283

    const/4 v4, 0x0

    const/16 v3, 0x8

    const-string/jumbo v0, "demo_mode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "SignalClusterView mDemoMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", command = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    if-nez v0, :cond_2

    const-string/jumbo v0, "enter"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiAp:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifi:Landroid/widget/ImageView;

    const v1, 0x7f0202c8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mEnableDarkMode:Z

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/Icons;->get(Ljava/lang/Integer;Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mSpeechHd:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileRoam:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardNum:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobile:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchSignal:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchSignal2:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v5}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->updateIcon(Landroid/widget/ImageView;I)V

    :goto_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivity:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWcdmaCardSlot:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCarrierLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mAirplane:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileGroupCdma:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mVolte:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchMobile:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobile:Landroid/widget/ImageView;

    const v1, 0x7f020281

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mEnableDarkMode:Z

    invoke-static {v1, v2}, Lcom/android/systemui/statusbar/Icons;->get(Ljava/lang/Integer;Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "exit"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    sget-boolean v0, Lmiui/os/Build;->IS_CTA_BUILD:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNC:Lcom/android/systemui/statusbar/policy/NetworkController;

    iget v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardSlot:I

    invoke-virtual {v0, p0, v1}, Lcom/android/systemui/statusbar/policy/NetworkController;->refreshSignalCluster(Lcom/android/systemui/statusbar/policy/BaseNetworkController$SignalCluster;I)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->apply()V

    goto :goto_1
.end method

.method public getViewId2()I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mViewId2:I

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    invoke-super {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    const/16 v1, 0x8

    invoke-super {p0}, Lcom/android/systemui/statusbar/BaseSignalClusterView;->onFinishInflate()V

    const v0, 0x7f0f009f

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchMobile:Landroid/view/View;

    const v0, 0x7f0f00a0

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchSignal:Landroid/widget/ImageView;

    const v0, 0x7f0f00a1

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchSignal2:Landroid/widget/ImageView;

    const v0, 0x7f0f009c

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    const v0, 0x7f0f00a5

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    const v0, 0x7f0f00ae

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeCdma:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiAp:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeCdma:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f020299

    iput v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgradeId:I

    return-void
.end method

.method public setCardNum(I)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iput p1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardNum:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobile:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchMobile:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobile:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchMobile:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setIsActiveNetworkMetered(Z)V
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiActivity:Landroid/widget/ImageView;

    iget v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiActivityId:I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    const/16 v0, 0x8

    :cond_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public setIsImsRegisted(Z)V
    .locals 4

    const/4 v0, 0x0

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    if-eqz v2, :cond_0

    return-void

    :cond_0
    sget-boolean v2, Lcom/android/systemui/statusbar/NotchSignalClusterView;->sNotch:Z

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardNum:I

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mNotchVolte:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mVolte:Landroid/widget/TextView;

    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public setIsRoaming(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public setMobileDataIndicators(IZIIILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    const/4 v1, 0x0

    sget-boolean v0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->sNotch:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mCardNum:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 p1, 0x0

    :cond_0
    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileVisible:[Z

    aput-boolean p2, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    aput p3, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityId:[I

    aput p4, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeId:[I

    aput p5, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileDescription:[Ljava/lang/String;

    aput-object p6, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeDescription:[Ljava/lang/String;

    aput-object p7, v0, p1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIsSimMissing:[Z

    aput-boolean p8, v0, p1

    :goto_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->apply()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileVisible:[Z

    aput-boolean p2, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    aput p3, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityId:[I

    aput p4, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeId:[I

    aput p5, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileDescription:[Ljava/lang/String;

    aput-object p6, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeDescription:[Ljava/lang/String;

    aput-object p7, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIsSimMissing:[Z

    aput-boolean p8, v0, v1

    goto :goto_0
.end method

.method public setMobileDataIndicators(ZIIILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileVisible:[Z

    aput-boolean p1, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileStrengthId:[I

    aput p2, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileActivityId:[I

    aput p3, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeId:[I

    aput p4, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileDescription:[Ljava/lang/String;

    aput-object p5, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileTypeDescription:[Ljava/lang/String;

    aput-object p6, v0, v1

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mIsSimMissing:[Z

    aput-boolean p7, v0, v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->apply()V

    return-void
.end method

.method public setSimpleStatusBar(Lcom/android/systemui/statusbar/phone/SimpleStatusBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mSimpleStatusBar:Lcom/android/systemui/statusbar/phone/SimpleStatusBar;

    return-void
.end method

.method public setSpeechHd(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public setViewId2(I)V
    .locals 0

    iput p1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mViewId2:I

    return-void
.end method

.method public setWifiApEnabled(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mDemoMode:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    return-void
.end method

.method public setWifiIndicators(ZIILjava/lang/String;)V
    .locals 2

    const/16 v1, 0x8

    iput-boolean p1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiVisible:Z

    iput p2, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiStrengthId:I

    iput p3, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiActivityId:I

    iput-object p4, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiDescription:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiAp:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mWifiApConnectMark:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/statusbar/NotchSignalClusterView;->apply()V

    return-void
.end method

.method public updateMobileType(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    const-string/jumbo v1, "4G+"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileSignalUpgrade:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    const-string/jumbo v2, "4G"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/NotchSignalClusterView;->mMobileType:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
