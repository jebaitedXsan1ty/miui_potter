.class public Lcom/android/systemui/statusbar/WeatherView;
.super Landroid/widget/RelativeLayout;
.source "WeatherView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/systemui/statusbar/WeatherView$1;,
        Lcom/android/systemui/statusbar/WeatherView$2;,
        Lcom/android/systemui/statusbar/WeatherView$3;,
        Lcom/android/systemui/statusbar/WeatherView$4;,
        Lcom/android/systemui/statusbar/WeatherView$5;,
        Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;,
        Lcom/android/systemui/statusbar/WeatherView$QueryHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final sProjection:[Ljava/lang/String;


# instance fields
.field private mAqi:I

.field private mCalendar:Ljava/util/Calendar;

.field private mClock:Lcom/android/systemui/statusbar/policy/Clock;

.field mColor:I

.field private mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

.field private mCurRotation:I

.field private mCurStyle:I

.field private mDateView:Lcom/android/systemui/statusbar/policy/Clock;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mIsNight:Z

.field private mIsNotch:Z

.field mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field mOnClickListener:Landroid/view/View$OnClickListener;

.field private mPkgChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mPublishTime:J

.field private mQueryHandler:Lcom/android/systemui/statusbar/WeatherView$QueryHandler;

.field public mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

.field private mShowPublishTime:Z

.field private mSunriseMill:J

.field private mSunsetMill:J

.field private mTemperature:Ljava/lang/String;

.field private mTemperatureUnit:I

.field private mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

.field private mTimeTickReceiver:Landroid/content/BroadcastReceiver;

.field private mWeatherDescription:Ljava/lang/String;

.field private mWeatherDescriptionView:Landroid/widget/TextView;

.field private final mWeatherObserver:Landroid/database/ContentObserver;

.field private mWeatherType:I


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/systemui/statusbar/WeatherView;->DEBUG:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/systemui/statusbar/WeatherView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/systemui/statusbar/WeatherView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/systemui/statusbar/WeatherView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureUnit:I

    return v0
.end method

.method static synthetic -get4(Lcom/android/systemui/statusbar/WeatherView;)I
    .locals 1

    iget v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherType:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/systemui/statusbar/WeatherView;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/systemui/statusbar/WeatherView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/WeatherView;->changeStyle(I)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/systemui/statusbar/WeatherView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->onBootCompleted()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/systemui/statusbar/WeatherView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->refreshViews()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/systemui/statusbar/WeatherView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->registerObserver()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/systemui/statusbar/WeatherView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->startQuery()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/systemui/statusbar/WeatherView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->unregisterObserver()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/systemui/statusbar/WeatherView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->updateIsNight()V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/systemui/statusbar/WeatherView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->updateShowPublishTime()V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/systemui/statusbar/WeatherView;Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/systemui/statusbar/WeatherView;->updateWeatherInfo(Landroid/database/Cursor;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x3

    const-string/jumbo v0, "WeatherView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/statusbar/WeatherView;->DEBUG:Z

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "temperature"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "weather_type"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "aqilevel"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string/jumbo v1, "publish_time"

    aput-object v1, v0, v3

    const-string/jumbo v1, "sunrise"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string/jumbo v1, "sunset"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string/jumbo v1, "temperature_unit"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/systemui/statusbar/WeatherView;->sProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mCurStyle:I

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mIsNotch:Z

    iput v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherType:I

    iput v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mAqi:I

    iput-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunriseMill:J

    iput-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunsetMill:J

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->DEFAULT:Lcom/android/systemui/statusbar/ColorSuits;

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mListeners:Ljava/util/ArrayList;

    iput v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mCurRotation:I

    new-instance v0, Lcom/android/systemui/statusbar/WeatherView$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/WeatherView$1;-><init>(Lcom/android/systemui/statusbar/WeatherView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mTimeTickReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/systemui/statusbar/WeatherView$2;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/WeatherView$2;-><init>(Lcom/android/systemui/statusbar/WeatherView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/systemui/statusbar/WeatherView$3;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/WeatherView$3;-><init>(Lcom/android/systemui/statusbar/WeatherView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mPkgChangedReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/systemui/statusbar/WeatherView$4;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/WeatherView$4;-><init>(Lcom/android/systemui/statusbar/WeatherView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/systemui/statusbar/WeatherView$5;

    invoke-direct {v0, p0}, Lcom/android/systemui/statusbar/WeatherView$5;-><init>(Lcom/android/systemui/statusbar/WeatherView;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private changeStyle(I)V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mCurStyle:I

    if-eq v3, p1, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mDateView:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v3}, Lcom/android/systemui/statusbar/policy/Clock;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0, v2}, Lcom/android/systemui/statusbar/WeatherView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mDateView:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v3, v0}, Lcom/android/systemui/statusbar/policy/Clock;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput p1, p0, Lcom/android/systemui/statusbar/WeatherView;->mCurStyle:I

    :cond_0
    return-void

    :pswitch_0
    const v3, 0x7f0b00f1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    const v3, 0x7f0b00f3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

    invoke-virtual {v3, v4}, Lcom/android/systemui/statusbar/ColorImageTextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescriptionView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-boolean v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mIsNotch:Z

    if-nez v3, :cond_1

    const v3, 0x7f0b00f4

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    :goto_1
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherType:I

    const-string/jumbo v3, ""

    iput-object v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

    invoke-virtual {v3, v5}, Lcom/android/systemui/statusbar/ColorImageTextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescriptionView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const v3, 0x7f0b00f5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    const v3, 0x7f0b016c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getTemperature()I
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static getWeatherName(ILandroid/content/Context;Z)Ljava/lang/String;
    .locals 5

    if-nez p0, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz p2, :cond_0

    const/4 v2, 0x2

    :goto_0
    const v4, 0x7f110006

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080054

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v0, v2

    if-ltz p0, :cond_2

    array-length v2, v0

    if-ge p0, v2, :cond_2

    aget-object v1, v0, p0

    :cond_2
    return-object v1
.end method

.method private initSunMilli()V
    .locals 3

    const/16 v2, 0xb

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x6

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunriseMill:J

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0x12

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->set(II)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunsetMill:J

    return-void
.end method

.method public static maybeAddUserId(Landroid/net/Uri;I)Landroid/net/Uri;
    .locals 4

    const/4 v3, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_0

    return-object p0

    :cond_0
    if-nez p0, :cond_1

    return-object v3

    :cond_1
    const/4 v1, -0x2

    if-eq p1, v1, :cond_2

    const-string/jumbo v1, "content"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/android/systemui/statusbar/WeatherView;->uriHasUserId(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1

    :cond_2
    return-object p0
.end method

.method private needShowPublishTime()Z
    .locals 6

    const/4 v0, 0x0

    iget-wide v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mPublishTime:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mPublishTime:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x1b7740

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private onBootCompleted()V
    .locals 2

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "com.miui.weather2"

    invoke-static {v0, v1}, Lcom/android/systemui/Util;->isPkgExist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mIsNotch:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/WeatherView;->changeStyle(I)V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->registerObserver()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->startQuery()V

    :goto_0
    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->refreshViews()V

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/systemui/statusbar/WeatherView;->changeStyle(I)V

    goto :goto_0
.end method

.method private onWeatherBgChange()V
    .locals 3

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mListeners:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-interface {v0, v2}, Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;->onColorChanged(Lcom/android/systemui/statusbar/ColorSuits;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private refreshViews()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->updateColorSuits()V

    iget v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mCurStyle:I

    if-nez v1, :cond_1

    iget v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherType:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/Object;

    iget v3, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherType:I

    iget-object v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/android/systemui/statusbar/WeatherView;->mIsNight:Z

    invoke-static {v3, v4, v5}, Lcom/android/systemui/statusbar/WeatherView;->getWeatherName(ILandroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const v3, 0x7f0d030c

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescription:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

    invoke-virtual {v1, v6}, Lcom/android/systemui/statusbar/ColorImageTextView;->setVisibility(I)V

    :goto_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/ColorImageTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->getTemperature()I

    move-result v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const v4, 0x7f110007

    invoke-virtual {v2, v4, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/ColorImageTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescription:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescriptionView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescriptionView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d030b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescription:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/android/systemui/statusbar/ColorImageTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private registerObserver()V
    .locals 5

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mQueryHandler:Lcom/android/systemui/statusbar/WeatherView$QueryHandler;

    if-nez v0, :cond_0

    const-string/jumbo v0, "weather"

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v1, v1, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mCurrentUserId:I

    invoke-static {v0, v1}, Lcom/android/systemui/Util;->isProviderAccess(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;-><init>(Lcom/android/systemui/statusbar/WeatherView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mQueryHandler:Lcom/android/systemui/statusbar/WeatherView$QueryHandler;

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lmiui/provider/Weather$LocalWeather;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x1

    const/4 v4, -0x2

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    :cond_0
    return-void
.end method

.method private startQuery()V
    .locals 8

    const/16 v1, 0x64

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mQueryHandler:Lcom/android/systemui/statusbar/WeatherView$QueryHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mQueryHandler:Lcom/android/systemui/statusbar/WeatherView$QueryHandler;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;->cancelOperation(I)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mQueryHandler:Lcom/android/systemui/statusbar/WeatherView$QueryHandler;

    sget-object v3, Lmiui/provider/Weather$LocalWeather;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mService:Lcom/android/systemui/statusbar/phone/PhoneStatusBar;

    iget v4, v4, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->mCurrentUserId:I

    invoke-static {v3, v4}, Lcom/android/systemui/statusbar/WeatherView;->maybeAddUserId(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/systemui/statusbar/WeatherView;->sProjection:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/systemui/statusbar/WeatherView$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private unregisterObserver()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mQueryHandler:Lcom/android/systemui/statusbar/WeatherView$QueryHandler;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mQueryHandler:Lcom/android/systemui/statusbar/WeatherView$QueryHandler;

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method private updateColorSuits()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->getColorSuits()Lcom/android/systemui/statusbar/ColorSuits;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    if-eq v1, v0, :cond_0

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->onWeatherBgChange()V

    :cond_0
    return-void
.end method

.method private updateIsNight()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->isNight()Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mIsNight:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mIsNight:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->refreshViews()V

    :cond_0
    return-void
.end method

.method private updateShowPublishTime()V
    .locals 2

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->needShowPublishTime()Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mShowPublishTime:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mShowPublishTime:Z

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->refreshViews()V

    :cond_0
    return-void
.end method

.method private updateWeatherInfo(Landroid/database/Cursor;)V
    .locals 14

    const/4 v10, -0x1

    const-wide/16 v12, 0x0

    iput v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherType:I

    iput v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mAqi:I

    const-string/jumbo v10, ""

    iput-object v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    const/4 v10, 0x1

    iput v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureUnit:I

    iput-wide v12, p0, Lcom/android/systemui/statusbar/WeatherView;->mPublishTime:J

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->initSunMilli()V

    if-eqz p1, :cond_5

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string/jumbo v0, ""

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v10

    if-eqz v10, :cond_4

    const-string/jumbo v10, "temperature"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    xor-int/lit8 v5, v10, 0x1

    if-eqz v5, :cond_0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "temperature"

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, "d"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperature:Ljava/lang/String;

    :cond_0
    const-string/jumbo v10, "temperature_unit"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    iput v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureUnit:I

    const-string/jumbo v10, "weather_type"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    iput v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherType:I

    const-string/jumbo v10, "sunrise"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-string/jumbo v10, "sunset"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-string/jumbo v10, "aqilevel"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    if-nez v10, :cond_1

    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iput v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mAqi:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    const-string/jumbo v10, "publish_time"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mPublishTime:J

    iget-wide v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mPublishTime:J

    cmp-long v10, v10, v12

    if-lez v10, :cond_2

    new-instance v1, Ljava/text/SimpleDateFormat;

    iget-object v10, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    const v11, 0x7f0d0309

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    :cond_2
    cmp-long v10, v6, v12

    if-nez v10, :cond_3

    cmp-long v10, v8, v12

    if-eqz v10, :cond_4

    :cond_3
    iput-wide v6, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunriseMill:J

    iput-wide v8, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunsetMill:J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    :try_start_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_5
    :goto_1
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v2

    :try_start_4
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catchall_0
    move-exception v10

    :try_start_6
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :goto_2
    throw v10

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :catch_4
    move-exception v3

    goto :goto_0
.end method

.method public static uriHasUserId(Landroid/net/Uri;)Z
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getUserInfo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public getColorSuits()Lcom/android/systemui/statusbar/ColorSuits;
    .locals 2

    iget v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherType:I

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->DEFAULT:Lcom/android/systemui/statusbar/ColorSuits;

    return-object v0

    :pswitch_0
    iget-boolean v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mIsNight:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->CLEAR:Lcom/android/systemui/statusbar/ColorSuits;

    return-object v0

    :cond_0
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SUNNYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->SUNNYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    array-length v1, v1

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/WeatherView;->getRandInt(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->CLOUDYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->CLOUDYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    array-length v1, v1

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/WeatherView;->getRandInt(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->CLOUDYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->CLOUDYSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    array-length v1, v1

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/WeatherView;->getRandInt(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0

    :pswitch_3
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->FOG:Lcom/android/systemui/statusbar/ColorSuits;

    return-object v0

    :pswitch_4
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->HAZE:Lcom/android/systemui/statusbar/ColorSuits;

    return-object v0

    :pswitch_5
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->RAINSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    sget-object v1, Lcom/android/systemui/statusbar/ColorSuits;->RAINSUITS:[Lcom/android/systemui/statusbar/ColorSuits;

    array-length v1, v1

    invoke-virtual {p0, v1}, Lcom/android/systemui/statusbar/WeatherView;->getRandInt(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0

    :pswitch_6
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SNOW:Lcom/android/systemui/statusbar/ColorSuits;

    return-object v0

    :pswitch_7
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->SAND:Lcom/android/systemui/statusbar/ColorSuits;

    return-object v0

    :pswitch_8
    sget-object v0, Lcom/android/systemui/statusbar/ColorSuits;->HAIL:Lcom/android/systemui/statusbar/ColorSuits;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method getRandInt(I)I
    .locals 4

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    rem-int/2addr v0, p1

    return v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method isNight()Z
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-wide/32 v6, 0x5265c00

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    rem-long v0, v4, v6

    iget-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunriseMill:J

    add-long/2addr v4, v6

    rem-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunriseMill:J

    iget-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunsetMill:J

    add-long/2addr v4, v6

    rem-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunsetMill:J

    iget-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunriseMill:J

    iget-wide v6, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunsetMill:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    iget-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunriseMill:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    iget-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunsetMill:J

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0

    :cond_1
    iget-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunriseMill:J

    cmp-long v4, v0, v4

    if-ltz v4, :cond_2

    iget-wide v4, p0, Lcom/android/systemui/statusbar/WeatherView;->mSunsetMill:J

    cmp-long v4, v0, v4

    if-lez v4, :cond_3

    :cond_2
    :goto_1
    return v2

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method protected onAttachedToWindow()V
    .locals 6

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v0, "android.intent.action.TIME_TICK"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mTimeTickReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-static {}, Lcom/android/systemui/Util;->getTimeTickHandler()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v0, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "package"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mPkgChangedReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->updateIsNight()V

    iget v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mColor:I

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/WeatherView;->setColor(I)V

    sget-boolean v0, Lcom/android/systemui/statusbar/phone/PhoneStatusBar;->sBootCompleted:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->onBootCompleted()V

    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mCurRotation:I

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mCurRotation:I

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mClock:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0059

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/android/systemui/statusbar/policy/Clock;->setTextSize(IF)V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->unregisterObserver()V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mTimeTickReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mPkgChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    invoke-direct {p0}, Lcom/android/systemui/statusbar/WeatherView;->initSunMilli()V

    const v0, 0x7f0f0036

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/WeatherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/ColorImageTextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

    const v0, 0x7f0f0038

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/WeatherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescriptionView:Landroid/widget/TextView;

    const v0, 0x7f0f0037

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/WeatherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/Clock;

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mDateView:Lcom/android/systemui/statusbar/policy/Clock;

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mDateView:Lcom/android/systemui/statusbar/policy/Clock;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/Clock;->setShowDate(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/ColorImageTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescriptionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mDateView:Lcom/android/systemui/statusbar/policy/Clock;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/Clock;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/android/systemui/Util;->isNotch()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f0098

    invoke-virtual {p0, v0}, Lcom/android/systemui/statusbar/WeatherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/systemui/statusbar/policy/Clock;

    iput-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mClock:Lcom/android/systemui/statusbar/policy/Clock;

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mClock:Lcom/android/systemui/statusbar/policy/Clock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/Clock;->setShowAmPm(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mClock:Lcom/android/systemui/statusbar/policy/Clock;

    iget-object v1, p0, Lcom/android/systemui/statusbar/WeatherView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/systemui/statusbar/policy/Clock;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/systemui/statusbar/WeatherView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerColorChangeListener(Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mColorSuits:Lcom/android/systemui/statusbar/ColorSuits;

    invoke-interface {p1, v0}, Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;->onColorChanged(Lcom/android/systemui/statusbar/ColorSuits;)V

    :cond_0
    return-void
.end method

.method public setColor(I)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lcom/android/systemui/statusbar/WeatherView;->mColor:I

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mTemperatureView:Lcom/android/systemui/statusbar/ColorImageTextView;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/ColorImageTextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mWeatherDescriptionView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mDateView:Lcom/android/systemui/statusbar/policy/Clock;

    invoke-virtual {v0, p1}, Lcom/android/systemui/statusbar/policy/Clock;->setEnabled(Z)V

    return-void
.end method

.method public unregisterColorChangeListener(Lcom/android/systemui/statusbar/WeatherView$ColorChangeListener;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/systemui/statusbar/WeatherView;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
