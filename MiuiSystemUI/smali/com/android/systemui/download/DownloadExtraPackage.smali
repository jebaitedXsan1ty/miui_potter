.class public Lcom/android/systemui/download/DownloadExtraPackage;
.super Ljava/lang/Object;
.source "DownloadExtraPackage.java"


# static fields
.field private static sAm:Landroid/app/AlarmManager;

.field private static sIndex:I

.field private static sPackageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSender:Landroid/app/PendingIntent;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/systemui/download/DownloadExtraPackage;->sIndex:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/systemui/download/DownloadExtraPackage;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/systemui/download/DownloadExtraPackage;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    sput-object v0, Lcom/android/systemui/download/DownloadExtraPackage;->sAm:Landroid/app/AlarmManager;

    return-void
.end method

.method public static addIndex()V
    .locals 3

    sget v0, Lcom/android/systemui/download/DownloadExtraPackage;->sIndex:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/systemui/download/DownloadExtraPackage;->sIndex:I

    sget v0, Lcom/android/systemui/download/DownloadExtraPackage;->sIndex:I

    sget-object v1, Lcom/android/systemui/download/DownloadExtraPackage;->sPackageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/android/systemui/download/DownloadExtraPackage;->sAm:Landroid/app/AlarmManager;

    sget-object v1, Lcom/android/systemui/download/DownloadExtraPackage;->sSender:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    const-string/jumbo v0, "DownloadExtraPackage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "cancel alarmManager packageList size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/systemui/download/DownloadExtraPackage;->sPackageList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static getDestPackages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/android/systemui/download/DownloadExtraPackage;->sPackageList:Ljava/util/List;

    return-object v0
.end method

.method public static getIndex()I
    .locals 1

    sget v0, Lcom/android/systemui/download/DownloadExtraPackage;->sIndex:I

    return v0
.end method


# virtual methods
.method public queryDestPackages()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v5, "miui.intent.action.UPDATE_MIUI_APPS_LIB"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/systemui/download/DownloadExtraPackage;->mContext:Landroid/content/Context;

    const/16 v6, 0x40

    const/4 v7, -0x2

    invoke-static {v5, v1, v6, v7}, Lcom/android/systemui/CompatibilityN;->queryBroadcastReceiversAsUser(Landroid/content/Context;Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v4

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ResolveInfo;

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public setSenderAlarm()V
    .locals 9

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/systemui/download/DownloadExtraPackage;->queryDestPackages()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/download/DownloadExtraPackage;->sPackageList:Ljava/util/List;

    sput v1, Lcom/android/systemui/download/DownloadExtraPackage;->sIndex:I

    sget-object v0, Lcom/android/systemui/download/DownloadExtraPackage;->sPackageList:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/systemui/download/DownloadExtraPackage;->sPackageList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v8, Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/systemui/download/DownloadExtraPackage;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/systemui/statusbar/phone/UpdateAppsReceiver;

    invoke-direct {v8, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/android/systemui/download/DownloadExtraPackage;->mContext:Landroid/content/Context;

    const/high16 v2, 0x8000000

    invoke-static {v0, v1, v8, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sput-object v0, Lcom/android/systemui/download/DownloadExtraPackage;->sSender:Landroid/app/PendingIntent;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v0, 0xd

    const/16 v2, 0xa

    invoke-virtual {v7, v0, v2}, Ljava/util/Calendar;->add(II)V

    sget-object v0, Lcom/android/systemui/download/DownloadExtraPackage;->sAm:Landroid/app/AlarmManager;

    sget-object v2, Lcom/android/systemui/download/DownloadExtraPackage;->sSender:Landroid/app/PendingIntent;

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    sget-object v0, Lcom/android/systemui/download/DownloadExtraPackage;->sAm:Landroid/app/AlarmManager;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sget-object v6, Lcom/android/systemui/download/DownloadExtraPackage;->sSender:Landroid/app/PendingIntent;

    const-wide/32 v4, 0x493e0

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    return-void
.end method
