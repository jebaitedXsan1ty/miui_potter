.class public Lcom/android/systemui/analytics/NotificationRemoveAllEvent;
.super Ljava/lang/Object;
.source "NotificationRemoveAllEvent.java"


# instance fields
.field private mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

.field private mRemoveIndex:I

.field private mRemoveLocation:Ljava/lang/String;

.field private mRemoveTimestamp:J

.field private mSessionIndex:I


# direct methods
.method public constructor <init>(Lcom/android/systemui/analytics/RemoveAllEvent;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/android/systemui/analytics/RemoveAllEvent;->notificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    iget-object v0, p1, Lcom/android/systemui/analytics/RemoveAllEvent;->location:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mRemoveLocation:Ljava/lang/String;

    iget v0, p1, Lcom/android/systemui/analytics/RemoveAllEvent;->index:I

    iput v0, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mRemoveIndex:I

    iget v0, p1, Lcom/android/systemui/analytics/RemoveAllEvent;->sessionIndex:I

    iput v0, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mSessionIndex:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mRemoveTimestamp:J

    return-void
.end method


# virtual methods
.method public getTinyData()Lcom/android/systemui/analytics/TinyData;
    .locals 5

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    new-instance v1, Lcom/android/systemui/analytics/TinyData;

    const-string/jumbo v2, "notification"

    const-string/jumbo v3, "remove_all"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/android/systemui/analytics/TinyData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 6

    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mNotificationEvent:Lcom/android/systemui/analytics/NotificationEvent;

    invoke-virtual {v1, p1}, Lcom/android/systemui/analytics/NotificationEvent;->wrapJSONObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p1

    :cond_0
    :try_start_0
    const-string/jumbo v1, "remove_location"

    iget-object v2, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mRemoveLocation:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget v1, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mRemoveIndex:I

    if-lez v1, :cond_1

    :try_start_1
    const-string/jumbo v1, "index"

    iget v2, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mRemoveIndex:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    iget v1, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mSessionIndex:I

    if-lez v1, :cond_2

    :try_start_2
    const-string/jumbo v1, "session_index"

    iget v2, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mSessionIndex:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_2
    iget-wide v2, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mRemoveTimestamp:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    :try_start_3
    const-string/jumbo v1, "remove_timestamp"

    iget-wide v2, p0, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->mRemoveTimestamp:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    :goto_3
    :try_start_4
    const-string/jumbo v1, "bucket"

    sget v2, Lcom/android/systemui/statusbar/phone/rank/RankConfig;->sRandomNum:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    const-string/jumbo v1, "user_fold"

    invoke-static {}, Lcom/android/systemui/Util;->isUserFold()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    return-object p1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_4

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_5
.end method
