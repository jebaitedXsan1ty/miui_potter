.class public Lcom/android/systemui/analytics/NotificationEventSender;
.super Ljava/lang/Object;
.source "NotificationEventSender.java"


# static fields
.field public static DEBUG:Z


# instance fields
.field private final XMSF:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "NSender"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/systemui/analytics/NotificationEventSender;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "com.xiaomi.xmsf"

    iput-object v0, p0, Lcom/android/systemui/analytics/NotificationEventSender;->XMSF:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/systemui/analytics/NotificationEventSender;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public onADBlock(Lcom/android/systemui/analytics/ADBlock;)V
    .locals 3
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    iget-object v1, p1, Lcom/android/systemui/analytics/ADBlock;->adId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    sget-boolean v1, Lcom/android/systemui/analytics/NotificationEventSender;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "NSender"

    iget-object v2, p1, Lcom/android/systemui/analytics/ADBlock;->adId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.adblock"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_2

    const-string/jumbo v1, "com.miui.msa.global"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    const-string/jumbo v1, "adid"

    iget-object v2, p1, Lcom/android/systemui/analytics/ADBlock;->adId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationEventSender;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void

    :cond_2
    const-string/jumbo v1, "com.miui.systemAdSolution"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onArriveNotification(Lcom/android/systemui/analytics/ArriveEvent;)V
    .locals 3
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    new-instance v0, Lcom/android/systemui/analytics/NotificationArriveEvent;

    invoke-direct {v0}, Lcom/android/systemui/analytics/NotificationArriveEvent;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/systemui/analytics/NotificationArriveEvent;->setArriveEvent(Lcom/android/systemui/analytics/ArriveEvent;)V

    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/systemui/analytics/NotificationArriveEvent;->getTinyData()Lcom/android/systemui/analytics/TinyData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public onBlockNotification(Lcom/android/systemui/analytics/BlockEvent;)V
    .locals 4
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    new-instance v1, Lcom/android/systemui/analytics/NotificationBlockEvent;

    invoke-direct {v1}, Lcom/android/systemui/analytics/NotificationBlockEvent;-><init>()V

    invoke-virtual {v1, p1}, Lcom/android/systemui/analytics/NotificationBlockEvent;->setBlockEvent(Lcom/android/systemui/analytics/BlockEvent;)V

    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/systemui/analytics/NotificationBlockEvent;->getTinyData()Lcom/android/systemui/analytics/TinyData;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    const-string/jumbo v2, "com.miui.systemAdSolution"

    iget-object v3, p1, Lcom/android/systemui/analytics/BlockEvent;->pkg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "com.miui.msa.global"

    iget-object v3, p1, Lcom/android/systemui/analytics/BlockEvent;->pkg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    new-instance v0, Lcom/android/systemui/analytics/ADBlock;

    invoke-direct {v0}, Lcom/android/systemui/analytics/ADBlock;-><init>()V

    iget-object v2, p1, Lcom/android/systemui/analytics/BlockEvent;->adId:Ljava/lang/String;

    iput-object v2, v0, Lcom/android/systemui/analytics/ADBlock;->adId:Ljava/lang/String;

    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public onClickNotification(Lcom/android/systemui/analytics/ClickEvent;)V
    .locals 3
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    new-instance v0, Lcom/android/systemui/analytics/NotificationClickEvent;

    invoke-direct {v0}, Lcom/android/systemui/analytics/NotificationClickEvent;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/systemui/analytics/NotificationClickEvent;->setClickEvent(Lcom/android/systemui/analytics/ClickEvent;)V

    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/systemui/analytics/NotificationClickEvent;->getTinyData()Lcom/android/systemui/analytics/TinyData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public onExposeNotification(Lcom/android/systemui/analytics/ExposeEvent;)V
    .locals 3
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    new-instance v0, Lcom/android/systemui/analytics/NotificationExposeEvent;

    invoke-direct {v0}, Lcom/android/systemui/analytics/NotificationExposeEvent;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/systemui/analytics/NotificationExposeEvent;->setExposeEvent(Lcom/android/systemui/analytics/ExposeEvent;)V

    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/systemui/analytics/NotificationExposeEvent;->getTinyData()Lcom/android/systemui/analytics/TinyData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public onRemoveAllNotification(Lcom/android/systemui/analytics/NotificationRemoveAllEvent;)V
    .locals 2
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/systemui/analytics/NotificationRemoveAllEvent;->getTinyData()Lcom/android/systemui/analytics/TinyData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public onRemoveNotification(Lcom/android/systemui/analytics/RemoveEvent;)V
    .locals 3
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    new-instance v0, Lcom/android/systemui/analytics/NotificationRemoveEvent;

    invoke-direct {v0}, Lcom/android/systemui/analytics/NotificationRemoveEvent;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/systemui/analytics/NotificationRemoveEvent;->setRemoveEvent(Lcom/android/systemui/analytics/RemoveEvent;)V

    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/systemui/analytics/NotificationRemoveEvent;->getTinyData()Lcom/android/systemui/analytics/TinyData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public onSetImportance(Lcom/android/systemui/analytics/ImportanceEvent;)V
    .locals 3
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    new-instance v0, Lcom/android/systemui/analytics/NotificationImportanceEvent;

    invoke-direct {v0}, Lcom/android/systemui/analytics/NotificationImportanceEvent;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/systemui/analytics/NotificationImportanceEvent;->setImportanceEvent(Lcom/android/systemui/analytics/ImportanceEvent;)V

    invoke-static {}, Lorg/greenrobot/eventbus/EventBus;->getDefault()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/systemui/analytics/NotificationImportanceEvent;->getTinyData()Lcom/android/systemui/analytics/TinyData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public onTinyDataReceive(Lcom/android/systemui/analytics/TinyData;)V
    .locals 4
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    sget-boolean v1, Lcom/android/systemui/analytics/NotificationEventSender;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "NSender"

    invoke-virtual {p1}, Lcom/android/systemui/analytics/TinyData;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/android/systemui/Util;->isUserExperienceProgramEnable()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_2

    :cond_1
    return-void

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.xiaomi.xmsf.push.XMSF_UPLOAD_ACTIVE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.xiaomi.xmsf"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "pkgname"

    invoke-virtual {p1}, Lcom/android/systemui/analytics/TinyData;->getPkg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "category"

    invoke-virtual {p1}, Lcom/android/systemui/analytics/TinyData;->getCategory()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "name"

    invoke-virtual {p1}, Lcom/android/systemui/analytics/TinyData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "data"

    invoke-virtual {p1}, Lcom/android/systemui/analytics/TinyData;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/systemui/analytics/NotificationEventSender;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const-string/jumbo v3, "com.xiaomi.xmsf.permission.USE_XMSF_UPLOAD"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    return-void
.end method
