.class public Lcom/android/systemui/settings/CameraKeyActionAppPicker;
.super Lmiui/preference/PreferenceActivity;
.source "CameraKeyActionAppPicker.java"


# instance fields
.field private mActivities:Landroid/preference/PreferenceGroup;

.field private mLoading:Z


# direct methods
.method static synthetic -get0(Lcom/android/systemui/settings/CameraKeyActionAppPicker;)Landroid/preference/PreferenceGroup;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->mActivities:Landroid/preference/PreferenceGroup;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/systemui/settings/CameraKeyActionAppPicker;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->mLoading:Z

    return p1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v0, 0x7f060000

    invoke-virtual {p0, v0}, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "activities"

    invoke-virtual {p0, v0}, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->mActivities:Landroid/preference/PreferenceGroup;

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "camera_key_preferred_action_type"

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string/jumbo v2, "camera_key_preferred_action_app_component"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->finish()V

    const/4 v2, 0x1

    return v2
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    iget-boolean v1, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->mLoading:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/systemui/settings/CameraKeyActionAppPicker;->mLoading:Z

    new-instance v0, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;

    invoke-direct {v0, p0}, Lcom/android/systemui/settings/CameraKeyActionAppPicker$1;-><init>(Lcom/android/systemui/settings/CameraKeyActionAppPicker;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method
