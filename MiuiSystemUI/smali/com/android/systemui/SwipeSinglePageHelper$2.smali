.class Lcom/android/systemui/SwipeSinglePageHelper$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SwipeSinglePageHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/systemui/SwipeSinglePageHelper;->resetViewIfNeed(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/systemui/SwipeSinglePageHelper;

.field final synthetic val$animView:Landroid/view/View;

.field final synthetic val$canAnimViewBeDismissed:Z


# direct methods
.method constructor <init>(Lcom/android/systemui/SwipeSinglePageHelper;Landroid/view/View;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/systemui/SwipeSinglePageHelper$2;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    iput-object p2, p0, Lcom/android/systemui/SwipeSinglePageHelper$2;->val$animView:Landroid/view/View;

    iput-boolean p3, p0, Lcom/android/systemui/SwipeSinglePageHelper$2;->val$canAnimViewBeDismissed:Z

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper$2;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    iget-object v1, p0, Lcom/android/systemui/SwipeSinglePageHelper$2;->val$animView:Landroid/view/View;

    iget-boolean v2, p0, Lcom/android/systemui/SwipeSinglePageHelper$2;->val$canAnimViewBeDismissed:Z

    invoke-static {v0, v1, v2}, Lcom/android/systemui/SwipeSinglePageHelper;->-wrap0(Lcom/android/systemui/SwipeSinglePageHelper;Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/android/systemui/SwipeSinglePageHelper$2;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    invoke-static {v0}, Lcom/android/systemui/SwipeSinglePageHelper;->-get0(Lcom/android/systemui/SwipeSinglePageHelper;)Lcom/android/systemui/SwipeHelper$Callback;

    move-result-object v0

    iget-object v1, p0, Lcom/android/systemui/SwipeSinglePageHelper$2;->this$0:Lcom/android/systemui/SwipeSinglePageHelper;

    invoke-static {v1}, Lcom/android/systemui/SwipeSinglePageHelper;->-get1(Lcom/android/systemui/SwipeSinglePageHelper;)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/systemui/SwipeHelper$Callback;->onChildSnappedBack(Landroid/view/View;)V

    return-void
.end method
