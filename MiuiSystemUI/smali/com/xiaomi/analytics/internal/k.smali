.class final Lcom/xiaomi/analytics/internal/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/xiaomi/analytics/internal/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/analytics/internal/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "k"
.end annotation


# instance fields
.field final synthetic bs:Lcom/xiaomi/analytics/internal/b;


# direct methods
.method constructor <init>(Lcom/xiaomi/analytics/internal/b;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/k;->bs:Lcom/xiaomi/analytics/internal/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bO(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/k;->bs:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/b;->aU(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "SdkManager"

    const-string/jumbo v1, "download finished, use new analytics."

    invoke-static {v0, v1}, Lcom/xiaomi/analytics/internal/util/f;->p(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/k;->bs:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/b;->bk(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/xiaomi/analytics/internal/a/a;->ag()V

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/analytics/internal/k;->bs:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v1, v0}, Lcom/xiaomi/analytics/internal/b;->bd(Lcom/xiaomi/analytics/internal/b;Lcom/xiaomi/analytics/internal/a/a;)Lcom/xiaomi/analytics/internal/a/a;

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/k;->bs:Lcom/xiaomi/analytics/internal/b;

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/k;->bs:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v1}, Lcom/xiaomi/analytics/internal/b;->aU(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/analytics/internal/b;->bq(Lcom/xiaomi/analytics/internal/b;Lcom/xiaomi/analytics/internal/a/a;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/k;->bs:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/b;->aV(Lcom/xiaomi/analytics/internal/b;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/util/h;->A(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0
.end method
