.class Lcom/xiaomi/analytics/internal/d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/analytics/internal/e;,
        Lcom/xiaomi/analytics/internal/n;,
        Lcom/xiaomi/analytics/internal/o;
    }
.end annotation


# static fields
.field private static final aE:J

.field private static volatile bd:Lcom/xiaomi/analytics/internal/d;


# instance fields
.field private aW:Ljava/lang/String;

.field private aX:Ljava/lang/Runnable;

.field private aY:I

.field private aZ:Ljava/lang/String;

.field private ba:Ljava/lang/String;

.field private bb:Lcom/xiaomi/analytics/internal/e;

.field private bc:Ljava/lang/Runnable;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Lcom/xiaomi/analytics/internal/util/l;->z:I

    int-to-long v0, v0

    sput-wide v0, Lcom/xiaomi/analytics/internal/d;->aE:J

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/d;->aW:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/d;->aZ:Ljava/lang/String;

    new-instance v0, Lcom/xiaomi/analytics/internal/n;

    invoke-direct {v0, p0}, Lcom/xiaomi/analytics/internal/n;-><init>(Lcom/xiaomi/analytics/internal/d;)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/d;->bc:Ljava/lang/Runnable;

    new-instance v0, Lcom/xiaomi/analytics/internal/o;

    invoke-direct {v0, p0}, Lcom/xiaomi/analytics/internal/o;-><init>(Lcom/xiaomi/analytics/internal/d;)V

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/d;->aX:Ljava/lang/Runnable;

    invoke-static {p1}, Lcom/xiaomi/analytics/internal/util/h;->z(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/analytics/internal/d;->mContext:Landroid/content/Context;

    return-void
.end method

.method private bA(Ljava/lang/String;)J
    .locals 2

    :try_start_0
    const-string/jumbo v0, "-"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private declared-synchronized bB(J)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "analytics_updater"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "updateTime"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic bC(Lcom/xiaomi/analytics/internal/d;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic bD(Lcom/xiaomi/analytics/internal/d;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->aW:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bE(Lcom/xiaomi/analytics/internal/d;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->aX:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic bF(Lcom/xiaomi/analytics/internal/d;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->aZ:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bG(Lcom/xiaomi/analytics/internal/d;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->ba:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bH(Lcom/xiaomi/analytics/internal/d;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/d;->aW:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic bI(Lcom/xiaomi/analytics/internal/d;I)I
    .locals 0

    iput p1, p0, Lcom/xiaomi/analytics/internal/d;->aY:I

    return p1
.end method

.method static synthetic bJ(Lcom/xiaomi/analytics/internal/d;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/d;->aZ:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic bK(Lcom/xiaomi/analytics/internal/d;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/d;->by()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic bL(Lcom/xiaomi/analytics/internal/d;Ljava/lang/String;)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/xiaomi/analytics/internal/d;->bA(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic bM(Lcom/xiaomi/analytics/internal/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/d;->bz()V

    return-void
.end method

.method static synthetic bN(Lcom/xiaomi/analytics/internal/d;J)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/analytics/internal/d;->bB(J)V

    return-void
.end method

.method private declared-synchronized bx()J
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "analytics_updater"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "updateTime"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private by()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/analytics/internal/d;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/analytics/internal/util/g;->y(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/util/g;->y(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private bz()V
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/d;->bb:Lcom/xiaomi/analytics/internal/e;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/analytics/internal/d;->bb:Lcom/xiaomi/analytics/internal/e;

    iget-object v2, p0, Lcom/xiaomi/analytics/internal/d;->ba:Ljava/lang/String;

    iget v3, p0, Lcom/xiaomi/analytics/internal/d;->aY:I

    if-ne v3, v0, :cond_1

    :goto_0
    invoke-interface {v1, v2, v0}, Lcom/xiaomi/analytics/internal/e;->bO(Ljava/lang/String;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/xiaomi/analytics/internal/d;
    .locals 2

    const-class v1, Lcom/xiaomi/analytics/internal/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/xiaomi/analytics/internal/d;->bd:Lcom/xiaomi/analytics/internal/d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/analytics/internal/d;

    invoke-direct {v0, p0}, Lcom/xiaomi/analytics/internal/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/analytics/internal/d;->bd:Lcom/xiaomi/analytics/internal/d;

    :cond_0
    sget-object v0, Lcom/xiaomi/analytics/internal/d;->bd:Lcom/xiaomi/analytics/internal/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public bu()Z
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "UpdateManager"

    invoke-static {v0, v1}, Lcom/xiaomi/analytics/internal/util/n;->Z(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v5

    :cond_0
    invoke-static {}, Lcom/xiaomi/analytics/Analytics;->isUpdateEnable()Z

    move-result v0

    if-nez v0, :cond_1

    return v5

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/analytics/internal/d;->bx()J

    move-result-wide v0

    const-string/jumbo v2, "UpdateManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "last update check time is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/xiaomi/analytics/internal/util/f;->p(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    sget-wide v2, Lcom/xiaomi/analytics/internal/d;->aE:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    return v5

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method public bv(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "UpdateManager"

    invoke-static {v0, v1}, Lcom/xiaomi/analytics/internal/util/n;->Z(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string/jumbo v0, "UpdateManager"

    const-string/jumbo v1, "checkUpdate "

    invoke-static {v0, v1}, Lcom/xiaomi/analytics/internal/util/f;->p(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/d;->ba:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/d;->bc:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/util/a;->a(Ljava/lang/Runnable;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/analytics/internal/d;->bB(J)V

    return-void
.end method

.method public bw(Lcom/xiaomi/analytics/internal/e;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/d;->bb:Lcom/xiaomi/analytics/internal/e;

    return-void
.end method
