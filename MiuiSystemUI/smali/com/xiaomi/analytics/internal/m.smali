.class final Lcom/xiaomi/analytics/internal/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/analytics/internal/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "m"
.end annotation


# instance fields
.field final synthetic bu:Lcom/xiaomi/analytics/internal/b;


# direct methods
.method constructor <init>(Lcom/xiaomi/analytics/internal/b;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/analytics/internal/m;->bu:Lcom/xiaomi/analytics/internal/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/xiaomi/analytics/internal/b;->bc()Lcom/xiaomi/analytics/internal/b;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/analytics/internal/m;->bu:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/b;->bi(Lcom/xiaomi/analytics/internal/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/m;->bu:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/b;->aX(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/m;->bu:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/b;->aX(Lcom/xiaomi/analytics/internal/b;)Lcom/xiaomi/analytics/internal/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/xiaomi/analytics/internal/a/a;->ag()V

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/m;->bu:Lcom/xiaomi/analytics/internal/b;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/xiaomi/analytics/internal/b;->bg(Lcom/xiaomi/analytics/internal/b;Lcom/xiaomi/analytics/internal/a/a;)Lcom/xiaomi/analytics/internal/a/a;

    iget-object v0, p0, Lcom/xiaomi/analytics/internal/m;->bu:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v0}, Lcom/xiaomi/analytics/internal/b;->aV(Lcom/xiaomi/analytics/internal/b;)Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/analytics/internal/m;->bu:Lcom/xiaomi/analytics/internal/b;

    invoke-static {v2}, Lcom/xiaomi/analytics/internal/b;->aZ(Lcom/xiaomi/analytics/internal/b;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string/jumbo v0, "SdkManager"

    const-string/jumbo v2, "pending dex init executed, unregister and clear pending"

    invoke-static {v0, v2}, Lcom/xiaomi/analytics/internal/util/f;->p(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_1
    return-void

    :cond_0
    :try_start_3
    const-string/jumbo v0, "SdkManager"

    const-string/jumbo v2, "skip init dex"

    invoke-static {v0, v2}, Lcom/xiaomi/analytics/internal/util/f;->p(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "SdkManager"

    const-string/jumbo v2, "dexInitTask"

    invoke-static {v1, v2, v0}, Lcom/xiaomi/analytics/internal/util/f;->o(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
