.class public interface abstract Lcom/juphoon/cmcc/lemon/MtcImLargeConstants;
.super Ljava/lang/Object;
.source "MtcImLargeConstants.java"


# static fields
.field public static final EN_MTC_IM_LMSG_CONT_MSG_139MAIL_NEW:I = 0xd

.field public static final EN_MTC_IM_LMSG_CONT_MSG_CARD:I = 0xc

.field public static final EN_MTC_IM_LMSG_CONT_MSG_CHRED_BAG:I = 0x10

.field public static final EN_MTC_IM_LMSG_CONT_MSG_CLOUD_FILE:I = 0xa

.field public static final EN_MTC_IM_LMSG_CONT_MSG_CMRED_BAG:I = 0xb

.field public static final EN_MTC_IM_LMSG_CONT_MSG_COM_TEMP:I = 0xf

.field public static final EN_MTC_IM_LMSG_CONT_MSG_IMAGE_BMP:I = 0x6

.field public static final EN_MTC_IM_LMSG_CONT_MSG_IMAGE_JPEG:I = 0x5

.field public static final EN_MTC_IM_LMSG_CONT_MSG_IMDN:I = 0x7

.field public static final EN_MTC_IM_LMSG_CONT_MSG_REVOKE:I = 0x11

.field public static final EN_MTC_IM_LMSG_CONT_MSG_TXT_PLAIN:I = 0x4

.field public static final EN_MTC_IM_LMSG_CONT_MSG_TXT_SMS:I = 0xe

.field public static final EN_MTC_IM_LMSG_CONT_MSG_VEMOTICON:I = 0x9

.field public static final EN_MTC_IM_LMSG_CONT_MSG_XML:I = 0x8

.field public static final EN_MTC_IM_LMSG_CONT_OMAPUSH:I = 0x3

.field public static final EN_MTC_IM_LMSG_CONT_TXT_PLAIN:I = 0x1

.field public static final EN_MTC_IM_LMSG_CONT_UNKNOWN:I = 0x0

.field public static final EN_MTC_IM_LMSG_CONT_XML:I = 0x2

.field public static final EN_MTC_IM_LMSG_IMDN_BOTH:I = 0x4

.field public static final EN_MTC_IM_LMSG_IMDN_BURN:I = 0x5

.field public static final EN_MTC_IM_LMSG_IMDN_DELI:I = 0x2

.field public static final EN_MTC_IM_LMSG_IMDN_DISP:I = 0x3

.field public static final EN_MTC_IM_LMSG_IMDN_REG:I = 0x1

.field public static final EN_MTC_IM_LMSG_IMDN_UNKNOWN:I = 0x0

.field public static final EN_MTC_IM_LMSG_SPAM_FRAUD:I = 0x3

.field public static final EN_MTC_IM_LMSG_SPAM_JURISPRUDENCE:I = 0x2

.field public static final EN_MTC_IM_LMSG_SPAM_MALICIOUS_MARKETING:I = 0x4

.field public static final EN_MTC_IM_LMSG_SPAM_MAX:I = 0xa

.field public static final EN_MTC_IM_LMSG_SPAM_MIN:I = 0x0

.field public static final EN_MTC_IM_LMSG_SPAM_OTHER:I = 0x9

.field public static final EN_MTC_IM_LMSG_SPAM_POLITICAL_RUMOR:I = 0x1

.field public static final EN_MTC_IM_LMSG_SPAM_VIOLENCE:I = 0x5
