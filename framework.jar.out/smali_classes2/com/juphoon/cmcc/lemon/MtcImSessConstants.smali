.class public interface abstract Lcom/juphoon/cmcc/lemon/MtcImSessConstants;
.super Ljava/lang/Object;
.source "MtcImSessConstants.java"


# static fields
.field public static final EN_MTC_IM_SESS_AT_ALL:I = 0x1

.field public static final EN_MTC_IM_SESS_AT_NONE:I = 0x2

.field public static final EN_MTC_IM_SESS_AT_PARTIAL:I = 0x0

.field public static final EN_MTC_IM_SESS_CONT_LOCATION:I = 0x2

.field public static final EN_MTC_IM_SESS_CONT_MSG_CARD:I = 0xc

.field public static final EN_MTC_IM_SESS_CONT_MSG_CHRED_BAG:I = 0xe

.field public static final EN_MTC_IM_SESS_CONT_MSG_CLOUD_FILE:I = 0xa

.field public static final EN_MTC_IM_SESS_CONT_MSG_CMRED_BAG:I = 0xb

.field public static final EN_MTC_IM_SESS_CONT_MSG_COM_TEMP:I = 0xd

.field public static final EN_MTC_IM_SESS_CONT_MSG_FTHTTP:I = 0x7

.field public static final EN_MTC_IM_SESS_CONT_MSG_IMAGE_BMP:I = 0x5

.field public static final EN_MTC_IM_SESS_CONT_MSG_IMAGE_JPEG:I = 0x4

.field public static final EN_MTC_IM_SESS_CONT_MSG_IMDN:I = 0x6

.field public static final EN_MTC_IM_SESS_CONT_MSG_LOCATION:I = 0x8

.field public static final EN_MTC_IM_SESS_CONT_MSG_REVOKE:I = 0xf

.field public static final EN_MTC_IM_SESS_CONT_MSG_TXT_PLAIN:I = 0x3

.field public static final EN_MTC_IM_SESS_CONT_MSG_VEMOTICON:I = 0x9

.field public static final EN_MTC_IM_SESS_CONT_TXT_PLAIN:I = 0x1

.field public static final EN_MTC_IM_SESS_CONT_UNKNOWN:I = 0x0

.field public static final EN_MTC_IM_SESS_GRP_TYPE_ALL:I = 0x0

.field public static final EN_MTC_IM_SESS_GRP_TYPE_END:I = 0x4

.field public static final EN_MTC_IM_SESS_GRP_TYPE_ENTERPRISE:I = 0x2

.field public static final EN_MTC_IM_SESS_GRP_TYPE_GENERAL:I = 0x1

.field public static final EN_MTC_IM_SESS_GRP_TYPE_PARTY:I = 0x3

.field public static final EN_MTC_IM_SESS_IMDN_BOTH:I = 0x4

.field public static final EN_MTC_IM_SESS_IMDN_DELI:I = 0x2

.field public static final EN_MTC_IM_SESS_IMDN_DISP:I = 0x3

.field public static final EN_MTC_IM_SESS_IMDN_REG:I = 0x1

.field public static final EN_MTC_IM_SESS_IMDN_UNKNOWN:I = 0x0

.field public static final EN_MTC_IM_SESS_OFFLINE_CREATE:I = 0x2

.field public static final EN_MTC_IM_SESS_OFFLINE_MSG:I = 0x3

.field public static final EN_MTC_IM_SESS_ONLINE_CREATE:I = 0x0

.field public static final EN_MTC_IM_SESS_ONLINE_MSG:I = 0x1

.field public static final EN_MTC_IM_SESS_REJECT_REASON_BUSY:I = 0x0

.field public static final EN_MTC_IM_SESS_REJECT_REASON_DECLINE:I = 0x1

.field public static final EN_MTC_IM_SESS_STAT_ACTIVE:I = 0x3

.field public static final EN_MTC_IM_SESS_STAT_IDLE:I = 0x1

.field public static final EN_MTC_IM_SESS_STAT_INACTIVE:I = 0x4

.field public static final EN_MTC_IM_SESS_STAT_PENDING:I = 0x2

.field public static final EN_MTC_IM_SESS_STAT_UNKNOWN:I = 0x0

.field public static final MTC_IM_SESS_AT_ALL_MEMBER:I = -0x2
