.class Lcom/miui/whetstone/WhetstoneSleepModeController$1;
.super Landroid/content/BroadcastReceiver;
.source "WhetstoneSleepModeController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/whetstone/WhetstoneSleepModeController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;


# direct methods
.method constructor <init>(Lcom/miui/whetstone/WhetstoneSleepModeController;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    const/4 v10, 0x0

    const-string/jumbo v7, "WhetstoneSleepModeController"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onReceive action : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    const-string/jumbo v7, "action_sleep_state_changed"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string/jumbo v7, "pre_state"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string/jumbo v7, "cur_state"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    const-string/jumbo v8, "charging"

    const/4 v9, 0x1

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-static {v7, v8}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-set0(Lcom/miui/whetstone/WhetstoneSleepModeController;I)I

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    const-string/jumbo v8, "ab_type"

    invoke-virtual {p2, v8, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-static {v7, v8}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-set4(Lcom/miui/whetstone/WhetstoneSleepModeController;I)I

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    const-string/jumbo v8, "ab_enable"

    invoke-virtual {p2, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-static {v7, v8}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-set3(Lcom/miui/whetstone/WhetstoneSleepModeController;Z)Z

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    const-string/jumbo v8, "ab_proc_enable"

    invoke-virtual {p2, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-static {v7, v8}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-set2(Lcom/miui/whetstone/WhetstoneSleepModeController;Z)Z

    const-string/jumbo v7, "WhetstoneSleepModeController"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "receive sleep changed, pre = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v9, v3}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-wrap0(Lcom/miui/whetstone/WhetstoneSleepModeController;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", cur = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v9, v0}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-wrap0(Lcom/miui/whetstone/WhetstoneSleepModeController;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ", charging = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v9}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get2(Lcom/miui/whetstone/WhetstoneSleepModeController;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    iput v10, v2, Landroid/os/Message;->what:I

    iput v3, v2, Landroid/os/Message;->arg1:I

    iput v0, v2, Landroid/os/Message;->arg2:I

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v7}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get1(Lcom/miui/whetstone/WhetstoneSleepModeController;)Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    if-eqz p2, :cond_1

    const-string/jumbo v7, "sleep_control_cloud"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    const-string/jumbo v8, "enable_process_control"

    invoke-virtual {p2, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-static {v7, v8}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-set1(Lcom/miui/whetstone/WhetstoneSleepModeController;Z)Z

    const-string/jumbo v7, "process_white_list"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    const-string/jumbo v7, "disable_rtc_list"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string/jumbo v7, "clean_white_list"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    const-string/jumbo v7, "start_white_list"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    const-string/jumbo v7, "WhetstoneSleepModeController"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Process Control Enable : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v9}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get3(Lcom/miui/whetstone/WhetstoneSleepModeController;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v7}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get4(Lcom/miui/whetstone/WhetstoneSleepModeController;)Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->addProtectedProcessList(Ljava/util/List;)V

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-virtual {v7, v5, v6}, Lcom/miui/whetstone/WhetstoneSleepModeController;->setSleepCleanConfig(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v7}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get5(Lcom/miui/whetstone/WhetstoneSleepModeController;)Lcom/miui/whetstone/WhetstoneWakeUpManager;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->setDisableRTCList(Ljava/util/ArrayList;)V

    :cond_1
    if-eqz p2, :cond_2

    const-string/jumbo v7, "android.intent.action.USER_PRESENT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v7}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get0(Lcom/miui/whetstone/WhetstoneSleepModeController;)I

    move-result v7

    if-eqz v7, :cond_2

    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    iput v10, v2, Landroid/os/Message;->what:I

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v7}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get0(Lcom/miui/whetstone/WhetstoneSleepModeController;)I

    move-result v7

    iput v7, v2, Landroid/os/Message;->arg1:I

    iput v10, v2, Landroid/os/Message;->arg2:I

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v7}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get1(Lcom/miui/whetstone/WhetstoneSleepModeController;)Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    if-eqz p2, :cond_3

    const-string/jumbo v7, "sleep_wakeup_dump"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const-string/jumbo v7, "WhetstoneSleepModeController"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Process Control Enable : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v9}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get3(Lcom/miui/whetstone/WhetstoneSleepModeController;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v7}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get5(Lcom/miui/whetstone/WhetstoneSleepModeController;)Lcom/miui/whetstone/WhetstoneWakeUpManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->dump()V

    iget-object v7, p0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;->this$0:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-static {v7}, Lcom/miui/whetstone/WhetstoneSleepModeController;->-get4(Lcom/miui/whetstone/WhetstoneSleepModeController;)Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->dump()V

    :cond_3
    return-void
.end method
