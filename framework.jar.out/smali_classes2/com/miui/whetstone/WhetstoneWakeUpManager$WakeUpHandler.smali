.class Lcom/miui/whetstone/WhetstoneWakeUpManager$WakeUpHandler;
.super Landroid/os/Handler;
.source "WhetstoneWakeUpManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/whetstone/WhetstoneWakeUpManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WakeUpHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/whetstone/WhetstoneWakeUpManager;


# direct methods
.method public constructor <init>(Lcom/miui/whetstone/WhetstoneWakeUpManager;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager$WakeUpHandler;->this$0:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneWakeUpManager$WakeUpHandler;->this$0:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    invoke-static {v5}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->-get0(Lcom/miui/whetstone/WhetstoneWakeUpManager;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/miui/whetstone/WhetstoneSleepModeController;->getInstance(Landroid/content/Context;)Lcom/miui/whetstone/WhetstoneSleepModeController;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/whetstone/WhetstoneSleepModeController;->isInSleep()Z

    move-result v5

    if-nez v5, :cond_1

    return-void

    :cond_1
    iget v4, p1, Landroid/os/Message;->arg1:I

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/app/IActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    if-ne v5, v4, :cond_2

    const-string/jumbo v5, "WhetstoneWakeUpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "kill RTC wake up app uid : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [I

    iget v7, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    const/4 v8, 0x0

    aput v7, v6, v8

    const-string/jumbo v7, "whetstone : sleep clean"

    const/4 v8, 0x1

    invoke-interface {v5, v6, v7, v8}, Landroid/app/IActivityManager;->killPids([ILjava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v5, "WhetstoneWakeUpManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "WakeUpHandler killRtc : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
