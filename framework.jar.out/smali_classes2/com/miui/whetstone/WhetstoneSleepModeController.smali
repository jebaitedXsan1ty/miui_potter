.class public Lcom/miui/whetstone/WhetstoneSleepModeController;
.super Ljava/lang/Object;
.source "WhetstoneSleepModeController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/whetstone/WhetstoneSleepModeController$1;,
        Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;
    }
.end annotation


# static fields
.field private static final ACTION_SLEEP_CONTROL_CLOUD:Ljava/lang/String; = "sleep_control_cloud"

.field private static final ACTION_SLEEP_STATE_CHANGED:Ljava/lang/String; = "action_sleep_state_changed"

.field private static final ACTION_SLEEP_WAKEUP_DUMP:Ljava/lang/String; = "sleep_wakeup_dump"

.field private static final DEBUG:Z

.field public static final EXIT_SLEEP_MODE_ACTION:Ljava/lang/String; = "exit_sleep_mode_action"

.field public static final EXIT_SLEEP_MODE_REASON:Ljava/lang/String; = "exit_sleep_mode_reason"

.field public static final EXIT_SLEEP_MODE_REASON_TYPE:I = 0x1

.field private static final GOOGLE_TALKBACK_PACKAGE:Ljava/lang/String; = "com.google.android.marvin.talkback"

.field public static final INTENT_EXTRA_ABTEST_ENABLE:Ljava/lang/String; = "ab_enable"

.field public static final INTENT_EXTRA_ABTEST_PROC_ENABLE:Ljava/lang/String; = "ab_proc_enable"

.field public static final INTENT_EXTRA_ABTEST_TYPE:Ljava/lang/String; = "ab_type"

.field public static final INTENT_EXTRA_CHARGING:Ljava/lang/String; = "charging"

.field public static final INTENT_EXTRA_CLEAN_WHITE_LIST:Ljava/lang/String; = "clean_white_list"

.field public static final INTENT_EXTRA_CUR_STATE:Ljava/lang/String; = "cur_state"

.field public static final INTENT_EXTRA_DISABLE_RTC_LIST:Ljava/lang/String; = "disable_rtc_list"

.field public static final INTENT_EXTRA_PRE_STATE:Ljava/lang/String; = "pre_state"

.field public static final INTENT_EXTRA_PROCESS_CONTROL_ENABLE:Ljava/lang/String; = "enable_process_control"

.field public static final INTENT_EXTRA_PROCESS_WHITE_LIST:Ljava/lang/String; = "process_white_list"

.field public static final INTENT_EXTRA_START_WHITE_LIST:Ljava/lang/String; = "start_white_list"

.field private static final MSG_MODE_CHANGED:I = 0x0

.field public static final REASON_SLEEP_EXIT_BY_ACTIVITY:I = 0x1

.field private static final STATE_DEEP_SLEEP:I = 0x2

.field public static final STATE_IS_CHARGING:I = 0x1

.field private static final STATE_LIGHT_SLEEP1:I = 0x1

.field private static final STATE_LIGHT_SLEEP2:I = 0x3

.field public static final STATE_NO_CHARGING:I = 0x0

.field private static final STATE_NO_SLEEP:I = 0x0

.field private static final TAG:Ljava/lang/String; = "WhetstoneSleepModeController"

.field public static final THIRD_PARTY_RTC_WAKEUP_RECORD_PROP:Ljava/lang/String; = "persist.sys.rtc.wakeup_record"

.field public static final TYPE_BATTERY_COMPARE_TEST:I = 0x1

.field public static final TYPE_DEFAULT_COMPARE_TEST:I = 0x0

.field public static final TYPE_MODEL_COMPARE_TEST:I = 0x2

.field public static final TYPE_STRATEGY_COMPARE_TEST:I = 0x3

.field private static volatile mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurState:I

.field private mHandler:Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;

.field private mIsCharging:I

.field private mIsProcessControlEnable:Z

.field private final mLock:Ljava/lang/Object;

.field private mSleepABProcControlEnable:Z

.field private mSleepABTestEnable:Z

.field private mSleepABTestType:I

.field private mSleepCleanConfig:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

.field mSleepModeReceiver:Landroid/content/BroadcastReceiver;

.field private mWakeUpManager:Lcom/miui/whetstone/WhetstoneWakeUpManager;


# direct methods
.method static synthetic -get0(Lcom/miui/whetstone/WhetstoneSleepModeController;)I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    return v0
.end method

.method static synthetic -get1(Lcom/miui/whetstone/WhetstoneSleepModeController;)Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mHandler:Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;

    return-object v0
.end method

.method static synthetic -get2(Lcom/miui/whetstone/WhetstoneSleepModeController;)I
    .locals 1

    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mIsCharging:I

    return v0
.end method

.method static synthetic -get3(Lcom/miui/whetstone/WhetstoneSleepModeController;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mIsProcessControlEnable:Z

    return v0
.end method

.method static synthetic -get4(Lcom/miui/whetstone/WhetstoneSleepModeController;)Lcom/miui/whetstone/WhetstoneSleepCleanConfig;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepCleanConfig:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    return-object v0
.end method

.method static synthetic -get5(Lcom/miui/whetstone/WhetstoneSleepModeController;)Lcom/miui/whetstone/WhetstoneWakeUpManager;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mWakeUpManager:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    return-object v0
.end method

.method static synthetic -set0(Lcom/miui/whetstone/WhetstoneSleepModeController;I)I
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mIsCharging:I

    return p1
.end method

.method static synthetic -set1(Lcom/miui/whetstone/WhetstoneSleepModeController;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mIsProcessControlEnable:Z

    return p1
.end method

.method static synthetic -set2(Lcom/miui/whetstone/WhetstoneSleepModeController;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepABProcControlEnable:Z

    return p1
.end method

.method static synthetic -set3(Lcom/miui/whetstone/WhetstoneSleepModeController;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepABTestEnable:Z

    return p1
.end method

.method static synthetic -set4(Lcom/miui/whetstone/WhetstoneSleepModeController;I)I
    .locals 0

    iput p1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepABTestType:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/miui/whetstone/WhetstoneSleepModeController;I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/miui/whetstone/WhetstoneSleepModeController;->getStateString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/miui/whetstone/WhetstoneSleepModeController;IIIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/whetstone/WhetstoneSleepModeController;->handleSleepModeChanged(IIIZ)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    sput-boolean v0, Lcom/miui/whetstone/WhetstoneSleepModeController;->DEBUG:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mLock:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mHandler:Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;

    new-instance v0, Lcom/miui/whetstone/WhetstoneSleepModeController$1;

    invoke-direct {v0, p0}, Lcom/miui/whetstone/WhetstoneSleepModeController$1;-><init>(Lcom/miui/whetstone/WhetstoneSleepModeController;)V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepModeReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mContext:Landroid/content/Context;

    iput v2, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    new-instance v0, Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;-><init>(Lcom/miui/whetstone/WhetstoneSleepModeController;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mHandler:Lcom/miui/whetstone/WhetstoneSleepModeController$MyHandler;

    invoke-direct {p0}, Lcom/miui/whetstone/WhetstoneSleepModeController;->registerSleepModeChangeReceiver()V

    iput-boolean v2, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mIsProcessControlEnable:Z

    new-instance v0, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    invoke-direct {v0}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepCleanConfig:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->getInstance(Landroid/content/Context;)Lcom/miui/whetstone/WhetstoneWakeUpManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mWakeUpManager:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/miui/whetstone/WhetstoneSleepModeController;
    .locals 2

    sget-object v0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;

    if-nez v0, :cond_1

    const-class v1, Lcom/miui/whetstone/WhetstoneSleepModeController;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-direct {v0, p0}, Lcom/miui/whetstone/WhetstoneSleepModeController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getStateString(I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const-string/jumbo v0, "unknown"

    return-object v0

    :pswitch_0
    const-string/jumbo v0, "no sleep"

    return-object v0

    :pswitch_1
    const-string/jumbo v0, "light1"

    return-object v0

    :pswitch_2
    const-string/jumbo v0, "light2"

    return-object v0

    :pswitch_3
    const-string/jumbo v0, "deep"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private handleSleepModeChanged(IIIZ)V
    .locals 4

    iget-object v1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    if-ne v0, p2, :cond_0

    const-string/jumbo v0, "WhetstoneSleepModeController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "already in state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    if-eq v0, p1, :cond_1

    const-string/jumbo v0, "WhetstoneSleepModeController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sleep state not sync: mCurState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    invoke-direct {p0, v3}, Lcom/miui/whetstone/WhetstoneSleepModeController;->getStateString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ",preState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/miui/whetstone/WhetstoneSleepModeController;->getStateString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string/jumbo v0, "WhetstoneSleepModeController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "state change: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    invoke-direct {p0, v3}, Lcom/miui/whetstone/WhetstoneSleepModeController;->getStateString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "->"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p2}, Lcom/miui/whetstone/WhetstoneSleepModeController;->getStateString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput p2, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepCleanConfig:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    invoke-virtual {v0, p2}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->setSleepState(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    packed-switch p2, :pswitch_data_0

    :cond_2
    :goto_0
    :pswitch_0
    monitor-exit v1

    return-void

    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mWakeUpManager:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->onSleepModeEnter(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_2
    if-nez p3, :cond_2

    :try_start_3
    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mWakeUpManager:Lcom/miui/whetstone/WhetstoneWakeUpManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->onSleepModeEnter(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private registerSleepModeChangeReceiver()V
    .locals 3

    sget-boolean v1, Lcom/miui/whetstone/WhetstoneSleepModeController;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "WhetstoneSleepModeController"

    const-string/jumbo v2, "register sleep mode receiver"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "action_sleep_state_changed"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "sleep_control_cloud"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "sleep_wakeup_dump"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private sendExitSleepModeBroadCast(I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "exit_sleep_mode_action"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "exit_sleep_mode_reason"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void
.end method


# virtual methods
.method public checkIfExitSleepMode(I)Z
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    if-eqz v0, :cond_0

    const-string/jumbo v0, "WhetstoneSleepModeController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "exit the sleep mode reason is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    iget v1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mIsCharging:I

    invoke-direct {p0, v0, v3, v1, v3}, Lcom/miui/whetstone/WhetstoneSleepModeController;->handleSleepModeChanged(IIIZ)V

    invoke-direct {p0, p1}, Lcom/miui/whetstone/WhetstoneSleepModeController;->sendExitSleepModeBroadCast(I)V

    const/4 v0, 0x1

    return v0

    :cond_0
    return v3
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "=========== dump SleepModeController ============"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method public exitSleepModeByActivity()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    if-eqz v0, :cond_0

    const-string/jumbo v0, "WhetstoneSleepModeController"

    const-string/jumbo v1, "exitSleepModeByActivity"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v2, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    iget-object v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->getInstance(Landroid/content/Context;)Lcom/miui/whetstone/WhetstoneWakeUpManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->onSleepModeEnter(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/whetstone/WhetstoneSleepModeController;->sendExitSleepModeBroadCast(I)V

    :cond_0
    return-void
.end method

.method public isForbidProcessStartBySleepMode(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mIsProcessControlEnable:Z

    if-nez v0, :cond_0

    return v2

    :cond_0
    const-string/jumbo v0, "com.google.android.marvin.talkback"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v3, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepCleanConfig:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, p2, p1, v0}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->checkIfProcessStartAllow(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const-string/jumbo v0, "WhetstoneSleepModeController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "whtetstone process start is allow by sleep mode : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ", processName : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "WhetstoneSleepModeController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "whtetstone process start is forbid by sleep mode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", processName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method public isInCharging()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mIsCharging:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInSleep()Z
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mCurState:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    return v1
.end method

.method public isProcessControlEnable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mIsProcessControlEnable:Z

    return v0
.end method

.method public isSleepABProcControlEnable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepABProcControlEnable:Z

    return v0
.end method

.method public isSleepABTestEnableByType(I)Z
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepABTestEnable:Z

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepABTestType:I

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method

.method public isSleepModeEnableByType(I)Z
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepABTestEnable:Z

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget v0, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepABTestType:I

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method

.method public setSleepCleanConfig(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v8, 0x2

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepCleanConfig:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    invoke-virtual {v5, v2, v8}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->addPackageControlCleanInfo2Config(Ljava/lang/String;I)Z

    goto :goto_0

    :cond_0
    const-string/jumbo v5, "WhetstoneSleepModeController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Sleep Clean WhiteList : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepCleanConfig:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    invoke-virtual {v5, v2, v8}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->addPackageControlStartInfo2Config(Ljava/lang/String;I)Z

    goto :goto_1

    :cond_2
    const-string/jumbo v5, "WhetstoneSleepModeController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Sleep Start WhiteList : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mContext:Landroid/content/Context;

    const-string/jumbo v6, "accessibility"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v4, Landroid/speech/tts/TtsEngines;

    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/speech/tts/TtsEngines;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Landroid/speech/tts/TtsEngines;->getDefaultEngine()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/miui/whetstone/WhetstoneSleepModeController;->mSleepCleanConfig:Lcom/miui/whetstone/WhetstoneSleepCleanConfig;

    invoke-virtual {v5, v1, v8, v8}, Lcom/miui/whetstone/WhetstoneSleepCleanConfig;->addPackageControlInfo2Config(Ljava/lang/String;II)Z

    const-string/jumbo v5, "WhetstoneSleepModeController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "TtsEngines : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return-void
.end method
