.class public Lcom/miui/whetstone/component/PackageComponentPolicy;
.super Ljava/lang/Object;
.source "PackageComponentPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field public static final MATCH_METHOD_EQUAL:I = 0x1

.field public static final MATCH_METHOD_INCLUDE:I = 0x2

.field public static final MATCH_METHOD_NOT_EQUAL:I = 0x4

.field public static final MATCH_METHOD_NOT_INCLUDE:I = 0x8

.field public static final POLICY_CHECK_ALLOW:I = 0x2

.field public static final POLICY_CHECK_DENY:I = 0x4

.field public static final POLICY_CHECK_FLAG:I = 0x1

.field public static final POLICY_FLAG_CALLED_AMS_RESTART:I = 0x2000

.field public static final POLICY_FLAG_CALLED_CKECK_INTENT:I = 0x8000

.field public static final POLICY_FLAG_CALLED_FOREGROUND:I = 0x10000

.field public static final POLICY_FLAG_CALLED_INPUT_METHOD:I = 0x20000

.field public static final POLICY_FLAG_CALLED_SELF:I = 0x4000

.field public static final POLICY_FLAG_CALLED_SYSTEM:I = 0x1000

.field public static final POLICY_FLAG_CALLER_FOREGROUND:I = 0x4

.field public static final POLICY_FLAG_CALLER_SYSTEM:I = 0x1

.field public static final POLICY_FLAG_CALLER_SYSTEM_UID:I = 0x2

.field public static final POLICY_FLAG_UNKNOWN:I = 0x0

.field public static final POLICY_TYPE_ALL:I = 0xf

.field public static final POLICY_TYPE_BROADCAST:I = 0x2

.field public static final POLICY_TYPE_COMPONENT:I = 0x1

.field public static final POLICY_TYPE_PUSH:I = 0x4

.field public static final POLICY_TYPE_SLEEP:I = 0x8

.field public static final POLICY_TYPE_UNKNOWN:I = 0x0

.field public static final START_BY_ACTIVITY:I = 0x1

.field public static final START_BY_ALL:I = 0xf

.field public static final START_BY_PROVIDER:I = 0x8

.field public static final START_BY_RECEIVER:I = 0x4

.field public static final START_BY_SERVICE:I = 0x2

.field public static final START_BY_UNKNOWN:I = 0x0

.field public static final START_TIME_WITH_TYPE_ACTIVITY:I = 0x0

.field public static final START_TIME_WITH_TYPE_PROCIDER:I = 0x3

.field public static final START_TIME_WITH_TYPE_RECEIVER:I = 0x2

.field public static final START_TIME_WITH_TYPE_SERVICE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "PackageComponentPolicy"

.field private static final sMutex:Ljava/lang/Object;


# instance fields
.field private mBlackListWithStartTypeAsCalled:[Z

.field private mBlackListWithStartTypeAsCaller:[Z

.field private mPkgName:Ljava/lang/String;

.field private mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/miui/whetstone/component/ComponentPolicyInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mProcessBlackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;",
            ">;"
        }
    .end annotation
.end field

.field private mProcessWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;",
            ">;"
        }
    .end annotation
.end field

.field private mStartTimeByType:[J

.field private mWhiteIntents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWhiteListWithStartTypeAsCalled:[Z

.field private mWhiteListWithStartTypeAsCaller:[Z


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/miui/whetstone/component/PackageComponentPolicy;->DEBUG:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/miui/whetstone/component/WtComponentManager;->isDebug()Z

    move-result v0

    sput-boolean v0, Lcom/miui/whetstone/component/PackageComponentPolicy;->DEBUG:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/miui/whetstone/component/PackageComponentPolicy;->sMutex:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [Z

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    new-array v0, v1, [Z

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    new-array v0, v1, [Z

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    new-array v0, v1, [Z

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessWhiteList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessBlackList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteIntents:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-array v0, v1, [J

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mStartTimeByType:[J

    iput-object p1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPkgName:Ljava/lang/String;

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_2
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_3
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_4
    .array-data 8
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public checkPackagePolicyWithStartType(IILjava/lang/String;ILandroid/content/Intent;)Z
    .locals 8

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    const/4 v0, 0x0

    const/4 v7, 0x1

    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_0

    invoke-virtual {p0, p5}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isIntentWhiteList(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v7, 0x1

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getStartType()I

    move-result v1

    and-int/2addr v1, p2

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->isCommonPolicy()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getEnableStatus()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPkgName:Ljava/lang/String;

    move v2, p1

    move v3, p4

    move-object v4, p5

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->checkPolicyResult(Ljava/lang/String;IILandroid/content/Intent;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v7, 0x0

    :cond_1
    return v7
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    return-void

    :cond_0
    const-string/jumbo v6, ""

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v6, " ### Package Policy ### "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Package: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPkgName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Total: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v7}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v6, ""

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v6, " ### Package Called White Lists ### "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " Activity: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    aget-boolean v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Service: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    aget-boolean v7, v7, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Receiver: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    aget-boolean v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Provider: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    aget-boolean v7, v7, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessWhiteList:Ljava/util/List;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;

    invoke-virtual {v4}, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string/jumbo v6, ""

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v6, " ### Package Called Black Lists ### "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " Activity: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    aget-boolean v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Service: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    aget-boolean v7, v7, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Receiver: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    aget-boolean v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Provider: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    aget-boolean v7, v7, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessBlackList:Ljava/util/List;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;

    invoke-virtual {v4}, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string/jumbo v6, ""

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v6, " ### Package Caller White Lists ### "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " Activity: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    aget-boolean v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Service: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    aget-boolean v7, v7, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Receiver: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    aget-boolean v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Provider: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    aget-boolean v7, v7, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v6, ""

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v6, " ### Package Caller Black Lists ### "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, " Activity: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    aget-boolean v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Service: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    aget-boolean v7, v7, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Receiver: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    aget-boolean v7, v7, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " Provider: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    aget-boolean v7, v7, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v6, ""

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v6, " ### Package White Intents ### "

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteIntents:Ljava/util/List;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const-string/jumbo v6, ""

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    return-void
.end method

.method public dumpPackagtCommonInfo(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public getPackagePolicy(Ljava/lang/String;)Lcom/miui/whetstone/component/PackageComponentPolicy;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPkgName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPackageStartTimeWithType(I)J
    .locals 2

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mStartTimeByType:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public getPkgName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getPolicy(I)Lcom/miui/whetstone/component/ComponentPolicyInfo;
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v2

    if-ne v2, p1, :cond_0

    return-object v0

    :cond_1
    return-object v3
.end method

.method public inCheckList(Ljava/lang/String;IZZ)Z
    .locals 8

    if-eqz p1, :cond_3

    const/4 v1, 0x1

    :goto_0
    const/4 v5, 0x0

    if-eqz p4, :cond_6

    if-eqz p3, :cond_4

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    :goto_1
    sget-object v7, Lcom/miui/whetstone/component/PackageComponentPolicy;->sMutex:Ljava/lang/Object;

    monitor-enter v7

    packed-switch p2, :pswitch_data_0

    :goto_2
    :pswitch_0
    monitor-exit v7

    if-eqz v1, :cond_2

    if-eqz p3, :cond_5

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessWhiteList:Ljava/util/List;

    :goto_3
    sget-object v7, Lcom/miui/whetstone/component/PackageComponentPolicy;->sMutex:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;

    iget-object v6, v3, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->processName:Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v3, p2}, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->getCheckResult(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v5

    if-eqz v5, :cond_0

    :cond_1
    :goto_4
    monitor-exit v7

    :cond_2
    return v5

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    goto :goto_1

    :pswitch_1
    const/4 v6, 0x0

    :try_start_1
    aget-boolean v5, v0, v6

    goto :goto_2

    :pswitch_2
    const/4 v6, 0x1

    aget-boolean v5, v0, v6

    goto :goto_2

    :pswitch_3
    const/4 v6, 0x2

    aget-boolean v5, v0, v6

    goto :goto_2

    :pswitch_4
    const/4 v6, 0x3

    aget-boolean v5, v0, v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_5
    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessBlackList:Ljava/util/List;

    goto :goto_3

    :catchall_1
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_6
    if-eqz p3, :cond_7

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    :goto_5
    sget-object v7, Lcom/miui/whetstone/component/PackageComponentPolicy;->sMutex:Ljava/lang/Object;

    monitor-enter v7

    packed-switch p2, :pswitch_data_1

    :pswitch_5
    goto :goto_4

    :pswitch_6
    const/4 v6, 0x0

    :try_start_2
    aget-boolean v5, v0, v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    goto :goto_5

    :pswitch_7
    const/4 v6, 0x1

    :try_start_3
    aget-boolean v5, v0, v6

    goto :goto_4

    :pswitch_8
    const/4 v6, 0x2

    aget-boolean v5, v0, v6

    goto :goto_4

    :pswitch_9
    const/4 v6, 0x3

    aget-boolean v5, v0, v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_4

    :catchall_2
    move-exception v6

    monitor-exit v7

    throw v6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_9
    .end packed-switch
.end method

.method public isEmpty()Z
    .locals 6

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    return v2

    :cond_0
    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessWhiteList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessBlackList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    :cond_1
    return v2

    :cond_2
    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteIntents:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    return v2

    :cond_3
    iget-object v3, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_5

    aget-boolean v5, v3, v1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_4

    return v2

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    array-length v4, v3

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_7

    aget-boolean v5, v3, v1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_6

    return v2

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    iget-object v3, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    array-length v4, v3

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_9

    aget-boolean v5, v3, v1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_8

    return v2

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_9
    iget-object v3, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    array-length v4, v3

    move v1, v2

    :goto_3
    if-ge v1, v4, :cond_b

    aget-boolean v5, v3, v1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_a

    return v2

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_b
    const/4 v1, 0x1

    return v1
.end method

.method public isEquals(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPkgName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isIntentWhiteList(Landroid/content/Intent;)Z
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteIntents:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteIntents:Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public policyEnableWithModule(Ljava/lang/String;Z)V
    .locals 3

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getPolicyModuleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p2}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->setEnableState(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public policyEnableWithSetter(Ljava/lang/String;Z)V
    .locals 3

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getSetter()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p2}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->setEnableState(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public removeAllPoliciesByModule(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v0, 0x0

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getPolicyModuleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method public removeAllPoliciesBySetter(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v0, 0x0

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getSetter()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method public removePolicyById(I)V
    .locals 3

    if-gez p1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v0, 0x0

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v2

    if-ne v2, p1, :cond_1

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method public removePolicyByUser(I)V
    .locals 3

    if-gez p1, :cond_0

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    :cond_0
    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v0, 0x0

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getUserId()I

    move-result v2

    if-ne v2, p1, :cond_1

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method public setPackageStartTimeWithType(IJ)V
    .locals 2

    const/4 v0, -0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mStartTimeByType:[J

    aput-wide p2, v1, v0

    :cond_0
    return-void

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public updateCheckIntens(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteIntents:Ljava/util/List;

    monitor-enter v1

    if-eqz p2, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteIntents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    sget-boolean v0, Lcom/miui/whetstone/component/PackageComponentPolicy;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PackageComponentPolicy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Component Check Intents: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteIntents:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteIntents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public updateCheckList(Ljava/lang/String;IZZZ)V
    .locals 10

    const/4 v9, 0x1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    sget-boolean v6, Lcom/miui/whetstone/component/PackageComponentPolicy;->DEBUG:Z

    if-eqz v6, :cond_0

    const-string/jumbo v6, "PackageComponentPolicy"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Update CheckList: Pkg:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " Process:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " StartType:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " isAdd: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " isWhiteList:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " isCalled:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p5, :cond_8

    if-eqz v0, :cond_7

    if-eqz p4, :cond_3

    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessWhiteList:Ljava/util/List;

    :goto_1
    sget-object v7, Lcom/miui/whetstone/component/PackageComponentPolicy;->sMutex:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v2, 0x1

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;

    if-eqz v5, :cond_1

    iget-object v6, v5, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->processName:Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v5, p2, p3}, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->updateCheckList(IZ)V

    if-nez p3, :cond_1

    invoke-virtual {v5}, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->hasAllow()Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mProcessBlackList:Ljava/util/List;

    goto :goto_1

    :cond_4
    if-eqz p3, :cond_5

    if-eqz v2, :cond_5

    :try_start_1
    new-instance v4, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;

    invoke-direct {v4, p0, p1}, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;-><init>(Lcom/miui/whetstone/component/PackageComponentPolicy;Ljava/lang/String;)V

    const/4 v6, 0x1

    invoke-virtual {v4, p2, v6}, Lcom/miui/whetstone/component/PackageComponentPolicy$ProcessPermission;->updateCheckList(IZ)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    monitor-exit v7

    :cond_6
    :goto_3
    return-void

    :cond_7
    invoke-virtual {p0, p4, p2, p3, v9}, Lcom/miui/whetstone/component/PackageComponentPolicy;->updatePackageCheckList(ZIZZ)V

    sget-boolean v6, Lcom/miui/whetstone/component/PackageComponentPolicy;->DEBUG:Z

    if-eqz v6, :cond_6

    const-string/jumbo v6, "PackageComponentPolicy"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " white caller: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    invoke-static {v8}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " white called: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    invoke-static {v8}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " black caller: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    invoke-static {v8}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " black called: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    invoke-static {v8}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_8
    const/4 v6, 0x0

    invoke-virtual {p0, p4, p2, p3, v6}, Lcom/miui/whetstone/component/PackageComponentPolicy;->updatePackageCheckList(ZIZZ)V

    sget-boolean v6, Lcom/miui/whetstone/component/PackageComponentPolicy;->DEBUG:Z

    if-eqz v6, :cond_6

    const-string/jumbo v6, "PackageComponentPolicy"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " white caller: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    invoke-static {v8}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " white called: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    invoke-static {v8}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " black caller: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    invoke-static {v8}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " black called: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    invoke-static {v8}, Ljava/util/Arrays;->toString([Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method public updateCheckListAll(ZZ)V
    .locals 4

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    :goto_0
    sget-object v3, Lcom/miui/whetstone/component/PackageComponentPolicy;->sMutex:Ljava/lang/Object;

    monitor-enter v3

    const/4 v1, 0x0

    :goto_1
    :try_start_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    aput-boolean p1, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    goto :goto_0

    :cond_1
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public updatePackageCheckList(ZIZZ)V
    .locals 3

    const/4 v2, 0x0

    if-nez p4, :cond_5

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCaller:[Z

    :goto_0
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_0

    aput-boolean p3, v0, v2

    :cond_0
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    aput-boolean p3, v0, v1

    :cond_1
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    aput-boolean p3, v0, v1

    :cond_2
    and-int/lit8 v1, p2, 0x8

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    aput-boolean p3, v0, v1

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCaller:[Z

    goto :goto_0

    :cond_5
    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mWhiteListWithStartTypeAsCalled:[Z

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mBlackListWithStartTypeAsCalled:[Z

    goto :goto_0
.end method

.method public updatePolicy(Lcom/miui/whetstone/component/ComponentPolicyInfo;)I
    .locals 7

    if-nez p1, :cond_0

    const/4 v4, -0x1

    return v4

    :cond_0
    const/4 v1, 0x0

    iget-object v4, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v4

    invoke-virtual {v1}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v5

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    sget-boolean v4, Lcom/miui/whetstone/component/PackageComponentPolicy;->DEBUG:Z

    if-eqz v4, :cond_1

    const-string/jumbo v4, "PackageComponentPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Updated package Policy: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v2, 0x1

    :cond_2
    if-nez v2, :cond_3

    iget-object v4, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    sget-boolean v4, Lcom/miui/whetstone/component/PackageComponentPolicy;->DEBUG:Z

    if-eqz v4, :cond_3

    const-string/jumbo v4, "PackageComponentPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Added package Policy: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p1}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v4

    return v4

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public updatePolicyEnableState(Ljava/lang/String;II)V
    .locals 3

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->updatePolicyEnableStatus(Ljava/lang/String;II)V

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->isRemovedWhenPolicyDisbale()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getEnableStatus()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/miui/whetstone/component/PackageComponentPolicy;->mPolicyInfos:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method
