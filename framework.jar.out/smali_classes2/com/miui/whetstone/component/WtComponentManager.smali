.class public Lcom/miui/whetstone/component/WtComponentManager;
.super Ljava/lang/Object;
.source "WtComponentManager.java"


# static fields
.field private static final DEBUG:Z

.field private static final SERVICE_RESTART:Ljava/lang/String; = "Restart: AMS"

.field private static final TAG:Ljava/lang/String; = "WtComponentManager"

.field private static final sMutex:Ljava/lang/Object;

.field private static volatile sWtComponentManager:Lcom/miui/whetstone/component/WtComponentManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/miui/whetstone/component/ComponentPolicyInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mIdIndex:I

.field private mInputPackages:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/miui/whetstone/component/PackageComponentPolicy;",
            ">;"
        }
    .end annotation
.end field

.field private mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "persist.sys.whetstone.debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    sput-boolean v0, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/miui/whetstone/component/WtComponentManager;->sWtComponentManager:Lcom/miui/whetstone/component/WtComponentManager;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/miui/whetstone/component/WtComponentManager;->sMutex:Ljava/lang/Object;

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/whetstone/component/WtComponentManager;->mInputPackages:Ljava/util/concurrent/CopyOnWriteArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/whetstone/component/WtComponentManager;->mIdIndex:I

    iput-object p1, p0, Lcom/miui/whetstone/component/WtComponentManager;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/miui/whetstone/WhetstoneSleepModeController;->getInstance(Landroid/content/Context;)Lcom/miui/whetstone/WhetstoneSleepModeController;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/whetstone/component/WtComponentManager;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;

    return-void
.end method

.method private checkDefaultComponentPolicy(IIILandroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11

    const/4 v10, 0x1

    iget-object v3, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->isCommonPolicy()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_0

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-virtual {v2, v0, v1}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->isCommonPolicyIgnore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v3, p6

    move v4, p1

    move v5, p2

    move v6, p3

    move-object v7, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v2 .. v8}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->checkPolicyResultWithStartType(Ljava/lang/String;IIILandroid/content/Intent;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    :cond_1
    return v10
.end method

.method private checkPackagePolicy(IILandroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)Z
    .locals 10

    const/4 v9, 0x1

    const/4 v7, 0x0

    iget-object v2, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v0, p5}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isEquals(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v7, 0x1

    move/from16 v1, p6

    move v2, p1

    move-object v3, p4

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/miui/whetstone/component/PackageComponentPolicy;->checkPackagePolicyWithStartType(IILjava/lang/String;ILandroid/content/Intent;)Z

    move-result v9

    if-nez v9, :cond_0

    :cond_1
    if-nez v7, :cond_2

    new-instance v1, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-direct {v1, p5}, Lcom/miui/whetstone/component/PackageComponentPolicy;-><init>(Ljava/lang/String;)V

    move/from16 v2, p6

    move v3, p1

    move-object v4, p4

    move v5, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/miui/whetstone/component/PackageComponentPolicy;->checkPackagePolicyWithStartType(IILjava/lang/String;ILandroid/content/Intent;)Z

    move-result v9

    iget-object v2, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return v9
.end method

.method public static getInstance()Lcom/miui/whetstone/component/WtComponentManager;
    .locals 1

    sget-object v0, Lcom/miui/whetstone/component/WtComponentManager;->sWtComponentManager:Lcom/miui/whetstone/component/WtComponentManager;

    return-object v0
.end method

.method public static isDebug()Z
    .locals 1

    sget-boolean v0, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    return v0
.end method

.method private isInputMethod(Ljava/lang/String;)Z
    .locals 3

    iget-object v2, p0, Lcom/miui/whetstone/component/WtComponentManager;->mInputPackages:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    return v2

    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method public static makeInstance(Landroid/content/Context;)Lcom/miui/whetstone/component/WtComponentManager;
    .locals 2

    sget-object v0, Lcom/miui/whetstone/component/WtComponentManager;->sWtComponentManager:Lcom/miui/whetstone/component/WtComponentManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/miui/whetstone/component/WtComponentManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/miui/whetstone/component/WtComponentManager;->sWtComponentManager:Lcom/miui/whetstone/component/WtComponentManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/whetstone/component/WtComponentManager;

    invoke-direct {v0, p0}, Lcom/miui/whetstone/component/WtComponentManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/miui/whetstone/component/WtComponentManager;->sWtComponentManager:Lcom/miui/whetstone/component/WtComponentManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lcom/miui/whetstone/component/WtComponentManager;->sWtComponentManager:Lcom/miui/whetstone/component/WtComponentManager;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private updatePolicyStatus(Ljava/lang/String;IZI)V
    .locals 7

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v0, 0x0

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2, p4}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->updatePolicyEnableStatus(Ljava/lang/String;II)V

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->isRemovedWhenPolicyDisbale()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getEnableStatus()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    sget-boolean v4, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v4, :cond_1

    const-string/jumbo v4, "WtComponentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "remove policy pkg:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getPkg()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " userId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getUserId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " Id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/whetstone/component/PackageComponentPolicy;

    if-eqz v3, :cond_2

    sget-boolean v4, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v4, :cond_3

    const-string/jumbo v4, "WtComponentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "check policy status: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/miui/whetstone/component/PackageComponentPolicy;->getPkgName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {v3, p1}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isEquals(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {v3, p4, v4, v5}, Lcom/miui/whetstone/component/PackageComponentPolicy;->setPackageStartTimeWithType(IJ)V

    :cond_4
    invoke-virtual {v3, p1, p2, p4}, Lcom/miui/whetstone/component/PackageComponentPolicy;->updatePolicyEnableState(Ljava/lang/String;II)V

    goto :goto_1

    :cond_5
    return-void
.end method


# virtual methods
.method public checkPackagePolicyState(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;IILandroid/os/Bundle;)Z
    .locals 21

    const/4 v8, 0x0

    const/16 v17, 0x0

    invoke-static/range {p7 .. p7}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v14

    const-string/jumbo v5, "intent"

    move-object/from16 v0, p9

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/content/Intent;

    sget-boolean v5, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v5, :cond_0

    const-string/jumbo v6, "WtComponentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Process Start: pkg: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " userId: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " Process:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " Caller: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " CallerUserId:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " StartType: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " Flag: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " intent: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v9, :cond_3

    const-string/jumbo v5, "null"

    :goto_0
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/miui/whetstone/component/WtComponentManager;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-virtual {v5}, Lcom/miui/whetstone/WhetstoneSleepModeController;->isInSleep()Z

    move-result v5

    if-eqz v5, :cond_1

    and-int/lit8 v5, p3, 0x1

    if-eqz v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/miui/whetstone/component/WtComponentManager;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-virtual {v5}, Lcom/miui/whetstone/WhetstoneSleepModeController;->exitSleepModeByActivity()V

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_5

    :cond_2
    const-string/jumbo v5, "WtComponentManager"

    const-string/jumbo v6, "error start info, pass"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x1

    return v5

    :cond_3
    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/miui/whetstone/component/WtComponentManager;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-virtual {v5}, Lcom/miui/whetstone/WhetstoneSleepModeController;->isInCharging()Z

    move-result v5

    if-nez v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/miui/whetstone/component/WtComponentManager;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;

    invoke-virtual {v5}, Lcom/miui/whetstone/WhetstoneSleepModeController;->isProcessControlEnable()Z

    move-result v5

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/miui/whetstone/component/WtComponentManager;->mSleepModeController:Lcom/miui/whetstone/WhetstoneSleepModeController;

    move-object/from16 v0, p1

    move-object/from16 v1, p6

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Lcom/miui/whetstone/WhetstoneSleepModeController;->isForbidProcessStartBySleepMode(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v16

    if-eqz v16, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/miui/whetstone/component/WtComponentManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->getInstance(Landroid/content/Context;)Lcom/miui/whetstone/WhetstoneWakeUpManager;

    move-result-object v5

    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v5, v6, v0, v7}, Lcom/miui/whetstone/WhetstoneWakeUpManager;->checkIfAppBeAllowedStartForWakeUpControl(ILjava/lang/String;Landroid/app/PendingIntent;)I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    const/4 v5, 0x0

    return v5

    :cond_5
    if-eqz p2, :cond_2

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    if-ltz p4, :cond_2

    if-ltz v14, :cond_2

    if-ltz p8, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_6
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/miui/whetstone/component/PackageComponentPolicy;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isEquals(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, p6

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/miui/whetstone/component/PackageComponentPolicy;->inCheckList(Ljava/lang/String;IZZ)Z

    move-result v5

    if-eqz v5, :cond_8

    sget-boolean v5, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v5, :cond_7

    const-string/jumbo v5, "WtComponentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Process "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " white list start permission: allow"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/miui/whetstone/component/WtComponentManager;->updatePolicyStatus(Ljava/lang/String;IZI)V

    const/4 v5, 0x1

    return v5

    :cond_8
    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, p6

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/miui/whetstone/component/PackageComponentPolicy;->inCheckList(Ljava/lang/String;IZZ)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string/jumbo v5, "WtComponentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Process "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " black list start permission: deny "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/miui/whetstone/component/WtComponentManager;->updatePolicyStatus(Ljava/lang/String;IZI)V

    const/4 v5, 0x0

    return v5

    :cond_9
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isEquals(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v0, v5, v1, v6, v7}, Lcom/miui/whetstone/component/PackageComponentPolicy;->inCheckList(Ljava/lang/String;IZZ)Z

    move-result v5

    if-eqz v5, :cond_b

    sget-boolean v5, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v5, :cond_a

    const-string/jumbo v5, "WtComponentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Process "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " white list start permission: allow"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/miui/whetstone/component/WtComponentManager;->updatePolicyStatus(Ljava/lang/String;IZI)V

    const/4 v5, 0x1

    return v5

    :cond_b
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v0, v5, v1, v6, v7}, Lcom/miui/whetstone/component/PackageComponentPolicy;->inCheckList(Ljava/lang/String;IZZ)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string/jumbo v5, "WtComponentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Process "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " black list start permission: deny "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/miui/whetstone/component/WtComponentManager;->updatePolicyStatus(Ljava/lang/String;IZI)V

    const/4 v5, 0x0

    return v5

    :cond_c
    invoke-direct/range {p0 .. p1}, Lcom/miui/whetstone/component/WtComponentManager;->isInputMethod(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    const/high16 v8, 0x20000

    :cond_d
    const-string/jumbo v5, "Restart: AMS"

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    or-int/lit16 v8, v8, 0x2000

    :cond_e
    invoke-static/range {p8 .. p8}, Lcom/miui/whetstone/component/ComponentHelper;->isCallerHasForegroundActivities(I)Z

    move-result v5

    if-eqz v5, :cond_10

    and-int/lit16 v5, v8, 0x2000

    if-nez v5, :cond_f

    or-int/lit8 v8, v8, 0x4

    :cond_f
    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    move/from16 v0, p4

    if-ne v0, v14, :cond_10

    const v5, 0x14000

    or-int/2addr v8, v5

    const/16 v17, 0x1

    :cond_10
    if-nez v17, :cond_11

    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    move/from16 v0, p4

    if-ne v0, v14, :cond_11

    or-int/lit16 v8, v8, 0x4000

    const/16 v17, 0x1

    :cond_11
    move-object/from16 v0, p1

    move/from16 v1, p4

    invoke-static {v0, v1}, Lcom/miui/whetstone/component/ComponentHelper;->isSystemPackage(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_12

    or-int/lit16 v8, v8, 0x1000

    :cond_12
    invoke-static/range {p7 .. p7}, Lcom/miui/whetstone/component/ComponentHelper;->isCallerSystemUid(I)Z

    move-result v5

    if-eqz v5, :cond_13

    or-int/lit8 v8, v8, 0x2

    :cond_13
    move-object/from16 v0, p2

    invoke-static {v0, v14}, Lcom/miui/whetstone/component/ComponentHelper;->isSystemPackage(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_14

    or-int/lit8 v8, v8, 0x1

    :cond_14
    const/4 v5, 0x4

    move/from16 v0, p3

    if-ne v0, v5, :cond_15

    const v5, 0x8000

    or-int/2addr v8, v5

    :cond_15
    move-object/from16 v5, p0

    move/from16 v6, p4

    move/from16 v7, p3

    move-object/from16 v10, p6

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    invoke-direct/range {v5 .. v12}, Lcom/miui/whetstone/component/WtComponentManager;->checkDefaultComponentPolicy(IIILandroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_17

    move-object/from16 v6, p0

    move/from16 v7, p3

    move-object/from16 v10, p6

    move-object/from16 v11, p1

    move/from16 v12, p4

    move-object/from16 v13, p2

    invoke-direct/range {v6 .. v14}, Lcom/miui/whetstone/component/WtComponentManager;->checkPackagePolicy(IILandroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)Z

    move-result v20

    :goto_1
    sget-boolean v5, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v5, :cond_16

    const-string/jumbo v6, "WtComponentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Process "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " start permission: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v20, :cond_18

    const-string/jumbo v5, "allow"

    :goto_2
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_16
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, v20

    move/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/miui/whetstone/component/WtComponentManager;->updatePolicyStatus(Ljava/lang/String;IZI)V

    return v20

    :cond_17
    const/16 v20, 0x0

    goto :goto_1

    :cond_18
    const-string/jumbo v5, "deny"

    goto :goto_2
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    const-string/jumbo v4, " ===== Common Policy ===== "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Total: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    const-string/jumbo v4, ""

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string/jumbo v4, ""

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string/jumbo v4, " ===== Package Policy ===== "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Total: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v2, p1, p2, p3}, Lcom/miui/whetstone/component/PackageComponentPolicy;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public getPackageComponentPolicy(Ljava/lang/String;)Lcom/miui/whetstone/component/PackageComponentPolicy;
    .locals 3

    iget-object v2, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v0, p1}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isEquals(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    :cond_1
    const/4 v2, 0x0

    return-object v2
.end method

.method public getPolicy(I)Lcom/miui/whetstone/component/ComponentPolicyInfo;
    .locals 5

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v4

    if-ne v4, p1, :cond_0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v2, p1}, Lcom/miui/whetstone/component/PackageComponentPolicy;->getPolicy(I)Lcom/miui/whetstone/component/ComponentPolicyInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_3
    return-object v0
.end method

.method public policyEnableWithModule(Ljava/lang/String;Z)V
    .locals 5

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getPolicyModuleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0, p2}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->setEnableState(Z)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/component/PackageComponentPolicy;

    if-eqz v2, :cond_2

    invoke-virtual {v2, p1, p2}, Lcom/miui/whetstone/component/PackageComponentPolicy;->policyEnableWithSetter(Ljava/lang/String;Z)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public policyEnableWithSetter(Ljava/lang/String;Z)V
    .locals 5

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getSetter()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0, p2}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->setEnableState(Z)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/component/PackageComponentPolicy;

    if-eqz v2, :cond_2

    invoke-virtual {v2, p1, p2}, Lcom/miui/whetstone/component/PackageComponentPolicy;->policyEnableWithSetter(Ljava/lang/String;Z)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public removePackagePoliciesByPackageInfos(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string/jumbo v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    aget-object v6, v4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    aget-object v3, v4, v6

    iget-object v6, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/PackageComponentPolicy;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isEquals(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0, v5}, Lcom/miui/whetstone/component/PackageComponentPolicy;->removePolicyByUser(I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public removePackagesPolicies([I)V
    .locals 8

    const/4 v5, 0x0

    array-length v6, p1

    :goto_0
    if-ge v5, v6, :cond_3

    aget v0, p1, v5

    iget-object v7, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v7}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v1, 0x0

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v7

    if-ne v7, v0, :cond_0

    iget-object v7, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v7, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v7, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v3, v0}, Lcom/miui/whetstone/component/PackageComponentPolicy;->removePolicyById(I)V

    goto :goto_2

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public removePackagesPoliciesByModule(Ljava/lang/String;)V
    .locals 5

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getPolicyModuleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v2, p1}, Lcom/miui/whetstone/component/PackageComponentPolicy;->removeAllPoliciesBySetter(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public removePackagesPoliciesBySetter(Ljava/lang/String;)V
    .locals 5

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getSetter()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v2, p1}, Lcom/miui/whetstone/component/PackageComponentPolicy;->removeAllPoliciesBySetter(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public updateCheckIntents(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v1}, Lcom/miui/whetstone/component/PackageComponentPolicy;->getPkgName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v1, p2, p3}, Lcom/miui/whetstone/component/PackageComponentPolicy;->updateCheckIntens(Ljava/util/List;Z)V

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    new-instance v1, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-direct {v1, p1}, Lcom/miui/whetstone/component/PackageComponentPolicy;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Lcom/miui/whetstone/component/PackageComponentPolicy;->updateCheckIntens(Ljava/util/List;Z)V

    iget-object v3, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method public updateCheckList(Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface/range {p1 .. p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string/jumbo v12, "#"

    invoke-virtual {v7, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v9, v11, v12

    const/4 v12, 0x1

    aget-object v12, v11, v12

    const-string/jumbo v13, "null"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    const/4 v1, 0x0

    :goto_1
    const/4 v12, 0x2

    aget-object v12, v11, v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v12, 0x3

    aget-object v12, v11, v12

    invoke-static {v12}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    const/4 v12, 0x4

    aget-object v12, v11, v12

    invoke-static {v12}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    const/4 v12, 0x5

    aget-object v12, v11, v12

    invoke-static {v12}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x0

    sget-boolean v12, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v12, :cond_1

    const-string/jumbo v12, "WtComponentManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Update check list:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string/jumbo v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v12, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v12}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v0}, Lcom/miui/whetstone/component/PackageComponentPolicy;->getPkgName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v6, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/miui/whetstone/component/PackageComponentPolicy;->updateCheckList(Ljava/lang/String;IZZZ)V

    :cond_3
    if-nez v6, :cond_0

    new-instance v0, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-direct {v0, v9}, Lcom/miui/whetstone/component/PackageComponentPolicy;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v0 .. v5}, Lcom/miui/whetstone/component/PackageComponentPolicy;->updateCheckList(Ljava/lang/String;IZZZ)V

    iget-object v12, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v12, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    const/4 v12, 0x1

    aget-object v1, v11, v12

    goto/16 :goto_1

    :cond_5
    return-void
.end method

.method public updateInputMethod(Ljava/lang/String;Z)V
    .locals 1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/miui/whetstone/component/WtComponentManager;->mInputPackages:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/whetstone/component/WtComponentManager;->mInputPackages:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/miui/whetstone/component/WtComponentManager;->mInputPackages:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public updatePackagesPolicies(Ljava/util/List;)[I
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/miui/whetstone/component/ComponentPolicyInfo;",
            ">;)[I"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    new-array v0, v9, [I

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    sget-boolean v9, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v9, :cond_0

    const-string/jumbo v9, "WtComponentManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Receive Update Policy: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v9, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v9}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    iget-object v9, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v9}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v3, 0x0

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->isCommonPolicy()Z

    move-result v9

    if-eqz v9, :cond_4

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/whetstone/component/ComponentPolicyInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v9

    if-ltz v9, :cond_1

    invoke-virtual {v2}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v9

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v10

    if-ne v9, v10, :cond_1

    iget-object v9, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v10, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v10, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v10

    invoke-virtual {v9, v10, v7}, Ljava/util/concurrent/CopyOnWriteArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v9

    aput v9, v0, v1

    const/4 v3, 0x1

    sget-boolean v9, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v9, :cond_2

    const-string/jumbo v9, "WtComponentManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Updated Common Policy: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-nez v3, :cond_3

    sget-object v10, Lcom/miui/whetstone/component/WtComponentManager;->sMutex:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    iget v9, p0, Lcom/miui/whetstone/component/WtComponentManager;->mIdIndex:I

    add-int/lit8 v11, v9, 0x1

    iput v11, p0, Lcom/miui/whetstone/component/WtComponentManager;->mIdIndex:I

    aput v9, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v10

    aget v9, v0, v1

    invoke-virtual {v7, v9}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->setId(I)V

    iget-object v9, p0, Lcom/miui/whetstone/component/WtComponentManager;->mDefaultComponentPoliy:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v9, v7}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    sget-boolean v9, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v9, :cond_3

    const-string/jumbo v9, "WtComponentManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Added Common Policy: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :catchall_0
    move-exception v9

    monitor-exit v10

    throw v9

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/miui/whetstone/component/PackageComponentPolicy;

    if-eqz v6, :cond_4

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getPkg()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/miui/whetstone/component/PackageComponentPolicy;->isEquals(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v9

    if-gez v9, :cond_5

    sget-object v10, Lcom/miui/whetstone/component/WtComponentManager;->sMutex:Ljava/lang/Object;

    monitor-enter v10

    :try_start_1
    iget v9, p0, Lcom/miui/whetstone/component/WtComponentManager;->mIdIndex:I

    add-int/lit8 v11, v9, 0x1

    iput v11, p0, Lcom/miui/whetstone/component/WtComponentManager;->mIdIndex:I

    invoke-virtual {v7, v9}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->setId(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v10

    :cond_5
    invoke-virtual {v6, v7}, Lcom/miui/whetstone/component/PackageComponentPolicy;->updatePolicy(Lcom/miui/whetstone/component/ComponentPolicyInfo;)I

    sget-boolean v9, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v9, :cond_6

    const-string/jumbo v9, "WtComponentManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Updated Package Policy: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getId()I

    move-result v9

    aput v9, v0, v1

    const/4 v3, 0x1

    goto :goto_2

    :catchall_1
    move-exception v9

    monitor-exit v10

    throw v9

    :cond_7
    if-nez v3, :cond_3

    new-instance v6, Lcom/miui/whetstone/component/PackageComponentPolicy;

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->getPkg()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lcom/miui/whetstone/component/PackageComponentPolicy;-><init>(Ljava/lang/String;)V

    sget-object v10, Lcom/miui/whetstone/component/WtComponentManager;->sMutex:Ljava/lang/Object;

    monitor-enter v10

    :try_start_2
    iget v9, p0, Lcom/miui/whetstone/component/WtComponentManager;->mIdIndex:I

    add-int/lit8 v11, v9, 0x1

    iput v11, p0, Lcom/miui/whetstone/component/WtComponentManager;->mIdIndex:I

    invoke-virtual {v7, v9}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->setId(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    monitor-exit v10

    invoke-virtual {v6, v7}, Lcom/miui/whetstone/component/PackageComponentPolicy;->updatePolicy(Lcom/miui/whetstone/component/ComponentPolicyInfo;)I

    move-result v9

    aput v9, v0, v1

    aget v9, v0, v1

    if-ltz v9, :cond_3

    iget-object v9, p0, Lcom/miui/whetstone/component/WtComponentManager;->mPackagePolices:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v9, v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    sget-boolean v9, Lcom/miui/whetstone/component/WtComponentManager;->DEBUG:Z

    if-eqz v9, :cond_3

    const-string/jumbo v9, "WtComponentManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Added Package Policy: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/miui/whetstone/component/ComponentPolicyInfo;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catchall_2
    move-exception v9

    monitor-exit v10

    throw v9

    :cond_8
    return-object v0
.end method
