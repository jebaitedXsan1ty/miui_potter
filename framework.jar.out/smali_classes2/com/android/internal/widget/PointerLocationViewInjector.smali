.class public Lcom/android/internal/widget/PointerLocationViewInjector;
.super Ljava/lang/Object;
.source "PointerLocationViewInjector.java"


# static fields
.field private static final CUSTOM_TOUCH_STYLE_ENABLED:Ljava/lang/String; = "debug.customtouchstyle.enabled"

.field private static final CUSTOM_TOUCH_STYLE_OVAL_SIZE:Ljava/lang/String; = "debug.customtouchstyle.ovalsize"

.field private static final CUSTOM_TOUCH_STYLE_PAINT_COLOR:Ljava/lang/String; = "debug.customtouchstyle.paintcolor"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/internal/widget/PointerLocationViewInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/internal/widget/PointerLocationViewInjector;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static drawOval(Landroid/graphics/Canvas;FFFFFLandroid/graphics/Paint;)V
    .locals 14

    move/from16 v4, p3

    move/from16 v5, p4

    new-instance v2, Landroid/graphics/Paint;

    move-object/from16 v0, p6

    invoke-direct {v2, v0}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    invoke-static {}, Lcom/android/internal/widget/PointerLocationViewInjector;->isCustomTouchStyleEnabled()Z

    move-result v10

    if-eqz v10, :cond_2

    sget-object v10, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const-string/jumbo v10, "debug.customtouchstyle.paintcolor"

    const/4 v11, -0x1

    invoke-static {v10, v11}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v10, -0x1

    if-eq v3, v10, :cond_0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    :cond_0
    const-string/jumbo v10, "debug.customtouchstyle.ovalsize"

    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    xor-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_1

    :try_start_0
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    move v4, v6

    move v5, v6

    :cond_1
    :goto_0
    sget-object v10, Lcom/android/internal/widget/PointerLocationViewInjector;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "customColor="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, " customSize="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Landroid/graphics/Canvas;->save(I)I

    const/high16 v10, 0x43340000    # 180.0f

    mul-float v10, v10, p5

    float-to-double v10, v10

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v10, v12

    double-to-float v10, v10

    move/from16 v0, p2

    invoke-virtual {p0, v10, p1, v0}, Landroid/graphics/Canvas;->rotate(FFF)V

    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    const/high16 v10, 0x40000000    # 2.0f

    div-float v10, v5, v10

    sub-float v10, p1, v10

    iput v10, v8, Landroid/graphics/RectF;->left:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float v10, v5, v10

    add-float/2addr v10, p1

    iput v10, v8, Landroid/graphics/RectF;->right:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float v10, v4, v10

    sub-float v10, p2, v10

    iput v10, v8, Landroid/graphics/RectF;->top:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float v10, v4, v10

    add-float v10, v10, p2

    iput v10, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0, v8, v2}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p0}, Landroid/graphics/Canvas;->restore()V

    return-void

    :catch_0
    move-exception v7

    sget-object v10, Lcom/android/internal/widget/PointerLocationViewInjector;->TAG:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isCustomTouchStyleEnabled()Z
    .locals 2

    const-string/jumbo v0, "debug.customtouchstyle.enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
