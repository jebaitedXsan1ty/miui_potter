.class public Landroid/util/MiuiMultiWindowUtils;
.super Ljava/lang/Object;
.source "MiuiMultiWindowUtils.java"


# static fields
.field static final ENABLE_WHITELIST:Z = false

.field static final FREEFORM_TO_NAVIBAR:I = 0x96

.field static final FREEFORM_WINDOW_HEIGHT:I = 0x42e

.field static final FREEFORM_WINDOW_WIDTH:I = 0x3a2

.field static final TAG:Ljava/lang/String; = "FreeformWindow"

.field private static sGameList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sIMList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Landroid/util/MiuiMultiWindowUtils;->sGameList:Ljava/util/HashMap;

    sget-object v0, Landroid/util/MiuiMultiWindowUtils;->sGameList:Ljava/util/HashMap;

    const-string/jumbo v1, "com.happyelements.AndroidAnimal"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Landroid/util/MiuiMultiWindowUtils;->sGameList:Ljava/util/HashMap;

    const-string/jumbo v1, "com.tencent.tmgp.sgame"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Landroid/util/MiuiMultiWindowUtils;->sGameList:Ljava/util/HashMap;

    const-string/jumbo v1, "com.netease.onmyoji.mi"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Landroid/util/MiuiMultiWindowUtils;->sIMList:Ljava/util/HashMap;

    sget-object v0, Landroid/util/MiuiMultiWindowUtils;->sIMList:Ljava/util/HashMap;

    const-string/jumbo v1, "com.tencent.mm"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Landroid/util/MiuiMultiWindowUtils;->sIMList:Ljava/util/HashMap;

    const-string/jumbo v1, "com.tencent.mobileqq"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getActivityOptions(Landroid/content/Context;Ljava/lang/String;)Landroid/app/ActivityOptions;
    .locals 7

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "gb_boosting"

    const/4 v5, 0x0

    const/4 v6, -0x2

    invoke-static {v3, v4, v5, v6}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    xor-int/lit8 v1, v3, 0x1

    :goto_0
    const-string/jumbo v3, "FreeformWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "isLaunchMultiWindow:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " gameKey:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-static {p1}, Landroid/util/MiuiMultiWindowUtils;->supportFreeFromWindow(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/app/ActivityOptions;->setLaunchStackId(I)V

    :cond_0
    return-object v2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getFreeformRect(Landroid/content/Context;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 9

    const/4 v7, 0x0

    if-nez p0, :cond_0

    return-object v7

    :cond_0
    const-string/jumbo v7, "window"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/view/Display;->getRectSize(Landroid/graphics/Rect;)V

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    if-le v7, v8, :cond_1

    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v7

    add-int/lit16 v7, v7, -0x3a2

    div-int/lit8 v1, v7, 0x2

    :goto_1
    add-int/lit16 v3, v1, 0x3a2

    if-eqz v2, :cond_3

    const/16 v5, 0x96

    :goto_2
    add-int/lit16 v0, v5, 0x42e

    if-nez p1, :cond_4

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v1, v5, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v7

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/16 v1, 0x96

    goto :goto_1

    :cond_3
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/lit16 v7, v7, -0x42e

    div-int/lit8 v5, v7, 0x2

    goto :goto_2

    :cond_4
    if-eqz v2, :cond_6

    iget v7, p1, Landroid/graphics/Rect;->top:I

    if-ne v7, v5, :cond_5

    const/16 v5, 0x96

    :cond_5
    add-int/lit16 v0, v5, 0x42e

    :goto_3
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v1, v5, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v7

    :cond_6
    iget v7, p1, Landroid/graphics/Rect;->left:I

    if-ne v7, v1, :cond_7

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v7

    add-int/lit16 v1, v7, -0x3a2

    :cond_7
    add-int/lit16 v3, v1, 0x3a2

    goto :goto_3
.end method

.method public static getOrientation(Landroid/content/Context;)I
    .locals 4

    if-nez p0, :cond_0

    const/4 v2, -0x1

    return v2

    :cond_0
    const-string/jumbo v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getRectSize(Landroid/graphics/Rect;)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-le v2, v3, :cond_1

    const/4 v2, 0x2

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static supportFreeFromWindow(I)Z
    .locals 3

    const-string/jumbo v1, ""

    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    invoke-interface {v2, p0}, Landroid/content/pm/IPackageManager;->getNameForUid(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    sget-object v2, Landroid/util/MiuiMultiWindowUtils;->sIMList:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static supportFreeFromWindow(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Landroid/util/MiuiMultiWindowUtils;->sIMList:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
