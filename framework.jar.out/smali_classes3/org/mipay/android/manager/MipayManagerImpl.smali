.class public Lorg/mipay/android/manager/MipayManagerImpl;
.super Ljava/lang/Object;
.source "MipayManagerImpl.java"

# interfaces
.implements Lorg/mipay/android/manager/IMipayManager;


# static fields
.field private static CODE_CONTAINS:I

.field private static CODE_GEN_KEY_PAIR:I

.field private static CODE_GET_FP_IDS:I

.field private static CODE_RM_ALL_KEY:I

.field private static CODE_SIGN:I

.field private static CODE_SIGN_INIT:I

.field private static CODE_SIGN_UPDATE:I

.field private static DEBUG:Z

.field private static volatile INSTANCE:Lorg/mipay/android/manager/MipayManagerImpl;

.field private static INTERFACE_DESCRIPTOR:Ljava/lang/String;

.field private static MIPAY_TYPE_FINGER:I

.field private static MIPAY_TYPE_IRIS:I

.field private static MIPAY_VERISON_1:I

.field private static SERVICE_NAME:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mService:Landroid/os/IHwBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const-string/jumbo v0, "MipayManagerImpl"

    sput-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    sput-boolean v1, Lorg/mipay/android/manager/MipayManagerImpl;->DEBUG:Z

    const/4 v0, 0x0

    sput-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->INSTANCE:Lorg/mipay/android/manager/MipayManagerImpl;

    sput v1, Lorg/mipay/android/manager/MipayManagerImpl;->MIPAY_VERISON_1:I

    sput v1, Lorg/mipay/android/manager/MipayManagerImpl;->MIPAY_TYPE_FINGER:I

    sput v2, Lorg/mipay/android/manager/MipayManagerImpl;->MIPAY_TYPE_IRIS:I

    sput v1, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_CONTAINS:I

    sput v2, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_GEN_KEY_PAIR:I

    const/4 v0, 0x3

    sput v0, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_SIGN_INIT:I

    const/4 v0, 0x4

    sput v0, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_SIGN_UPDATE:I

    const/4 v0, 0x5

    sput v0, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_SIGN:I

    const/4 v0, 0x6

    sput v0, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_RM_ALL_KEY:I

    const/4 v0, 0x7

    sput v0, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_GET_FP_IDS:I

    const-string/jumbo v0, "vendor.xiaomi.hardware.tidaservice@1.0::ITidaService"

    sput-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->SERVICE_NAME:Ljava/lang/String;

    const-string/jumbo v0, "vendor.xiaomi.hardware.tidaservice@1.0::ITidaService"

    sput-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lorg/mipay/android/manager/IMipayManager;
    .locals 2

    sget-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->INSTANCE:Lorg/mipay/android/manager/MipayManagerImpl;

    if-nez v0, :cond_1

    const-class v1, Lorg/mipay/android/manager/MipayManagerImpl;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->INSTANCE:Lorg/mipay/android/manager/MipayManagerImpl;

    if-nez v0, :cond_0

    new-instance v0, Lorg/mipay/android/manager/MipayManagerImpl;

    invoke-direct {v0}, Lorg/mipay/android/manager/MipayManagerImpl;-><init>()V

    sput-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->INSTANCE:Lorg/mipay/android/manager/MipayManagerImpl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->INSTANCE:Lorg/mipay/android/manager/MipayManagerImpl;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private initService()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    if-nez v0, :cond_0

    sget-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->SERVICE_NAME:Ljava/lang/String;

    const-string/jumbo v1, "default"

    invoke-static {v0, v1}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v0

    iput-object v0, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    :cond_0
    return-void
.end method

.method private signUpdate(Ljava/lang/String;)I
    .locals 7

    const/4 v3, -0x1

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    :try_start_0
    invoke-direct {p0}, Lorg/mipay/android/manager/MipayManagerImpl;->initService()V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    if-eqz v4, :cond_0

    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    sget v5, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_SIGN_UPDATE:I

    const/4 v6, 0x0

    invoke-interface {v4, v5, v2, v1, v6}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    invoke-virtual {v2}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    :cond_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    :goto_0
    sget-boolean v4, Lorg/mipay/android/manager/MipayManagerImpl;->DEBUG:Z

    if-eqz v4, :cond_1

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "signUpdate, res:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v3

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "transact fail. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    throw v4
.end method


# virtual methods
.method public contains(Ljava/lang/String;)Z
    .locals 7

    const/4 v3, 0x0

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    :try_start_0
    invoke-direct {p0}, Lorg/mipay/android/manager/MipayManagerImpl;->initService()V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    if-eqz v4, :cond_0

    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    sget v5, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_CONTAINS:I

    const/4 v6, 0x0

    invoke-interface {v4, v5, v2, v1, v6}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    invoke-virtual {v2}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    :cond_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    :goto_0
    sget-boolean v4, Lorg/mipay/android/manager/MipayManagerImpl;->DEBUG:Z

    if-eqz v4, :cond_1

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "contains, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " res:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v3

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "transact fail. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    throw v4
.end method

.method public generateKeyPair(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    const/4 v3, -0x1

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    :try_start_0
    invoke-direct {p0}, Lorg/mipay/android/manager/MipayManagerImpl;->initService()V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    if-eqz v4, :cond_0

    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    sget v5, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_GEN_KEY_PAIR:I

    const/4 v6, 0x0

    invoke-interface {v4, v5, v2, v1, v6}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    invoke-virtual {v2}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    :cond_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    :goto_0
    sget-boolean v4, Lorg/mipay/android/manager/MipayManagerImpl;->DEBUG:Z

    if-eqz v4, :cond_1

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "generateKeyPair, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " res:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v3

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "transact fail. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    throw v4
.end method

.method public getFpIds()Ljava/lang/String;
    .locals 7

    const-string/jumbo v1, ""

    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    :try_start_0
    invoke-direct {p0}, Lorg/mipay/android/manager/MipayManagerImpl;->initService()V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    if-eqz v4, :cond_0

    new-instance v3, Landroid/os/HwParcel;

    invoke-direct {v3}, Landroid/os/HwParcel;-><init>()V

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    sget v5, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_GET_FP_IDS:I

    const/4 v6, 0x0

    invoke-interface {v4, v5, v3, v2, v6}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    invoke-virtual {v2}, Landroid/os/HwParcel;->verifySuccess()V

    invoke-virtual {v3}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    invoke-virtual {v2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :cond_0
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    :goto_0
    sget-boolean v4, Lorg/mipay/android/manager/MipayManagerImpl;->DEBUG:Z

    if-eqz v4, :cond_1

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "getFpIds, fpIds:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object v1

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "transact fail. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    throw v4
.end method

.method public getSupportBIOTypes(Landroid/content/Context;)I
    .locals 3

    sget-boolean v0, Lorg/mipay/android/manager/MipayManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getSupportBIOTypes :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lorg/mipay/android/manager/MipayManagerImpl;->MIPAY_TYPE_FINGER:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget v0, Lorg/mipay/android/manager/MipayManagerImpl;->MIPAY_TYPE_FINGER:I

    return v0
.end method

.method public getVersion()I
    .locals 3

    sget-boolean v0, Lorg/mipay/android/manager/MipayManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getVersion :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lorg/mipay/android/manager/MipayManagerImpl;->MIPAY_VERISON_1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget v0, Lorg/mipay/android/manager/MipayManagerImpl;->MIPAY_VERISON_1:I

    return v0
.end method

.method public removeAllKey()I
    .locals 7

    const/4 v3, -0x1

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    :try_start_0
    invoke-direct {p0}, Lorg/mipay/android/manager/MipayManagerImpl;->initService()V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    if-eqz v4, :cond_0

    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    sget v5, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_RM_ALL_KEY:I

    const/4 v6, 0x0

    invoke-interface {v4, v5, v2, v1, v6}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    invoke-virtual {v2}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    :cond_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    :goto_0
    sget-boolean v4, Lorg/mipay/android/manager/MipayManagerImpl;->DEBUG:Z

    if-eqz v4, :cond_1

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "removeAllKey, res:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v3

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "transact fail. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    throw v4
.end method

.method public sign()[B
    .locals 11

    const/4 v10, 0x0

    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    :try_start_0
    invoke-direct {p0}, Lorg/mipay/android/manager/MipayManagerImpl;->initService()V

    iget-object v7, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    if-eqz v7, :cond_1

    new-instance v3, Landroid/os/HwParcel;

    invoke-direct {v3}, Landroid/os/HwParcel;-><init>()V

    sget-object v7, Lorg/mipay/android/manager/MipayManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {v3, v7}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    sget v8, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_SIGN:I

    const/4 v9, 0x0

    invoke-interface {v7, v8, v3, v2, v9}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    invoke-virtual {v2}, Landroid/os/HwParcel;->verifySuccess()V

    invoke-virtual {v3}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    invoke-virtual {v2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [B

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Byte;

    invoke-virtual {v7}, Ljava/lang/Byte;->byteValue()B

    move-result v7

    aput-byte v7, v0, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    return-object v0

    :cond_1
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    :goto_1
    sget-object v7, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "sign fail, return null"

    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v10

    :catch_0
    move-exception v1

    :try_start_1
    sget-object v7, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "transact fail. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    goto :goto_1

    :catchall_0
    move-exception v7

    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    throw v7
.end method

.method public signInit(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    const/4 v3, -0x1

    new-instance v1, Landroid/os/HwParcel;

    invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V

    :try_start_0
    invoke-direct {p0}, Lorg/mipay/android/manager/MipayManagerImpl;->initService()V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    if-eqz v4, :cond_0

    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/mipay/android/manager/MipayManagerImpl;->mService:Landroid/os/IHwBinder;

    sget v5, Lorg/mipay/android/manager/MipayManagerImpl;->CODE_SIGN_INIT:I

    const/4 v6, 0x0

    invoke-interface {v4, v5, v2, v1, v6}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V

    invoke-virtual {v2}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    invoke-virtual {v1}, Landroid/os/HwParcel;->readInt32()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    :cond_0
    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    :goto_0
    sget-boolean v4, Lorg/mipay/android/manager/MipayManagerImpl;->DEBUG:Z

    if-eqz v4, :cond_1

    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "signInit, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " res:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v3

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v4, Lorg/mipay/android/manager/MipayManagerImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "transact fail. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Landroid/os/HwParcel;->release()V

    throw v4
.end method

.method public signUpdate([BII)I
    .locals 2

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([BII)V

    invoke-direct {p0, v0}, Lorg/mipay/android/manager/MipayManagerImpl;->signUpdate(Ljava/lang/String;)I

    move-result v1

    return v1
.end method
