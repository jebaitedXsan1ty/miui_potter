.class public Lorg/ifaa/android/manager/IFAAManagerImpl;
.super Lorg/ifaa/android/manager/IFAAManagerV2;
.source "IFAAManagerImpl.java"


# static fields
.field private static CODE_PROCESS_CMD:I

.field private static DEBUG:Z

.field private static IFAA_TYPE_FINGER:I

.field private static IFAA_TYPE_IRIS:I

.field private static volatile INSTANCE:Lorg/ifaa/android/manager/IFAAManagerImpl;

.field private static INTERFACE_DESCRIPTOR:Ljava/lang/String;

.field private static SERVICE_NAME:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;

.field private static mFingerActName:Ljava/lang/String;

.field private static mFingerPackName:Ljava/lang/String;


# instance fields
.field private mDevModel:Ljava/lang/String;

.field private mService:Landroid/os/IHwBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    const-string/jumbo v0, "IfaaManagerImpl"

    sput-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->TAG:Ljava/lang/String;

    sput-boolean v1, Lorg/ifaa/android/manager/IFAAManagerImpl;->DEBUG:Z

    const/4 v0, 0x0

    sput-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->INSTANCE:Lorg/ifaa/android/manager/IFAAManagerImpl;

    sput v1, Lorg/ifaa/android/manager/IFAAManagerImpl;->IFAA_TYPE_FINGER:I

    const/4 v0, 0x2

    sput v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->IFAA_TYPE_IRIS:I

    sput v1, Lorg/ifaa/android/manager/IFAAManagerImpl;->CODE_PROCESS_CMD:I

    const-string/jumbo v0, "com.android.settings"

    sput-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mFingerPackName:Ljava/lang/String;

    const-string/jumbo v0, "com.android.settings.NewFingerprintActivity"

    sput-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mFingerActName:Ljava/lang/String;

    const-string/jumbo v0, "vendor.xiaomi.hardware.mlipay@1.0::IMlipayService"

    sput-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->SERVICE_NAME:Ljava/lang/String;

    const-string/jumbo v0, "vendor.xiaomi.hardware.mlipay@1.0::IMlipayService"

    sput-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/ifaa/android/manager/IFAAManagerV2;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mDevModel:Ljava/lang/String;

    return-void
.end method

.method public static getInstance()Lorg/ifaa/android/manager/IFAAManagerV2;
    .locals 2

    sget-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->INSTANCE:Lorg/ifaa/android/manager/IFAAManagerImpl;

    if-nez v0, :cond_1

    const-class v1, Lorg/ifaa/android/manager/IFAAManagerImpl;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->INSTANCE:Lorg/ifaa/android/manager/IFAAManagerImpl;

    if-nez v0, :cond_0

    new-instance v0, Lorg/ifaa/android/manager/IFAAManagerImpl;

    invoke-direct {v0}, Lorg/ifaa/android/manager/IFAAManagerImpl;-><init>()V

    sput-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->INSTANCE:Lorg/ifaa/android/manager/IFAAManagerImpl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    :cond_1
    sget-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->INSTANCE:Lorg/ifaa/android/manager/IFAAManagerImpl;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getDeviceModel()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mDevModel:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string/jumbo v1, "finger_alipay_ifaa_model"

    invoke-static {v1}, Lmiui/util/FeatureParser;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mDevModel:Ljava/lang/String;

    :cond_1
    :goto_0
    sget-boolean v1, Lorg/ifaa/android/manager/IFAAManagerImpl;->DEBUG:Z

    if-eqz v1, :cond_2

    sget-object v1, Lorg/ifaa/android/manager/IFAAManagerImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getDeviceModel devcieModel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mDevModel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v1, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mDevModel:Ljava/lang/String;

    return-object v1

    :cond_3
    iput-object v0, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mDevModel:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSupportBIOTypes(Landroid/content/Context;)I
    .locals 5

    const-string/jumbo v2, "persist.sys.ifaa"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sget v2, Lorg/ifaa/android/manager/IFAAManagerImpl;->IFAA_TYPE_FINGER:I

    sget v3, Lorg/ifaa/android/manager/IFAAManagerImpl;->IFAA_TYPE_IRIS:I

    or-int/2addr v2, v3

    and-int v1, v0, v2

    sget-boolean v2, Lorg/ifaa/android/manager/IFAAManagerImpl;->DEBUG:Z

    if-eqz v2, :cond_0

    sget-object v2, Lorg/ifaa/android/manager/IFAAManagerImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getSupportBIOTypes return:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " res:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return v1
.end method

.method public getVersion()I
    .locals 3

    sget-boolean v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getVersion sdk:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " IfaaVer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lorg/ifaa/android/manager/IFAAManagerImpl;->mIfaaVer:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget v0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mIfaaVer:I

    return v0
.end method

.method public processCmdV2(Landroid/content/Context;[B)[B
    .locals 12

    const/4 v11, 0x0

    new-instance v2, Landroid/os/HwParcel;

    invoke-direct {v2}, Landroid/os/HwParcel;-><init>()V

    :try_start_0
    iget-object v8, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mService:Landroid/os/IHwBinder;

    if-nez v8, :cond_0

    sget-object v8, Lorg/ifaa/android/manager/IFAAManagerImpl;->SERVICE_NAME:Ljava/lang/String;

    const-string/jumbo v9, "default"

    invoke-static {v8, v9}, Landroid/os/HwBinder;->getService(Ljava/lang/String;Ljava/lang/String;)Landroid/os/IHwBinder;

    move-result-object v8

    iput-object v8, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mService:Landroid/os/IHwBinder;

    :cond_0
    iget-object v8, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mService:Landroid/os/IHwBinder;

    if-eqz v8, :cond_2

    new-instance v3, Landroid/os/HwParcel;

    invoke-direct {v3}, Landroid/os/HwParcel;-><init>()V

    sget-object v8, Lorg/ifaa/android/manager/IFAAManagerImpl;->INTERFACE_DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-static {p2}, Landroid/os/HwBlob;->wrapArray([B)[Ljava/lang/Byte;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v6}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v3, v8}, Landroid/os/HwParcel;->writeInt32(I)V

    iget-object v8, p0, Lorg/ifaa/android/manager/IFAAManagerImpl;->mService:Landroid/os/IHwBinder;

    sget v9, Lorg/ifaa/android/manager/IFAAManagerImpl;->CODE_PROCESS_CMD:I

    const/4 v10, 0x0

    invoke-interface {v8, v9, v3, v2, v10}, Landroid/os/IHwBinder;->transact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V

    invoke-virtual {v2}, Landroid/os/HwParcel;->verifySuccess()V

    invoke-virtual {v3}, Landroid/os/HwParcel;->releaseTemporaryStorage()V

    invoke-virtual {v2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [B

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Byte;

    invoke-virtual {v8}, Ljava/lang/Byte;->byteValue()B

    move-result v8

    aput-byte v8, v0, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    return-object v0

    :cond_2
    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    :goto_1
    sget-object v8, Lorg/ifaa/android/manager/IFAAManagerImpl;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "processCmdV2, return null"

    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v11

    :catch_0
    move-exception v1

    :try_start_1
    sget-object v8, Lorg/ifaa/android/manager/IFAAManagerImpl;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "transact failed. "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    goto :goto_1

    :catchall_0
    move-exception v8

    invoke-virtual {v2}, Landroid/os/HwParcel;->release()V

    throw v8
.end method

.method public startBIOManager(Landroid/content/Context;I)I
    .locals 5

    const/4 v1, -0x1

    sget v2, Lorg/ifaa/android/manager/IFAAManagerImpl;->IFAA_TYPE_FINGER:I

    if-ne v2, p2, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget-object v2, Lorg/ifaa/android/manager/IFAAManagerImpl;->mFingerPackName:Ljava/lang/String;

    sget-object v3, Lorg/ifaa/android/manager/IFAAManagerImpl;->mFingerActName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x0

    :cond_0
    sget-boolean v2, Lorg/ifaa/android/manager/IFAAManagerImpl;->DEBUG:Z

    if-eqz v2, :cond_1

    sget-object v2, Lorg/ifaa/android/manager/IFAAManagerImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startBIOManager authType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " res:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return v1
.end method
