.class public Lmiui/securityspace/SecuritySpaceEcryptManager;
.super Ljava/lang/Object;
.source "SecuritySpaceEcryptManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SecuritySpaceEcryptManager"

.field private static mActivityManager:Landroid/app/IActivityManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getService()Landroid/app/IActivityManager;
    .locals 1

    sget-object v0, Lmiui/securityspace/SecuritySpaceEcryptManager;->mActivityManager:Landroid/app/IActivityManager;

    if-eqz v0, :cond_0

    sget-object v0, Lmiui/securityspace/SecuritySpaceEcryptManager;->mActivityManager:Landroid/app/IActivityManager;

    return-object v0

    :cond_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    sput-object v0, Lmiui/securityspace/SecuritySpaceEcryptManager;->mActivityManager:Landroid/app/IActivityManager;

    sget-object v0, Lmiui/securityspace/SecuritySpaceEcryptManager;->mActivityManager:Landroid/app/IActivityManager;

    return-object v0
.end method

.method public static needAirlockUser(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static spaceEcryptfsMount(IILjava/lang/String;)I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public static spaceEcryptfsUnmount(I)V
    .locals 0

    return-void
.end method

.method public static spaceEcryptfsUnmountRemovableNonOwner(I)V
    .locals 0

    return-void
.end method

.method public static spaceEcryptfsUpdate(IILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public static spaceNeedsEcryptfsMount(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static spaceSwitchUser(I)Z
    .locals 4

    :try_start_0
    invoke-static {}, Lmiui/securityspace/SecuritySpaceEcryptManager;->getService()Landroid/app/IActivityManager;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/app/IActivityManager;->switchUser(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const-string/jumbo v1, "SecuritySpaceEcryptManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "switch user failed : userId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    return v1
.end method
