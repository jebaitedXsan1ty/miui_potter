.class public Lmiui/maml/util/JSONPath;
.super Ljava/lang/Object;
.source "JSONPath.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "JSONPath"


# instance fields
.field private mRoot:Lorg/json/JSONObject;

.field private mRootArray:Lorg/json/JSONArray;


# direct methods
.method public constructor <init>(Lorg/json/JSONArray;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/maml/util/JSONPath;->mRootArray:Lorg/json/JSONArray;

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/maml/util/JSONPath;->mRoot:Lorg/json/JSONObject;

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 14

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    const/4 v12, 0x0

    return-object v12

    :cond_0
    const-string/jumbo v12, "/"

    invoke-virtual {p1, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    iget-object v12, p0, Lmiui/maml/util/JSONPath;->mRoot:Lorg/json/JSONObject;

    if-eqz v12, :cond_1

    iget-object v7, p0, Lmiui/maml/util/JSONPath;->mRoot:Lorg/json/JSONObject;

    :goto_0
    if-nez v7, :cond_2

    const/4 v12, 0x0

    return-object v12

    :cond_1
    iget-object v7, p0, Lmiui/maml/util/JSONPath;->mRootArray:Lorg/json/JSONArray;

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_1
    :try_start_0
    array-length v12, v10

    if-ge v4, v12, :cond_7

    aget-object v9, v10, v4

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_4
    const/4 v6, -0x1

    const-string/jumbo v12, "["

    invoke-virtual {v9, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v12, -0x1

    if-eq v5, v12, :cond_5

    add-int/lit8 v12, v5, 0x1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v9, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/4 v12, 0x0

    invoke-virtual {v9, v12, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    :cond_5
    instance-of v12, v7, Lorg/json/JSONObject;

    if-eqz v12, :cond_6

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    xor-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_6

    move-object v0, v7

    check-cast v0, Lorg/json/JSONObject;

    move-object v11, v0

    invoke-virtual {v11, v9}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    :cond_6
    instance-of v12, v7, Lorg/json/JSONArray;

    if-eqz v12, :cond_9

    move-object v0, v7

    check-cast v0, Lorg/json/JSONArray;

    move-object v1, v0

    const/4 v12, -0x1

    if-ne v6, v12, :cond_8

    :cond_7
    return-object v7

    :cond_8
    invoke-virtual {v1, v6}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    :cond_9
    if-eqz v7, :cond_a

    sget-object v12, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v7, v12, :cond_3

    :cond_a
    const/4 v12, 0x0

    return-object v12

    :catch_0
    move-exception v2

    const-string/jumbo v12, "JSONPath"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    const/4 v12, 0x0

    return-object v12

    :catch_1
    move-exception v3

    const-string/jumbo v12, "JSONPath"

    invoke-virtual {v3}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
