.class public Lmiui/process/MiuiApplicationThread;
.super Lmiui/process/IMiuiApplicationThread$Stub;
.source "MiuiApplicationThread.java"


# instance fields
.field private mContentPort:Lmiui/util/LongScreenshotUtils$ContentPort;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/process/IMiuiApplicationThread$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public longScreenshot(I)Z
    .locals 1

    iget-object v0, p0, Lmiui/process/MiuiApplicationThread;->mContentPort:Lmiui/util/LongScreenshotUtils$ContentPort;

    if-nez v0, :cond_0

    new-instance v0, Lmiui/util/LongScreenshotUtils$ContentPort;

    invoke-direct {v0}, Lmiui/util/LongScreenshotUtils$ContentPort;-><init>()V

    iput-object v0, p0, Lmiui/process/MiuiApplicationThread;->mContentPort:Lmiui/util/LongScreenshotUtils$ContentPort;

    :cond_0
    iget-object v0, p0, Lmiui/process/MiuiApplicationThread;->mContentPort:Lmiui/util/LongScreenshotUtils$ContentPort;

    invoke-virtual {v0, p1}, Lmiui/util/LongScreenshotUtils$ContentPort;->longScreenshot(I)Z

    move-result v0

    return v0
.end method
