.class public Lmiui/telephony/TelephonyUtils;
.super Ljava/lang/Object;
.source "TelephonyUtils.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "TelephonyUtils"

.field private static final sNonRoamingMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40401"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40402"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40403"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40404"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40405"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40407"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40409"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40410"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40411"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40412"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40413"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40414"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40415"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40416"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40417"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40418"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40419"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40420"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40421"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40422"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40424"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40425"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40427"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40428"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40429"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40430"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40431"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40434"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40436"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40437"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40438"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40440"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40441"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40442"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40443"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40444"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40445"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40446"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40448"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40449"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40450"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40451"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40452"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40453"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40454"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40455"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40456"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40457"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40458"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40459"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40460"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40462"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40464"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40466"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40467"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40468"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40469"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40470"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40471"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40472"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40473"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40474"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40475"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40476"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40477"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40478"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40479"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40480"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40481"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40482"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40483"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40484"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40485"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40486"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40487"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40488"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40489"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40490"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40491"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40492"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40493"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40494"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40495"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40496"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40497"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40498"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40501"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40503"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40504"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40505"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40506"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40507"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40508"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40509"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40510"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40511"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40512"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40513"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40514"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40515"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40517"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40518"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40519"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40520"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40521"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40522"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40523"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40525"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40526"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40527"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40528"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40529"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40530"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40531"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40532"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40533"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40534"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40535"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40536"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40537"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40538"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40539"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40541"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40542"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40543"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40544"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40545"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40546"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40547"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40551"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40552"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40553"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40554"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40555"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40556"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40566"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40570"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405750"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405751"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405752"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405753"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405754"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405755"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405756"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405799"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405800"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405801"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405802"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405803"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405804"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405805"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405806"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405807"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405808"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405809"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405810"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405811"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405812"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405819"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405818"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405820"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405821"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405822"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405824"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405827"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405834"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405844"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405845"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405846"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405847"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405848"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405849"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405850"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405851"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405852"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405853"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405854"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405855"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405856"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405857"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405858"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405859"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405860"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405861"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405862"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405863"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405864"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405865"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405866"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405867"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405868"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405869"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405870"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405871"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405872"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405873"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405874"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405875"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405880"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405881"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405908"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405909"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405910"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405911"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405912"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405913"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405914"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405917"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405927"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "405929"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40475"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40451"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40458"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40481"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40474"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40438"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40457"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40480"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40473"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40434"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40466"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40455"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40472"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40477"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40464"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40454"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40471"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40476"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40462"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40453"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiui/telephony/TelephonyUtils;->sNonRoamingMap:Ljava/util/HashMap;

    const-string/jumbo v1, "40459"

    const-string/jumbo v2, "India"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isOperatorConsideredNonRoaming(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return v3

    :cond_1
    const-string/jumbo v0, "TelephonyUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "isOperatorConsideredNonRoaming for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "404"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "405"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const-string/jumbo v0, "404"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "405"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    return v0

    :cond_4
    return v3
.end method

.method public static pii(Ljava/lang/CharSequence;II)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_0

    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    if-lt v1, p1, :cond_1

    sub-int v3, v2, p2

    if-ge v1, v3, :cond_1

    const/16 v3, 0x78

    :goto_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    goto :goto_0

    :cond_1
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_3
    const-string/jumbo v3, ""

    return-object v3
.end method

.method public static pii(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v4, 0x6

    const/4 v3, 0x2

    const/4 v2, 0x0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    const-string/jumbo v1, ""

    return-object v1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :cond_1
    const/16 v1, 0xf

    if-lt v0, v1, :cond_2

    invoke-static {p0, v4, v3}, Lmiui/telephony/TelephonyUtils;->pii(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_2
    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    invoke-static {p0, v3, v3}, Lmiui/telephony/TelephonyUtils;->pii(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_3
    if-lt v0, v4, :cond_4

    invoke-static {p0, v2, v3}, Lmiui/telephony/TelephonyUtils;->pii(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_4
    if-lt v0, v3, :cond_5

    const/4 v1, 0x1

    invoke-static {p0, v2, v1}, Lmiui/telephony/TelephonyUtils;->pii(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_5
    invoke-static {p0, v2, v2}, Lmiui/telephony/TelephonyUtils;->pii(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static putDialConferenceExtra(Landroid/content/Intent;Z)V
    .locals 2

    if-eqz p0, :cond_0

    const-string/jumbo v0, "org.codeaurora.extra.DIAL_CONFERENCE_URI"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "TelephonyUtils"

    const-string/jumbo v1, "putDialConferenceExtra intent==null"

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static putVideoStateExtra(Landroid/content/Intent;I)V
    .locals 2

    if-eqz p0, :cond_0

    const-string/jumbo v0, "android.telecom.extra.START_CALL_WITH_VIDEO_STATE"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "TelephonyUtils"

    const-string/jumbo v1, "putVideoStateExtra intent==null"

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
