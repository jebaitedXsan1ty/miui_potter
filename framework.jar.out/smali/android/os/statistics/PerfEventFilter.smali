.class public Landroid/os/statistics/PerfEventFilter;
.super Ljava/lang/Object;
.source "PerfEventFilter.java"


# static fields
.field private static final DEFAULT_CAPACITY:I = 0x40

.field private static final DEFAULT_SUSPECTED_CAPACITY:I = 0x80


# instance fields
.field private final effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/statistics/FastPerfEventList",
            "<",
            "Landroid/os/statistics/MicroscopicEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final eventsWaitingBlockingPeer:Landroid/os/statistics/FastPerfEventList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/statistics/FastPerfEventList",
            "<",
            "Landroid/os/statistics/MicroscopicEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final isAppSideFilter:Z

.field private lastBatchBeginTimeMillis:J

.field private nativeBacktraceMapUpdated:Z

.field private final suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/statistics/FastPerfEventList",
            "<",
            "Landroid/os/statistics/MicroscopicEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final tempCompletedMacroEvents:Landroid/os/statistics/FastPerfEventList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/statistics/FastPerfEventList",
            "<",
            "Landroid/os/statistics/MacroscopicEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final tempCompletedMicroEvents:Landroid/os/statistics/FastPerfEventList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/statistics/FastPerfEventList",
            "<",
            "Landroid/os/statistics/MicroscopicEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Z)V
    .locals 4

    const/16 v3, 0x40

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/statistics/FastPerfEventList;

    const-class v1, Landroid/os/statistics/MicroscopicEvent;

    invoke-direct {v0, v1, v3}, Landroid/os/statistics/FastPerfEventList;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    new-instance v0, Landroid/os/statistics/FastPerfEventList;

    const-class v1, Landroid/os/statistics/MicroscopicEvent;

    invoke-direct {v0, v1, v3}, Landroid/os/statistics/FastPerfEventList;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Landroid/os/statistics/PerfEventFilter;->eventsWaitingBlockingPeer:Landroid/os/statistics/FastPerfEventList;

    new-instance v0, Landroid/os/statistics/FastPerfEventList;

    const-class v1, Landroid/os/statistics/MicroscopicEvent;

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Landroid/os/statistics/FastPerfEventList;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/os/statistics/PerfEventFilter;->lastBatchBeginTimeMillis:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/os/statistics/PerfEventFilter;->nativeBacktraceMapUpdated:Z

    new-instance v0, Landroid/os/statistics/FastPerfEventList;

    const-class v1, Landroid/os/statistics/MacroscopicEvent;

    invoke-direct {v0, v1, v3}, Landroid/os/statistics/FastPerfEventList;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMacroEvents:Landroid/os/statistics/FastPerfEventList;

    new-instance v0, Landroid/os/statistics/FastPerfEventList;

    const-class v1, Landroid/os/statistics/MicroscopicEvent;

    invoke-direct {v0, v1, v3}, Landroid/os/statistics/FastPerfEventList;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMicroEvents:Landroid/os/statistics/FastPerfEventList;

    iput-boolean p1, p0, Landroid/os/statistics/PerfEventFilter;->isAppSideFilter:Z

    return-void
.end method

.method private addCompletedEvent(Landroid/os/statistics/MicroscopicEvent;)V
    .locals 8

    iget-boolean v4, p0, Landroid/os/statistics/PerfEventFilter;->isAppSideFilter:Z

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Landroid/os/statistics/MicroscopicEvent;->resolveLazyInfo()V

    :cond_0
    const/4 v3, 0x0

    iget-object v4, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMicroEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v1, v4, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v1, [Landroid/os/statistics/MicroscopicEvent;

    iget-object v4, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMicroEvents:Landroid/os/statistics/FastPerfEventList;

    iget v4, v4, Landroid/os/statistics/FastPerfEventList;->size:I

    add-int/lit8 v2, v4, -0x1

    :goto_0
    if-ltz v2, :cond_3

    aget-object v0, v1, v2

    iget-wide v4, v0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v6, p1, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_2
    iget-wide v4, v0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v6, p1, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_4

    add-int/lit8 v3, v2, 0x1

    :cond_3
    :goto_1
    iget-object v4, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMicroEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v4, v3, p1}, Landroid/os/statistics/FastPerfEventList;->add(ILandroid/os/statistics/PerfEvent;)V

    return-void

    :cond_4
    invoke-virtual {v0}, Landroid/os/statistics/MicroscopicEvent;->isRootEvent()Z

    move-result v4

    invoke-virtual {p1}, Landroid/os/statistics/MicroscopicEvent;->isRootEvent()Z

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Boolean;->compare(ZZ)I

    move-result v4

    if-gtz v4, :cond_5

    add-int/lit8 v3, v2, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual {v0}, Landroid/os/statistics/MicroscopicEvent;->isMasterEvent()Z

    move-result v4

    invoke-virtual {p1}, Landroid/os/statistics/MicroscopicEvent;->isMasterEvent()Z

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Boolean;->compare(ZZ)I

    move-result v4

    if-gtz v4, :cond_1

    add-int/lit8 v3, v2, 0x1

    goto :goto_1
.end method

.method private checkEffectivePerfEvent(Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 10

    const/4 v9, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    iget-boolean v3, p0, Landroid/os/statistics/PerfEventFilter;->isAppSideFilter:Z

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Landroid/os/statistics/MicroscopicEvent;->hasNativeStack()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Landroid/os/statistics/PerfEventFilter;->nativeBacktraceMapUpdated:Z

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    invoke-static {}, Landroid/os/statistics/NativeBackTrace;->updateBacktraceMap()I

    iput-boolean v8, p0, Landroid/os/statistics/PerfEventFilter;->nativeBacktraceMapUpdated:Z

    :cond_0
    invoke-virtual {p1}, Landroid/os/statistics/MicroscopicEvent;->resolveLazyInfo()V

    invoke-virtual {p1}, Landroid/os/statistics/MicroscopicEvent;->isMeaningful()Z

    move-result v3

    if-nez v3, :cond_1

    return v4

    :cond_1
    invoke-virtual {p1}, Landroid/os/statistics/MicroscopicEvent;->hasPeerBlockingEvent()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Landroid/os/statistics/PerfEventFilter;->isAppSideFilter:Z

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Landroid/os/statistics/MicroscopicEvent;->isBlockedBySameProcess()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Landroid/os/statistics/PerfEventFilter;->eventsWaitingBlockingPeer:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v3, p1}, Landroid/os/statistics/FastPerfEventList;->add(Landroid/os/statistics/PerfEvent;)V

    :cond_3
    return v8

    :cond_4
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget v3, v3, Landroid/os/statistics/FastPerfEventList;->size:I

    if-ge v0, v3, :cond_7

    iget-object v3, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v3, v3, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v3, [Landroid/os/statistics/MicroscopicEvent;

    aget-object v2, v3, v0

    if-nez v2, :cond_6

    :cond_5
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    iget-wide v4, v2, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    iget-wide v6, p1, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    cmp-long v3, v4, v6

    if-ltz v3, :cond_8

    :cond_7
    return v8

    :cond_8
    iget-wide v4, v2, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v6, p1, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    invoke-virtual {v2}, Landroid/os/statistics/MicroscopicEvent;->isPeerBlockingEvent()Z

    move-result v3

    if-nez v3, :cond_a

    invoke-direct {p0, p1, v2}, Landroid/os/statistics/PerfEventFilter;->isIncludedEnough(Landroid/os/statistics/MicroscopicEvent;Landroid/os/statistics/MicroscopicEvent;)Z

    move-result v1

    :goto_2
    if-eqz v1, :cond_5

    iget-object v3, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v3, v3, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v3, [Landroid/os/statistics/MicroscopicEvent;

    aput-object v9, v3, v0

    iget v3, v2, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    and-int/lit8 v3, v3, 0x3

    if-nez v3, :cond_9

    iget v3, v2, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    iget v4, p1, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    and-int/lit8 v4, v4, 0x3

    or-int/2addr v3, v4

    iput v3, v2, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    :cond_9
    iget-object v3, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v3, v2}, Landroid/os/statistics/FastPerfEventList;->add(Landroid/os/statistics/PerfEvent;)V

    goto :goto_1

    :cond_a
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private checkEffectivePerfEvents(II)V
    .locals 5

    const/4 v4, 0x0

    move v1, p1

    :goto_0
    if-ge v1, p2, :cond_2

    iget-object v3, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v3, v3, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v3, [Landroid/os/statistics/MicroscopicEvent;

    aget-object v0, v3, v1

    if-nez v0, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Landroid/os/statistics/PerfEventFilter;->checkEffectivePerfEvent(Landroid/os/statistics/MicroscopicEvent;)Z

    move-result v2

    iget-object v3, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v3, v3, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v3, [Landroid/os/statistics/MicroscopicEvent;

    aput-object v4, v3, v1

    if-eqz v2, :cond_1

    invoke-direct {p0, v0}, Landroid/os/statistics/PerfEventFilter;->addCompletedEvent(Landroid/os/statistics/MicroscopicEvent;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private checkEventWaitingBlockingPeer(Landroid/os/statistics/MicroscopicEvent;J)Z
    .locals 34

    move-object/from16 v0, p1

    iget-wide v0, v0, Landroid/os/statistics/MicroscopicEvent;->matchedPeerBlockingDuration:J

    move-wide/from16 v24, v0

    const-wide/16 v10, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/os/statistics/FastPerfEventList;->size:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    iget-wide v4, v0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    move-object/from16 v0, p1

    iget-wide v8, v0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    move-object/from16 v17, v0

    check-cast v17, [Landroid/os/statistics/MicroscopicEvent;

    const/4 v15, 0x0

    :goto_0
    move/from16 v0, v27

    if-ge v15, v0, :cond_2

    aget-object v26, v17, v15

    if-nez v26, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, v26

    iget-wide v0, v0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    move-wide/from16 v28, v0

    cmp-long v28, v28, v8

    if-ltz v28, :cond_5

    :cond_2
    move-wide/from16 v0, v24

    move-object/from16 v2, p1

    iput-wide v0, v2, Landroid/os/statistics/MicroscopicEvent;->matchedPeerBlockingDuration:J

    sub-long v6, v8, v4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/os/statistics/PerfEventFilter;->isAppSideFilter:Z

    move/from16 v28, v0

    if-eqz v28, :cond_c

    move-wide/from16 v22, v6

    const-wide/16 v28, 0x7d0

    cmp-long v28, v6, v28

    if-gez v28, :cond_3

    const-wide/16 v22, 0x7d0

    :cond_3
    :goto_2
    const/16 v28, 0x1

    shr-long v28, v6, v28

    const/16 v30, 0x2

    shr-long v30, v6, v30

    add-long v28, v28, v30

    const/16 v30, 0x3

    shr-long v30, v6, v30

    add-long v28, v28, v30

    const/16 v30, 0x4

    shr-long v30, v6, v30

    add-long v20, v28, v30

    invoke-virtual/range {p1 .. p1}, Landroid/os/statistics/MicroscopicEvent;->hasMultiplePeerBlockingEvents()Z

    move-result v28

    if-eqz v28, :cond_f

    const/16 v28, 0x1

    shr-long v28, v6, v28

    const/16 v30, 0x2

    shr-long v30, v6, v30

    add-long v28, v28, v30

    const/16 v30, 0x4

    shr-long v30, v6, v30

    add-long v18, v28, v30

    cmp-long v28, v10, v18

    if-gez v28, :cond_d

    cmp-long v28, v24, v20

    if-ltz v28, :cond_e

    const/4 v14, 0x1

    :goto_3
    if-nez v14, :cond_4

    add-long v28, v8, v22

    cmp-long v28, v28, p2

    if-gez v28, :cond_11

    :cond_4
    const/16 v28, 0x1

    :goto_4
    return v28

    :cond_5
    move-object/from16 v0, v26

    iget-wide v0, v0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    move-wide/from16 v28, v0

    cmp-long v28, v28, v4

    if-lez v28, :cond_0

    invoke-virtual/range {v26 .. v26}, Landroid/os/statistics/MicroscopicEvent;->isPeerBlockingEvent()Z

    move-result v28

    if-eqz v28, :cond_9

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/statistics/MicroscopicEvent;->isBlockedBy(Landroid/os/statistics/MicroscopicEvent;)Z

    move-result v28

    if-eqz v28, :cond_9

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/os/statistics/PerfEventFilter;->isTimeIncludedEnough(Landroid/os/statistics/MicroscopicEvent;Landroid/os/statistics/MicroscopicEvent;)Z

    move-result v16

    :goto_5
    if-eqz v16, :cond_0

    move-object/from16 v0, v26

    iget-wide v0, v0, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v26

    iget-wide v0, v0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    move-wide/from16 v32, v0

    cmp-long v28, v28, v32

    if-ltz v28, :cond_a

    move-object/from16 v0, v26

    iget-wide v0, v0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    move-wide/from16 v28, v0

    :goto_6
    sub-long v12, v30, v28

    invoke-virtual/range {p1 .. p1}, Landroid/os/statistics/MicroscopicEvent;->hasMultiplePeerBlockingEvents()Z

    move-result v28

    if-eqz v28, :cond_b

    add-long v24, v24, v12

    :cond_6
    :goto_7
    cmp-long v28, v10, v12

    if-gez v28, :cond_7

    move-wide v10, v12

    :cond_7
    const/16 v28, 0x0

    aput-object v28, v17, v15

    move-object/from16 v0, v26

    iget v0, v0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    move/from16 v28, v0

    and-int/lit8 v28, v28, 0x3

    if-nez v28, :cond_8

    move-object/from16 v0, v26

    iget v0, v0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    move/from16 v28, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    move/from16 v29, v0

    and-int/lit8 v29, v29, 0x3

    or-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, v26

    iput v0, v1, Landroid/os/statistics/MicroscopicEvent;->eventFlags:I

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/statistics/FastPerfEventList;->add(Landroid/os/statistics/PerfEvent;)V

    goto/16 :goto_1

    :cond_9
    const/16 v16, 0x0

    goto :goto_5

    :cond_a
    move-object/from16 v0, p1

    iget-wide v0, v0, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    move-wide/from16 v28, v0

    goto :goto_6

    :cond_b
    cmp-long v28, v24, v12

    if-gez v28, :cond_6

    move-wide/from16 v24, v12

    goto :goto_7

    :cond_c
    const/16 v28, 0x1

    shl-long v22, v6, v28

    const-wide/16 v28, 0x2710

    cmp-long v28, v22, v28

    if-gez v28, :cond_3

    const-wide/16 v22, 0x2710

    goto/16 :goto_2

    :cond_d
    const/4 v14, 0x1

    goto/16 :goto_3

    :cond_e
    const/4 v14, 0x0

    goto/16 :goto_3

    :cond_f
    cmp-long v28, v24, v20

    if-ltz v28, :cond_10

    const/4 v14, 0x1

    goto/16 :goto_3

    :cond_10
    const/4 v14, 0x0

    goto/16 :goto_3

    :cond_11
    const/16 v28, 0x0

    goto/16 :goto_4
.end method

.method private checkEventsWaitingBlockingPeer(IIJ)V
    .locals 7

    const/4 v5, 0x0

    move v1, p1

    iget-object v4, p0, Landroid/os/statistics/PerfEventFilter;->eventsWaitingBlockingPeer:Landroid/os/statistics/FastPerfEventList;

    iget-object v2, v4, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v2, [Landroid/os/statistics/MicroscopicEvent;

    :goto_0
    if-ge v1, p2, :cond_2

    aget-object v0, v2, v1

    if-nez v0, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0, p3, p4}, Landroid/os/statistics/PerfEventFilter;->checkEventWaitingBlockingPeer(Landroid/os/statistics/MicroscopicEvent;J)Z

    move-result v3

    if-eqz v3, :cond_1

    aput-object v5, v2, v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private checkSuspectedPerfEvents(JJ)V
    .locals 11

    const/4 v4, 0x0

    :goto_0
    iget-object v6, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget v6, v6, Landroid/os/statistics/FastPerfEventList;->size:I

    if-ge v4, v6, :cond_6

    iget-object v6, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v6, v6, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v6, [Landroid/os/statistics/MicroscopicEvent;

    aget-object v5, v6, v4

    if-nez v5, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    iget-wide v6, v5, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    cmp-long v6, v6, p1

    if-gez v6, :cond_2

    iget-object v6, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v6, v6, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v6, [Landroid/os/statistics/MicroscopicEvent;

    const/4 v7, 0x0

    aput-object v7, v6, v4

    goto :goto_1

    :cond_2
    iget-wide v6, v5, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v8, v5, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    sub-long v2, v6, v8

    iget-boolean v6, p0, Landroid/os/statistics/PerfEventFilter;->isAppSideFilter:Z

    if-eqz v6, :cond_4

    const/4 v6, 0x6

    shl-long v6, v2, v6

    const-wide/16 v8, 0x2710

    cmp-long v6, v8, v6

    if-lez v6, :cond_3

    const-wide/16 v6, 0x2710

    :goto_2
    sub-long v0, p3, v6

    :goto_3
    iget-wide v6, v5, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    cmp-long v6, v6, v0

    if-gez v6, :cond_0

    iget-object v6, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v6, v6, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v6, [Landroid/os/statistics/MicroscopicEvent;

    const/4 v7, 0x0

    aput-object v7, v6, v4

    goto :goto_1

    :cond_3
    const/4 v6, 0x6

    shl-long v6, v2, v6

    goto :goto_2

    :cond_4
    const/4 v6, 0x7

    shl-long v6, v2, v6

    const-wide/16 v8, 0x4e20

    cmp-long v6, v8, v6

    if-lez v6, :cond_5

    const-wide/16 v6, 0x4e20

    :goto_4
    sub-long v0, p3, v6

    goto :goto_3

    :cond_5
    const/4 v6, 0x7

    shl-long v6, v2, v6

    goto :goto_4

    :cond_6
    return-void
.end method

.method private getEarliestEventBeginUptimeMillisInBatch(Landroid/os/statistics/FastPerfEventList;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/os/statistics/PerfEvent;",
            ">(",
            "Landroid/os/statistics/FastPerfEventList",
            "<TT;>;)J"
        }
    .end annotation

    const-wide v2, 0x7fffffffffffffffL

    iget v7, p1, Landroid/os/statistics/FastPerfEventList;->size:I

    iget-object v4, p1, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v6, v4, v5

    invoke-virtual {v6}, Landroid/os/statistics/PerfEvent;->getBeginUptimeMillis()J

    move-result-wide v0

    cmp-long v8, v2, v0

    if-lez v8, :cond_0

    move-wide v2, v0

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    const-wide v8, 0x7fffffffffffffffL

    cmp-long v8, v2, v8

    if-nez v8, :cond_2

    const-wide/16 v2, 0x0

    :cond_2
    return-wide v2
.end method

.method private isIncludedEnough(Landroid/os/statistics/MicroscopicEvent;Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 2

    iget v0, p1, Landroid/os/statistics/MicroscopicEvent;->pid:I

    iget v1, p2, Landroid/os/statistics/MicroscopicEvent;->pid:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Landroid/os/statistics/MicroscopicEvent;->threadId:I

    iget v1, p2, Landroid/os/statistics/MicroscopicEvent;->threadId:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    invoke-direct {p0, p1, p2}, Landroid/os/statistics/PerfEventFilter;->isTimeIncludedEnough(Landroid/os/statistics/MicroscopicEvent;Landroid/os/statistics/MicroscopicEvent;)Z

    move-result v0

    return v0
.end method

.method private isTimeIncludedEnough(Landroid/os/statistics/MicroscopicEvent;Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 12

    const/4 v6, 0x1

    iget-wide v8, p1, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v10, p1, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    sub-long v0, v8, v10

    iget-wide v8, p2, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    sub-long v4, v8, v10

    iget-wide v8, p1, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    cmp-long v7, v8, v10

    if-lez v7, :cond_2

    iget-wide v8, p1, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    cmp-long v7, v8, v10

    if-gez v7, :cond_1

    iget-wide v8, p1, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    cmp-long v7, v8, v10

    if-ltz v7, :cond_1

    iget-wide v8, p2, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v10, p1, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    sub-long v2, v8, v10

    :goto_0
    cmp-long v7, v2, v4

    if-eqz v7, :cond_0

    sget v7, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionDivisionRatio:I

    int-to-long v8, v7

    mul-long/2addr v8, v2

    sget v7, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionSoftThreshold:I

    int-to-long v10, v7

    cmp-long v7, v8, v10

    if-ltz v7, :cond_6

    :cond_0
    :goto_1
    return v6

    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0

    :cond_2
    iget-wide v8, p1, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    cmp-long v7, v8, v10

    if-nez v7, :cond_4

    iget-wide v8, p1, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    cmp-long v7, v8, v10

    if-ltz v7, :cond_3

    iget-wide v8, p2, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    sub-long v2, v8, v10

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_0

    :cond_4
    iget-wide v8, p1, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    cmp-long v7, v8, v10

    if-lez v7, :cond_5

    iget-wide v8, p1, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->endUptimeMillis:J

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    iget-wide v10, p2, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    sub-long v2, v8, v10

    goto :goto_0

    :cond_5
    const-wide/16 v2, 0x0

    goto :goto_0

    :cond_6
    const-wide/16 v8, 0x5

    mul-long/2addr v8, v2

    cmp-long v7, v8, v0

    if-gez v7, :cond_0

    const/4 v6, 0x0

    goto :goto_1
.end method

.method private mergeSuspectedEvents(Landroid/os/statistics/FastPerfEventList;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/statistics/FastPerfEventList",
            "<",
            "Landroid/os/statistics/PerfEvent;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/statistics/FastPerfEventList;->size:I

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    if-nez v12, :cond_0

    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget v3, v13, Landroid/os/statistics/FastPerfEventList;->size:I

    add-int v11, v12, v3

    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v13, v13, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v13, [Landroid/os/statistics/MicroscopicEvent;

    array-length v13, v13

    if-le v11, v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v13, v11}, Landroid/os/statistics/FastPerfEventList;->ensureCapacity(I)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget-object v4, v13, Landroid/os/statistics/FastPerfEventList;->events:[Landroid/os/statistics/PerfEvent;

    check-cast v4, [Landroid/os/statistics/MicroscopicEvent;

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v12, :cond_6

    aget-object v8, v5, v6

    instance-of v13, v8, Landroid/os/statistics/MacroscopicEvent;

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget-boolean v13, v0, Landroid/os/statistics/PerfEventFilter;->isAppSideFilter:Z

    if-eqz v13, :cond_2

    invoke-virtual {v8}, Landroid/os/statistics/PerfEvent;->resolveLazyInfo()V

    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMacroEvents:Landroid/os/statistics/FastPerfEventList;

    check-cast v8, Landroid/os/statistics/MacroscopicEvent;

    invoke-virtual {v13, v8}, Landroid/os/statistics/FastPerfEventList;->add(Landroid/os/statistics/PerfEvent;)V

    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    move-object v9, v8

    check-cast v9, Landroid/os/statistics/MicroscopicEvent;

    const/4 v10, 0x0

    add-int/lit8 v7, v3, -0x1

    :goto_2
    if-ltz v7, :cond_4

    aget-object v2, v4, v7

    iget-wide v14, v2, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    iget-wide v0, v9, Landroid/os/statistics/MicroscopicEvent;->beginUptimeMillis:J

    move-wide/from16 v16, v0

    cmp-long v13, v14, v16

    if-gtz v13, :cond_5

    add-int/lit8 v10, v7, 0x1

    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v13, v10, v9}, Landroid/os/statistics/FastPerfEventList;->add(ILandroid/os/statistics/PerfEvent;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    add-int/lit8 v7, v7, -0x1

    goto :goto_2

    :cond_6
    return-void
.end method


# virtual methods
.method public filter(Landroid/os/statistics/FastPerfEventList;Landroid/os/statistics/FastPerfEventList;JLandroid/os/statistics/FastPerfEventList;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/statistics/FastPerfEventList",
            "<",
            "Landroid/os/statistics/MicroscopicEvent;",
            ">;",
            "Landroid/os/statistics/FastPerfEventList",
            "<",
            "Landroid/os/statistics/PerfEvent;",
            ">;J",
            "Landroid/os/statistics/FastPerfEventList",
            "<",
            "Landroid/os/statistics/PerfEvent;",
            ">;)V"
        }
    .end annotation

    const/4 v10, 0x0

    iput-boolean v10, p0, Landroid/os/statistics/PerfEventFilter;->nativeBacktraceMapUpdated:Z

    invoke-direct/range {p0 .. p1}, Landroid/os/statistics/PerfEventFilter;->getEarliestEventBeginUptimeMillisInBatch(Landroid/os/statistics/FastPerfEventList;)J

    move-result-wide v10

    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Landroid/os/statistics/PerfEventFilter;->getEarliestEventBeginUptimeMillisInBatch(Landroid/os/statistics/FastPerfEventList;)J

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-nez v10, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    :cond_0
    iget-wide v10, p0, Landroid/os/statistics/PerfEventFilter;->lastBatchBeginTimeMillis:J

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/PerfEventFilter;->lastBatchBeginTimeMillis:J

    move-object/from16 v0, p1

    iget v10, v0, Landroid/os/statistics/FastPerfEventList;->size:I

    if-lez v10, :cond_1

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/os/statistics/FastPerfEventList;->addAll(Landroid/os/statistics/FastPerfEventList;)V

    :cond_1
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Landroid/os/statistics/PerfEventFilter;->mergeSuspectedEvents(Landroid/os/statistics/FastPerfEventList;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->eventsWaitingBlockingPeer:Landroid/os/statistics/FastPerfEventList;

    iget v7, v10, Landroid/os/statistics/FastPerfEventList;->size:I

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget v6, v10, Landroid/os/statistics/FastPerfEventList;->size:I

    :goto_0
    if-ne v7, v5, :cond_2

    if-eq v6, v4, :cond_3

    :cond_2
    invoke-direct {p0, v5, v7, v2, v3}, Landroid/os/statistics/PerfEventFilter;->checkEventsWaitingBlockingPeer(IIJ)V

    move v5, v7

    invoke-direct {p0, v4, v6}, Landroid/os/statistics/PerfEventFilter;->checkEffectivePerfEvents(II)V

    move v4, v6

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->eventsWaitingBlockingPeer:Landroid/os/statistics/FastPerfEventList;

    iget v7, v10, Landroid/os/statistics/FastPerfEventList;->size:I

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    iget v6, v10, Landroid/os/statistics/FastPerfEventList;->size:I

    goto :goto_0

    :cond_3
    move-wide/from16 v0, p3

    invoke-direct {p0, v0, v1, v2, v3}, Landroid/os/statistics/PerfEventFilter;->checkSuspectedPerfEvents(JJ)V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMacroEvents:Landroid/os/statistics/FastPerfEventList;

    iget v10, v10, Landroid/os/statistics/FastPerfEventList;->size:I

    iget-object v11, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMicroEvents:Landroid/os/statistics/FastPerfEventList;

    iget v11, v11, Landroid/os/statistics/FastPerfEventList;->size:I

    add-int/2addr v10, v11

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Landroid/os/statistics/FastPerfEventList;->ensureCapacity(I)V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMacroEvents:Landroid/os/statistics/FastPerfEventList;

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Landroid/os/statistics/FastPerfEventList;->addAll(Landroid/os/statistics/FastPerfEventList;)V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMicroEvents:Landroid/os/statistics/FastPerfEventList;

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Landroid/os/statistics/FastPerfEventList;->addAll(Landroid/os/statistics/FastPerfEventList;)V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v10}, Landroid/os/statistics/FastPerfEventList;->compact()V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    const/16 v11, 0x40

    invoke-virtual {v10, v11}, Landroid/os/statistics/FastPerfEventList;->trimTo(I)V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->eventsWaitingBlockingPeer:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v10}, Landroid/os/statistics/FastPerfEventList;->compact()V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->eventsWaitingBlockingPeer:Landroid/os/statistics/FastPerfEventList;

    const/16 v11, 0x40

    invoke-virtual {v10, v11}, Landroid/os/statistics/FastPerfEventList;->trimTo(I)V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v10}, Landroid/os/statistics/FastPerfEventList;->compact()V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    const/16 v11, 0x80

    invoke-virtual {v10, v11}, Landroid/os/statistics/FastPerfEventList;->trimTo(I)V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMacroEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v10}, Landroid/os/statistics/FastPerfEventList;->clear()V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMacroEvents:Landroid/os/statistics/FastPerfEventList;

    const/16 v11, 0x40

    invoke-virtual {v10, v11}, Landroid/os/statistics/FastPerfEventList;->trimTo(I)V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMicroEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v10}, Landroid/os/statistics/FastPerfEventList;->clear()V

    iget-object v10, p0, Landroid/os/statistics/PerfEventFilter;->tempCompletedMicroEvents:Landroid/os/statistics/FastPerfEventList;

    const/16 v11, 0x40

    invoke-virtual {v10, v11}, Landroid/os/statistics/FastPerfEventList;->trimTo(I)V

    return-void
.end method

.method public hasPendingPerfEvents()Z
    .locals 1

    iget-object v0, p0, Landroid/os/statistics/PerfEventFilter;->effectivePerfEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v0}, Landroid/os/statistics/FastPerfEventList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/os/statistics/PerfEventFilter;->eventsWaitingBlockingPeer:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v0}, Landroid/os/statistics/FastPerfEventList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/os/statistics/PerfEventFilter;->suspectedPerfEvents:Landroid/os/statistics/FastPerfEventList;

    invoke-virtual {v0}, Landroid/os/statistics/FastPerfEventList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
