.class public final Landroid/os/statistics/MonitorSuperviser$SingleLockHold;
.super Landroid/os/statistics/MicroscopicEvent;
.source "MonitorSuperviser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/statistics/MonitorSuperviser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SingleLockHold"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/MonitorSuperviser$SingleLockHold$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/statistics/MonitorSuperviser$SingleLockHold;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIELD_MONITOR_ID:Ljava/lang/String; = "monitorId"

.field private static final FIELD_STACK:Ljava/lang/String; = "stack"


# instance fields
.field private javaBackTrace:Ljava/lang/Object;

.field public monitorId:J

.field public stackTrace:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold$1;

    invoke-direct {v0}, Landroid/os/statistics/MonitorSuperviser$SingleLockHold$1;-><init>()V

    sput-object v0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "LockHold"

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MicroscopicEvent;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method private copyFrom(Landroid/os/statistics/MonitorSuperviser$SingleLockHold;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget-wide v0, p1, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->monitorId:J

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->monitorId:J

    iget-object v0, p1, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->javaBackTrace:Ljava/lang/Object;

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;

    invoke-direct {p0, p1}, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->copyFrom(Landroid/os/statistics/MonitorSuperviser$SingleLockHold;)V

    return-void
.end method

.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->threadId:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->threadName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->beginUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->endUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Landroid/os/statistics/OsUtils;->decodeThreadSchedulePolicy(I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->schedPolicy:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->schedPriority:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->schedGroup:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->runningTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->runnableTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->sleepingTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->monitorId:J

    iput-object p2, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method public hasMultiplePeerBlockingEvents()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBy(Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBySameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingMultiplePeer()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isBlockingSameProcess()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isRootEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "monitorId"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->monitorId:J

    const-string/jumbo v0, "stack"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-static {v0}, Landroid/os/statistics/StackUtils;->getStackTrace(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->monitorId:J

    invoke-static {p1}, Landroid/os/statistics/ParcelUtils;->readStringArray(Landroid/os/Parcel;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/statistics/StackUtils;->emptyStack:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    :cond_0
    iput-object v2, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method public reset()V
    .locals 2

    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->reset()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->monitorId:J

    sget-object v0, Landroid/os/statistics/StackUtils;->emptyStack:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method resolveLazyInfo()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->lazyInfoResolved:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->resolveLazyInfo()V

    iget-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->javaBackTrace:Ljava/lang/Object;

    invoke-static {v0}, Landroid/os/statistics/JavaBackTrace;->resolve(Ljava/lang/Object;)[Ljava/lang/StackTraceElement;

    move-result-object v0

    iget-object v1, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->javaBackTrace:Ljava/lang/Object;

    invoke-static {v1}, Landroid/os/statistics/JavaBackTrace;->resolveClasses(Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/os/statistics/StackUtils;->getStackTrace([Ljava/lang/StackTraceElement;[Ljava/lang/Class;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    iput-object v2, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "monitorId"

    iget-wide v2, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->monitorId:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "stack"

    iget-object v2, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    invoke-static {v2}, Lorg/json/JSONObject;->wrap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/os/statistics/MicroscopicEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget-wide v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->monitorId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->stackTrace:[Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/os/statistics/ParcelUtils;->writeStringArray(Landroid/os/Parcel;[Ljava/lang/String;)V

    return-void
.end method
