.class public Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;
.super Landroid/os/statistics/MacroscopicEvent;
.source "EventLogSuperviser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/statistics/EventLogSuperviser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SingleEventLogItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIELD_EVENT_LOG_TAG_ID:Ljava/lang/String; = "eventLogTagId"

.field private static final FIELD_EVENT_LOG_TAG_NAME:Ljava/lang/String; = "eventLogTagName"

.field private static final FIELD_EVENT_LOG_TIME:Ljava/lang/String; = "eventLogTime"

.field private static final FIELD_EVENT_LOG_VALUE_STRS:Ljava/lang/String; = "eventlogValues"


# instance fields
.field public currentTimeMillis:J

.field public eventLogTagId:I

.field public eventLogTagName:Ljava/lang/String;

.field private eventLogValueObject:Ljava/lang/Object;

.field public eventLogValueStrs:[Ljava/lang/String;


# direct methods
.method static synthetic -set0(Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    return-object p1
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem$1;

    invoke-direct {v0}, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem$1;-><init>()V

    sput-object v0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "EventLog"

    const/high16 v1, 0x10000

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MacroscopicEvent;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method private copyFrom(Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget-wide v0, p1, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->currentTimeMillis:J

    iput-wide v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->currentTimeMillis:J

    iget v0, p1, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    iput v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    iget-object v0, p1, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;

    invoke-direct {p0, p1}, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->copyFrom(Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;)V

    return-void
.end method

.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 0

    return-void
.end method

.method isUserPerceptible()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    const/16 v2, 0x7538

    if-eq v1, v2, :cond_0

    iget v1, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    const/16 v2, 0x7588

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v2, "eventLogTime"

    const-wide/16 v4, 0x0

    invoke-virtual {p1, v2, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->currentTimeMillis:J

    const-string/jumbo v2, "eventLogTagId"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    const-string/jumbo v2, "eventLogTagName"

    const-string/jumbo v3, ""

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    const-string/jumbo v2, "eventlogValues"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Landroid/os/statistics/EventLogSuperviser;->-get0()[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    :cond_1
    iput-object v6, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->currentTimeMillis:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    :cond_0
    invoke-static {p1}, Landroid/os/statistics/ParcelUtils;->readStringArray(Landroid/os/Parcel;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-static {}, Landroid/os/statistics/EventLogSuperviser;->-get0()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    :cond_1
    iput-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    return-void
.end method

.method public reset()V
    .locals 2

    invoke-super {p0}, Landroid/os/statistics/MacroscopicEvent;->reset()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->currentTimeMillis:J

    const/4 v0, 0x0

    iput v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    invoke-static {}, Landroid/os/statistics/EventLogSuperviser;->-get0()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    return-void
.end method

.method resolveLazyInfo()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-boolean v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->lazyInfoResolved:Z

    if-eqz v2, :cond_0

    return-void

    :cond_0
    invoke-super {p0}, Landroid/os/statistics/MacroscopicEvent;->resolveLazyInfo()V

    iget v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    invoke-static {v2}, Landroid/util/EventLog;->getTagName(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    iget-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    if-eqz v2, :cond_4

    iget-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    instance-of v2, v2, [Ljava/lang/Object;

    if-eqz v2, :cond_2

    iget-object v1, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    array-length v2, v1

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    iget-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    iget-object v3, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    :cond_3
    :goto_1
    iput-object v5, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueObject:Ljava/lang/Object;

    return-void

    :cond_4
    invoke-static {}, Landroid/os/statistics/EventLogSuperviser;->-get0()[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    goto :goto_1
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v2, "eventLogTime"

    iget-wide v4, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->currentTimeMillis:J

    invoke-virtual {p1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v2, "eventLogTagId"

    iget v3, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v2, "eventLogTagName"

    iget-object v3, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v0, Lorg/json/JSONArray;

    iget-object v2, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    invoke-direct {v0, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/Object;)V

    const-string/jumbo v2, "eventlogValues"

    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/os/statistics/MacroscopicEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget-wide v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->currentTimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogValueStrs:[Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/os/statistics/ParcelUtils;->writeStringArray(Landroid/os/Parcel;[Ljava/lang/String;)V

    return-void
.end method
