.class public Landroid/os/statistics/PerfEventConstants;
.super Ljava/lang/Object;
.source "PerfEventConstants.java"


# static fields
.field public static final FIELD_BEGIN_TIME:Ljava/lang/String; = "beginTime"

.field public static final FIELD_DEX_FILES:Ljava/lang/String; = "dexFiles"

.field public static final FIELD_END_REAL_TIME:Ljava/lang/String; = "endRealTime"

.field public static final FIELD_END_TIME:Ljava/lang/String; = "endTime"

.field public static final FIELD_EVENT_FLAGS:Ljava/lang/String; = "eventFlags"

.field public static final FIELD_OCCUR_TIME:Ljava/lang/String; = "occurTime"

.field public static final FIELD_RUNNABLE_TIME:Ljava/lang/String; = "runnableTime"

.field public static final FIELD_RUNNING_TIME:Ljava/lang/String; = "runningTime"

.field public static final FIELD_SCHED_GROUP:Ljava/lang/String; = "schedGroup"

.field public static final FIELD_SCHED_POLICY:Ljava/lang/String; = "policy"

.field public static final FIELD_SCHED_PRIORITY:Ljava/lang/String; = "priority"

.field public static final FIELD_SLEEPING_TIME:Ljava/lang/String; = "sleepingTime"

.field public static final FIELD_THREAD_COUNT:Ljava/lang/String; = "threadCount"

.field public static final FIELD_THREAD_ID:Ljava/lang/String; = "threadId"

.field public static final FIELD_THREAD_NAME:Ljava/lang/String; = "threadName"

.field public static final MAX_PERF_EVENT_PARCEL_SIZE:I = 0x1000


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
