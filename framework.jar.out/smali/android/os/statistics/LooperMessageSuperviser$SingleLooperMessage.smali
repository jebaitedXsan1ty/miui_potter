.class public Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;
.super Landroid/os/statistics/MicroscopicEvent;
.source "LooperMessageSuperviser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/statistics/LooperMessageSuperviser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SingleLooperMessage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage$1;
    }
.end annotation


# static fields
.field private static final ACTIVITY_CONFIGURATION_CHANGED:I = 0x7d

.field private static final APPLICATION_OPERATION_MESSAGE_CODES:[I

.field private static final BIND_APPLICATION:I = 0x6e

.field private static final CONFIGURATION_CHANGED:I = 0x76

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIELD_MESSAGE_NAME:Ljava/lang/String; = "messageName"

.field private static final FIELD_PLAN_UPTIME:Ljava/lang/String; = "planTime"

.field private static final HIDE_WINDOW:I = 0x6a

.field private static final LAUNCH_ACTIVITY:I = 0x64

.field private static final MULTI_WINDOW_MODE_CHANGED:I = 0x98

.field private static final NEW_INTENT:I = 0x70

.field private static final PAUSE_ACTIVITY:I = 0x65

.field private static final PAUSE_ACTIVITY_FINISHING:I = 0x66

.field private static final PICTURE_IN_PICTURE_MODE_CHANGED:I = 0x99

.field private static final RELAUNCH_ACTIVITY:I = 0x7e

.field private static final RESUME_ACTIVITY:I = 0x6b

.field private static final SEND_RESULT:I = 0x6c

.field private static final SHOW_WINDOW:I = 0x69

.field private static final STOP_ACTIVITY_HIDE:I = 0x68

.field private static final STOP_ACTIVITY_SHOW:I = 0x67

.field private static final WINDOW_OPERATION_MESSAGE_CODES:[I

.field private static activityThreadHandler:Landroid/os/Handler;

.field private static frameDisplayMessageCallbackClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static frameDisplayMessageTargetClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static isFrameDisplayMessageClassLoaded:Z


# instance fields
.field private messageCallbackClazz:Ljava/lang/Class;

.field public messageName:Ljava/lang/String;

.field private messageTargetClazz:Ljava/lang/Class;

.field private messageWhat:I

.field public planUptimeMillis:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage$1;

    invoke-direct {v0}, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage$1;-><init>()V

    sput-object v0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->WINDOW_OPERATION_MESSAGE_CODES:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/16 v1, 0x6e

    const/4 v2, 0x0

    aput v1, v0, v2

    sput-object v0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->APPLICATION_OPERATION_MESSAGE_CODES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x64
        0x65
        0x66
        0x67
        0x68
        0x69
        0x6a
        0x6b
        0x6c
        0x70
        0x76
        0x7d
        0x7e
        0x98
        0x99
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "LooperMessage"

    const/16 v1, 0x8

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MicroscopicEvent;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method private copyFrom(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget-object v0, p1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    iget-wide v0, p1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->planUptimeMillis:J

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->planUptimeMillis:J

    iget v0, p1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    iput v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    iget-object v0, p1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    iget-object v0, p1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    return-void
.end method

.method private static isActivityThreadMessage(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)Z
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->activityThreadHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    invoke-static {}, Landroid/app/ActivityThreadInjector;->getHandler()Landroid/os/Handler;

    move-result-object v1

    sput-object v1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->activityThreadHandler:Landroid/os/Handler;

    :cond_0
    sget-object v1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->activityThreadHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    sget-object v1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->activityThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private static isApplicationOperationMessage(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)Z
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->APPLICATION_OPERATION_MESSAGE_CODES:[I

    iget v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    invoke-static {v1, v2}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static isFrameDisplayMessage(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    if-nez v3, :cond_1

    :cond_0
    return v1

    :cond_1
    sget-boolean v3, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->isFrameDisplayMessageClassLoaded:Z

    if-nez v3, :cond_2

    :try_start_0
    const-string/jumbo v3, "android.view.Choreographer$FrameDisplayEventReceiver"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->frameDisplayMessageCallbackClass:Ljava/lang/Class;

    const-string/jumbo v3, "android.view.Choreographer$FrameHandler"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->frameDisplayMessageTargetClass:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    sput-boolean v2, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->isFrameDisplayMessageClassLoaded:Z

    :cond_2
    iget-object v3, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    sget-object v4, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->frameDisplayMessageCallbackClass:Ljava/lang/Class;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    sget-object v4, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->frameDisplayMessageTargetClass:Ljava/lang/Class;

    if-ne v3, v4, :cond_3

    move v1, v2

    :cond_3
    return v1

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    sput-boolean v2, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->isFrameDisplayMessageClassLoaded:Z

    throw v1
.end method

.method private static isWindowOperationMessage(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)Z
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->WINDOW_OPERATION_MESSAGE_CODES:[I

    iget v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    invoke-static {v1, v2}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;

    invoke-direct {p0, p1}, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->copyFrom(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)V

    return-void
.end method

.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->threadId:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->threadName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->beginUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->endUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Landroid/os/statistics/OsUtils;->decodeThreadSchedulePolicy(I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->schedPolicy:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->schedPriority:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->schedGroup:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->runningTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->runnableTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->sleepingTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->endRealTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->planUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    return-void
.end method

.method public hasMultiplePeerBlockingEvents()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBy(Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBySameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingMultiplePeer()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingSameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method isConcerned()Z
    .locals 14

    invoke-static {p0}, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->isActivityThreadMessage(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)Z

    move-result v0

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->isWindowOperationMessage(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)Z

    move-result v4

    invoke-static {p0}, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->isApplicationOperationMessage(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)Z

    move-result v1

    :cond_0
    invoke-static {p0}, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->isFrameDisplayMessage(Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;)Z

    move-result v2

    xor-int/lit8 v5, v4, 0x1

    xor-int/lit8 v10, v1, 0x1

    and-int/2addr v5, v10

    xor-int/lit8 v10, v2, 0x1

    and-int v3, v5, v10

    iget-wide v10, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->endUptimeMillis:J

    iget-wide v12, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->beginUptimeMillis:J

    sub-long v6, v10, v12

    const-wide v8, 0x7fffffffffffffffL

    if-eqz v3, :cond_2

    move-wide v8, v6

    :cond_1
    :goto_0
    cmp-long v5, v6, v8

    if-ltz v5, :cond_5

    const/4 v5, 0x1

    :goto_1
    return v5

    :cond_2
    if-eqz v2, :cond_3

    sget v5, Landroid/os/statistics/PerfSupervisionSettings;->sPerfSupervisionSoftThreshold:I

    int-to-long v8, v5

    goto :goto_0

    :cond_3
    if-eqz v4, :cond_4

    const-wide/16 v8, 0x12c

    goto :goto_0

    :cond_4
    if-eqz v1, :cond_1

    const-wide/16 v8, 0x258

    goto :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public isPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRootEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "messageName"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    const-string/jumbo v0, "planTime"

    invoke-virtual {p1, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->planUptimeMillis:J

    iput v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    iput-object v3, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    iput-object v3, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->planUptimeMillis:J

    const/4 v0, 0x0

    iput v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    iput-object v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    iput-object v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    return-void
.end method

.method public reset()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->reset()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->planUptimeMillis:J

    const/4 v0, 0x0

    iput v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    iput-object v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    iput-object v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    return-void
.end method

.method resolveLazyInfo()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->lazyInfoResolved:Z

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->resolveLazyInfo()V

    iget-object v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    if-eqz v1, :cond_3

    const-string/jumbo v1, "callback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    iget-object v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    if-eqz v1, :cond_1

    const-string/jumbo v1, " target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    iput-object v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageTargetClazz:Ljava/lang/Class;

    iput-object v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageCallbackClazz:Ljava/lang/Class;

    return-void

    :cond_3
    const-string/jumbo v1, "what="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageWhat:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "messageName"

    iget-object v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "planTime"

    iget-wide v2, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->planUptimeMillis:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/os/statistics/MicroscopicEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->messageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->planUptimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
