.class public final Landroid/os/statistics/SingleJniMethod;
.super Landroid/os/statistics/MicroscopicEvent;
.source "SingleJniMethod.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/SingleJniMethod$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/statistics/SingleJniMethod;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIELD_STACK:Ljava/lang/String; = "stack"

.field private static final exceptionalClassMethodSimpleNames:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final exceptionalJniMethodFullNames:[Ljava/lang/String;


# instance fields
.field private javaBackTrace:Ljava/lang/Object;

.field public stackTrace:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "java.lang.Object.wait"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "java.lang.Object.notify"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "java.lang.Object.notifyAll"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string/jumbo v1, "java.lang.Thread.sleep"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string/jumbo v1, "android.os.BinderProxy.transactNative"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Landroid/os/statistics/SingleJniMethod;->exceptionalJniMethodFullNames:[Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Landroid/os/statistics/SingleJniMethod;->exceptionalClassMethodSimpleNames:Ljava/util/HashMap;

    new-instance v0, Landroid/os/statistics/SingleJniMethod$1;

    invoke-direct {v0}, Landroid/os/statistics/SingleJniMethod$1;-><init>()V

    sput-object v0, Landroid/os/statistics/SingleJniMethod;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "JniMethod"

    const/16 v1, 0xa

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MicroscopicEvent;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method private static buildStackTrace([Ljava/lang/StackTraceElement;[Ljava/lang/Class;)[Ljava/lang/String;
    .locals 14

    sget-object v9, Landroid/os/statistics/SingleJniMethod;->exceptionalClassMethodSimpleNames:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_2

    sget-object v10, Landroid/os/statistics/SingleJniMethod;->exceptionalClassMethodSimpleNames:Ljava/util/HashMap;

    monitor-enter v10

    :try_start_0
    sget-object v11, Landroid/os/statistics/SingleJniMethod;->exceptionalJniMethodFullNames:[Ljava/lang/String;

    const/4 v9, 0x0

    array-length v12, v11

    :goto_0
    if-ge v9, v12, :cond_1

    aget-object v4, v11, v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string/jumbo v13, "."

    invoke-virtual {v4, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v13, v2, 0x1

    invoke-virtual {v4, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const/4 v13, 0x0

    invoke-virtual {v4, v13, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sget-object v13, Landroid/os/statistics/SingleJniMethod;->exceptionalClassMethodSimpleNames:Ljava/util/HashMap;

    invoke-virtual {v13, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    if-nez v3, :cond_0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget-object v13, Landroid/os/statistics/SingleJniMethod;->exceptionalClassMethodSimpleNames:Ljava/util/HashMap;

    invoke-virtual {v13, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_1
    monitor-exit v10

    :cond_2
    if-eqz p0, :cond_3

    array-length v9, p0

    if-nez v9, :cond_4

    :cond_3
    sget-object v9, Landroid/os/statistics/StackUtils;->emptyStack:[Ljava/lang/String;

    return-object v9

    :catchall_0
    move-exception v9

    monitor-exit v10

    throw v9

    :cond_4
    if-eqz p1, :cond_3

    array-length v9, p1

    if-eqz v9, :cond_3

    const/4 v9, 0x0

    aget-object v7, p0, v9

    const/4 v9, 0x0

    aget-object v6, p1, v9

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "android.os.statistics."

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    sget-object v9, Landroid/os/statistics/StackUtils;->emptyStack:[Ljava/lang/String;

    return-object v9

    :cond_5
    sget-object v9, Landroid/os/statistics/SingleJniMethod;->exceptionalClassMethodSimpleNames:Ljava/util/HashMap;

    invoke-virtual {v9, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    if-eqz v3, :cond_6

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    sget-object v9, Landroid/os/statistics/StackUtils;->emptyStack:[Ljava/lang/String;

    return-object v9

    :cond_6
    const/4 v9, 0x0

    invoke-static {p0, p1, v9}, Landroid/os/statistics/StackUtils;->getStackTrace([Ljava/lang/StackTraceElement;[Ljava/lang/Class;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    return-object v9

    :catch_0
    move-exception v8

    goto :goto_1
.end method

.method private copyFrom(Landroid/os/statistics/SingleJniMethod;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget-object v0, p1, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/SingleJniMethod;->javaBackTrace:Ljava/lang/Object;

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/SingleJniMethod;

    invoke-direct {p0, p1}, Landroid/os/statistics/SingleJniMethod;->copyFrom(Landroid/os/statistics/SingleJniMethod;)V

    return-void
.end method

.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/SingleJniMethod;->threadId:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/SingleJniMethod;->beginUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/SingleJniMethod;->endUptimeMillis:J

    iput-object p2, p0, Landroid/os/statistics/SingleJniMethod;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method public hasMultiplePeerBlockingEvents()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBy(Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBySameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingMultiplePeer()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingSameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method isMeaningful()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRootEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "stack"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-static {v0}, Landroid/os/statistics/StackUtils;->getStackTrace(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-static {p1}, Landroid/os/statistics/ParcelUtils;->readStringArray(Landroid/os/Parcel;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/statistics/StackUtils;->emptyStack:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    :cond_0
    iput-object v1, p0, Landroid/os/statistics/SingleJniMethod;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method public reset()V
    .locals 1

    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->reset()V

    sget-object v0, Landroid/os/statistics/StackUtils;->emptyStack:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method resolveLazyInfo()V
    .locals 2

    iget-boolean v0, p0, Landroid/os/statistics/SingleJniMethod;->lazyInfoResolved:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->resolveLazyInfo()V

    iget-object v0, p0, Landroid/os/statistics/SingleJniMethod;->javaBackTrace:Ljava/lang/Object;

    invoke-static {v0}, Landroid/os/statistics/JavaBackTrace;->resolve(Ljava/lang/Object;)[Ljava/lang/StackTraceElement;

    move-result-object v0

    iget-object v1, p0, Landroid/os/statistics/SingleJniMethod;->javaBackTrace:Ljava/lang/Object;

    invoke-static {v1}, Landroid/os/statistics/JavaBackTrace;->resolveClasses(Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/statistics/SingleJniMethod;->buildStackTrace([Ljava/lang/StackTraceElement;[Ljava/lang/Class;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/statistics/SingleJniMethod;->javaBackTrace:Ljava/lang/Object;

    return-void
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "stack"

    iget-object v2, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    invoke-static {v2}, Lorg/json/JSONObject;->wrap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/os/statistics/MicroscopicEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Landroid/os/statistics/SingleJniMethod;->stackTrace:[Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/os/statistics/ParcelUtils;->writeStringArray(Landroid/os/Parcel;[Ljava/lang/String;)V

    return-void
.end method
