.class public Landroid/os/statistics/EventLogSuperviser;
.super Ljava/lang/Object;
.source "EventLogSuperviser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/EventLogSuperviser$EventLogTags;,
        Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;
    }
.end annotation


# static fields
.field private static final emptyEventValueStrs:[Ljava/lang/String;

.field private static final supervisedEventLogTags:[I


# direct methods
.method static synthetic -get0()[Ljava/lang/String;
    .locals 1

    sget-object v0, Landroid/os/statistics/EventLogSuperviser;->emptyEventValueStrs:[Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Landroid/os/statistics/EventLogSuperviser;->supervisedEventLogTags:[I

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Landroid/os/statistics/EventLogSuperviser;->emptyEventValueStrs:[Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0xaa8
        0x7538
        0x7541
        0x7555
        0x7557
        0x7559
        0x7562
        0x7588
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isSupervised(I)Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/statistics/PerfSupervisionSettings;->getSupervisionLevel()I

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/statistics/EventLogSuperviser;->supervisedEventLogTags:[I

    invoke-static {v1, p0}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static notifyEvent(IF)V
    .locals 1

    invoke-static {p0}, Landroid/os/statistics/EventLogSuperviser;->isSupervised(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/os/statistics/EventLogSuperviser;->notifyEventWithObject(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public static notifyEvent(II)V
    .locals 1

    invoke-static {p0}, Landroid/os/statistics/EventLogSuperviser;->isSupervised(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/os/statistics/EventLogSuperviser;->notifyEventWithObject(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public static notifyEvent(IJ)V
    .locals 1

    invoke-static {p0}, Landroid/os/statistics/EventLogSuperviser;->isSupervised(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/os/statistics/EventLogSuperviser;->notifyEventWithObject(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public static notifyEvent(ILjava/lang/String;)V
    .locals 1

    invoke-static {p0}, Landroid/os/statistics/EventLogSuperviser;->isSupervised(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/os/statistics/EventLogSuperviser;->notifyEventWithObject(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public static varargs notifyEvent(I[Ljava/lang/Object;)V
    .locals 1

    invoke-static {p0}, Landroid/os/statistics/EventLogSuperviser;->isSupervised(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/os/statistics/EventLogSuperviser;->notifyEventWithObject(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private static notifyEventWithObject(ILjava/lang/Object;)V
    .locals 4

    new-instance v0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;

    invoke-direct {v0}, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;-><init>()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->occurUptimeMillis:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->currentTimeMillis:J

    iput p0, v0, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->eventLogTagId:I

    invoke-static {v0, p1}, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->-set0(Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Landroid/os/statistics/PerfEventReporter;->report(Landroid/os/statistics/PerfEvent;)V

    return-void
.end method
