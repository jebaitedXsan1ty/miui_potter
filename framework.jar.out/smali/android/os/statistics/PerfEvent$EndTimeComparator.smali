.class Landroid/os/statistics/PerfEvent$EndTimeComparator;
.super Ljava/lang/Object;
.source "PerfEvent.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/statistics/PerfEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EndTimeComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/os/statistics/PerfEvent;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/statistics/PerfEvent$EndTimeComparator;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/statistics/PerfEvent$EndTimeComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/os/statistics/PerfEvent;Landroid/os/statistics/PerfEvent;)I
    .locals 8

    invoke-virtual {p1}, Landroid/os/statistics/PerfEvent;->getEndUptimeMillis()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/os/statistics/PerfEvent;->getEndUptimeMillis()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Long;->compare(JJ)I

    move-result v2

    if-eqz v2, :cond_0

    return v2

    :cond_0
    invoke-virtual {p2}, Landroid/os/statistics/PerfEvent;->getBeginUptimeMillis()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/os/statistics/PerfEvent;->getBeginUptimeMillis()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Long;->compare(JJ)I

    move-result v2

    if-eqz v2, :cond_1

    return v2

    :cond_1
    instance-of v3, p1, Landroid/os/statistics/MacroscopicEvent;

    if-eqz v3, :cond_2

    const/4 v3, -0x1

    return v3

    :cond_2
    instance-of v3, p2, Landroid/os/statistics/MacroscopicEvent;

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    return v3

    :cond_3
    move-object v0, p1

    check-cast v0, Landroid/os/statistics/MicroscopicEvent;

    move-object v1, p2

    check-cast v1, Landroid/os/statistics/MicroscopicEvent;

    invoke-virtual {v0}, Landroid/os/statistics/MicroscopicEvent;->isRootEvent()Z

    move-result v3

    invoke-virtual {v1}, Landroid/os/statistics/MicroscopicEvent;->isRootEvent()Z

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Boolean;->compare(ZZ)I

    move-result v2

    if-eqz v2, :cond_4

    return v2

    :cond_4
    invoke-virtual {v0}, Landroid/os/statistics/MicroscopicEvent;->isMasterEvent()Z

    move-result v3

    invoke-virtual {v1}, Landroid/os/statistics/MicroscopicEvent;->isMasterEvent()Z

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Boolean;->compare(ZZ)I

    move-result v3

    return v3
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Landroid/os/statistics/PerfEvent;

    check-cast p2, Landroid/os/statistics/PerfEvent;

    invoke-virtual {p0, p1, p2}, Landroid/os/statistics/PerfEvent$EndTimeComparator;->compare(Landroid/os/statistics/PerfEvent;Landroid/os/statistics/PerfEvent;)I

    move-result v0

    return v0
.end method
