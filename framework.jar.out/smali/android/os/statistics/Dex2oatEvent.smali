.class public Landroid/os/statistics/Dex2oatEvent;
.super Landroid/os/statistics/MacroscopicEvent;
.source "Dex2oatEvent.java"


# static fields
.field public static EV_TYPE_NAME:Ljava/lang/String;


# instance fields
.field private mBeginUpTimeMills:J

.field private mCGroup:I

.field private mCPURunnableTimeMills:J

.field private mCPURunningTimeMills:J

.field private mDexFiles:Ljava/lang/String;

.field private mEndUpTimeMills:J

.field private mNice:I

.field private mScheduler:I

.field private mThreadCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "dex2oat"

    sput-object v0, Landroid/os/statistics/Dex2oatEvent;->EV_TYPE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 15

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const-string/jumbo v2, ""

    move-object v0, p0

    move v3, v1

    move-wide v6, v4

    move v8, v1

    move v9, v1

    move v10, v1

    move-wide v11, v4

    move-wide v13, v4

    invoke-direct/range {v0 .. v14}, Landroid/os/statistics/Dex2oatEvent;-><init>(ILjava/lang/String;IJJIIIJJ)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;IJJIIIJJ)V
    .locals 2

    sget-object v0, Landroid/os/statistics/Dex2oatEvent;->EV_TYPE_NAME:Ljava/lang/String;

    const v1, 0x10003

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MacroscopicEvent;-><init>(ILjava/lang/String;)V

    iput-object p2, p0, Landroid/os/statistics/Dex2oatEvent;->mDexFiles:Ljava/lang/String;

    iput p3, p0, Landroid/os/statistics/Dex2oatEvent;->mThreadCount:I

    iput-wide p4, p0, Landroid/os/statistics/Dex2oatEvent;->mBeginUpTimeMills:J

    iput-wide p6, p0, Landroid/os/statistics/Dex2oatEvent;->mEndUpTimeMills:J

    iput p8, p0, Landroid/os/statistics/Dex2oatEvent;->mScheduler:I

    iput p9, p0, Landroid/os/statistics/Dex2oatEvent;->mNice:I

    iput p10, p0, Landroid/os/statistics/Dex2oatEvent;->mCGroup:I

    iput-wide p11, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunningTimeMills:J

    iput-wide p13, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunnableTimeMills:J

    iget-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mEndUpTimeMills:J

    iput-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->occurUptimeMillis:J

    iput p1, p0, Landroid/os/statistics/Dex2oatEvent;->pid:I

    iget v0, p0, Landroid/os/statistics/Dex2oatEvent;->pid:I

    invoke-static {v0}, Lmiui/os/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/Dex2oatEvent;->processName:Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/Dex2oatEvent;->processName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/os/statistics/Dex2oatEvent;->pid:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/Dex2oatEvent;->processName:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 0

    return-void
.end method

.method public getBeginUptimeMillis()J
    .locals 2

    iget-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mBeginUpTimeMills:J

    return-wide v0
.end method

.method public getEndUptimeMillis()J
    .locals 2

    iget-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mEndUpTimeMills:J

    return-wide v0
.end method

.method occursInCurrentProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "dexFiles"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/Dex2oatEvent;->mDexFiles:Ljava/lang/String;

    const-string/jumbo v0, "threadCount"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/Dex2oatEvent;->mThreadCount:I

    const-string/jumbo v0, "beginTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mBeginUpTimeMills:J

    const-string/jumbo v0, "endTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mEndUpTimeMills:J

    const-string/jumbo v0, "policy"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/Dex2oatEvent;->mScheduler:I

    const-string/jumbo v0, "priority"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/Dex2oatEvent;->mNice:I

    const-string/jumbo v0, "schedGroup"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/Dex2oatEvent;->mCGroup:I

    const-string/jumbo v0, "runningTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunningTimeMills:J

    const-string/jumbo v0, "runnableTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunnableTimeMills:J

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/Dex2oatEvent;->mDexFiles:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/Dex2oatEvent;->mThreadCount:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mBeginUpTimeMills:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mEndUpTimeMills:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/Dex2oatEvent;->mScheduler:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/Dex2oatEvent;->mNice:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/Dex2oatEvent;->mCGroup:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunningTimeMills:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunnableTimeMills:J

    return-void
.end method

.method public reset()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/os/statistics/MacroscopicEvent;->reset()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/Dex2oatEvent;->mDexFiles:Ljava/lang/String;

    iput v1, p0, Landroid/os/statistics/Dex2oatEvent;->mThreadCount:I

    iput-wide v2, p0, Landroid/os/statistics/Dex2oatEvent;->mBeginUpTimeMills:J

    iput-wide v2, p0, Landroid/os/statistics/Dex2oatEvent;->mEndUpTimeMills:J

    iput v1, p0, Landroid/os/statistics/Dex2oatEvent;->mScheduler:I

    iput v1, p0, Landroid/os/statistics/Dex2oatEvent;->mNice:I

    iput v1, p0, Landroid/os/statistics/Dex2oatEvent;->mCGroup:I

    iput-wide v2, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunningTimeMills:J

    iput-wide v2, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunnableTimeMills:J

    return-void
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/MacroscopicEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "dexFiles"

    iget-object v2, p0, Landroid/os/statistics/Dex2oatEvent;->mDexFiles:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "threadCount"

    iget v2, p0, Landroid/os/statistics/Dex2oatEvent;->mThreadCount:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "beginTime"

    iget-wide v2, p0, Landroid/os/statistics/Dex2oatEvent;->mBeginUpTimeMills:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "endTime"

    iget-wide v2, p0, Landroid/os/statistics/Dex2oatEvent;->mEndUpTimeMills:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "policy"

    iget v2, p0, Landroid/os/statistics/Dex2oatEvent;->mScheduler:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "priority"

    iget v2, p0, Landroid/os/statistics/Dex2oatEvent;->mNice:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "schedGroup"

    iget v2, p0, Landroid/os/statistics/Dex2oatEvent;->mCGroup:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "runningTime"

    iget-wide v2, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunningTimeMills:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "runnableTime"

    iget-wide v2, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunnableTimeMills:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/os/statistics/MacroscopicEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Landroid/os/statistics/Dex2oatEvent;->mDexFiles:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Landroid/os/statistics/Dex2oatEvent;->mThreadCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mBeginUpTimeMills:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mEndUpTimeMills:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Landroid/os/statistics/Dex2oatEvent;->mScheduler:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/statistics/Dex2oatEvent;->mNice:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/statistics/Dex2oatEvent;->mCGroup:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunningTimeMills:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Landroid/os/statistics/Dex2oatEvent;->mCPURunnableTimeMills:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
