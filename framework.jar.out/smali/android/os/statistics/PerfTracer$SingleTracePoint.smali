.class public Landroid/os/statistics/PerfTracer$SingleTracePoint;
.super Landroid/os/statistics/MicroscopicEvent;
.source "PerfTracer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/statistics/PerfTracer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SingleTracePoint"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/PerfTracer$SingleTracePoint$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/statistics/PerfTracer$SingleTracePoint;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIELD_LEVEL:Ljava/lang/String; = "level"

.field private static final FIELD_NUMBER_VALUE:Ljava/lang/String; = "numberValue"

.field private static final FIELD_REASON:Ljava/lang/String; = "reason"

.field private static final FIELD_TAG:Ljava/lang/String; = "tag"

.field private static final FIELD_TEXT_VALUE:Ljava/lang/String; = "textValue"


# instance fields
.field public level:I

.field public numberValue:J

.field private objectValue:Ljava/lang/Object;

.field public tag:Ljava/lang/String;

.field public textValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/os/statistics/PerfTracer$SingleTracePoint$1;

    invoke-direct {v0}, Landroid/os/statistics/PerfTracer$SingleTracePoint$1;-><init>()V

    sput-object v0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "TracePoint"

    const/4 v1, 0x6

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MicroscopicEvent;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method private copyFrom(Landroid/os/statistics/PerfTracer$SingleTracePoint;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget v0, p1, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    iput v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    iget-object v0, p1, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    iget-wide v0, p1, Landroid/os/statistics/PerfTracer$SingleTracePoint;->numberValue:J

    iput-wide v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->numberValue:J

    iget-object v0, p1, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/PerfTracer$SingleTracePoint;->objectValue:Ljava/lang/Object;

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->objectValue:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/PerfTracer$SingleTracePoint;

    invoke-direct {p0, p1}, Landroid/os/statistics/PerfTracer$SingleTracePoint;->copyFrom(Landroid/os/statistics/PerfTracer$SingleTracePoint;)V

    return-void
.end method

.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->threadId:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->threadName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->beginUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->endUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Landroid/os/statistics/OsUtils;->decodeThreadSchedulePolicy(I)I

    move-result v1

    iput v1, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->schedPolicy:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->schedPriority:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->schedGroup:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->runningTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->runnableTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->sleepingTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->endRealTimeMs:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    iget v1, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->eventFlags:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->numberValue:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readObject()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->objectValue:Ljava/lang/Object;

    return-void
.end method

.method public hasMultiplePeerBlockingEvents()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBy(Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockedBySameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingMultiplePeer()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingSameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRootEvent()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method isUserPerceptible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "level"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    const-string/jumbo v0, "tag"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    const-string/jumbo v0, "numberValue"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->numberValue:J

    const-string/jumbo v0, "textValue"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->objectValue:Ljava/lang/Object;

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->numberValue:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    :cond_1
    iput-object v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->objectValue:Ljava/lang/Object;

    return-void
.end method

.method public reset()V
    .locals 2

    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->reset()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->numberValue:J

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->objectValue:Ljava/lang/Object;

    return-void
.end method

.method resolveLazyInfo()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->lazyInfoResolved:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->resolveLazyInfo()V

    iget-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->objectValue:Ljava/lang/Object;

    if-nez v0, :cond_2

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    :cond_1
    iput-object v1, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->objectValue:Ljava/lang/Object;

    return-void

    :cond_2
    iget-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->objectValue:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    goto :goto_0
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "level"

    iget v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "reason"

    iget-object v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "tag"

    iget-object v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "numberValue"

    iget-wide v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->numberValue:J

    invoke-virtual {p1, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string/jumbo v1, "textValue"

    iget-object v2, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/os/statistics/MicroscopicEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->level:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->tag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->numberValue:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Landroid/os/statistics/PerfTracer$SingleTracePoint;->textValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
