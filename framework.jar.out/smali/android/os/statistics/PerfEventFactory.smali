.class public Landroid/os/statistics/PerfEventFactory;
.super Ljava/lang/Object;
.source "PerfEventFactory.java"


# static fields
.field private static final MACRO_EVENT_TYPE_INDEX_OFFSET:I = -0xfff4


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createPerfEvent(I)Landroid/os/statistics/PerfEvent;
    .locals 3

    const/4 v1, 0x0

    const/high16 v2, 0x10000

    if-ge p0, v2, :cond_0

    move v0, p0

    :goto_0
    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    return-object v1

    :cond_0
    const v2, -0xfff4

    add-int v0, p0, v2

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;

    invoke-direct {v1}, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;-><init>()V

    goto :goto_1

    :pswitch_2
    new-instance v1, Landroid/os/statistics/MonitorSuperviser$SingleLockWait;

    invoke-direct {v1}, Landroid/os/statistics/MonitorSuperviser$SingleLockWait;-><init>()V

    goto :goto_1

    :pswitch_3
    new-instance v1, Landroid/os/statistics/MonitorSuperviser$SingleConditionAwaken;

    invoke-direct {v1}, Landroid/os/statistics/MonitorSuperviser$SingleConditionAwaken;-><init>()V

    goto :goto_1

    :pswitch_4
    new-instance v1, Landroid/os/statistics/MonitorSuperviser$SingleConditionWait;

    invoke-direct {v1}, Landroid/os/statistics/MonitorSuperviser$SingleConditionWait;-><init>()V

    goto :goto_1

    :pswitch_5
    new-instance v1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;

    invoke-direct {v1}, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;-><init>()V

    goto :goto_1

    :pswitch_6
    new-instance v1, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;

    invoke-direct {v1}, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;-><init>()V

    goto :goto_1

    :pswitch_7
    new-instance v1, Landroid/os/statistics/PerfTracer$SingleTracePoint;

    invoke-direct {v1}, Landroid/os/statistics/PerfTracer$SingleTracePoint;-><init>()V

    goto :goto_1

    :pswitch_8
    new-instance v1, Landroid/os/statistics/SystemTraceSuperviser$SingleSystemTraceEvent;

    invoke-direct {v1}, Landroid/os/statistics/SystemTraceSuperviser$SingleSystemTraceEvent;-><init>()V

    goto :goto_1

    :pswitch_9
    new-instance v1, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;

    invoke-direct {v1}, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;-><init>()V

    goto :goto_1

    :pswitch_a
    new-instance v1, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;

    invoke-direct {v1}, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;-><init>()V

    goto :goto_1

    :pswitch_b
    new-instance v1, Landroid/os/statistics/SingleJniMethod;

    invoke-direct {v1}, Landroid/os/statistics/SingleJniMethod;-><init>()V

    goto :goto_1

    :pswitch_c
    new-instance v1, Landroid/os/statistics/LooperOnce;

    invoke-direct {v1}, Landroid/os/statistics/LooperOnce;-><init>()V

    goto :goto_1

    :pswitch_d
    new-instance v1, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;

    invoke-direct {v1}, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;-><init>()V

    goto :goto_1

    :pswitch_e
    new-instance v1, Landroid/os/statistics/SingleJankRecord;

    invoke-direct {v1}, Landroid/os/statistics/SingleJankRecord;-><init>()V

    goto :goto_1

    :pswitch_f
    new-instance v1, Landroid/os/statistics/Dex2oatEvent;

    invoke-direct {v1}, Landroid/os/statistics/Dex2oatEvent;-><init>()V

    goto :goto_1

    :pswitch_10
    new-instance v1, Landroid/os/statistics/BinderSuperviser$BinderStarvation;

    invoke-direct {v1}, Landroid/os/statistics/BinderSuperviser$BinderStarvation;-><init>()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public static createPerfEvent(ILandroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)Landroid/os/statistics/PerfEvent;
    .locals 1

    invoke-static {p0}, Landroid/os/statistics/PerfEventFactory;->createPerfEvent(I)Landroid/os/statistics/PerfEvent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/statistics/PerfEvent;->fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V

    :cond_0
    return-object v0
.end method

.method public static createPerfEvent(Lorg/json/JSONObject;)Landroid/os/statistics/PerfEvent;
    .locals 20

    invoke-static/range {p0 .. p0}, Landroid/os/statistics/PerfEvent;->getPerfEventType(Lorg/json/JSONObject;)I

    move-result v4

    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v4, v0, :cond_0

    const/16 v19, 0x0

    return-object v19

    :cond_0
    packed-switch v4, :pswitch_data_0

    packed-switch v4, :pswitch_data_1

    :pswitch_0
    new-instance v19, Ljava/lang/IllegalArgumentException;

    invoke-direct/range {v19 .. v19}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v19

    :pswitch_1
    new-instance v14, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;

    invoke-direct {v14}, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/os/statistics/MonitorSuperviser$SingleLockHold;->readFromJson(Lorg/json/JSONObject;)V

    return-object v14

    :pswitch_2
    new-instance v15, Landroid/os/statistics/MonitorSuperviser$SingleLockWait;

    invoke-direct {v15}, Landroid/os/statistics/MonitorSuperviser$SingleLockWait;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Landroid/os/statistics/MonitorSuperviser$SingleLockWait;->readFromJson(Lorg/json/JSONObject;)V

    return-object v15

    :pswitch_3
    new-instance v8, Landroid/os/statistics/MonitorSuperviser$SingleConditionAwaken;

    invoke-direct {v8}, Landroid/os/statistics/MonitorSuperviser$SingleConditionAwaken;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Landroid/os/statistics/MonitorSuperviser$SingleConditionAwaken;->readFromJson(Lorg/json/JSONObject;)V

    return-object v8

    :pswitch_4
    new-instance v9, Landroid/os/statistics/MonitorSuperviser$SingleConditionWait;

    invoke-direct {v9}, Landroid/os/statistics/MonitorSuperviser$SingleConditionWait;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Landroid/os/statistics/MonitorSuperviser$SingleConditionWait;->readFromJson(Lorg/json/JSONObject;)V

    return-object v9

    :pswitch_5
    new-instance v6, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;

    invoke-direct {v6}, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->readFromJson(Lorg/json/JSONObject;)V

    return-object v6

    :pswitch_6
    new-instance v7, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;

    invoke-direct {v7}, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;->readFromJson(Lorg/json/JSONObject;)V

    return-object v7

    :pswitch_7
    new-instance v18, Landroid/os/statistics/PerfTracer$SingleTracePoint;

    invoke-direct/range {v18 .. v18}, Landroid/os/statistics/PerfTracer$SingleTracePoint;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/os/statistics/PerfTracer$SingleTracePoint;->readFromJson(Lorg/json/JSONObject;)V

    return-object v18

    :pswitch_8
    new-instance v17, Landroid/os/statistics/SystemTraceSuperviser$SingleSystemTraceEvent;

    invoke-direct/range {v17 .. v17}, Landroid/os/statistics/SystemTraceSuperviser$SingleSystemTraceEvent;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/os/statistics/SystemTraceSuperviser$SingleSystemTraceEvent;->readFromJson(Lorg/json/JSONObject;)V

    return-object v17

    :pswitch_9
    new-instance v16, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;

    invoke-direct/range {v16 .. v16}, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/os/statistics/LooperMessageSuperviser$SingleLooperMessage;->readFromJson(Lorg/json/JSONObject;)V

    return-object v16

    :pswitch_a
    new-instance v11, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;

    invoke-direct {v11}, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Landroid/os/statistics/InputEventSuperviser$SingleInputEvent;->readFromJson(Lorg/json/JSONObject;)V

    return-object v11

    :pswitch_b
    new-instance v13, Landroid/os/statistics/SingleJniMethod;

    invoke-direct {v13}, Landroid/os/statistics/SingleJniMethod;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/os/statistics/SingleJniMethod;->readFromJson(Lorg/json/JSONObject;)V

    return-object v13

    :pswitch_c
    new-instance v5, Landroid/os/statistics/LooperOnce;

    invoke-direct {v5}, Landroid/os/statistics/LooperOnce;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Landroid/os/statistics/LooperOnce;->readFromJson(Lorg/json/JSONObject;)V

    return-object v5

    :pswitch_d
    new-instance v10, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;

    invoke-direct {v10}, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/os/statistics/EventLogSuperviser$SingleEventLogItem;->readFromJson(Lorg/json/JSONObject;)V

    return-object v10

    :pswitch_e
    new-instance v12, Landroid/os/statistics/SingleJankRecord;

    invoke-direct {v12}, Landroid/os/statistics/SingleJankRecord;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Landroid/os/statistics/SingleJankRecord;->readFromJson(Lorg/json/JSONObject;)V

    return-object v12

    :pswitch_f
    new-instance v3, Landroid/os/statistics/Dex2oatEvent;

    invoke-direct {v3}, Landroid/os/statistics/Dex2oatEvent;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/os/statistics/Dex2oatEvent;->readFromJson(Lorg/json/JSONObject;)V

    return-object v3

    :pswitch_10
    new-instance v2, Landroid/os/statistics/BinderSuperviser$BinderStarvation;

    invoke-direct {v2}, Landroid/os/statistics/BinderSuperviser$BinderStarvation;-><init>()V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/os/statistics/BinderSuperviser$BinderStarvation;->readFromJson(Lorg/json/JSONObject;)V

    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x10000
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method
