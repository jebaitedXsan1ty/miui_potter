.class public Landroid/os/statistics/BinderSuperviser$SingleBinderCall;
.super Landroid/os/statistics/MicroscopicEvent;
.source "BinderSuperviser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/statistics/BinderSuperviser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SingleBinderCall"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/statistics/BinderSuperviser$SingleBinderCall$1;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/os/statistics/BinderSuperviser$SingleBinderCall;",
            ">;"
        }
    .end annotation
.end field

.field private static final FIELD_CODE:Ljava/lang/String; = "code"

.field private static final FIELD_INTERFACE_DESCRIPTOR:Ljava/lang/String; = "interface"

.field private static final FIELD_STACK:Ljava/lang/String; = "stack"

.field private static sBinderProxyClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private binder:Landroid/os/IBinder;

.field public code:I

.field public interfaceDescriptor:Ljava/lang/String;

.field private javaBackTrace:Ljava/lang/Object;

.field private nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

.field public stackTrace:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall$1;

    invoke-direct {v0}, Landroid/os/statistics/BinderSuperviser$SingleBinderCall$1;-><init>()V

    sput-object v0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "BinderCall"

    const/4 v1, 0x4

    invoke-direct {p0, v1, v0}, Landroid/os/statistics/MicroscopicEvent;-><init>(ILjava/lang/String;)V

    return-void
.end method

.method private copyFrom(Landroid/os/statistics/BinderSuperviser$SingleBinderCall;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->copyFrom(Landroid/os/statistics/PerfEvent;)V

    iget-object v0, p1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    iget v0, p1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->code:I

    iput v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->code:I

    iget-object v0, p1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    iget-object v0, p1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->binder:Landroid/os/IBinder;

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->binder:Landroid/os/IBinder;

    iget-object v0, p1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->javaBackTrace:Ljava/lang/Object;

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->javaBackTrace:Ljava/lang/Object;

    iget-object v0, p1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    return-void
.end method

.method private static initClasses()V
    .locals 2

    sget-object v1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->sBinderProxyClass:Ljava/lang/Class;

    if-nez v1, :cond_0

    :try_start_0
    const-string/jumbo v1, "android.os.BinderProxy"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->sBinderProxyClass:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private static isJavaBinderCall([Ljava/lang/StackTraceElement;[Ljava/lang/Class;)Z
    .locals 3

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    array-length v2, p0

    if-nez v2, :cond_1

    :cond_0
    return v1

    :cond_1
    if-eqz p1, :cond_0

    array-length v2, p1

    if-eqz v2, :cond_0

    aget-object v0, p1, v1

    sget-object v2, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->sBinderProxyClass:Ljava/lang/Class;

    if-ne v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method


# virtual methods
.method public copyFrom(Landroid/os/statistics/PerfEvent;)V
    .locals 0

    check-cast p1, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;

    invoke-direct {p0, p1}, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->copyFrom(Landroid/os/statistics/BinderSuperviser$SingleBinderCall;)V

    return-void
.end method

.method fillIn(Landroid/os/statistics/JniParcel;Ljava/lang/Object;Landroid/os/statistics/NativeBackTrace;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->threadId:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->threadName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->beginUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->endUptimeMillis:J

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Landroid/os/statistics/OsUtils;->decodeThreadSchedulePolicy(I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->schedPolicy:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->schedPriority:I

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readBinder()Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->binder:Landroid/os/IBinder;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/statistics/JniParcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->code:I

    iput-object p2, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->javaBackTrace:Ljava/lang/Object;

    iput-object p3, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    return-void
.end method

.method public hasMultiplePeerBlockingEvents()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method hasNativeStack()Z
    .locals 1

    iget-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isBlockedBy(Landroid/os/statistics/MicroscopicEvent;)Z
    .locals 6

    const/4 v1, 0x0

    instance-of v2, p1, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;

    if-nez v2, :cond_0

    return v1

    :cond_0
    move-object v0, p1

    check-cast v0, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;

    iget v2, v0, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;->callingPid:I

    iget v3, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->pid:I

    if-ne v2, v3, :cond_1

    iget v2, v0, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;->callingUid:I

    iget v3, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->uid:I

    if-ne v2, v3, :cond_1

    iget-wide v2, v0, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;->beginUptimeMillis:J

    iget-wide v4, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->beginUptimeMillis:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    iget-wide v2, v0, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;->endUptimeMillis:J

    iget-wide v4, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->endUptimeMillis:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    iget v2, v0, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;->code:I

    iget v3, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->code:I

    if-ne v2, v3, :cond_1

    iget-object v1, v0, Landroid/os/statistics/BinderSuperviser$SingleBinderExecution;->interfaceDescriptor:Ljava/lang/String;

    iget-object v2, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    :cond_1
    return v1
.end method

.method public isBlockedBySameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingMultiplePeer()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBlockingSameProcess()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isPeerBlockingEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRootEvent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public readFromJson(Lorg/json/JSONObject;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromJson(Lorg/json/JSONObject;)V

    const-string/jumbo v0, "interface"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    const-string/jumbo v0, "code"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->code:I

    const-string/jumbo v0, "stack"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-static {v0}, Landroid/os/statistics/StackUtils;->getStackTrace(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    iput-object v2, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->binder:Landroid/os/IBinder;

    iput-object v2, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->javaBackTrace:Ljava/lang/Object;

    iput-object v2, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->readFromParcel(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->code:I

    invoke-static {p1}, Landroid/os/statistics/ParcelUtils;->readStringArray(Landroid/os/Parcel;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    iget-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/statistics/StackUtils;->emptyStack:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    :cond_1
    iput-object v1, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->binder:Landroid/os/IBinder;

    iput-object v1, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->javaBackTrace:Ljava/lang/Object;

    iput-object v1, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    return-void
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->reset()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->code:I

    sget-object v0, Landroid/os/statistics/StackUtils;->emptyStack:[Ljava/lang/String;

    iput-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    iput-object v1, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->binder:Landroid/os/IBinder;

    iput-object v1, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->javaBackTrace:Ljava/lang/Object;

    iput-object v1, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    return-void
.end method

.method resolveLazyInfo()V
    .locals 9

    const/4 v8, 0x0

    iget-boolean v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->lazyInfoResolved:Z

    if-eqz v7, :cond_0

    return-void

    :cond_0
    invoke-super {p0}, Landroid/os/statistics/MicroscopicEvent;->resolveLazyInfo()V

    invoke-static {}, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->initClasses()V

    iget-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->binder:Landroid/os/IBinder;

    if-eqz v7, :cond_1

    :try_start_0
    iget-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->binder:Landroid/os/IBinder;

    invoke-interface {v7}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    if-nez v7, :cond_2

    const-string/jumbo v7, ""

    iput-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    :cond_2
    iget-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->javaBackTrace:Ljava/lang/Object;

    invoke-static {v7}, Landroid/os/statistics/JavaBackTrace;->resolve(Ljava/lang/Object;)[Ljava/lang/StackTraceElement;

    move-result-object v4

    iget-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->javaBackTrace:Ljava/lang/Object;

    invoke-static {v7}, Landroid/os/statistics/JavaBackTrace;->resolveClasses(Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->isJavaBinderCall([Ljava/lang/StackTraceElement;[Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_6

    const/4 v2, 0x0

    :goto_1
    array-length v7, v3

    if-ge v2, v7, :cond_4

    aget-object v0, v3, v2

    sget-object v7, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->sBinderProxyClass:Ljava/lang/Class;

    if-ne v0, v7, :cond_3

    aput-object v8, v3, v2

    aput-object v8, v4, v2

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    invoke-static {v4, v3, v8}, Landroid/os/statistics/StackUtils;->getStackTrace([Ljava/lang/StackTraceElement;[Ljava/lang/Class;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    :goto_2
    iput-object v8, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->binder:Landroid/os/IBinder;

    iput-object v8, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->javaBackTrace:Ljava/lang/Object;

    iget-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    if-eqz v7, :cond_5

    iget-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    invoke-virtual {v7}, Landroid/os/statistics/NativeBackTrace;->dispose()V

    :cond_5
    iput-object v8, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    return-void

    :cond_6
    iget-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->nativeBackTrace:Landroid/os/statistics/NativeBackTrace;

    invoke-static {v7}, Landroid/os/statistics/NativeBackTrace;->resolve(Landroid/os/statistics/NativeBackTrace;)[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_8

    const/4 v2, 0x0

    :goto_3
    array-length v7, v6

    if-ge v2, v7, :cond_8

    aget-object v5, v6, v2

    if-eqz v5, :cond_8

    const-string/jumbo v7, "BinderSuperviser"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    const-string/jumbo v7, "libbinder.so"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    :cond_7
    aput-object v8, v6, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    invoke-static {v4, v3, v6}, Landroid/os/statistics/StackUtils;->getStackTrace([Ljava/lang/StackTraceElement;[Ljava/lang/Class;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public writeToJson(Lorg/json/JSONObject;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/os/statistics/MicroscopicEvent;->writeToJson(Lorg/json/JSONObject;)V

    :try_start_0
    const-string/jumbo v1, "interface"

    iget-object v2, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v1, "code"

    iget v2, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->code:I

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string/jumbo v1, "stack"

    iget-object v2, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    invoke-static {v2}, Lorg/json/JSONObject;->wrap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/os/statistics/MicroscopicEvent;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->interfaceDescriptor:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->code:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/os/statistics/BinderSuperviser$SingleBinderCall;->stackTrace:[Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/os/statistics/ParcelUtils;->writeStringArray(Landroid/os/Parcel;[Ljava/lang/String;)V

    return-void
.end method
