.class public Lcom/android/phone/networkmode/NMFilterDisableLte;
.super Lcom/android/phone/networkmode/NMFilter;
.source "NMFilterDisableLte.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Lcom/android/phone/networkmode/NMFilter;-><init>()V

    return-void
.end method


# virtual methods
.method public filter([[I)[[I
    .locals 2
    .param p1, "networkModes"    # [[I

    .prologue
    .line 7
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->isDisableLte()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 9
    aget-object v1, p1, v0

    invoke-static {v1}, Lcom/android/phone/networkmode/NMFilterDisableLte;->removeLte([I)[I

    move-result-object v1

    aput-object v1, p1, v0

    .line 8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12
    .end local v0    # "i":I
    :cond_0
    return-object p1
.end method
