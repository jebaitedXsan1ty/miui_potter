.class public Lcom/android/phone/networkmode/NetworkModeHandlerMtk;
.super Lcom/android/phone/networkmode/NetworkModeHandlerBase;
.source "NetworkModeHandlerMtk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected handleSetNetworkTypeDone(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 43
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 44
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    check-cast v2, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;

    .line 45
    .local v2, "settingArgs":Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v3, :cond_0

    .line 47
    const/4 v3, 0x2

    invoke-virtual {p0, v3, v2}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 48
    .local v1, "m":Landroid/os/Message;
    const-wide/16 v4, 0x7d0

    invoke-virtual {p0, v1, v4, v5}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 50
    .end local v1    # "m":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method protected handleSetNetworkTypeRetry(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 53
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;

    .line 54
    .local v1, "settingArgs":Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handleSetNetworkTypeRetry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 55
    iget v2, v1, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->times:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_0

    .line 56
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v2

    iget v3, v1, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->dataSlot:I

    if-eq v2, v3, :cond_1

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->getPreferredNetworkTypes()V

    .line 58
    return-void

    .line 62
    :cond_1
    iget v2, v1, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->times:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->times:I

    .line 63
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 64
    .local v0, "m":Landroid/os/Message;
    iget v2, v1, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->dataSlot:I

    invoke-static {v2}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iget v3, v1, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->mode:I

    invoke-virtual {v2, v3, v0}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    .line 65
    return-void
.end method

.method protected updateNetworkTypeIfNeeded()V
    .locals 14

    .prologue
    const/16 v13, 0x29

    const/4 v12, 0x1

    .line 68
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->isReadyToUpdate()Z

    move-result v10

    if-nez v10, :cond_0

    .line 69
    return-void

    .line 72
    :cond_0
    iget v5, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    .line 73
    .local v5, "oldDefaultSlotId":I
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v10

    iput v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    .line 74
    iget-object v6, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mVirtualSimIccId:Ljava/lang/String;

    .line 75
    .local v6, "oldVirtualSimIccid":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->getVirtualSimIccId()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mVirtualSimIccId:Ljava/lang/String;

    .line 76
    iget v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    if-ne v10, v5, :cond_2

    iget-object v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mVirtualSimIccId:Ljava/lang/String;

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    xor-int/lit8 v1, v10, 0x1

    .line 77
    :goto_0
    invoke-static {}, Lmiui/telephony/VirtualSimUtils;->getInstance()Lmiui/telephony/VirtualSimUtils;

    move-result-object v10

    invoke-virtual {v10}, Lmiui/telephony/VirtualSimUtils;->getVirtualSimSlotId()I

    move-result v9

    .line 78
    .local v9, "virtualSlot":I
    new-instance v10, Ljava/lang/StringBuilder;

    const/16 v11, 0x200

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string/jumbo v11, "updateNetworkTypeIfNeeded"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 79
    const-string/jumbo v11, " defaultSlotId="

    .line 78
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 79
    iget v11, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    .line 78
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 80
    const-string/jumbo v11, " (old="

    .line 78
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 81
    const-string/jumbo v11, " virtualSimIccId="

    .line 78
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 81
    sget-boolean v10, Lmiui/telephony/PhoneDebug;->VDBG:Z

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mVirtualSimIccId:Ljava/lang/String;

    .line 78
    :goto_1
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 82
    const-string/jumbo v11, " (old="

    .line 78
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 82
    sget-boolean v11, Lmiui/telephony/PhoneDebug;->VDBG:Z

    if-eqz v11, :cond_4

    .line 78
    .end local v6    # "oldVirtualSimIccid":Ljava/lang/String;
    :goto_2
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 83
    const-string/jumbo v11, " virtualSlot="

    .line 78
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 84
    .local v2, "logs":Ljava/lang/StringBuilder;
    iget-object v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mVirtualSimIccId:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    iget v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    if-eq v10, v9, :cond_5

    .line 86
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 87
    return-void

    .line 76
    .end local v2    # "logs":Ljava/lang/StringBuilder;
    .end local v9    # "virtualSlot":I
    .restart local v6    # "oldVirtualSimIccid":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x1

    .local v1, "isUpdate":Z
    goto :goto_0

    .line 81
    .end local v1    # "isUpdate":Z
    .restart local v9    # "virtualSlot":I
    :cond_3
    iget-object v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mVirtualSimIccId:Ljava/lang/String;

    invoke-static {v10}, Lmiui/telephony/TelephonyUtils;->pii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    .line 82
    :cond_4
    invoke-static {v6}, Lmiui/telephony/TelephonyUtils;->pii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 89
    .end local v6    # "oldVirtualSimIccid":Ljava/lang/String;
    .restart local v2    # "logs":Ljava/lang/StringBuilder;
    :cond_5
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v10

    invoke-static {v10}, Landroid/provider/MiuiSettings$VirtualSim;->isSupport4G(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 91
    const-string/jumbo v10, " support4G"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 92
    return-void

    .line 94
    :cond_6
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v10

    iget v11, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    invoke-static {v10, v11}, Lmiui/telephony/DefaultSimManager;->getPreferredNetworkModeFromDb(Landroid/content/Context;I)I

    move-result v0

    .line 95
    .local v0, "dbMode":I
    move v4, v0

    .line 96
    .local v4, "newDbMode":I
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->getInstance()Lcom/android/phone/networkmode/NetworkMode;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/phone/networkmode/NetworkMode;->getSupportModes()[[I

    move-result-object v8

    .line 97
    .local v8, "supportModes":[[I
    if-nez v1, :cond_7

    iget v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    aget-object v10, v8, v10

    invoke-static {v10, v0}, Lcom/android/internal/util/ArrayUtils;->contains([II)Z

    move-result v10

    xor-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_8

    .line 98
    :cond_7
    iget v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    aget-object v10, v8, v10

    const/4 v11, 0x0

    aget v4, v10, v11

    .line 100
    :cond_8
    const-string/jumbo v10, " dbMode="

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 101
    const-string/jumbo v11, " newDbMode="

    .line 100
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 102
    const-string/jumbo v11, " supportModes="

    .line 100
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 102
    iget v11, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    aget-object v11, v8, v11

    invoke-static {v11}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v11

    .line 100
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 104
    iget v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    invoke-static {v10, v4}, Lmiui/telephony/DefaultSimManager;->setNetworkModeInDb(II)V

    .line 106
    new-instance v7, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;

    iget v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    invoke-direct {v7, p0, v10}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;-><init>(Lcom/android/phone/networkmode/NetworkModeHandlerMtk;I)V

    .line 107
    .local v7, "settingArgs":Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "setPreferredNetworkType "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0, v12}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->removeMessages(I)V

    .line 109
    const/4 v10, 0x2

    invoke-virtual {p0, v10}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->removeMessages(I)V

    .line 110
    invoke-virtual {p0, v12, v7}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 111
    .local v3, "msg":Landroid/os/Message;
    iget v10, p0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;->mDefaultDataSlotId:I

    invoke-static {v10}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v10

    iget v11, v7, Lcom/android/phone/networkmode/NetworkModeHandlerMtk$SettingArgs;->mode:I

    invoke-virtual {v10, v11, v3}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    .line 112
    return-void
.end method
