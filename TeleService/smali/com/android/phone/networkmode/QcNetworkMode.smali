.class public Lcom/android/phone/networkmode/QcNetworkMode;
.super Lcom/android/phone/networkmode/NetworkMode;
.source "QcNetworkMode.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/android/phone/networkmode/NetworkMode;-><init>()V

    return-void
.end method

.method public static getDeviceMasterSupportModes()[I
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 93
    const/4 v0, 0x0

    .line 95
    .local v0, "supportModes":[I
    sget-boolean v1, Lmiui/os/Build;->IS_MITHREE:Z

    if-eqz v1, :cond_4

    .line 96
    sget-boolean v1, Lmiui/os/Build;->IS_MITHREE_TDSCDMA:Z

    if-eqz v1, :cond_1

    .line 98
    invoke-static {v4}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .line 139
    .local v0, "supportModes":[I
    :cond_0
    :goto_0
    return-object v0

    .line 99
    .local v0, "supportModes":[I
    :cond_1
    sget-boolean v1, Lmiui/os/Build;->IS_MITHREE_CDMA:Z

    if-eqz v1, :cond_3

    .line 101
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v1

    invoke-static {v1}, Lcom/android/phone/networkmode/QcNetworkMode;->getPhoneTypesAccordingSimType(I)I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 102
    invoke-static {v2}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .local v0, "supportModes":[I
    :cond_2
    invoke-static {v3}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .line 105
    .local v0, "supportModes":[I
    :cond_3
    invoke-static {v2}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .line 107
    .local v0, "supportModes":[I
    :cond_4
    sget-boolean v1, Lmiui/os/Build;->IS_MIFOUR:Z

    if-eqz v1, :cond_c

    .line 108
    sget-boolean v1, Lmiui/os/Build;->IS_MIFOUR_LTE_CM:Z

    if-eqz v1, :cond_5

    .line 110
    const/4 v1, 0x4

    invoke-static {v1}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .line 111
    .local v0, "supportModes":[I
    :cond_5
    sget-boolean v1, Lmiui/os/Build;->IS_MIFOUR_CDMA:Z

    if-eqz v1, :cond_7

    .line 113
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v1

    invoke-static {v1}, Lcom/android/phone/networkmode/QcNetworkMode;->getPhoneTypesAccordingSimType(I)I

    move-result v1

    if-ne v1, v2, :cond_6

    .line 114
    invoke-static {v2}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .local v0, "supportModes":[I
    :cond_6
    invoke-static {v3}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .line 115
    .local v0, "supportModes":[I
    :cond_7
    sget-boolean v1, Lmiui/os/Build;->IS_MIFOUR_LTE_CT:Z

    if-eqz v1, :cond_9

    .line 117
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v1

    invoke-static {v1}, Lcom/android/phone/networkmode/QcNetworkMode;->getPhoneTypesAccordingSimType(I)I

    move-result v1

    if-ne v1, v2, :cond_8

    .line 118
    invoke-static {v5}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .local v0, "supportModes":[I
    :cond_8
    const/4 v1, 0x6

    invoke-static {v1}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .line 119
    .local v0, "supportModes":[I
    :cond_9
    sget-boolean v1, Lmiui/os/Build;->IS_MIFOUR_LTE_CU:Z

    if-nez v1, :cond_a

    sget-boolean v1, Lmiui/os/Build;->IS_MIFOUR_LTE_INDIA:Z

    if-nez v1, :cond_a

    sget-boolean v1, Lmiui/os/Build;->IS_MIFOUR_LTE_SEASA:Z

    if-eqz v1, :cond_b

    .line 121
    :cond_a
    invoke-static {v5}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .line 124
    .local v0, "supportModes":[I
    :cond_b
    invoke-static {v2}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .line 126
    .local v0, "supportModes":[I
    :cond_c
    sget-boolean v1, Lmiui/os/Build;->IS_MITWO:Z

    if-eqz v1, :cond_10

    .line 127
    sget-boolean v1, Lmiui/os/Build;->IS_MITWO_TDSCDMA:Z

    if-eqz v1, :cond_d

    .line 128
    invoke-static {v4}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto :goto_0

    .line 129
    .local v0, "supportModes":[I
    :cond_d
    sget-boolean v1, Lmiui/os/Build;->IS_MITWO_CDMA:Z

    if-eqz v1, :cond_f

    .line 130
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v1

    invoke-static {v1}, Lcom/android/phone/networkmode/QcNetworkMode;->getPhoneTypesAccordingSimType(I)I

    move-result v1

    if-ne v1, v2, :cond_e

    .line 131
    invoke-static {v2}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto/16 :goto_0

    .local v0, "supportModes":[I
    :cond_e
    invoke-static {v3}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto/16 :goto_0

    .line 133
    .local v0, "supportModes":[I
    :cond_f
    invoke-static {v2}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto/16 :goto_0

    .line 135
    .local v0, "supportModes":[I
    :cond_10
    const-string/jumbo v1, "hammerhead"

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    const/16 v1, 0x8

    invoke-static {v1}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v0

    .local v0, "supportModes":[I
    goto/16 :goto_0
.end method


# virtual methods
.method public getDeviceSupportModes()[[I
    .locals 15

    .prologue
    const v14, 0x7f070087

    const/4 v13, 0x2

    const/4 v12, 0x0

    .line 20
    sget v10, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v9, v10, [[I

    .line 21
    .local v9, "supportModes":[[I
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v2

    .line 23
    .local v2, "defaultSlot":I
    sget v10, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    .line 24
    invoke-static {}, Lcom/android/phone/networkmode/QcNetworkMode;->getDeviceMasterSupportModes()[I

    move-result-object v10

    aput-object v10, v9, v12

    .line 25
    return-object v9

    .line 28
    :cond_0
    sget-boolean v10, Lmiui/os/Build;->IS_MITHREE:Z

    if-nez v10, :cond_1

    sget-boolean v10, Lmiui/os/Build;->IS_MIFOUR:Z

    if-eqz v10, :cond_4

    .line 30
    :cond_1
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    sget v10, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v5, v10, :cond_3

    .line 31
    if-ne v5, v2, :cond_2

    .line 32
    invoke-static {}, Lcom/android/phone/networkmode/QcNetworkMode;->getDeviceMasterSupportModes()[I

    move-result-object v10

    aput-object v10, v9, v5

    .line 30
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 34
    :cond_2
    invoke-static {v12}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v10

    aput-object v10, v9, v5

    goto :goto_1

    .line 37
    :cond_3
    return-object v9

    .line 40
    .end local v5    # "i":I
    :cond_4
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/phone/PhoneGlobals;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 41
    .local v8, "res":Landroid/content/res/Resources;
    sget-boolean v10, Lmiui/os/Build;->IS_HONGMI_TWOX_CT:Z

    if-eqz v10, :cond_8

    .line 43
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_2
    sget v10, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v5, v10, :cond_7

    .line 44
    if-nez v5, :cond_6

    .line 45
    invoke-static {v12}, Lcom/android/phone/networkmode/QcNetworkMode;->getPhoneTypesAccordingSimType(I)I

    move-result v6

    .line 46
    .local v6, "phoneType":I
    if-ne v6, v13, :cond_5

    .line 47
    const/4 v10, 0x6

    invoke-static {v10}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v10

    .line 46
    :goto_3
    aput-object v10, v9, v5

    .line 43
    .end local v6    # "phoneType":I
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 48
    .restart local v6    # "phoneType":I
    :cond_5
    invoke-virtual {v8, v14}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v10

    goto :goto_3

    .line 50
    .end local v6    # "phoneType":I
    :cond_6
    invoke-static {v12}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v10

    aput-object v10, v9, v5

    goto :goto_4

    .line 53
    :cond_7
    return-object v9

    .line 56
    .end local v5    # "i":I
    :cond_8
    const v10, 0x7f070088

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v4

    .line 57
    .local v4, "gsmVice":[I
    const v10, 0x7f070086

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    .line 58
    .local v1, "cdmaVice":[I
    invoke-virtual {v8, v14}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v3

    .line 59
    .local v3, "gsmMaster":[I
    const v10, 0x7f070085

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 60
    .local v0, "cdmaMaster":[I
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isHMH2xCmForThreeMode()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 61
    const/4 v10, 0x4

    invoke-static {v10}, Lcom/android/phone/networkmode/QcNetworkMode;->getSupportNetworkMode(I)[I

    move-result-object v3

    .line 62
    move-object v0, v3

    .line 68
    :cond_9
    :goto_5
    invoke-static {}, Lcom/android/phone/networkmode/QcNetworkMode;->getPhoneTypesAccordingSimType()[I

    move-result-object v7

    .line 69
    .local v7, "phoneTypes":[I
    aget v10, v7, v2

    if-ne v10, v13, :cond_c

    .line 70
    aput-object v0, v9, v2

    .line 71
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_6
    array-length v10, v7

    if-ge v5, v10, :cond_f

    .line 72
    if-eq v5, v2, :cond_a

    .line 73
    aput-object v4, v9, v5

    .line 71
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 63
    .end local v5    # "i":I
    .end local v7    # "phoneTypes":[I
    :cond_b
    sget-boolean v10, Lmiui/os/Build;->IS_MIFIVE:Z

    if-eqz v10, :cond_9

    const-string/jumbo v10, "LTE-X5"

    const-string/jumbo v11, "persist.radio.modem"

    invoke-static {v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 65
    move-object v0, v3

    goto :goto_5

    .line 78
    .restart local v7    # "phoneTypes":[I
    :cond_c
    aput-object v3, v9, v2

    .line 79
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_7
    array-length v10, v7

    if-ge v5, v10, :cond_f

    .line 80
    if-eq v5, v2, :cond_d

    .line 81
    aget v10, v7, v5

    if-ne v10, v13, :cond_e

    move-object v10, v1

    :goto_8
    aput-object v10, v9, v5

    .line 79
    :cond_d
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_e
    move-object v10, v4

    .line 81
    goto :goto_8

    .line 86
    :cond_f
    return-object v9
.end method

.method public isShowCdmaUnvailable()Z
    .locals 2

    .prologue
    .line 144
    const-string/jumbo v0, "ido"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    const-string/jumbo v0, "virgo"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "LTE-X5-ALL"

    const-string/jumbo v1, "persist.radio.modem"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 144
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 145
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
