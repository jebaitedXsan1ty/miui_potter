.class public Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;
.super Ljava/lang/Object;
.source "NetworkModeHandlerQc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/networkmode/NetworkModeHandlerQc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SettingArgs"
.end annotation


# instance fields
.field dataSlot:I

.field index:I

.field modes:[I

.field final synthetic this$0:Lcom/android/phone/networkmode/NetworkModeHandlerQc;

.field times:[I


# direct methods
.method constructor <init>(Lcom/android/phone/networkmode/NetworkModeHandlerQc;I)V
    .locals 3
    .param p1, "this$0"    # Lcom/android/phone/networkmode/NetworkModeHandlerQc;
    .param p2, "defaultData"    # I

    .prologue
    .line 39
    iput-object p1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->this$0:Lcom/android/phone/networkmode/NetworkModeHandlerQc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p2, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->dataSlot:I

    .line 41
    iget v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->dataSlot:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->index:I

    .line 42
    sget v1, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->times:[I

    .line 43
    sget v1, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->modes:[I

    .line 44
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v1, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->times:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 46
    iget-object v1, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->modes:[I

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    invoke-static {v2, v0}, Lmiui/telephony/DefaultSimManager;->getPreferredNetworkModeFromDb(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v0

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method getSlot()I
    .locals 2

    .prologue
    .line 51
    iget v0, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->index:I

    sget v1, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    rem-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->getSlot()I

    move-result v0

    .line 31
    .local v0, "slotId":I
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x100

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 32
    const-string/jumbo v2, "SettingArgs: currentSlot="

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 33
    const-string/jumbo v2, " mode="

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 33
    iget-object v2, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->modes:[I

    aget v2, v2, v0

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 34
    const-string/jumbo v2, " dataSlot="

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 34
    iget v2, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->dataSlot:I

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 35
    const-string/jumbo v2, " times="

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/android/phone/networkmode/NetworkModeHandlerQc$SettingArgs;->times:[I

    aget v2, v2, v0

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
