.class public Lcom/android/phone/networkmode/NetworkModeHandlerBase;
.super Landroid/os/Handler;
.source "NetworkModeHandlerBase.java"

# interfaces
.implements Lmiui/telephony/DefaultSimManager$DataSlotListener;


# static fields
.field public static sIsNetworkMccAbroad:Z


# instance fields
.field protected mDefaultDataSlotId:I

.field protected mVirtualSimIccId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->sIsNetworkMccAbroad:Z

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 42
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    iput v0, p0, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->mDefaultDataSlotId:I

    .line 43
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->getVirtualSimIccId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->mVirtualSimIccId:Ljava/lang/String;

    .line 44
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/telephony/DefaultSimManager;->addDataSlotListener(Lmiui/telephony/DefaultSimManager$DataSlotListener;)V

    .line 45
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->registerForNetworkAttached()V

    .line 46
    return-void
.end method


# virtual methods
.method protected getPreferredNetworkTypes()V
    .locals 5

    .prologue
    .line 158
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 159
    .local v1, "phones":[Lcom/android/internal/telephony/Phone;
    const/4 v2, 0x0

    array-length v3, v1

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .line 160
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/android/internal/telephony/Phone;->getPreferredNetworkType(Landroid/os/Message;)V

    .line 159
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 162
    .end local v0    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_0
    return-void
.end method

.method protected getVirtualSimIccId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    invoke-static {}, Lmiui/telephony/VirtualSimUtils;->getInstance()Lmiui/telephony/VirtualSimUtils;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/VirtualSimUtils;->isVirtualSimEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$VirtualSim;->getVirtualSimIccId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 169
    :cond_0
    const-string/jumbo v0, ""

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 71
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 87
    :goto_0
    return-void

    .line 73
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->updateNetworkTypeIfNeeded()V

    goto :goto_0

    .line 76
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->handleSetNetworkTypeDone(Landroid/os/Message;)V

    goto :goto_0

    .line 79
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->handleSetNetworkTypeRetry(Landroid/os/Message;)V

    goto :goto_0

    .line 82
    :pswitch_3
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->setPreferredNetworkModeForCmcc()V

    goto :goto_0

    .line 71
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected handleSetNetworkTypeDone(Landroid/os/Message;)V
    .locals 0
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 90
    return-void
.end method

.method protected handleSetNetworkTypeRetry(Landroid/os/Message;)V
    .locals 0
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 93
    return-void
.end method

.method public isNetworkMccAbroad()Z
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 114
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v3, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v3, :cond_1

    .line 115
    invoke-static {v0}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/PhoneProxy;->getServiceStateTracker(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v3

    iget-object v3, v3, Lcom/android/internal/telephony/ServiceStateTracker;->mSS:Landroid/telephony/ServiceState;

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "operatorNumeric":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_0

    .line 117
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 118
    .local v1, "networkMcc":Ljava/lang/String;
    const-string/jumbo v3, "000"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "460"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 119
    const/4 v3, 0x1

    return v3

    .line 114
    .end local v1    # "networkMcc":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    .end local v2    # "operatorNumeric":Ljava/lang/String;
    :cond_1
    return v4
.end method

.method protected isReadyToUpdate()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 127
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v2

    .line 128
    .local v2, "subInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 129
    const-string/jumbo v3, "isReadyToUpdate return for empty subinfolist"

    invoke-static {v3}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 130
    return v6

    .line 133
    :cond_0
    const/4 v1, 0x0

    .line 134
    .local v1, "needWait":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v3, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v3, :cond_5

    .line 135
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lmiui/telephony/DefaultSimManager;->isCardPresent(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 136
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 137
    invoke-static {v0}, Lcom/android/phone/NetworkModeManager;->getPhoneType(I)I

    move-result v3

    if-nez v3, :cond_2

    .line 139
    :cond_1
    const/4 v1, 0x1

    .line 147
    :cond_2
    :goto_1
    if-eqz v1, :cond_4

    .line 148
    const-string/jumbo v3, "isReadyToUpdate return false"

    invoke-static {v3}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0, v6}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->removeMessages(I)V

    .line 150
    const-wide/16 v4, 0x7d0

    invoke-virtual {p0, v6, v4, v5}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->sendEmptyMessageDelayed(IJ)Z

    .line 151
    return v6

    .line 142
    :cond_3
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 144
    const/4 v1, 0x1

    goto :goto_1

    .line 134
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_5
    const/4 v3, 0x1

    return v3
.end method

.method public onDataSlotReady(Z)V
    .locals 1
    .param p1, "isDataSlotChanged"    # Z

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0, v0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->removeMessages(I)V

    .line 62
    invoke-virtual {p0, v0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->sendEmptyMessage(I)Z

    .line 63
    return-void
.end method

.method public registerForNetworkAttached()V
    .locals 5

    .prologue
    .line 49
    sget-boolean v3, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/phone/NetworkModeManager;->isDeviceLockViceNetworkModeForCmcc()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    .line 50
    :cond_0
    return-void

    .line 52
    :cond_1
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 53
    .local v1, "phones":[Lcom/android/internal/telephony/Phone;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_2

    .line 54
    aget-object v3, v1, v0

    invoke-static {v3}, Lcom/android/phone/PhoneProxy;->getServiceStateTracker(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v2

    .line 55
    .local v2, "serviceStateTracker":Lcom/android/internal/telephony/ServiceStateTracker;
    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v2, p0, v3, v4}, Lcom/android/internal/telephony/ServiceStateTracker;->registerForNetworkAttached(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    .end local v2    # "serviceStateTracker":Lcom/android/internal/telephony/ServiceStateTracker;
    :cond_2
    return-void
.end method

.method protected setPreferredNetworkModeForCmcc()V
    .locals 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->isNetworkMccAbroad()Z

    move-result v0

    .line 100
    .local v0, "newIsNetworkMccAbroad":Z
    sget-boolean v1, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->sIsNetworkMccAbroad:Z

    if-ne v1, v0, :cond_0

    .line 101
    return-void

    .line 103
    :cond_0
    sput-boolean v0, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->sIsNetworkMccAbroad:Z

    .line 106
    sget v1, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    invoke-static {}, Lcom/android/phone/DefaultSlotSelectorImpl;->getDataSlotForCmcc()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set PreferredNetworkMode for cmcc sIsNetworkMccAbroad "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->sIsNetworkMccAbroad:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/NetworkModeManager;->log(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;->updateNetworkTypeIfNeeded()V

    .line 110
    :cond_1
    return-void
.end method

.method protected updateNetworkTypeIfNeeded()V
    .locals 0

    .prologue
    .line 96
    return-void
.end method
