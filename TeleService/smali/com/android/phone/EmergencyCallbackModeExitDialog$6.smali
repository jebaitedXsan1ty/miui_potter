.class Lcom/android/phone/EmergencyCallbackModeExitDialog$6;
.super Ljava/lang/Object;
.source "EmergencyCallbackModeExitDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/EmergencyCallbackModeExitDialog;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/EmergencyCallbackModeExitDialog;


# direct methods
.method constructor <init>(Lcom/android/phone/EmergencyCallbackModeExitDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/EmergencyCallbackModeExitDialog;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/EmergencyCallbackModeExitDialog$6;->this$0:Lcom/android/phone/EmergencyCallbackModeExitDialog;

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 240
    iget-object v0, p0, Lcom/android/phone/EmergencyCallbackModeExitDialog$6;->this$0:Lcom/android/phone/EmergencyCallbackModeExitDialog;

    invoke-static {v0}, Lcom/android/phone/EmergencyCallbackModeExitDialog;->-get1(Lcom/android/phone/EmergencyCallbackModeExitDialog;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->exitEmergencyCallbackMode()V

    .line 243
    iget-object v0, p0, Lcom/android/phone/EmergencyCallbackModeExitDialog$6;->this$0:Lcom/android/phone/EmergencyCallbackModeExitDialog;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/phone/EmergencyCallbackModeExitDialog;->showDialog(I)V

    .line 244
    iget-object v0, p0, Lcom/android/phone/EmergencyCallbackModeExitDialog$6;->this$0:Lcom/android/phone/EmergencyCallbackModeExitDialog;

    iget-object v0, v0, Lcom/android/phone/EmergencyCallbackModeExitDialog;->mTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 245
    return-void
.end method
