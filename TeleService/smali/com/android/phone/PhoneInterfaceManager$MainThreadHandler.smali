.class final Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;
.super Landroid/os/Handler;
.source "PhoneInterfaceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/PhoneInterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MainThreadHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/PhoneInterfaceManager;


# direct methods
.method private constructor <init>(Lcom/android/phone/PhoneInterfaceManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/PhoneInterfaceManager;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/PhoneInterfaceManager;
    .param p2, "-this1"    # Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;-><init>(Lcom/android/phone/PhoneInterfaceManager;)V

    return-void
.end method

.method private handleNullReturnEvent(Landroid/os/Message;Ljava/lang/String;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 1020
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 1021
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    check-cast v1, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 1022
    .local v1, "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v2, :cond_0

    .line 1023
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 1032
    :goto_0
    monitor-enter v1

    .line 1033
    :try_start_0
    invoke-virtual {v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    .line 1035
    return-void

    .line 1025
    :cond_0
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 1026
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v2, v2, Lcom/android/internal/telephony/CommandException;

    if-eqz v2, :cond_1

    .line 1027
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": CommandException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_0

    .line 1029
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": Unknown exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_0

    .line 1032
    :catchall_0
    move-exception v2

    monitor-exit v1

    throw v2
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 50
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 299
    move-object/from16 v0, p1

    iget v5, v0, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 1014
    :pswitch_0
    const-string/jumbo v5, "PhoneInterfaceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "MainThreadHandler: unexpected message code: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    iget v7, v0, Landroid/os/Message;->what:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    :cond_0
    :goto_0
    return-void

    .line 301
    :pswitch_1
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 302
    .local v39, "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap1(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/Phone;

    move-result-object v37

    .line 303
    .local v37, "phone":Lcom/android/internal/telephony/Phone;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    move-object/from16 v47, v0

    check-cast v47, Landroid/util/Pair;

    .line 304
    .local v47, "ussdObject":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Landroid/os/ResultReceiver;>;"
    move-object/from16 v0, v47

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v48, v0

    check-cast v48, Ljava/lang/String;

    .line 305
    .local v48, "ussdRequest":Ljava/lang/String;
    move-object/from16 v0, v47

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v49, v0

    check-cast v49, Landroid/os/ResultReceiver;

    .line 307
    .local v49, "wrappedCallback":Landroid/os/ResultReceiver;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    iget-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->subId:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v5, v6}, Lcom/android/phone/PhoneInterfaceManager;->-wrap0(Lcom/android/phone/PhoneInterfaceManager;I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 309
    const-string/jumbo v5, "PhoneInterfaceManager"

    const-string/jumbo v6, "handleUssdRequest: carrier does not support USSD apis."

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    new-instance v40, Landroid/telephony/UssdResponse;

    const/4 v5, 0x0

    move-object/from16 v0, v40

    move-object/from16 v1, v48

    invoke-direct {v0, v1, v5}, Landroid/telephony/UssdResponse;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 311
    .local v40, "response":Landroid/telephony/UssdResponse;
    new-instance v42, Landroid/os/Bundle;

    invoke-direct/range {v42 .. v42}, Landroid/os/Bundle;-><init>()V

    .line 312
    .local v42, "returnData":Landroid/os/Bundle;
    const-string/jumbo v5, "USSD_RESPONSE"

    move-object/from16 v0, v42

    move-object/from16 v1, v40

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 313
    const/4 v5, -0x1

    move-object/from16 v0, v49

    move-object/from16 v1, v42

    invoke-virtual {v0, v5, v1}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 315
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 316
    monitor-enter v39

    .line 317
    :try_start_0
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v39

    .line 319
    return-void

    .line 316
    :catchall_0
    move-exception v5

    monitor-exit v39

    throw v5

    .line 323
    .end local v40    # "response":Landroid/telephony/UssdResponse;
    .end local v42    # "returnData":Landroid/os/Bundle;
    :cond_1
    if-eqz v37, :cond_2

    .line 324
    :try_start_1
    move-object/from16 v0, v37

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->handleUssdRequest(Ljava/lang/String;Landroid/os/ResultReceiver;)Z

    move-result v5

    .line 323
    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;
    :try_end_1
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 330
    :goto_2
    monitor-enter v39

    .line 331
    :try_start_2
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .end local v37    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v47    # "ussdObject":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Landroid/os/ResultReceiver;>;"
    .end local v48    # "ussdRequest":Ljava/lang/String;
    .end local v49    # "wrappedCallback":Landroid/os/ResultReceiver;
    :goto_3
    monitor-exit v39

    goto/16 :goto_0

    .line 325
    .restart local v37    # "phone":Lcom/android/internal/telephony/Phone;
    .restart local v47    # "ussdObject":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Landroid/os/ResultReceiver;>;"
    .restart local v48    # "ussdRequest":Ljava/lang/String;
    .restart local v49    # "wrappedCallback":Landroid/os/ResultReceiver;
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 326
    :catch_0
    move-exception v26

    .line 327
    .local v26, "cse":Lcom/android/internal/telephony/CallStateException;
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    goto :goto_2

    .line 330
    .end local v26    # "cse":Lcom/android/internal/telephony/CallStateException;
    :catchall_1
    move-exception v5

    monitor-exit v39

    throw v5

    .line 337
    .end local v37    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    .end local v47    # "ussdObject":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Landroid/os/ResultReceiver;>;"
    .end local v48    # "ussdRequest":Ljava/lang/String;
    .end local v49    # "wrappedCallback":Landroid/os/ResultReceiver;
    :pswitch_2
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 338
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap1(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/Phone;

    move-result-object v37

    .line 339
    .restart local v37    # "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v37, :cond_3

    .line 340
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap1(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Lcom/android/internal/telephony/Phone;->handlePinMmi(Ljava/lang/String;)Z

    move-result v5

    .line 339
    :goto_4
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 343
    monitor-enter v39

    .line 344
    :try_start_3
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_3

    .line 343
    :catchall_2
    move-exception v5

    monitor-exit v39

    throw v5

    .line 341
    :cond_3
    const/4 v5, 0x0

    goto :goto_4

    .line 350
    .end local v37    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_3
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 351
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/4 v5, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 353
    .local v12, "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get1(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, Landroid/os/WorkSource;

    invoke-virtual {v6, v12, v5}, Lcom/android/internal/telephony/Phone;->getNeighboringCids(Landroid/os/Message;Landroid/os/WorkSource;)V

    goto/16 :goto_0

    .line 357
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_4
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 358
    .local v23, "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 359
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_4

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_4

    .line 360
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 366
    :goto_5
    monitor-enter v39

    .line 367
    :try_start_4
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto/16 :goto_3

    .line 366
    :catchall_3
    move-exception v5

    monitor-exit v39

    throw v5

    .line 363
    :cond_4
    new-instance v5, Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    goto :goto_5

    .line 372
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_5
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 373
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->subId:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v21

    .line 374
    .local v21, "answer_subId":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move/from16 v0, v21

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap4(Lcom/android/phone/PhoneInterfaceManager;I)V

    .line 375
    const-string/jumbo v5, ""

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 377
    monitor-enter v39

    .line 378
    :try_start_5
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    goto/16 :goto_3

    .line 377
    :catchall_4
    move-exception v5

    monitor-exit v39

    throw v5

    .line 383
    .end local v21    # "answer_subId":I
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_6
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 384
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->subId:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 386
    .local v27, "end_subId":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move/from16 v0, v27

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap2(Lcom/android/phone/PhoneInterfaceManager;I)Lcom/android/internal/telephony/Phone;

    move-result-object v37

    .line 387
    .restart local v37    # "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v37, :cond_0

    .line 391
    invoke-virtual/range {v37 .. v37}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v38

    .line 392
    .local v38, "phoneType":I
    const/4 v5, 0x2

    move/from16 v0, v38

    if-ne v0, v5, :cond_5

    .line 395
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move/from16 v0, v27

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap2(Lcom/android/phone/PhoneInterfaceManager;I)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneUtils;->hangupRingingAndActive(Lcom/android/internal/telephony/Phone;)Z

    move-result v30

    .line 403
    .local v30, "hungUp":Z
    :goto_6
    invoke-static/range {v30 .. v30}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 405
    monitor-enter v39

    .line 406
    :try_start_6
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    goto/16 :goto_3

    .line 405
    :catchall_5
    move-exception v5

    monitor-exit v39

    throw v5

    .line 396
    .end local v30    # "hungUp":Z
    :cond_5
    const/4 v5, 0x1

    move/from16 v0, v38

    if-ne v0, v5, :cond_6

    .line 398
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get0(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/CallManager;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneUtils;->hangup(Lcom/android/internal/telephony/CallManager;)Z

    move-result v30

    .restart local v30    # "hungUp":Z
    goto :goto_6

    .line 400
    .end local v30    # "hungUp":Z
    :cond_6
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Unexpected phone type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v38

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 411
    .end local v27    # "end_subId":I
    .end local v37    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v38    # "phoneType":I
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_7
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 412
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    move-object/from16 v32, v0

    check-cast v32, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;

    .line 413
    .local v32, "iccArgument":Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap3(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v4

    .line 414
    .local v4, "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    if-nez v4, :cond_7

    .line 415
    const-string/jumbo v5, "iccTransmitApduLogicalChannel: No UICC"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 416
    new-instance v6, Lcom/android/internal/telephony/uicc/IccIoResult;

    const/4 v5, 0x0

    check-cast v5, [B

    const/16 v7, 0x6f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 417
    monitor-enter v39

    .line 418
    :try_start_7
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    goto/16 :goto_3

    .line 417
    :catchall_6
    move-exception v5

    monitor-exit v39

    throw v5

    .line 421
    :cond_7
    const/16 v5, 0x8

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 424
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, v32

    iget v5, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->channel:I

    move-object/from16 v0, v32

    iget v6, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->cla:I

    move-object/from16 v0, v32

    iget v7, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->command:I

    .line 425
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->p1:I

    move-object/from16 v0, v32

    iget v9, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->p2:I

    move-object/from16 v0, v32

    iget v10, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->p3:I

    move-object/from16 v0, v32

    iget-object v11, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->data:Ljava/lang/String;

    .line 423
    invoke-virtual/range {v4 .. v12}, Lcom/android/internal/telephony/uicc/UiccCard;->iccTransmitApduLogicalChannel(IIIIIILjava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 431
    .end local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v32    # "iccArgument":Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_8
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 432
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 433
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_8

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_8

    .line 434
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 446
    :goto_7
    monitor-enter v39

    .line 447
    :try_start_8
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_7

    goto/16 :goto_3

    .line 446
    :catchall_7
    move-exception v5

    monitor-exit v39

    throw v5

    .line 436
    :cond_8
    new-instance v6, Lcom/android/internal/telephony/uicc/IccIoResult;

    const/4 v5, 0x0

    check-cast v5, [B

    const/16 v7, 0x6f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 437
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_9

    .line 438
    const-string/jumbo v5, "iccTransmitApduLogicalChannel: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_7

    .line 439
    :cond_9
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_a

    .line 440
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "iccTransmitApduLogicalChannel: CommandException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 441
    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 440
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_7

    .line 443
    :cond_a
    const-string/jumbo v5, "iccTransmitApduLogicalChannel: Unknown exception"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_7

    .line 452
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_9
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 453
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    move-object/from16 v32, v0

    check-cast v32, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;

    .line 454
    .restart local v32    # "iccArgument":Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap3(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v4

    .line 455
    .restart local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    if-nez v4, :cond_b

    .line 456
    const-string/jumbo v5, "iccTransmitApduBasicChannel: No UICC"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 457
    new-instance v6, Lcom/android/internal/telephony/uicc/IccIoResult;

    const/4 v5, 0x0

    check-cast v5, [B

    const/16 v7, 0x6f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 458
    monitor-enter v39

    .line 459
    :try_start_9
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_8

    goto/16 :goto_3

    .line 458
    :catchall_8
    move-exception v5

    monitor-exit v39

    throw v5

    .line 462
    :cond_b
    const/16 v5, 0x1e

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 465
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, v32

    iget v5, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->cla:I

    move-object/from16 v0, v32

    iget v6, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->command:I

    move-object/from16 v0, v32

    iget v7, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->p1:I

    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->p2:I

    .line 466
    move-object/from16 v0, v32

    iget v9, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->p3:I

    move-object/from16 v0, v32

    iget-object v10, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->data:Ljava/lang/String;

    move-object v11, v12

    .line 464
    invoke-virtual/range {v4 .. v11}, Lcom/android/internal/telephony/uicc/UiccCard;->iccTransmitApduBasicChannel(IIIIILjava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 471
    .end local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v32    # "iccArgument":Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_a
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 472
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 473
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_c

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_c

    .line 474
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 486
    :goto_8
    monitor-enter v39

    .line 487
    :try_start_a
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_9

    goto/16 :goto_3

    .line 486
    :catchall_9
    move-exception v5

    monitor-exit v39

    throw v5

    .line 476
    :cond_c
    new-instance v6, Lcom/android/internal/telephony/uicc/IccIoResult;

    const/4 v5, 0x0

    check-cast v5, [B

    const/16 v7, 0x6f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 477
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_d

    .line 478
    const-string/jumbo v5, "iccTransmitApduBasicChannel: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_8

    .line 479
    :cond_d
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_e

    .line 480
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "iccTransmitApduBasicChannel: CommandException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 481
    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 480
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_8

    .line 483
    :cond_e
    const-string/jumbo v5, "iccTransmitApduBasicChannel: Unknown exception"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_8

    .line 492
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_b
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 493
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    move-object/from16 v32, v0

    check-cast v32, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;

    .line 494
    .restart local v32    # "iccArgument":Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap3(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v4

    .line 495
    .restart local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    if-nez v4, :cond_f

    .line 496
    const-string/jumbo v5, "iccExchangeSimIO: No UICC"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 497
    new-instance v6, Lcom/android/internal/telephony/uicc/IccIoResult;

    const/4 v5, 0x0

    check-cast v5, [B

    const/16 v7, 0x6f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 498
    monitor-enter v39

    .line 499
    :try_start_b
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_a

    goto/16 :goto_3

    .line 498
    :catchall_a
    move-exception v5

    monitor-exit v39

    throw v5

    .line 502
    :cond_f
    const/16 v5, 0x20

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 504
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, v32

    iget v5, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->cla:I

    .line 505
    move-object/from16 v0, v32

    iget v6, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->command:I

    move-object/from16 v0, v32

    iget v7, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->p1:I

    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->p2:I

    move-object/from16 v0, v32

    iget v9, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->p3:I

    .line 506
    move-object/from16 v0, v32

    iget-object v10, v0, Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;->data:Ljava/lang/String;

    move-object v11, v12

    .line 504
    invoke-virtual/range {v4 .. v11}, Lcom/android/internal/telephony/uicc/UiccCard;->iccExchangeSimIO(IIIIILjava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 511
    .end local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v32    # "iccArgument":Lcom/android/phone/PhoneInterfaceManager$IccAPDUArgument;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_c
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 512
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 513
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_10

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_10

    .line 514
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 518
    :goto_9
    monitor-enter v39

    .line 519
    :try_start_c
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_b

    goto/16 :goto_3

    .line 518
    :catchall_b
    move-exception v5

    monitor-exit v39

    throw v5

    .line 516
    :cond_10
    new-instance v6, Lcom/android/internal/telephony/uicc/IccIoResult;

    const/4 v5, 0x0

    check-cast v5, [B

    const/16 v7, 0x6f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    goto :goto_9

    .line 524
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_d
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 525
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap3(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v4

    .line 526
    .restart local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    if-nez v4, :cond_11

    .line 527
    const-string/jumbo v5, "sendEnvelopeWithStatus: No UICC"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 528
    new-instance v6, Lcom/android/internal/telephony/uicc/IccIoResult;

    const/4 v5, 0x0

    check-cast v5, [B

    const/16 v7, 0x6f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 529
    monitor-enter v39

    .line 530
    :try_start_d
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_c

    goto/16 :goto_3

    .line 529
    :catchall_c
    move-exception v5

    monitor-exit v39

    throw v5

    .line 533
    :cond_11
    const/16 v5, 0x1a

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 534
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5, v12}, Lcom/android/internal/telephony/uicc/UiccCard;->sendEnvelopeWithStatus(Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 539
    .end local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_e
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 540
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 541
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_12

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_12

    .line 542
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 554
    :goto_a
    monitor-enter v39

    .line 555
    :try_start_e
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_d

    goto/16 :goto_3

    .line 554
    :catchall_d
    move-exception v5

    monitor-exit v39

    throw v5

    .line 544
    :cond_12
    new-instance v6, Lcom/android/internal/telephony/uicc/IccIoResult;

    const/4 v5, 0x0

    check-cast v5, [B

    const/16 v7, 0x6f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 545
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_13

    .line 546
    const-string/jumbo v5, "sendEnvelopeWithStatus: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_a

    .line 547
    :cond_13
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_14

    .line 548
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "sendEnvelopeWithStatus: CommandException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 549
    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 548
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_a

    .line 551
    :cond_14
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "sendEnvelopeWithStatus: exception:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_a

    .line 560
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_f
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 561
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap3(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v4

    .line 562
    .restart local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    move-object/from16 v35, v0

    check-cast v35, Landroid/util/Pair;

    .line 563
    .local v35, "openChannelArgs":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-nez v4, :cond_15

    .line 564
    const-string/jumbo v5, "iccOpenLogicalChannel: No UICC"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 565
    new-instance v5, Landroid/telephony/IccOpenLogicalChannelResponse;

    const/4 v6, -0x1

    .line 566
    const/4 v7, 0x2

    const/4 v8, 0x0

    .line 565
    invoke-direct {v5, v6, v7, v8}, Landroid/telephony/IccOpenLogicalChannelResponse;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 567
    monitor-enter v39

    .line 568
    :try_start_f
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_e

    goto/16 :goto_3

    .line 567
    :catchall_e
    move-exception v5

    monitor-exit v39

    throw v5

    .line 571
    :cond_15
    const/16 v5, 0xa

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 572
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, v35

    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    .line 573
    move-object/from16 v0, v35

    iget-object v6, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 572
    invoke-virtual {v4, v5, v6, v12}, Lcom/android/internal/telephony/uicc/UiccCard;->iccOpenLogicalChannel(Ljava/lang/String;ILandroid/os/Message;)V

    goto/16 :goto_0

    .line 578
    .end local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v35    # "openChannelArgs":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_10
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 579
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 581
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v25, -0x1

    .line 582
    .local v25, "channelId":I
    const/16 v44, 0x0

    .line 585
    .local v44, "selectResponse":[B
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_16

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v5, [I

    array-length v5, v5

    const/4 v6, 0x1

    if-lt v5, v6, :cond_16

    .line 586
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v41, v0

    check-cast v41, [I

    .line 587
    .local v41, "result":[I
    const/4 v5, 0x0

    aget v25, v41, v5

    .line 588
    move-object/from16 v0, v41

    array-length v5, v0

    const/4 v6, 0x1

    if-le v5, v6, :cond_16

    .line 589
    move-object/from16 v0, v41

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    new-array v0, v5, [B

    move-object/from16 v44, v0

    .line 590
    .local v44, "selectResponse":[B
    const/16 v31, 0x1

    .local v31, "i":I
    :goto_b
    move-object/from16 v0, v41

    array-length v5, v0

    move/from16 v0, v31

    if-ge v0, v5, :cond_16

    .line 591
    add-int/lit8 v5, v31, -0x1

    aget v6, v41, v31

    int-to-byte v6, v6

    aput-byte v6, v44, v5

    .line 590
    add-int/lit8 v31, v31, 0x1

    goto :goto_b

    .line 596
    .end local v31    # "i":I
    .end local v41    # "result":[I
    .end local v44    # "selectResponse":[B
    :cond_16
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_17

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_17

    .line 597
    const-string/jumbo v5, "PhoneInterfaceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "iccOpenLogicalChannel: success response "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    new-instance v36, Landroid/telephony/IccOpenLogicalChannelResponse;

    .line 599
    const/4 v5, 0x1

    .line 598
    move-object/from16 v0, v36

    move/from16 v1, v25

    move-object/from16 v2, v44

    invoke-direct {v0, v1, v5, v2}, Landroid/telephony/IccOpenLogicalChannelResponse;-><init>(II[B)V

    .line 623
    .local v36, "openChannelResp":Landroid/telephony/IccOpenLogicalChannelResponse;
    :goto_c
    move-object/from16 v0, v36

    move-object/from16 v1, v39

    iput-object v0, v1, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 624
    monitor-enter v39

    .line 625
    :try_start_10
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_f

    goto/16 :goto_3

    .line 624
    :catchall_f
    move-exception v5

    monitor-exit v39

    throw v5

    .line 603
    .end local v36    # "openChannelResp":Landroid/telephony/IccOpenLogicalChannelResponse;
    :cond_17
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_18

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v5, [I

    array-length v5, v5

    const/4 v6, 0x1

    if-ge v5, v6, :cond_19

    .line 604
    :cond_18
    const-string/jumbo v5, "iccOpenLogicalChannel: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 606
    :cond_19
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_1a

    .line 607
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "iccOpenLogicalChannel: Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 610
    :cond_1a
    const/16 v29, 0x4

    .line 611
    .local v29, "errorCode":I
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_1b

    .line 613
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v5, Lcom/android/internal/telephony/CommandException;

    invoke-virtual {v5}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v28

    .line 614
    .local v28, "error":Lcom/android/internal/telephony/CommandException$Error;
    sget-object v5, Lcom/android/internal/telephony/CommandException$Error;->MISSING_RESOURCE:Lcom/android/internal/telephony/CommandException$Error;

    move-object/from16 v0, v28

    if-ne v0, v5, :cond_1c

    .line 615
    const/16 v29, 0x2

    .line 620
    .end local v28    # "error":Lcom/android/internal/telephony/CommandException$Error;
    :cond_1b
    :goto_d
    new-instance v36, Landroid/telephony/IccOpenLogicalChannelResponse;

    move-object/from16 v0, v36

    move/from16 v1, v25

    move/from16 v2, v29

    move-object/from16 v3, v44

    invoke-direct {v0, v1, v2, v3}, Landroid/telephony/IccOpenLogicalChannelResponse;-><init>(II[B)V

    .restart local v36    # "openChannelResp":Landroid/telephony/IccOpenLogicalChannelResponse;
    goto :goto_c

    .line 616
    .end local v36    # "openChannelResp":Landroid/telephony/IccOpenLogicalChannelResponse;
    .restart local v28    # "error":Lcom/android/internal/telephony/CommandException$Error;
    :cond_1c
    sget-object v5, Lcom/android/internal/telephony/CommandException$Error;->NO_SUCH_ELEMENT:Lcom/android/internal/telephony/CommandException$Error;

    move-object/from16 v0, v28

    if-ne v0, v5, :cond_1b

    .line 617
    const/16 v29, 0x3

    goto :goto_d

    .line 630
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v25    # "channelId":I
    .end local v28    # "error":Lcom/android/internal/telephony/CommandException$Error;
    .end local v29    # "errorCode":I
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_11
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 631
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap3(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v4

    .line 632
    .restart local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    if-nez v4, :cond_1d

    .line 633
    const-string/jumbo v5, "iccCloseLogicalChannel: No UICC"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 634
    new-instance v6, Lcom/android/internal/telephony/uicc/IccIoResult;

    const/4 v5, 0x0

    check-cast v5, [B

    const/16 v7, 0x6f

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Lcom/android/internal/telephony/uicc/IccIoResult;-><init>(II[B)V

    move-object/from16 v0, v39

    iput-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 635
    monitor-enter v39

    .line 636
    :try_start_11
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_10

    goto/16 :goto_3

    .line 635
    :catchall_10
    move-exception v5

    monitor-exit v39

    throw v5

    .line 639
    :cond_1d
    const/16 v5, 0xc

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 640
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5, v12}, Lcom/android/internal/telephony/uicc/UiccCard;->iccCloseLogicalChannel(ILandroid/os/Message;)V

    goto/16 :goto_0

    .line 645
    .end local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_12
    const-string/jumbo v5, "iccCloseLogicalChannel"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->handleNullReturnEvent(Landroid/os/Message;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 649
    :pswitch_13
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 650
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0xe

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 651
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get1(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v6, v5, v12}, Lcom/android/internal/telephony/Phone;->nvReadItem(ILandroid/os/Message;)V

    goto/16 :goto_0

    .line 655
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_14
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 656
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 657
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_1e

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_1e

    .line 658
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 670
    :goto_e
    monitor-enter v39

    .line 671
    :try_start_12
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_11

    goto/16 :goto_3

    .line 670
    :catchall_11
    move-exception v5

    monitor-exit v39

    throw v5

    .line 660
    :cond_1e
    const-string/jumbo v5, ""

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 661
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_1f

    .line 662
    const-string/jumbo v5, "nvReadItem: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_e

    .line 663
    :cond_1f
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_20

    .line 664
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "nvReadItem: CommandException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 665
    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 664
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_e

    .line 667
    :cond_20
    const-string/jumbo v5, "nvReadItem: Unknown exception"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_e

    .line 676
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_15
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 677
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x10

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 678
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    move-object/from16 v33, v0

    check-cast v33, Landroid/util/Pair;

    .line 679
    .local v33, "idValue":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get1(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    move-object/from16 v0, v33

    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-object/from16 v0, v33

    iget-object v5, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v7, v5, v12}, Lcom/android/internal/telephony/Phone;->nvWriteItem(ILjava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 683
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v33    # "idValue":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_16
    const-string/jumbo v5, "nvWriteItem"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->handleNullReturnEvent(Landroid/os/Message;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 687
    :pswitch_17
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 688
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x12

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 689
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get1(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, [B

    invoke-virtual {v6, v5, v12}, Lcom/android/internal/telephony/Phone;->nvWriteCdmaPrl([BLandroid/os/Message;)V

    goto/16 :goto_0

    .line 693
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_18
    const-string/jumbo v5, "nvWriteCdmaPrl"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->handleNullReturnEvent(Landroid/os/Message;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 697
    :pswitch_19
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 698
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x14

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 699
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get1(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v6, v5, v12}, Lcom/android/internal/telephony/Phone;->nvResetConfig(ILandroid/os/Message;)V

    goto/16 :goto_0

    .line 703
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_1a
    const-string/jumbo v5, "nvResetConfig"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->handleNullReturnEvent(Landroid/os/Message;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 707
    :pswitch_1b
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 708
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x16

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 709
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap1(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/android/internal/telephony/Phone;->getPreferredNetworkType(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 713
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_1c
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 714
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 715
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_21

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_21

    .line 716
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 728
    :goto_f
    monitor-enter v39

    .line 729
    :try_start_13
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_12

    goto/16 :goto_3

    .line 728
    :catchall_12
    move-exception v5

    monitor-exit v39

    throw v5

    .line 718
    :cond_21
    const/4 v5, 0x0

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 719
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_22

    .line 720
    const-string/jumbo v5, "getPreferredNetworkType: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_f

    .line 721
    :cond_22
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_23

    .line 722
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "getPreferredNetworkType: CommandException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 723
    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 722
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_f

    .line 725
    :cond_23
    const-string/jumbo v5, "getPreferredNetworkType: Unknown exception"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_f

    .line 734
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_1d
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 735
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x18

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 736
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v34

    .line 737
    .local v34, "networkType":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap1(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    move/from16 v0, v34

    invoke-virtual {v5, v0, v12}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    goto/16 :goto_0

    .line 741
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v34    # "networkType":I
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_1e
    const-string/jumbo v5, "setPreferredNetworkType"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->handleNullReturnEvent(Landroid/os/Message;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 745
    :pswitch_1f
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 746
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x1c

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 747
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get1(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, [B

    invoke-virtual {v6, v5, v12}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_0

    .line 751
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_20
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 752
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 753
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    move-object/from16 v1, v39

    iput-object v0, v1, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 754
    monitor-enter v39

    .line 755
    :try_start_14
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_13

    goto/16 :goto_3

    .line 754
    :catchall_13
    move-exception v5

    monitor-exit v39

    throw v5

    .line 760
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_21
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 761
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x22

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 762
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    move-object/from16 v45, v0

    check-cast v45, Landroid/util/Pair;

    .line 763
    .local v45, "tagNum":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap1(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/Phone;

    move-result-object v7

    move-object/from16 v0, v45

    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v45

    iget-object v6, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v5, v6, v12}, Lcom/android/internal/telephony/Phone;->setVoiceMailNumber(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 768
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    .end local v45    # "tagNum":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :pswitch_22
    const-string/jumbo v5, "setVoicemailNumber"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->handleNullReturnEvent(Landroid/os/Message;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 772
    :pswitch_23
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 773
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x24

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 775
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap1(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/android/internal/telephony/Phone;->setNetworkSelectionModeAutomatic(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 779
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_24
    const-string/jumbo v5, "setNetworkSelectionModeAutomatic"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->handleNullReturnEvent(Landroid/os/Message;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 783
    :pswitch_25
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 784
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x28

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 785
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap1(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/android/internal/telephony/Phone;->getAvailableNetworks(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 789
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_26
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 790
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 792
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_24

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_24

    .line 793
    new-instance v24, Lcom/android/internal/telephony/CellNetworkScanResult;

    .line 795
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v5, Ljava/util/List;

    .line 794
    const/4 v6, 0x1

    .line 793
    move-object/from16 v0, v24

    invoke-direct {v0, v6, v5}, Lcom/android/internal/telephony/CellNetworkScanResult;-><init>(ILjava/util/List;)V

    .line 815
    .local v24, "cellScanResult":Lcom/android/internal/telephony/CellNetworkScanResult;
    :goto_10
    move-object/from16 v0, v24

    move-object/from16 v1, v39

    iput-object v0, v1, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 816
    monitor-enter v39

    .line 817
    :try_start_15
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_14

    goto/16 :goto_3

    .line 816
    :catchall_14
    move-exception v5

    monitor-exit v39

    throw v5

    .line 797
    .end local v24    # "cellScanResult":Lcom/android/internal/telephony/CellNetworkScanResult;
    :cond_24
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_25

    .line 798
    const-string/jumbo v5, "getCellNetworkScanResults: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 800
    :cond_25
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_26

    .line 801
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "getCellNetworkScanResults: Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 803
    :cond_26
    const/16 v29, 0x4

    .line 804
    .restart local v29    # "errorCode":I
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_27

    .line 806
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v5, Lcom/android/internal/telephony/CommandException;

    invoke-virtual {v5}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v28

    .line 807
    .restart local v28    # "error":Lcom/android/internal/telephony/CommandException$Error;
    sget-object v5, Lcom/android/internal/telephony/CommandException$Error;->RADIO_NOT_AVAILABLE:Lcom/android/internal/telephony/CommandException$Error;

    move-object/from16 v0, v28

    if-ne v0, v5, :cond_28

    .line 808
    const/16 v29, 0x2

    .line 813
    .end local v28    # "error":Lcom/android/internal/telephony/CommandException$Error;
    :cond_27
    :goto_11
    new-instance v24, Lcom/android/internal/telephony/CellNetworkScanResult;

    const/4 v5, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v29

    invoke-direct {v0, v1, v5}, Lcom/android/internal/telephony/CellNetworkScanResult;-><init>(ILjava/util/List;)V

    .restart local v24    # "cellScanResult":Lcom/android/internal/telephony/CellNetworkScanResult;
    goto :goto_10

    .line 809
    .end local v24    # "cellScanResult":Lcom/android/internal/telephony/CellNetworkScanResult;
    .restart local v28    # "error":Lcom/android/internal/telephony/CommandException$Error;
    :cond_28
    sget-object v5, Lcom/android/internal/telephony/CommandException$Error;->GENERIC_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    move-object/from16 v0, v28

    if-ne v0, v5, :cond_27

    .line 810
    const/16 v29, 0x3

    goto :goto_11

    .line 822
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v28    # "error":Lcom/android/internal/telephony/CommandException$Error;
    .end local v29    # "errorCode":I
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_27
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 824
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    move-object/from16 v43, v0

    check-cast v43, Lcom/android/phone/PhoneInterfaceManager$ManualNetworkSelectionArgument;

    .line 825
    .local v43, "selArg":Lcom/android/phone/PhoneInterfaceManager$ManualNetworkSelectionArgument;
    const/16 v5, 0x2a

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 827
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap1(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    move-object/from16 v0, v43

    iget-object v6, v0, Lcom/android/phone/PhoneInterfaceManager$ManualNetworkSelectionArgument;->operatorInfo:Lcom/android/internal/telephony/OperatorInfo;

    .line 828
    move-object/from16 v0, v43

    iget-boolean v7, v0, Lcom/android/phone/PhoneInterfaceManager$ManualNetworkSelectionArgument;->persistSelection:Z

    .line 827
    invoke-virtual {v5, v6, v7, v12}, Lcom/android/internal/telephony/Phone;->selectNetworkManually(Lcom/android/internal/telephony/OperatorInfo;ZLandroid/os/Message;)V

    goto/16 :goto_0

    .line 832
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    .end local v43    # "selArg":Lcom/android/phone/PhoneInterfaceManager$ManualNetworkSelectionArgument;
    :pswitch_28
    const-string/jumbo v5, "setNetworkSelectionModeManual"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->handleNullReturnEvent(Landroid/os/Message;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 836
    :pswitch_29
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 837
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x26

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 838
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get1(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/android/internal/telephony/Phone;->getModemActivityInfo(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 842
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_2a
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 843
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 844
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_2a

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_2a

    .line 845
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 857
    :goto_12
    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    if-nez v5, :cond_29

    .line 858
    new-instance v13, Landroid/telephony/ModemActivityInfo;

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-direct/range {v13 .. v20}, Landroid/telephony/ModemActivityInfo;-><init>(JII[III)V

    move-object/from16 v0, v39

    iput-object v13, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 860
    :cond_29
    monitor-enter v39

    .line 861
    :try_start_16
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_15

    goto/16 :goto_3

    .line 860
    :catchall_15
    move-exception v5

    monitor-exit v39

    throw v5

    .line 847
    :cond_2a
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_2b

    .line 848
    const-string/jumbo v5, "queryModemActivityInfo: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_12

    .line 849
    :cond_2b
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_2c

    .line 850
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "queryModemActivityInfo: CommandException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 851
    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 850
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_12

    .line 853
    :cond_2c
    const-string/jumbo v5, "queryModemActivityInfo: Unknown exception"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_12

    .line 866
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_2b
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 867
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x2c

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 868
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get1(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    .line 869
    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    check-cast v5, Ljava/util/List;

    .line 868
    invoke-virtual {v6, v5, v12}, Lcom/android/internal/telephony/Phone;->setAllowedCarriers(Ljava/util/List;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 874
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_2c
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 875
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 876
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_2e

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_2e

    .line 877
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 889
    :goto_13
    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    if-nez v5, :cond_2d

    .line 890
    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v6, -0x1

    const/4 v7, 0x0

    aput v6, v5, v7

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 892
    :cond_2d
    monitor-enter v39

    .line 893
    :try_start_17
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_16

    goto/16 :goto_3

    .line 892
    :catchall_16
    move-exception v5

    monitor-exit v39

    throw v5

    .line 879
    :cond_2e
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_2f

    .line 880
    const-string/jumbo v5, "setAllowedCarriers: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_13

    .line 881
    :cond_2f
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_30

    .line 882
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "setAllowedCarriers: CommandException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 883
    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 882
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_13

    .line 885
    :cond_30
    const-string/jumbo v5, "setAllowedCarriers: Unknown exception"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_13

    .line 898
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_2d
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 899
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    const/16 v5, 0x2e

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 900
    .restart local v12    # "onCompleted":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-get1(Lcom/android/phone/PhoneInterfaceManager;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/android/internal/telephony/Phone;->getAllowedCarriers(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 904
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_2e
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 905
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 906
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_32

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_32

    .line 907
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 919
    :goto_14
    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    if-nez v5, :cond_31

    .line 920
    new-instance v5, Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 922
    :cond_31
    monitor-enter v39

    .line 923
    :try_start_18
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_17

    goto/16 :goto_3

    .line 922
    :catchall_17
    move-exception v5

    monitor-exit v39

    throw v5

    .line 909
    :cond_32
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_33

    .line 910
    const-string/jumbo v5, "getAllowedCarriers: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_14

    .line 911
    :cond_33
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_34

    .line 912
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "getAllowedCarriers: CommandException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 913
    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 912
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_14

    .line 915
    :cond_34
    const-string/jumbo v5, "getAllowedCarriers: Unknown exception"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_14

    .line 928
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_2f
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 929
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 930
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_35

    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v5, :cond_35

    .line 931
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 941
    :goto_15
    monitor-enter v39

    .line 942
    :try_start_19
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_18

    goto/16 :goto_3

    .line 941
    :catchall_18
    move-exception v5

    monitor-exit v39

    throw v5

    .line 933
    :cond_35
    new-instance v5, Ljava/lang/IllegalArgumentException;

    .line 934
    const-string/jumbo v6, "Failed to retrieve Forbidden Plmns"

    .line 933
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 935
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_36

    .line 936
    const-string/jumbo v5, "getForbiddenPlmns: Empty response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_15

    .line 938
    :cond_36
    const-string/jumbo v5, "getForbiddenPlmns: Unknown exception"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_15

    .line 947
    .end local v23    # "ar":Landroid/os/AsyncResult;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_30
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 948
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap3(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v4

    .line 949
    .restart local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    if-nez v4, :cond_37

    .line 950
    const-string/jumbo v5, "getForbiddenPlmns() UiccCard is null"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 951
    new-instance v5, Ljava/lang/IllegalArgumentException;

    .line 952
    const-string/jumbo v6, "getForbiddenPlmns() UiccCard is null"

    .line 951
    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 953
    monitor-enter v39

    .line 954
    :try_start_1a
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_19

    goto/16 :goto_3

    .line 953
    :catchall_19
    move-exception v5

    monitor-exit v39

    throw v5

    .line 958
    :cond_37
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->argument:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Integer;

    .line 959
    .local v22, "appType":Ljava/lang/Integer;
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/internal/telephony/uicc/UiccCard;->getApplicationByType(I)Lcom/android/internal/telephony/uicc/UiccCardApplication;

    move-result-object v46

    .line 960
    .local v46, "uiccApp":Lcom/android/internal/telephony/uicc/UiccCardApplication;
    if-nez v46, :cond_38

    .line 961
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "getForbiddenPlmns() no app with specified type -- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 963
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "Failed to get UICC App"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 964
    monitor-enter v39

    .line 965
    :try_start_1b
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1a

    goto/16 :goto_3

    .line 964
    :catchall_1a
    move-exception v5

    monitor-exit v39

    throw v5

    .line 972
    :cond_38
    const/16 v5, 0x31

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 973
    .restart local v12    # "onCompleted":Landroid/os/Message;
    invoke-virtual/range {v46 .. v46}, Lcom/android/internal/telephony/uicc/UiccCardApplication;->getIccRecords()Lcom/android/internal/telephony/uicc/IccRecords;

    move-result-object v5

    check-cast v5, Lcom/android/internal/telephony/uicc/SIMRecords;

    invoke-virtual {v5, v12}, Lcom/android/internal/telephony/uicc/SIMRecords;->getForbiddenPlmns(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 978
    .end local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v22    # "appType":Ljava/lang/Integer;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    .end local v46    # "uiccApp":Lcom/android/internal/telephony/uicc/UiccCardApplication;
    :pswitch_31
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 979
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->this$0:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v0, v39

    invoke-static {v5, v0}, Lcom/android/phone/PhoneInterfaceManager;->-wrap3(Lcom/android/phone/PhoneInterfaceManager;Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v4

    .line 980
    .restart local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    if-nez v4, :cond_39

    .line 981
    const-string/jumbo v5, "getAtr: No UICC"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    .line 982
    const-string/jumbo v5, ""

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 983
    monitor-enter v39

    .line 984
    :try_start_1c
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1b

    goto/16 :goto_3

    .line 983
    :catchall_1b
    move-exception v5

    monitor-exit v39

    throw v5

    .line 987
    :cond_39
    const/16 v5, 0x33

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-virtual {v0, v5, v1}, Lcom/android/phone/PhoneInterfaceManager$MainThreadHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    .line 988
    .restart local v12    # "onCompleted":Landroid/os/Message;
    invoke-virtual {v4, v12}, Lcom/android/internal/telephony/uicc/UiccCard;->getAtr(Landroid/os/Message;)V

    goto/16 :goto_0

    .line 993
    .end local v4    # "uiccCard":Lcom/android/internal/telephony/uicc/UiccCard;
    .end local v12    # "onCompleted":Landroid/os/Message;
    .end local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    :pswitch_32
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v23, v0

    check-cast v23, Landroid/os/AsyncResult;

    .line 994
    .restart local v23    # "ar":Landroid/os/AsyncResult;
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    move-object/from16 v39, v0

    check-cast v39, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;

    .line 995
    .restart local v39    # "request":Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v5, :cond_3a

    .line 996
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 1008
    :goto_16
    monitor-enter v39

    .line 1009
    :try_start_1d
    invoke-virtual/range {v39 .. v39}, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->notifyAll()V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1c

    goto/16 :goto_3

    .line 1008
    :catchall_1c
    move-exception v5

    monitor-exit v39

    throw v5

    .line 998
    :cond_3a
    const-string/jumbo v5, ""

    move-object/from16 v0, v39

    iput-object v5, v0, Lcom/android/phone/PhoneInterfaceManager$MainThreadRequest;->result:Ljava/lang/Object;

    .line 999
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-nez v5, :cond_3b

    .line 1000
    const-string/jumbo v5, "ccExchangeSimIO: Empty Response"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_16

    .line 1001
    :cond_3b
    move-object/from16 v0, v23

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v5, v5, Lcom/android/internal/telephony/CommandException;

    if-eqz v5, :cond_3c

    .line 1002
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "iccTransmitApduBasicChannel: CommandException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1003
    move-object/from16 v0, v23

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 1002
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_16

    .line 1005
    :cond_3c
    const-string/jumbo v5, "iccTransmitApduBasicChannel: Unknown exception"

    invoke-static {v5}, Lcom/android/phone/PhoneInterfaceManager;->-wrap5(Ljava/lang/String;)V

    goto :goto_16

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_d
        :pswitch_e
        :pswitch_1f
        :pswitch_20
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_29
        :pswitch_2a
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_1
        :pswitch_30
        :pswitch_2f
        :pswitch_31
        :pswitch_32
    .end packed-switch
.end method
