.class public Lcom/android/phone/MobileDataPreference;
.super Landroid/preference/DialogPreference;
.source "MobileDataPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/MobileDataPreference$1;,
        Lcom/android/phone/MobileDataPreference$CellDataState;,
        Lcom/android/phone/MobileDataPreference$DataStateListener;
    }
.end annotation


# instance fields
.field public mChecked:Z

.field private final mListener:Lcom/android/phone/MobileDataPreference$DataStateListener;

.field public mMultiSimDialog:Z

.field public mSubId:I

.field private mSubscriptionManager:Landroid/telephony/SubscriptionManager;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static synthetic -wrap0(Lcom/android/phone/MobileDataPreference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MobileDataPreference;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/MobileDataPreference;->updateChecked()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    const v0, 0x101036d

    invoke-direct {p0, p1, p2, v0}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    .line 236
    new-instance v0, Lcom/android/phone/MobileDataPreference$1;

    invoke-direct {v0, p0}, Lcom/android/phone/MobileDataPreference$1;-><init>(Lcom/android/phone/MobileDataPreference;)V

    iput-object v0, p0, Lcom/android/phone/MobileDataPreference;->mListener:Lcom/android/phone/MobileDataPreference$DataStateListener;

    .line 58
    return-void
.end method

.method private disableDataForOtherSubscriptions(I)V
    .locals 6
    .param p1, "subId"    # I

    .prologue
    .line 211
    iget-object v3, p0, Lcom/android/phone/MobileDataPreference;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v3}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v2

    .line 212
    .local v2, "subInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubscriptionInfo;>;"
    if-eqz v2, :cond_1

    .line 213
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "subInfo$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    .line 214
    .local v0, "subInfo":Landroid/telephony/SubscriptionInfo;
    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v3

    if-eq v3, p1, :cond_0

    .line 215
    iget-object v3, p0, Lcom/android/phone/MobileDataPreference;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->setDataEnabled(IZ)V

    goto :goto_0

    .line 219
    .end local v0    # "subInfo":Landroid/telephony/SubscriptionInfo;
    .end local v1    # "subInfo$iterator":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method private setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/android/phone/MobileDataPreference;->mChecked:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 165
    :cond_0
    iput-boolean p1, p0, Lcom/android/phone/MobileDataPreference;->mChecked:Z

    .line 166
    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->notifyChanged()V

    .line 167
    return-void
.end method

.method private setMobileDataEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 159
    iget-object v0, p0, Lcom/android/phone/MobileDataPreference;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget v1, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-virtual {v0, v1, p1}, Landroid/telephony/TelephonyManager;->setDataEnabled(IZ)V

    .line 160
    invoke-direct {p0, p1}, Lcom/android/phone/MobileDataPreference;->setChecked(Z)V

    .line 161
    return-void
.end method

.method private showDisableDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 3
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-virtual {p1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 188
    const v1, 0x7f0b03b7

    .line 187
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 189
    const v1, 0x104000a

    .line 187
    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 190
    const/high16 v1, 0x1040000

    .line 187
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 191
    return-void
.end method

.method private showMultiSimDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 8
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v4, 0x0

    .line 194
    iget-object v3, p0, Lcom/android/phone/MobileDataPreference;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    iget v5, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-virtual {v3, v5}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v0

    .line 195
    .local v0, "currentSir":Landroid/telephony/SubscriptionInfo;
    iget-object v3, p0, Lcom/android/phone/MobileDataPreference;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v3}, Landroid/telephony/SubscriptionManager;->getDefaultDataSubscriptionInfo()Landroid/telephony/SubscriptionInfo;

    move-result-object v1

    .line 197
    .local v1, "nextSir":Landroid/telephony/SubscriptionInfo;
    if-nez v1, :cond_0

    .line 198
    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b03b8

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 201
    .local v2, "previousName":Ljava/lang/String;
    :goto_0
    const v3, 0x7f0b03b9

    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 202
    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/Object;

    .line 203
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v3

    :goto_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    aput-object v3, v6, v7

    .line 204
    const/4 v3, 0x1

    aput-object v2, v6, v3

    .line 202
    const v3, 0x7f0b03ba

    invoke-virtual {v5, v3, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 206
    const v3, 0x7f0b02f6

    invoke-virtual {p1, v3, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 207
    const v3, 0x7f0b02f3

    invoke-virtual {p1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 208
    return-void

    .line 199
    .end local v2    # "previousName":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "previousName":Ljava/lang/String;
    goto :goto_0

    :cond_1
    move-object v3, v4

    .line 203
    goto :goto_1
.end method

.method private updateChecked()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/phone/MobileDataPreference;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget v1, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getDataEnabled(I)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/phone/MobileDataPreference;->setChecked(Z)V

    .line 113
    return-void
.end method


# virtual methods
.method public initialize(I)V
    .locals 2
    .param p1, "subId"    # I

    .prologue
    .line 99
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "MobileDataPreference needs a SubscriptionInfo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/MobileDataPreference;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    .line 103
    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/MobileDataPreference;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 104
    iget v0, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    if-eq v0, p1, :cond_1

    .line 105
    iput p1, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/phone/MobileDataPreference;->setKey(Ljava/lang/String;)V

    .line 108
    :cond_1
    invoke-direct {p0}, Lcom/android/phone/MobileDataPreference;->updateChecked()V

    .line 109
    return-void
.end method

.method protected onAttachedToActivity()V
    .locals 4

    .prologue
    .line 85
    invoke-super {p0}, Landroid/preference/DialogPreference;->onAttachedToActivity()V

    .line 86
    iget-object v0, p0, Lcom/android/phone/MobileDataPreference;->mListener:Lcom/android/phone/MobileDataPreference$DataStateListener;

    iget v1, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/phone/MobileDataPreference$DataStateListener;->setListener(ZILandroid/content/Context;)V

    .line 87
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 171
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindView(Landroid/view/View;)V

    .line 172
    const v1, 0x1020040

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 173
    .local v0, "checkableView":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 174
    check-cast v0, Landroid/widget/Checkable;

    .end local v0    # "checkableView":Landroid/view/View;
    iget-boolean v1, p0, Lcom/android/phone/MobileDataPreference;->mChecked:Z

    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 175
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 223
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 224
    return-void

    .line 226
    :cond_0
    iget-boolean v0, p0, Lcom/android/phone/MobileDataPreference;->mMultiSimDialog:Z

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Lcom/android/phone/MobileDataPreference;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    iget v0, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->setDefaultDataSubId(I)V

    .line 228
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/phone/MobileDataPreference;->setMobileDataEnabled(Z)V

    .line 229
    iget v0, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-direct {p0, v0}, Lcom/android/phone/MobileDataPreference;->disableDataForOtherSubscriptions(I)V

    .line 234
    :goto_0
    return-void

    .line 232
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/phone/MobileDataPreference;->setMobileDataEnabled(Z)V

    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 1
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/android/phone/MobileDataPreference;->mMultiSimDialog:Z

    if-eqz v0, :cond_0

    .line 180
    invoke-direct {p0, p1}, Lcom/android/phone/MobileDataPreference;->showMultiSimDialog(Landroid/app/AlertDialog$Builder;)V

    .line 184
    :goto_0
    return-void

    .line 182
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/phone/MobileDataPreference;->showDisableDialog(Landroid/app/AlertDialog$Builder;)V

    goto :goto_0
.end method

.method protected onPrepareForRemoval()V
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/phone/MobileDataPreference;->mListener:Lcom/android/phone/MobileDataPreference$DataStateListener;

    iget v1, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v2}, Lcom/android/phone/MobileDataPreference$DataStateListener;->setListener(ZILandroid/content/Context;)V

    .line 92
    invoke-super {p0}, Landroid/preference/DialogPreference;->onPrepareForRemoval()V

    .line 93
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "s"    # Landroid/os/Parcelable;

    .prologue
    move-object v0, p1

    .line 62
    check-cast v0, Lcom/android/phone/MobileDataPreference$CellDataState;

    .line 63
    .local v0, "state":Lcom/android/phone/MobileDataPreference$CellDataState;
    invoke-virtual {v0}, Lcom/android/phone/MobileDataPreference$CellDataState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 64
    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/MobileDataPreference;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 65
    iget-boolean v1, v0, Lcom/android/phone/MobileDataPreference$CellDataState;->mChecked:Z

    iput-boolean v1, p0, Lcom/android/phone/MobileDataPreference;->mChecked:Z

    .line 66
    iget-boolean v1, v0, Lcom/android/phone/MobileDataPreference$CellDataState;->mMultiSimDialog:Z

    iput-boolean v1, p0, Lcom/android/phone/MobileDataPreference;->mMultiSimDialog:Z

    .line 67
    iget v1, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 68
    iget v1, v0, Lcom/android/phone/MobileDataPreference$CellDataState;->mSubId:I

    iput v1, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/phone/MobileDataPreference;->setKey(Ljava/lang/String;)V

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->notifyChanged()V

    .line 72
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/android/phone/MobileDataPreference$CellDataState;

    invoke-super {p0}, Landroid/preference/DialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/phone/MobileDataPreference$CellDataState;-><init>(Landroid/os/Parcelable;)V

    .line 77
    .local v0, "state":Lcom/android/phone/MobileDataPreference$CellDataState;
    iget-boolean v1, p0, Lcom/android/phone/MobileDataPreference;->mChecked:Z

    iput-boolean v1, v0, Lcom/android/phone/MobileDataPreference$CellDataState;->mChecked:Z

    .line 78
    iget-boolean v1, p0, Lcom/android/phone/MobileDataPreference;->mMultiSimDialog:Z

    iput-boolean v1, v0, Lcom/android/phone/MobileDataPreference$CellDataState;->mMultiSimDialog:Z

    .line 79
    iget v1, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    iput v1, v0, Lcom/android/phone/MobileDataPreference$CellDataState;->mSubId:I

    .line 80
    return-object v0
.end method

.method public performClick(Landroid/preference/PreferenceScreen;)V
    .locals 7
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 117
    invoke-virtual {p0}, Lcom/android/phone/MobileDataPreference;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-static {v3}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    .line 118
    :cond_0
    return-void

    .line 120
    :cond_1
    iget-object v3, p0, Lcom/android/phone/MobileDataPreference;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    .line 121
    iget v4, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    .line 120
    invoke-virtual {v3, v4}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v0

    .line 122
    .local v0, "currentSir":Landroid/telephony/SubscriptionInfo;
    iget-object v3, p0, Lcom/android/phone/MobileDataPreference;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v3}, Landroid/telephony/SubscriptionManager;->getDefaultDataSubscriptionInfo()Landroid/telephony/SubscriptionInfo;

    move-result-object v2

    .line 123
    .local v2, "nextSir":Landroid/telephony/SubscriptionInfo;
    iget-object v3, p0, Lcom/android/phone/MobileDataPreference;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimCount()I

    move-result v3

    if-le v3, v5, :cond_4

    const/4 v1, 0x1

    .line 124
    .local v1, "isMultiSim":Z
    :goto_0
    iget-boolean v3, p0, Lcom/android/phone/MobileDataPreference;->mChecked:Z

    if-eqz v3, :cond_6

    .line 127
    if-nez v1, :cond_2

    if-eqz v2, :cond_5

    if-eqz v0, :cond_5

    .line 128
    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v3

    invoke-virtual {v2}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v4

    if-ne v3, v4, :cond_5

    .line 129
    :cond_2
    invoke-direct {p0, v6}, Lcom/android/phone/MobileDataPreference;->setMobileDataEnabled(Z)V

    .line 130
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 131
    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v3

    invoke-virtual {v2}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 132
    iget v3, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-direct {p0, v3}, Lcom/android/phone/MobileDataPreference;->disableDataForOtherSubscriptions(I)V

    .line 134
    :cond_3
    return-void

    .line 123
    .end local v1    # "isMultiSim":Z
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "isMultiSim":Z
    goto :goto_0

    .line 138
    :cond_5
    iput-boolean v6, p0, Lcom/android/phone/MobileDataPreference;->mMultiSimDialog:Z

    .line 139
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    .line 155
    :goto_1
    return-void

    .line 142
    :cond_6
    if-eqz v1, :cond_8

    .line 143
    iput-boolean v5, p0, Lcom/android/phone/MobileDataPreference;->mMultiSimDialog:Z

    .line 144
    if-eqz v2, :cond_7

    if-eqz v0, :cond_7

    .line 145
    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v3

    invoke-virtual {v2}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v4

    if-ne v3, v4, :cond_7

    .line 146
    invoke-direct {p0, v5}, Lcom/android/phone/MobileDataPreference;->setMobileDataEnabled(Z)V

    .line 147
    iget v3, p0, Lcom/android/phone/MobileDataPreference;->mSubId:I

    invoke-direct {p0, v3}, Lcom/android/phone/MobileDataPreference;->disableDataForOtherSubscriptions(I)V

    .line 148
    return-void

    .line 150
    :cond_7
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    goto :goto_1

    .line 152
    :cond_8
    invoke-direct {p0, v5}, Lcom/android/phone/MobileDataPreference;->setMobileDataEnabled(Z)V

    goto :goto_1
.end method
