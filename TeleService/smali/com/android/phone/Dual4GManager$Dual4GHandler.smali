.class public Lcom/android/phone/Dual4GManager$Dual4GHandler;
.super Landroid/os/Handler;
.source "Dual4GManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/Dual4GManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "Dual4GHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/Dual4GManager;


# direct methods
.method public constructor <init>(Lcom/android/phone/Dual4GManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/Dual4GManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/android/phone/Dual4GManager$Dual4GHandler;->this$0:Lcom/android/phone/Dual4GManager;

    .line 86
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 87
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 91
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 93
    :pswitch_0
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v4, v4, Landroid/os/AsyncResult;

    if-nez v4, :cond_1

    .line 94
    return-void

    .line 96
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 97
    .local v0, "ar":Landroid/os/AsyncResult;
    const/4 v2, 0x0

    .line 98
    .local v2, "resultCode":I
    invoke-static {}, Lcom/android/phone/Dual4GManager;->-get0()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "EVENT_SET_VICE_NETWORK_TYPE_DONE exception="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v4, :cond_2

    .line 100
    const/4 v2, -0x1

    .line 102
    :cond_2
    iget-object v4, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    instance-of v4, v4, Lcom/android/phone/Dual4GManager$EnableDual4GParams;

    if-eqz v4, :cond_0

    .line 103
    iget-object v1, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    check-cast v1, Lcom/android/phone/Dual4GManager$EnableDual4GParams;

    .line 104
    .local v1, "params":Lcom/android/phone/Dual4GManager$EnableDual4GParams;
    if-nez v2, :cond_3

    .line 105
    iget-object v3, p0, Lcom/android/phone/Dual4GManager$Dual4GHandler;->this$0:Lcom/android/phone/Dual4GManager;

    iget-boolean v4, v1, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->enabled:Z

    invoke-virtual {v3, v4}, Lcom/android/phone/Dual4GManager;->notifyDual4GModeChanged(Z)V

    .line 110
    :goto_1
    iget-object v3, v1, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->callback:Lcom/android/phone/Dual4GManager$CallBack;

    if-eqz v3, :cond_0

    .line 111
    iget-object v3, v1, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->callback:Lcom/android/phone/Dual4GManager$CallBack;

    iget-boolean v4, v1, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->enabled:Z

    invoke-interface {v3, v4, v2}, Lcom/android/phone/Dual4GManager$CallBack;->onSetDual4GResult(ZI)V

    goto :goto_0

    .line 108
    :cond_3
    iget-object v4, p0, Lcom/android/phone/Dual4GManager$Dual4GHandler;->this$0:Lcom/android/phone/Dual4GManager;

    invoke-static {v4}, Lcom/android/phone/Dual4GManager;->-get1(Lcom/android/phone/Dual4GManager;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "dual_4g_mode_enabled"

    iget-boolean v6, v1, Lcom/android/phone/Dual4GManager$EnableDual4GParams;->enabled:Z

    if-eqz v6, :cond_4

    :goto_2
    invoke-static {v4, v5, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_4
    const/4 v3, 0x1

    goto :goto_2

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
