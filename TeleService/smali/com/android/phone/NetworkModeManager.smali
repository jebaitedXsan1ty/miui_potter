.class public Lcom/android/phone/NetworkModeManager;
.super Ljava/lang/Object;
.source "NetworkModeManager.java"


# static fields
.field private static sInstance:Lcom/android/phone/NetworkModeManager;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    sget-boolean v0, Lmiui/telephony/MiuiTelephony;->IS_QCOM:Z

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Lcom/android/phone/networkmode/NetworkModeHandlerQc;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NetworkModeHandlerQc;-><init>()V

    .line 56
    :goto_0
    return-void

    .line 51
    :cond_0
    sget-boolean v0, Lmiui/telephony/MiuiTelephony;->IS_MTK:Z

    if-eqz v0, :cond_1

    .line 52
    new-instance v0, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NetworkModeHandlerMtk;-><init>()V

    goto :goto_0

    .line 54
    :cond_1
    new-instance v0, Lcom/android/phone/networkmode/NetworkModeHandlerBase;

    invoke-direct {v0}, Lcom/android/phone/networkmode/NetworkModeHandlerBase;-><init>()V

    goto :goto_0
.end method

.method public static dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 81
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->getInstance()Lcom/android/phone/networkmode/NetworkMode;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/android/phone/networkmode/NetworkMode;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public static getPhoneType(I)I
    .locals 3
    .param p0, "slotId"    # I

    .prologue
    .line 59
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 60
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    .line 61
    .local v0, "ic":Lcom/android/internal/telephony/IccCard;
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_RUIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    invoke-interface {v0, v2}, Lcom/android/internal/telephony/IccCard;->isApplicationOnIcc(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 62
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_CSIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    invoke-interface {v0, v2}, Lcom/android/internal/telephony/IccCard;->isApplicationOnIcc(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Z

    move-result v2

    .line 61
    if-eqz v2, :cond_1

    .line 63
    :cond_0
    const/4 v2, 0x2

    return v2

    .line 64
    :cond_1
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_SIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    invoke-interface {v0, v2}, Lcom/android/internal/telephony/IccCard;->isApplicationOnIcc(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 65
    sget-object v2, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;->APPTYPE_USIM:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;

    invoke-interface {v0, v2}, Lcom/android/internal/telephony/IccCard;->isApplicationOnIcc(Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$AppType;)Z

    move-result v2

    .line 64
    if-eqz v2, :cond_3

    .line 66
    :cond_2
    const/4 v2, 0x1

    return v2

    .line 68
    :cond_3
    const/4 v2, 0x0

    return v2
.end method

.method public static isDeviceLockViceNetworkModeForCmcc()Z
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->isDeviceLockViceNetworkModeForCmcc()Z

    move-result v0

    return v0
.end method

.method public static isDisableLte()Z
    .locals 1

    .prologue
    .line 85
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->isDisableLte()Z

    move-result v0

    return v0
.end method

.method public static isNetworkModeLteOnly(I)Z
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 101
    invoke-static {p0}, Lcom/android/phone/networkmode/NetworkMode;->isNetworkModeLteOnly(I)Z

    move-result v0

    return v0
.end method

.method public static isRemoveNetworkModeSettings()Z
    .locals 1

    .prologue
    .line 73
    sget-boolean v0, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_CM_COOPERATION:Z

    if-eqz v0, :cond_1

    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_CTA_BUILD:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 74
    const/4 v0, 0x1

    return v0

    .line 77
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public static isShowCdmaUnavailable()Z
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->getInstance()Lcom/android/phone/networkmode/NetworkMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/phone/networkmode/NetworkMode;->isShowCdmaUnvailable()Z

    move-result v0

    return v0
.end method

.method public static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 108
    const-string/jumbo v0, "NetworkModeManager"

    invoke-static {v0, p0}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return-void
.end method

.method public static make()Lcom/android/phone/NetworkModeManager;
    .locals 2

    .prologue
    .line 32
    sget-object v0, Lcom/android/phone/NetworkModeManager;->sInstance:Lcom/android/phone/NetworkModeManager;

    if-nez v0, :cond_0

    .line 33
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->init()V

    .line 34
    new-instance v0, Lcom/android/phone/NetworkModeManager;

    invoke-direct {v0}, Lcom/android/phone/NetworkModeManager;-><init>()V

    sput-object v0, Lcom/android/phone/NetworkModeManager;->sInstance:Lcom/android/phone/NetworkModeManager;

    .line 38
    sget-object v0, Lcom/android/phone/NetworkModeManager;->sInstance:Lcom/android/phone/NetworkModeManager;

    return-object v0

    .line 36
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "DefaultCardManager.make() should only be called once"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static shouldShowViceNetworkTypePref()Z
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Lcom/android/phone/networkmode/NetworkMode;->shouldShowViceNetworkTypePref()Z

    move-result v0

    return v0
.end method
