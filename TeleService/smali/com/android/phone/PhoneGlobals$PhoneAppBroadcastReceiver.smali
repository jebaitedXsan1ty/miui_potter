.class Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhoneGlobals.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/PhoneGlobals;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhoneAppBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/PhoneGlobals;


# direct methods
.method private constructor <init>(Lcom/android/phone/PhoneGlobals;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    .line 714
    iput-object p1, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/PhoneGlobals;Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/PhoneGlobals;
    .param p2, "-this1"    # Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;-><init>(Lcom/android/phone/PhoneGlobals;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 717
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 718
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v9, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 719
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-virtual {v9}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 720
    const-string/jumbo v10, "airplane_mode_on"

    const/4 v11, 0x0

    .line 719
    invoke-static {v9, v10, v11}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 722
    .local v1, "airplaneMode":I
    if-eqz v1, :cond_0

    .line 723
    const/4 v1, 0x1

    .line 725
    :cond_0
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9, p1, v1}, Lcom/android/phone/PhoneGlobals;->-wrap0(Lcom/android/phone/PhoneGlobals;Landroid/content/Context;I)V

    .line 826
    .end local v1    # "airplaneMode":I
    :cond_1
    :goto_0
    return-void

    .line 726
    :cond_2
    const-string/jumbo v9, "android.intent.action.ANY_DATA_STATE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 727
    const-string/jumbo v9, "android.intent.action.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 726
    if-eqz v9, :cond_7

    .line 728
    :cond_3
    const-string/jumbo v9, "subscription"

    .line 729
    const/4 v10, -0x1

    .line 728
    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 730
    .local v8, "subId":I
    invoke-static {v8}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v5

    .line 731
    .local v5, "phoneId":I
    const-string/jumbo v9, "apnType"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 732
    .local v2, "apnType":Ljava/lang/String;
    const-string/jumbo v9, "state"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 733
    .local v7, "state":Ljava/lang/String;
    const-string/jumbo v9, "reason"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 740
    .local v6, "reason":Ljava/lang/String;
    invoke-static {v5}, Landroid/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 741
    invoke-static {v5}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    .line 745
    .local v4, "phone":Lcom/android/internal/telephony/Phone;
    :goto_1
    const-string/jumbo v9, "default"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 747
    return-void

    .line 741
    .end local v4    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_4
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    .restart local v4    # "phone":Lcom/android/internal/telephony/Phone;
    goto :goto_1

    .line 755
    :cond_5
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-get1(Lcom/android/phone/PhoneGlobals;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 756
    sget-object v9, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    invoke-virtual {v9}, Lcom/android/internal/telephony/PhoneConstants$DataState;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 755
    if-eqz v9, :cond_6

    .line 757
    const-string/jumbo v9, "roamingOn"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 755
    if-eqz v9, :cond_6

    .line 758
    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getDataRoamingEnabled()Z

    move-result v9

    xor-int/lit8 v9, v9, 0x1

    .line 755
    if-eqz v9, :cond_6

    .line 759
    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getDataEnabled()Z

    move-result v9

    .line 755
    if-eqz v9, :cond_6

    .line 762
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    iget-object v9, v9, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    const/16 v10, 0xa

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 763
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/android/phone/PhoneGlobals;->-set0(Lcom/android/phone/PhoneGlobals;Z)Z

    goto/16 :goto_0

    .line 764
    :cond_6
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-get1(Lcom/android/phone/PhoneGlobals;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 765
    sget-object v9, Lcom/android/internal/telephony/PhoneConstants$DataState;->CONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    invoke-virtual {v9}, Lcom/android/internal/telephony/PhoneConstants$DataState;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 764
    if-eqz v9, :cond_1

    .line 768
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    iget-object v9, v9, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    const/16 v10, 0xb

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 769
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/phone/PhoneGlobals;->-set0(Lcom/android/phone/PhoneGlobals;Z)Z

    goto/16 :goto_0

    .line 771
    .end local v2    # "apnType":Ljava/lang/String;
    .end local v4    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v5    # "phoneId":I
    .end local v6    # "reason":Ljava/lang/String;
    .end local v7    # "state":Ljava/lang/String;
    .end local v8    # "subId":I
    :cond_7
    const-string/jumbo v9, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 772
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-get3(Lcom/android/phone/PhoneGlobals;)Landroid/app/Activity;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 777
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    iget-object v9, v9, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    iget-object v10, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    iget-object v10, v10, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    .line 778
    const-string/jumbo v11, "ss"

    invoke-virtual {p2, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 777
    const/16 v12, 0x8

    invoke-virtual {v10, v12, v11}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 780
    :cond_8
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    iget-object v9, v9, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    iget-object v10, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    iget-object v10, v10, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    .line 781
    const-string/jumbo v11, "ss"

    invoke-virtual {p2, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 780
    const/16 v12, 0x10

    invoke-virtual {v10, v12, v11}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 782
    :cond_9
    const-string/jumbo v9, "android.intent.action.RADIO_TECHNOLOGY"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 783
    const-string/jumbo v9, "phoneName"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 784
    .local v3, "newPhone":Ljava/lang/String;
    const-string/jumbo v9, "PhoneGlobals"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Radio technology switched. Now "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " is active."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-wrap2(Lcom/android/phone/PhoneGlobals;)V

    goto/16 :goto_0

    .line 786
    .end local v3    # "newPhone":Ljava/lang/String;
    :cond_a
    const-string/jumbo v9, "android.intent.action.SERVICE_STATE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 787
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9, p2}, Lcom/android/phone/PhoneGlobals;->-wrap1(Lcom/android/phone/PhoneGlobals;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 788
    :cond_b
    const-string/jumbo v9, "android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 789
    const-string/jumbo v9, "phone"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 790
    .restart local v5    # "phoneId":I
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v5}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/phone/PhoneGlobals;->-set4(Lcom/android/phone/PhoneGlobals;Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    .line 791
    const-string/jumbo v9, "PhoneGlobals"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Emergency Callback Mode. phoneId:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-get5(Lcom/android/phone/PhoneGlobals;)Lcom/android/internal/telephony/Phone;

    move-result-object v9

    if-eqz v9, :cond_e

    .line 793
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-get5(Lcom/android/phone/PhoneGlobals;)Lcom/android/internal/telephony/Phone;

    move-result-object v9

    invoke-static {v9}, Lcom/android/internal/telephony/TelephonyCapabilities;->supportsEcm(Lcom/android/internal/telephony/Phone;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 794
    const-string/jumbo v9, "PhoneGlobals"

    const-string/jumbo v10, "Emergency Callback Mode arrived in PhoneApp."

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    const-string/jumbo v9, "phoneinECMState"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 797
    new-instance v9, Landroid/content/Intent;

    .line 798
    const-class v10, Lcom/android/phone/EmergencyCallbackModeService;

    .line 797
    invoke-direct {v9, p1, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v9}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 800
    :cond_c
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/phone/PhoneGlobals;->-set4(Lcom/android/phone/PhoneGlobals;Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0

    .line 805
    :cond_d
    const-string/jumbo v9, "PhoneGlobals"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Got ACTION_EMERGENCY_CALLBACK_MODE_CHANGED, but ECM isn\'t supported for phone: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 806
    iget-object v11, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v11}, Lcom/android/phone/PhoneGlobals;->-get5(Lcom/android/phone/PhoneGlobals;)Lcom/android/internal/telephony/Phone;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/internal/telephony/Phone;->getPhoneName()Ljava/lang/String;

    move-result-object v11

    .line 805
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/android/phone/PhoneGlobals;->-set4(Lcom/android/phone/PhoneGlobals;Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    goto/16 :goto_0

    .line 810
    :cond_e
    const-string/jumbo v9, "PhoneGlobals"

    const-string/jumbo v10, "phoneInEcm is null."

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 812
    .end local v5    # "phoneId":I
    :cond_f
    const-string/jumbo v9, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 815
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-wrap5(Lcom/android/phone/PhoneGlobals;)V

    goto/16 :goto_0

    .line 816
    :cond_10
    const-string/jumbo v9, "android.intent.action.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 819
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultDataSubscriptionId()I

    move-result v10

    invoke-static {v9, v10}, Lcom/android/phone/PhoneGlobals;->-set1(Lcom/android/phone/PhoneGlobals;I)I

    .line 820
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-wrap4(Lcom/android/phone/PhoneGlobals;)V

    .line 821
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-get2(Lcom/android/phone/PhoneGlobals;)I

    move-result v9

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    .line 822
    .restart local v4    # "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v4, :cond_1

    .line 823
    iget-object v9, p0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-static {v9}, Lcom/android/phone/PhoneGlobals;->-wrap5(Lcom/android/phone/PhoneGlobals;)V

    goto/16 :goto_0
.end method
