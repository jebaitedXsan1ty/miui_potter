.class public Lcom/android/phone/IccNetworkDepersonalizationPanel;
.super Lcom/android/phone/IccPanel;
.source "IccNetworkDepersonalizationPanel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/IccNetworkDepersonalizationPanel$1;,
        Lcom/android/phone/IccNetworkDepersonalizationPanel$2;,
        Lcom/android/phone/IccNetworkDepersonalizationPanel$3;,
        Lcom/android/phone/IccNetworkDepersonalizationPanel$4;,
        Lcom/android/phone/IccNetworkDepersonalizationPanel$5;,
        Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;
    }
.end annotation


# static fields
.field private static sNdpPanel:Lcom/android/phone/IccNetworkDepersonalizationPanel;

.field private static sShowingDialog:Z


# instance fields
.field private ERROR:I

.field private final mCallback:Lorg/codeaurora/internal/IDepersoResCallback;

.field private mDismissButton:Landroid/widget/Button;

.field mDismissListener:Landroid/view/View$OnClickListener;

.field private mEntryPanel:Landroid/widget/LinearLayout;

.field private mExtTelephony:Lorg/codeaurora/internal/IExtTelephony;

.field private mHandler:Landroid/os/Handler;

.field private mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

.field private mPersoSubtype:I

.field private mPersoSubtypeText:Landroid/widget/TextView;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mPinEntry:Landroid/widget/EditText;

.field private mPinEntryWatcher:Landroid/text/TextWatcher;

.field private mStatusPanel:Landroid/widget/LinearLayout;

.field private mStatusText:Landroid/widget/TextView;

.field private mUnlockButton:Landroid/widget/Button;

.field mUnlockListener:Landroid/view/View$OnClickListener;


# direct methods
.method static synthetic -get0(Lcom/android/phone/IccNetworkDepersonalizationPanel;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    iget v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->ERROR:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Lorg/codeaurora/internal/IDepersoResCallback;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    iget-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mCallback:Lorg/codeaurora/internal/IDepersoResCallback;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Lorg/codeaurora/internal/IExtTelephony;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    iget-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mExtTelephony:Lorg/codeaurora/internal/IExtTelephony;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Landroid/os/Handler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    iget-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    iget-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/phone/IccNetworkDepersonalizationPanel;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    iget v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubtype:I

    return v0
.end method

.method static synthetic -get6(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    iget-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/phone/IccNetworkDepersonalizationPanel;)Landroid/widget/EditText;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    iget-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPinEntry:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/IccNetworkDepersonalizationPanel;Ljava/lang/String;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->displayStatus(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/IccNetworkDepersonalizationPanel;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->hideAlert()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/IccNetworkDepersonalizationPanel;Ljava/lang/String;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/IccNetworkDepersonalizationPanel;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->log(Ljava/lang/String;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sShowingDialog:Z

    .line 72
    const/4 v0, 0x0

    sput-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sNdpPanel:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "subtype"    # I

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lcom/android/phone/IccPanel;-><init>(Landroid/content/Context;)V

    .line 71
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->ERROR:I

    .line 93
    const-string/jumbo v0, "extphone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 92
    invoke-static {v0}, Lorg/codeaurora/internal/IExtTelephony$Stub;->asInterface(Landroid/os/IBinder;)Lorg/codeaurora/internal/IExtTelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mExtTelephony:Lorg/codeaurora/internal/IExtTelephony;

    .line 116
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$1;

    invoke-direct {v0, p0}, Lcom/android/phone/IccNetworkDepersonalizationPanel$1;-><init>(Lcom/android/phone/IccNetworkDepersonalizationPanel;)V

    iput-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPinEntryWatcher:Landroid/text/TextWatcher;

    .line 132
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$2;

    invoke-direct {v0, p0}, Lcom/android/phone/IccNetworkDepersonalizationPanel$2;-><init>(Lcom/android/phone/IccNetworkDepersonalizationPanel;)V

    iput-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mHandler:Landroid/os/Handler;

    .line 158
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$3;

    invoke-direct {v0, p0}, Lcom/android/phone/IccNetworkDepersonalizationPanel$3;-><init>(Lcom/android/phone/IccNetworkDepersonalizationPanel;)V

    iput-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mCallback:Lorg/codeaurora/internal/IDepersoResCallback;

    .line 243
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;

    invoke-direct {v0, p0}, Lcom/android/phone/IccNetworkDepersonalizationPanel$4;-><init>(Lcom/android/phone/IccNetworkDepersonalizationPanel;)V

    iput-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mUnlockListener:Landroid/view/View$OnClickListener;

    .line 303
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$5;

    invoke-direct {v0, p0}, Lcom/android/phone/IccNetworkDepersonalizationPanel$5;-><init>(Lcom/android/phone/IccNetworkDepersonalizationPanel;)V

    iput-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mDismissListener:Landroid/view/View$OnClickListener;

    .line 176
    iput p2, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubtype:I

    .line 177
    return-void
.end method

.method public static dialogDismiss()V
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sNdpPanel:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sShowingDialog:Z

    if-eqz v0, :cond_0

    .line 111
    sget-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sNdpPanel:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    invoke-virtual {v0}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->dismiss()V

    .line 113
    :cond_0
    return-void
.end method

.method private displayStatus(Ljava/lang/String;)V
    .locals 7
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 268
    const/4 v1, 0x0

    .line 270
    .local v1, "label":I
    invoke-static {}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->values()[Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    move-result-object v2

    iget v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubtype:I

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    .line 271
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "displayStatus mPersoSubState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->log(Ljava/lang/String;)V

    .line 273
    invoke-virtual {p0}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    invoke-virtual {v4}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 274
    const-string/jumbo v4, "_"

    .line 273
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 274
    const-string/jumbo v4, "string"

    const-string/jumbo v5, "com.android.phone"

    .line 273
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 276
    if-nez v1, :cond_0

    .line 277
    const-string/jumbo v2, "Unable to get the PersoSubType string"

    invoke-direct {p0, v2}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->log(Ljava/lang/String;)V

    .line 278
    return-void

    .line 281
    :cond_0
    iget-object v2, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->PERSOSUBSTATE_UNKNOWN:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    if-eq v2, v3, :cond_1

    .line 282
    iget-object v2, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->PERSOSUBSTATE_IN_PROGRESS:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    if-ne v2, v3, :cond_2

    .line 284
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unsupported Perso Subtype :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    invoke-virtual {v3}, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->log(Ljava/lang/String;)V

    .line 285
    return-void

    .line 283
    :cond_2
    iget-object v2, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubState:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    sget-object v3, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;->PERSOSUBSTATE_READY:Lcom/android/internal/telephony/uicc/IccCardApplicationStatus$PersoSubState;

    if-eq v2, v3, :cond_1

    .line 288
    sget-object v2, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->ENTRY:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    invoke-virtual {v2}, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->name()Ljava/lang/String;

    move-result-object v2

    if-ne p1, v2, :cond_3

    .line 289
    invoke-virtual {p0}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 290
    .local v0, "displayText":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubtypeText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    .end local v0    # "displayText":Ljava/lang/String;
    :goto_0
    return-void

    .line 292
    :cond_3
    iget-object v2, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mStatusText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 293
    iget-object v2, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mEntryPanel:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 294
    iget-object v2, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mStatusPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private hideAlert()V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mEntryPanel:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mStatusPanel:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 301
    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 311
    const-string/jumbo v0, "PhoneGlobals"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "[IccNetworkDepersonalizationPanel] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    return-void
.end method

.method public static showDialog(I)V
    .locals 2
    .param p0, "subType"    # I

    .prologue
    .line 99
    sget-boolean v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sShowingDialog:Z

    if-eqz v0, :cond_0

    .line 100
    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "[IccNetworkDepersonalizationPanel] - showDialog; skipped already shown."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    return-void

    .line 103
    :cond_0
    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "[IccNetworkDepersonalizationPanel] - showDialog; showing dialog."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sShowingDialog:Z

    .line 105
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/phone/IccNetworkDepersonalizationPanel;-><init>(Landroid/content/Context;I)V

    sput-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sNdpPanel:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    .line 106
    sget-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sNdpPanel:Lcom/android/phone/IccNetworkDepersonalizationPanel;

    invoke-virtual {v0}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->show()V

    .line 107
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 181
    invoke-super {p0, p1}, Lcom/android/phone/IccPanel;->onCreate(Landroid/os/Bundle;)V

    .line 182
    const v3, 0x7f040062

    invoke-virtual {p0, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->setContentView(I)V

    .line 185
    const v3, 0x7f0d0103

    invoke-virtual {p0, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPinEntry:Landroid/widget/EditText;

    .line 186
    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPinEntry:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 187
    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPinEntry:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mUnlockListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPinEntry:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .local v2, "text":Ljava/lang/CharSequence;
    move-object v1, v2

    .line 191
    check-cast v1, Landroid/text/Spannable;

    .line 192
    .local v1, "span":Landroid/text/Spannable;
    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPinEntryWatcher:Landroid/text/TextWatcher;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const/16 v5, 0x12

    invoke-interface {v1, v3, v6, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 194
    const v3, 0x7f0d0101

    invoke-virtual {p0, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mEntryPanel:Landroid/widget/LinearLayout;

    .line 195
    const v3, 0x7f0d0102

    invoke-virtual {p0, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPersoSubtypeText:Landroid/widget/TextView;

    .line 196
    sget-object v3, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->ENTRY:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    invoke-virtual {v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->displayStatus(Ljava/lang/String;)V

    .line 198
    const v3, 0x7f0d0104

    invoke-virtual {p0, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mUnlockButton:Landroid/widget/Button;

    .line 199
    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mUnlockButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mUnlockListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    const v3, 0x7f0d0105

    invoke-virtual {p0, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mDismissButton:Landroid/widget/Button;

    .line 204
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/PhoneGlobals;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 206
    .local v0, "carrierConfig":Landroid/os/PersistableBundle;
    const-string/jumbo v3, "sim_network_unlock_allow_dismiss_bool"

    .line 205
    invoke-virtual {v0, v3}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 208
    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mDismissButton:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 209
    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mDismissButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mDismissListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    :goto_0
    const v3, 0x7f0d0106

    invoke-virtual {p0, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mStatusPanel:Landroid/widget/LinearLayout;

    .line 217
    const v3, 0x7f0d0107

    invoke-virtual {p0, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mStatusText:Landroid/widget/TextView;

    .line 219
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    iput-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 220
    return-void

    .line 212
    :cond_0
    iget-object v3, p0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->mDismissButton:Landroid/widget/Button;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 236
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 237
    const/4 v0, 0x1

    return v0

    .line 240
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/phone/IccPanel;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 224
    invoke-super {p0}, Lcom/android/phone/IccPanel;->onStart()V

    .line 225
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 229
    invoke-super {p0}, Lcom/android/phone/IccPanel;->onStop()V

    .line 230
    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "[IccNetworkDepersonalizationPanel] - showDialog; hiding dialog."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/phone/IccNetworkDepersonalizationPanel;->sShowingDialog:Z

    .line 232
    return-void
.end method
