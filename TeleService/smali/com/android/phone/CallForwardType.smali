.class public Lcom/android/phone/CallForwardType;
.super Landroid/preference/PreferenceActivity;
.source "CallForwardType.java"


# instance fields
.field private final DBG:Z

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

.field private mVideoPreference:Landroid/preference/Preference;

.field private mVoicePreference:Landroid/preference/Preference;


# direct methods
.method static synthetic -get0(Lcom/android/phone/CallForwardType;)Lcom/android/phone/SubscriptionInfoHelper;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CallForwardType;

    .prologue
    iget-object v0, p0, Lcom/android/phone/CallForwardType;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/CallForwardType;->DBG:Z

    .line 46
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    const-string/jumbo v1, "CallForwardType"

    const-string/jumbo v2, "onCreate.."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const v1, 0x7f060007

    invoke-virtual {p0, v1}, Lcom/android/phone/CallForwardType;->addPreferencesFromResource(I)V

    .line 64
    new-instance v1, Lcom/android/phone/SubscriptionInfoHelper;

    invoke-virtual {p0}, Lcom/android/phone/CallForwardType;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/phone/SubscriptionInfoHelper;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v1, p0, Lcom/android/phone/CallForwardType;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    .line 65
    iget-object v1, p0, Lcom/android/phone/CallForwardType;->mSubscriptionInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    invoke-virtual {v1}, Lcom/android/phone/SubscriptionInfoHelper;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/CallForwardType;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 68
    const-string/jumbo v1, "button_cf_key_voice"

    invoke-virtual {p0, v1}, Lcom/android/phone/CallForwardType;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/CallForwardType;->mVoicePreference:Landroid/preference/Preference;

    .line 69
    iget-object v1, p0, Lcom/android/phone/CallForwardType;->mVoicePreference:Landroid/preference/Preference;

    new-instance v2, Lcom/android/phone/CallForwardType$1;

    invoke-direct {v2, p0}, Lcom/android/phone/CallForwardType$1;-><init>(Lcom/android/phone/CallForwardType;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 83
    const-string/jumbo v1, "button_cf_key_video"

    invoke-virtual {p0, v1}, Lcom/android/phone/CallForwardType;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/CallForwardType;->mVideoPreference:Landroid/preference/Preference;

    .line 84
    iget-object v1, p0, Lcom/android/phone/CallForwardType;->mVideoPreference:Landroid/preference/Preference;

    new-instance v2, Lcom/android/phone/CallForwardType$2;

    invoke-direct {v2, p0}, Lcom/android/phone/CallForwardType$2;-><init>(Lcom/android/phone/CallForwardType;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 98
    iget-object v1, p0, Lcom/android/phone/CallForwardType;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    .line 99
    iget-object v2, p0, Lcom/android/phone/CallForwardType;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "config_hide_vt_callforward_option"

    .line 98
    invoke-static {v1, v2, v3}, Lorg/codeaurora/ims/utils/QtiImsExtUtils;->isCarrierConfigEnabled(ILandroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 100
    .local v0, "removeVtCfOption":Z
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/phone/CallForwardType;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/phone/CallForwardType;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->isVideoEnabled()Z

    move-result v1

    :goto_0
    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 101
    :cond_0
    const-string/jumbo v1, "CallForwardType"

    const-string/jumbo v2, "VT or/and Ut Service is not enabled"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-virtual {p0}, Lcom/android/phone/CallForwardType;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/CallForwardType;->mVideoPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 104
    :cond_1
    return-void

    .line 100
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 108
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 109
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/android/phone/CallForwardType;->onBackPressed()V

    .line 111
    const/4 v1, 0x1

    return v1

    .line 113
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method
