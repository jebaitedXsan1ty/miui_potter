.class Lcom/android/phone/CallForwardType$1;
.super Ljava/lang/Object;
.source "CallForwardType.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/CallForwardType;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CallForwardType;


# direct methods
.method constructor <init>(Lcom/android/phone/CallForwardType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/CallForwardType;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/CallForwardType$1;->this$0:Lcom/android/phone/CallForwardType;

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "pref"    # Landroid/preference/Preference;

    .prologue
    const/4 v3, 0x1

    .line 73
    iget-object v1, p0, Lcom/android/phone/CallForwardType$1;->this$0:Lcom/android/phone/CallForwardType;

    invoke-static {v1}, Lcom/android/phone/CallForwardType;->-get0(Lcom/android/phone/CallForwardType;)Lcom/android/phone/SubscriptionInfoHelper;

    move-result-object v1

    const-class v2, Lcom/android/phone/GsmUmtsCallForwardOptions;

    invoke-virtual {v1, v2}, Lcom/android/phone/SubscriptionInfoHelper;->getIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 74
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "CallForwardType"

    const-string/jumbo v2, "Voice button clicked!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const-string/jumbo v1, "service_class"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 77
    iget-object v1, p0, Lcom/android/phone/CallForwardType$1;->this$0:Lcom/android/phone/CallForwardType;

    invoke-virtual {v1, v0}, Lcom/android/phone/CallForwardType;->startActivity(Landroid/content/Intent;)V

    .line 78
    return v3
.end method
