.class public Lcom/android/phone/RestrictedSwitchPreference;
.super Landroid/preference/SwitchPreference;
.source "RestrictedSwitchPreference.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDisabledByAdmin:Z

.field private final mSwitchWidgetResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/RestrictedSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    const v0, 0x101036d

    invoke-direct {p0, p1, p2, v0}, Lcom/android/phone/RestrictedSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/phone/RestrictedSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 39
    invoke-virtual {p0}, Lcom/android/phone/RestrictedSwitchPreference;->getWidgetLayoutResource()I

    move-result v0

    iput v0, p0, Lcom/android/phone/RestrictedSwitchPreference;->mSwitchWidgetResId:I

    .line 40
    iput-object p1, p0, Lcom/android/phone/RestrictedSwitchPreference;->mContext:Landroid/content/Context;

    .line 41
    return-void
.end method


# virtual methods
.method public checkRestrictionAndSetDisabled(Ljava/lang/String;)V
    .locals 4
    .param p1, "userRestriction"    # Ljava/lang/String;

    .prologue
    .line 70
    iget-object v3, p0, Lcom/android/phone/RestrictedSwitchPreference;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    .line 71
    .local v1, "um":Landroid/os/UserManager;
    invoke-virtual {v1}, Landroid/os/UserManager;->getUserHandle()I

    move-result v3

    invoke-static {v3}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v2

    .line 72
    .local v2, "user":Landroid/os/UserHandle;
    invoke-virtual {v1, p1, v2}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    invoke-virtual {v1, p1, v2}, Landroid/os/UserManager;->hasBaseUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v3

    xor-int/lit8 v0, v3, 0x1

    .line 74
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/phone/RestrictedSwitchPreference;->setDisabledByAdmin(Z)V

    .line 75
    return-void

    .line 72
    :cond_0
    const/4 v0, 0x0

    .local v0, "disabledByAdmin":Z
    goto :goto_0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/preference/SwitchPreference;->onBindView(Landroid/view/View;)V

    .line 58
    iget-boolean v1, p0, Lcom/android/phone/RestrictedSwitchPreference;->mDisabledByAdmin:Z

    if-eqz v1, :cond_0

    .line 59
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 61
    :cond_0
    const v1, 0x1020010

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 62
    .local v0, "summaryView":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/android/phone/RestrictedSwitchPreference;->mDisabledByAdmin:Z

    if-eqz v1, :cond_1

    .line 64
    invoke-virtual {p0}, Lcom/android/phone/RestrictedSwitchPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0b0173

    .line 63
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 65
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 67
    :cond_1
    return-void

    .line 64
    :cond_2
    const v1, 0x7f0b0174

    goto :goto_0
.end method

.method public performClick(Landroid/preference/PreferenceScreen;)V
    .locals 2
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/android/phone/RestrictedSwitchPreference;->mDisabledByAdmin:Z

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/android/phone/RestrictedSwitchPreference;->mContext:Landroid/content/Context;

    .line 98
    new-instance v1, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    invoke-direct {v1}, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;-><init>()V

    .line 97
    invoke-static {v0, v1}, Lcom/android/settingslib/RestrictedLockUtils;->sendShowAdminSupportDetailsIntent(Landroid/content/Context;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/SwitchPreference;->performClick(Landroid/preference/PreferenceScreen;)V

    goto :goto_0
.end method

.method public setDisabledByAdmin(Z)V
    .locals 1
    .param p1, "disabled"    # Z

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/android/phone/RestrictedSwitchPreference;->mDisabledByAdmin:Z

    if-eq v0, p1, :cond_0

    .line 88
    iput-boolean p1, p0, Lcom/android/phone/RestrictedSwitchPreference;->mDisabledByAdmin:Z

    .line 89
    if-eqz p1, :cond_1

    const v0, 0x7f04005a

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/phone/RestrictedSwitchPreference;->setWidgetLayoutResource(I)V

    .line 90
    xor-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/android/phone/RestrictedSwitchPreference;->setEnabled(Z)V

    .line 92
    :cond_0
    return-void

    .line 89
    :cond_1
    iget v0, p0, Lcom/android/phone/RestrictedSwitchPreference;->mSwitchWidgetResId:I

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 79
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/phone/RestrictedSwitchPreference;->mDisabledByAdmin:Z

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phone/RestrictedSwitchPreference;->setDisabledByAdmin(Z)V

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    goto :goto_0
.end method
