.class public Lcom/android/phone/CarrierConfigLoaderInjector;
.super Ljava/lang/Object;
.source "CarrierConfigLoaderInjector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/CarrierConfigLoaderInjector$1;,
        Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;
    }
.end annotation


# static fields
.field public static mSimLoadedTimes:[J

.field private static sDataSlotListener:Lmiui/telephony/DefaultSimManager$DataSlotListener;

.field private static sEnableDual4G:Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    sget v0, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    new-array v0, v0, [J

    sput-object v0, Lcom/android/phone/CarrierConfigLoaderInjector;->mSimLoadedTimes:[J

    .line 59
    new-instance v0, Lcom/android/phone/CarrierConfigLoaderInjector$1;

    invoke-direct {v0}, Lcom/android/phone/CarrierConfigLoaderInjector$1;-><init>()V

    sput-object v0, Lcom/android/phone/CarrierConfigLoaderInjector;->sDataSlotListener:Lmiui/telephony/DefaultSimManager$DataSlotListener;

    .line 71
    new-instance v0, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;-><init>(I)V

    sput-object v0, Lcom/android/phone/CarrierConfigLoaderInjector;->sEnableDual4G:Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;

    .line 24
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static enableViceSlotVolteForDualCtSim()V
    .locals 5

    .prologue
    .line 141
    invoke-static {}, Lcom/android/phone/CarrierConfigLoaderInjector;->isAllSimLoaded()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isDualCTSim()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    .line 142
    :cond_0
    const-string/jumbo v2, "CarrierConfigLoaderInjector"

    const-string/jumbo v3, "not all sim loaded"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return-void

    .line 145
    :cond_1
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/telephony/DefaultSimManager;->isDataSlotReady()Z

    move-result v2

    if-nez v2, :cond_2

    .line 146
    const-string/jumbo v2, "CarrierConfigLoaderInjector"

    const-string/jumbo v3, "data slot is not ready"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v2

    sget-object v3, Lcom/android/phone/CarrierConfigLoaderInjector;->sDataSlotListener:Lmiui/telephony/DefaultSimManager$DataSlotListener;

    invoke-virtual {v2, v3}, Lmiui/telephony/DefaultSimManager;->addDataSlotListener(Lmiui/telephony/DefaultSimManager$DataSlotListener;)V

    .line 148
    return-void

    .line 150
    :cond_2
    invoke-static {}, Lcom/android/phone/Dual4GManager;->isDual4GEnabled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 151
    const-string/jumbo v2, "CarrierConfigLoaderInjector"

    const-string/jumbo v3, "dual ct sim, enable dual 4g for non-dds slot..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    sget-object v2, Lcom/android/phone/CarrierConfigLoaderInjector;->sEnableDual4G:Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;

    invoke-virtual {v2}, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->start()V

    .line 154
    :cond_3
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    .line 155
    .local v0, "ddsSlotId":I
    const-string/jumbo v2, "CarrierConfigLoaderInjector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "dds slot="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v2, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v1, v2, :cond_5

    .line 157
    if-ne v1, v0, :cond_4

    .line 156
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    :cond_4
    const-string/jumbo v2, "CarrierConfigLoaderInjector"

    const-string/jumbo v3, "dual ct sim, turn on volte for non-dds slot..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3, v1}, Lcom/android/services/telephony/ims/ImsAdapter;->setEnhanced4gLteModeSetting(Landroid/content/Context;ZI)V

    goto :goto_1

    .line 163
    :cond_5
    return-void
.end method

.method private static isAllSimLoaded()Z
    .locals 6

    .prologue
    .line 166
    const/4 v1, 0x0

    .line 167
    .local v1, "loadedSimCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v2, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ge v0, v2, :cond_1

    .line 168
    sget-object v2, Lcom/android/phone/CarrierConfigLoaderInjector;->mSimLoadedTimes:[J

    aget-wide v2, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 169
    add-int/lit8 v1, v1, 0x1

    .line 167
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_1
    sget v2, Lmiui/telephony/MiuiTelephony;->PHONE_COUNT:I

    if-ne v1, v2, :cond_2

    .line 173
    const/4 v2, 0x1

    return v2

    .line 175
    :cond_2
    const/4 v2, 0x0

    return v2
.end method

.method public static isVolteForceEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    const/4 v2, 0x1

    return v2
.end method

.method public static onIccAbsent(I)V
    .locals 4
    .param p0, "slotId"    # I

    .prologue
    .line 179
    sget-object v0, Lcom/android/phone/CarrierConfigLoaderInjector;->mSimLoadedTimes:[J

    const-wide/16 v2, 0x0

    aput-wide v2, v0, p0

    .line 180
    const-string/jumbo v0, "CarrierConfigLoaderInjector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mSimLoadedTimes["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/phone/CarrierConfigLoaderInjector;->mSimLoadedTimes:[J

    aget-wide v2, v2, p0

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v0

    sget-object v1, Lcom/android/phone/CarrierConfigLoaderInjector;->sDataSlotListener:Lmiui/telephony/DefaultSimManager$DataSlotListener;

    invoke-virtual {v0, v1}, Lmiui/telephony/DefaultSimManager;->removeDataSlotListener(Lmiui/telephony/DefaultSimManager$DataSlotListener;)V

    .line 182
    sget-object v0, Lcom/android/phone/CarrierConfigLoaderInjector;->sEnableDual4G:Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;

    invoke-virtual {v0}, Lcom/android/phone/CarrierConfigLoaderInjector$EnableDual4GForDualCTSim;->stop()V

    .line 183
    return-void
.end method

.method public static onIccLoaded(I)V
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 186
    sget-object v0, Lcom/android/phone/CarrierConfigLoaderInjector;->mSimLoadedTimes:[J

    aget-wide v2, v0, p0

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    aput-wide v2, v0, p0

    .line 187
    const-string/jumbo v0, "CarrierConfigLoaderInjector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mSimLoadedTimes["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/phone/CarrierConfigLoaderInjector;->mSimLoadedTimes:[J

    aget-wide v2, v2, p0

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    return-void
.end method

.method public static setVolteForceEnabled(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 53
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 54
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 55
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "force_enable_volte"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 56
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 57
    return-void
.end method

.method public static updateEnhanced4GLteDefaultValue(Landroid/content/Context;I)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneId"    # I

    .prologue
    const/4 v6, -0x1

    .line 105
    invoke-static {p1}, Lmiui/telephony/SubscriptionManager;->isRealSlotId(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    invoke-static {p1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/MiuiPhoneUtils;->isCTVoLTESupport(Lcom/android/internal/telephony/Phone;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 105
    if-eqz v2, :cond_1

    .line 107
    :cond_0
    const-string/jumbo v2, "CarrierConfigLoaderInjector"

    const-string/jumbo v3, "No need to updateEnhanced4GLteDefaultValue!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    return-void

    .line 112
    :cond_1
    sget-object v2, Lcom/android/phone/CarrierConfigLoaderInjector;->mSimLoadedTimes:[J

    aget-wide v2, v2, p1

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 113
    const-string/jumbo v2, "CarrierConfigLoaderInjector"

    const-string/jumbo v3, "updateEnhanced4GLteDefaultValue: not first loaded, return"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    return-void

    .line 117
    :cond_2
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v0

    .line 118
    .local v0, "subId":I
    invoke-static {v0}, Lcom/android/phone/VolteEnableManager;->getUserSelectedVolteState(I)I

    move-result v1

    .line 122
    .local v1, "userEnabled":I
    if-eq v1, v6, :cond_3

    .line 123
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lmiui/telephony/DefaultSimManager;->getSimInsertStates(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    .line 125
    :cond_3
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "volte_vt_enabled"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 126
    if-eq v1, v6, :cond_4

    .line 127
    invoke-static {v0}, Lcom/android/phone/VolteEnableManager;->clearUserSelectedVolteState(I)V

    .line 129
    :cond_4
    const-string/jumbo v2, "CarrierConfigLoaderInjector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateEnhanced4GLteDefaultValue subId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", phoneId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_5
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isCTDualVolteSupported()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 133
    invoke-static {}, Lcom/android/phone/CarrierConfigLoaderInjector;->enableViceSlotVolteForDualCtSim()V

    .line 135
    :cond_6
    return-void

    .line 124
    :cond_7
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lmiui/telephony/DefaultSimManager;->getSimInsertStates(I)I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_5

    goto :goto_0
.end method

.method public static updateVoLTECarrierConfig(Landroid/content/Context;Landroid/os/PersistableBundle;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "carrierConfig"    # Landroid/os/PersistableBundle;
    .param p2, "operatorNumeric"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 31
    sget-boolean v2, Lmiui/os/Build;->IS_CT_CUSTOMIZATION_TEST:Z

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/android/phone/CarrierConfigLoaderInjector;->isVolteForceEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 32
    :cond_0
    invoke-static {p1, v3}, Lcom/android/phone/PhoneProxy;->setVolteCarrierConfigValue(Landroid/os/PersistableBundle;Z)V

    .line 33
    return-void

    .line 36
    :cond_1
    invoke-static {}, Lcom/android/phone/MiuiCloudDataManager;->getInstance()Lcom/android/phone/MiuiCloudDataManager;

    move-result-object v0

    .line 37
    .local v0, "cloudDataMgr":Lcom/android/phone/MiuiCloudDataManager;
    if-eqz v0, :cond_2

    .line 38
    invoke-virtual {v0, p2}, Lcom/android/phone/MiuiCloudDataManager;->getCloudVolteConfig(Ljava/lang/String;)I

    move-result v1

    .line 39
    .local v1, "cloudValue":I
    if-ne v1, v3, :cond_3

    .line 40
    invoke-static {p1, v3}, Lcom/android/phone/PhoneProxy;->setVolteCarrierConfigValue(Landroid/os/PersistableBundle;Z)V

    .line 45
    .end local v1    # "cloudValue":I
    :cond_2
    :goto_0
    return-void

    .line 41
    .restart local v1    # "cloudValue":I
    :cond_3
    if-nez v1, :cond_2

    .line 42
    invoke-static {p1, v4}, Lcom/android/phone/PhoneProxy;->setVolteCarrierConfigValue(Landroid/os/PersistableBundle;Z)V

    goto :goto_0
.end method
