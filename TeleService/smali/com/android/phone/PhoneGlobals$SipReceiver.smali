.class Lcom/android/phone/PhoneGlobals$SipReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhoneGlobals.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/PhoneGlobals;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SipReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/PhoneGlobals;


# direct methods
.method private constructor <init>(Lcom/android/phone/PhoneGlobals;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    .line 829
    iput-object p1, p0, Lcom/android/phone/PhoneGlobals$SipReceiver;->this$0:Lcom/android/phone/PhoneGlobals;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/PhoneGlobals;Lcom/android/phone/PhoneGlobals$SipReceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/PhoneGlobals;
    .param p2, "-this1"    # Lcom/android/phone/PhoneGlobals$SipReceiver;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/PhoneGlobals$SipReceiver;-><init>(Lcom/android/phone/PhoneGlobals;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 833
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 835
    .local v0, "action":Ljava/lang/String;
    invoke-static {}, Lcom/android/services/telephony/sip/SipAccountRegistry;->getInstance()Lcom/android/services/telephony/sip/SipAccountRegistry;

    move-result-object v1

    .line 836
    .local v1, "sipAccountRegistry":Lcom/android/services/telephony/sip/SipAccountRegistry;
    const-string/jumbo v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 837
    invoke-static {}, Lcom/android/services/telephony/sip/SipUtil;->startSipService()V

    .line 851
    :cond_0
    :goto_0
    return-void

    .line 838
    :cond_1
    const-string/jumbo v2, "android.net.sip.SIP_SERVICE_UP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 839
    const-string/jumbo v2, "com.android.phone.SIP_CALL_OPTION_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 838
    if-eqz v2, :cond_3

    .line 840
    :cond_2
    invoke-virtual {v1, p1}, Lcom/android/services/telephony/sip/SipAccountRegistry;->setup(Landroid/content/Context;)V

    goto :goto_0

    .line 841
    :cond_3
    const-string/jumbo v2, "com.android.phone.SIP_REMOVE_PHONE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 842
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->-get0()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 843
    const-string/jumbo v2, "PhoneGlobals"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "SIP_REMOVE_PHONE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 844
    const-string/jumbo v4, "android:localSipUri"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 843
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    :cond_4
    const-string/jumbo v2, "android:localSipUri"

    .line 846
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipAccountRegistry;->removeSipProfile(Ljava/lang/String;)V

    goto :goto_0

    .line 849
    :cond_5
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->-get0()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "PhoneGlobals"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onReceive, action not processed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
