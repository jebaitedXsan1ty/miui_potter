.class Lcom/android/phone/CdmaCallOptions$2;
.super Ljava/lang/Object;
.source "CdmaCallOptions.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/CdmaCallOptions;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CdmaCallOptions;

.field final synthetic val$subInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;


# direct methods
.method constructor <init>(Lcom/android/phone/CdmaCallOptions;Lcom/android/phone/SubscriptionInfoHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/CdmaCallOptions;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/CdmaCallOptions$2;->this$0:Lcom/android/phone/CdmaCallOptions;

    iput-object p2, p0, Lcom/android/phone/CdmaCallOptions$2;->val$subInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 234
    iget-object v2, p0, Lcom/android/phone/CdmaCallOptions$2;->val$subInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    invoke-virtual {v2}, Lcom/android/phone/SubscriptionInfoHelper;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 235
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 236
    iget-object v2, p0, Lcom/android/phone/CdmaCallOptions$2;->val$subInfoHelper:Lcom/android/phone/SubscriptionInfoHelper;

    const-class v3, Lcom/android/phone/CallForwardType;

    invoke-virtual {v2, v3}, Lcom/android/phone/SubscriptionInfoHelper;->getIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 238
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    const-string/jumbo v2, "subscription"

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 239
    iget-object v2, p0, Lcom/android/phone/CdmaCallOptions$2;->this$0:Lcom/android/phone/CdmaCallOptions;

    invoke-virtual {v2, v0}, Lcom/android/phone/CdmaCallOptions;->startActivity(Landroid/content/Intent;)V

    .line 240
    const/4 v2, 0x1

    return v2

    .line 237
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "org.codeaurora.settings.CDMA_CALL_FORWARDING"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method
