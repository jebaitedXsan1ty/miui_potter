.class final Lcom/android/phone/MiuiLog$1;
.super Ljava/lang/Object;
.source "MiuiLog.java"

# interfaces
.implements Lmiui/telephony/PhoneDebug$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/MiuiLog;->register()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onDebugChanged()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 36
    sget-boolean v0, Lmiui/telephony/PhoneDebug;->VDBG:Z

    if-nez v0, :cond_2

    .line 37
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/android/services/telephony/Log;->isLoggable(I)Z

    move-result v0

    .line 36
    :goto_0
    sput-boolean v0, Lcom/android/services/telephony/Log;->DEBUG:Z

    .line 38
    sget-boolean v0, Lmiui/telephony/PhoneDebug;->VDBG:Z

    if-nez v0, :cond_0

    .line 39
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/android/services/telephony/Log;->isLoggable(I)Z

    move-result v1

    .line 38
    :cond_0
    sput-boolean v1, Lcom/android/services/telephony/Log;->VERBOSE:Z

    .line 40
    sget-boolean v0, Lmiui/telephony/PhoneDebug;->VDBG:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "onDebugChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 36
    goto :goto_0
.end method
