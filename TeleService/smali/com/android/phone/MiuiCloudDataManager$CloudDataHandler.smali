.class Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;
.super Landroid/os/Handler;
.source "MiuiCloudDataManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/MiuiCloudDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CloudDataHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/MiuiCloudDataManager;


# direct methods
.method public constructor <init>(Lcom/android/phone/MiuiCloudDataManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/MiuiCloudDataManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;->this$0:Lcom/android/phone/MiuiCloudDataManager;

    .line 88
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 89
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/32 v2, 0x2932e00

    const/4 v1, 0x2

    .line 93
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 95
    :pswitch_0
    iget-object v0, p0, Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;->this$0:Lcom/android/phone/MiuiCloudDataManager;

    invoke-static {v0}, Lcom/android/phone/MiuiCloudDataManager;->-wrap1(Lcom/android/phone/MiuiCloudDataManager;)V

    .line 96
    invoke-virtual {p0, v1}, Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 101
    :pswitch_1
    iget-object v0, p0, Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;->this$0:Lcom/android/phone/MiuiCloudDataManager;

    invoke-static {v0}, Lcom/android/phone/MiuiCloudDataManager;->-wrap0(Lcom/android/phone/MiuiCloudDataManager;)Z

    .line 102
    invoke-virtual {p0, v1, v2, v3}, Lcom/android/phone/MiuiCloudDataManager$CloudDataHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
