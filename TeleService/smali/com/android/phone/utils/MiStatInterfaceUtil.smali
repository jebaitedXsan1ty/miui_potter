.class public Lcom/android/phone/utils/MiStatInterfaceUtil;
.super Ljava/lang/Object;
.source "MiStatInterfaceUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;,
        Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;
    }
.end annotation


# static fields
.field private static final IS_INTERNATIONAL_BUILD:Z

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final mHandler:Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -wrap0(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    invoke-static {p0}, Lcom/android/phone/utils/MiStatInterfaceUtil;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const-class v0, Lcom/android/phone/utils/MiStatInterfaceUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil;->LOG_TAG:Ljava/lang/String;

    .line 41
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    .line 40
    sput-boolean v0, Lcom/android/phone/utils/MiStatInterfaceUtil;->IS_INTERNATIONAL_BUILD:Z

    .line 44
    new-instance v0, Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;

    invoke-static {}, Lcom/android/phone/utils/MiuiThreadUtil;->getWorkThreadLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;-><init>(Landroid/os/Looper;)V

    .line 43
    sput-object v0, Lcom/android/phone/utils/MiStatInterfaceUtil;->mHandler:Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;

    .line 30
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final addCustomEvent(Ljava/lang/String;)V
    .locals 4
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 77
    sget-boolean v2, Lcom/android/phone/utils/MiStatInterfaceUtil;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_0

    return-void

    .line 78
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 79
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "extra_custom_value"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    sget-object v2, Lcom/android/phone/utils/MiStatInterfaceUtil;->mHandler:Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 82
    .local v1, "message":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 84
    return-void
.end method

.method private static getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 87
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static final init(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    sget-boolean v1, Lcom/android/phone/utils/MiStatInterfaceUtil;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_0

    return-void

    .line 48
    :cond_0
    sget-object v1, Lcom/android/phone/utils/MiStatInterfaceUtil;->mHandler:Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p0}, Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 49
    .local v0, "message":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 50
    return-void
.end method

.method public static final recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 53
    sget-boolean v2, Lcom/android/phone/utils/MiStatInterfaceUtil;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_0

    return-void

    .line 54
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 55
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "extra_category"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string/jumbo v2, "extra_key"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    sget-object v2, Lcom/android/phone/utils/MiStatInterfaceUtil;->mHandler:Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 59
    .local v1, "message":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 61
    return-void
.end method

.method public static final recordCountEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p0, "category"    # Ljava/lang/String;
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p2, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-boolean v2, Lcom/android/phone/utils/MiStatInterfaceUtil;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_0

    return-void

    .line 66
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 67
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v2, "extra_category"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string/jumbo v2, "extra_key"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    sget-object v2, Lcom/android/phone/utils/MiStatInterfaceUtil;->mHandler:Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;

    .line 71
    const/4 v3, 0x2

    .line 70
    invoke-virtual {v2, v3, p2}, Lcom/android/phone/utils/MiStatInterfaceUtil$MiStatHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 72
    .local v1, "message":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 74
    return-void
.end method
