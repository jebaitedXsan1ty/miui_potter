.class Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;
.super Landroid/os/HandlerThread;
.source "MiuiThreadUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/utils/MiuiThreadUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WorkThreadHolder"
.end annotation


# static fields
.field public static sInstance:Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;

    invoke-direct {v0}, Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;-><init>()V

    sput-object v0, Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;->sInstance:Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;

    .line 12
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 17
    const-string/jumbo v0, "PhoneWorkThread"

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 18
    invoke-virtual {p0}, Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;->start()V

    .line 19
    return-void
.end method
