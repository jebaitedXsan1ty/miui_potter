.class public Lcom/android/phone/utils/MiuiThreadUtil;
.super Ljava/lang/Object;
.source "MiuiThreadUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getWorkThreadLooper()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;->sInstance:Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;

    invoke-virtual {v0}, Lcom/android/phone/utils/MiuiThreadUtil$WorkThreadHolder;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method
