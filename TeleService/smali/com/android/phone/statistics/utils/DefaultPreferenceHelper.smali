.class public Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;
.super Ljava/lang/Object;
.source "DefaultPreferenceHelper.java"


# static fields
.field private static final PREFERENCE_VALUES:Landroid/os/Bundle;

.field private static volatile sInstance:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;


# instance fields
.field private final MODE:I

.field private final mSp:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    sput-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->sInstance:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    .line 35
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    .line 15
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput v0, p0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->MODE:I

    .line 49
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->mSp:Landroid/content/SharedPreferences;

    .line 50
    return-void
.end method

.method public static getInstance()Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;
    .locals 4

    .prologue
    .line 38
    sget-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->sInstance:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    if-nez v0, :cond_1

    .line 39
    const-class v1, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    monitor-enter v1

    .line 40
    :try_start_0
    sget-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->sInstance:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    const-string/jumbo v3, "telephony_statistic"

    invoke-direct {v0, v2, v3}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->sInstance:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    .line 45
    :cond_1
    sget-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->sInstance:Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getBooleanPreference(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defValue"    # Ljava/lang/Boolean;

    .prologue
    .line 98
    sget-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    sget-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->mSp:Landroid/content/SharedPreferences;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getIntPreference(Ljava/lang/String;)I
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 89
    sget-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    sget-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->mSp:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getLastUserMdlogTime()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 150
    const-string/jumbo v0, "last_update_user_modem_log_time"

    invoke-virtual {p0, v0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getLongPref(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getLongPref(Ljava/lang/String;)J
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 114
    sget-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->mSp:Landroid/content/SharedPreferences;

    const-wide/16 v2, 0x0

    invoke-interface {v0, p1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getRuleEvent()I
    .locals 1

    .prologue
    .line 170
    const-string/jumbo v0, "rule_event"

    invoke-virtual {p0, v0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getIntPreference(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getStatisticEnabled()Z
    .locals 2

    .prologue
    .line 142
    const-string/jumbo v0, "statistic_enabled"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getBooleanPreference(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getUserDisagreeCounter()I
    .locals 1

    .prologue
    .line 158
    const-string/jumbo v0, "user_disagree_counter"

    invoke-virtual {p0, v0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getIntPreference(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public resetRuleEvent()V
    .locals 2

    .prologue
    .line 189
    const-string/jumbo v0, "rule_event"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setIntPreference(Ljava/lang/String;I)V

    .line 190
    return-void
.end method

.method public revokeUserPermission()V
    .locals 2

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getRuleEvent()I

    move-result v0

    invoke-static {v0}, Lcom/android/phone/statistics/utils/Utils;->isAnyValidRuleEventExist(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getLastUserMdlogTime()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->isNeedToRevokeUserPermission(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const-string/jumbo v0, "DefaultPreferenceHelper"

    const-string/jumbo v1, "revokeUserPermission reset userpermission"

    invoke-static {v0, v1}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-virtual {p0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->resetRuleEvent()V

    .line 200
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setUserDisagreeCounter(I)V

    .line 203
    :cond_0
    return-void
.end method

.method public setBooleanPreference(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 60
    sget-object v1, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    invoke-virtual {v1, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 62
    iget-object v1, p0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->mSp:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 63
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 64
    return-void
.end method

.method public setIntPreference(Ljava/lang/String;I)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 67
    sget-object v1, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    invoke-virtual {v1, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    iget-object v1, p0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->mSp:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 70
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 71
    return-void
.end method

.method public setLastUserMdlogTime(Ljava/lang/Long;)V
    .locals 4
    .param p1, "time"    # Ljava/lang/Long;

    .prologue
    .line 146
    const-string/jumbo v0, "last_update_user_modem_log_time"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setLongPref(Ljava/lang/String;J)V

    .line 147
    return-void
.end method

.method public setLongPref(Ljava/lang/String;J)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 74
    sget-object v1, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->PREFERENCE_VALUES:Landroid/os/Bundle;

    invoke-virtual {v1, p1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 76
    iget-object v1, p0, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->mSp:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 77
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 78
    return-void
.end method

.method public setRuleEvent(I)V
    .locals 1
    .param p1, "ruleEvent"    # I

    .prologue
    .line 174
    const-string/jumbo v0, "rule_event"

    invoke-virtual {p0, v0, p1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setIntPreference(Ljava/lang/String;I)V

    .line 175
    return-void
.end method

.method public setRuleEvent(IZ)V
    .locals 3
    .param p1, "ruleEventId"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 178
    const/4 v2, 0x1

    shl-int v1, v2, p1

    .line 179
    .local v1, "flag":I
    invoke-virtual {p0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getRuleEvent()I

    move-result v0

    .line 180
    .local v0, "curRuleEvent":I
    if-eqz p2, :cond_0

    .line 181
    or-int/2addr v0, v1

    .line 185
    :goto_0
    const-string/jumbo v2, "rule_event"

    invoke-virtual {p0, v2, v0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setIntPreference(Ljava/lang/String;I)V

    .line 186
    return-void

    .line 183
    :cond_0
    not-int v2, v1

    and-int/2addr v0, v2

    goto :goto_0
.end method

.method public setStatisticEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 138
    const-string/jumbo v0, "statistic_enabled"

    invoke-virtual {p0, v0, p1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setBooleanPreference(Ljava/lang/String;Z)V

    .line 139
    return-void
.end method

.method public setUserDisagreeCounter(I)V
    .locals 1
    .param p1, "disagreeCn"    # I

    .prologue
    .line 154
    const-string/jumbo v0, "user_disagree_counter"

    invoke-virtual {p0, v0, p1}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setIntPreference(Ljava/lang/String;I)V

    .line 155
    return-void
.end method

.method public updateMdlogFromSP()V
    .locals 4

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->getRuleEvent()I

    move-result v0

    .line 163
    .local v0, "ruleEvent":I
    invoke-virtual {p0, v0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->setRuleEvent(I)V

    .line 165
    const-string/jumbo v1, "DefaultPreferenceHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateMdlogFromSP ruleEvent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-virtual {p0}, Lcom/android/phone/statistics/utils/DefaultPreferenceHelper;->revokeUserPermission()V

    .line 167
    return-void
.end method
