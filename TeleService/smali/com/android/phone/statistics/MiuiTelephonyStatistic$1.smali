.class Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiTelephonyStatistic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/statistics/MiuiTelephonyStatistic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;


# direct methods
.method constructor <init>(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .line 297
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v8, -0x1

    const/4 v4, 0x0

    .line 300
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 301
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_0

    return-void

    .line 302
    :cond_0
    const-string/jumbo v5, "MiuiTelephonyStatistic"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "mIntentReceiver action="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const-string/jumbo v5, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 304
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {v3, v4, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 321
    :cond_1
    :goto_0
    return-void

    .line 305
    :cond_2
    const-string/jumbo v5, "android.intent.action.ACTION_IMS_REGISTED"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 306
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v3

    const/16 v4, 0x9

    invoke-virtual {v3, v4, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 307
    :cond_3
    const-string/jumbo v5, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 308
    iget-object v5, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get0(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string/jumbo v7, "airplane_mode_on"

    invoke-static {v6, v7, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v3, :cond_4

    :goto_1
    invoke-static {v5, v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-set1(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Z)Z

    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_1

    .line 309
    :cond_5
    const-string/jumbo v3, "miui.intent.action.ACTION_DEFAULT_DATA_SLOT_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 310
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 311
    :cond_6
    const-string/jumbo v3, "miui.intent.action.CLOUD_MODEM_LOG"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 312
    const-string/jumbo v3, "ruleEventId"

    invoke-virtual {p2, v3, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 313
    .local v2, "ruleEventId":I
    if-eq v2, v8, :cond_1

    .line 314
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v3

    .line 315
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 314
    const/16 v5, 0xf

    invoke-virtual {v3, v5, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 317
    .end local v2    # "ruleEventId":I
    :cond_7
    const-string/jumbo v3, "telephonyStatistics.intent.debugall"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 318
    const-string/jumbo v3, "debug_all"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 319
    .local v1, "debugAll":Z
    iget-object v3, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$1;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/16 v5, 0x64

    invoke-virtual {v3, v5, v4}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0
.end method
