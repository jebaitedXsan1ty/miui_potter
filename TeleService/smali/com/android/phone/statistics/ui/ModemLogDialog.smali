.class public Lcom/android/phone/statistics/ui/ModemLogDialog;
.super Lcom/android/phone/statistics/ui/BaseErrorDialog;
.source "ModemLogDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/statistics/ui/ModemLogDialog$DialogCallBack;
    }
.end annotation


# instance fields
.field private mRuleEventId:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/statistics/ui/ModemLogDialog;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/statistics/ui/ModemLogDialog;

    .prologue
    iget v0, p0, Lcom/android/phone/statistics/ui/ModemLogDialog;->mRuleEventId:I

    return v0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/phone/statistics/ui/ModemLogDialog$DialogCallBack;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callBack"    # Lcom/android/phone/statistics/ui/ModemLogDialog$DialogCallBack;
    .param p3, "ruleEventId"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/android/phone/statistics/ui/BaseErrorDialog;-><init>(Landroid/content/Context;)V

    .line 27
    const-string/jumbo v2, "ModemLogDialog"

    const-string/jumbo v3, "ModemLogDialog enter"

    invoke-static {v2, v3}, Lcom/android/phone/statistics/utils/Utils;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    iput p3, p0, Lcom/android/phone/statistics/ui/ModemLogDialog;->mRuleEventId:I

    .line 29
    invoke-virtual {p0}, Lcom/android/phone/statistics/ui/ModemLogDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 30
    .local v0, "attrs":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v2, v2, 0x110

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 32
    invoke-virtual {p0}, Lcom/android/phone/statistics/ui/ModemLogDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 35
    .local v1, "r":Landroid/content/res/Resources;
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/phone/statistics/ui/ModemLogDialog;->setCancelable(Z)V

    .line 36
    const v2, 0x7f0b071f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/phone/statistics/ui/ModemLogDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 37
    const v2, 0x7f0b0720

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/phone/statistics/ui/ModemLogDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 38
    const v2, 0x7f0b0722

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/android/phone/statistics/ui/ModemLogDialog$1;

    invoke-direct {v3, p0, p2}, Lcom/android/phone/statistics/ui/ModemLogDialog$1;-><init>(Lcom/android/phone/statistics/ui/ModemLogDialog;Lcom/android/phone/statistics/ui/ModemLogDialog$DialogCallBack;)V

    const/4 v4, -0x1

    invoke-virtual {p0, v4, v2, v3}, Lcom/android/phone/statistics/ui/ModemLogDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 45
    const v2, 0x7f0b0721

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/android/phone/statistics/ui/ModemLogDialog$2;

    invoke-direct {v3, p0, p2}, Lcom/android/phone/statistics/ui/ModemLogDialog$2;-><init>(Lcom/android/phone/statistics/ui/ModemLogDialog;Lcom/android/phone/statistics/ui/ModemLogDialog$DialogCallBack;)V

    const/4 v4, -0x2

    invoke-virtual {p0, v4, v2, v3}, Lcom/android/phone/statistics/ui/ModemLogDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 52
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Lcom/android/phone/statistics/ui/BaseErrorDialog;->dismiss()V

    .line 57
    return-void
.end method
