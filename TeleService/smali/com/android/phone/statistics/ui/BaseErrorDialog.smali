.class public Lcom/android/phone/statistics/ui/BaseErrorDialog;
.super Lmiui/app/AlertDialog;
.source "BaseErrorDialog.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v3, 0x20000

    .line 10
    sget v1, Lmiui/R$style;->Theme_Light_Dialog_Alert:I

    invoke-direct {p0, p1, v1}, Lmiui/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    .line 12
    invoke-virtual {p0}, Lcom/android/phone/statistics/ui/BaseErrorDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d3

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 13
    invoke-virtual {p0}, Lcom/android/phone/statistics/ui/BaseErrorDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 15
    invoke-virtual {p0}, Lcom/android/phone/statistics/ui/BaseErrorDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 16
    .local v0, "attrs":Landroid/view/WindowManager$LayoutParams;
    const-string/jumbo v1, "Error Dialog"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 17
    invoke-virtual {p0}, Lcom/android/phone/statistics/ui/BaseErrorDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 18
    const v1, 0x1010355

    invoke-virtual {p0, v1}, Lcom/android/phone/statistics/ui/BaseErrorDialog;->setIconAttribute(I)V

    .line 19
    return-void
.end method
