.class public Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;
.super Landroid/os/Handler;
.source "MiuiTelephonyStatistic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/statistics/MiuiTelephonyStatistic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MiuiTelephonyStatisticHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;


# direct methods
.method public constructor <init>(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 383
    iput-object p1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .line 384
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 385
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 389
    const/4 v1, 0x0

    .line 390
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 391
    .local v0, "ar":Landroid/os/AsyncResult;
    iget v6, p1, Landroid/os/Message;->what:I

    sparse-switch v6, :sswitch_data_0

    .line 461
    .end local v0    # "ar":Landroid/os/AsyncResult;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 393
    .restart local v0    # "ar":Landroid/os/AsyncResult;
    .restart local v1    # "intent":Landroid/content/Intent;
    :sswitch_0
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap0(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)V

    goto :goto_0

    .line 396
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "ar":Landroid/os/AsyncResult;
    check-cast v0, Landroid/os/AsyncResult;

    .line 397
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget-object v6, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v6, Lcom/android/internal/telephony/Connection;

    invoke-static {v7, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap1(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Lcom/android/internal/telephony/Connection;)V

    goto :goto_0

    .line 400
    .local v0, "ar":Landroid/os/AsyncResult;
    :sswitch_2
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget v8, p1, Landroid/os/Message;->arg1:I

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/telephony/ServiceState;

    invoke-static {v7, v8, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap11(Lcom/android/phone/statistics/MiuiTelephonyStatistic;ILandroid/telephony/ServiceState;)V

    goto :goto_0

    .line 403
    :sswitch_3
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget v8, p1, Landroid/os/Message;->arg1:I

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/telephony/CellLocation;

    invoke-static {v7, v8, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap2(Lcom/android/phone/statistics/MiuiTelephonyStatistic;ILandroid/telephony/CellLocation;)V

    goto :goto_0

    .line 406
    :sswitch_4
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/content/Intent;

    invoke-static {v7, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap13(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Landroid/content/Intent;)V

    goto :goto_0

    .line 409
    :sswitch_5
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v1    # "intent":Landroid/content/Intent;
    check-cast v1, Landroid/content/Intent;

    .line 410
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v6, "old_data_slot"

    sget v7, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 411
    .local v3, "oldSlotId":I
    sget-object v6, Lmiui/telephony/SubscriptionManager;->SLOT_KEY:Ljava/lang/String;

    sget v7, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 412
    .local v2, "newSlotId":I
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v6, v2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-set0(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)I

    .line 413
    const-string/jumbo v6, "MiuiTelephonyStatistic"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "ACTION_DEFAULT_DATA_SLOT_CHANGED oldSlotId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", newSlotId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/phone/statistics/utils/Utils;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    invoke-static {v3}, Lmiui/telephony/SubscriptionManager;->isRealSlotId(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 415
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    new-instance v7, Landroid/telephony/ServiceState;

    invoke-direct {v7}, Landroid/telephony/ServiceState;-><init>()V

    invoke-static {v6, v7, v3, v10}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap16(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Landroid/telephony/ServiceState;IZ)V

    .line 416
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get1(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)[J

    move-result-object v6

    const-wide/16 v8, -0x1

    aput-wide v8, v6, v3

    .line 418
    :cond_1
    invoke-static {v2}, Lmiui/telephony/SubscriptionManager;->isRealSlotId(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 419
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v7}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get2(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)[Landroid/telephony/ServiceState;

    move-result-object v7

    aget-object v7, v7, v2

    invoke-static {v6, v7, v2, v10}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap16(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Landroid/telephony/ServiceState;IZ)V

    goto/16 :goto_0

    .line 423
    .end local v2    # "newSlotId":I
    .end local v3    # "oldSlotId":I
    .local v1, "intent":Landroid/content/Intent;
    :sswitch_6
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v1    # "intent":Landroid/content/Intent;
    check-cast v1, Landroid/content/Intent;

    .line 424
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v6, "phone"

    sget v7, Lmiui/telephony/SubscriptionManager;->INVALID_PHONE_ID:I

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 425
    .local v4, "phoneId":I
    const-string/jumbo v6, "state"

    invoke-virtual {v1, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 426
    .local v5, "registed":Z
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v6, v4, v5, v8}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap15(Lcom/android/phone/statistics/MiuiTelephonyStatistic;IZZ)V

    goto/16 :goto_0

    .line 429
    .end local v4    # "phoneId":I
    .end local v5    # "registed":Z
    .local v1, "intent":Landroid/content/Intent;
    :sswitch_7
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget v8, p1, Landroid/os/Message;->arg1:I

    iget v9, p1, Landroid/os/Message;->arg2:I

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Ljava/util/HashMap;

    invoke-static {v7, v8, v9, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap4(Lcom/android/phone/statistics/MiuiTelephonyStatistic;IILjava/util/HashMap;)V

    goto/16 :goto_0

    .line 432
    :sswitch_8
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget v7, p1, Landroid/os/Message;->arg1:I

    iget v8, p1, Landroid/os/Message;->arg2:I

    invoke-static {v6, v7, v8}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;II)V

    goto/16 :goto_0

    .line 435
    :sswitch_9
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget v8, p1, Landroid/os/Message;->arg1:I

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/telephony/SignalStrength;

    invoke-static {v7, v8, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap12(Lcom/android/phone/statistics/MiuiTelephonyStatistic;ILandroid/telephony/SignalStrength;)V

    goto/16 :goto_0

    .line 438
    :sswitch_a
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v7, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap8(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)V

    goto/16 :goto_0

    .line 441
    :sswitch_b
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v7, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap9(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)V

    goto/16 :goto_0

    .line 444
    :sswitch_c
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget v7, p1, Landroid/os/Message;->arg1:I

    iget v8, p1, Landroid/os/Message;->arg2:I

    invoke-static {v6, v7, v8}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap10(Lcom/android/phone/statistics/MiuiTelephonyStatistic;II)V

    goto/16 :goto_0

    .line 447
    :sswitch_d
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v7, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap14(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)V

    goto/16 :goto_0

    .line 450
    :sswitch_e
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v7, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap6(Lcom/android/phone/statistics/MiuiTelephonyStatistic;I)V

    goto/16 :goto_0

    .line 453
    :sswitch_f
    iget-object v6, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap7(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)V

    goto/16 :goto_0

    .line 456
    :sswitch_10
    iget-object v7, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-static {v7, v6}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-wrap5(Lcom/android/phone/statistics/MiuiTelephonyStatistic;Z)V

    goto/16 :goto_0

    .line 391
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x5 -> :sswitch_7
        0x6 -> :sswitch_0
        0x7 -> :sswitch_4
        0x8 -> :sswitch_5
        0x9 -> :sswitch_6
        0xa -> :sswitch_8
        0xb -> :sswitch_9
        0xc -> :sswitch_a
        0xd -> :sswitch_b
        0xe -> :sswitch_c
        0xf -> :sswitch_d
        0x10 -> :sswitch_e
        0x11 -> :sswitch_f
        0x64 -> :sswitch_10
    .end sparse-switch
.end method
