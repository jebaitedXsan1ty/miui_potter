.class Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;
.super Ljava/lang/Object;
.source "MiuiTelephonyStatistic.java"

# interfaces
.implements Lmiui/telephony/TelephonyStatAdapter$CallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/statistics/MiuiTelephonyStatistic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TelephonyCallBack"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;


# direct methods
.method constructor <init>(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    .prologue
    .line 1109
    iput-object p1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCellLocationChanged(ILandroid/telephony/CellLocation;)V
    .locals 3
    .param p1, "phoneId"    # I
    .param p2, "cellLocation"    # Landroid/telephony/CellLocation;

    .prologue
    .line 1136
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v0

    const/4 v1, 0x3

    .line 1137
    const/4 v2, 0x0

    .line 1136
    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1138
    return-void
.end method

.method public onDataConnectionChanged(II)V
    .locals 2
    .param p1, "phoneId"    # I
    .param p2, "dataState"    # I

    .prologue
    .line 1118
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1120
    return-void
.end method

.method public onDataStatisticChanged(Lmiui/telephony/TelephonyStatAdapter$DataStatType;ILjava/util/Map;)V
    .locals 3
    .param p1, "dataStatType"    # Lmiui/telephony/TelephonyStatAdapter$DataStatType;
    .param p2, "phoneId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/telephony/TelephonyStatAdapter$DataStatType;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1112
    .local p3, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v0

    .line 1113
    invoke-virtual {p1}, Lmiui/telephony/TelephonyStatAdapter$DataStatType;->ordinal()I

    move-result v1

    .line 1112
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1, p2, p3}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1114
    return-void
.end method

.method public onPreciseCallStateChanged(II)V
    .locals 2
    .param p1, "phoneId"    # I
    .param p2, "foregroundCallState"    # I

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1144
    return-void
.end method

.method public onServiceStateChanged(ILandroid/telephony/ServiceState;)V
    .locals 3
    .param p1, "phoneId"    # I
    .param p2, "ss"    # Landroid/telephony/ServiceState;

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v0

    const/4 v1, 0x2

    .line 1131
    const/4 v2, 0x0

    .line 1130
    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1132
    return-void
.end method

.method public onSignalStrengthChanged(ILandroid/telephony/SignalStrength;)V
    .locals 3
    .param p1, "phoneId"    # I
    .param p2, "signal"    # Landroid/telephony/SignalStrength;

    .prologue
    .line 1124
    iget-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$TelephonyCallBack;->this$0:Lcom/android/phone/statistics/MiuiTelephonyStatistic;

    invoke-static {v0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic;->-get3(Lcom/android/phone/statistics/MiuiTelephonyStatistic;)Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;

    move-result-object v0

    const/16 v1, 0xb

    .line 1125
    const/4 v2, 0x0

    .line 1124
    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$MiuiTelephonyStatisticHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 1126
    return-void
.end method
