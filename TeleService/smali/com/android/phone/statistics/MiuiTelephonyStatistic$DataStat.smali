.class public Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;
.super Ljava/lang/Object;
.source "MiuiTelephonyStatistic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/statistics/MiuiTelephonyStatistic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DataStat"
.end annotation


# instance fields
.field public apn:Ljava/lang/String;

.field public apnType:Ljava/lang/String;

.field public cause:Ljava/lang/String;

.field public reason:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1071
    invoke-virtual {p0}, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->reset()V

    .line 1072
    return-void
.end method


# virtual methods
.method public reset()V
    .locals 1

    .prologue
    .line 1075
    const-string/jumbo v0, "NA"

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->apn:Ljava/lang/String;

    .line 1076
    const-string/jumbo v0, "NA"

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->apnType:Ljava/lang/String;

    .line 1077
    const-string/jumbo v0, "NA"

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->reason:Ljava/lang/String;

    .line 1078
    const-string/jumbo v0, "NA"

    iput-object v0, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->cause:Ljava/lang/String;

    .line 1079
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1083
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{DataStat apn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->apn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " apnType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->apnType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1084
    const-string/jumbo v1, " reason="

    .line 1083
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1084
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->reason:Ljava/lang/String;

    .line 1083
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1084
    const-string/jumbo v1, " cause="

    .line 1083
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1084
    iget-object v1, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->cause:Ljava/lang/String;

    .line 1083
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1084
    const-string/jumbo v1, "}"

    .line 1083
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateDataStat(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1088
    .local p1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_4

    .line 1089
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 1090
    .local v0, "entries":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "entry$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1091
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1092
    .local v3, "key":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1093
    .local v4, "value":Ljava/lang/String;
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    .line 1094
    const-string/jumbo v5, "apnName"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1095
    iput-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->apn:Ljava/lang/String;

    goto :goto_0

    .line 1096
    :cond_1
    const-string/jumbo v5, "apnType"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1097
    iput-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->apnType:Ljava/lang/String;

    goto :goto_0

    .line 1098
    :cond_2
    const-string/jumbo v5, "reasonCode"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1099
    iput-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->reason:Ljava/lang/String;

    goto :goto_0

    .line 1100
    :cond_3
    const-string/jumbo v5, "errorCode"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1101
    iput-object v4, p0, Lcom/android/phone/statistics/MiuiTelephonyStatistic$DataStat;->cause:Ljava/lang/String;

    goto :goto_0

    .line 1106
    .end local v0    # "entries":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "entry$iterator":Ljava/util/Iterator;
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "value":Ljava/lang/String;
    :cond_4
    return-void
.end method
