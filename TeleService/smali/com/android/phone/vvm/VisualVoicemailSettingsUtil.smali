.class public Lcom/android/phone/vvm/VisualVoicemailSettingsUtil;
.super Ljava/lang/Object;
.source "VisualVoicemailSettingsUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dump(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/os/Bundle;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 33
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 34
    .local v1, "result":Landroid/os/Bundle;
    new-instance v0, Lcom/android/phone/vvm/VisualVoicemailPreferences;

    invoke-direct {v0, p0, p1}, Lcom/android/phone/vvm/VisualVoicemailPreferences;-><init>(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 36
    .local v0, "prefs":Lcom/android/phone/vvm/VisualVoicemailPreferences;
    const-string/jumbo v2, "is_enabled"

    invoke-virtual {v0, v2}, Lcom/android/phone/vvm/VisualVoicemailPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    const-string/jumbo v2, "android.telephony.extra.VISUAL_VOICEMAIL_ENABLED_BY_USER_BOOL"

    .line 38
    const-string/jumbo v3, "is_enabled"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/android/phone/vvm/VisualVoicemailPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 37
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 40
    :cond_0
    const-string/jumbo v2, "android.telephony.extra.VOICEMAIL_SCRAMBLED_PIN_STRING"

    .line 41
    const-string/jumbo v3, "default_old_pin"

    invoke-virtual {v0, v3}, Lcom/android/phone/vvm/VisualVoicemailPreferences;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 40
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    return-object v1
.end method
