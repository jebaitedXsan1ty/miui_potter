.class Lcom/android/phone/vvm/RemoteVvmTaskManager$1;
.super Landroid/os/Handler;
.source "RemoteVvmTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/vvm/RemoteVvmTaskManager;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;


# direct methods
.method constructor <init>(Lcom/android/phone/vvm/RemoteVvmTaskManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$1;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .line 182
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 185
    invoke-static {}, Lcom/android/phone/Assert;->isMainThread()V

    .line 186
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 192
    const-string/jumbo v0, "RemoteVvmTaskManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unexpected message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/vvm/VvmLog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :goto_0
    return-void

    .line 188
    :pswitch_0
    iget-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$1;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    invoke-static {v0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->-get1(Lcom/android/phone/vvm/RemoteVvmTaskManager;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->-set1(Lcom/android/phone/vvm/RemoteVvmTaskManager;I)I

    .line 189
    iget-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$1;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    invoke-static {v0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->-wrap1(Lcom/android/phone/vvm/RemoteVvmTaskManager;)V

    goto :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
