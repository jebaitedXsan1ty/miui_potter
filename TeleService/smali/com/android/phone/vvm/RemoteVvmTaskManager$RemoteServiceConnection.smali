.class Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;
.super Ljava/lang/Object;
.source "RemoteVvmTaskManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/vvm/RemoteVvmTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemoteServiceConnection"
.end annotation


# instance fields
.field private mConnected:Z

.field private mRemoteMessenger:Landroid/os/Messenger;

.field private final mTaskQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;


# direct methods
.method private constructor <init>(Lcom/android/phone/vvm/RemoteVvmTaskManager;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mTaskQueue:Ljava/util/Queue;

    .line 246
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/vvm/RemoteVvmTaskManager;Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/vvm/RemoteVvmTaskManager;
    .param p2, "-this1"    # Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;-><init>(Lcom/android/phone/vvm/RemoteVvmTaskManager;)V

    return-void
.end method

.method private runQueue()V
    .locals 4

    .prologue
    .line 285
    invoke-static {}, Lcom/android/phone/Assert;->isMainThread()V

    .line 286
    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mTaskQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    .line 287
    .local v1, "message":Landroid/os/Message;
    :goto_0
    if-eqz v1, :cond_0

    .line 288
    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    invoke-static {v2}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->-get0(Lcom/android/phone/vvm/RemoteVvmTaskManager;)Landroid/os/Messenger;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 289
    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    invoke-static {v2}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->-wrap0(Lcom/android/phone/vvm/RemoteVvmTaskManager;)I

    move-result v2

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 292
    :try_start_0
    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mRemoteMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    :goto_1
    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mTaskQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "message":Landroid/os/Message;
    check-cast v1, Landroid/os/Message;

    .restart local v1    # "message":Landroid/os/Message;
    goto :goto_0

    .line 293
    :catch_0
    move-exception v0

    .line 294
    .local v0, "e":Landroid/os/RemoteException;
    const-string/jumbo v2, "RemoteVvmTaskManager"

    const-string/jumbo v3, "Error sending message to remote service"

    invoke-static {v2, v3, v0}, Lcom/android/phone/vvm/VvmLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 298
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    return-void
.end method


# virtual methods
.method public enqueue(Landroid/os/Message;)V
    .locals 1
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 258
    iget-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mTaskQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 259
    iget-boolean v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mConnected:Z

    if-eqz v0, :cond_0

    .line 260
    invoke-direct {p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->runQueue()V

    .line 262
    :cond_0
    return-void
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mConnected:Z

    return v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 270
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mRemoteMessenger:Landroid/os/Messenger;

    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mConnected:Z

    .line 272
    invoke-direct {p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->runQueue()V

    .line 273
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 276
    iget-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    invoke-static {v0, v1}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->-set0(Lcom/android/phone/vvm/RemoteVvmTaskManager;Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;)Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    .line 277
    iput-boolean v3, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mConnected:Z

    .line 278
    iput-object v1, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->mRemoteMessenger:Landroid/os/Messenger;

    .line 279
    const-string/jumbo v0, "RemoteVvmTaskManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Service disconnected, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    invoke-static {v2}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->-get1(Lcom/android/phone/vvm/RemoteVvmTaskManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " tasks dropped."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/vvm/VvmLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    invoke-static {v0, v3}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->-set1(Lcom/android/phone/vvm/RemoteVvmTaskManager;I)I

    .line 281
    iget-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->this$0:Lcom/android/phone/vvm/RemoteVvmTaskManager;

    invoke-static {v0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->-wrap1(Lcom/android/phone/vvm/RemoteVvmTaskManager;)V

    .line 282
    return-void
.end method
