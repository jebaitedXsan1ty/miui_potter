.class public Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;
.super Ljava/lang/Object;
.source "VisualVoicemailSmsFilterConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;,
        Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;
    }
.end annotation


# direct methods
.method static synthetic -wrap0(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    invoke-static {p0}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "subId"    # I

    .prologue
    invoke-static {p0, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;->makePerPhoneAccountKeyPrefix(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static disableVisualVoicemailSmsFilter(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "subId"    # I

    .prologue
    .line 60
    new-instance v0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 61
    const-string/jumbo v1, "_enabled"

    const/4 v2, 0x0

    .line 60
    invoke-static {v0, v1, v2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->-wrap0(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;Ljava/lang/String;Z)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->apply()V

    .line 63
    return-void
.end method

.method public static enableVisualVoicemailSmsFilter(Landroid/content/Context;Ljava/lang/String;ILandroid/telephony/VisualVoicemailSmsFilterSettings;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "subId"    # I
    .param p3, "settings"    # Landroid/telephony/VisualVoicemailSmsFilterSettings;

    .prologue
    .line 50
    new-instance v0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 51
    const-string/jumbo v1, "_enabled"

    const/4 v2, 0x1

    .line 50
    invoke-static {v0, v1, v2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->-wrap0(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;Ljava/lang/String;Z)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    move-result-object v0

    .line 52
    const-string/jumbo v1, "_prefix"

    iget-object v2, p3, Landroid/telephony/VisualVoicemailSmsFilterSettings;->clientPrefix:Ljava/lang/String;

    .line 50
    invoke-static {v0, v1, v2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->-wrap3(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;Ljava/lang/String;Ljava/lang/String;)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    move-result-object v0

    .line 53
    const-string/jumbo v1, "_originating_numbers"

    iget-object v2, p3, Landroid/telephony/VisualVoicemailSmsFilterSettings;->originatingNumbers:Ljava/util/List;

    .line 50
    invoke-static {v0, v1, v2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->-wrap2(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;Ljava/lang/String;Ljava/util/List;)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    move-result-object v0

    .line 54
    const-string/jumbo v1, "_destination_port"

    iget v2, p3, Landroid/telephony/VisualVoicemailSmsFilterSettings;->destinationPort:I

    .line 50
    invoke-static {v0, v1, v2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->-wrap1(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;Ljava/lang/String;I)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->apply()V

    .line 56
    return-void
.end method

.method public static getActiveVisualVoicemailSmsFilterSettings(Landroid/content/Context;I)Landroid/telephony/VisualVoicemailSmsFilterSettings;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # I

    .prologue
    .line 67
    invoke-static {p0, p1}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->getRemotePackage(Landroid/content/Context;I)Landroid/content/ComponentName;

    move-result-object v0

    .line 69
    .local v0, "componentName":Landroid/content/ComponentName;
    if-nez v0, :cond_0

    .line 70
    const-string/jumbo v1, "com.android.phone"

    .line 74
    .local v1, "packageName":Ljava/lang/String;
    :goto_0
    invoke-static {p0, v1, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;->getVisualVoicemailSmsFilterSettings(Landroid/content/Context;Ljava/lang/String;I)Landroid/telephony/VisualVoicemailSmsFilterSettings;

    move-result-object v2

    return-object v2

    .line 72
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "packageName":Ljava/lang/String;
    goto :goto_0
.end method

.method private static getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    invoke-virtual {p0}, Landroid/content/Context;->createDeviceProtectedStorageContext()Landroid/content/Context;

    move-result-object v0

    .line 99
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getVisualVoicemailSmsFilterSettings(Landroid/content/Context;Ljava/lang/String;I)Landroid/telephony/VisualVoicemailSmsFilterSettings;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "subId"    # I

    .prologue
    .line 84
    new-instance v0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 85
    .local v0, "reader":Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;
    const-string/jumbo v1, "_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->-wrap0(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    const/4 v1, 0x0

    return-object v1

    .line 88
    :cond_0
    new-instance v1, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;

    invoke-direct {v1}, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;-><init>()V

    .line 89
    const-string/jumbo v2, "_prefix"

    .line 90
    const-string/jumbo v3, "//VVM"

    .line 89
    invoke-static {v0, v2, v3}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->-wrap2(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 88
    invoke-virtual {v1, v2}, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;->setClientPrefix(Ljava/lang/String;)Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;

    move-result-object v1

    .line 91
    const-string/jumbo v2, "_originating_numbers"

    .line 92
    sget-object v3, Landroid/telephony/VisualVoicemailSmsFilterSettings;->DEFAULT_ORIGINATING_NUMBERS:Ljava/util/List;

    .line 91
    invoke-static {v0, v2, v3}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->-wrap3(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 88
    invoke-virtual {v1, v2}, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;->setOriginatingNumbers(Ljava/util/List;)Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;

    move-result-object v1

    .line 93
    const-string/jumbo v2, "_destination_port"

    .line 94
    const/4 v3, -0x1

    .line 93
    invoke-static {v0, v2, v3}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;->-wrap1(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Reader;Ljava/lang/String;I)I

    move-result v2

    .line 88
    invoke-virtual {v1, v2}, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;->setDestinationPort(I)Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/VisualVoicemailSmsFilterSettings$Builder;->build()Landroid/telephony/VisualVoicemailSmsFilterSettings;

    move-result-object v1

    return-object v1
.end method

.method private static makePerPhoneAccountKeyPrefix(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "subId"    # I

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "vvm_sms_filter_config_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
