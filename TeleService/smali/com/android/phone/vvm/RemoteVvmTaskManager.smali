.class public Lcom/android/phone/vvm/RemoteVvmTaskManager;
.super Landroid/app/Service;
.source "RemoteVvmTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;
    }
.end annotation


# instance fields
.field private mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

.field private mMessenger:Landroid/os/Messenger;

.field private mTaskReferenceCount:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/vvm/RemoteVvmTaskManager;)Landroid/os/Messenger;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .prologue
    iget-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mMessenger:Landroid/os/Messenger;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/vvm/RemoteVvmTaskManager;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .prologue
    iget v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mTaskReferenceCount:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/phone/vvm/RemoteVvmTaskManager;Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;)Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/vvm/RemoteVvmTaskManager;
    .param p1, "-value"    # Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    .prologue
    iput-object p1, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/phone/vvm/RemoteVvmTaskManager;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/vvm/RemoteVvmTaskManager;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mTaskReferenceCount:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/vvm/RemoteVvmTaskManager;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->getTaskId()I

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/vvm/RemoteVvmTaskManager;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->checkReference()V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private checkReference()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 338
    iget-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    if-nez v0, :cond_0

    .line 339
    return-void

    .line 341
    :cond_0
    iget v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mTaskReferenceCount:I

    if-nez v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    invoke-virtual {p0, v0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->unbindService(Landroid/content/ServiceConnection;)V

    .line 343
    iput-object v1, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    .line 345
    :cond_1
    return-void
.end method

.method private static getBroadcastPackage(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 165
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.phone.vvm.ACTION_VISUAL_VOICEMAIL_SERVICE_EVENT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-class v2, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/TelecomManager;

    invoke-virtual {v2}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;

    move-result-object v2

    .line 166
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 169
    const/high16 v3, 0x20000

    .line 168
    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 170
    .local v1, "info":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v1, :cond_0

    .line 171
    return-object v4

    .line 173
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    return-object v4

    .line 176
    :cond_1
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v2}, Landroid/content/pm/ResolveInfo;->getComponentInfo()Landroid/content/pm/ComponentInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/pm/ComponentInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    return-object v2
.end method

.method public static getRemotePackage(Landroid/content/Context;I)Landroid/content/ComponentName;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x0

    .line 112
    invoke-static {p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->getBroadcastPackage(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v1

    .line 113
    .local v1, "broadcastPackage":Landroid/content/ComponentName;
    if-eqz v1, :cond_0

    .line 114
    return-object v1

    .line 117
    :cond_0
    invoke-static {p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->newBindIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 119
    .local v0, "bindIntent":Landroid/content/Intent;
    const-class v9, Landroid/telecom/TelecomManager;

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telecom/TelecomManager;

    .line 120
    .local v7, "telecomManager":Landroid/telecom/TelecomManager;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v6, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v7}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    const-class v9, Landroid/telephony/CarrierConfigManager;

    .line 122
    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/telephony/CarrierConfigManager;

    invoke-virtual {v9, p1}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v2

    .line 125
    .local v2, "carrierConfig":Landroid/os/PersistableBundle;
    const-string/jumbo v9, "carrier_vvm_package_name_string"

    invoke-virtual {v2, v9}, Landroid/os/PersistableBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 124
    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    const-string/jumbo v9, "carrier_vvm_package_name_string_array"

    .line 126
    invoke-virtual {v2, v9}, Landroid/os/PersistableBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 128
    .local v8, "vvmPackages":[Ljava/lang/String;
    if-eqz v8, :cond_1

    array-length v9, v8

    if-lez v9, :cond_1

    .line 129
    array-length v11, v8

    move v9, v10

    :goto_0
    if-ge v9, v11, :cond_1

    aget-object v4, v8, v9

    .line 130
    .local v4, "packageName":Ljava/lang/String;
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 133
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b0272

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    invoke-virtual {v7}, Landroid/telecom/TelecomManager;->getSystemDialerPackage()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "packageName$iterator":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 136
    .restart local v4    # "packageName":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 139
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 141
    const/high16 v10, 0x20000

    .line 140
    invoke-virtual {v9, v0, v10}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 142
    .local v3, "info":Landroid/content/pm/ResolveInfo;
    if-eqz v3, :cond_2

    .line 145
    iget-object v9, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-nez v9, :cond_3

    .line 146
    const-string/jumbo v9, "RemoteVvmTaskManager"

    .line 147
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "Component "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Landroid/content/pm/ResolveInfo;->getComponentInfo()Landroid/content/pm/ComponentInfo;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " is not a service, ignoring"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 146
    invoke-static {v9, v10}, Lcom/android/phone/vvm/VvmLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 150
    :cond_3
    const-string/jumbo v9, "android.permission.BIND_VISUAL_VOICEMAIL_SERVICE"

    .line 151
    iget-object v10, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v10, v10, Landroid/content/pm/ServiceInfo;->permission:Ljava/lang/String;

    .line 150
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 152
    const-string/jumbo v9, "RemoteVvmTaskManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "package "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v11, v11, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 153
    const-string/jumbo v11, " does not enforce BIND_VISUAL_VOICEMAIL_SERVICE, ignoring"

    .line 152
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/phone/vvm/VvmLog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 157
    :cond_4
    invoke-virtual {v3}, Landroid/content/pm/ResolveInfo;->getComponentInfo()Landroid/content/pm/ComponentInfo;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/pm/ComponentInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v9

    return-object v9

    .line 160
    .end local v3    # "info":Landroid/content/pm/ResolveInfo;
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_5
    return-object v12
.end method

.method private getTaskId()I
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x1

    return v0
.end method

.method public static hasRemoteService(Landroid/content/Context;I)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "subId"    # I

    .prologue
    .line 107
    invoke-static {p0, p1}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->getRemotePackage(Landroid/content/Context;I)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static newBindIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 348
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 349
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.telephony.VisualVoicemailService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    return-object v0
.end method

.method private send(Landroid/content/ComponentName;ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "remotePackage"    # Landroid/content/ComponentName;
    .param p2, "what"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 302
    invoke-static {}, Lcom/android/phone/Assert;->isMainThread()V

    .line 304
    invoke-static {p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->getBroadcastPackage(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 312
    const-string/jumbo v2, "RemoteVvmTaskManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sending broadcast "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/phone/vvm/VvmLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.phone.vvm.ACTION_VISUAL_VOICEMAIL_SERVICE_EVENT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 314
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 315
    const-string/jumbo v2, "what"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 316
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 317
    invoke-virtual {p0, v0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 318
    return-void

    .line 321
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 322
    .local v1, "message":Landroid/os/Message;
    iput p2, v1, Landroid/os/Message;->what:I

    .line 323
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 324
    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    if-nez v2, :cond_1

    .line 325
    new-instance v2, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    invoke-direct {v2, p0, v3}, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;-><init>(Lcom/android/phone/vvm/RemoteVvmTaskManager;Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;)V

    iput-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    .line 327
    :cond_1
    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    invoke-virtual {v2, v1}, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->enqueue(Landroid/os/Message;)V

    .line 329
    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    invoke-virtual {v2}, Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;->isConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 330
    invoke-static {p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->newBindIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 331
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 332
    const-string/jumbo v2, "RemoteVvmTaskManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Binding to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/phone/vvm/VvmLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v2, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mConnection:Lcom/android/phone/vvm/RemoteVvmTaskManager$RemoteServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 335
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    return-void
.end method

.method public static startCellServiceConnected(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "ACTION_START_CELL_SERVICE_CONNECTED"

    .line 85
    const-class v2, Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .line 84
    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, p0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "data_phone_account_handle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 88
    return-void
.end method

.method public static startSimRemoved(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 100
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "ACTION_START_SIM_REMOVED"

    .line 101
    const-class v2, Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .line 100
    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, p0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 102
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "data_phone_account_handle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 103
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 104
    return-void
.end method

.method public static startSmsReceived(Landroid/content/Context;Landroid/telephony/VisualVoicemailSms;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sms"    # Landroid/telephony/VisualVoicemailSms;

    .prologue
    .line 91
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "ACTION_START_SMS_RECEIVED"

    .line 92
    const-class v2, Lcom/android/phone/vvm/RemoteVvmTaskManager;

    .line 91
    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, p0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "data_phone_account_handle"

    .line 94
    invoke-virtual {p1}, Landroid/telephony/VisualVoicemailSms;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 93
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 95
    const-string/jumbo v1, "data_sms"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 96
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 97
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 181
    invoke-static {}, Lcom/android/phone/Assert;->isMainThread()V

    .line 182
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/android/phone/vvm/RemoteVvmTaskManager$1;

    invoke-direct {v1, p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager$1;-><init>(Lcom/android/phone/vvm/RemoteVvmTaskManager;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mMessenger:Landroid/os/Messenger;

    .line 196
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v6, 0x2

    .line 200
    invoke-static {}, Lcom/android/phone/Assert;->isMainThread()V

    .line 201
    iget v3, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mTaskReferenceCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/phone/vvm/RemoteVvmTaskManager;->mTaskReferenceCount:I

    .line 203
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 204
    const-string/jumbo v4, "data_phone_account_handle"

    .line 203
    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    .line 205
    .local v0, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    invoke-static {v0}, Lcom/android/phone/vvm/PhoneAccountHandleConverter;->toSubId(Landroid/telecom/PhoneAccountHandle;)I

    move-result v2

    .line 206
    .local v2, "subId":I
    invoke-static {p0, v2}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->getRemotePackage(Landroid/content/Context;I)Landroid/content/ComponentName;

    move-result-object v1

    .line 207
    .local v1, "remotePackage":Landroid/content/ComponentName;
    if-nez v1, :cond_0

    .line 208
    const-string/jumbo v3, "RemoteVvmTaskManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "No service to handle "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", ignoring"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/phone/vvm/VvmLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-direct {p0}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->checkReference()V

    .line 210
    return v6

    .line 213
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "ACTION_START_CELL_SERVICE_CONNECTED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 216
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 215
    const/4 v4, 0x1

    invoke-direct {p0, v1, v4, v3}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->send(Landroid/content/ComponentName;ILandroid/os/Bundle;)V

    .line 229
    :goto_0
    return v6

    .line 213
    :cond_1
    const-string/jumbo v4, "ACTION_START_SMS_RECEIVED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 219
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {p0, v1, v6, v3}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->send(Landroid/content/ComponentName;ILandroid/os/Bundle;)V

    goto :goto_0

    .line 213
    :cond_2
    const-string/jumbo v4, "ACTION_START_SIM_REMOVED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 222
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const/4 v4, 0x3

    invoke-direct {p0, v1, v4, v3}, Lcom/android/phone/vvm/RemoteVvmTaskManager;->send(Landroid/content/ComponentName;ILandroid/os/Bundle;)V

    goto :goto_0

    .line 225
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unexpected action +"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/Assert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method
