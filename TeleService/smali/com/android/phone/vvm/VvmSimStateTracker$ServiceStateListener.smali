.class Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "VvmSimStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/vvm/VvmSimStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceStateListener"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

.field final synthetic this$0:Lcom/android/phone/vvm/VvmSimStateTracker;


# direct methods
.method public constructor <init>(Lcom/android/phone/vvm/VvmSimStateTracker;Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/vvm/VvmSimStateTracker;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->this$0:Lcom/android/phone/vvm/VvmSimStateTracker;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 77
    iput-object p2, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->mContext:Landroid/content/Context;

    .line 78
    iput-object p3, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->mPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    .line 79
    return-void
.end method


# virtual methods
.method public listen()V
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->mPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v1, v2}, Lcom/android/phone/vvm/VvmSimStateTracker;->-wrap0(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 83
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_0

    .line 84
    const-string/jumbo v1, "VvmSimStateTracker"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cannot create TelephonyManager from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->mPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/vvm/VvmLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    return-void

    .line 87
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 88
    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 3
    .param p1, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-nez v0, :cond_0

    .line 102
    const-string/jumbo v0, "VvmSimStateTracker"

    const-string/jumbo v1, "in service"

    invoke-static {v0, v1}, Lcom/android/phone/vvm/VvmLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->this$0:Lcom/android/phone/vvm/VvmSimStateTracker;

    iget-object v1, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->mPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    invoke-static {v0, v1, v2}, Lcom/android/phone/vvm/VvmSimStateTracker;->-wrap1(Lcom/android/phone/vvm/VvmSimStateTracker;Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)V

    .line 104
    invoke-virtual {p0}, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->unlisten()V

    .line 106
    :cond_0
    return-void
.end method

.method public unlisten()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->mContext:Landroid/content/Context;

    const-class v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 95
    const/4 v1, 0x0

    .line 94
    invoke-virtual {v0, p0, v1}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 96
    invoke-static {}, Lcom/android/phone/vvm/VvmSimStateTracker;->-get0()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/vvm/VvmSimStateTracker$ServiceStateListener;->mPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    return-void
.end method
