.class Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
.super Ljava/lang/Object;
.source "VisualVoicemailSmsFilterConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Editor"
.end annotation


# instance fields
.field private final mKeyPrefix:Ljava/lang/String;

.field private final mPrefsEditor:Landroid/content/SharedPreferences$Editor;


# direct methods
.method static synthetic -wrap0(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;Ljava/lang/String;Z)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->setBoolean(Ljava/lang/String;Z)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;Ljava/lang/String;I)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->setInt(Ljava/lang/String;I)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap2(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;Ljava/lang/String;Ljava/util/List;)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/util/List;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->setStringList(Ljava/lang/String;Ljava/util/List;)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap3(Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;Ljava/lang/String;Ljava/lang/String;)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->setString(Ljava/lang/String;Ljava/lang/String;)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;

    move-result-object v0

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "subId"    # I

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    invoke-static {p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;->-wrap0(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 118
    invoke-static {p2, p3}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig;->-wrap1(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->mKeyPrefix:Ljava/lang/String;

    .line 119
    return-void
.end method

.method private makeKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->mKeyPrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setBoolean(Ljava/lang/String;Z)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 132
    iget-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->makeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 133
    return-object p0
.end method

.method private setInt(Ljava/lang/String;I)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->makeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 123
    return-object p0
.end method

.method private setString(Ljava/lang/String;Ljava/lang/String;)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->makeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 128
    return-object p0
.end method

.method private setStringList(Ljava/lang/String;Ljava/util/List;)Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;"
        }
    .end annotation

    .prologue
    .line 137
    .local p2, "value":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, p1}, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->makeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2, p2}, Landroid/util/ArraySet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 138
    return-object p0
.end method


# virtual methods
.method public apply()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/phone/vvm/VisualVoicemailSmsFilterConfig$Editor;->mPrefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 143
    return-void
.end method
