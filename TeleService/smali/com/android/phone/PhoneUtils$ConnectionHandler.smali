.class Lcom/android/phone/PhoneUtils$ConnectionHandler;
.super Landroid/os/Handler;
.source "PhoneUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/PhoneUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConnectionHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/PhoneUtils$ConnectionHandler;)V
    .locals 0
    .param p1, "-this0"    # Lcom/android/phone/PhoneUtils$ConnectionHandler;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/PhoneUtils$ConnectionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0x8

    .line 171
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 173
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/phone/PhoneUtils$FgRingCalls;

    .line 176
    .local v0, "frC":Lcom/android/phone/PhoneUtils$FgRingCalls;
    invoke-static {v0}, Lcom/android/phone/PhoneUtils$FgRingCalls;->-get0(Lcom/android/phone/PhoneUtils$FgRingCalls;)Lcom/android/internal/telephony/Call;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 177
    invoke-static {v0}, Lcom/android/phone/PhoneUtils$FgRingCalls;->-get0(Lcom/android/phone/PhoneUtils$FgRingCalls;)Lcom/android/internal/telephony/Call;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/Call$State;->DISCONNECTING:Lcom/android/internal/telephony/Call$State;

    if-ne v2, v3, :cond_1

    .line 178
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ge v2, v4, :cond_1

    .line 180
    invoke-static {}, Lcom/android/phone/PhoneUtils;->-get0()Lcom/android/phone/PhoneUtils$ConnectionHandler;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/android/phone/PhoneUtils$ConnectionHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 181
    .local v1, "retryMsg":Landroid/os/Message;
    iget v2, p1, Landroid/os/Message;->arg1:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 182
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 183
    invoke-static {}, Lcom/android/phone/PhoneUtils;->-get0()Lcom/android/phone/PhoneUtils$ConnectionHandler;

    move-result-object v2

    .line 184
    const-wide/16 v4, 0xc8

    .line 183
    invoke-virtual {v2, v1, v4, v5}, Lcom/android/phone/PhoneUtils$ConnectionHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 188
    .end local v1    # "retryMsg":Landroid/os/Message;
    :cond_1
    invoke-static {v0}, Lcom/android/phone/PhoneUtils$FgRingCalls;->-get1(Lcom/android/phone/PhoneUtils$FgRingCalls;)Lcom/android/internal/telephony/Call;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->isRinging()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 189
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-ne v2, v4, :cond_2

    .line 190
    const-string/jumbo v2, "PhoneUtils"

    const-string/jumbo v3, "DISCONNECTING time out"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_2
    invoke-static {v0}, Lcom/android/phone/PhoneUtils$FgRingCalls;->-get1(Lcom/android/phone/PhoneUtils$FgRingCalls;)Lcom/android/internal/telephony/Call;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/PhoneUtils;->answerCall(Lcom/android/internal/telephony/Call;)Z

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
