.class public Lcom/android/phone/CdmaUnavailableAlertDialog;
.super Lmiui/app/Activity;
.source "CdmaUnavailableAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/CdmaUnavailableAlertDialog$1;,
        Lcom/android/phone/CdmaUnavailableAlertDialog$2;
    }
.end annotation


# static fields
.field private static sLastDataSlot:I


# instance fields
.field private final REMIND_USER_CDMA_ALERT_DIALOG_TYPE:I

.field private mNegativeOnClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mPositiveOnClickListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method static synthetic -wrap0(Lcom/android/phone/CdmaUnavailableAlertDialog;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/CdmaUnavailableAlertDialog;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/CdmaUnavailableAlertDialog;->isMobileNetworkSettingsFront()Z

    move-result v0

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    sget v0, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    sput v0, Lcom/android/phone/CdmaUnavailableAlertDialog;->sLastDataSlot:I

    .line 30
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/phone/CdmaUnavailableAlertDialog;->REMIND_USER_CDMA_ALERT_DIALOG_TYPE:I

    .line 113
    new-instance v0, Lcom/android/phone/CdmaUnavailableAlertDialog$1;

    invoke-direct {v0, p0}, Lcom/android/phone/CdmaUnavailableAlertDialog$1;-><init>(Lcom/android/phone/CdmaUnavailableAlertDialog;)V

    iput-object v0, p0, Lcom/android/phone/CdmaUnavailableAlertDialog;->mPositiveOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 125
    new-instance v0, Lcom/android/phone/CdmaUnavailableAlertDialog$2;

    invoke-direct {v0, p0}, Lcom/android/phone/CdmaUnavailableAlertDialog$2;-><init>(Lcom/android/phone/CdmaUnavailableAlertDialog;)V

    iput-object v0, p0, Lcom/android/phone/CdmaUnavailableAlertDialog;->mNegativeOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 30
    return-void
.end method

.method private isMobileNetworkSettingsFront()Z
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 133
    const-string/jumbo v2, "activity"

    invoke-virtual {p0, v2}, Lcom/android/phone/CdmaUnavailableAlertDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 134
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 135
    .local v1, "tastList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 136
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/android/phone/CdmaUnavailableAlertDialog;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 136
    if-eqz v2, :cond_0

    .line 138
    return v5

    .line 141
    :cond_0
    return v4
.end method

.method private static showCdmaUnavailableDialog()V
    .locals 5

    .prologue
    .line 76
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 77
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    const-class v3, Lcom/android/phone/CdmaUnavailableAlertDialog;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 78
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 80
    :try_start_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/phone/PhoneGlobals;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "anfe":Landroid/content/ActivityNotFoundException;
    const-string/jumbo v2, "CdmaUnavailableAlertDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "startActivity failed anfe="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/telephony/Rlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static showDialogIfNeeded()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 35
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    .line 36
    .local v0, "defaultDataSlot":I
    sget v5, Lcom/android/phone/CdmaUnavailableAlertDialog;->sLastDataSlot:I

    if-ne v5, v0, :cond_2

    .line 37
    const/4 v2, 0x1

    .line 38
    .local v2, "isCardNoChange":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v5, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    if-ge v1, v5, :cond_1

    if-eqz v2, :cond_1

    .line 39
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getInstance()Lmiui/telephony/DefaultSimManager;

    move-result-object v5

    invoke-virtual {v5, v1}, Lmiui/telephony/DefaultSimManager;->getSimInsertStates(I)I

    move-result v5

    if-nez v5, :cond_0

    const/4 v2, 0x1

    .line 38
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 39
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 41
    :cond_1
    if-eqz v2, :cond_2

    return-void

    .line 43
    .end local v1    # "i":I
    .end local v2    # "isCardNoChange":Z
    :cond_2
    sput v0, Lcom/android/phone/CdmaUnavailableAlertDialog;->sLastDataSlot:I

    .line 45
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v4

    .line 46
    .local v4, "subInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    if-eqz v4, :cond_3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-gt v5, v7, :cond_4

    .line 48
    :cond_3
    const-string/jumbo v5, "CdmaUnavailableAlertDialog"

    const-string/jumbo v6, "showCdmaUnavailableDialogIfNeeded return for sub not ready"

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void

    .line 52
    :cond_4
    sget v5, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    new-array v3, v5, [I

    .line 53
    .local v3, "simTypes":[I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    sget v5, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    if-ge v1, v5, :cond_6

    .line 54
    invoke-static {v1}, Lcom/android/phone/NetworkModeManager;->getPhoneType(I)I

    move-result v5

    aput v5, v3, v1

    .line 55
    aget v5, v3, v1

    if-nez v5, :cond_5

    .line 57
    const-string/jumbo v5, "CdmaUnavailableAlertDialog"

    const-string/jumbo v6, "showCdmaUnavailableDialogIfNeeded return for sim application not ready"

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    return-void

    .line 53
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 62
    :cond_6
    const/4 v1, 0x0

    :goto_3
    array-length v5, v3

    if-ge v1, v5, :cond_9

    .line 63
    if-eq v1, v0, :cond_7

    aget v5, v3, v1

    if-ne v5, v8, :cond_7

    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultVoiceSlotId()I

    move-result v5

    if-ne v5, v1, :cond_7

    .line 65
    const-string/jumbo v5, "CdmaUnavailableAlertDialog"

    const-string/jumbo v6, "showCdmaUnavailableDialogIfNeeded reset preferred voice slot"

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v5

    sget v6, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    invoke-virtual {v5, v6}, Lmiui/telephony/SubscriptionManager;->setDefaultVoiceSlotId(I)V

    .line 69
    :cond_7
    aget v5, v3, v0

    if-ne v5, v7, :cond_8

    aget v5, v3, v1

    if-ne v5, v8, :cond_8

    .line 70
    invoke-static {}, Lcom/android/phone/CdmaUnavailableAlertDialog;->showCdmaUnavailableDialog()V

    .line 62
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 73
    :cond_9
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phone/CdmaUnavailableAlertDialog;->showDialog(I)V

    .line 91
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1, "id"    # I

    .prologue
    const/4 v4, 0x0

    .line 100
    if-eqz p1, :cond_0

    .line 101
    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v2

    return-object v2

    .line 103
    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    sget v3, Lmiui/R$style;->Theme_Light_Dialog_Alert:I

    invoke-direct {v2, p0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 104
    const v3, 0x7f0b06f6

    .line 103
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 106
    const v3, 0x7f0b06f5

    .line 103
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 107
    iget-object v3, p0, Lcom/android/phone/CdmaUnavailableAlertDialog;->mPositiveOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    const v4, 0x7f0b06f7

    .line 103
    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 108
    iget-object v3, p0, Lcom/android/phone/CdmaUnavailableAlertDialog;->mNegativeOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    const v4, 0x7f0b06f8

    .line 103
    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 109
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 110
    .local v1, "dialog":Landroid/app/AlertDialog;
    return-object v1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phone/CdmaUnavailableAlertDialog;->showDialog(I)V

    .line 96
    return-void
.end method
