.class public Lcom/android/phone/settings/MobileNetworkSettings;
.super Lmiui/preference/PreferenceActivity;
.source "MobileNetworkSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/MobileNetworkSettings$1;
    }
.end annotation


# static fields
.field private static final BIG_SIM_SLOT_ICON:[I

.field private static final SMALL_SIM_SLOT_ICON:[I

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mButtonDataEnabled:Landroid/preference/CheckBoxPreference;

.field private mButtonDataRoam:Lcom/android/phone/settings/RestrictedPreference;

.field private mButtonDual4G:Landroid/preference/CheckBoxPreference;

.field private mButtonMmsEnabled:Landroid/preference/CheckBoxPreference;

.field private mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

.field private mClickedPreference:Landroid/preference/Preference;

.field private mConnectManager:Landroid/net/ConnectivityManager;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mIsAirplaneEnabled:Z

.field private mLteDataServicePref:Landroid/preference/Preference;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mPrefData:Lcom/android/phone/settings/MultiSimListPreference;

.field private mPrefDefault:Landroid/preference/PreferenceCategory;

.field private mPrefGeneral:Landroid/preference/PreferenceCategory;

.field private mPrefSimInfo:Landroid/preference/PreferenceCategory;

.field private mPrefSimService:Landroid/preference/PreferenceCategory;

.field private mPrefVoice:Lcom/android/phone/settings/MultiSimListPreference;

.field private mPreferenceScreen:Landroid/preference/PreferenceScreen;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSimInfoRecordList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/telephony/SubscriptionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSimNum:I

.field private mVolteSwitchView:Lcom/android/phone/settings/VolteSwitchView;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MobileNetworkSettings;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataEnabled:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MobileNetworkSettings;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MobileNetworkSettings;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonMmsEnabled:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/MobileNetworkSettings;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/MobileNetworkSettings;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mIsAirplaneEnabled:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/MobileNetworkSettings;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/MobileNetworkSettings;
    .param p1, "enabled"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/MobileNetworkSettings;->setPreferenceScreenEnabled(Z)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 104
    const-class v0, Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    .line 107
    const v0, 0x7f020056

    const v1, 0x7f020057

    .line 106
    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/MobileNetworkSettings;->BIG_SIM_SLOT_ICON:[I

    .line 110
    const v0, 0x7f0200a3

    const v1, 0x7f0200a4

    .line 109
    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/MobileNetworkSettings;->SMALL_SIM_SLOT_ICON:[I

    .line 101
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimInfoRecordList:Ljava/util/List;

    .line 158
    iput v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimNum:I

    .line 161
    iput-boolean v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mIsAirplaneEnabled:Z

    .line 163
    new-instance v0, Lcom/android/phone/settings/MobileNetworkSettings$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/MobileNetworkSettings$1;-><init>(Lcom/android/phone/settings/MobileNetworkSettings;)V

    iput-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 101
    return-void
.end method

.method private addSimInfoPreference()V
    .locals 9

    .prologue
    .line 614
    iget-object v6, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimInfo:Landroid/preference/PreferenceCategory;

    invoke-virtual {v6}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 615
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getVirtualSimSlot()I

    move-result v5

    .line 616
    .local v5, "virtuslSimSlot":I
    iget-object v6, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimInfoRecordList:Ljava/util/List;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "siminfo$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/telephony/SubscriptionInfo;

    .line 617
    .local v3, "siminfo":Lmiui/telephony/SubscriptionInfo;
    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v6

    invoke-static {v6}, Lcom/android/phone/MiuiPhoneUtils;->isIccCardActivated(I)Z

    move-result v1

    .line 619
    .local v1, "enable":Z
    new-instance v2, Lcom/android/phone/settings/LTRValuePreference;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/LTRValuePreference;-><init>(Landroid/content/Context;)V

    .line 620
    .local v2, "simInfoPre":Lmiui/preference/ValuePreference;
    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 621
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getVirtualSimCarrierName()Ljava/lang/String;

    move-result-object v0

    .line 622
    .local v0, "displayName":Ljava/lang/String;
    :goto_1
    if-eqz v1, :cond_1

    .end local v0    # "displayName":Ljava/lang/String;
    :goto_2
    invoke-virtual {v2, v0}, Lmiui/preference/ValuePreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 623
    invoke-direct {p0, v2, v3, v1}, Lcom/android/phone/settings/MobileNetworkSettings;->setSimSummary(Lmiui/preference/ValuePreference;Lmiui/telephony/SubscriptionInfo;Z)V

    .line 624
    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lmiui/preference/ValuePreference;->setKey(Ljava/lang/String;)V

    .line 625
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    .line 626
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v6

    if-ne v5, v6, :cond_2

    .line 627
    const v6, 0x7f02009b

    .line 626
    :goto_3
    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v2, v6}, Lmiui/preference/ValuePreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 628
    invoke-virtual {v2, p0}, Lmiui/preference/ValuePreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 629
    iget-object v6, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimInfo:Landroid/preference/PreferenceCategory;

    invoke-virtual {v6, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 621
    :cond_0
    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "displayName":Ljava/lang/String;
    goto :goto_1

    .line 622
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b064d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 627
    .end local v0    # "displayName":Ljava/lang/String;
    :cond_2
    sget-object v6, Lcom/android/phone/settings/MobileNetworkSettings;->BIG_SIM_SLOT_ICON:[I

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v8

    aget v6, v6, v8

    goto :goto_3

    .line 631
    .end local v1    # "enable":Z
    .end local v2    # "simInfoPre":Lmiui/preference/ValuePreference;
    .end local v3    # "siminfo":Lmiui/telephony/SubscriptionInfo;
    :cond_3
    return-void
.end method

.method private addSimServicePreference()V
    .locals 4

    .prologue
    .line 671
    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimService:Landroid/preference/PreferenceCategory;

    if-eqz v1, :cond_0

    .line 672
    return-void

    .line 674
    :cond_0
    const/4 v0, 0x0

    .line 675
    .local v0, "count":I
    const v1, 0x7f060038

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/MobileNetworkSettings;->addPreferencesFromResource(I)V

    .line 676
    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v2, "sim_service"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceCategory;

    iput-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimService:Landroid/preference/PreferenceCategory;

    .line 677
    invoke-static {p0}, Landroid/util/VirtualSim;->isSupportMiSim(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 678
    const/4 v0, 0x1

    .line 679
    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimService:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v3, "pref_mi_sim_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 681
    :cond_1
    invoke-static {p0}, Landroid/util/VirtualSim;->isSupportVirtualSim(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 682
    add-int/lit8 v0, v0, 0x1

    .line 683
    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimService:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v3, "pref_sim_roaming_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 685
    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 686
    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimService:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 688
    :cond_3
    return-void
.end method

.method private init()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 315
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v7

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    .line 317
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "sim_info"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimInfo:Landroid/preference/PreferenceCategory;

    .line 318
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "sim_general"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefGeneral:Landroid/preference/PreferenceCategory;

    .line 319
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "sim_default"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefDefault:Landroid/preference/PreferenceCategory;

    .line 321
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "default_data_key"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Lcom/android/phone/settings/MultiSimListPreference;

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefData:Lcom/android/phone/settings/MultiSimListPreference;

    .line 322
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefData:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v7, p0}, Lcom/android/phone/settings/MultiSimListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 323
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "default_voice_key"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Lcom/android/phone/settings/MultiSimListPreference;

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefVoice:Lcom/android/phone/settings/MultiSimListPreference;

    .line 324
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefVoice:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v7, p0}, Lcom/android/phone/settings/MultiSimListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 326
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "button_data_enabled_key"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataEnabled:Landroid/preference/CheckBoxPreference;

    .line 327
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "button_dual_4g"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    .line 328
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "button_roaming_key"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Lcom/android/phone/settings/RestrictedPreference;

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataRoam:Lcom/android/phone/settings/RestrictedPreference;

    .line 329
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "button_mms_enabled_key"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/CheckBoxPreference;

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonMmsEnabled:Landroid/preference/CheckBoxPreference;

    .line 330
    sget-boolean v7, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-nez v7, :cond_6

    invoke-static {p0}, Lcom/android/services/telephony/ims/ImsAdapter;->isVolteSupportedByDevice(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 331
    new-instance v7, Lcom/android/phone/settings/VolteSwitchView;

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefGeneral:Landroid/preference/PreferenceCategory;

    iget-object v9, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-direct {v7, p0, v8, v9}, Lcom/android/phone/settings/VolteSwitchView;-><init>(Landroid/preference/PreferenceActivity;Landroid/preference/PreferenceGroup;Lcom/android/internal/telephony/Phone;)V

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mVolteSwitchView:Lcom/android/phone/settings/VolteSwitchView;

    .line 336
    :goto_0
    sget-boolean v7, Lcom/android/phone/MiuiPhoneUtils;->DUAL_VOLTE_SUPPORTED:Z

    if-eqz v7, :cond_0

    invoke-static {}, Lcom/android/phone/NetworkModeManager;->isDisableLte()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 337
    :cond_0
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    if-eqz v7, :cond_1

    .line 338
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefGeneral:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 339
    iput-object v10, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    .line 342
    :cond_1
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "cdma_lte_data_service_key"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mLteDataServicePref:Landroid/preference/Preference;

    .line 344
    sget-boolean v7, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v7, :cond_7

    .line 345
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataRoam:Lcom/android/phone/settings/RestrictedPreference;

    const v8, 0x7f0b0664

    invoke-virtual {v7, v8}, Lcom/android/phone/settings/RestrictedPreference;->setTitle(I)V

    .line 350
    :goto_1
    invoke-static {}, Lcom/android/phone/TelephonyCapabilities;->supportShowPLMNPreference()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 351
    :cond_2
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v9, "button_uplmn_key"

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 354
    :cond_3
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getLteOnCdmaMode()I

    move-result v7

    if-ne v7, v11, :cond_8

    const/4 v4, 0x1

    .line 357
    .local v4, "isLteOnCdma":Z
    :goto_2
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 358
    const-string/jumbo v8, "setup_prepaid_data_service_url"

    .line 357
    invoke-static {v7, v8}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 356
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    .line 359
    .local v5, "missingDataServiceUrl":Z
    if-eqz v4, :cond_4

    if-eqz v5, :cond_9

    .line 360
    :cond_4
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mLteDataServicePref:Landroid/preference/Preference;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 366
    :goto_3
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 367
    const v8, 0x7f0e001b

    .line 366
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 368
    .local v3, "isCarrierSettingsEnabled":Z
    if-nez v3, :cond_5

    .line 369
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "carrier_settings_key"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 370
    .local v6, "pref":Landroid/preference/Preference;
    if-eqz v6, :cond_5

    .line 371
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v7, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 374
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v8, "carrier_settings_key"

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 375
    if-eqz v6, :cond_5

    .line 376
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v7, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 381
    .end local v6    # "pref":Landroid/preference/Preference;
    :cond_5
    new-instance v7, Lcom/android/phone/settings/MobileNetworkSettings$3;

    new-instance v8, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getMainLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v7, p0, v8}, Lcom/android/phone/settings/MobileNetworkSettings$3;-><init>(Lcom/android/phone/settings/MobileNetworkSettings;Landroid/os/Handler;)V

    iput-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mContentObserver:Landroid/database/ContentObserver;

    .line 397
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataEnabled:Landroid/preference/CheckBoxPreference;

    if-eqz v7, :cond_a

    .line 398
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "mobile_data"

    invoke-static {v8}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 399
    iget-object v9, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mContentObserver:Landroid/database/ContentObserver;

    .line 398
    invoke-virtual {v7, v8, v11, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 400
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 401
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_4
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v7

    invoke-virtual {v7}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v7

    if-ge v1, v7, :cond_a

    .line 402
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "mobile_data"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 403
    iget-object v9, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mContentObserver:Landroid/database/ContentObserver;

    .line 402
    invoke-virtual {v7, v8, v12, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 401
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 333
    .end local v1    # "i":I
    .end local v3    # "isCarrierSettingsEnabled":Z
    .end local v4    # "isLteOnCdma":Z
    .end local v5    # "missingDataServiceUrl":Z
    :cond_6
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefGeneral:Landroid/preference/PreferenceCategory;

    invoke-static {v7}, Lcom/android/phone/settings/VolteSwitchView;->removeVolteSwitch(Landroid/preference/PreferenceGroup;)V

    goto/16 :goto_0

    .line 347
    :cond_7
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataRoam:Lcom/android/phone/settings/RestrictedPreference;

    const v8, 0x7f0b0665

    invoke-virtual {v7, v8}, Lcom/android/phone/settings/RestrictedPreference;->setTitle(I)V

    goto/16 :goto_1

    .line 354
    :cond_8
    const/4 v4, 0x0

    .restart local v4    # "isLteOnCdma":Z
    goto/16 :goto_2

    .line 362
    .restart local v5    # "missingDataServiceUrl":Z
    :cond_9
    sget-object v7, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "Keep ltePref"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 407
    .restart local v3    # "isCarrierSettingsEnabled":Z
    :cond_a
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonMmsEnabled:Landroid/preference/CheckBoxPreference;

    if-eqz v7, :cond_b

    .line 408
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "always_enable_mms"

    invoke-static {v8}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 409
    iget-object v9, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mContentObserver:Landroid/database/ContentObserver;

    .line 408
    invoke-virtual {v7, v8, v11, v9}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 412
    :cond_b
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v7

    if-eqz v7, :cond_c

    invoke-static {}, Lmiui/telephony/TelephonyManager;->isCustSingleSimDevice()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 413
    :cond_c
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimInfo:Landroid/preference/PreferenceCategory;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 414
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefGeneral:Landroid/preference/PreferenceCategory;

    invoke-virtual {v7, v12}, Landroid/preference/PreferenceCategory;->setOrder(I)V

    .line 415
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefDefault:Landroid/preference/PreferenceCategory;

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 416
    invoke-direct {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->setGsmCdmaOptions()V

    .line 419
    :cond_d
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v7, "miui.intent.action.NETWORKASSISTANT_SETTINGS"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 420
    .local v2, "intent":Landroid/content/Intent;
    const-string/jumbo v7, "com.miui.securitycenter"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v7

    if-nez v7, :cond_e

    .line 422
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefGeneral:Landroid/preference/PreferenceCategory;

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v9, "pref_key_data_usage"

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 425
    :cond_e
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 426
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_f

    .line 427
    invoke-virtual {v0, v11}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 428
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v7

    if-eqz v7, :cond_f

    invoke-static {}, Lmiui/telephony/TelephonyManager;->isCustSingleSimDevice()Z

    move-result v7

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_f

    .line 429
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0645

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 432
    :cond_f
    return-void
.end method

.method private setGsmCdmaOptions()V
    .locals 4

    .prologue
    .line 435
    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v0

    .line 436
    .local v0, "phoneType":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 437
    new-instance v1, Lcom/android/phone/settings/CdmaOptions;

    iget-object v2, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-direct {v1, p0, v2, v3}, Lcom/android/phone/settings/CdmaOptions;-><init>(Landroid/preference/PreferenceActivity;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V

    iput-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    .line 443
    :goto_0
    return-void

    .line 438
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 439
    new-instance v1, Lcom/android/phone/settings/GsmUmtsOptions;

    iget-object v2, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-direct {v1, p0, v2, v3}, Lcom/android/phone/settings/GsmUmtsOptions;-><init>(Landroid/preference/PreferenceActivity;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V

    iput-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    goto :goto_0

    .line 441
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unexpected phone type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private setNoSimInfoUi()V
    .locals 2

    .prologue
    .line 664
    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimInfo:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 665
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 666
    .local v0, "pref":Landroid/preference/Preference;
    const v1, 0x7f0b0649

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 667
    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimInfo:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 668
    return-void
.end method

.method private setPreferenceScreenEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 199
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimInfo:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 200
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefGeneral:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 201
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefDefault:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 202
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimService:Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefSimService:Landroid/preference/PreferenceCategory;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 205
    :cond_0
    return-void
.end method

.method private setPreferenceSummary(Landroid/preference/Preference;I)V
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "slotId"    # I

    .prologue
    .line 529
    sget v4, Lcom/android/phone/settings/MultiSimListPreference;->DO_NOT_SET:I

    if-eq p2, v4, :cond_4

    .line 530
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v4

    invoke-virtual {v4, p2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v1

    .line 531
    .local v1, "info":Lmiui/telephony/SubscriptionInfo;
    if-eqz v1, :cond_3

    .line 532
    invoke-virtual {v1}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v4

    invoke-static {v4}, Lcom/android/phone/MiuiPhoneUtils;->isVirtualSim(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 533
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getVirtualSimCarrierName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 534
    const v4, 0x7f02009c

    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setIcon(I)V

    .line 555
    .end local v1    # "info":Lmiui/telephony/SubscriptionInfo;
    :goto_0
    return-void

    .line 536
    .restart local v1    # "info":Lmiui/telephony/SubscriptionInfo;
    :cond_0
    invoke-virtual {v1}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 537
    .local v2, "name":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 538
    .local v3, "newName":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 539
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x25

    if-ne v4, v5, :cond_1

    .line 540
    const-string/jumbo v4, "%%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 542
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 545
    :cond_2
    invoke-virtual {p1, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 546
    sget-object v4, Lcom/android/phone/settings/MobileNetworkSettings;->SMALL_SIM_SLOT_ICON:[I

    aget v4, v4, p2

    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setIcon(I)V

    goto :goto_0

    .line 549
    .end local v0    # "i":I
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "newName":Ljava/lang/StringBuilder;
    :cond_3
    sget-object v4, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "setPreferenceSummary SimInfoRecord is null."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 552
    .end local v1    # "info":Lmiui/telephony/SubscriptionInfo;
    :cond_4
    const v4, 0x7f0b065a

    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setSummary(I)V

    .line 553
    const v4, 0x7f02009d

    invoke-virtual {p1, v4}, Landroid/preference/Preference;->setIcon(I)V

    goto :goto_0
.end method

.method private setSimSummary(Lmiui/preference/ValuePreference;Lmiui/telephony/SubscriptionInfo;Z)V
    .locals 4
    .param p1, "simInfoPre"    # Lmiui/preference/ValuePreference;
    .param p2, "siminfo"    # Lmiui/telephony/SubscriptionInfo;
    .param p3, "simEnabled"    # Z

    .prologue
    .line 634
    if-nez p3, :cond_0

    .line 635
    const-string/jumbo v1, ""

    invoke-virtual {p1, v1}, Lmiui/preference/ValuePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 636
    return-void

    .line 639
    :cond_0
    invoke-virtual {p2}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const-string/jumbo v0, ""

    .line 640
    .local v0, "summary":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/android/phone/NetworkModeManager;->isShowCdmaUnavailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 641
    invoke-virtual {p2}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v1

    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 642
    invoke-virtual {p2}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v1

    invoke-static {v1}, Lcom/android/phone/NetworkModeManager;->getPhoneType(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v2, v1, :cond_1

    .line 643
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b06f9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 645
    :cond_1
    invoke-static {v0}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmiui/preference/ValuePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 646
    return-void

    .line 639
    .end local v0    # "summary":Ljava/lang/String;
    :cond_2
    invoke-virtual {p2}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "summary":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public getSimInfo()V
    .locals 1

    .prologue
    .line 558
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimInfoRecordList:Ljava/util/List;

    .line 559
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimInfoRecordList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimNum:I

    .line 560
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 692
    packed-switch p1, :pswitch_data_0

    .line 706
    :cond_0
    :goto_0
    return-void

    .line 695
    :pswitch_0
    const-string/jumbo v1, "exit_ecm_result"

    const/4 v2, 0x0

    .line 694
    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 696
    .local v0, "isChoiceYes":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 698
    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    iget-object v2, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mClickedPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CdmaOptions;->showDialog(Landroid/preference/Preference;)V

    goto :goto_0

    .line 692
    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 297
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 298
    const v0, 0x7f060021

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MobileNetworkSettings;->addPreferencesFromResource(I)V

    .line 300
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MobileNetworkSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mConnectManager:Landroid/net/ConnectivityManager;

    .line 301
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 303
    invoke-direct {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->init()V

    .line 305
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mIntentFilter:Landroid/content/IntentFilter;

    .line 306
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmiui/telephony/TelephonyManager;->isCustSingleSimDevice()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isCTDualVolteSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mIntentFilter:Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/android/phone/settings/MobileNetworkSettings;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 311
    const-string/jumbo v0, "event_settings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_enter"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/phone/utils/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 720
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 721
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 722
    iput-object v2, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mContentObserver:Landroid/database/ContentObserver;

    .line 724
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    if-eqz v0, :cond_1

    .line 725
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    invoke-virtual {v0}, Lcom/android/phone/settings/GsmUmtsOptions;->destroy()V

    .line 727
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    if-eqz v0, :cond_2

    .line 728
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    invoke-virtual {v0}, Lcom/android/phone/settings/CdmaOptions;->destroy()V

    .line 730
    :cond_2
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mVolteSwitchView:Lcom/android/phone/settings/VolteSwitchView;

    if-eqz v0, :cond_3

    .line 731
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mVolteSwitchView:Lcom/android/phone/settings/VolteSwitchView;

    invoke-virtual {v0}, Lcom/android/phone/settings/VolteSwitchView;->destroy()V

    .line 733
    :cond_3
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MobileNetworkSettings;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 734
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    .line 735
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 710
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 711
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 712
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->finish()V

    .line 713
    const/4 v1, 0x1

    return v1

    .line 715
    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 486
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onPause()V

    .line 487
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmiui/telephony/TelephonyManager;->isCustSingleSimDevice()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 488
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/telephony/SubscriptionManager;->removeOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 490
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    .line 503
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 504
    .local v1, "slotId":I
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 505
    .local v0, "key":Ljava/lang/String;
    const-string/jumbo v2, "default_data_key"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 506
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getCallManager()Lcom/android/internal/telephony/CallManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    if-eq v2, v3, :cond_0

    .line 507
    const v2, 0x7f0b0654

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 508
    const/4 v2, 0x0

    return v2

    .line 511
    :cond_0
    sget-boolean v2, Lmiui/os/Build;->IS_HONGMI_TWOX_CT:Z

    if-eqz v2, :cond_2

    if-ne v1, v4, :cond_2

    .line 513
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b065d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 512
    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 520
    :goto_0
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmiui/telephony/SubscriptionManager;->setDefaultDataSlotId(I)V

    .line 524
    :cond_1
    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/android/phone/settings/MobileNetworkSettings;->setPreferenceSummary(Landroid/preference/Preference;I)V

    .line 525
    return v4

    .line 517
    :cond_2
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b065c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 516
    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 521
    :cond_3
    const-string/jumbo v2, "default_voice_key"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 522
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmiui/telephony/SubscriptionManager;->setDefaultVoiceSlotId(I)V

    goto :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 650
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 652
    .local v3, "slotId":I
    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 653
    .local v1, "extras":Landroid/os/Bundle;
    invoke-static {v1, v3}, Lmiui/telephony/SubscriptionManager;->putSlotId(Landroid/os/Bundle;I)V

    .line 654
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/android/phone/settings/MultiSimInfoEditorActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 655
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 656
    invoke-virtual {p0, v2}, Lcom/android/phone/settings/MobileNetworkSettings;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 660
    .end local v1    # "extras":Landroid/os/Bundle;
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    const/4 v4, 0x1

    return v4

    .line 657
    :catch_0
    move-exception v0

    .line 658
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "onPreferenceClick() has error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 11
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 209
    if-eqz p2, :cond_0

    .line 210
    const-string/jumbo v8, "event_settings"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/phone/utils/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    if-eqz v8, :cond_1

    .line 213
    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    invoke-virtual {v8, p2}, Lcom/android/phone/settings/GsmUmtsOptions;->preferenceTreeClick(Landroid/preference/Preference;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 214
    return v7

    .line 215
    :cond_1
    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    if-eqz v8, :cond_3

    .line 216
    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    invoke-virtual {v8, p2}, Lcom/android/phone/settings/CdmaOptions;->preferenceTreeClick(Landroid/preference/Preference;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 218
    const-string/jumbo v6, "ril.cdma.inecmmode"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 217
    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 219
    iput-object p2, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mClickedPreference:Landroid/preference/Preference;

    .line 222
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v8, "com.android.internal.intent.action.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS"

    invoke-direct {v6, v8, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 223
    const/16 v8, 0x11

    .line 221
    invoke-virtual {p0, v6, v8}, Lcom/android/phone/settings/MobileNetworkSettings;->startActivityForResult(Landroid/content/Intent;I)V

    .line 225
    :cond_2
    return v7

    .line 226
    :cond_3
    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataEnabled:Landroid/preference/CheckBoxPreference;

    if-ne p2, v8, :cond_4

    .line 227
    invoke-static {p0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v6

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataEnabled:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v8}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v8

    invoke-virtual {v6, v8}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 228
    return v7

    .line 229
    :cond_4
    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mLteDataServicePref:Landroid/preference/Preference;

    if-ne p2, v8, :cond_8

    .line 230
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 231
    const-string/jumbo v9, "setup_prepaid_data_service_url"

    .line 230
    invoke-static {v8, v9}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 232
    .local v4, "tmpl":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 233
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v3

    .line 234
    .local v3, "tm":Lmiui/telephony/TelephonyManager;
    invoke-virtual {v3}, Lmiui/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "imsi":Ljava/lang/String;
    if-nez v1, :cond_5

    .line 236
    const-string/jumbo v1, ""

    .line 238
    :cond_5
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v5, 0x0

    .line 240
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v6, "android.intent.action.VIEW"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v2, v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 241
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/android/phone/settings/MobileNetworkSettings;->startActivity(Landroid/content/Intent;)V

    .line 245
    .end local v1    # "imsi":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "tm":Lmiui/telephony/TelephonyManager;
    :goto_1
    return v7

    .line 239
    .restart local v1    # "imsi":Ljava/lang/String;
    .restart local v3    # "tm":Lmiui/telephony/TelephonyManager;
    :cond_6
    new-array v8, v7, [Ljava/lang/CharSequence;

    aput-object v1, v8, v6

    invoke-static {v4, v8}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .local v5, "url":Ljava/lang/String;
    goto :goto_0

    .line 243
    .end local v1    # "imsi":Ljava/lang/String;
    .end local v3    # "tm":Lmiui/telephony/TelephonyManager;
    .end local v5    # "url":Ljava/lang/String;
    :cond_7
    sget-object v6, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "Missing SETUP_PREPAID_DATA_SERVICE_URL"

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 246
    .end local v4    # "tmpl":Ljava/lang/String;
    :cond_8
    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonMmsEnabled:Landroid/preference/CheckBoxPreference;

    if-ne p2, v8, :cond_a

    .line 247
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 248
    const-string/jumbo v9, "always_enable_mms"

    .line 249
    iget-object v10, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonMmsEnabled:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v10}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v10

    if-eqz v10, :cond_9

    move v6, v7

    .line 247
    :cond_9
    invoke-static {v8, v9, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 250
    return v7

    .line 251
    :cond_a
    const-string/jumbo v8, "pref_mi_sim_key"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 252
    const-string/jumbo v8, "pref_sim_roaming_key"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 251
    if-eqz v8, :cond_e

    .line 253
    :cond_b
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 254
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string/jumbo v6, "pref_mi_sim_key"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 255
    new-instance v6, Landroid/content/ComponentName;

    const-string/jumbo v8, "com.miui.virtualsim"

    .line 256
    const-string/jumbo v9, "com.miui.mimobile.ui.MmRouterActivity"

    .line 255
    invoke-direct {v6, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 262
    :cond_c
    :goto_2
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/android/phone/settings/MobileNetworkSettings;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    :goto_3
    return v7

    .line 257
    :cond_d
    const-string/jumbo v6, "pref_sim_roaming_key"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 258
    new-instance v6, Landroid/content/ComponentName;

    const-string/jumbo v8, "com.miui.virtualsim"

    .line 259
    const-string/jumbo v9, "com.miui.virtualsim.ui.MainActivity"

    .line 258
    invoke-direct {v6, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_2

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "enter mi_sim or roaming service error."

    invoke-static {v6, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 267
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_e
    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    if-ne p2, v8, :cond_10

    .line 268
    sget-object v8, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getCallManager()Lcom/android/internal/telephony/CallManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v9

    if-eq v8, v9, :cond_f

    .line 269
    const v7, 0x7f0b0654

    invoke-static {p0, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 271
    iget-object v7, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v8}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v8

    xor-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v8}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 272
    return v6

    .line 274
    :cond_f
    invoke-static {}, Lcom/android/phone/Dual4GManager;->getInstance()Lcom/android/phone/Dual4GManager;

    move-result-object v6

    iget-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v8}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v8

    new-instance v9, Lcom/android/phone/settings/MobileNetworkSettings$2;

    invoke-direct {v9, p0}, Lcom/android/phone/settings/MobileNetworkSettings$2;-><init>(Lcom/android/phone/settings/MobileNetworkSettings;)V

    invoke-virtual {v6, v8, v9}, Lcom/android/phone/Dual4GManager;->setDual4GMode(ZLcom/android/phone/Dual4GManager$CallBack;)V

    .line 290
    return v7

    .line 292
    :cond_10
    return v6
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 447
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 449
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 450
    const-string/jumbo v3, "airplane_mode_on"

    const/4 v4, -0x1

    .line 448
    invoke-static {v0, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mIsAirplaneEnabled:Z

    .line 451
    iget-boolean v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mIsAirplaneEnabled:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/phone/settings/MobileNetworkSettings;->setPreferenceScreenEnabled(Z)V

    .line 453
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataEnabled:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mConnectManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v3}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 457
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonMmsEnabled:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 458
    const-string/jumbo v4, "always_enable_mms"

    .line 457
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_4

    :goto_1
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 459
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mVolteSwitchView:Lcom/android/phone/settings/VolteSwitchView;

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mVolteSwitchView:Lcom/android/phone/settings/VolteSwitchView;

    invoke-virtual {v0}, Lcom/android/phone/settings/VolteSwitchView;->updateVolteButtonUI()V

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 463
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/phone/Dual4GManager;->isDual4GEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 464
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isCTDualVolteSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isDualCTSim()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 465
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 466
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f0b0736

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 469
    :cond_1
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lmiui/telephony/TelephonyManager;->isCustSingleSimDevice()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    .line 470
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 471
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getSimInfo()V

    .line 472
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->updatePreferenceUI()V

    .line 473
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefVoice:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v0}, Lcom/android/phone/settings/MultiSimListPreference;->notifySimInfoDataChanged()V

    .line 474
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefData:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v0}, Lcom/android/phone/settings/MultiSimListPreference;->notifySimInfoDataChanged()V

    .line 481
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->addSimServicePreference()V

    .line 482
    return-void

    :cond_3
    move v0, v2

    .line 448
    goto :goto_0

    :cond_4
    move v1, v2

    .line 457
    goto :goto_1

    .line 476
    :cond_5
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/telephony/TelephonyManager;->hasIccCard()Z

    move-result v0

    if-nez v0, :cond_2

    .line 477
    invoke-direct {p0, v2}, Lcom/android/phone/settings/MobileNetworkSettings;->setPreferenceScreenEnabled(Z)V

    goto :goto_2
.end method

.method public onSubscriptionsChanged()V
    .locals 1

    .prologue
    .line 494
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getSimInfo()V

    .line 495
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->updatePreferenceUI()V

    .line 497
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefVoice:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v0}, Lcom/android/phone/settings/MultiSimListPreference;->notifySimInfoDataChanged()V

    .line 498
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefData:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v0}, Lcom/android/phone/settings/MultiSimListPreference;->notifySimInfoDataChanged()V

    .line 499
    return-void
.end method

.method public updatePreferenceUI()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 563
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->isResumed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 564
    sget-object v4, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "updatePreferenceUI(): on backgroud no need update preference"

    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    :cond_0
    iget-boolean v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mIsAirplaneEnabled:Z

    if-nez v4, :cond_5

    iget v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimNum:I

    if-lez v4, :cond_5

    move v4, v5

    :goto_0
    invoke-direct {p0, v4}, Lcom/android/phone/settings/MobileNetworkSettings;->setPreferenceScreenEnabled(Z)V

    .line 568
    iget v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimNum:I

    if-lez v4, :cond_6

    .line 569
    invoke-direct {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->addSimInfoPreference()V

    .line 575
    :goto_1
    iget v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimNum:I

    if-le v4, v5, :cond_7

    const/4 v2, 0x1

    .line 576
    .local v2, "isVoiceEnabled":Z
    :goto_2
    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/phone/MiuiPhoneUtils;->isDcOnlyVirtualSim(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 577
    iget v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimNum:I

    const/4 v7, 0x2

    if-le v4, v7, :cond_8

    const/4 v2, 0x1

    .line 579
    :cond_1
    :goto_3
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefVoice:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v4, v2}, Lcom/android/phone/settings/MultiSimListPreference;->setEnabled(Z)V

    .line 581
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v3

    .line 582
    .local v3, "slotId":I
    iget v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mSimNum:I

    if-le v4, v5, :cond_9

    const/4 v1, 0x1

    .line 584
    .local v1, "isDataEnabled":Z
    :goto_4
    sget v4, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    invoke-static {}, Lcom/android/phone/DefaultSlotSelectorImpl;->getDataSlotForCmcc()I

    move-result v5

    if-eq v4, v5, :cond_3

    .line 586
    invoke-static {}, Lcom/android/phone/DefaultSlotSelectorImpl;->isDdsForceSetForCmcc()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 587
    const/4 v1, 0x0

    .line 590
    :cond_2
    invoke-static {}, Lcom/android/phone/NetworkModeManager;->isDeviceLockViceNetworkModeForCmcc()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    if-eqz v4, :cond_3

    .line 591
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefGeneral:Landroid/preference/PreferenceCategory;

    iget-object v5, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 592
    iput-object v8, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDual4G:Landroid/preference/CheckBoxPreference;

    .line 595
    :cond_3
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefData:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v4, v1}, Lcom/android/phone/settings/MultiSimListPreference;->setEnabled(Z)V

    .line 596
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefData:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/phone/settings/MultiSimListPreference;->setValue(Ljava/lang/String;)V

    .line 597
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefData:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-direct {p0, v4, v3}, Lcom/android/phone/settings/MobileNetworkSettings;->setPreferenceSummary(Landroid/preference/Preference;I)V

    .line 599
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultVoiceSlotId()I

    move-result v3

    .line 600
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefVoice:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/phone/settings/MultiSimListPreference;->setValue(Ljava/lang/String;)V

    .line 601
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mPrefVoice:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-direct {p0, v4, v3}, Lcom/android/phone/settings/MobileNetworkSettings;->setPreferenceSummary(Landroid/preference/Preference;I)V

    .line 603
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataRoam:Lcom/android/phone/settings/RestrictedPreference;

    invoke-virtual {v4}, Lcom/android/phone/settings/RestrictedPreference;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 604
    invoke-virtual {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 605
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/android/services/telephony/SimpleFeatures;->hasBaseUserRestriction(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 606
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataRoam:Lcom/android/phone/settings/RestrictedPreference;

    invoke-virtual {v4, v6}, Lcom/android/phone/settings/RestrictedPreference;->setEnabled(Z)V

    .line 611
    .end local v0    # "context":Landroid/content/Context;
    :cond_4
    :goto_5
    return-void

    .end local v1    # "isDataEnabled":Z
    .end local v2    # "isVoiceEnabled":Z
    .end local v3    # "slotId":I
    :cond_5
    move v4, v6

    .line 567
    goto/16 :goto_0

    .line 571
    :cond_6
    invoke-direct {p0}, Lcom/android/phone/settings/MobileNetworkSettings;->setNoSimInfoUi()V

    goto/16 :goto_1

    .line 575
    :cond_7
    const/4 v2, 0x0

    .restart local v2    # "isVoiceEnabled":Z
    goto/16 :goto_2

    .line 577
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 582
    .restart local v3    # "slotId":I
    :cond_9
    const/4 v1, 0x0

    .restart local v1    # "isDataEnabled":Z
    goto :goto_4

    .line 608
    .restart local v0    # "context":Landroid/content/Context;
    :cond_a
    iget-object v4, p0, Lcom/android/phone/settings/MobileNetworkSettings;->mButtonDataRoam:Lcom/android/phone/settings/RestrictedPreference;

    invoke-static {v0}, Lcom/android/services/telephony/SimpleFeatures;->checkDataRoamingRestriction(Landroid/content/Context;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/phone/settings/RestrictedPreference;->setDisabledByAdmin(Z)V

    goto :goto_5
.end method
