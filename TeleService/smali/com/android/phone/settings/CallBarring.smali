.class public Lcom/android/phone/settings/CallBarring;
.super Lcom/android/phone/settings/TimeConsumingPreferenceActivity;
.source "CallBarring.java"

# interfaces
.implements Lcom/android/phone/settings/CallBarringInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/CallBarring$1;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private DBG:Z

.field private mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

.field private mCallCancel:Lcom/android/phone/settings/CallBarringResetPreference;

.field private mCallChangePassword:Landroid/preference/PreferenceScreen;

.field private mCallInButton:Lcom/android/phone/settings/CallBarringBasePreference;

.field private mCallInButton2:Lcom/android/phone/settings/CallBarringBasePreference;

.field private mCallInternationalOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

.field private mCallInternationalOutButton2:Lcom/android/phone/settings/CallBarringBasePreference;

.field private mErrorState:I

.field private mFirstResume:Z

.field private mHasShowSuppServiceDialog:Z

.field private mInitIndex:I

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mPreferences:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSlotId:I


# direct methods
.method static synthetic -get0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/phone/settings/CallBarring;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/CallBarring;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallBarring;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/CallBarring;->mSlotId:I

    return v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/android/phone/settings/CallBarring;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/CallBarring;->LOG_TAG:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;-><init>()V

    .line 45
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/CallBarring;->DBG:Z

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    .line 64
    iput v1, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    .line 65
    iput v1, p0, Lcom/android/phone/settings/CallBarring;->mErrorState:I

    .line 66
    iput-boolean v1, p0, Lcom/android/phone/settings/CallBarring;->mHasShowSuppServiceDialog:Z

    .line 69
    iput v1, p0, Lcom/android/phone/settings/CallBarring;->mSlotId:I

    .line 76
    new-instance v0, Lcom/android/phone/settings/CallBarring$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/CallBarring$1;-><init>(Lcom/android/phone/settings/CallBarring;)V

    iput-object v0, p0, Lcom/android/phone/settings/CallBarring;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 41
    return-void

    :cond_0
    move v0, v1

    .line 45
    goto :goto_0
.end method

.method private doGetCallState(Landroid/preference/Preference;)V
    .locals 1
    .param p1, "p"    # Landroid/preference/Preference;

    .prologue
    .line 305
    instance-of v0, p1, Lcom/android/phone/settings/CallBarringBasePreference;

    if-eqz v0, :cond_0

    .line 306
    check-cast p1, Lcom/android/phone/settings/CallBarringBasePreference;

    .end local p1    # "p":Landroid/preference/Preference;
    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->init(Lcom/android/phone/settings/TimeConsumingPreferenceListener;Z)V

    .line 308
    :cond_0
    return-void
.end method

.method private initial()V
    .locals 3

    .prologue
    .line 278
    iget v1, p0, Lcom/android/phone/settings/CallBarring;->mSlotId:I

    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 280
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    const-string/jumbo v2, "AO"

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    .line 281
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    const v2, 0x7f0b0603

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmTitle(I)V

    .line 282
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setPhoneInstance(Lcom/android/internal/telephony/Phone;)V

    .line 284
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    const-string/jumbo v2, "OI"

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    .line 285
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    const v2, 0x7f0b0604

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmTitle(I)V

    .line 286
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setPhoneInstance(Lcom/android/internal/telephony/Phone;)V

    .line 288
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    const-string/jumbo v2, "OX"

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    .line 289
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    const v2, 0x7f0b0605

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmTitle(I)V

    .line 290
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setPhoneInstance(Lcom/android/internal/telephony/Phone;)V

    .line 292
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton:Lcom/android/phone/settings/CallBarringBasePreference;

    const-string/jumbo v2, "AI"

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    .line 293
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton:Lcom/android/phone/settings/CallBarringBasePreference;

    const v2, 0x7f0b0606

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmTitle(I)V

    .line 294
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setPhoneInstance(Lcom/android/internal/telephony/Phone;)V

    .line 296
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    const-string/jumbo v2, "IR"

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    .line 297
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    const v2, 0x7f0b0607

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CallBarringBasePreference;->setmTitle(I)V

    .line 298
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setPhoneInstance(Lcom/android/internal/telephony/Phone;)V

    .line 300
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallCancel:Lcom/android/phone/settings/CallBarringResetPreference;

    invoke-virtual {v1, p0}, Lcom/android/phone/settings/CallBarringResetPreference;->setListener(Lcom/android/phone/settings/TimeConsumingPreferenceListener;)V

    .line 301
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallCancel:Lcom/android/phone/settings/CallBarringResetPreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringResetPreference;->setPhoneInstance(Lcom/android/internal/telephony/Phone;)V

    .line 302
    return-void
.end method

.method private isOperatorSupportChangeCancelCB(Lcom/android/internal/telephony/Phone;)Z
    .locals 3
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->isJioSim(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    const/4 v1, 0x0

    return v1

    .line 174
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/CallBarring;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-static {v1, v2}, Landroid/telephony/SubscriptionManager;->getResourcesForSubId(Landroid/content/Context;I)Landroid/content/res/Resources;

    move-result-object v0

    .line 175
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0e0037

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    return v1
.end method

.method private setPreferenceNotSupported(Landroid/preference/Preference;)V
    .locals 3
    .param p1, "p"    # Landroid/preference/Preference;

    .prologue
    .line 179
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 180
    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0706

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 181
    .local v0, "summary":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 182
    return-void
.end method

.method private startUpdate()V
    .locals 3

    .prologue
    .line 263
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    .line 264
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget v2, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    .line 265
    .local v0, "p":Landroid/preference/Preference;
    if-eqz v0, :cond_0

    .line 266
    invoke-direct {p0, v0}, Lcom/android/phone/settings/CallBarring;->doGetCallState(Landroid/preference/Preference;)V

    .line 268
    :cond_0
    return-void
.end method


# virtual methods
.method public doCancelAllState()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 341
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/CallBarringBasePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b060b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 342
    .local v0, "summary":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 343
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Lcom/android/phone/settings/CallBarringBasePreference;->setChecked(Z)V

    .line 344
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 345
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Lcom/android/phone/settings/CallBarringBasePreference;->setChecked(Z)V

    .line 346
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 347
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Lcom/android/phone/settings/CallBarringBasePreference;->setChecked(Z)V

    .line 348
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 349
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Lcom/android/phone/settings/CallBarringBasePreference;->setChecked(Z)V

    .line 350
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/CallBarringBasePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Lcom/android/phone/settings/CallBarringBasePreference;->setChecked(Z)V

    .line 352
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    .line 102
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 104
    invoke-static {p0}, Lcom/android/phone/settings/SimPickerPreference;->showSimPicker(Landroid/app/Activity;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 105
    return-void

    .line 108
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/CallBarring;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v7

    invoke-virtual {v7}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v7

    invoke-static {v6, v7}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v6

    iput v6, p0, Lcom/android/phone/settings/CallBarring;->mSlotId:I

    .line 110
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    .line 112
    .local v1, "app":Lcom/android/phone/PhoneGlobals;
    new-instance v6, Landroid/content/IntentFilter;

    const-string/jumbo v7, "android.intent.action.SIM_STATE_CHANGED"

    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/android/phone/settings/CallBarring;->mIntentFilter:Landroid/content/IntentFilter;

    .line 113
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v7, p0, Lcom/android/phone/settings/CallBarring;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v6, v7}, Lcom/android/phone/settings/CallBarring;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 115
    const v6, 0x7f060005

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/CallBarring;->addPreferencesFromResource(I)V

    .line 117
    invoke-virtual {p0}, Lcom/android/phone/settings/CallBarring;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    .line 119
    .local v5, "prefSet":Landroid/preference/PreferenceScreen;
    const-string/jumbo v6, "all_outing_key"

    .line 118
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/android/phone/settings/CallBarringBasePreference;

    iput-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    .line 121
    const-string/jumbo v6, "all_outing_international_key"

    .line 120
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/android/phone/settings/CallBarringBasePreference;

    iput-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    .line 123
    const-string/jumbo v6, "all_outing_except_key"

    .line 122
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/android/phone/settings/CallBarringBasePreference;

    iput-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    .line 125
    const-string/jumbo v6, "all_incoming_key"

    .line 124
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/android/phone/settings/CallBarringBasePreference;

    iput-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton:Lcom/android/phone/settings/CallBarringBasePreference;

    .line 127
    const-string/jumbo v6, "all_incoming_except_key"

    .line 126
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/android/phone/settings/CallBarringBasePreference;

    iput-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    .line 130
    const-string/jumbo v6, "deactivate_all_key"

    .line 129
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Lcom/android/phone/settings/CallBarringResetPreference;

    iput-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallCancel:Lcom/android/phone/settings/CallBarringResetPreference;

    .line 131
    const-string/jumbo v6, "change_password_key"

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    check-cast v6, Landroid/preference/PreferenceScreen;

    iput-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallChangePassword:Landroid/preference/PreferenceScreen;

    .line 132
    iget v6, p0, Lcom/android/phone/settings/CallBarring;->mSlotId:I

    invoke-static {v6}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    .line 133
    .local v4, "phone":Lcom/android/internal/telephony/Phone;
    invoke-direct {p0, v4}, Lcom/android/phone/settings/CallBarring;->isOperatorSupportChangeCancelCB(Lcom/android/internal/telephony/Phone;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-static {v4}, Lcom/android/phone/PhoneAdapter;->isUtEnabled(Lcom/android/internal/telephony/Phone;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 134
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallCancel:Lcom/android/phone/settings/CallBarringResetPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 135
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallChangePassword:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 138
    :cond_1
    invoke-direct {p0}, Lcom/android/phone/settings/CallBarring;->initial()V

    .line 139
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/phone/settings/CallBarring;->mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallAllOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, p0}, Lcom/android/phone/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/android/phone/settings/CallBarringInterface;)V

    .line 146
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, p0}, Lcom/android/phone/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/android/phone/settings/CallBarringInterface;)V

    .line 147
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallInternationalOutButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, p0}, Lcom/android/phone/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/android/phone/settings/CallBarringInterface;)V

    .line 148
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, p0}, Lcom/android/phone/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/android/phone/settings/CallBarringInterface;)V

    .line 149
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallInButton2:Lcom/android/phone/settings/CallBarringBasePreference;

    invoke-virtual {v6, p0}, Lcom/android/phone/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/android/phone/settings/CallBarringInterface;)V

    .line 150
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallCancel:Lcom/android/phone/settings/CallBarringResetPreference;

    invoke-virtual {v6, p0}, Lcom/android/phone/settings/CallBarringResetPreference;->setCallBarringInterface(Lcom/android/phone/settings/CallBarringInterface;)V

    .line 153
    iget v6, p0, Lcom/android/phone/settings/CallBarring;->mSlotId:I

    invoke-static {v6}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    .line 154
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "p$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/Preference;

    .line 155
    .local v2, "p":Landroid/preference/Preference;
    invoke-direct {p0, v2}, Lcom/android/phone/settings/CallBarring;->setPreferenceNotSupported(Landroid/preference/Preference;)V

    goto :goto_0

    .line 157
    .end local v2    # "p":Landroid/preference/Preference;
    :cond_2
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallCancel:Lcom/android/phone/settings/CallBarringResetPreference;

    invoke-direct {p0, v6}, Lcom/android/phone/settings/CallBarring;->setPreferenceNotSupported(Landroid/preference/Preference;)V

    .line 158
    iget-object v6, p0, Lcom/android/phone/settings/CallBarring;->mCallChangePassword:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v6}, Lcom/android/phone/settings/CallBarring;->setPreferenceNotSupported(Landroid/preference/Preference;)V

    .line 159
    return-void

    .line 162
    .end local v3    # "p$iterator":Ljava/util/Iterator;
    :cond_3
    iput-boolean v8, p0, Lcom/android/phone/settings/CallBarring;->mFirstResume:Z

    .line 164
    invoke-virtual {p0}, Lcom/android/phone/settings/CallBarring;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 165
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_4

    .line 166
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 168
    :cond_4
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .param p1, "id"    # I

    .prologue
    const v5, 0x7f0b0368

    .line 186
    const/16 v4, 0x2bc

    if-eq p1, v4, :cond_0

    const/16 v4, 0x320

    if-ne p1, v4, :cond_1

    .line 187
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 190
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0b035c

    .line 192
    .local v3, "titleId":I
    sparse-switch p1, :sswitch_data_0

    .line 202
    const v2, 0x7f0b0361

    .line 203
    .local v2, "msgId":I
    iget-object v4, p0, Lcom/android/phone/settings/CallBarring;->mDismissAndFinish:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v5, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 207
    :goto_0
    invoke-virtual {p0, v3}, Lcom/android/phone/settings/CallBarring;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 208
    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallBarring;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 209
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 210
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 213
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 215
    return-object v1

    .line 194
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    .end local v2    # "msgId":I
    :sswitch_0
    const v2, 0x1040409

    .line 195
    .restart local v2    # "msgId":I
    iget-object v4, p0, Lcom/android/phone/settings/CallBarring;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v5, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 198
    .end local v2    # "msgId":I
    :sswitch_1
    const v2, 0x7f0b062e

    .line 199
    .restart local v2    # "msgId":I
    iget-object v4, p0, Lcom/android/phone/settings/CallBarring;->mDismiss:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v5, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 218
    .end local v0    # "b":Landroid/app/AlertDialog$Builder;
    .end local v2    # "msgId":I
    .end local v3    # "titleId":I
    :cond_1
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v4

    return-object v4

    .line 192
    :sswitch_data_0
    .sparse-switch
        0x2bc -> :sswitch_0
        0x320 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 403
    invoke-super {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onDestroy()V

    .line 405
    iget-object v0, p0, Lcom/android/phone/settings/CallBarring;->mIntentFilter:Landroid/content/IntentFilter;

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/android/phone/settings/CallBarring;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/CallBarring;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 408
    :cond_0
    return-void
.end method

.method public onError(Landroid/preference/Preference;I)V
    .locals 1
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "error"    # I

    .prologue
    .line 335
    invoke-super {p0, p1, p2}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    .line 336
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 337
    return-void
.end method

.method public onFinished(Landroid/preference/Preference;Z)V
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "reading"    # Z

    .prologue
    .line 312
    iget v3, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    iget-object v4, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/android/phone/settings/CallBarring;->isFinishing()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    .line 313
    iget-object v3, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    .line 314
    iget v4, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    .line 313
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/settings/CallBarringBasePreference;

    .line 315
    .local v0, "cb":Lcom/android/phone/settings/CallBarringBasePreference;
    invoke-virtual {v0}, Lcom/android/phone/settings/CallBarringBasePreference;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 316
    iget v3, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    .line 317
    iget-boolean v3, p0, Lcom/android/phone/settings/CallBarring;->DBG:Z

    if-eqz v3, :cond_0

    .line 318
    sget-object v3, Lcom/android/phone/settings/CallBarring;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onFinished() is called (init part) mInitIndex is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 319
    const-string/jumbo v5, "is reading?  "

    .line 318
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    :cond_0
    iget-object v3, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget v4, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/Preference;

    .line 322
    .local v2, "p":Landroid/preference/Preference;
    invoke-direct {p0, v2}, Lcom/android/phone/settings/CallBarring;->doGetCallState(Landroid/preference/Preference;)V

    .line 330
    .end local v0    # "cb":Lcom/android/phone/settings/CallBarringBasePreference;
    .end local v2    # "p":Landroid/preference/Preference;
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onFinished(Landroid/preference/Preference;Z)V

    .line 331
    return-void

    .line 324
    .restart local v0    # "cb":Lcom/android/phone/settings/CallBarringBasePreference;
    :cond_2
    iget v1, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 325
    iget-object v3, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/Preference;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 324
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 327
    :cond_3
    iget-object v3, p0, Lcom/android/phone/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 240
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 241
    .local v0, "itemId":I
    packed-switch v0, :pswitch_data_0

    .line 248
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 243
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/phone/settings/CallBarring;->finish()V

    .line 244
    const/4 v1, 0x1

    return v1

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 253
    iget-object v1, p0, Lcom/android/phone/settings/CallBarring;->mCallChangePassword:Landroid/preference/PreferenceScreen;

    if-ne p2, v1, :cond_0

    .line 254
    new-instance v0, Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    .line 255
    iget v1, p0, Lcom/android/phone/settings/CallBarring;->mSlotId:I

    .line 254
    invoke-direct {v0, p0, v1}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;-><init>(Landroid/content/Context;I)V

    .line 256
    .local v0, "dialog":Lcom/android/phone/settings/CallBarringChangePasswordDialog;
    invoke-virtual {v0}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->show()V

    .line 257
    const/4 v1, 0x1

    return v1

    .line 259
    .end local v0    # "dialog":Lcom/android/phone/settings/CallBarringChangePasswordDialog;
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 223
    invoke-super {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onResume()V

    .line 224
    iget-boolean v0, p0, Lcom/android/phone/settings/CallBarring;->mFirstResume:Z

    if-eqz v0, :cond_2

    .line 225
    iget-boolean v0, p0, Lcom/android/phone/settings/CallBarring;->mHasShowSuppServiceDialog:Z

    if-eqz v0, :cond_0

    .line 226
    invoke-virtual {p0}, Lcom/android/phone/settings/CallBarring;->finish()V

    .line 227
    return-void

    .line 228
    :cond_0
    iget v0, p0, Lcom/android/phone/settings/CallBarring;->mSlotId:I

    invoke-static {v0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/phone/MiuiPhoneUtils;->maybeShowDialogForSuppService(Lcom/android/internal/telephony/Phone;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/CallBarring;->mHasShowSuppServiceDialog:Z

    .line 231
    return-void

    .line 233
    :cond_1
    invoke-direct {p0}, Lcom/android/phone/settings/CallBarring;->startUpdate()V

    .line 234
    iput-boolean v1, p0, Lcom/android/phone/settings/CallBarring;->mFirstResume:Z

    .line 236
    :cond_2
    return-void
.end method

.method public resetIndex(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 398
    iput p1, p0, Lcom/android/phone/settings/CallBarring;->mInitIndex:I

    .line 399
    return-void
.end method

.method public setErrorState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 394
    iput p1, p0, Lcom/android/phone/settings/CallBarring;->mErrorState:I

    .line 395
    return-void
.end method
