.class public Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;
.super Lmiui/preference/PreferenceActivity;
.source "MiuiPhoneAccountSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;
    }
.end annotation


# instance fields
.field private mEnabledNonSimAccounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccount;",
            ">;"
        }
    .end annotation
.end field

.field private mPrefAccountsInfo:Landroid/preference/PreferenceCategory;

.field private mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

.field private mTelecomManager:Landroid/telecom/TelecomManager;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;)Landroid/telecom/TelecomManager;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mTelecomManager:Landroid/telecom/TelecomManager;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->updateView()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mEnabledNonSimAccounts:Ljava/util/List;

    .line 25
    return-void
.end method

.method private updatePreferredPhoneAcount()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 77
    iget-object v7, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mEnabledNonSimAccounts:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    .line 78
    .local v0, "accountSize":I
    iget-object v7, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v7}, Landroid/telecom/TelecomManager;->getUserSelectedOutgoingPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 79
    .local v1, "currentSelection":Landroid/telecom/PhoneAccountHandle;
    add-int/lit8 v7, v0, 0x1

    new-array v3, v7, [Ljava/lang/String;

    .line 80
    .local v3, "entryValues":[Ljava/lang/String;
    add-int/lit8 v7, v0, 0x1

    new-array v2, v7, [Ljava/lang/CharSequence;

    .line 82
    .local v2, "entries":[Ljava/lang/CharSequence;
    move v6, v0

    .line 83
    .local v6, "selectedIndex":I
    const/4 v4, 0x0

    .line 84
    .local v4, "i":I
    :goto_0
    if-ge v4, v0, :cond_1

    .line 85
    iget-object v7, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mEnabledNonSimAccounts:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telecom/PhoneAccount;

    invoke-virtual {v7}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v5

    .line 86
    .local v5, "label":Ljava/lang/CharSequence;
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v4

    .line 87
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    .line 89
    iget-object v7, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mEnabledNonSimAccounts:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telecom/PhoneAccount;

    invoke-virtual {v7}, Landroid/telecom/PhoneAccount;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v7

    .line 88
    invoke-static {v1, v7}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 90
    move v6, v4

    .line 84
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 93
    .end local v5    # "label":Ljava/lang/CharSequence;
    :cond_1
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v4

    .line 94
    const v7, 0x7f0b0726

    invoke-virtual {p0, v7}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v4

    .line 96
    iget-object v7, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {v7, v3}, Lcom/android/phone/settings/ValueListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 97
    iget-object v7, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {v7, v2}, Lcom/android/phone/settings/ValueListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 98
    iget-object v7, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {v7, v6}, Lcom/android/phone/settings/ValueListPreference;->setValueIndex(I)V

    .line 99
    iget-object v7, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    aget-object v9, v2, v6

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 100
    iget-object v9, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    if-nez v4, :cond_2

    move v7, v8

    :goto_1
    invoke-virtual {v9, v7}, Lcom/android/phone/settings/ValueListPreference;->setEnabled(Z)V

    .line 101
    return-void

    .line 100
    :cond_2
    const/4 v7, 0x1

    goto :goto_1
.end method

.method private updateView()V
    .locals 6

    .prologue
    .line 55
    iget-object v4, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPrefAccountsInfo:Landroid/preference/PreferenceCategory;

    invoke-virtual {v4}, Landroid/preference/PreferenceCategory;->removeAll()V

    .line 56
    iget-object v4, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mEnabledNonSimAccounts:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 59
    iget-object v4, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mTelecomManager:Landroid/telecom/TelecomManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts(Z)Ljava/util/List;

    move-result-object v1

    .line 61
    .local v1, "accountHandles":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "handle$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/PhoneAccountHandle;

    .line 62
    .local v2, "handle":Landroid/telecom/PhoneAccountHandle;
    iget-object v4, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v4, v2}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 63
    .local v0, "account":Landroid/telecom/PhoneAccount;
    if-eqz v0, :cond_0

    .line 64
    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 65
    iget-object v4, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPrefAccountsInfo:Landroid/preference/PreferenceCategory;

    new-instance v5, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;

    invoke-direct {v5, p0, p0, v0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity$AccountSwitchPreference;-><init>(Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;Landroid/content/Context;Landroid/telecom/PhoneAccount;)V

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 66
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 67
    iget-object v4, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mEnabledNonSimAccounts:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    .end local v0    # "account":Landroid/telecom/PhoneAccount;
    .end local v2    # "handle":Landroid/telecom/PhoneAccountHandle;
    :cond_1
    invoke-direct {p0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->updatePreferredPhoneAcount()V

    .line 74
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f060022

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->addPreferencesFromResource(I)V

    .line 40
    const-string/jumbo v0, "accounts_category"

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPrefAccountsInfo:Landroid/preference/PreferenceCategory;

    .line 41
    const-string/jumbo v0, "button_preferred_phone_account"

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/phone/settings/ValueListPreference;

    iput-object v0, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    .line 42
    iget-object v0, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {v0, p0}, Lcom/android/phone/settings/ValueListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 44
    invoke-static {p0}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mTelecomManager:Landroid/telecom/TelecomManager;

    .line 45
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 105
    iget-object v2, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    if-ne p1, v2, :cond_0

    .line 106
    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 107
    .local v1, "index":I
    iget-object v2, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mEnabledNonSimAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 108
    iget-object v2, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mEnabledNonSimAccounts:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/PhoneAccount;

    invoke-virtual {v2}, Landroid/telecom/PhoneAccount;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 109
    :goto_0
    iget-object v2, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v2, v0}, Landroid/telecom/TelecomManager;->setUserSelectedOutgoingPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 110
    iget-object v2, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    .line 111
    iget-object v3, p0, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->mPreferredPhoneAccount:Lcom/android/phone/settings/ValueListPreference;

    invoke-virtual {v3}, Lcom/android/phone/settings/ValueListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 110
    invoke-virtual {v2, v3}, Lcom/android/phone/settings/ValueListPreference;->setRightValue(Ljava/lang/String;)V

    .line 113
    .end local v1    # "index":I
    :cond_0
    const/4 v2, 0x1

    return v2

    .line 108
    .restart local v1    # "index":I
    :cond_1
    const/4 v0, 0x0

    .local v0, "accountHandle":Landroid/telecom/PhoneAccountHandle;
    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 51
    invoke-direct {p0}, Lcom/android/phone/settings/MiuiPhoneAccountSettingsActivity;->updateView()V

    .line 52
    return-void
.end method
