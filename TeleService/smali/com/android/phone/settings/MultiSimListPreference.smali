.class public Lcom/android/phone/settings/MultiSimListPreference;
.super Landroid/preference/ListPreference;
.source "MultiSimListPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;
    }
.end annotation


# static fields
.field public static final DO_NOT_SET:I

.field private static final SMALL_SIM_SLOT_ICON:[I


# instance fields
.field private mClickedDialogEntryIndex:I

.field private mHasDoNotSet:Z

.field private mMultiSimAdapter:Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;

.field private mSimInfoRecordList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiui/telephony/SubscriptionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic -get0()[I
    .locals 1

    sget-object v0, Lcom/android/phone/settings/MultiSimListPreference;->SMALL_SIM_SLOT_ICON:[I

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/MultiSimListPreference;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimListPreference;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/MultiSimListPreference;->mHasDoNotSet:Z

    return v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/MultiSimListPreference;)Ljava/util/List;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimListPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimListPreference;->mSimInfoRecordList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/MultiSimListPreference;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimListPreference;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/settings/MultiSimListPreference;->mClickedDialogEntryIndex:I

    return p1
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    sget v0, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    sput v0, Lcom/android/phone/settings/MultiSimListPreference;->DO_NOT_SET:I

    .line 37
    const v0, 0x7f0200a3

    const v1, 0x7f0200a4

    .line 36
    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/MultiSimListPreference;->SMALL_SIM_SLOT_ICON:[I

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/settings/MultiSimListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    sget-object v1, Lcom/android/phone/R$styleable;->MultiSimListPreference:[I

    .line 51
    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/phone/settings/MultiSimListPreference;->mHasDoNotSet:Z

    .line 54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 56
    new-instance v1, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;

    invoke-direct {v1, p0, p1}, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;-><init>(Lcom/android/phone/settings/MultiSimListPreference;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/phone/settings/MultiSimListPreference;->mMultiSimAdapter:Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;

    .line 58
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimListPreference;->createValues()V

    .line 59
    return-void
.end method

.method private createValues()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 95
    const/4 v1, 0x0

    .line 96
    .local v1, "simCount":I
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v5

    invoke-virtual {v5}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/android/phone/settings/MultiSimListPreference;->mSimInfoRecordList:Ljava/util/List;

    .line 97
    iget-object v5, p0, Lcom/android/phone/settings/MultiSimListPreference;->mSimInfoRecordList:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 98
    iget-object v5, p0, Lcom/android/phone/settings/MultiSimListPreference;->mSimInfoRecordList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    .line 102
    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 103
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Dialog;->dismiss()V

    .line 104
    return-void

    .line 107
    :cond_1
    const/4 v4, 0x0

    .line 109
    .local v4, "values":[Ljava/lang/String;
    iget-boolean v5, p0, Lcom/android/phone/settings/MultiSimListPreference;->mHasDoNotSet:Z

    if-eqz v5, :cond_2

    .line 110
    add-int/lit8 v5, v1, 0x1

    new-array v4, v5, [Ljava/lang/String;

    .line 111
    .local v4, "values":[Ljava/lang/String;
    sget v5, Lcom/android/phone/settings/MultiSimListPreference;->DO_NOT_SET:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 112
    const/4 v2, 0x1

    .line 118
    .local v2, "simIndex":I
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    move v3, v2

    .end local v2    # "simIndex":I
    .local v3, "simIndex":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 119
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "simIndex":I
    .restart local v2    # "simIndex":I
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    .line 118
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    .end local v2    # "simIndex":I
    .restart local v3    # "simIndex":I
    goto :goto_1

    .line 114
    .end local v0    # "i":I
    .end local v3    # "simIndex":I
    .local v4, "values":[Ljava/lang/String;
    :cond_2
    new-array v4, v1, [Ljava/lang/String;

    .line 115
    .local v4, "values":[Ljava/lang/String;
    const/4 v2, 0x0

    .restart local v2    # "simIndex":I
    goto :goto_0

    .line 121
    .end local v2    # "simIndex":I
    .restart local v0    # "i":I
    .restart local v3    # "simIndex":I
    :cond_3
    invoke-virtual {p0, v4}, Lcom/android/phone/settings/MultiSimListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 122
    iget-object v5, p0, Lcom/android/phone/settings/MultiSimListPreference;->mMultiSimAdapter:Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;

    invoke-virtual {v5}, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->notifyDataSetChanged()V

    .line 123
    return-void
.end method


# virtual methods
.method public notifySimInfoDataChanged()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimListPreference;->createValues()V

    .line 92
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 3
    .param p1, "positiveResult"    # Z

    .prologue
    .line 80
    if-eqz p1, :cond_0

    iget v1, p0, Lcom/android/phone/settings/MultiSimListPreference;->mClickedDialogEntryIndex:I

    if-ltz v1, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/settings/MultiSimListPreference;->mClickedDialogEntryIndex:I

    aget-object v1, v1, v2

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MultiSimListPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MultiSimListPreference;->setValue(Ljava/lang/String;)V

    .line 88
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 4
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MultiSimListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/phone/settings/MultiSimListPreference;->mClickedDialogEntryIndex:I

    .line 64
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimListPreference;->mMultiSimAdapter:Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;

    iget v1, p0, Lcom/android/phone/settings/MultiSimListPreference;->mClickedDialogEntryIndex:I

    .line 65
    new-instance v2, Lcom/android/phone/settings/MultiSimListPreference$1;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/MultiSimListPreference$1;-><init>(Lcom/android/phone/settings/MultiSimListPreference;)V

    .line 64
    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 75
    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 76
    return-void
.end method
