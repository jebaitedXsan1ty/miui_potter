.class Lcom/android/phone/settings/MobileNetworkSettings$1;
.super Landroid/content/BroadcastReceiver;
.source "MobileNetworkSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/MobileNetworkSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/MobileNetworkSettings;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/MobileNetworkSettings;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/MobileNetworkSettings;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    .line 163
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 166
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 168
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    const-string/jumbo v4, "state"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v3, v4}, Lcom/android/phone/settings/MobileNetworkSettings;->-set0(Lcom/android/phone/settings/MobileNetworkSettings;Z)Z

    .line 169
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-virtual {v3}, Lcom/android/phone/settings/MobileNetworkSettings;->updatePreferenceUI()V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    const-string/jumbo v3, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 171
    const-string/jumbo v3, "ss"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 172
    .local v2, "state":Ljava/lang/String;
    invoke-static {}, Lmiui/telephony/TelephonyManager;->isCustSingleSimDevice()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 173
    sget v3, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    invoke-static {p2, v3}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v1

    .line 174
    .local v1, "slotId":I
    if-ne v1, v5, :cond_2

    .line 175
    sget-object v3, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "skip slot 2 sim state change for cust single sim device."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    return-void

    .line 179
    .end local v1    # "slotId":I
    :cond_2
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isMultiSimEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lmiui/telephony/TelephonyManager;->isCustSingleSimDevice()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 180
    :cond_3
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    const-string/jumbo v4, "ABSENT"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Lcom/android/phone/settings/MobileNetworkSettings;->-wrap0(Lcom/android/phone/settings/MobileNetworkSettings;Z)V

    .line 183
    :cond_4
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-static {v3}, Lcom/android/phone/settings/MobileNetworkSettings;->-get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isCTDualVolteSupported()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "LOADED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 184
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isDualCTSim()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 185
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-static {v3}, Lcom/android/phone/settings/MobileNetworkSettings;->-get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 186
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-static {v3}, Lcom/android/phone/settings/MobileNetworkSettings;->-get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 187
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-static {v3}, Lcom/android/phone/settings/MobileNetworkSettings;->-get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    const v4, 0x7f0b0736

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto :goto_0

    .line 189
    :cond_5
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-static {v3}, Lcom/android/phone/settings/MobileNetworkSettings;->-get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-static {}, Lcom/android/phone/Dual4GManager;->isDual4GEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 190
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-static {v3}, Lcom/android/phone/settings/MobileNetworkSettings;->-get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 191
    iget-object v3, p0, Lcom/android/phone/settings/MobileNetworkSettings$1;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-static {v3}, Lcom/android/phone/settings/MobileNetworkSettings;->-get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v3

    const v4, 0x7f0b06ee

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    goto/16 :goto_0
.end method
