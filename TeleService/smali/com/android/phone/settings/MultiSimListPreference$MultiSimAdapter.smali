.class Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;
.super Landroid/widget/BaseAdapter;
.source "MultiSimListPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/MultiSimListPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiSimAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/android/phone/settings/MultiSimListPreference;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/MultiSimListPreference;Landroid/content/Context;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/phone/settings/MultiSimListPreference;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->this$0:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 130
    iput-object p2, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->mContext:Landroid/content/Context;

    .line 131
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 132
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->this$0:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v0}, Lcom/android/phone/settings/MultiSimListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->getItem(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lmiui/telephony/SubscriptionInfo;
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 141
    iget-object v2, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->this$0:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-static {v2}, Lcom/android/phone/settings/MultiSimListPreference;->-get1(Lcom/android/phone/settings/MultiSimListPreference;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_0

    .line 142
    return-object v1

    .line 144
    :cond_0
    iget-object v2, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->this$0:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-static {v2}, Lcom/android/phone/settings/MultiSimListPreference;->-get1(Lcom/android/phone/settings/MultiSimListPreference;)Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v0, p1, -0x1

    .line 145
    .local v0, "realPosition":I
    :goto_0
    iget-object v2, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->this$0:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-static {v2}, Lcom/android/phone/settings/MultiSimListPreference;->-get2(Lcom/android/phone/settings/MultiSimListPreference;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v0, :cond_1

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->this$0:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-static {v1}, Lcom/android/phone/settings/MultiSimListPreference;->-get2(Lcom/android/phone/settings/MultiSimListPreference;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/telephony/SubscriptionInfo;

    :cond_1
    return-object v1

    .line 144
    .end local v0    # "realPosition":I
    :cond_2
    move v0, p1

    .restart local v0    # "realPosition":I
    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 150
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 165
    if-nez p2, :cond_0

    .line 166
    iget-object v6, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f04003c

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 167
    new-instance v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;-><init>(Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;)V

    .line 169
    .local v5, "viewHolder":Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;
    const v6, 0x7f0d00ca

    .line 168
    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckedTextView;

    iput-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simDiplayName:Landroid/widget/CheckedTextView;

    .line 170
    const v6, 0x7f0d00cb

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simIcon:Landroid/widget/ImageView;

    .line 171
    const v6, 0x7f0d00cc

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simNumber:Landroid/widget/TextView;

    .line 172
    invoke-virtual {p2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 177
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->getItem(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v2

    .line 178
    .local v2, "simInfo":Lmiui/telephony/SubscriptionInfo;
    if-nez v2, :cond_2

    .line 179
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simDiplayName:Landroid/widget/CheckedTextView;

    const v7, 0x7f0b065a

    invoke-virtual {v6, v7}, Landroid/widget/CheckedTextView;->setText(I)V

    .line 180
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simIcon:Landroid/widget/ImageView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 181
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simNumber:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    sget v6, Lcom/android/phone/settings/MultiSimListPreference;->DO_NOT_SET:I

    iget-object v7, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->this$0:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v7}, Lcom/android/phone/settings/MultiSimListPreference;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    if-ne v6, v7, :cond_1

    .line 183
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simDiplayName:Landroid/widget/CheckedTextView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 216
    :goto_1
    return-object p2

    .line 174
    .end local v2    # "simInfo":Lmiui/telephony/SubscriptionInfo;
    .end local v5    # "viewHolder":Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;

    .restart local v5    # "viewHolder":Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;
    goto :goto_0

    .line 185
    .restart local v2    # "simInfo":Lmiui/telephony/SubscriptionInfo;
    :cond_1
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simDiplayName:Landroid/widget/CheckedTextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 188
    :cond_2
    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v6

    invoke-static {v6}, Lcom/android/phone/MiuiPhoneUtils;->isVirtualSim(I)Z

    move-result v0

    .line 189
    .local v0, "isVirtual":Z
    if-eqz v0, :cond_3

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getVirtualSimCarrierName()Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "name":Ljava/lang/CharSequence;
    :goto_2
    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->isActivated()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 191
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simDiplayName:Landroid/widget/CheckedTextView;

    invoke-virtual {v6, v1}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    :goto_3
    if-eqz v0, :cond_5

    const v4, 0x7f02009c

    .line 197
    .local v4, "slotResId":I
    :goto_4
    if-lez v4, :cond_6

    .line 198
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 202
    :goto_5
    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v3

    .line 203
    .local v3, "simNum":Ljava/lang/String;
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simNumber:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simIcon:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 205
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simNumber:Landroid/widget/TextView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setSelected(Z)V

    .line 206
    iget-object v7, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simNumber:Landroid/widget/TextView;

    .line 207
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    const/16 v6, 0x8

    .line 206
    :goto_6
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 209
    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v6

    iget-object v7, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->this$0:Lcom/android/phone/settings/MultiSimListPreference;

    invoke-virtual {v7}, Lcom/android/phone/settings/MultiSimListPreference;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    if-ne v6, v7, :cond_8

    .line 210
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simDiplayName:Landroid/widget/CheckedTextView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 189
    .end local v1    # "name":Ljava/lang/CharSequence;
    .end local v3    # "simNum":Ljava/lang/String;
    .end local v4    # "slotResId":I
    :cond_3
    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v1

    .restart local v1    # "name":Ljava/lang/CharSequence;
    goto :goto_2

    .line 193
    :cond_4
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simDiplayName:Landroid/widget/CheckedTextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 194
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0663

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 193
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 196
    :cond_5
    invoke-static {}, Lcom/android/phone/settings/MultiSimListPreference;->-get0()[I

    move-result-object v6

    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v7

    aget v4, v6, v7

    .restart local v4    # "slotResId":I
    goto :goto_4

    .line 200
    :cond_6
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simIcon:Landroid/widget/ImageView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    .line 207
    .restart local v3    # "simNum":Ljava/lang/String;
    :cond_7
    const/4 v6, 0x0

    goto :goto_6

    .line 212
    :cond_8
    iget-object v6, v5, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter$ViewHolder;->simDiplayName:Landroid/widget/CheckedTextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_1
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/MultiSimListPreference$MultiSimAdapter;->getItem(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v0

    .line 156
    .local v0, "simInfo":Lmiui/telephony/SubscriptionInfo;
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->isActivated()Z

    move-result v1

    return v1

    .line 159
    :cond_0
    const/4 v1, 0x1

    return v1
.end method
