.class Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;
.super Landroid/os/Handler;
.source "PreferredNetworkTypeListPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/PreferredNetworkTypeListPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;


# direct methods
.method private constructor <init>(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference;
    .param p2, "-this1"    # Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;-><init>(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)V

    return-void
.end method

.method private handleGetPreferredNetworkTypeResponse(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 248
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 250
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_0

    .line 251
    iget-object v2, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, [I

    aget v1, v1, v4

    invoke-static {v2, v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-set0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;I)I

    .line 253
    const-string/jumbo v1, "PreferedNetworkTypeListPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "get preferred network type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->setEnabled(Z)V

    .line 255
    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->setValue(Ljava/lang/String;)V

    .line 263
    :goto_0
    return-void

    .line 259
    :cond_0
    const-string/jumbo v1, "PreferedNetworkTypeListPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "get preferred network type, exception = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    invoke-direct {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->setEnabled(Z)V

    .line 261
    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    const-string/jumbo v2, ""

    invoke-static {v1, v2}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-wrap2(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleSetPreferredNetworkTypeResponse(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 266
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 268
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    .line 270
    invoke-direct {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->setEnabled(Z)V

    .line 273
    const-string/jumbo v1, "PreferedNetworkTypeListPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "set preferred network type, exception = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get3(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/Phone;->getPreferredNetworkType(Landroid/os/Message;)V

    .line 295
    :goto_0
    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-virtual {v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->finish()V

    .line 296
    return-void

    .line 276
    :cond_0
    const-string/jumbo v1, "PreferedNetworkTypeListPreference"

    const-string/jumbo v2, "set preferred network type done"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-wrap0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 278
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_1

    .line 280
    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get3(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 281
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "preferred_network_mode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get3(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 282
    iget-object v3, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)I

    move-result v3

    .line 279
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 286
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get3(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 287
    const-string/jumbo v2, "preferred_network_mode"

    .line 288
    iget-object v3, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)I

    move-result v3

    .line 285
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 291
    :cond_2
    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get4(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)I

    move-result v1

    iget-object v2, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v2}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)I

    move-result v2

    invoke-static {v3, v1, v2}, Lcom/android/phone/MiuiPhoneUtils;->setNetworkModeInDb(Ljava/util/List;II)V

    goto :goto_0
.end method

.method private setEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 223
    iget-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-virtual {v0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 224
    return-void
.end method

.method private setValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v0, p1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-wrap2(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;Ljava/lang/String;)V

    .line 220
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 228
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 245
    :goto_0
    return-void

    .line 230
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->handleGetPreferredNetworkTypeResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 235
    :pswitch_1
    iget-object v0, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v0}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get3(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get0(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)I

    move-result v1

    iget-object v2, p0, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->this$0:Lcom/android/phone/settings/PreferredNetworkTypeListPreference;

    invoke-static {v2}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference;->-get1(Lcom/android/phone/settings/PreferredNetworkTypeListPreference;)Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;

    move-result-object v2

    .line 236
    const/4 v3, 0x2

    .line 235
    invoke-virtual {v2, v3}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    goto :goto_0

    .line 240
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PreferredNetworkTypeListPreference$MyHandler;->handleSetPreferredNetworkTypeResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
