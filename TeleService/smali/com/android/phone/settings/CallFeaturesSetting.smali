.class public Lcom/android/phone/settings/CallFeaturesSetting;
.super Lmiui/preference/PreferenceActivity;
.source "CallFeaturesSetting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/CallFeaturesSetting$1;,
        Lcom/android/phone/settings/CallFeaturesSetting$2;
    }
.end annotation


# instance fields
.field private mAntispamSetting:Lmiui/preference/ValuePreference;

.field private mAutoAnswer:Lmiui/preference/ValuePreference;

.field private mCallRecord:Lmiui/preference/ValuePreference;

.field private mCloudAntispam:Lmiui/preference/ValuePreference;

.field private mFirstInit:Z

.field mHandler:Landroid/os/Handler;

.field private mIccCardCount:I

.field private mIsReceiverRegistered:Z

.field private mNetworkSetting:Landroid/preference/PreferenceScreen;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mTelocation:Landroid/preference/PreferenceScreen;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/CallFeaturesSetting;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallFeaturesSetting;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mFirstInit:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/CallFeaturesSetting;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallFeaturesSetting;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mIccCardCount:I

    return v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/CallFeaturesSetting;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallFeaturesSetting;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mFirstInit:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/phone/settings/CallFeaturesSetting;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallFeaturesSetting;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mIccCardCount:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/CallFeaturesSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallFeaturesSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->initScreen()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/CallFeaturesSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallFeaturesSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->updateScreen()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mIsReceiverRegistered:Z

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mFirstInit:Z

    .line 123
    new-instance v0, Lcom/android/phone/settings/CallFeaturesSetting$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/CallFeaturesSetting$1;-><init>(Lcom/android/phone/settings/CallFeaturesSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 155
    new-instance v0, Lcom/android/phone/settings/CallFeaturesSetting$2;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/CallFeaturesSetting$2;-><init>(Lcom/android/phone/settings/CallFeaturesSetting;)V

    iput-object v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mHandler:Landroid/os/Handler;

    .line 73
    return-void
.end method

.method private initScreen()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 228
    const v2, 0x7f060018

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->addPreferencesFromResource(I)V

    .line 230
    const-string/jumbo v2, "pref_key_call_network_setting"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceScreen;

    iput-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mNetworkSetting:Landroid/preference/PreferenceScreen;

    .line 231
    const-string/jumbo v2, "pref_key_call_record_setting"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lmiui/preference/ValuePreference;

    iput-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mCallRecord:Lmiui/preference/ValuePreference;

    .line 232
    const-string/jumbo v2, "pref_key_cloud_antispam"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lmiui/preference/ValuePreference;

    iput-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mCloudAntispam:Lmiui/preference/ValuePreference;

    .line 233
    const-string/jumbo v2, "pref_key_antispam_setting"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lmiui/preference/ValuePreference;

    iput-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mAntispamSetting:Lmiui/preference/ValuePreference;

    .line 234
    const-string/jumbo v2, "button_auto_answer_screen"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lmiui/preference/ValuePreference;

    iput-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mAutoAnswer:Lmiui/preference/ValuePreference;

    .line 235
    const-string/jumbo v2, "pref_key_telocation"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceScreen;

    iput-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mTelocation:Landroid/preference/PreferenceScreen;

    .line 237
    invoke-virtual {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 239
    .local v1, "prefScreen":Landroid/preference/PreferenceScreen;
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mCallRecord:Lmiui/preference/ValuePreference;

    if-eqz v2, :cond_0

    .line 240
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mCallRecord:Lmiui/preference/ValuePreference;

    invoke-virtual {v2, v3}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    .line 241
    invoke-static {p0}, Lcom/android/phone/TelephonyCapabilities;->supportsCallRecording(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 242
    const-string/jumbo v2, "pref_key_call_record_setting"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 246
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->isVoipAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 247
    const-string/jumbo v2, "pref_key_voip_setting"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 250
    :cond_1
    invoke-direct {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->isLivetalkAvaible()Z

    move-result v2

    if-nez v2, :cond_2

    .line 251
    const-string/jumbo v2, "pref_key_livetalk_setting"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 254
    :cond_2
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mCloudAntispam:Lmiui/preference/ValuePreference;

    if-eqz v2, :cond_3

    .line 255
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mCloudAntispam:Lmiui/preference/ValuePreference;

    invoke-virtual {v2, v3}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    .line 256
    invoke-static {p0}, Lmiui/yellowpage/YellowPageUtils;->isYellowPageAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 257
    const-string/jumbo v2, "pref_key_cloud_antispam"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 269
    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mAntispamSetting:Lmiui/preference/ValuePreference;

    if-eqz v2, :cond_4

    .line 270
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mAntispamSetting:Lmiui/preference/ValuePreference;

    invoke-virtual {v2, v3}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    .line 273
    :cond_4
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mAutoAnswer:Lmiui/preference/ValuePreference;

    if-eqz v2, :cond_5

    .line 274
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mAutoAnswer:Lmiui/preference/ValuePreference;

    invoke-virtual {v2, v3}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    .line 277
    :cond_5
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_6

    .line 278
    const-string/jumbo v2, "pref_key_auto_ip"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    .line 280
    :cond_6
    return-void

    .line 259
    :cond_7
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "miui.intent.action.TURN_ON_SMART_ANTISPAM"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 260
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "com.miui.yellowpage"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    invoke-virtual {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    if-nez v2, :cond_8

    .line 262
    const-string/jumbo v2, "pref_key_cloud_antispam"

    invoke-static {v1, v2}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    goto :goto_0

    .line 264
    :cond_8
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mCloudAntispam:Lmiui/preference/ValuePreference;

    invoke-virtual {v2, v0}, Lmiui/preference/ValuePreference;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private isLivetalkAvaible()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 405
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 406
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lmiui/telephony/livetalk/LivetalkUtils;->getLivetalkStatus(Landroid/content/Context;)I

    move-result v2

    if-eq v2, v6, :cond_0

    .line 407
    return v5

    .line 410
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.miui.milivetalk"

    .line 411
    const/16 v4, 0x80

    .line 410
    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 412
    return v6

    .line 413
    :catch_0
    move-exception v1

    .line 415
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    return v5
.end method

.method private isVoipAvailable()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 390
    invoke-virtual {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 391
    return v4

    .line 395
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.miui.voip"

    .line 396
    const/16 v3, 0x80

    .line 395
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    const/4 v1, 0x1

    return v1

    .line 398
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    return v4
.end method

.method public static removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z
    .locals 5
    .param p0, "group"    # Landroid/preference/PreferenceGroup;
    .param p1, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 374
    const/4 v3, 0x0

    .line 375
    .local v3, "ret":Z
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 376
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    .line 377
    .local v2, "prefCount":I
    const/4 v0, 0x0

    .end local v3    # "ret":Z
    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    xor-int/lit8 v4, v3, 0x1

    if-eqz v4, :cond_2

    .line 378
    invoke-virtual {p0, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    .line 379
    .local v1, "p":Landroid/preference/Preference;
    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 380
    invoke-virtual {p0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    move-result v3

    .line 377
    .end local v1    # "p":Landroid/preference/Preference;
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 381
    .restart local v1    # "p":Landroid/preference/Preference;
    :cond_1
    instance-of v4, v1, Landroid/preference/PreferenceGroup;

    if-eqz v4, :cond_0

    .line 382
    check-cast v1, Landroid/preference/PreferenceGroup;

    .end local v1    # "p":Landroid/preference/Preference;
    invoke-static {v1, p1}, Lcom/android/phone/settings/CallFeaturesSetting;->removePreference(Landroid/preference/PreferenceGroup;Ljava/lang/String;)Z

    move-result v3

    .local v3, "ret":Z
    goto :goto_1

    .line 386
    .end local v0    # "i":I
    .end local v2    # "prefCount":I
    .end local v3    # "ret":Z
    :cond_2
    return v3
.end method

.method private setEnabledForKey(ZLjava/lang/String;)V
    .locals 2
    .param p1, "enabled"    # Z
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 368
    .local v0, "pref":Landroid/preference/Preference;
    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 371
    :cond_0
    return-void
.end method

.method private updateScreen()V
    .locals 22

    .prologue
    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mNetworkSetting:Landroid/preference/PreferenceScreen;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 285
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lmiui/telephony/TelephonyManager;->isMultiSimEnabled()Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-static {}, Lmiui/telephony/TelephonyManager;->isCustSingleSimDevice()Z

    move-result v17

    xor-int/lit8 v17, v17, 0x1

    if-eqz v17, :cond_5

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mNetworkSetting:Landroid/preference/PreferenceScreen;

    move-object/from16 v17, v0

    const v18, 0x7f0b0645

    invoke-virtual/range {v17 .. v18}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 292
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mCallRecord:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    .line 293
    invoke-static/range {p0 .. p0}, Lcom/android/phone/settings/CallRecordSetting;->isAutoRecordOn(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mCallRecord:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    const v18, 0x7f0b05ae

    invoke-virtual/range {v17 .. v18}, Lmiui/preference/ValuePreference;->setValue(I)V

    .line 300
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mTelocation:Landroid/preference/PreferenceScreen;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 303
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/provider/MiuiSettings$Telephony;->isTelocationEnable(Landroid/content/ContentResolver;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 304
    const v17, 0x7f0b063e

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 308
    .local v15, "summaryTeloc":Ljava/lang/String;
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/provider/MiuiSettings$Telephony;->isAutoCountryCodeEnable(Landroid/content/ContentResolver;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 310
    const v17, 0x7f0b063f

    .line 309
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 315
    .local v14, "summaryCountryCode":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mTelocation:Landroid/preference/PreferenceScreen;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string/jumbo v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 318
    .end local v14    # "summaryCountryCode":Ljava/lang/String;
    .end local v15    # "summaryTeloc":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mAntispamSetting:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    if-eqz v17, :cond_3

    .line 319
    invoke-static/range {p0 .. p0}, Landroid/provider/MiuiSettings$AntiSpam;->isAntiSpam(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 320
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mAntispamSetting:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    const v18, 0x7f0b05ae

    invoke-virtual/range {v17 .. v18}, Lmiui/preference/ValuePreference;->setValue(I)V

    .line 326
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mAutoAnswer:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 327
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v17

    .line 328
    const-string/jumbo v18, "button_auto_answer"

    const/16 v19, 0x0

    .line 327
    invoke-interface/range {v17 .. v19}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    if-nez v17, :cond_a

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mAutoAnswer:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    const v18, 0x7f0b05d9

    invoke-virtual/range {v17 .. v18}, Lmiui/preference/ValuePreference;->setValue(I)V

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mAutoAnswer:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    const-string/jumbo v18, ""

    invoke-virtual/range {v17 .. v18}, Lmiui/preference/ValuePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 346
    :cond_4
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    .line 347
    const-string/jumbo v18, "airplane_mode_on"

    const/16 v19, 0x0

    .line 346
    invoke-static/range {v17 .. v19}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v17

    if-lez v17, :cond_b

    const/4 v11, 0x1

    .line 348
    .local v11, "isAirPlane":Z
    :goto_6
    const/4 v6, 0x0

    .line 349
    .local v6, "fdnDisabled":Z
    const/4 v7, 0x0

    .line 350
    .local v7, "hasICC":Z
    if-nez v11, :cond_e

    .line 351
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v13

    .line 352
    .local v13, "slotCount":I
    const/16 v17, 0x1

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lmiui/telephony/VirtualSimUtils;->getVirtualSimSlot(Landroid/content/Context;ZI)I

    move-result v16

    .line 353
    .local v16, "virtualSlot":I
    const/4 v9, 0x0

    .end local v6    # "fdnDisabled":Z
    .end local v7    # "hasICC":Z
    .local v9, "i":I
    :goto_7
    if-ge v9, v13, :cond_e

    .line 354
    invoke-static {v9}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v12

    .line 355
    .local v12, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v12}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/android/internal/telephony/IccCard;->hasIccCard()Z

    move-result v17

    if-eqz v17, :cond_c

    move/from16 v0, v16

    if-eq v9, v0, :cond_c

    const/4 v8, 0x1

    .line 356
    .local v8, "hasIccCard":Z
    :goto_8
    if-eqz v8, :cond_d

    invoke-virtual {v12}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/android/internal/telephony/IccCard;->getIccFdnEnabled()Z

    move-result v17

    xor-int/lit8 v17, v17, 0x1

    :goto_9
    or-int v6, v6, v17

    .line 357
    .local v6, "fdnDisabled":Z
    or-int/2addr v7, v8

    .line 353
    .local v7, "hasICC":Z
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 288
    .end local v6    # "fdnDisabled":Z
    .end local v7    # "hasICC":Z
    .end local v8    # "hasIccCard":Z
    .end local v9    # "i":I
    .end local v11    # "isAirPlane":Z
    .end local v12    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v13    # "slotCount":I
    .end local v16    # "virtualSlot":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mNetworkSetting:Landroid/preference/PreferenceScreen;

    move-object/from16 v17, v0

    const v18, 0x7f0b037e

    invoke-virtual/range {v17 .. v18}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    goto/16 :goto_0

    .line 296
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mCallRecord:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    const v18, 0x7f0b05af

    invoke-virtual/range {v17 .. v18}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto/16 :goto_1

    .line 306
    :cond_7
    const v17, 0x7f0b063d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->getString(I)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "summaryTeloc":Ljava/lang/String;
    goto/16 :goto_2

    .line 313
    :cond_8
    const v17, 0x7f0b0643

    .line 312
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->getString(I)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "summaryCountryCode":Ljava/lang/String;
    goto/16 :goto_3

    .line 322
    .end local v14    # "summaryCountryCode":Ljava/lang/String;
    .end local v15    # "summaryTeloc":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mAntispamSetting:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    const v18, 0x7f0b05af

    invoke-virtual/range {v17 .. v18}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto/16 :goto_4

    .line 333
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    .line 332
    invoke-static/range {v17 .. v17}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v17

    .line 334
    const-string/jumbo v18, "button_auto_answer_delay"

    const-string/jumbo v19, "3"

    .line 332
    invoke-interface/range {v17 .. v19}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 335
    .local v5, "delay":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 336
    const v18, 0x7f07007b

    .line 335
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 338
    .local v3, "autoAnswerSummary":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    .line 337
    invoke-static/range {v17 .. v17}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v17

    .line 338
    const-string/jumbo v18, "button_auto_answer_scenario"

    .line 339
    const/16 v19, 0x0

    .line 337
    invoke-interface/range {v17 .. v19}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 340
    .local v10, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mAutoAnswer:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    aget-object v18, v3, v10

    invoke-virtual/range {v17 .. v18}, Lmiui/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/CallFeaturesSetting;->mAutoAnswer:Lmiui/preference/ValuePreference;

    move-object/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    const/16 v21, 0x0

    aput-object v20, v19, v21

    const v20, 0x7f100005

    move-object/from16 v0, v18

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lmiui/preference/ValuePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 346
    .end local v3    # "autoAnswerSummary":[Ljava/lang/String;
    .end local v5    # "delay":I
    .end local v10    # "index":I
    :cond_b
    const/4 v11, 0x0

    .restart local v11    # "isAirPlane":Z
    goto/16 :goto_6

    .line 355
    .restart local v9    # "i":I
    .restart local v12    # "phone":Lcom/android/internal/telephony/Phone;
    .restart local v13    # "slotCount":I
    .restart local v16    # "virtualSlot":I
    :cond_c
    const/4 v8, 0x0

    .restart local v8    # "hasIccCard":Z
    goto/16 :goto_8

    .line 356
    :cond_d
    const/16 v17, 0x0

    goto/16 :goto_9

    .line 360
    .end local v8    # "hasIccCard":Z
    .end local v9    # "i":I
    .end local v12    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v13    # "slotCount":I
    .end local v16    # "virtualSlot":I
    :cond_e
    if-eqz v6, :cond_f

    if-eqz v7, :cond_f

    xor-int/lit8 v4, v11, 0x1

    .line 361
    :goto_a
    const-string/jumbo v17, "button_call_forwarding"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v4, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->setEnabledForKey(ZLjava/lang/String;)V

    .line 362
    const-string/jumbo v17, "button_call_waiting"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v4, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->setEnabledForKey(ZLjava/lang/String;)V

    .line 364
    return-void

    .line 360
    :cond_f
    const/4 v4, 0x0

    .local v4, "canBeEnabled":Z
    goto :goto_a
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 174
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 177
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-eqz v2, :cond_0

    .line 178
    const v2, 0x7f0b06f1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 180
    invoke-virtual {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->finish()V

    .line 181
    return-void

    .line 183
    :cond_0
    invoke-static {v3}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 185
    invoke-virtual {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 186
    .local v0, "actionBar":Lmiui/app/ActionBar;
    if-eqz v0, :cond_1

    .line 187
    invoke-virtual {v0, v4}, Lmiui/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 190
    :cond_1
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/telephony/TelephonyManager;->getIccCardCount()I

    move-result v2

    iput v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mIccCardCount:I

    .line 191
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->isDcOnlyVirtualSim(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 192
    iget v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mIccCardCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mIccCardCount:I

    .line 194
    :cond_2
    invoke-direct {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->initScreen()V

    .line 195
    new-instance v1, Landroid/content/IntentFilter;

    .line 196
    const-string/jumbo v2, "android.intent.action.RADIO_TECHNOLOGY"

    .line 195
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 197
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v2, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 198
    iget-object v2, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/android/phone/settings/CallFeaturesSetting;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 199
    iput-boolean v4, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mIsReceiverRegistered:Z

    .line 200
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 210
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    .line 211
    iget-object v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 212
    iget-boolean v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mIsReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mIsReceiverRegistered:Z

    .line 214
    iget-object v0, p0, Lcom/android/phone/settings/CallFeaturesSetting;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/CallFeaturesSetting;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 216
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 220
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->finish()V

    .line 222
    const/4 v0, 0x1

    return v0

    .line 224
    :cond_0
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 204
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 205
    invoke-direct {p0}, Lcom/android/phone/settings/CallFeaturesSetting;->updateScreen()V

    .line 206
    return-void
.end method
