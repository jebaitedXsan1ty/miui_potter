.class public Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;
.super Landroid/app/DialogFragment;
.source "MultiSimInfoEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/MultiSimInfoEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DisableSimDialogFragment"
.end annotation


# instance fields
.field private mName:Ljava/lang/CharSequence;

.field private mSimHandler:Landroid/os/Handler;

.field private mSimSlotId:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mSimHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 658
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 654
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mSimSlotId:I

    .line 660
    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Landroid/os/Handler;)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "simName"    # Ljava/lang/CharSequence;
    .param p3, "h"    # Landroid/os/Handler;

    .prologue
    .line 662
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 654
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mSimSlotId:I

    .line 663
    iput p1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mSimSlotId:I

    .line 664
    iput-object p2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mName:Ljava/lang/CharSequence;

    .line 665
    iput-object p3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mSimHandler:Landroid/os/Handler;

    .line 666
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 670
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 671
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->setCancelable(Z)V

    .line 672
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 676
    const/4 v1, 0x0

    .line 678
    .local v1, "canDisable":Z
    const v4, 0x7f0b0662

    .line 679
    .local v4, "negativeBtnText":I
    const-string/jumbo v5, "ril.cdma.inecmmode"

    invoke-static {v5, v8}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 680
    .local v2, "isInEcm":Z
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultVoiceSlotId()I

    move-result v5

    iget v6, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mSimSlotId:I

    if-eq v5, v6, :cond_0

    .line 681
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v5

    iget v6, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mSimSlotId:I

    if-ne v5, v6, :cond_2

    .line 682
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 683
    const v6, 0x7f0b0660

    .line 682
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 696
    .local v3, "message":Ljava/lang/String;
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 697
    .local v0, "alertDialog":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f0b065e

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 698
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 699
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 700
    if-eqz v1, :cond_1

    .line 702
    new-instance v5, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment$1;

    invoke-direct {v5, p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment$1;-><init>(Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;)V

    .line 701
    const v6, 0x104000a

    invoke-virtual {v0, v6, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 712
    :cond_1
    new-instance v5, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment$2;

    invoke-direct {v5, p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment$2;-><init>(Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 720
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5

    .line 684
    .end local v0    # "alertDialog":Landroid/app/AlertDialog$Builder;
    .end local v3    # "message":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v5

    iget v6, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mSimSlotId:I

    if-eq v5, v6, :cond_3

    if-eqz v2, :cond_3

    .line 685
    const-string/jumbo v5, "MultiSimInfoEditorActivity"

    const-string/jumbo v6, "In emergency call back mode ,deactivating the telecom vice card is forbidden"

    invoke-static {v5, v6}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 687
    const v6, 0x7f0b0661

    .line 686
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "message":Ljava/lang/String;
    goto :goto_0

    .line 689
    .end local v3    # "message":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x1

    .line 690
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    .line 692
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->mName:Ljava/lang/CharSequence;

    aput-object v7, v6, v8

    .line 691
    const v7, 0x7f0b065f

    .line 690
    invoke-virtual {v5, v7, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 693
    .restart local v3    # "message":Ljava/lang/String;
    const/high16 v4, 0x1040000

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 725
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 726
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->updateEnableSimChecked(Z)V

    .line 727
    :cond_0
    return-void
.end method
