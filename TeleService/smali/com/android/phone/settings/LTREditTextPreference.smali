.class public Lcom/android/phone/settings/LTREditTextPreference;
.super Lcom/android/phone/settings/CustomEditTextPreference;
.source "LTREditTextPreference.java"


# instance fields
.field private isSummaryLTR:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CustomEditTextPreference;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/LTREditTextPreference;->isSummaryLTR:Z

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/android/phone/settings/CustomEditTextPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/LTREditTextPreference;->isSummaryLTR:Z

    .line 24
    return-void
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/android/phone/settings/LTREditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 45
    .local v0, "editText":Landroid/widget/EditText;
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 47
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextDirection(I)V

    .line 48
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextAlignment(I)V

    .line 50
    :cond_0
    invoke-super {p0, p1}, Lcom/android/phone/settings/CustomEditTextPreference;->onBindDialogView(Landroid/view/View;)V

    .line 51
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x3

    .line 28
    iget-boolean v2, p0, Lcom/android/phone/settings/LTREditTextPreference;->isSummaryLTR:Z

    if-eqz v2, :cond_0

    .line 29
    const v2, 0x1020010

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 30
    .local v0, "summaryView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextDirection(I)V

    .line 35
    .end local v0    # "summaryView":Landroid/widget/TextView;
    :cond_0
    sget v2, Lmiui/R$id;->value_right:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 36
    .local v1, "valueView":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 37
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextDirection(I)V

    .line 39
    :cond_1
    invoke-super {p0, p1}, Lcom/android/phone/settings/CustomEditTextPreference;->onBindView(Landroid/view/View;)V

    .line 40
    return-void
.end method

.method public setSummaryLTR(Z)V
    .locals 0
    .param p1, "ltr"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/android/phone/settings/LTREditTextPreference;->isSummaryLTR:Z

    .line 55
    return-void
.end method
