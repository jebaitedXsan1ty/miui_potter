.class public Lcom/android/phone/settings/CallForwardEditPreference;
.super Lcom/android/phone/settings/EditPhoneNumberPreference;
.source "CallForwardEditPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;
    }
.end annotation


# static fields
.field private static final SRC_TAGS:[Ljava/lang/String;


# instance fields
.field private DBG:Z

.field public callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

.field private mButtonClicked:I

.field private mHandler:Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;

.field private mServiceClass:I

.field private mSummaryOnTemplate:Ljava/lang/CharSequence;

.field phone:Lcom/android/internal/telephony/Phone;

.field public reason:I

.field tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/CallForwardEditPreference;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallForwardEditPreference;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->DBG:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/CallForwardEditPreference;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallForwardEditPreference;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mButtonClicked:I

    return v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/CallForwardEditPreference;)Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallForwardEditPreference;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mHandler:Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/CallForwardEditPreference;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/CallForwardEditPreference;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mServiceClass:I

    return v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/CallForwardEditPreference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/CallForwardEditPreference;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/CallForwardEditPreference;->updateSummaryText()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "{0}"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/phone/settings/CallForwardEditPreference;->SRC_TAGS:[Ljava/lang/String;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/settings/CallForwardEditPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/android/phone/settings/EditPhoneNumberPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    sget v1, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v4, 0x2

    if-lt v1, v4, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->DBG:Z

    .line 53
    new-instance v1, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;-><init>(Lcom/android/phone/settings/CallForwardEditPreference;Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;)V

    iput-object v1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mHandler:Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;

    .line 62
    invoke-virtual {p0}, Lcom/android/phone/settings/CallForwardEditPreference;->getSummaryOn()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mSummaryOnTemplate:Ljava/lang/CharSequence;

    .line 65
    sget-object v1, Lcom/android/phone/R$styleable;->CallForwardEditPreference:[I

    const v4, 0x7f0c0194

    .line 64
    invoke-virtual {p1, p2, v1, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 66
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mServiceClass:I

    .line 68
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    .line 70
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    iget-boolean v1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->DBG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mServiceClass="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mServiceClass:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", reason="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_0
    return-void

    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_1
    move v1, v3

    .line 40
    goto :goto_0
.end method

.method private updateSummaryText()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 182
    invoke-virtual {p0}, Lcom/android/phone/settings/CallForwardEditPreference;->isToggled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 184
    invoke-virtual {p0}, Lcom/android/phone/settings/CallForwardEditPreference;->getRawPhoneNumber()Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "number":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 186
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v3

    invoke-static {v0}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    aput-object v0, v2, v5

    .line 188
    .local v2, "values":[Ljava/lang/String;
    iget-object v3, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mSummaryOnTemplate:Ljava/lang/CharSequence;

    sget-object v4, Lcom/android/phone/settings/CallForwardEditPreference;->SRC_TAGS:[Ljava/lang/String;

    invoke-static {v3, v4, v2}, Landroid/text/TextUtils;->replace(Ljava/lang/CharSequence;[Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 192
    .end local v2    # "values":[Ljava/lang/String;
    .local v1, "summaryOn":Ljava/lang/CharSequence;
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/phone/settings/CallForwardEditPreference;->setSummaryOn(Ljava/lang/CharSequence;)Lcom/android/phone/settings/EditPhoneNumberPreference;

    .line 195
    .end local v0    # "number":Ljava/lang/String;
    .end local v1    # "summaryOn":Ljava/lang/CharSequence;
    :cond_0
    return-void

    .line 190
    .restart local v0    # "number":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/CallForwardEditPreference;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b033c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "summaryOn":Ljava/lang/CharSequence;
    goto :goto_0
.end method


# virtual methods
.method public handleCallForwardResult(Lcom/android/internal/telephony/CallForwardInfo;)V
    .locals 4
    .param p1, "cf"    # Lcom/android/internal/telephony/CallForwardInfo;

    .prologue
    const/4 v0, 0x1

    .line 173
    iput-object p1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    .line 174
    iget-boolean v1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->DBG:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handleGetCFResponse done, callForwardInfo="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget v1, v1, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    if-ne v1, v0, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/phone/settings/CallForwardEditPreference;->setToggled(Z)Lcom/android/phone/settings/EditPhoneNumberPreference;

    .line 177
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget-object v0, v0, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/CallForwardEditPreference;->setPhoneNumber(Ljava/lang/String;)Lcom/android/phone/settings/EditPhoneNumberPreference;

    .line 178
    invoke-direct {p0}, Lcom/android/phone/settings/CallForwardEditPreference;->updateSummaryText()V

    .line 179
    return-void

    .line 176
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleCallForwardResult(Lcom/android/internal/telephony/CallForwardInfo;Z)V
    .locals 0
    .param p1, "cf"    # Lcom/android/internal/telephony/CallForwardInfo;
    .param p2, "enabled"    # Z

    .prologue
    .line 168
    invoke-virtual {p0, p1}, Lcom/android/phone/settings/CallForwardEditPreference;->handleCallForwardResult(Lcom/android/internal/telephony/CallForwardInfo;)V

    .line 169
    invoke-virtual {p0, p2}, Lcom/android/phone/settings/CallForwardEditPreference;->setEnabled(Z)V

    .line 170
    return-void
.end method

.method public init(Lcom/android/phone/settings/TimeConsumingPreferenceListener;ZI)V
    .locals 6
    .param p1, "listener"    # Lcom/android/phone/settings/TimeConsumingPreferenceListener;
    .param p2, "skipReading"    # Z
    .param p3, "subscription"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 85
    iget-boolean v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->DBG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "CallForwardEditPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Getting CallForwardEditPreference subscription ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :cond_0
    invoke-static {p3}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->phone:Lcom/android/internal/telephony/Phone;

    .line 88
    iput-object p1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    .line 89
    if-nez p2, :cond_1

    .line 90
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->phone:Lcom/android/internal/telephony/Phone;

    iget v1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    iget v2, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mServiceClass:I

    .line 91
    iget-object v3, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mHandler:Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;

    invoke-virtual {v3, v4, v4, v4, v5}, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    .line 90
    invoke-static {v0, v1, v2, v3}, Lcom/android/phone/PhoneProxy;->getCallForwardingOption(Lcom/android/internal/telephony/Phone;IILandroid/os/Message;)V

    .line 96
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    .line 100
    :cond_1
    return-void
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 105
    const/4 v0, -0x2

    iput v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mButtonClicked:I

    .line 106
    invoke-super {p0, p1}, Lcom/android/phone/settings/EditPhoneNumberPreference;->onBindDialogView(Landroid/view/View;)V

    .line 107
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 111
    invoke-super {p0, p1, p2}, Lcom/android/phone/settings/EditPhoneNumberPreference;->onClick(Landroid/content/DialogInterface;I)V

    .line 112
    iput p2, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mButtonClicked:I

    .line 113
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 8
    .param p1, "positiveResult"    # Z

    .prologue
    const/4 v7, 0x3

    .line 117
    invoke-super {p0, p1}, Lcom/android/phone/settings/EditPhoneNumberPreference;->onDialogClosed(Z)V

    .line 119
    iget-boolean v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->DBG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mButtonClicked="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mButtonClicked:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 120
    const-string/jumbo v4, ", positiveResult="

    .line 119
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_0
    iget v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mButtonClicked:I

    const/4 v2, -0x2

    if-eq v0, v2, :cond_3

    .line 124
    invoke-virtual {p0}, Lcom/android/phone/settings/CallForwardEditPreference;->isToggled()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mButtonClicked:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_4

    .line 125
    :cond_1
    const/4 v1, 0x3

    .line 127
    .local v1, "action":I
    :goto_0
    iget v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v5, 0x0

    .line 128
    .local v5, "time":I
    :goto_1
    invoke-virtual {p0}, Lcom/android/phone/settings/CallForwardEditPreference;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 130
    .local v3, "number":Ljava/lang/String;
    iget-boolean v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->DBG:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "callForwardInfo="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_2
    if-ne v1, v7, :cond_6

    .line 133
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    if-eqz v0, :cond_6

    .line 134
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget v0, v0, Lcom/android/internal/telephony/CallForwardInfo;->status:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    .line 135
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->callForwardInfo:Lcom/android/internal/telephony/CallForwardInfo;

    iget-object v0, v0, Lcom/android/internal/telephony/CallForwardInfo;->number:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 132
    if-eqz v0, :cond_6

    .line 137
    iget-boolean v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->DBG:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "CallForwardEditPreference"

    const-string/jumbo v2, "no change, do nothing"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    .end local v1    # "action":I
    .end local v3    # "number":Ljava/lang/String;
    .end local v5    # "time":I
    :cond_3
    :goto_2
    return-void

    .line 126
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "action":I
    goto :goto_0

    .line 127
    :cond_5
    const/16 v5, 0x14

    .restart local v5    # "time":I
    goto :goto_1

    .line 140
    .restart local v3    # "number":Ljava/lang/String;
    :cond_6
    iget-boolean v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->DBG:Z

    if-eqz v0, :cond_7

    const-string/jumbo v0, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "reason="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ", action="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 141
    const-string/jumbo v4, ", number="

    .line 140
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 141
    invoke-static {v3}, Lmiui/telephony/PhoneNumberUtils;->toLogSafePhoneNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 140
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_7
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/CallForwardEditPreference;->setSummaryOn(Ljava/lang/CharSequence;)Lcom/android/phone/settings/EditPhoneNumberPreference;

    .line 150
    const-string/jumbo v0, "CallForwardEditPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onDialogClosed mServiceClass="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mServiceClass:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->phone:Lcom/android/internal/telephony/Phone;

    .line 152
    iget v2, p0, Lcom/android/phone/settings/CallForwardEditPreference;->reason:I

    .line 154
    iget v4, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mServiceClass:I

    .line 156
    iget-object v6, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mHandler:Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;

    invoke-virtual {v6, v7, v1, v7}, Lcom/android/phone/settings/CallForwardEditPreference$MyHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v6

    .line 151
    invoke-static/range {v0 .. v6}, Lcom/android/phone/PhoneProxy;->setCallForwardingOption(Lcom/android/internal/telephony/Phone;IILjava/lang/String;IILandroid/os/Message;)V

    .line 160
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    if-eqz v0, :cond_3

    .line 161
    iget-object v0, p0, Lcom/android/phone/settings/CallForwardEditPreference;->tcpListener:Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    const/4 v2, 0x0

    invoke-interface {v0, p0, v2}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    goto :goto_2
.end method

.method public setServiceClass(I)V
    .locals 0
    .param p1, "serviceClass"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/android/phone/settings/CallForwardEditPreference;->mServiceClass:I

    .line 77
    return-void
.end method
