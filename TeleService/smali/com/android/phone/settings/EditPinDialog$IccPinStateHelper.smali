.class Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;
.super Ljava/lang/Object;
.source "EditPinDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/EditPinDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IccPinStateHelper"
.end annotation


# static fields
.field private static sInitialized:[Z

.field private static sLocker:[Ljava/lang/Object;

.field private static sPin2Locked:[Z


# direct methods
.method static synthetic -get0()[Z
    .locals 1

    sget-object v0, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sInitialized:[Z

    return-object v0
.end method

.method static synthetic -get1()[Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sLocker:[Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic -get2()[Z
    .locals 1

    sget-object v0, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sPin2Locked:[Z

    return-object v0
.end method

.method static synthetic -wrap0(I)Lcom/android/internal/telephony/CommandsInterface;
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    invoke-static {p0}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->getCommandsInterface(I)Lcom/android/internal/telephony/CommandsInterface;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Landroid/os/AsyncResult;I)V
    .locals 0
    .param p0, "ar"    # Landroid/os/AsyncResult;
    .param p1, "slotId"    # I

    .prologue
    invoke-static {p0, p1}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->handlerIccCardStatus(Landroid/os/AsyncResult;I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 604
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    new-array v0, v0, [Z

    sput-object v0, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sInitialized:[Z

    .line 605
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    new-array v0, v0, [Z

    sput-object v0, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sPin2Locked:[Z

    .line 606
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sLocker:[Ljava/lang/Object;

    .line 597
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getCommandsInterface(I)Lcom/android/internal/telephony/CommandsInterface;
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 669
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/PhoneProxy;->getCommandsInterface(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/CommandsInterface;

    move-result-object v0

    return-object v0
.end method

.method private static handlerIccCardStatus(Landroid/os/AsyncResult;I)V
    .locals 9
    .param p0, "ar"    # Landroid/os/AsyncResult;
    .param p1, "slotId"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 673
    iget-object v1, p0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, Lcom/android/internal/telephony/uicc/IccCardStatus;

    .line 674
    .local v1, "cardStatus":Lcom/android/internal/telephony/uicc/IccCardStatus;
    sget-object v6, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sPin2Locked:[Z

    aput-boolean v5, v6, p1

    .line 675
    if-eqz v1, :cond_1

    .line 676
    const/4 v2, -0x1

    .line 677
    .local v2, "index":I
    invoke-static {v5}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v3

    .line 678
    .local v3, "phoneType":I
    if-ne v3, v4, :cond_2

    .line 679
    iget v2, v1, Lcom/android/internal/telephony/uicc/IccCardStatus;->mGsmUmtsSubscriptionAppIndex:I

    .line 683
    :cond_0
    :goto_0
    if-ltz v2, :cond_1

    iget-object v6, v1, Lcom/android/internal/telephony/uicc/IccCardStatus;->mApplications:[Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;

    array-length v6, v6

    if-ge v2, v6, :cond_1

    .line 684
    iget-object v6, v1, Lcom/android/internal/telephony/uicc/IccCardStatus;->mApplications:[Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;

    aget-object v0, v6, v2

    .line 685
    .local v0, "appStatus":Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;
    if-eqz v0, :cond_1

    .line 686
    sget-object v6, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sPin2Locked:[Z

    iget-object v7, v0, Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;->pin2:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    sget-object v8, Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;->PINSTATE_ENABLED_BLOCKED:Lcom/android/internal/telephony/uicc/IccCardStatus$PinState;

    if-ne v7, v8, :cond_3

    :goto_1
    aput-boolean v4, v6, p1

    .line 691
    .end local v0    # "appStatus":Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;
    .end local v2    # "index":I
    .end local v3    # "phoneType":I
    :cond_1
    sget-object v4, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sLocker:[Ljava/lang/Object;

    aget-object v5, v4, p1

    monitor-enter v5

    .line 692
    :try_start_0
    sget-object v4, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sInitialized:[Z

    const/4 v6, 0x1

    aput-boolean v6, v4, p1

    .line 693
    sget-object v4, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sLocker:[Ljava/lang/Object;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v5

    .line 695
    return-void

    .line 680
    .restart local v2    # "index":I
    .restart local v3    # "phoneType":I
    :cond_2
    const/4 v6, 0x2

    if-ne v3, v6, :cond_0

    .line 681
    iget v2, v1, Lcom/android/internal/telephony/uicc/IccCardStatus;->mCdmaSubscriptionAppIndex:I

    goto :goto_0

    .restart local v0    # "appStatus":Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;
    :cond_3
    move v4, v5

    .line 686
    goto :goto_1

    .line 691
    .end local v0    # "appStatus":Lcom/android/internal/telephony/uicc/IccCardApplicationStatus;
    .end local v2    # "index":I
    .end local v3    # "phoneType":I
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method public static isInitialized(I)Z
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 635
    sget-object v0, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sInitialized:[Z

    aget-boolean v0, v0, p0

    return v0
.end method

.method public static isPin1Locked(I)Z
    .locals 2
    .param p0, "slotId"    # I

    .prologue
    .line 650
    invoke-static {p0}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/telephony/IccCard;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    move-result-object v0

    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPin2Locked(I)Z
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 654
    sget-object v0, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sPin2Locked:[Z

    aget-boolean v0, v0, p0

    return v0
.end method

.method public static updateIccCardStatus()V
    .locals 1

    .prologue
    .line 610
    new-instance v0, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper$1;

    invoke-direct {v0}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper$1;-><init>()V

    invoke-virtual {v0}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper$1;->start()V

    .line 632
    return-void
.end method

.method public static updatePin2State(Landroid/os/AsyncResult;I)V
    .locals 3
    .param p0, "ar"    # Landroid/os/AsyncResult;
    .param p1, "slotId"    # I

    .prologue
    .line 658
    iget-object v1, p0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-nez v1, :cond_1

    .line 659
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sPin2Locked:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, p1

    .line 666
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    iget-object v1, p0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    instance-of v1, v1, Lcom/android/internal/telephony/CommandException;

    if-eqz v1, :cond_0

    .line 661
    iget-object v0, p0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v0, Lcom/android/internal/telephony/CommandException;

    .line 662
    .local v0, "ce":Lcom/android/internal/telephony/CommandException;
    invoke-virtual {v0}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v1

    sget-object v2, Lcom/android/internal/telephony/CommandException$Error;->SIM_PUK2:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v1, v2, :cond_0

    .line 663
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sPin2Locked:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, p1

    goto :goto_0
.end method

.method public static waitForInitialization(I)V
    .locals 3
    .param p0, "slotId"    # I

    .prologue
    .line 639
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sLocker:[Ljava/lang/Object;

    aget-object v2, v1, p0

    monitor-enter v2

    .line 640
    :try_start_0
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sInitialized:[Z

    aget-boolean v1, v1, p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 642
    :try_start_1
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->sLocker:[Ljava/lang/Object;

    aget-object v1, v1, p0

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    monitor-exit v2

    .line 647
    return-void

    .line 639
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 643
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/InterruptedException;
    goto :goto_0
.end method
