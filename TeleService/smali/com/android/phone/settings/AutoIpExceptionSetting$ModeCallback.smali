.class Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;
.super Ljava/lang/Object;
.source "AutoIpExceptionSetting.java"

# interfaces
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/AutoIpExceptionSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/AutoIpExceptionSetting;


# direct methods
.method private constructor <init>(Lcom/android/phone/settings/AutoIpExceptionSetting;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/AutoIpExceptionSetting;

    .prologue
    .line 287
    iput-object p1, p0, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoIpExceptionSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/settings/AutoIpExceptionSetting;Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/AutoIpExceptionSetting;
    .param p2, "-this1"    # Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;-><init>(Lcom/android/phone/settings/AutoIpExceptionSetting;)V

    return-void
.end method

.method private delete(Landroid/view/ActionMode;)V
    .locals 9
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f0b0668

    const/4 v6, 0x0

    .line 326
    iget-object v1, p0, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoIpExceptionSetting;

    invoke-static {v1}, Lcom/android/phone/settings/AutoIpExceptionSetting;->-get0(Lcom/android/phone/settings/AutoIpExceptionSetting;)Lmiui/widget/EditableListView;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/widget/EditableListView;->getCheckedItemIds()[J

    move-result-object v0

    .line 327
    .local v0, "checkedIds":[J
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 328
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoIpExceptionSetting;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 330
    const v2, 0x1010355

    .line 328
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 331
    iget-object v2, p0, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoIpExceptionSetting;

    invoke-virtual {v2}, Lcom/android/phone/settings/AutoIpExceptionSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    array-length v3, v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    array-length v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const v5, 0x7f100003

    invoke-virtual {v2, v5, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 328
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 332
    new-instance v2, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback$1;-><init>(Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;[JLandroid/view/ActionMode;)V

    .line 328
    invoke-virtual {v1, v7, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 351
    const/high16 v2, 0x1040000

    .line 328
    invoke-virtual {v1, v2, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 354
    :cond_0
    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 305
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 312
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 307
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;->delete(Landroid/view/ActionMode;)V

    goto :goto_0

    .line 305
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 5
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 291
    iget-object v1, p0, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoIpExceptionSetting;

    sget v2, Lmiui/R$attr;->actionBarDeleteIcon:I

    invoke-static {v1, v2}, Lmiui/util/AttributeResolver;->resolveDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 292
    .local v0, "delDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x2

    const v2, 0x7f0b0668

    invoke-interface {p2, v3, v1, v3, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 294
    return v4
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 0
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 317
    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 0
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "position"    # I
    .param p3, "id"    # J
    .param p5, "checked"    # Z

    .prologue
    .line 322
    invoke-virtual {p1}, Landroid/view/ActionMode;->invalidate()V

    .line 323
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 299
    const/4 v2, 0x2

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget-object v3, p0, Lcom/android/phone/settings/AutoIpExceptionSetting$ModeCallback;->this$0:Lcom/android/phone/settings/AutoIpExceptionSetting;

    invoke-static {v3}, Lcom/android/phone/settings/AutoIpExceptionSetting;->-get0(Lcom/android/phone/settings/AutoIpExceptionSetting;)Lmiui/widget/EditableListView;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/widget/EditableListView;->getCheckedItemCount()I

    move-result v3

    if-lez v3, :cond_0

    move v0, v1

    :cond_0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 300
    return v1
.end method
