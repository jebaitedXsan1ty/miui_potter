.class public Lcom/android/phone/settings/AutoRecordWhiteListSetting;
.super Lmiui/app/Activity;
.source "AutoRecordWhiteListSetting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;
    }
.end annotation


# static fields
.field private static final CALLER_ID_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAdapter:Landroid/widget/SimpleAdapter;

.field private mAllowAddedSize:I

.field private mList:Lmiui/widget/EditableListView;

.field private mMenu:Landroid/view/Menu;

.field private mModeCallBack:Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;

.field private mNumbers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSummaryView:Landroid/widget/TextView;


# direct methods
.method static synthetic -get0()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->CALLER_ID_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Lmiui/widget/EditableListView;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mList:Lmiui/widget/EditableListView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Ljava/util/HashMap;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mNumbers:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Landroid/widget/TextView;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mSummaryView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/AutoRecordWhiteListSetting;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    .param p1, "-value"    # Ljava/util/HashMap;

    .prologue
    iput-object p1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mNumbers:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic -wrap0(Landroid/content/Context;)Ljava/util/HashMap;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    invoke-static {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->loadNumbers(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Ljava/util/HashMap;Landroid/util/Pair;)V
    .locals 0
    .param p0, "numbers"    # Ljava/util/HashMap;
    .param p1, "pair"    # Landroid/util/Pair;

    .prologue
    invoke-static {p0, p1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->insertIntoNumbers(Ljava/util/HashMap;Landroid/util/Pair;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->saveNumbers()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->startAddNumbers()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->updateAdapterData()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/phone/settings/AutoRecordWhiteListSetting;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    .param p1, "currentSize"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->updateAllowAddedSize(I)V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->updateMenu()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 58
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "data1"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "display_name"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->CALLER_ID_PROJECTION:[Ljava/lang/String;

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mNumbers:Ljava/util/HashMap;

    .line 49
    return-void
.end method

.method private canSaveNumbers([Landroid/os/Parcelable;)Z
    .locals 7
    .param p1, "uris"    # [Landroid/os/Parcelable;

    .prologue
    .line 274
    const/4 v0, 0x1

    .line 275
    .local v0, "result":Z
    array-length v1, p1

    iget v2, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mAllowAddedSize:I

    if-le v1, v2, :cond_0

    .line 276
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 277
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 279
    iget v3, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mAllowAddedSize:I

    .line 277
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 279
    iget v5, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mAllowAddedSize:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 278
    const v5, 0x7f100004

    .line 277
    invoke-virtual {v2, v5, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 276
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 280
    new-instance v2, Lcom/android/phone/settings/AutoRecordWhiteListSetting$3;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$3;-><init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V

    const v3, 0x104000a

    .line 276
    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 286
    const/high16 v2, 0x1040000

    const/4 v3, 0x0

    .line 276
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 288
    const/4 v0, 0x0

    .line 290
    :cond_0
    return v0
.end method

.method public static getSummary(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 100
    invoke-static {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->loadNumbers(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    .line 101
    .local v0, "numbers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 102
    const v1, 0x7f0b0667

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 104
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const v4, 0x7f100002

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static insertIntoNumbers(Ljava/util/HashMap;Landroid/util/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 317
    .local p0, "numbers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    .local p1, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->parse(Ljava/lang/CharSequence;)Lmiui/telephony/PhoneNumberUtils$PhoneNumber;

    move-result-object v1

    .line 318
    .local v1, "pn":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getNormalizedNumber(ZZ)Ljava/lang/String;

    move-result-object v0

    .line 319
    .local v0, "normalizedNumber":Ljava/lang/String;
    invoke-virtual {v1}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->recycle()V

    .line 320
    invoke-virtual {p0, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    return-void
.end method

.method private static loadNumbers(Landroid/content/Context;)Ljava/util/HashMap;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 324
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 326
    .local v8, "numbers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 327
    .local v5, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-static {v10}, Landroid/provider/MiuiSettings$Telephony;->getRecordWhiteList(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v4

    .line 328
    .local v4, "jsonString":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 329
    return-object v8

    .line 332
    :cond_0
    :try_start_0
    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 333
    .local v9, "whiteListJsonArray":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v3, v10, :cond_1

    .line 334
    invoke-virtual {v9, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    const-string/jumbo v11, "name"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 335
    .local v6, "name":Ljava/lang/String;
    invoke-virtual {v9, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    const-string/jumbo v11, "num"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 336
    .local v7, "number":Ljava/lang/String;
    invoke-interface {v5, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 338
    .end local v3    # "i":I
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "number":Ljava/lang/String;
    .end local v9    # "whiteListJsonArray":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 339
    .local v2, "ex":Lorg/json/JSONException;
    const-string/jumbo v10, "AutoRecordWhiteListSetting"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "white list string parsed to json error: %s"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 341
    .end local v2    # "ex":Lorg/json/JSONException;
    :cond_1
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "entry$iterator":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 342
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    instance-of v10, v10, Ljava/lang/String;

    if-eqz v10, :cond_2

    .line 343
    new-instance v12, Landroid/util/Pair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-direct {v12, v10, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v8, v12}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->insertIntoNumbers(Ljava/util/HashMap;Landroid/util/Pair;)V

    goto :goto_1

    .line 346
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    :cond_3
    return-object v8
.end method

.method private processPickResult(Landroid/content/Intent;)V
    .locals 2
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 208
    const-string/jumbo v1, "com.android.contacts.extra.PHONE_URIS"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 209
    .local v0, "uris":[Landroid/os/Parcelable;
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->canSaveNumbers([Landroid/os/Parcelable;)Z

    move-result v1

    if-eqz v1, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 210
    new-instance v1, Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;

    invoke-direct {v1, p0, p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;-><init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V

    invoke-virtual {v1, v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 271
    :cond_0
    return-void
.end method

.method private saveNumbers()V
    .locals 10

    .prologue
    .line 350
    const-string/jumbo v5, ""

    .line 353
    .local v5, "result":Ljava/lang/String;
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 354
    .local v1, "jsonArray":Lorg/json/JSONArray;
    iget-object v6, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mNumbers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "pair$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 355
    .local v3, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v6, "{num:\"%s\", name:\"%s\"}"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v8, v7, v9

    iget-object v8, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    const/4 v9, 0x1

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 356
    .local v2, "jsonObj":Ljava/lang/String;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 359
    .end local v1    # "jsonArray":Lorg/json/JSONArray;
    .end local v2    # "jsonObj":Ljava/lang/String;
    .end local v3    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "pair$iterator":Ljava/util/Iterator;
    :catch_0
    move-exception v0

    .line 360
    .local v0, "ex":Lorg/json/JSONException;
    const-string/jumbo v6, "AutoRecordWhiteListSetting"

    const-string/jumbo v7, "perse json error"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 362
    .end local v0    # "ex":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v5}, Landroid/provider/MiuiSettings$Telephony;->setRecordWhiteList(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 363
    return-void

    .line 358
    .restart local v1    # "jsonArray":Lorg/json/JSONArray;
    .restart local v4    # "pair$iterator":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    goto :goto_1
.end method

.method private startAddNumbers()V
    .locals 3

    .prologue
    .line 199
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.android.contacts.action.GET_MULTIPLE_PHONES"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 200
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    const-string/jumbo v1, "vnd.android.cursor.dir/phone_v2"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 202
    const-string/jumbo v1, "android.intent.extra.include_unknown_numbers"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 203
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->startActivityForResult(Landroid/content/Intent;I)V

    .line 204
    return-void
.end method

.method private updateAdapterData()V
    .locals 10

    .prologue
    .line 294
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 296
    .local v3, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mNumbers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "pair$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/util/Pair;

    .line 297
    .local v8, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 298
    .local v7, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string/jumbo v1, "number"

    iget-object v0, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    const-string/jumbo v1, "name"

    iget-object v0, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 303
    .end local v7    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v8    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    new-instance v0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$4;

    .line 305
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const-string/jumbo v1, "name"

    const/4 v2, 0x0

    aput-object v1, v5, v2

    const-string/jumbo v1, "number"

    const/4 v2, 0x1

    aput-object v1, v5, v2

    .line 306
    const v1, 0x7f0d006a

    const v2, 0x7f0d006b

    filled-new-array {v1, v2}, [I

    move-result-object v6

    .line 304
    const v4, 0x7f04001f

    move-object v1, p0

    move-object v2, p0

    .line 303
    invoke-direct/range {v0 .. v6}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$4;-><init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mAdapter:Landroid/widget/SimpleAdapter;

    .line 312
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mList:Lmiui/widget/EditableListView;

    iget-object v1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Lmiui/widget/EditableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 313
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mNumbers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->updateAllowAddedSize(I)V

    .line 314
    return-void
.end method

.method private updateAllowAddedSize(I)V
    .locals 2
    .param p1, "currentSize"    # I

    .prologue
    const/4 v1, 0x0

    .line 194
    rsub-int v0, p1, 0x190

    .line 195
    .local v0, "size":I
    if-lez v0, :cond_0

    .end local v0    # "size":I
    :goto_0
    iput v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mAllowAddedSize:I

    .line 196
    return-void

    .restart local v0    # "size":I
    :cond_0
    move v0, v1

    .line 195
    goto :goto_0
.end method

.method private updateMenu()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 188
    iget-object v2, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mMenu:Landroid/view/Menu;

    if-eqz v2, :cond_0

    .line 189
    iget-object v2, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mMenu:Landroid/view/Menu;

    invoke-interface {v2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget v3, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mAllowAddedSize:I

    if-lez v3, :cond_1

    :goto_0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 191
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 189
    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 176
    invoke-super {p0, p1, p2, p3}, Lmiui/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 177
    packed-switch p1, :pswitch_data_0

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 179
    :pswitch_0
    if-eqz p3, :cond_0

    .line 180
    invoke-direct {p0, p3}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->processPickResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 110
    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 111
    const v0, 0x7f04001e

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->setContentView(I)V

    .line 113
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/widget/EditableListView;

    iput-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mList:Lmiui/widget/EditableListView;

    .line 114
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mList:Lmiui/widget/EditableListView;

    const v1, 0x1020004

    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/widget/EditableListView;->setEmptyView(Landroid/view/View;)V

    .line 115
    const v0, 0x7f0d0068

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mSummaryView:Landroid/widget/TextView;

    .line 116
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mList:Lmiui/widget/EditableListView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lmiui/widget/EditableListView;->setChoiceMode(I)V

    .line 117
    new-instance v0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;-><init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;)V

    iput-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mModeCallBack:Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;

    .line 118
    iget-object v0, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mList:Lmiui/widget/EditableListView;

    iget-object v1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mModeCallBack:Lcom/android/phone/settings/AutoRecordWhiteListSetting$ModeCallback;

    invoke-virtual {v0, v1}, Lmiui/widget/EditableListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 120
    new-instance v0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$1;

    invoke-direct {v0, p0, p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$1;-><init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 146
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 150
    sget v1, Lmiui/R$attr;->actionBarNewIcon:I

    invoke-static {p0, v1}, Lmiui/util/AttributeResolver;->resolveDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 151
    .local v0, "newDrawable":Landroid/graphics/drawable/Drawable;
    iput-object p1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mMenu:Landroid/view/Menu;

    .line 152
    iget-object v1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0b05ac

    invoke-interface {v1, v4, v3, v4, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 154
    invoke-direct {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->updateMenu()V

    .line 155
    return v3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 160
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 161
    .local v0, "itemId":I
    sparse-switch v0, :sswitch_data_0

    .line 170
    invoke-super {p0, p1}, Lmiui/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 164
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->finish()V

    .line 165
    return v1

    .line 167
    :sswitch_1
    invoke-direct {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->startAddNumbers()V

    .line 168
    return v1

    .line 161
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method
