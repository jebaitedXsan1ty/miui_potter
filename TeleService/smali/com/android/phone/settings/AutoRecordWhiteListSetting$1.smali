.class Lcom/android/phone/settings/AutoRecordWhiteListSetting$1;
.super Lcom/android/phone/UIDataLoadTask;
.source "AutoRecordWhiteListSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/AutoRecordWhiteListSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/phone/UIDataLoadTask",
        "<",
        "Lcom/android/phone/settings/AutoRecordWhiteListSetting;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/AutoRecordWhiteListSetting;Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    .param p2, "$anonymous0"    # Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$1;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .line 120
    invoke-direct {p0, p2}, Lcom/android/phone/UIDataLoadTask;-><init>(Ljava/lang/Object;)V

    .line 1
    return-void
.end method


# virtual methods
.method protected doPrepare()V
    .locals 3

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$1;->getReferenceObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .line 124
    .local v0, "autoRecordUI":Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get3(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0b059b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 125
    return-void
.end method

.method protected doTask([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 129
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap0(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method protected doUI(Ljava/lang/Object;)V
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 134
    invoke-virtual {p0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting$1;->getReferenceObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    .line 135
    .local v0, "autoRecordUI":Lcom/android/phone/settings/AutoRecordWhiteListSetting;
    check-cast p1, Ljava/util/HashMap;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-static {v0, p1}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-set0(Lcom/android/phone/settings/AutoRecordWhiteListSetting;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 136
    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get2(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get2(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 137
    :cond_0
    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get3(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0b059c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 138
    iget-object v1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$1;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    invoke-static {v1, v3}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap5(Lcom/android/phone/settings/AutoRecordWhiteListSetting;I)V

    .line 142
    :goto_0
    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap4(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V

    .line 143
    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap6(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)V

    .line 144
    return-void

    .line 140
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/AutoRecordWhiteListSetting$1;->this$0:Lcom/android/phone/settings/AutoRecordWhiteListSetting;

    invoke-static {v0}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-get2(Lcom/android/phone/settings/AutoRecordWhiteListSetting;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/phone/settings/AutoRecordWhiteListSetting;->-wrap5(Lcom/android/phone/settings/AutoRecordWhiteListSetting;I)V

    goto :goto_0
.end method
