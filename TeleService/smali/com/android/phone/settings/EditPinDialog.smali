.class public Lcom/android/phone/settings/EditPinDialog;
.super Ljava/lang/Object;
.source "EditPinDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/EditPinDialog$1;,
        Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;,
        Lcom/android/phone/settings/EditPinDialog$HotSwapBroadcastReceiver;,
        Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;,
        Lcom/android/phone/settings/EditPinDialog$Mode;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-phone-settings-EditPinDialog$ModeSwitchesValues:[I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mEditTextConfirmNewPin:Landroid/widget/EditText;

.field private mEditTextNewPin:Landroid/widget/EditText;

.field private mEditTextOldPinPuk:Landroid/widget/EditText;

.field private mErrorMessage:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mHotSwapReceiver:Lcom/android/phone/settings/EditPinDialog$HotSwapBroadcastReceiver;

.field private mIccCard:Lcom/android/internal/telephony/IccCard;

.field private mListener:Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;

.field private mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mProgressDialog:Lmiui/app/ProgressDialog;

.field private mTextViewPrompt:Landroid/widget/TextView;

.field slotId:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/EditPinDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/EditPinDialog;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/EditPinDialog;)Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/EditPinDialog;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mListener:Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/EditPinDialog;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/EditPinDialog;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/EditPinDialog;)Lmiui/app/ProgressDialog;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/EditPinDialog;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    return-object v0
.end method

.method private static synthetic -getcom-android-phone-settings-EditPinDialog$ModeSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/phone/settings/EditPinDialog;->-com-android-phone-settings-EditPinDialog$ModeSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/phone/settings/EditPinDialog;->-com-android-phone-settings-EditPinDialog$ModeSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/phone/settings/EditPinDialog$Mode;->values()[Lcom/android/phone/settings/EditPinDialog$Mode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$Mode;->FDN:Lcom/android/phone/settings/EditPinDialog$Mode;

    invoke-virtual {v1}, Lcom/android/phone/settings/EditPinDialog$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$Mode;->ICC_LOCK:Lcom/android/phone/settings/EditPinDialog$Mode;

    invoke-virtual {v1}, Lcom/android/phone/settings/EditPinDialog$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$Mode;->PIN1:Lcom/android/phone/settings/EditPinDialog$Mode;

    invoke-virtual {v1}, Lcom/android/phone/settings/EditPinDialog$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$Mode;->PIN2:Lcom/android/phone/settings/EditPinDialog$Mode;

    invoke-virtual {v1}, Lcom/android/phone/settings/EditPinDialog$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$Mode;->PUK1:Lcom/android/phone/settings/EditPinDialog$Mode;

    invoke-virtual {v1}, Lcom/android/phone/settings/EditPinDialog$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$Mode;->PUK2:Lcom/android/phone/settings/EditPinDialog$Mode;

    invoke-virtual {v1}, Lcom/android/phone/settings/EditPinDialog$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/phone/settings/EditPinDialog$Mode;->Unknow:Lcom/android/phone/settings/EditPinDialog$Mode;

    invoke-virtual {v1}, Lcom/android/phone/settings/EditPinDialog$Mode;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    sput-object v0, Lcom/android/phone/settings/EditPinDialog;->-com-android-phone-settings-EditPinDialog$ModeSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1

    :catch_6
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/EditPinDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/EditPinDialog;
    .param p1, "-value"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/android/phone/settings/EditPinDialog;->mErrorMessage:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/EditPinDialog;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/EditPinDialog;
    .param p1, "cancel"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/EditPinDialog;->dismiss(Z)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/EditPinDialog;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/EditPinDialog;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/EditPinDialog;->process()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/EditPinDialog;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/EditPinDialog;
    .param p1, "id"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/EditPinDialog;->showToast(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/phone/settings/EditPinDialog$Mode;Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;Lcom/android/internal/telephony/Phone;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # Lcom/android/phone/settings/EditPinDialog$Mode;
    .param p3, "listener"    # Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;
    .param p4, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput v6, p0, Lcom/android/phone/settings/EditPinDialog;->slotId:I

    .line 88
    new-instance v1, Lcom/android/phone/settings/EditPinDialog$HotSwapBroadcastReceiver;

    invoke-direct {v1, p0, v5}, Lcom/android/phone/settings/EditPinDialog$HotSwapBroadcastReceiver;-><init>(Lcom/android/phone/settings/EditPinDialog;Lcom/android/phone/settings/EditPinDialog$HotSwapBroadcastReceiver;)V

    iput-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mHotSwapReceiver:Lcom/android/phone/settings/EditPinDialog$HotSwapBroadcastReceiver;

    .line 430
    new-instance v1, Lcom/android/phone/settings/EditPinDialog$1;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/EditPinDialog$1;-><init>(Lcom/android/phone/settings/EditPinDialog;)V

    iput-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mHandler:Landroid/os/Handler;

    .line 95
    iput-object p1, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    .line 96
    iput-object p2, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    .line 97
    iput-object v5, p0, Lcom/android/phone/settings/EditPinDialog;->mErrorMessage:Ljava/lang/String;

    .line 98
    iput-object p3, p0, Lcom/android/phone/settings/EditPinDialog;->mListener:Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;

    .line 100
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 101
    const v2, 0x104000a

    .line 100
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 102
    const/high16 v2, 0x1040000

    .line 100
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 103
    new-instance v2, Lcom/android/phone/settings/EditPinDialog$2;

    invoke-direct {v2, p0}, Lcom/android/phone/settings/EditPinDialog$2;-><init>(Lcom/android/phone/settings/EditPinDialog;)V

    .line 100
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mDialog:Landroid/app/AlertDialog;

    .line 111
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 113
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040032

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 114
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 116
    const v1, 0x7f0d00ab

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mTextViewPrompt:Landroid/widget/TextView;

    .line 118
    const v1, 0x7f0d00ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    .line 119
    const v1, 0x7f0d00ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    .line 120
    const v1, 0x7f0d00ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    .line 122
    iput-object p4, p0, Lcom/android/phone/settings/EditPinDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 123
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    .line 125
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 126
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 127
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 128
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setTextAlignment(I)V

    .line 129
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 130
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 131
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 132
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setTextAlignment(I)V

    .line 133
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 134
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 135
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 136
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setTextAlignment(I)V

    .line 138
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 139
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 140
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 142
    new-instance v1, Lmiui/app/ProgressDialog;

    iget-object v2, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lmiui/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    .line 143
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v1, v6}, Lmiui/app/ProgressDialog;->setCancelable(Z)V

    .line 144
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v1, v3}, Lmiui/app/ProgressDialog;->setIndeterminate(Z)V

    .line 145
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v1}, Lmiui/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d8

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 146
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v1}, Lmiui/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 148
    invoke-static {}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->updateIccCardStatus()V

    .line 149
    return-void
.end method

.method private dismiss(Z)V
    .locals 1
    .param p1, "cancel"    # Z

    .prologue
    .line 529
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v0}, Lmiui/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v0}, Lmiui/app/ProgressDialog;->dismiss()V

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mListener:Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;

    if-eqz v0, :cond_2

    .line 536
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mListener:Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;

    invoke-interface {v0, p0, p1}, Lcom/android/phone/settings/EditPinDialog$DialogClosedListener;->OnDialogClose(Lcom/android/phone/settings/EditPinDialog;Z)V

    .line 538
    :cond_2
    return-void
.end method

.method private process()V
    .locals 11

    .prologue
    const v10, 0x7f0b069e

    const/4 v9, 0x1

    const v8, 0x7f0b06b4

    const v7, 0x7f0b069c

    const/4 v6, 0x0

    .line 343
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 344
    .local v3, "pinpuk":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 345
    .local v2, "newPin":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 347
    .local v0, "confirmNewPin":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 348
    .local v1, "msg":Landroid/os/Message;
    invoke-static {}, Lcom/android/phone/settings/EditPinDialog;->-getcom-android-phone-settings-EditPinDialog$ModeSwitchesValues()[I

    move-result-object v4

    iget-object v5, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    invoke-virtual {v5}, Lcom/android/phone/settings/EditPinDialog$Mode;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 428
    :goto_0
    return-void

    .line 350
    :pswitch_0
    invoke-static {v3, v6}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_0

    .line 351
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 352
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 354
    :cond_0
    const/4 v4, 0x2

    iput v4, v1, Landroid/os/Message;->what:I

    .line 355
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    iget-object v5, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    invoke-interface {v5}, Lcom/android/internal/telephony/IccCard;->getIccLockEnabled()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    invoke-interface {v4, v5, v3, v1}, Lcom/android/internal/telephony/IccCard;->setIccLockEnabled(ZLjava/lang/String;Landroid/os/Message;)V

    goto :goto_0

    .line 359
    :pswitch_1
    invoke-static {v3, v6}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 360
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 361
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 363
    :cond_1
    const/4 v4, 0x3

    iput v4, v1, Landroid/os/Message;->what:I

    .line 364
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    iget-object v5, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    invoke-interface {v5}, Lcom/android/internal/telephony/IccCard;->getIccFdnEnabled()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    invoke-interface {v4, v5, v3, v1}, Lcom/android/internal/telephony/IccCard;->setIccFdnEnabled(ZLjava/lang/String;Landroid/os/Message;)V

    goto :goto_0

    .line 368
    :pswitch_2
    invoke-static {v3, v6}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_2

    .line 369
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 370
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 371
    :cond_2
    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 372
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 373
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 374
    :cond_3
    invoke-static {v2, v6}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_4

    .line 375
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 376
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 378
    :cond_4
    const/4 v4, 0x4

    iput v4, v1, Landroid/os/Message;->what:I

    .line 379
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    invoke-interface {v4, v3, v2, v1}, Lcom/android/internal/telephony/IccCard;->changeIccLockPassword(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 383
    :pswitch_3
    invoke-static {v3, v6}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_5

    .line 384
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 385
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 386
    :cond_5
    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 387
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f0b06b3

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 388
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 389
    :cond_6
    invoke-static {v2, v6}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_7

    .line 390
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 391
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 393
    :cond_7
    const/4 v4, 0x5

    iput v4, v1, Landroid/os/Message;->what:I

    .line 394
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    invoke-interface {v4, v3, v2, v1}, Lcom/android/internal/telephony/IccCard;->changeIccFdnPassword(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 398
    :pswitch_4
    invoke-static {v3, v9}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_8

    .line 399
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f0b069d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 400
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 401
    :cond_8
    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 402
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 403
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 404
    :cond_9
    invoke-static {v2, v6}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_a

    .line 405
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 406
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 408
    :cond_a
    const/4 v4, 0x6

    iput v4, v1, Landroid/os/Message;->what:I

    .line 409
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    invoke-interface {v4, v3, v2, v1}, Lcom/android/internal/telephony/IccCard;->supplyPuk(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 413
    :pswitch_5
    invoke-static {v3, v9}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_b

    .line 414
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f0b06b5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 415
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 416
    :cond_b
    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 417
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f0b06b3

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 418
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 419
    :cond_c
    invoke-static {v2, v6}, Lcom/android/phone/settings/EditPinDialog;->validatePin(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_d

    .line 420
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 421
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 423
    :cond_d
    const/4 v4, 0x7

    iput v4, v1, Landroid/os/Message;->what:I

    .line 424
    iget-object v4, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    invoke-interface {v4, v3, v2, v1}, Lcom/android/internal/telephony/IccCard;->supplyPuk2(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 348
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 339
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 340
    return-void
.end method

.method private updateDialog()Z
    .locals 14

    .prologue
    const/16 v12, 0x8

    const/4 v13, 0x0

    .line 161
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->PIN1:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-eq v10, v11, :cond_0

    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->ICC_LOCK:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_1

    :cond_0
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v10}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v10

    invoke-static {v10}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->isPin1Locked(I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 163
    return v13

    .line 164
    :cond_1
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->PIN2:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_6

    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v10}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v10

    invoke-static {v10}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->isPin2Locked(I)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 165
    sget-object v10, Lcom/android/phone/settings/EditPinDialog$Mode;->PUK2:Lcom/android/phone/settings/EditPinDialog$Mode;

    iput-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    .line 180
    :cond_2
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->ICC_LOCK:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-eq v10, v11, :cond_3

    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->PIN1:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_7

    .line 181
    :cond_3
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->ICC_LOCK:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_b

    .line 182
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mIccCard:Lcom/android/internal/telephony/IccCard;

    invoke-interface {v10}, Lcom/android/internal/telephony/IccCard;->getIccLockEnabled()Z

    move-result v10

    if-eqz v10, :cond_a

    const v9, 0x7f0b0458

    .line 184
    .local v9, "titleId":I
    :goto_0
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->PIN1:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-eq v10, v11, :cond_4

    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->ICC_LOCK:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_c

    .line 186
    :cond_4
    const/4 v3, 0x0

    .line 196
    .local v3, "pin1RetryCount":I
    iget-object v6, p0, Lcom/android/phone/settings/EditPinDialog;->mErrorMessage:Ljava/lang/String;

    .line 198
    .local v6, "prompt":Ljava/lang/String;
    const v2, 0x7f0b06a1

    .line 199
    .local v2, "oldPinPukHitId":I
    const v1, 0x7f0b06a0

    .line 200
    .local v1, "newPinHitId":I
    const v0, 0x7f0b0698

    .line 260
    .end local v3    # "pin1RetryCount":I
    .local v0, "confirmNewPinHitId":I
    :goto_1
    invoke-static {}, Lcom/android/phone/settings/EditPinDialog;->-getcom-android-phone-settings-EditPinDialog$ModeSwitchesValues()[I

    move-result-object v10

    iget-object v11, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    invoke-virtual {v11}, Lcom/android/phone/settings/EditPinDialog$Mode;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 276
    :pswitch_0
    const v5, 0x7f0b06bc

    .line 279
    .local v5, "progressId":I
    :goto_2
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->FDN:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-eq v10, v11, :cond_5

    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->ICC_LOCK:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_10

    .line 280
    :cond_5
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-virtual {v10, v12}, Landroid/widget/EditText;->setVisibility(I)V

    .line 281
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-virtual {v10, v12}, Landroid/widget/EditText;->setVisibility(I)V

    .line 287
    :goto_3
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v10, v9}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 288
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_11

    .line 289
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mTextViewPrompt:Landroid/widget/TextView;

    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mTextViewPrompt:Landroid/widget/TextView;

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    :goto_4
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    invoke-virtual {v10, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 295
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    const-string/jumbo v11, ""

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-virtual {v10, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 297
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    const-string/jumbo v11, ""

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 298
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-virtual {v10, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 299
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    const-string/jumbo v11, ""

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 300
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    iget-object v11, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v11, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v10, v11}, Lmiui/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 301
    const/4 v10, 0x1

    return v10

    .line 166
    .end local v0    # "confirmNewPinHitId":I
    .end local v1    # "newPinHitId":I
    .end local v2    # "oldPinPukHitId":I
    .end local v5    # "progressId":I
    .end local v6    # "prompt":Ljava/lang/String;
    .end local v9    # "titleId":I
    :cond_6
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->FDN:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_2

    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v10}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v10

    invoke-static {v10}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->isPin2Locked(I)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 167
    new-instance v10, Landroid/app/AlertDialog$Builder;

    iget-object v11, p0, Lcom/android/phone/settings/EditPinDialog;->mContext:Landroid/content/Context;

    invoke-direct {v10, v11}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v11, 0x1010355

    invoke-virtual {v10, v11}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    .line 168
    const v11, 0x7f0b0420

    .line 167
    invoke-virtual {v10, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    .line 169
    const v11, 0x7f0b046b

    .line 167
    invoke-virtual {v10, v11}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    .line 170
    const v11, 0x104000a

    const/4 v12, 0x0

    .line 167
    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 171
    return v13

    .line 180
    :cond_7
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->PUK1:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-eq v10, v11, :cond_3

    .line 219
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->FDN:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-eq v10, v11, :cond_8

    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->PIN2:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_d

    .line 220
    :cond_8
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->FDN:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_e

    const v9, 0x7f0b0420

    .line 221
    .restart local v9    # "titleId":I
    :goto_5
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->PIN2:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-eq v10, v11, :cond_9

    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->FDN:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-ne v10, v11, :cond_f

    .line 223
    :cond_9
    const/4 v4, 0x0

    .line 233
    .local v4, "pin2RetryCount":I
    iget-object v6, p0, Lcom/android/phone/settings/EditPinDialog;->mErrorMessage:Ljava/lang/String;

    .line 235
    .restart local v6    # "prompt":Ljava/lang/String;
    const v2, 0x7f0b06ad

    .line 236
    .restart local v2    # "oldPinPukHitId":I
    const v1, 0x7f0b06ae

    .line 237
    .restart local v1    # "newPinHitId":I
    const v0, 0x7f0b06af

    .line 221
    .restart local v0    # "confirmNewPinHitId":I
    goto/16 :goto_1

    .line 182
    .end local v0    # "confirmNewPinHitId":I
    .end local v1    # "newPinHitId":I
    .end local v2    # "oldPinPukHitId":I
    .end local v4    # "pin2RetryCount":I
    .end local v6    # "prompt":Ljava/lang/String;
    .end local v9    # "titleId":I
    :cond_a
    const v9, 0x7f0b0459

    .restart local v9    # "titleId":I
    goto/16 :goto_0

    .line 183
    .end local v9    # "titleId":I
    :cond_b
    const v9, 0x7f0b0450

    .restart local v9    # "titleId":I
    goto/16 :goto_0

    .line 203
    :cond_c
    const/4 v7, 0x0

    .line 213
    .local v7, "puk1RetryCount":I
    iget-object v6, p0, Lcom/android/phone/settings/EditPinDialog;->mErrorMessage:Ljava/lang/String;

    .line 215
    .restart local v6    # "prompt":Ljava/lang/String;
    const v2, 0x7f0b06a5

    .line 216
    .restart local v2    # "oldPinPukHitId":I
    const v1, 0x7f0b06a0

    .line 217
    .restart local v1    # "newPinHitId":I
    const v0, 0x7f0b0698

    .restart local v0    # "confirmNewPinHitId":I
    goto/16 :goto_1

    .line 219
    .end local v0    # "confirmNewPinHitId":I
    .end local v1    # "newPinHitId":I
    .end local v2    # "oldPinPukHitId":I
    .end local v6    # "prompt":Ljava/lang/String;
    .end local v7    # "puk1RetryCount":I
    .end local v9    # "titleId":I
    :cond_d
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    sget-object v11, Lcom/android/phone/settings/EditPinDialog$Mode;->PUK2:Lcom/android/phone/settings/EditPinDialog$Mode;

    if-eq v10, v11, :cond_8

    .line 257
    return v13

    .line 220
    :cond_e
    const v9, 0x7f0b0429

    .restart local v9    # "titleId":I
    goto :goto_5

    .line 240
    :cond_f
    const/4 v8, 0x0

    .line 250
    .local v8, "puk2RetryCount":I
    iget-object v6, p0, Lcom/android/phone/settings/EditPinDialog;->mErrorMessage:Ljava/lang/String;

    .line 252
    .restart local v6    # "prompt":Ljava/lang/String;
    const v2, 0x7f0b06b7

    .line 253
    .restart local v2    # "oldPinPukHitId":I
    const v1, 0x7f0b06ae

    .line 254
    .restart local v1    # "newPinHitId":I
    const v0, 0x7f0b06af

    .restart local v0    # "confirmNewPinHitId":I
    goto/16 :goto_1

    .line 262
    .end local v8    # "puk2RetryCount":I
    :pswitch_1
    const v5, 0x7f0b06b8

    .line 263
    .restart local v5    # "progressId":I
    goto/16 :goto_2

    .line 265
    .end local v5    # "progressId":I
    :pswitch_2
    const v5, 0x7f0b06b9

    .line 266
    .restart local v5    # "progressId":I
    goto/16 :goto_2

    .line 268
    .end local v5    # "progressId":I
    :pswitch_3
    const v5, 0x7f0b06ba

    .line 269
    .restart local v5    # "progressId":I
    goto/16 :goto_2

    .line 272
    .end local v5    # "progressId":I
    :pswitch_4
    const v5, 0x7f0b06bb

    .line 273
    .restart local v5    # "progressId":I
    goto/16 :goto_2

    .line 283
    :cond_10
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-virtual {v10, v13}, Landroid/widget/EditText;->setVisibility(I)V

    .line 284
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-virtual {v10, v13}, Landroid/widget/EditText;->setVisibility(I)V

    goto/16 :goto_3

    .line 292
    :cond_11
    iget-object v10, p0, Lcom/android/phone/settings/EditPinDialog;->mTextViewPrompt:Landroid/widget/TextView;

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 260
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method static validatePin(Ljava/lang/String;Z)Z
    .locals 3
    .param p0, "pin"    # Ljava/lang/String;
    .param p1, "isPUK"    # Z

    .prologue
    .line 328
    if-eqz p1, :cond_1

    const/16 v0, 0x8

    .line 331
    .local v0, "pinMinimum":I
    :goto_0
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v0, :cond_2

    .line 332
    :cond_0
    const/4 v1, 0x0

    return v1

    .line 328
    .end local v0    # "pinMinimum":I
    :cond_1
    const/4 v0, 0x4

    .restart local v0    # "pinMinimum":I
    goto :goto_0

    .line 331
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x8

    if-gt v1, v2, :cond_0

    .line 334
    const/4 v1, 0x1

    return v1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 555
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 556
    const/4 v0, 0x1

    .line 557
    .local v0, "enablePositiveButton":Z
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextOldPinPuk:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 558
    const/4 v0, 0x0

    .line 560
    :cond_0
    if-eqz v0, :cond_1

    .line 561
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 562
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextNewPin:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 563
    const/4 v0, 0x0

    .line 565
    :cond_1
    if-eqz v0, :cond_2

    .line 566
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 567
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mEditTextConfirmNewPin:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 568
    const/4 v0, 0x0

    .line 571
    :cond_2
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mDialog:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 573
    .end local v0    # "enablePositiveButton":Z
    :cond_3
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 547
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/phone/settings/EditPinDialog;->dismiss(Z)V

    .line 542
    return-void
.end method

.method public getMode()Lcom/android/phone/settings/EditPinDialog$Mode;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 700
    const/4 v0, -0x2

    if-ne p2, v0, :cond_1

    .line 701
    invoke-virtual {p0}, Lcom/android/phone/settings/EditPinDialog;->dismiss()V

    .line 705
    :cond_0
    :goto_0
    return-void

    .line 702
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 551
    return-void
.end method

.method public registerHotSwapReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 576
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.intent.action.SIM_HOT_SWAP"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 577
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/android/phone/settings/EditPinDialog;->mHotSwapReceiver:Lcom/android/phone/settings/EditPinDialog$HotSwapBroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 578
    return-void
.end method

.method public setMode(Lcom/android/phone/settings/EditPinDialog$Mode;)V
    .locals 1
    .param p1, "mode"    # Lcom/android/phone/settings/EditPinDialog$Mode;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/android/phone/settings/EditPinDialog;->mMode:Lcom/android/phone/settings/EditPinDialog$Mode;

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mErrorMessage:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->isInitialized(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v0}, Lmiui/app/ProgressDialog;->show()V

    .line 307
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v0

    invoke-static {v0}, Lcom/android/phone/settings/EditPinDialog$IccPinStateHelper;->waitForInitialization(I)V

    .line 308
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mProgressDialog:Lmiui/app/ProgressDialog;

    invoke-virtual {v0}, Lmiui/app/ProgressDialog;->dismiss()V

    .line 311
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/settings/EditPinDialog;->updateDialog()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 313
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/EditPinDialog;->afterTextChanged(Landroid/text/Editable;)V

    .line 317
    :goto_0
    return-void

    .line 315
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/EditPinDialog;->dismiss()V

    goto :goto_0
.end method

.method public unregisterHotSwapReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 581
    iget-object v0, p0, Lcom/android/phone/settings/EditPinDialog;->mHotSwapReceiver:Lcom/android/phone/settings/EditPinDialog$HotSwapBroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 582
    return-void
.end method
