.class Lcom/android/phone/settings/MobileNetworkSettings$2$1;
.super Ljava/lang/Object;
.source "MobileNetworkSettings.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/MobileNetworkSettings$2;->onSetDual4GResult(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/phone/settings/MobileNetworkSettings$2;

.field final synthetic val$enabled:Z


# direct methods
.method constructor <init>(Lcom/android/phone/settings/MobileNetworkSettings$2;Z)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/phone/settings/MobileNetworkSettings$2;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/MobileNetworkSettings$2$1;->this$1:Lcom/android/phone/settings/MobileNetworkSettings$2;

    iput-boolean p2, p0, Lcom/android/phone/settings/MobileNetworkSettings$2$1;->val$enabled:Z

    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 282
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings$2$1;->this$1:Lcom/android/phone/settings/MobileNetworkSettings$2;

    iget-object v0, v0, Lcom/android/phone/settings/MobileNetworkSettings$2;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-virtual {v0}, Lcom/android/phone/settings/MobileNetworkSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v0, p0, Lcom/android/phone/settings/MobileNetworkSettings$2$1;->val$enabled:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0b06ef

    .line 283
    :goto_0
    const/4 v2, 0x0

    .line 282
    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 284
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings$2$1;->this$1:Lcom/android/phone/settings/MobileNetworkSettings$2;

    iget-object v0, v0, Lcom/android/phone/settings/MobileNetworkSettings$2;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-static {v0}, Lcom/android/phone/settings/MobileNetworkSettings;->-get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings$2$1;->this$1:Lcom/android/phone/settings/MobileNetworkSettings$2;

    iget-object v0, v0, Lcom/android/phone/settings/MobileNetworkSettings$2;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    invoke-static {v0}, Lcom/android/phone/settings/MobileNetworkSettings;->-get1(Lcom/android/phone/settings/MobileNetworkSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/phone/settings/MobileNetworkSettings$2$1;->val$enabled:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 285
    :cond_0
    return-void

    .line 282
    :cond_1
    const v0, 0x7f0b06f0

    goto :goto_0
.end method
