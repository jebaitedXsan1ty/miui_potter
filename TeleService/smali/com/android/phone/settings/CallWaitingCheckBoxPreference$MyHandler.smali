.class Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;
.super Landroid/os/Handler;
.source "CallWaitingCheckBoxPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/CallWaitingCheckBoxPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;


# direct methods
.method private constructor <init>(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference;
    .param p2, "-this1"    # Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;-><init>(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)V

    return-void
.end method

.method private handleGetCallWaitingResponse(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 142
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 144
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v5, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v5}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 145
    iget v5, p1, Landroid/os/Message;->arg2:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    .line 146
    iget-object v5, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v5}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v5

    iget-object v6, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-interface {v5, v6, v4}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    .line 152
    :cond_0
    :goto_0
    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_4

    .line 153
    iget-object v3, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get0(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 154
    const-string/jumbo v3, "CallWaitingCheckBoxPreference"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "handleGetCallWaitingResponse: ar.exception="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_1
    iget-object v3, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 157
    iget-object v3, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v4

    iget-object v5, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .line 158
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v3, Lcom/android/internal/telephony/CommandException;

    .line 157
    invoke-interface {v4, v5, v3}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onException(Landroid/preference/Preference;Lcom/android/internal/telephony/CommandException;)V

    .line 179
    :cond_2
    :goto_1
    return-void

    .line 148
    :cond_3
    iget-object v5, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v5}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v5

    iget-object v6, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-interface {v5, v6, v3}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onFinished(Landroid/preference/Preference;Z)V

    goto :goto_0

    .line 160
    :cond_4
    iget-object v5, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    instance-of v5, v5, Ljava/lang/Throwable;

    if-eqz v5, :cond_5

    .line 161
    iget-object v3, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 162
    iget-object v3, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get4(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/TimeConsumingPreferenceListener;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    const/16 v5, 0x190

    invoke-interface {v3, v4, v5}, Lcom/android/phone/settings/TimeConsumingPreferenceListener;->onError(Landroid/preference/Preference;I)V

    goto :goto_1

    .line 165
    :cond_5
    iget-object v5, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v5}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get0(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 166
    const-string/jumbo v5, "CallWaitingCheckBoxPreference"

    const-string/jumbo v6, "handleGetCallWaitingResponse: CW state successfully queried."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :cond_6
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, [I

    .line 173
    .local v1, "cwArray":[I
    :try_start_0
    iget-object v5, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    const/4 v6, 0x0

    aget v6, v1, v6

    if-ne v6, v3, :cond_7

    const/4 v6, 0x1

    aget v6, v1, v6

    and-int/lit8 v6, v6, 0x1

    if-ne v6, v3, :cond_7

    :goto_2
    invoke-virtual {v5, v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->setChecked(Z)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 174
    :catch_0
    move-exception v2

    .line 175
    .local v2, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v3, "CallWaitingCheckBoxPreference"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "handleGetCallWaitingResponse: improper result: err ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 176
    invoke-virtual {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 175
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .end local v2    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_7
    move v3, v4

    .line 173
    goto :goto_2
.end method

.method private handleSetCallWaitingResponse(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x3

    .line 182
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 184
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get0(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    const-string/jumbo v1, "CallWaitingCheckBoxPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "handleSetCallWaitingResponse: ar.exception="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get0(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "CallWaitingCheckBoxPreference"

    const-string/jumbo v2, "handleSetCallWaitingResponse: re get"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get3(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 193
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    .line 192
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v4, v4, v2}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/Phone;->getCallWaiting(Landroid/os/Message;)V

    .line 194
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 117
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 118
    .local v0, "ar":Landroid/os/AsyncResult;
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 139
    :goto_0
    return-void

    .line 122
    :pswitch_0
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 123
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->handleGetCallWaitingResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 125
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get3(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v2}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get2(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    add-int/lit8 v3, v3, 0x1

    .line 126
    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    .line 125
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/Phone;->getCallWaiting(Landroid/os/Message;)V

    goto :goto_0

    .line 132
    :pswitch_1
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_2

    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    .line 133
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->handleSetCallWaitingResponse(Landroid/os/Message;)V

    goto :goto_0

    .line 135
    :cond_3
    iget-object v1, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get3(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iget-object v2, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-virtual {v2}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->isChecked()Z

    move-result v2

    iget-object v3, p0, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->this$0:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-static {v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->-get2(Lcom/android/phone/settings/CallWaitingCheckBoxPreference;)Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/internal/telephony/Phone;->setCallWaiting(ZLandroid/os/Message;)V

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
