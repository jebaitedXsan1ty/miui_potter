.class public Lcom/android/phone/settings/RespondViaSmsManager$Settings;
.super Lmiui/preference/PreferenceActivity;
.source "RespondViaSmsManager.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/RespondViaSmsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# instance fields
.field private mButtonRejectRespondViaSms:Landroid/preference/CheckBoxPreference;

.field private mResponsePrefs:[Landroid/preference/EditTextPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 79
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-static {}, Lcom/android/phone/settings/RespondViaSmsManager;->-get0()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "Settings: onCreate()..."

    invoke-static {v2}, Lcom/android/phone/settings/RespondViaSmsManager;->-wrap0(Ljava/lang/String;)V

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v2

    const-string/jumbo v4, "respond_via_sms_prefs"

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 95
    const v2, 0x7f060023

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->addPreferencesFromResource(I)V

    .line 97
    const-string/jumbo v2, "button_call_respond_via_sms"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    .line 96
    iput-object v2, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mButtonRejectRespondViaSms:Landroid/preference/CheckBoxPreference;

    .line 98
    iget-object v2, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mButtonRejectRespondViaSms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 100
    const/4 v2, 0x4

    new-array v2, v2, [Landroid/preference/EditTextPreference;

    iput-object v2, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mResponsePrefs:[Landroid/preference/EditTextPreference;

    .line 102
    iget-object v4, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mResponsePrefs:[Landroid/preference/EditTextPreference;

    const-string/jumbo v2, "canned_response_pref_1"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/EditTextPreference;

    aput-object v2, v4, v3

    .line 103
    iget-object v4, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mResponsePrefs:[Landroid/preference/EditTextPreference;

    const-string/jumbo v2, "canned_response_pref_2"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/EditTextPreference;

    aput-object v2, v4, v6

    .line 104
    iget-object v4, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mResponsePrefs:[Landroid/preference/EditTextPreference;

    const-string/jumbo v2, "canned_response_pref_3"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/EditTextPreference;

    const/4 v5, 0x2

    aput-object v2, v4, v5

    .line 105
    iget-object v4, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mResponsePrefs:[Landroid/preference/EditTextPreference;

    const-string/jumbo v2, "canned_response_pref_4"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/EditTextPreference;

    const/4 v5, 0x3

    aput-object v2, v4, v5

    .line 107
    iget-object v4, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mResponsePrefs:[Landroid/preference/EditTextPreference;

    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v1, v4, v2

    .line 108
    .local v1, "pref":Landroid/preference/EditTextPreference;
    invoke-virtual {v1}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/preference/EditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    invoke-virtual {v1, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 110
    invoke-virtual {v1, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 107
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    .end local v1    # "pref":Landroid/preference/EditTextPreference;
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 114
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_2

    .line 116
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 118
    :cond_2
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f110000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 190
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v5, 0x1

    .line 167
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 168
    .local v1, "itemId":I
    sparse-switch v1, :sswitch_data_0

    .line 184
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    return v3

    .line 170
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->finish()V

    .line 171
    return v5

    .line 174
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v5}, Landroid/provider/MiuiSettings$Telephony;->setRejectViaSmsEnable(Landroid/content/ContentResolver;Z)V

    .line 176
    const-string/jumbo v3, "respond_via_sms_prefs"

    const/4 v4, 0x0

    .line 175
    invoke-virtual {p0, v3, v4}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 177
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 178
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 179
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 180
    invoke-virtual {p0}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->finish()V

    .line 181
    return v5

    .line 168
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0d010f -> :sswitch_1
    .end sparse-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 134
    invoke-static {}, Lcom/android/phone/settings/RespondViaSmsManager;->-get0()Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onPreferenceChange: key = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/phone/settings/RespondViaSmsManager;->-wrap0(Ljava/lang/String;)V

    .line 137
    :cond_0
    iget-object v4, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mButtonRejectRespondViaSms:Landroid/preference/CheckBoxPreference;

    if-ne p1, v4, :cond_2

    move-object v0, p2

    .line 138
    check-cast v0, Ljava/lang/Boolean;

    .line 139
    .local v0, "isRejectRespondViaSmsEnable":Ljava/lang/Boolean;
    iget-object v4, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mResponsePrefs:[Landroid/preference/EditTextPreference;

    array-length v5, v4

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v1, v4, v3

    .line 140
    .local v1, "pref":Landroid/preference/EditTextPreference;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v1, v6}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 139
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 143
    .end local v1    # "pref":Landroid/preference/EditTextPreference;
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 142
    invoke-static {v3, v4}, Landroid/provider/MiuiSettings$Telephony;->setRejectViaSmsEnable(Landroid/content/ContentResolver;Z)V

    .line 162
    .end local v0    # "isRejectRespondViaSmsEnable":Ljava/lang/Boolean;
    .end local p2    # "newValue":Ljava/lang/Object;
    :goto_1
    const/4 v3, 0x1

    return v3

    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_2
    move-object v1, p1

    .line 145
    check-cast v1, Landroid/preference/EditTextPreference;

    .restart local v1    # "pref":Landroid/preference/EditTextPreference;
    move-object v2, p2

    .line 147
    check-cast v2, Ljava/lang/String;

    .line 148
    .local v2, "value":Ljava/lang/String;
    if-eqz v2, :cond_3

    const-string/jumbo v4, "\\s*"

    const-string/jumbo v5, ""

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 150
    invoke-virtual {p0}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b068b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 149
    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 152
    return v3

    .line 159
    :cond_3
    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v1, p2}, Landroid/preference/EditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 195
    const-string/jumbo v1, "canned_response_pref_1"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 196
    const-string/jumbo v1, "canned_response_pref_2"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 195
    if-nez v1, :cond_0

    .line 197
    const-string/jumbo v1, "canned_response_pref_3"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 195
    if-nez v1, :cond_0

    .line 198
    const-string/jumbo v1, "canned_response_pref_4"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 195
    if-eqz v1, :cond_1

    .line 199
    :cond_0
    check-cast p1, Landroid/preference/EditTextPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 200
    .local v0, "et":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 202
    .end local v0    # "et":Landroid/widget/EditText;
    :cond_1
    const/4 v1, 0x1

    return v1
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 122
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 124
    invoke-virtual {p0}, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2}, Landroid/provider/MiuiSettings$Telephony;->isRejectViaSmsEnable(Landroid/content/ContentResolver;)Z

    move-result v0

    .line 125
    .local v0, "isRejectRespondViaSmsEnable":Z
    iget-object v2, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mButtonRejectRespondViaSms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 126
    iget-object v3, p0, Lcom/android/phone/settings/RespondViaSmsManager$Settings;->mResponsePrefs:[Landroid/preference/EditTextPreference;

    const/4 v2, 0x0

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 127
    .local v1, "pref":Landroid/preference/EditTextPreference;
    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setEnabled(Z)V

    .line 126
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 129
    .end local v1    # "pref":Landroid/preference/EditTextPreference;
    :cond_0
    return-void
.end method
