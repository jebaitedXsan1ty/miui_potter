.class Lcom/android/phone/settings/NetworkSetting$4;
.super Landroid/content/BroadcastReceiver;
.source "NetworkSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/NetworkSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/NetworkSetting;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/NetworkSetting;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/NetworkSetting;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    .line 538
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 541
    const-string/jumbo v5, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 542
    const-string/jumbo v5, "state"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 543
    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    invoke-virtual {v5}, Lcom/android/phone/settings/NetworkSetting;->finish()V

    .line 546
    :cond_0
    const-string/jumbo v5, "slot"

    .line 547
    sget v6, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    .line 546
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 548
    .local v1, "slotId":I
    sget v5, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    if-ne v1, v5, :cond_1

    .line 549
    const-string/jumbo v5, "subscription"

    .line 550
    sget v6, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    .line 549
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 551
    .local v4, "subId":I
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v5

    invoke-virtual {v5, v4}, Lmiui/telephony/SubscriptionManager;->getSlotIdForSubscription(I)I

    move-result v1

    .line 553
    .end local v4    # "subId":I
    :cond_1
    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    invoke-static {v5}, Lcom/android/phone/settings/NetworkSetting;->-get3(Lcom/android/phone/settings/NetworkSetting;)I

    move-result v5

    if-eq v1, v5, :cond_2

    .line 554
    return-void

    .line 556
    :cond_2
    const-string/jumbo v5, "android.intent.action.SERVICE_STATE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 557
    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    invoke-static {v5}, Lcom/android/phone/settings/NetworkSetting;->-get1(Lcom/android/phone/settings/NetworkSetting;)Landroid/preference/CheckBoxPreference;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 558
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v5}, Landroid/telephony/ServiceState;->newFromBundle(Landroid/os/Bundle;)Landroid/telephony/ServiceState;

    move-result-object v2

    .line 559
    .local v2, "ss":Landroid/telephony/ServiceState;
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getState()I

    move-result v3

    .line 560
    .local v3, "state":I
    if-nez v3, :cond_3

    .line 561
    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    invoke-static {v5}, Lcom/android/phone/settings/NetworkSetting;->-get1(Lcom/android/phone/settings/NetworkSetting;)Landroid/preference/CheckBoxPreference;

    move-result-object v5

    iget-object v6, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/android/phone/settings/NetworkSetting;->getAutoSelectionSummary(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setSummaryOn(Ljava/lang/CharSequence;)V

    .line 562
    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    invoke-static {v5}, Lcom/android/phone/settings/NetworkSetting;->-get4(Lcom/android/phone/settings/NetworkSetting;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 563
    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    invoke-static {v5, v8}, Lcom/android/phone/settings/NetworkSetting;->-set2(Lcom/android/phone/settings/NetworkSetting;Z)Z

    .line 564
    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    invoke-static {v5}, Lcom/android/phone/settings/NetworkSetting;->-wrap1(Lcom/android/phone/settings/NetworkSetting;)V

    .line 574
    .end local v2    # "ss":Landroid/telephony/ServiceState;
    .end local v3    # "state":I
    :cond_3
    :goto_0
    return-void

    .line 568
    :cond_4
    const-string/jumbo v5, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 569
    const-string/jumbo v5, "ss"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 570
    .local v0, "iccState":Ljava/lang/String;
    const-string/jumbo v5, "ABSENT"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 571
    iget-object v5, p0, Lcom/android/phone/settings/NetworkSetting$4;->this$0:Lcom/android/phone/settings/NetworkSetting;

    invoke-virtual {v5}, Lcom/android/phone/settings/NetworkSetting;->finish()V

    goto :goto_0
.end method
