.class public Lcom/android/phone/settings/cloudbackup/CallAdvancedBackupHelper;
.super Ljava/lang/Object;
.source "CallAdvancedBackupHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/android/phone/settings/cloudbackup/CallAdvancedBackupHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/cloudbackup/CallAdvancedBackupHelper;->TAG:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static backupCallAdvanced(Landroid/content/Context;Lcom/xiaomi/settingsdk/backup/data/DataPackage;)V
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "dataPackage"    # Lcom/xiaomi/settingsdk/backup/data/DataPackage;

    .prologue
    .line 29
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 30
    .local v2, "resolver":Landroid/content/ContentResolver;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 32
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string/jumbo v3, "CKT9IndexMethod"

    .line 34
    const-string/jumbo v4, "t9_indexing_key"

    .line 35
    invoke-static {}, Landroid/provider/MiuiSettings$System;->getT9IndexingKeyDefault()I

    move-result v5

    .line 33
    invoke-static {v2, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    .line 32
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 36
    const-string/jumbo v3, "CKEnableAutoRedial"

    .line 37
    invoke-static {v2}, Landroid/provider/MiuiSettings$Telephony;->getEnabledAutoRedial(Landroid/content/ContentResolver;)Z

    move-result v4

    .line 36
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 38
    const-string/jumbo v3, "CKCallBackground"

    .line 39
    invoke-static {v2}, Landroid/provider/MiuiSettings$Telephony;->getCallBackgroundType(Landroid/content/ContentResolver;)I

    move-result v4

    .line 38
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 40
    const-string/jumbo v3, "CKMissedCallNotifyTime"

    .line 41
    invoke-static {v2}, Landroid/provider/MiuiSettings$Telephony;->getMissedCallNotifyTimes(Landroid/content/ContentResolver;)I

    move-result v4

    .line 40
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 42
    const-string/jumbo v3, "CKEnableConnectDisconnectVibrate"

    .line 43
    invoke-static {v2}, Landroid/provider/MiuiSettings$Telephony;->getVibrateKey(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v4

    .line 42
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 44
    const-string/jumbo v3, "CKCallWaitingTone"

    .line 45
    invoke-static {p0}, Landroid/provider/MiuiSettings$Telephony;->getCallWaitingTone(Landroid/content/Context;)I

    move-result v4

    .line 44
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 46
    const-string/jumbo v3, "CKDTMFToneType"

    .line 47
    const-string/jumbo v4, "dtmf_tone_type"

    const/4 v5, 0x0

    .line 46
    invoke-static {v2, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    const-string/jumbo v3, "CallAdvanced"

    invoke-virtual {p1, v3, v0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 52
    return-void

    .line 48
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Lorg/json/JSONException;
    sget-object v3, Lcom/android/phone/settings/cloudbackup/CallAdvancedBackupHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "Build JSON failed"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static restoreCallAdvanced(Landroid/content/Context;Lcom/xiaomi/settingsdk/backup/data/DataPackage;)V
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "dataPackage"    # Lcom/xiaomi/settingsdk/backup/data/DataPackage;

    .prologue
    .line 55
    const-string/jumbo v4, "CallAdvanced"

    invoke-virtual {p1, v4}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v4

    if-nez v4, :cond_0

    .line 56
    return-void

    .line 58
    :cond_0
    const-string/jumbo v4, "CallAdvanced"

    invoke-virtual {p1, v4}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONObject;

    .line 59
    .local v2, "data":Lorg/json/JSONObject;
    if-nez v2, :cond_1

    .line 60
    return-void

    .line 62
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 63
    .local v3, "resolver":Landroid/content/ContentResolver;
    const-string/jumbo v4, "CKT9IndexMethod"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 64
    const-string/jumbo v4, "t9_indexing_key"

    .line 65
    const-string/jumbo v5, "CKT9IndexMethod"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    .line 64
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 67
    :cond_2
    const-string/jumbo v4, "CKEnableAutoRedial"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 69
    const-string/jumbo v4, "CKEnableAutoRedial"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 68
    invoke-static {v3, v4}, Landroid/provider/MiuiSettings$Telephony;->setAutoRedialEnabled(Landroid/content/ContentResolver;Z)V

    .line 71
    :cond_3
    const-string/jumbo v4, "CKCallBackground"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 73
    const-string/jumbo v4, "CKCallBackground"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 72
    invoke-static {v3, v4}, Landroid/provider/MiuiSettings$Telephony;->setCallBackgroundType(Landroid/content/ContentResolver;I)Z

    .line 75
    :cond_4
    const-string/jumbo v4, "CKMissedCallNotifyTime"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 77
    const-string/jumbo v4, "CKMissedCallNotifyTime"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 76
    invoke-static {v3, v4}, Landroid/provider/MiuiSettings$Telephony;->setMissedCallNotifyTimes(Landroid/content/ContentResolver;I)V

    .line 79
    :cond_5
    const-string/jumbo v4, "CKEnableConnectDisconnectVibrate"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 81
    const-string/jumbo v4, "CKEnableConnectDisconnectVibrate"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 80
    invoke-static {v3, v4}, Landroid/provider/MiuiSettings$Telephony;->setVibrateKey(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 83
    :cond_6
    const-string/jumbo v4, "CKCallWaitingTone"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 85
    const-string/jumbo v4, "CKCallWaitingTone"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    .line 86
    .local v1, "callWaitingIndex":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 87
    const v5, 0x7f070077

    .line 86
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "callWaitToneSummary":[Ljava/lang/String;
    array-length v4, v0

    if-ge v1, v4, :cond_7

    .line 89
    invoke-static {p0, v1}, Landroid/provider/MiuiSettings$Telephony;->setCallWaitingTone(Landroid/content/Context;I)V

    .line 92
    .end local v0    # "callWaitToneSummary":[Ljava/lang/String;
    .end local v1    # "callWaitingIndex":I
    :cond_7
    const-string/jumbo v4, "CKDTMFToneType"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 93
    const-string/jumbo v4, "dtmf_tone_type"

    .line 94
    const-string/jumbo v5, "CKDTMFToneType"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    .line 93
    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 96
    :cond_8
    return-void
.end method
