.class public Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;
.super Ljava/lang/Object;
.source "RespondViaSmsBackupHelper.java"


# static fields
.field private static final DEFAULT_VALUES_ID:[I

.field private static final SP_KEYS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 16
    const-class v0, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;->TAG:Ljava/lang/String;

    .line 19
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    .line 20
    const-string/jumbo v1, "canned_response_pref_1"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 21
    const-string/jumbo v1, "canned_response_pref_2"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 22
    const-string/jumbo v1, "canned_response_pref_3"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 23
    const-string/jumbo v1, "canned_response_pref_4"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 19
    sput-object v0, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;->SP_KEYS:[Ljava/lang/String;

    .line 26
    const v0, 0x7f0b0687

    .line 27
    const v1, 0x7f0b0688

    .line 28
    const v2, 0x7f0b0689

    .line 29
    const v3, 0x7f0b068a

    .line 25
    filled-new-array {v0, v1, v2, v3}, [I

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;->DEFAULT_VALUES_ID:[I

    .line 15
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static backupRespondViaSms(Landroid/content/Context;Lcom/xiaomi/settingsdk/backup/data/DataPackage;)V
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "dataPackage"    # Lcom/xiaomi/settingsdk/backup/data/DataPackage;

    .prologue
    .line 33
    invoke-static {p0}, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 35
    .local v7, "sp":Landroid/content/SharedPreferences;
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 36
    .local v4, "jsonArray":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    sget-object v8, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;->SP_KEYS:[Ljava/lang/String;

    array-length v8, v8

    if-ge v3, v8, :cond_0

    .line 37
    sget-object v8, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;->SP_KEYS:[Ljava/lang/String;

    aget-object v5, v8, v3

    .line 38
    .local v5, "key":Ljava/lang/String;
    sget-object v8, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;->DEFAULT_VALUES_ID:[I

    aget v1, v8, v3

    .line 39
    .local v1, "defaultValue":I
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 40
    .local v6, "object":Lorg/json/JSONObject;
    const-string/jumbo v8, "index"

    invoke-virtual {v6, v8, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 41
    const-string/jumbo v8, "content"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v5, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 42
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 36
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 44
    .end local v1    # "defaultValue":I
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "object":Lorg/json/JSONObject;
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 45
    .local v0, "data":Lorg/json/JSONObject;
    const-string/jumbo v8, "RespondViaSmsEnable"

    .line 46
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v9}, Landroid/provider/MiuiSettings$Telephony;->isRejectViaSmsEnable(Landroid/content/ContentResolver;)Z

    move-result v9

    .line 45
    invoke-virtual {v0, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 47
    const-string/jumbo v8, "array"

    invoke-virtual {v0, v8, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 48
    const-string/jumbo v8, "RespondViaSms"

    invoke-virtual {p1, v8, v0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    .end local v0    # "data":Lorg/json/JSONObject;
    .end local v3    # "i":I
    .end local v4    # "jsonArray":Lorg/json/JSONArray;
    :goto_1
    return-void

    .line 49
    :catch_0
    move-exception v2

    .line 50
    .local v2, "e":Lorg/json/JSONException;
    sget-object v8, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "Build JSON failed"

    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    const-string/jumbo v0, "respond_via_sms_prefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static restoreRespondViaSms(Landroid/content/Context;Lcom/xiaomi/settingsdk/backup/data/DataPackage;)V
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "dataPackage"    # Lcom/xiaomi/settingsdk/backup/data/DataPackage;

    .prologue
    .line 55
    const-string/jumbo v6, "RespondViaSms"

    invoke-virtual {p1, v6}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v6

    if-nez v6, :cond_0

    .line 56
    return-void

    .line 58
    :cond_0
    const-string/jumbo v6, "RespondViaSms"

    invoke-virtual {p1, v6}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 59
    .local v1, "data":Lorg/json/JSONObject;
    if-nez v1, :cond_1

    .line 60
    return-void

    .line 62
    :cond_1
    const-string/jumbo v6, "RespondViaSmsEnable"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 63
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 64
    const-string/jumbo v7, "RespondViaSmsEnable"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 63
    invoke-static {v6, v7}, Landroid/provider/MiuiSettings$Telephony;->setRejectViaSmsEnable(Landroid/content/ContentResolver;Z)V

    .line 66
    :cond_2
    const-string/jumbo v6, "array"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 67
    invoke-static {p0}, Lcom/android/phone/settings/cloudbackup/RespondViaSmsBackupHelper;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 68
    .local v5, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 69
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 70
    const-string/jumbo v6, "array"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 71
    .local v0, "array":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_3

    .line 72
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 73
    .local v4, "object":Lorg/json/JSONObject;
    const-string/jumbo v6, "index"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "content"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 71
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 75
    .end local v4    # "object":Lorg/json/JSONObject;
    :cond_3
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 77
    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "i":I
    .end local v5    # "sp":Landroid/content/SharedPreferences;
    :cond_4
    return-void
.end method
