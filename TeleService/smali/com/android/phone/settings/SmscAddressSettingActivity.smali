.class public Lcom/android/phone/settings/SmscAddressSettingActivity;
.super Lmiui/app/Activity;
.source "SmscAddressSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/SmscAddressSettingActivity$1;,
        Lcom/android/phone/settings/SmscAddressSettingActivity$2;
    }
.end annotation


# instance fields
.field private final WAIT_TIMEOUT:I

.field private isShowing:Z

.field private mEditTextSmscAddress:Landroid/widget/EditText;

.field private mFirstResume:Z

.field private mHandler:Landroid/os/Handler;

.field private mNeedAddressType:Z

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSmscAddress:Ljava/lang/String;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/SmscAddressSettingActivity;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/SmscAddressSettingActivity;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->isShowing:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/SmscAddressSettingActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/SmscAddressSettingActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mEditTextSmscAddress:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/SmscAddressSettingActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/SmscAddressSettingActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/SmscAddressSettingActivity;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/SmscAddressSettingActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/phone/settings/SmscAddressSettingActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/SmscAddressSettingActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mSmscAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/phone/settings/SmscAddressSettingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/SmscAddressSettingActivity;
    .param p1, "-value"    # Ljava/lang/String;

    .prologue
    iput-object p1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mSmscAddress:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/SmscAddressSettingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/SmscAddressSettingActivity;
    .param p1, "smsc"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/SmscAddressSettingActivity;->smscToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/SmscAddressSettingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/SmscAddressSettingActivity;
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/SmscAddressSettingActivity;->stringToSmsc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    .line 49
    const/16 v0, 0x1388

    iput v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->WAIT_TIMEOUT:I

    .line 54
    iput-boolean v1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->isShowing:Z

    .line 55
    iput-boolean v1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mNeedAddressType:Z

    .line 65
    new-instance v0, Lcom/android/phone/settings/SmscAddressSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/SmscAddressSettingActivity$1;-><init>(Lcom/android/phone/settings/SmscAddressSettingActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 80
    new-instance v0, Lcom/android/phone/settings/SmscAddressSettingActivity$2;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/SmscAddressSettingActivity$2;-><init>(Lcom/android/phone/settings/SmscAddressSettingActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mHandler:Landroid/os/Handler;

    .line 36
    return-void
.end method

.method private getSmscChars(Ljava/lang/String;I)[C
    .locals 3
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "byteIndex"    # I

    .prologue
    .line 228
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v2, p2, 0x1

    mul-int/lit8 v2, v2, 0x2

    if-lt v1, v2, :cond_0

    .line 229
    const/4 v1, 0x2

    new-array v0, v1, [C

    .line 230
    .local v0, "chars":[C
    mul-int/lit8 v1, p2, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/4 v2, 0x0

    aput-char v1, v0, v2

    .line 231
    mul-int/lit8 v1, p2, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/4 v2, 0x1

    aput-char v1, v0, v2

    .line 232
    return-object v0

    .line 235
    .end local v0    # "chars":[C
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method private getSmscInt(Ljava/lang/String;I)I
    .locals 2
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "byteIndex"    # I

    .prologue
    .line 220
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v1, p2, 0x1

    mul-int/lit8 v1, v1, 0x2

    if-lt v0, v1, :cond_0

    .line 221
    mul-int/lit8 v0, p2, 0x2

    add-int/lit8 v1, p2, 0x1

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 224
    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method private smscToString(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "smsc"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 184
    const/16 v9, 0x2c

    invoke-virtual {p1, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 185
    .local v5, "indexOfComma":I
    if-lez v5, :cond_0

    .line 186
    iput-boolean v11, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mNeedAddressType:Z

    .line 187
    invoke-virtual {p1, v10, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 188
    const-string/jumbo v9, "\""

    const-string/jumbo v10, ""

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 191
    :cond_0
    if-eqz p1, :cond_4

    invoke-static {p0}, Lcom/android/phone/TelephonyCapabilities;->isSmscAddressEncoded(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 193
    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 194
    .local v7, "sb":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    invoke-direct {p0, p1, v9}, Lcom/android/phone/settings/SmscAddressSettingActivity;->getSmscInt(Ljava/lang/String;I)I

    move-result v0

    .line 195
    .local v0, "byteCount":I
    if-lt v0, v11, :cond_4

    .line 196
    const/4 v9, 0x1

    invoke-direct {p0, p1, v9}, Lcom/android/phone/settings/SmscAddressSettingActivity;->getSmscInt(Ljava/lang/String;I)I

    move-result v8

    .line 197
    .local v8, "ton_npi":I
    const/16 v9, 0x5b

    if-ne v8, v9, :cond_4

    .line 198
    const/16 v9, 0x2b

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 199
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    if-ge v4, v0, :cond_3

    .line 200
    add-int/lit8 v9, v4, 0x1

    invoke-direct {p0, p1, v9}, Lcom/android/phone/settings/SmscAddressSettingActivity;->getSmscChars(Ljava/lang/String;I)[C

    move-result-object v2

    .line 201
    .local v2, "chars":[C
    if-eqz v2, :cond_2

    .line 202
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    array-length v9, v2

    if-ge v6, v9, :cond_2

    .line 203
    aget-char v1, v2, v6

    .line 204
    .local v1, "c":C
    const/16 v9, 0x66

    if-eq v1, v9, :cond_1

    const/16 v9, 0x46

    if-eq v1, v9, :cond_1

    .line 205
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 199
    .end local v1    # "c":C
    .end local v6    # "j":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 210
    .end local v2    # "chars":[C
    :cond_3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 216
    .end local v0    # "byteCount":I
    .end local v4    # "i":I
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    .end local v8    # "ton_npi":I
    :cond_4
    :goto_2
    return-object p1

    .line 213
    :catch_0
    move-exception v3

    .local v3, "e":Ljava/lang/Exception;
    goto :goto_2
.end method

.method private stringToSmsc(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x30

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 244
    if-eqz p1, :cond_2

    invoke-static {p0}, Lcom/android/phone/TelephonyCapabilities;->isSmscAddressEncoded(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 245
    invoke-static {p1}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->parse(Ljava/lang/CharSequence;)Lmiui/telephony/PhoneNumberUtils$PhoneNumber;

    move-result-object v4

    .line 246
    .local v4, "number":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getCountryCode()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_2

    .line 247
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getCountryCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lmiui/telephony/PhoneNumberUtils$PhoneNumber;->getEffectiveNumber()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 248
    .local v1, "effective":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 249
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 250
    .local v5, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    .line 251
    .local v2, "effectiveLength":I
    div-int/lit8 v0, v2, 0x2

    .line 252
    .local v0, "byteLength":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 253
    mul-int/lit8 v7, v3, 0x2

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 254
    mul-int/lit8 v7, v3, 0x2

    invoke-virtual {v1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 252
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 256
    :cond_0
    and-int/lit8 v7, v2, 0x1

    if-ne v7, v10, :cond_1

    .line 257
    const/16 v7, 0x66

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 258
    add-int/lit8 v7, v2, -0x1

    invoke-virtual {v1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 260
    :cond_1
    const-string/jumbo v7, "91"

    invoke-virtual {v5, v9, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    div-int/lit8 v6, v7, 0x2

    .line 262
    .local v6, "totalLength":I
    const/16 v7, 0xa

    if-lt v6, v7, :cond_5

    .line 263
    invoke-virtual {v5, v9, v6}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    .line 268
    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 273
    .end local v0    # "byteLength":I
    .end local v1    # "effective":Ljava/lang/String;
    .end local v2    # "effectiveLength":I
    .end local v3    # "i":I
    .end local v4    # "number":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .end local v6    # "totalLength":I
    :cond_2
    iget-boolean v7, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mNeedAddressType:Z

    if-eqz v7, :cond_4

    .line 274
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v10, :cond_6

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x2b

    if-ne v7, v8, :cond_6

    .line 275
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "145"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 280
    :cond_4
    :goto_2
    return-object p1

    .line 265
    .restart local v0    # "byteLength":I
    .restart local v1    # "effective":Ljava/lang/String;
    .restart local v2    # "effectiveLength":I
    .restart local v3    # "i":I
    .restart local v4    # "number":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local v6    # "totalLength":I
    :cond_5
    invoke-virtual {v5, v9, v6}, Ljava/lang/StringBuilder;->insert(II)Ljava/lang/StringBuilder;

    .line 266
    invoke-virtual {v5, v9, v11}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 274
    .end local v0    # "byteLength":I
    .end local v1    # "effective":Ljava/lang/String;
    .end local v2    # "effectiveLength":I
    .end local v3    # "i":I
    .end local v4    # "number":Lmiui/telephony/PhoneNumberUtils$PhoneNumber;
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .end local v6    # "totalLength":I
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x2

    if-le v7, v8, :cond_7

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-ne v7, v11, :cond_7

    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v7, v11, :cond_3

    .line 277
    :cond_7
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "129"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 285
    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 287
    invoke-virtual {p0}, Lcom/android/phone/settings/SmscAddressSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v4

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v4

    invoke-static {v3, v4}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v3

    invoke-static {v3}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    iput-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 291
    new-instance v3, Landroid/widget/EditText;

    invoke-direct {v3, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mEditTextSmscAddress:Landroid/widget/EditText;

    .line 292
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mEditTextSmscAddress:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 293
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mEditTextSmscAddress:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setTextDirection(I)V

    .line 294
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mEditTextSmscAddress:Landroid/widget/EditText;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setTextAlignment(I)V

    .line 295
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mEditTextSmscAddress:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/DialerKeyListener;->getInstance()Landroid/text/method/DialerKeyListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 296
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 297
    .local v0, "fl":Landroid/widget/FrameLayout;
    invoke-virtual {p0}, Lcom/android/phone/settings/SmscAddressSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900ca

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 298
    .local v2, "paddingDimen":I
    invoke-virtual {v0, v2, v5, v2, v5}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 299
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mEditTextSmscAddress:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 301
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0b0628

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/SmscAddressSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 303
    const v4, 0x7f0b02f6

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/SmscAddressSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/android/phone/settings/SmscAddressSettingActivity$3;

    invoke-direct {v5, p0}, Lcom/android/phone/settings/SmscAddressSettingActivity$3;-><init>(Lcom/android/phone/settings/SmscAddressSettingActivity;)V

    .line 301
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 317
    new-instance v4, Lcom/android/phone/settings/SmscAddressSettingActivity$4;

    invoke-direct {v4, p0}, Lcom/android/phone/settings/SmscAddressSettingActivity$4;-><init>(Lcom/android/phone/settings/SmscAddressSettingActivity;)V

    .line 301
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 323
    const v4, 0x7f0b02f3

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/SmscAddressSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/android/phone/settings/SmscAddressSettingActivity$5;

    invoke-direct {v5, p0}, Lcom/android/phone/settings/SmscAddressSettingActivity$5;-><init>(Lcom/android/phone/settings/SmscAddressSettingActivity;)V

    .line 301
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 329
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mFirstResume:Z

    .line 330
    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.intent.action.SIM_STATE_CHANGED"

    invoke-direct {v1, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 331
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/android/phone/settings/SmscAddressSettingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 332
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 357
    invoke-super {p0}, Lmiui/app/Activity;->onDestroy()V

    .line 358
    iget-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/SmscAddressSettingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 359
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 349
    invoke-super {p0}, Lmiui/app/Activity;->onPause()V

    .line 351
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->isShowing:Z

    .line 352
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 336
    invoke-super {p0}, Lmiui/app/Activity;->onResume()V

    .line 338
    iget-boolean v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mFirstResume:Z

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->getSmscAddress(Landroid/os/Message;)V

    .line 341
    iget-object v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    const/16 v1, 0x3eb

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 342
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->mFirstResume:Z

    .line 344
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/SmscAddressSettingActivity;->isShowing:Z

    .line 345
    return-void
.end method
