.class public Lcom/android/phone/settings/AccountSelectionPreference;
.super Landroid/preference/ListPreference;
.source "AccountSelectionPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;
    }
.end annotation


# instance fields
.field private mAccounts:[Landroid/telecom/PhoneAccountHandle;

.field private final mContext:Landroid/content/Context;

.field private mEntries:[Ljava/lang/CharSequence;

.field private mEntryValues:[Ljava/lang/String;

.field private mListener:Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;

.field private mShowSelectionInSummary:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/phone/settings/AccountSelectionPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mShowSelectionInSummary:Z

    .line 60
    iput-object p1, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mContext:Landroid/content/Context;

    .line 61
    invoke-virtual {p0, p0}, Lcom/android/phone/settings/AccountSelectionPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 62
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 115
    iget-object v2, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mListener:Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;

    if-eqz v2, :cond_3

    .line 116
    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 117
    .local v1, "index":I
    iget-object v2, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    aget-object v0, v2, v1

    .line 118
    :goto_0
    iget-object v2, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mListener:Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;

    invoke-interface {v2, p0, v0}, Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;->onAccountSelected(Lcom/android/phone/settings/AccountSelectionPreference;Landroid/telecom/PhoneAccountHandle;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 119
    iget-boolean v2, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mShowSelectionInSummary:Z

    if-eqz v2, :cond_0

    .line 120
    iget-object v2, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntries:[Ljava/lang/CharSequence;

    aget-object v2, v2, v1

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/AccountSelectionPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/AccountSelectionPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/AccountSelectionPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 123
    invoke-virtual {p0, v1}, Lcom/android/phone/settings/AccountSelectionPreference;->setValueIndex(I)V

    .line 124
    iget-object v2, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mListener:Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;

    invoke-interface {v2, p0}, Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;->onAccountChanged(Lcom/android/phone/settings/AccountSelectionPreference;)V

    .line 126
    :cond_1
    const/4 v2, 0x1

    return v2

    .line 117
    :cond_2
    const/4 v0, 0x0

    .local v0, "account":Landroid/telecom/PhoneAccountHandle;
    goto :goto_0

    .line 129
    .end local v0    # "account":Landroid/telecom/PhoneAccountHandle;
    .end local v1    # "index":I
    :cond_3
    const/4 v2, 0x0

    return v2
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 1
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mListener:Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;

    invoke-interface {v0, p0}, Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;->onAccountSelectionDialogShow(Lcom/android/phone/settings/AccountSelectionPreference;)V

    .line 144
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    .line 145
    return-void
.end method

.method public setListener(Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mListener:Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;

    .line 66
    return-void
.end method

.method public setModel(Landroid/telecom/TelecomManager;Ljava/util/List;Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)V
    .locals 9
    .param p1, "telecomManager"    # Landroid/telecom/TelecomManager;
    .param p3, "currentSelection"    # Landroid/telecom/PhoneAccountHandle;
    .param p4, "nullSelectionString"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/telecom/TelecomManager;",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;",
            "Landroid/telecom/PhoneAccountHandle;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    .local p2, "accountsList":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Landroid/telecom/PhoneAccountHandle;

    invoke-interface {p2, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/telecom/PhoneAccountHandle;

    iput-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    .line 79
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    array-length v6, v6

    add-int/lit8 v6, v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    iput-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntryValues:[Ljava/lang/String;

    .line 80
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    array-length v6, v6

    add-int/lit8 v6, v6, 0x1

    new-array v6, v6, [Ljava/lang/CharSequence;

    iput-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntries:[Ljava/lang/CharSequence;

    .line 82
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 84
    .local v4, "pm":Landroid/content/pm/PackageManager;
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    array-length v5, v6

    .line 85
    .local v5, "selectedIndex":I
    const/4 v1, 0x0

    .line 86
    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    array-length v6, v6

    if-ge v1, v6, :cond_3

    .line 87
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 88
    .local v0, "account":Landroid/telecom/PhoneAccount;
    invoke-virtual {v0}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v3

    .line 89
    .local v3, "label":Ljava/lang/CharSequence;
    if-eqz v3, :cond_0

    .line 90
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Landroid/telecom/PhoneAccountHandle;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v6

    invoke-virtual {v4, v3, v6}, Landroid/content/pm/PackageManager;->getUserBadgedLabel(Ljava/lang/CharSequence;Landroid/os/UserHandle;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 93
    :cond_0
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v2

    .line 94
    .local v2, "isSimAccount":Z
    iget-object v7, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntries:[Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz v2, :cond_2

    .line 95
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mContext:Landroid/content/Context;

    const v8, 0x7f0b0316

    invoke-virtual {v6, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 94
    :goto_1
    aput-object v6, v7, v1

    .line 97
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntryValues:[Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 98
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mAccounts:[Landroid/telecom/PhoneAccountHandle;

    aget-object v6, v6, v1

    invoke-static {p3, v6}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 99
    move v5, v1

    .line 86
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :cond_2
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 102
    .end local v0    # "account":Landroid/telecom/PhoneAccount;
    .end local v2    # "isSimAccount":Z
    .end local v3    # "label":Ljava/lang/CharSequence;
    :cond_3
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntryValues:[Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 103
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntries:[Ljava/lang/CharSequence;

    aput-object p4, v6, v1

    .line 105
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntryValues:[Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/AccountSelectionPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 106
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntries:[Ljava/lang/CharSequence;

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/AccountSelectionPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 107
    invoke-virtual {p0, v5}, Lcom/android/phone/settings/AccountSelectionPreference;->setValueIndex(I)V

    .line 108
    iget-boolean v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mShowSelectionInSummary:Z

    if-eqz v6, :cond_4

    .line 109
    iget-object v6, p0, Lcom/android/phone/settings/AccountSelectionPreference;->mEntries:[Ljava/lang/CharSequence;

    aget-object v6, v6, v5

    invoke-virtual {p0, v6}, Lcom/android/phone/settings/AccountSelectionPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 111
    :cond_4
    return-void
.end method
