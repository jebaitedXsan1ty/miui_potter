.class Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;
.super Landroid/os/Handler;
.source "CallBarringChangePasswordDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/settings/CallBarringChangePasswordDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    .line 166
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 171
    .local v0, "ar":Landroid/os/AsyncResult;
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 210
    .end local v0    # "ar":Landroid/os/AsyncResult;
    :goto_0
    return-void

    .line 173
    .restart local v0    # "ar":Landroid/os/AsyncResult;
    :pswitch_0
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-wrap1(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)V

    goto :goto_0

    .line 176
    :pswitch_1
    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-set0(Lcom/android/phone/settings/CallBarringChangePasswordDialog;Ljava/lang/String;)Ljava/lang/String;

    .line 177
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-virtual {v3}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->show()V

    .line 178
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-get3(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)Lmiui/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 181
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "ar":Landroid/os/AsyncResult;
    check-cast v0, Landroid/os/AsyncResult;

    .line 182
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-wrap0(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)V

    .line 183
    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v3, :cond_3

    .line 185
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-get0(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 186
    invoke-static {}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-get1()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "handlePasswordChanged: ar.exception="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_0
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    check-cast v1, Lcom/android/internal/telephony/CommandException;

    .line 189
    .local v1, "ce":Lcom/android/internal/telephony/CommandException;
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/CommandException$Error;->PASSWORD_INCORRECT:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v3, v4, :cond_1

    .line 190
    const/16 v2, 0x64

    .line 196
    .local v2, "errorid":I
    :goto_1
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-static {v3, v2}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-wrap2(Lcom/android/phone/settings/CallBarringChangePasswordDialog;I)V

    goto :goto_0

    .line 191
    .end local v2    # "errorid":I
    :cond_1
    invoke-virtual {v1}, Lcom/android/internal/telephony/CommandException;->getCommandError()Lcom/android/internal/telephony/CommandException$Error;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/CommandException$Error;->FDN_CHECK_FAILURE:Lcom/android/internal/telephony/CommandException$Error;

    if-ne v3, v4, :cond_2

    .line 192
    const/16 v2, 0xc8

    .restart local v2    # "errorid":I
    goto :goto_1

    .line 194
    .end local v2    # "errorid":I
    :cond_2
    const/16 v2, 0x12c

    .restart local v2    # "errorid":I
    goto :goto_1

    .line 198
    .end local v1    # "ce":Lcom/android/internal/telephony/CommandException;
    .end local v2    # "errorid":I
    :cond_3
    iget-object v3, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    instance-of v3, v3, Ljava/lang/Throwable;

    if-eqz v3, :cond_4

    .line 199
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    const/16 v4, 0x190

    invoke-static {v3, v4}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-wrap2(Lcom/android/phone/settings/CallBarringChangePasswordDialog;I)V

    goto :goto_0

    .line 201
    :cond_4
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-get0(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 202
    invoke-static {}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-get1()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "handlePasswordChanged is called without exception"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    :cond_5
    iget-object v3, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-static {v3}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-get2(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/settings/CallBarringChangePasswordDialog$1;->this$0:Lcom/android/phone/settings/CallBarringChangePasswordDialog;

    invoke-static {v4}, Lcom/android/phone/settings/CallBarringChangePasswordDialog;->-get2(Lcom/android/phone/settings/CallBarringChangePasswordDialog;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b0612

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 205
    const/4 v5, 0x0

    .line 204
    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
