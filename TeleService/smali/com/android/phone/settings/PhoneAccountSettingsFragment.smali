.class public Lcom/android/phone/settings/PhoneAccountSettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "PhoneAccountSettingsFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/PhoneAccountSettingsFragment$1;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAccountList:Landroid/preference/PreferenceCategory;

.field private mDefaultOutgoingAccount:Lcom/android/phone/settings/AccountSelectionPreference;

.field private final mOnSubscriptionsChangeListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

.field private mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

.field private mSipReceiveCallsPreference:Landroid/preference/SwitchPreference;

.field private mSubscriptionManager:Landroid/telephony/SubscriptionManager;

.field private mTelecomManager:Landroid/telecom/TelecomManager;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mUseSipCalling:Landroid/preference/ListPreference;


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/PhoneAccountSettingsFragment;)Landroid/telephony/SubscriptionManager;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/PhoneAccountSettingsFragment;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/PhoneAccountSettingsFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PhoneAccountSettingsFragment;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/PhoneAccountSettingsFragment;ZZ)Ljava/util/List;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/PhoneAccountSettingsFragment;
    .param p1, "includeSims"    # Z
    .param p2, "includeDisabledAccounts"    # Z

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getCallingAccounts(ZZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/PhoneAccountSettingsFragment;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/PhoneAccountSettingsFragment;
    .param p1, "isEnabled"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->handleSipReceiveCallsOption(Z)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/phone/settings/PhoneAccountSettingsFragment;Ljava/util/List;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/PhoneAccountSettingsFragment;
    .param p1, "enabledAccounts"    # Ljava/util/List;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->initAccountList(Ljava/util/List;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->LOG_TAG:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 232
    new-instance v0, Lcom/android/phone/settings/PhoneAccountSettingsFragment$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment$1;-><init>(Lcom/android/phone/settings/PhoneAccountSettingsFragment;)V

    .line 231
    iput-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mOnSubscriptionsChangeListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    .line 40
    return-void
.end method

.method private static buildConfigureIntent(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountHandle"    # Landroid/telecom/PhoneAccountHandle;
    .param p2, "actionStr"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 475
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    if-nez v3, :cond_1

    .line 477
    :cond_0
    return-object v4

    .line 476
    :cond_1
    invoke-virtual {p1}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    .line 475
    if-nez v3, :cond_0

    .line 481
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 482
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 483
    const-string/jumbo v3, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 484
    const-string/jumbo v3, "android.telecom.extra.PHONE_ACCOUNT_HANDLE"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 487
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 488
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, v0, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 489
    .local v2, "resolutions":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 490
    const/4 v0, 0x0

    .line 493
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    return-object v0
.end method

.method public static buildPhoneAccountConfigureIntent(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 460
    const-string/jumbo v1, "android.telecom.action.CONFIGURE_PHONE_ACCOUNT"

    .line 459
    invoke-static {p0, p1, v1}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->buildConfigureIntent(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 462
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 465
    const-string/jumbo v1, "android.telecom.action.CONNECTION_SERVICE_CONFIGURE"

    .line 464
    invoke-static {p0, p1, v1}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->buildConfigureIntent(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 466
    if-eqz v0, :cond_0

    .line 467
    sget-object v1, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Phone account using old configuration intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    :cond_0
    return-object v0
.end method

.method private getCallingAccounts(ZZ)Ljava/util/List;
    .locals 6
    .param p1, "includeSims"    # Z
    .param p2, "includeDisabledAccounts"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 425
    invoke-direct {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getEmergencyPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 428
    .local v2, "emergencyAccountHandle":Landroid/telecom/PhoneAccountHandle;
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v5, p2}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts(Z)Ljava/util/List;

    move-result-object v1

    .line 429
    .local v1, "accountHandles":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/telecom/PhoneAccountHandle;>;"
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 430
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telecom/PhoneAccountHandle;

    .line 431
    .local v3, "handle":Landroid/telecom/PhoneAccountHandle;
    invoke-virtual {v3, v2}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 433
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 437
    :cond_1
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v5, v3}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 438
    .local v0, "account":Landroid/telecom/PhoneAccount;
    if-nez v0, :cond_2

    .line 439
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 440
    :cond_2
    if-nez p1, :cond_0

    .line 441
    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v5

    .line 440
    if-eqz v5, :cond_0

    .line 442
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 445
    .end local v0    # "account":Landroid/telecom/PhoneAccount;
    .end local v3    # "handle":Landroid/telecom/PhoneAccountHandle;
    :cond_3
    return-object v1
.end method

.method private getEmergencyPhoneAccount()Landroid/telecom/PhoneAccountHandle;
    .locals 3

    .prologue
    .line 454
    const/4 v0, 0x0

    check-cast v0, Lcom/android/internal/telephony/Phone;

    const-string/jumbo v1, ""

    const/4 v2, 0x1

    .line 453
    invoke-static {v0, v1, v2}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandleWithPrefix(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Z)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized handleSipReceiveCallsOption(Z)V
    .locals 3
    .param p1, "isEnabled"    # Z

    .prologue
    monitor-enter p0

    .line 276
    :try_start_0
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 277
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    monitor-exit p0

    .line 279
    return-void

    .line 282
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

    invoke-virtual {v2, p1}, Lcom/android/services/telephony/sip/SipPreferences;->setReceivingCallsEnabled(Z)V

    .line 284
    invoke-static {v0, p1}, Lcom/android/services/telephony/sip/SipUtil;->useSipToReceiveIncomingCalls(Landroid/content/Context;Z)V

    .line 287
    invoke-static {}, Lcom/android/services/telephony/sip/SipAccountRegistry;->getInstance()Lcom/android/services/telephony/sip/SipAccountRegistry;

    move-result-object v1

    .line 288
    .local v1, "sipAccountRegistry":Lcom/android/services/telephony/sip/SipAccountRegistry;
    invoke-virtual {v1, v0}, Lcom/android/services/telephony/sip/SipAccountRegistry;->restartSipService(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    .line 289
    return-void

    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "sipAccountRegistry":Lcom/android/services/telephony/sip/SipAccountRegistry;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private initAccountList(Ljava/util/List;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 305
    .local p1, "enabledAccounts":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/telephony/TelephonyManager;->isMultiSimEnabled()Z

    move-result v12

    .line 311
    .local v12, "isMultiSimDevice":Z
    if-nez v12, :cond_0

    .line 312
    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 311
    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getCallingAccounts(ZZ)Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 313
    return-void

    .line 317
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 318
    .local v7, "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccount;>;"
    invoke-interface/range {p1 .. p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "handle$iterator":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telecom/PhoneAccountHandle;

    .line 319
    .local v8, "handle":Landroid/telecom/PhoneAccountHandle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelecomManager:Landroid/telecom/TelecomManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v3

    .line 320
    .local v3, "account":Landroid/telecom/PhoneAccount;
    if-eqz v3, :cond_1

    .line 321
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 326
    .end local v3    # "account":Landroid/telecom/PhoneAccount;
    .end local v8    # "handle":Landroid/telecom/PhoneAccountHandle;
    :cond_2
    new-instance v17, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/phone/settings/PhoneAccountSettingsFragment$3;-><init>(Lcom/android/phone/settings/PhoneAccountSettingsFragment;)V

    move-object/from16 v0, v17

    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 368
    const/16 v14, 0x64

    .line 371
    .local v14, "order":I
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "account$iterator":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telecom/PhoneAccount;

    .line 372
    .restart local v3    # "account":Landroid/telecom/PhoneAccount;
    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v8

    .line 373
    .restart local v8    # "handle":Landroid/telecom/PhoneAccountHandle;
    const/4 v11, 0x0

    .line 376
    .local v11, "intent":Landroid/content/Intent;
    const/16 v17, 0x4

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 381
    if-eqz v12, :cond_3

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    move-object/from16 v17, v0

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/telephony/TelephonyManager;->getSubIdForPhoneAccount(Landroid/telecom/PhoneAccount;)I

    move-result v18

    .line 382
    invoke-virtual/range {v17 .. v18}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v16

    .line 385
    .local v16, "subInfo":Landroid/telephony/SubscriptionInfo;
    if-eqz v16, :cond_3

    .line 386
    new-instance v11, Landroid/content/Intent;

    .end local v11    # "intent":Landroid/content/Intent;
    const-string/jumbo v17, "android.telecom.action.SHOW_CALL_SETTINGS"

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 387
    .local v11, "intent":Landroid/content/Intent;
    const/high16 v17, 0x4000000

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 388
    move-object/from16 v0, v16

    invoke-static {v11, v0}, Lcom/android/phone/SubscriptionInfoHelper;->addExtrasToIntent(Landroid/content/Intent;Landroid/telephony/SubscriptionInfo;)V

    .line 396
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v16    # "subInfo":Landroid/telephony/SubscriptionInfo;
    :cond_3
    :goto_2
    new-instance v6, Landroid/preference/Preference;

    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 397
    .local v6, "accountPreference":Landroid/preference/Preference;
    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->getLabel()Ljava/lang/CharSequence;

    move-result-object v5

    .line 399
    .local v5, "accountLabel":Ljava/lang/CharSequence;
    const/16 v17, 0x4

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v13

    .line 400
    .local v13, "isSimAccount":Z
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_4

    if-eqz v13, :cond_4

    .line 401
    const v17, 0x7f0b0316

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 400
    .end local v5    # "accountLabel":Ljava/lang/CharSequence;
    :cond_4
    invoke-virtual {v6, v5}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 404
    invoke-virtual {v3}, Landroid/telecom/PhoneAccount;->getIcon()Landroid/graphics/drawable/Icon;

    move-result-object v10

    .line 405
    .local v10, "icon":Landroid/graphics/drawable/Icon;
    if-eqz v10, :cond_5

    .line 406
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/graphics/drawable/Icon;->loadDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 410
    :cond_5
    if-eqz v11, :cond_6

    .line 411
    invoke-virtual {v6, v11}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 414
    :cond_6
    add-int/lit8 v15, v14, 0x1

    .end local v14    # "order":I
    .local v15, "order":I
    invoke-virtual {v6, v14}, Landroid/preference/Preference;->setOrder(I)V

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mAccountList:Landroid/preference/PreferenceCategory;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    move v14, v15

    .end local v15    # "order":I
    .restart local v14    # "order":I
    goto/16 :goto_1

    .line 392
    .end local v6    # "accountPreference":Landroid/preference/Preference;
    .end local v10    # "icon":Landroid/graphics/drawable/Icon;
    .end local v13    # "isSimAccount":Z
    .local v11, "intent":Landroid/content/Intent;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v0, v8}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->buildPhoneAccountConfigureIntent(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;

    move-result-object v11

    .local v11, "intent":Landroid/content/Intent;
    goto :goto_2

    .line 417
    .end local v3    # "account":Landroid/telecom/PhoneAccount;
    .end local v8    # "handle":Landroid/telecom/PhoneAccountHandle;
    .end local v11    # "intent":Landroid/content/Intent;
    :cond_8
    return-void
.end method

.method private isPrimaryUser()Z
    .locals 3

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 501
    const-string/jumbo v2, "user"

    .line 500
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 502
    .local v0, "userManager":Landroid/os/UserManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->isPrimaryUser()Z

    move-result v1

    return v1
.end method

.method private nullToEmpty(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 449
    if-nez p1, :cond_0

    const-string/jumbo p1, ""

    .end local p1    # "str":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private shouldShowConnectionServiceList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/PhoneAccountHandle;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "allNonSimAccounts":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 420
    iget-object v2, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->isMultiSimEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private updateDefaultOutgoingAccountsModel()V
    .locals 5

    .prologue
    .line 296
    iget-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mDefaultOutgoingAccount:Lcom/android/phone/settings/AccountSelectionPreference;

    .line 297
    iget-object v1, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelecomManager:Landroid/telecom/TelecomManager;

    .line 298
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getCallingAccounts(ZZ)Ljava/util/List;

    move-result-object v2

    .line 299
    iget-object v3, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v3}, Landroid/telecom/TelecomManager;->getUserSelectedOutgoingPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    .line 300
    const v4, 0x7f0b0315

    invoke-virtual {p0, v4}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 296
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/phone/settings/AccountSelectionPreference;->setModel(Landroid/telecom/TelecomManager;Ljava/util/List;Landroid/telecom/PhoneAccountHandle;Ljava/lang/CharSequence;)V

    .line 301
    return-void
.end method


# virtual methods
.method public onAccountChanged(Lcom/android/phone/settings/AccountSelectionPreference;)V
    .locals 0
    .param p1, "pref"    # Lcom/android/phone/settings/AccountSelectionPreference;

    .prologue
    .line 273
    return-void
.end method

.method public onAccountSelected(Lcom/android/phone/settings/AccountSelectionPreference;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1
    .param p1, "pref"    # Lcom/android/phone/settings/AccountSelectionPreference;
    .param p2, "account"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mDefaultOutgoingAccount:Lcom/android/phone/settings/AccountSelectionPreference;

    if-ne p1, v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v0, p2}, Landroid/telecom/TelecomManager;->setUserSelectedOutgoingPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 255
    const/4 v0, 0x1

    return v0

    .line 257
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onAccountSelectionDialogShow(Lcom/android/phone/settings/AccountSelectionPreference;)V
    .locals 1
    .param p1, "pref"    # Lcom/android/phone/settings/AccountSelectionPreference;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mDefaultOutgoingAccount:Lcom/android/phone/settings/AccountSelectionPreference;

    if-ne p1, v0, :cond_0

    .line 268
    invoke-direct {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->updateDefaultOutgoingAccountsModel()V

    .line 270
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelecomManager:Landroid/telecom/TelecomManager;

    .line 87
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 88
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    .line 89
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 507
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 508
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    .line 509
    iget-object v1, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mOnSubscriptionsChangeListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    .line 508
    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->removeOnSubscriptionsChangedListener(Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 510
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "pref"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    .line 213
    iget-object v2, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    if-ne p1, v2, :cond_0

    .line 214
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 215
    .local v1, "option":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

    invoke-virtual {v2, v1}, Lcom/android/services/telephony/sip/SipPreferences;->setSipCallOption(Ljava/lang/String;)V

    .line 216
    iget-object v2, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    invoke-virtual {v3, v1}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 217
    iget-object v2, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 218
    return v4

    .line 219
    .end local v1    # "option":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipReceiveCallsPreference:Landroid/preference/SwitchPreference;

    if-ne p1, v2, :cond_1

    .line 220
    iget-object v2, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipReceiveCallsPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {v2}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v2

    xor-int/lit8 v0, v2, 0x1

    .line 221
    .local v0, "isEnabled":Z
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/android/phone/settings/PhoneAccountSettingsFragment$2;

    invoke-direct {v3, p0, v0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment$2;-><init>(Lcom/android/phone/settings/PhoneAccountSettingsFragment;Z)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 226
    return v4

    .line 228
    .end local v0    # "isEnabled":Z
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 93
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 95
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 99
    :cond_0
    const v5, 0x7f06002a

    invoke-virtual {p0, v5}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->addPreferencesFromResource(I)V

    .line 126
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    .line 127
    const-string/jumbo v6, "phone_accounts_accounts_list_category_key"

    .line 126
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceCategory;

    iput-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mAccountList:Landroid/preference/PreferenceCategory;

    .line 129
    invoke-direct {p0, v8, v7}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getCallingAccounts(ZZ)Ljava/util/List;

    move-result-object v1

    .line 131
    .local v1, "allNonSimAccounts":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    invoke-direct {p0, v1}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->shouldShowConnectionServiceList(Ljava/util/List;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 133
    invoke-direct {p0, v7, v8}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getCallingAccounts(ZZ)Ljava/util/List;

    move-result-object v2

    .line 135
    .local v2, "enabledAccounts":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    invoke-direct {p0, v2}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->initAccountList(Ljava/util/List;)V

    .line 138
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const-string/jumbo v6, "default_outgoing_account"

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 137
    check-cast v5, Lcom/android/phone/settings/AccountSelectionPreference;

    iput-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mDefaultOutgoingAccount:Lcom/android/phone/settings/AccountSelectionPreference;

    .line 139
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mDefaultOutgoingAccount:Lcom/android/phone/settings/AccountSelectionPreference;

    invoke-virtual {v5, p0}, Lcom/android/phone/settings/AccountSelectionPreference;->setListener(Lcom/android/phone/settings/AccountSelectionPreference$AccountSelectionListener;)V

    .line 142
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v7, :cond_4

    .line 143
    invoke-direct {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->updateDefaultOutgoingAccountsModel()V

    .line 148
    :goto_0
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const-string/jumbo v6, "phone_account_all_calling_accounts"

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 150
    .local v0, "allAccounts":Landroid/preference/Preference;
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v0, :cond_1

    .line 151
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mAccountList:Landroid/preference/PreferenceCategory;

    invoke-virtual {v5, v0}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 157
    .end local v0    # "allAccounts":Landroid/preference/Preference;
    .end local v2    # "enabledAccounts":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    :cond_1
    :goto_1
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getMultiSimConfiguration()Landroid/telephony/TelephonyManager$MultiSimVariants;

    move-result-object v5

    .line 158
    sget-object v6, Landroid/telephony/TelephonyManager$MultiSimVariants;->DSDS:Landroid/telephony/TelephonyManager$MultiSimVariants;

    .line 157
    if-eq v5, v6, :cond_2

    .line 159
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    .line 160
    const-string/jumbo v6, "button_smart_divert"

    .line 159
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 161
    .local v3, "mSmartDivertPref":Landroid/preference/Preference;
    if-eqz v3, :cond_2

    .line 162
    sget-object v5, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v6, "Remove smart divert preference: "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 167
    .end local v3    # "mSmartDivertPref":Landroid/preference/Preference;
    :cond_2
    invoke-direct {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->isPrimaryUser()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/android/services/telephony/sip/SipUtil;->isVoipSupported(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 168
    new-instance v5, Lcom/android/services/telephony/sip/SipPreferences;

    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/services/telephony/sip/SipPreferences;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

    .line 171
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const-string/jumbo v6, "use_sip_calling_options_key"

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 170
    check-cast v5, Landroid/preference/ListPreference;

    iput-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    .line 172
    iget-object v6, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/net/sip/SipManager;->isSipWifiOnly(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 173
    const v5, 0x7f070043

    .line 172
    :goto_2
    invoke-virtual {v6, v5}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 175
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    invoke-virtual {v5, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 178
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    iget-object v6, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

    invoke-virtual {v6}, Lcom/android/services/telephony/sip/SipPreferences;->getSipCallOption()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v4

    .line 179
    .local v4, "optionsValueIndex":I
    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 181
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

    .line 182
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b01f6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 181
    invoke-virtual {v5, v6}, Lcom/android/services/telephony/sip/SipPreferences;->setSipCallOption(Ljava/lang/String;)V

    .line 184
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    iget-object v6, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

    invoke-virtual {v6}, Lcom/android/services/telephony/sip/SipPreferences;->getSipCallOption()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v4

    .line 186
    :cond_3
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    invoke-virtual {v5, v4}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 187
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    iget-object v6, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mUseSipCalling:Landroid/preference/ListPreference;

    invoke-virtual {v6}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 190
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const-string/jumbo v6, "sip_receive_calls_key"

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 189
    check-cast v5, Landroid/preference/SwitchPreference;

    iput-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipReceiveCallsPreference:Landroid/preference/SwitchPreference;

    .line 191
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipReceiveCallsPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/android/services/telephony/sip/SipUtil;->isPhoneIdle(Landroid/content/Context;)Z

    move-result v6

    invoke-virtual {v5, v6}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 192
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipReceiveCallsPreference:Landroid/preference/SwitchPreference;

    .line 193
    iget-object v6, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipPreferences:Lcom/android/services/telephony/sip/SipPreferences;

    invoke-virtual {v6}, Lcom/android/services/telephony/sip/SipPreferences;->isReceivingCallsEnabled()Z

    move-result v6

    .line 192
    invoke-virtual {v5, v6}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 194
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mSipReceiveCallsPreference:Landroid/preference/SwitchPreference;

    invoke-virtual {v5, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 200
    .end local v4    # "optionsValueIndex":I
    :goto_3
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v5

    .line 201
    iget-object v6, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mOnSubscriptionsChangeListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    .line 200
    invoke-virtual {v5, v6}, Landroid/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 202
    return-void

    .line 145
    .restart local v2    # "enabledAccounts":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    :cond_4
    iget-object v5, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mAccountList:Landroid/preference/PreferenceCategory;

    iget-object v6, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mDefaultOutgoingAccount:Lcom/android/phone/settings/AccountSelectionPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0

    .line 154
    .end local v2    # "enabledAccounts":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    :cond_5
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->mAccountList:Landroid/preference/PreferenceCategory;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    .line 174
    :cond_6
    const v5, 0x7f070042

    goto/16 :goto_2

    .line 196
    :cond_7
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    .line 197
    invoke-virtual {p0}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    const-string/jumbo v7, "phone_accounts_sip_settings_category_key"

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 196
    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3
.end method
