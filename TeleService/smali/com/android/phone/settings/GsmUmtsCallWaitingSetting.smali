.class public Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;
.super Lcom/android/phone/settings/TimeConsumingPreferenceActivity;
.source "GsmUmtsCallWaitingSetting.java"


# instance fields
.field private DBG:Z

.field private mCWButton:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

.field private mFirstResume:Z

.field private mHasShowSuppServiceDialog:Z

.field private mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;-><init>()V

    .line 23
    sget v0, Lcom/android/phone/MiuiPhoneUtils;->DBG_LEVEL:I

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->DBG:Z

    .line 32
    iput-boolean v1, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mHasShowSuppServiceDialog:Z

    .line 20
    return-void

    :cond_0
    move v0, v1

    .line 23
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 36
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-static {p0}, Lcom/android/phone/settings/SimPickerPreference;->showSimPicker(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    return-void

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 42
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v3

    .line 41
    invoke-static {v2, v3}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v1

    .line 43
    .local v1, "slotId":I
    invoke-static {v1}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 45
    iget-boolean v2, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->DBG:Z

    if-eqz v2, :cond_1

    const-string/jumbo v2, "GsmUmtsCallWaitingSetting"

    const-string/jumbo v3, "Creating activity"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :cond_1
    const v2, 0x7f060013

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->addPreferencesFromResource(I)V

    .line 49
    const-string/jumbo v2, "button_cw"

    invoke-virtual {p0, v2}, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    iput-object v2, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mCWButton:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    .line 51
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->getActionBar()Lmiui/app/ActionBar;

    move-result-object v0

    .line 52
    .local v0, "bar":Landroid/app/ActionBar;
    if-eqz v0, :cond_2

    .line 53
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 55
    :cond_2
    iput-boolean v4, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mFirstResume:Z

    .line 56
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 84
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 85
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->finish()V

    .line 87
    const/4 v1, 0x1

    return v1

    .line 89
    :cond_0
    invoke-super {p0, p1}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    invoke-super {p0}, Lcom/android/phone/settings/TimeConsumingPreferenceActivity;->onResume()V

    .line 62
    iget-boolean v0, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mFirstResume:Z

    if-eqz v0, :cond_3

    .line 63
    iget-boolean v0, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mHasShowSuppServiceDialog:Z

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->finish()V

    .line 65
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/phone/MiuiPhoneUtils;->maybeShowDialogForSuppService(Lcom/android/internal/telephony/Phone;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iput-boolean v3, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mHasShowSuppServiceDialog:Z

    .line 68
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mCWButton:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-virtual {v0, v2}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->setEnabled(Z)V

    .line 72
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/telephony/IccCard;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    move-result-object v0

    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v0, v1, :cond_2

    .line 73
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/telephony/IccCard;->getIccFdnEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 72
    if-eqz v0, :cond_2

    .line 74
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mCWButton:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    invoke-virtual {v0, v3}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->setEnabled(Z)V

    .line 75
    iget-object v0, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mCWButton:Lcom/android/phone/settings/CallWaitingCheckBoxPreference;

    iget-object v1, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-virtual {v0, p0, v2, v1}, Lcom/android/phone/settings/CallWaitingCheckBoxPreference;->init(Lcom/android/phone/settings/TimeConsumingPreferenceListener;ZI)V

    .line 78
    :cond_2
    iput-boolean v2, p0, Lcom/android/phone/settings/GsmUmtsCallWaitingSetting;->mFirstResume:Z

    .line 80
    :cond_3
    return-void
.end method
