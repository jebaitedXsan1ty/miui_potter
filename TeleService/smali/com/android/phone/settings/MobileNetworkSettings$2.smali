.class Lcom/android/phone/settings/MobileNetworkSettings$2;
.super Ljava/lang/Object;
.source "MobileNetworkSettings.java"

# interfaces
.implements Lcom/android/phone/Dual4GManager$CallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/MobileNetworkSettings;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/settings/MobileNetworkSettings;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/MobileNetworkSettings;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/settings/MobileNetworkSettings;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/MobileNetworkSettings$2;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onSetDual4GResult(ZI)V
    .locals 3
    .param p1, "enabled"    # Z
    .param p2, "resultCode"    # I

    .prologue
    .line 277
    sget-object v0, Lcom/android/phone/settings/MobileNetworkSettings;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSetDual4GResult enabled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    if-eqz p2, :cond_0

    .line 279
    iget-object v0, p0, Lcom/android/phone/settings/MobileNetworkSettings$2;->this$0:Lcom/android/phone/settings/MobileNetworkSettings;

    new-instance v1, Lcom/android/phone/settings/MobileNetworkSettings$2$1;

    invoke-direct {v1, p0, p1}, Lcom/android/phone/settings/MobileNetworkSettings$2$1;-><init>(Lcom/android/phone/settings/MobileNetworkSettings$2;Z)V

    invoke-virtual {v0, v1}, Lcom/android/phone/settings/MobileNetworkSettings;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 288
    :cond_0
    return-void
.end method
