.class public Lcom/android/phone/settings/MultiSimInfoEditorActivity;
.super Lmiui/preference/PreferenceActivity;
.source "MultiSimInfoEditorActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Landroid/text/TextWatcher;
.implements Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/settings/MultiSimInfoEditorActivity$1;,
        Lcom/android/phone/settings/MultiSimInfoEditorActivity$2;,
        Lcom/android/phone/settings/MultiSimInfoEditorActivity$3;,
        Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;,
        Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;
    }
.end annotation


# instance fields
.field private mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

.field private mClickedPreference:Landroid/preference/Preference;

.field private mCommonOptionsEnabled:Z

.field private mDisableSimDialogFragment:Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;

.field private mDismissRunner:Ljava/lang/Runnable;

.field private mEnableSim:Landroid/preference/CheckBoxPreference;

.field private mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

.field private mHandler:Landroid/os/Handler;

.field private mIsDisableLteDevice:Z

.field private mIsForeground:Z

.field private mIsReceiverRegistered:Z

.field private mNotSet:Ljava/lang/String;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mPreferenceScreen:Landroid/preference/PreferenceScreen;

.field private mProgressDialog:Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

.field private mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

.field private mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

.field private mSlotId:I

.field private mSubId:I


# direct methods
.method static synthetic -get0(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimInfoEditorActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mDismissRunner:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimInfoEditorActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimInfoEditorActivity;

    .prologue
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimInfoEditorActivity;

    .prologue
    iget v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    return v0
.end method

.method static synthetic -wrap0(Lcom/android/phone/settings/MultiSimInfoEditorActivity;IZ)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimInfoEditorActivity;
    .param p1, "slotId"    # I
    .param p2, "isChecked"    # Z

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->switchRadioState(IZ)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimInfoEditorActivity;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->updateCheckState()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/settings/MultiSimInfoEditorActivity;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->updateNetworkTypeAvailable()V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCommonOptionsEnabled:Z

    .line 95
    iput-boolean v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsForeground:Z

    .line 98
    iput-boolean v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsReceiverRegistered:Z

    .line 107
    sget v0, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    iput v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSubId:I

    .line 108
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiui/telephony/TelephonyManager;->isDisableLte(Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsDisableLteDevice:Z

    .line 110
    new-instance v0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$1;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$1;-><init>(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mHandler:Landroid/os/Handler;

    .line 129
    new-instance v0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$2;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$2;-><init>(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 474
    new-instance v0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$3;

    invoke-direct {v0, p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$3;-><init>(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)V

    iput-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mDismissRunner:Ljava/lang/Runnable;

    .line 65
    return-void
.end method

.method private displayErrorDialog(I)V
    .locals 3
    .param p1, "messageId"    # I

    .prologue
    .line 385
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 386
    const v1, 0x1080027

    .line 385
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 387
    new-instance v1, Lcom/android/phone/settings/MultiSimInfoEditorActivity$4;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$4;-><init>(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)V

    const v2, 0x1040013

    .line 385
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    .line 391
    new-instance v1, Lcom/android/phone/settings/MultiSimInfoEditorActivity$5;

    invoke-direct {v1, p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$5;-><init>(Lcom/android/phone/settings/MultiSimInfoEditorActivity;)V

    .line 385
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 396
    return-void
.end method

.method private isIdentifyAllCardOperator()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 746
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v3, Lcom/android/phone/MiuiPhoneUtils;->PHONE_COUNT:I

    if-ge v0, v3, :cond_4

    .line 747
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/internal/telephony/uicc/UiccController;->getUiccCard(I)Lcom/android/internal/telephony/uicc/UiccCard;

    move-result-object v2

    .line 748
    .local v2, "uc":Lcom/android/internal/telephony/uicc/UiccCard;
    if-nez v2, :cond_0

    .line 749
    return v5

    .line 751
    :cond_0
    invoke-virtual {v2}, Lcom/android/internal/telephony/uicc/UiccCard;->getCardState()Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;->CARDSTATE_PRESENT:Lcom/android/internal/telephony/uicc/IccCardStatus$CardState;

    if-eq v3, v4, :cond_2

    .line 746
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 755
    :cond_2
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v1

    .line 756
    .local v1, "simInfo":Lmiui/telephony/SubscriptionInfo;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lmiui/telephony/SubscriptionInfo;->getMcc()I

    move-result v3

    if-nez v3, :cond_1

    .line 757
    :cond_3
    return v5

    .line 760
    .end local v1    # "simInfo":Lmiui/telephony/SubscriptionInfo;
    .end local v2    # "uc":Lcom/android/internal/telephony/uicc/UiccCard;
    :cond_4
    const/4 v3, 0x1

    return v3
.end method

.method private isPhoneInCall()Z
    .locals 2

    .prologue
    .line 529
    sget-object v0, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getCallManager()Lcom/android/internal/telephony/CallManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setEnabledStateOnPreferences(Landroid/preference/Preference;Z)V
    .locals 4
    .param p1, "prefer"    # Landroid/preference/Preference;
    .param p2, "enabled"    # Z

    .prologue
    .line 341
    instance-of v2, p1, Landroid/preference/PreferenceGroup;

    if-eqz v2, :cond_1

    move-object v1, p1

    .line 342
    check-cast v1, Landroid/preference/PreferenceGroup;

    .line 343
    .local v1, "preferGroup":Landroid/preference/PreferenceGroup;
    const-string/jumbo v2, "sim_info_editor"

    invoke-virtual {v1}, Landroid/preference/PreferenceGroup;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 344
    invoke-virtual {v1, p2}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    .line 346
    :cond_0
    invoke-virtual {v1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_2

    .line 347
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->setEnabledStateOnPreferences(Landroid/preference/Preference;Z)V

    .line 346
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 350
    .end local v0    # "i":I
    .end local v1    # "preferGroup":Landroid/preference/PreferenceGroup;
    :cond_1
    const-string/jumbo v2, "enable_sim"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 351
    invoke-virtual {p1, p2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 354
    :cond_2
    return-void
.end method

.method private showDisableSimDialog()V
    .locals 5

    .prologue
    .line 634
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    if-nez v1, :cond_0

    return-void

    .line 636
    :cond_0
    new-instance v1, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;

    iget v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    .line 637
    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mHandler:Landroid/os/Handler;

    .line 636
    invoke-direct {v1, v2, v3, v4}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;-><init>(ILjava/lang/CharSequence;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mDisableSimDialogFragment:Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;

    .line 638
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mDisableSimDialogFragment:Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->setCancelable(Z)V

    .line 640
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mDisableSimDialogFragment:Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;

    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "DisableSimDialog"

    invoke-virtual {v1, v2, v3}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 645
    :goto_0
    return-void

    .line 641
    :catch_0
    move-exception v0

    .line 642
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string/jumbo v1, "MultiSimInfoEditorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "error: show dialogfragment"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private switchRadioState(IZ)V
    .locals 8
    .param p1, "slotId"    # I
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v7, 0x0

    .line 440
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->isPhoneInCall()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    const v0, 0x7f0b0654

    invoke-direct {p0, v0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->displayErrorDialog(I)V

    .line 442
    return-void

    .line 445
    :cond_0
    invoke-static {p1}, Lmiui/telephony/SubscriptionManager;->isValidSlotId(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 446
    const v0, 0x7f0b0655

    invoke-direct {p0, v0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->displayErrorDialog(I)V

    .line 447
    return-void

    .line 450
    :cond_1
    iget-boolean v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsForeground:Z

    if-eqz v0, :cond_2

    .line 452
    if-eqz p2, :cond_3

    .line 453
    const v6, 0x7f0b0656

    .line 457
    .local v6, "msgId":I
    :goto_0
    invoke-virtual {p0, v6}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->showProgressDialog(I)V

    .line 458
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 459
    .local v4, "startTime":J
    new-instance v0, Lcom/android/phone/settings/MultiSimInfoEditorActivity$6;

    move-object v1, p0

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$6;-><init>(Lcom/android/phone/settings/MultiSimInfoEditorActivity;IZJ)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Void;

    .line 470
    const/4 v2, 0x0

    aput-object v7, v1, v2

    const/4 v2, 0x1

    aput-object v7, v1, v2

    .line 459
    invoke-virtual {v0, v1}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$6;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 472
    .end local v4    # "startTime":J
    .end local v6    # "msgId":I
    :cond_2
    return-void

    .line 455
    :cond_3
    const v6, 0x7f0b0657

    .restart local v6    # "msgId":I
    goto :goto_0
.end method

.method private updateCheckState()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 315
    iget v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-static {v3}, Lcom/android/phone/MiuiPhoneUtils;->isIccCardActivated(I)Z

    move-result v0

    .line 316
    .local v0, "enable":Z
    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mDisableSimDialogFragment:Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mDisableSimDialogFragment:Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;

    invoke-virtual {v3}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$DisableSimDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    if-nez v3, :cond_1

    .line 317
    :cond_0
    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_1

    .line 318
    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 321
    :cond_1
    sget-boolean v3, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-nez v3, :cond_2

    .line 322
    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v3, v0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->setEnabledStateOnPreferences(Landroid/preference/Preference;Z)V

    .line 326
    :cond_2
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 327
    const-string/jumbo v4, "airplane_mode_on"

    .line 326
    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_5

    const/4 v1, 0x1

    .line 328
    .local v1, "enableOptions":Z
    :goto_0
    if-eqz v0, :cond_6

    .end local v1    # "enableOptions":Z
    :goto_1
    iput-boolean v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCommonOptionsEnabled:Z

    .line 329
    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    if-eqz v2, :cond_3

    .line 330
    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    iget-boolean v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCommonOptionsEnabled:Z

    invoke-virtual {v2, v3}, Lcom/android/phone/settings/CdmaOptions;->setOptionEnabled(Z)V

    .line 332
    :cond_3
    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    if-eqz v2, :cond_4

    .line 333
    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    iget-boolean v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCommonOptionsEnabled:Z

    invoke-virtual {v2, v3}, Lcom/android/phone/settings/GsmUmtsOptions;->setOptionEnabled(Z)V

    .line 335
    :cond_4
    return-void

    .line 326
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "enableOptions":Z
    goto :goto_0

    :cond_6
    move v1, v2

    .line 328
    goto :goto_1
.end method

.method private updateInfo()V
    .locals 3

    .prologue
    .line 287
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-virtual {v1, v2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v0

    .line 288
    .local v0, "simInfo":Lmiui/telephony/SubscriptionInfo;
    iput-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    .line 289
    if-eqz v0, :cond_4

    .line 291
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    if-nez v1, :cond_1

    .line 293
    :cond_0
    return-void

    .line 295
    :cond_1
    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_2

    .line 296
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mNotSet:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CustomEditTextPreference;->setRightValue(Ljava/lang/String;)V

    .line 302
    :goto_0
    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_3

    .line 303
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CustomEditTextPreference;->setRightValue(Ljava/lang/String;)V

    .line 304
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 312
    :goto_1
    return-void

    .line 298
    :cond_2
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CustomEditTextPreference;->setRightValue(Ljava/lang/String;)V

    .line 299
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 306
    :cond_3
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mNotSet:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CustomEditTextPreference;->setRightValue(Ljava/lang/String;)V

    .line 307
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    goto :goto_1

    .line 310
    :cond_4
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->finish()V

    goto :goto_1
.end method

.method private updateNetworkTypeAvailable()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 735
    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v4, "button_preferred_network_type_key"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 736
    .local v1, "pref":Landroid/preference/Preference;
    if-eqz v1, :cond_2

    .line 737
    iget v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-static {v3}, Lcom/android/phone/NetworkModeManager;->getPhoneType(I)I

    move-result v3

    if-eqz v3, :cond_3

    const/4 v0, 0x1

    .line 738
    .local v0, "newEnabled":Z
    :goto_0
    iget-boolean v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsDisableLteDevice:Z

    if-eqz v3, :cond_0

    .line 739
    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->isIdentifyAllCardOperator()Z

    move-result v0

    .line 741
    .end local v0    # "newEnabled":Z
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    iget-boolean v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCommonOptionsEnabled:Z

    :cond_1
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 743
    :cond_2
    return-void

    .line 737
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "newEnabled":Z
    goto :goto_0

    .line 739
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 417
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 401
    return-void
.end method

.method public dismissProgressDialog()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 493
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mProgressDialog:Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mProgressDialog:Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mProgressDialog:Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;->dismissAllowingStateLoss()V

    .line 496
    :cond_0
    iput-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mProgressDialog:Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;

    .line 497
    return-void
.end method

.method public initMobileNetworkSetting()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 167
    iget v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-static {v8}, Lcom/android/phone/MiuiPhoneUtils;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 168
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0653

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mNotSet:Ljava/lang/String;

    .line 170
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    .line 171
    const-string/jumbo v8, "sim_name_number_category_key"

    invoke-virtual {p0, v8}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    .line 172
    .local v6, "simNameNumber":Landroid/preference/Preference;
    iget v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-static {v8}, Lcom/android/phone/MiuiPhoneUtils;->isVirtualSim(I)Z

    move-result v1

    .line 173
    .local v1, "isVirtualSim":Z
    if-eqz v6, :cond_0

    .line 174
    if-eqz v1, :cond_6

    .line 175
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v8, v6}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 176
    iput-object v11, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    .line 177
    iput-object v11, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    .line 189
    :cond_0
    :goto_0
    const-string/jumbo v8, "enable_sim"

    invoke-virtual {p0, v8}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Landroid/preference/CheckBoxPreference;

    iput-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    .line 190
    if-nez v1, :cond_1

    iget v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-static {v8}, Lcom/android/phone/MiuiPhoneUtils;->getIccCardCountExcludeSlot(I)I

    move-result v8

    if-ge v8, v10, :cond_7

    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    invoke-virtual {v8}, Lmiui/telephony/SubscriptionInfo;->isActivated()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 191
    :cond_1
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v9, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 192
    iput-object v11, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    .line 198
    :goto_1
    const-string/jumbo v8, "button_go_to_virtual_sim_key"

    invoke-virtual {p0, v8}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    .line 199
    .local v7, "virtualSimButton":Landroid/preference/Preference;
    if-eqz v1, :cond_9

    .line 200
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getVirtualSimCarrierName()Ljava/lang/String;

    move-result-object v2

    .line 201
    .local v2, "newTitle":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 202
    invoke-virtual {v7, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 210
    .end local v2    # "newTitle":Ljava/lang/String;
    :goto_2
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v3

    .line 211
    .local v3, "phoneType":I
    if-ne v3, v12, :cond_a

    .line 212
    new-instance v8, Lcom/android/phone/settings/CdmaOptions;

    iget-object v9, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v10, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-direct {v8, p0, v9, v10}, Lcom/android/phone/settings/CdmaOptions;-><init>(Landroid/preference/PreferenceActivity;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V

    iput-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    .line 220
    :goto_3
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v8

    iget v9, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    if-eq v8, v9, :cond_3

    .line 221
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v9, "button_apn_key"

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 222
    .local v5, "pref":Landroid/preference/Preference;
    if-eqz v5, :cond_2

    .line 223
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v8, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 225
    :cond_2
    invoke-static {}, Lcom/android/phone/NetworkModeManager;->shouldShowViceNetworkTypePref()Z

    move-result v8

    if-nez v8, :cond_3

    .line 226
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v9, "button_preferred_network_type_key"

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 227
    if-eqz v5, :cond_3

    .line 228
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v8, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 232
    .end local v5    # "pref":Landroid/preference/Preference;
    :cond_3
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    move-result-object v8

    iget-object v9, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v9, v12, v11}, Lcom/android/internal/telephony/uicc/UiccController;->registerForIccChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 233
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->updateNetworkTypeAvailable()V

    .line 236
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 237
    const v9, 0x7f0e001b

    .line 236
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 238
    .local v0, "isCarrierSettingsEnabled":Z
    if-nez v0, :cond_4

    .line 239
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    .line 240
    const-string/jumbo v9, "carrier_settings_key"

    .line 239
    invoke-virtual {v8, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 241
    .restart local v5    # "pref":Landroid/preference/Preference;
    if-eqz v5, :cond_4

    .line 242
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v8, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 246
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    .line 247
    const-string/jumbo v9, "carrier_settings_key"

    .line 246
    invoke-virtual {v8, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    .line 248
    if-eqz v5, :cond_4

    .line 249
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v8, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 253
    .end local v5    # "pref":Landroid/preference/Preference;
    :cond_4
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v9, "button_uplmn_key"

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 254
    .local v4, "plmn":Landroid/preference/Preference;
    invoke-virtual {v4}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v8

    iget v9, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-static {v8, v9}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    .line 255
    invoke-static {}, Lcom/android/phone/TelephonyCapabilities;->supportShowPLMNPreference()Z

    move-result v8

    if-nez v8, :cond_5

    .line 256
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v8, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 258
    :cond_5
    return-void

    .line 179
    .end local v0    # "isCarrierSettingsEnabled":Z
    .end local v3    # "phoneType":I
    .end local v4    # "plmn":Landroid/preference/Preference;
    .end local v7    # "virtualSimButton":Landroid/preference/Preference;
    :cond_6
    const-string/jumbo v8, "sim_name"

    invoke-virtual {p0, v8}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Lcom/android/phone/settings/CustomEditTextPreference;

    iput-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    .line 180
    const-string/jumbo v8, "sim_number"

    invoke-virtual {p0, v8}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    check-cast v8, Lcom/android/phone/settings/LTREditTextPreference;

    iput-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    .line 181
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v8, p0}, Lcom/android/phone/settings/CustomEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 182
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CustomEditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 183
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v8, p0}, Lcom/android/phone/settings/CustomEditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 184
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v8, p0}, Lcom/android/phone/settings/CustomEditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 185
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v8}, Lcom/android/phone/settings/CustomEditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 186
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v8, p0}, Lcom/android/phone/settings/CustomEditTextPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_0

    .line 194
    :cond_7
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v8, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_1

    .line 204
    .restart local v2    # "newTitle":Ljava/lang/String;
    .restart local v7    # "virtualSimButton":Landroid/preference/Preference;
    :cond_8
    const-string/jumbo v8, "MultiSimInfoEditorActivity"

    const-string/jumbo v9, "error: update Virtual Sim Title failed"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 207
    .end local v2    # "newTitle":Ljava/lang/String;
    :cond_9
    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v8, v7}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2

    .line 213
    .restart local v3    # "phoneType":I
    :cond_a
    if-ne v3, v10, :cond_b

    .line 214
    new-instance v8, Lcom/android/phone/settings/GsmUmtsOptions;

    iget-object v9, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v10, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-direct {v8, p0, v9, v10}, Lcom/android/phone/settings/GsmUmtsOptions;-><init>(Landroid/preference/PreferenceActivity;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V

    iput-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    goto/16 :goto_3

    .line 216
    :cond_b
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Unexpected phone type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 562
    packed-switch p1, :pswitch_data_0

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 565
    :pswitch_0
    const-string/jumbo v1, "exit_ecm_result"

    const/4 v2, 0x0

    .line 564
    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 566
    .local v0, "isChoiceYes":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 568
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mClickedPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Lcom/android/phone/settings/CdmaOptions;->showDialog(Landroid/preference/Preference;)V

    goto :goto_0

    .line 562
    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 141
    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 143
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 144
    :goto_0
    if-eqz v0, :cond_0

    .line 145
    sget v3, Lmiui/telephony/SubscriptionManager;->INVALID_SLOT_ID:I

    invoke-static {v0, v3}, Lmiui/telephony/SubscriptionManager;->getSlotId(Landroid/os/Bundle;I)I

    move-result v3

    iput v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    .line 148
    :cond_0
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-virtual {v3, v4}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    .line 150
    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    if-nez v3, :cond_2

    sget v3, Lmiui/telephony/SubscriptionManager;->INVALID_SUBSCRIPTION_ID:I

    :goto_1
    iput v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSubId:I

    .line 152
    iget v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSubId:I

    invoke-static {v3}, Lmiui/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 153
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->finish()V

    .line 154
    return-void

    .line 143
    :cond_1
    const/4 v0, 0x0

    .local v0, "extras":Landroid/os/Bundle;
    goto :goto_0

    .line 151
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_2
    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v3

    goto :goto_1

    .line 156
    :cond_3
    const v3, 0x7f060027

    invoke-virtual {p0, v3}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->addPreferencesFromResource(I)V

    .line 157
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->initMobileNetworkSetting()V

    .line 160
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsReceiverRegistered:Z

    .line 161
    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 162
    .local v2, "intentFilter":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v2}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 163
    const-string/jumbo v3, "MultiSimInfoEditorActivity_enter"

    iget v4, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-static {v3, v4}, Lcom/android/phone/utils/MiStatInterfaceUtil$PhoneSettings;->recordSlotIdInMultiSimInfoEditor(Ljava/lang/String;I)V

    .line 164
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->dismissProgressDialog()V

    .line 366
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onDestroy()V

    .line 367
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    invoke-virtual {v0}, Lcom/android/phone/settings/GsmUmtsOptions;->destroy()V

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    if-eqz v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    invoke-virtual {v0}, Lcom/android/phone/settings/CdmaOptions;->destroy()V

    .line 373
    :cond_1
    invoke-static {}, Lcom/android/internal/telephony/uicc/UiccController;->getInstance()Lcom/android/internal/telephony/uicc/UiccController;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/uicc/UiccController;->unregisterForIccChanged(Landroid/os/Handler;)V

    .line 374
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mDismissRunner:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 375
    iget-boolean v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsReceiverRegistered:Z

    if-eqz v0, :cond_2

    .line 376
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 378
    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 358
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onPause()V

    .line 359
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/telephony/SubscriptionManager;->removeOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 360
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsForeground:Z

    .line 361
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 11
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 581
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 582
    .local v0, "key":Ljava/lang/String;
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v7

    iget v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-virtual {v7, v8}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v4

    .line 583
    .local v4, "simInfo":Lmiui/telephony/SubscriptionInfo;
    if-nez v4, :cond_0

    .line 584
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->finish()V

    .line 585
    return v9

    .line 587
    :cond_0
    const-string/jumbo v7, "sim_name"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 588
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v7}, Lcom/android/phone/settings/CustomEditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    .line 589
    .local v5, "textName":Landroid/text/Editable;
    if-eqz v5, :cond_2

    .line 590
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v7}, Lcom/android/phone/settings/CustomEditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 591
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {v4}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 592
    return v9

    .line 595
    :cond_1
    invoke-static {}, Lmiui/telephony/SubscriptionManagerEx;->getDefault()Lmiui/telephony/SubscriptionManagerEx;

    move-result-object v7

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v8

    invoke-virtual {v7, v1, v8}, Lmiui/telephony/SubscriptionManagerEx;->setDisplayNameForSubscription(Ljava/lang/String;I)I

    move-result v3

    .line 596
    .local v3, "result":I
    if-lez v3, :cond_3

    .line 597
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v7, v1}, Lcom/android/phone/settings/CustomEditTextPreference;->setRightValue(Ljava/lang/String;)V

    .line 630
    .end local v1    # "name":Ljava/lang/String;
    .end local v3    # "result":I
    .end local v5    # "textName":Landroid/text/Editable;
    :cond_2
    :goto_0
    return v10

    .line 600
    .restart local v1    # "name":Ljava/lang/String;
    .restart local v3    # "result":I
    .restart local v5    # "textName":Landroid/text/Editable;
    :cond_3
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0652

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 599
    invoke-static {p0, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 602
    invoke-virtual {v4}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 603
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/phone/settings/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 605
    :cond_4
    return v9

    .line 609
    .end local v1    # "name":Ljava/lang/String;
    .end local v3    # "result":I
    .end local v5    # "textName":Landroid/text/Editable;
    :cond_5
    const-string/jumbo v7, "sim_number"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 610
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v7}, Lcom/android/phone/settings/CustomEditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    .line 611
    .local v6, "textNumber":Landroid/text/Editable;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 612
    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 613
    .local v2, "number":Ljava/lang/String;
    invoke-static {}, Lmiui/telephony/SubscriptionManagerEx;->getDefault()Lmiui/telephony/SubscriptionManagerEx;

    move-result-object v7

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v8

    invoke-virtual {v7, v2, v8}, Lmiui/telephony/SubscriptionManagerEx;->setDisplayNumberForSubscription(Ljava/lang/String;I)I

    move-result v7

    if-lez v7, :cond_7

    .line 614
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_6

    .line 615
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-static {v2}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/phone/settings/CustomEditTextPreference;->setRightValue(Ljava/lang/String;)V

    goto :goto_0

    .line 617
    :cond_6
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    iget-object v8, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mNotSet:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/phone/settings/CustomEditTextPreference;->setRightValue(Ljava/lang/String;)V

    goto :goto_0

    .line 620
    :cond_7
    invoke-virtual {v4}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_8

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_8

    .line 621
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v4}, Lmiui/telephony/SubscriptionInfo;->getDisplayNumber()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/phone/utils/Utils;->localizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/phone/settings/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    .line 625
    :goto_1
    return v9

    .line 623
    :cond_8
    iget-object v7, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    const-string/jumbo v8, ""

    invoke-virtual {v7, v8}, Lcom/android/phone/settings/CustomEditTextPreference;->setText(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 421
    const-string/jumbo v1, "enable_sim"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 422
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 423
    const-string/jumbo v1, "MultiSimInfoEditorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "User enable sim"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    if-eqz v1, :cond_0

    .line 425
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    invoke-virtual {v1}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v1

    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->switchRadioState(IZ)V

    .line 436
    .end local p1    # "preference":Landroid/preference/Preference;
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 428
    .restart local p1    # "preference":Landroid/preference/Preference;
    :cond_1
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->showDisableSimDialog()V

    goto :goto_0

    .line 430
    :cond_2
    const-string/jumbo v1, "sim_name"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 431
    const-string/jumbo v1, "sim_number"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 430
    if-eqz v1, :cond_0

    .line 432
    :cond_3
    check-cast p1, Lcom/android/phone/settings/CustomEditTextPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Lcom/android/phone/settings/CustomEditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    .line 433
    .local v0, "et":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 434
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 534
    if-eqz p2, :cond_0

    .line 535
    const-string/jumbo v1, "event_settings"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/phone/utils/MiStatInterfaceUtil;->recordCountEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    :cond_0
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mGsmUmtsOptions:Lcom/android/phone/settings/GsmUmtsOptions;

    invoke-virtual {v1, p2}, Lcom/android/phone/settings/GsmUmtsOptions;->preferenceTreeClick(Landroid/preference/Preference;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 538
    return v4

    .line 539
    :cond_1
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mCdmaOptions:Lcom/android/phone/settings/CdmaOptions;

    invoke-virtual {v1, p2}, Lcom/android/phone/settings/CdmaOptions;->preferenceTreeClick(Landroid/preference/Preference;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 540
    const-string/jumbo v1, "ril.cdma.inecmmode"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 542
    iput-object p2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mClickedPreference:Landroid/preference/Preference;

    .line 545
    new-instance v1, Landroid/content/Intent;

    .line 546
    const-string/jumbo v2, "com.android.internal.intent.action.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS"

    .line 545
    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 547
    const/16 v2, 0x11

    .line 545
    invoke-virtual {p0, v1, v2}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 549
    :cond_2
    return v4

    .line 550
    :cond_3
    const-string/jumbo v1, "button_go_to_virtual_sim_key"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 551
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->getVirtualSimIntent()Landroid/content/Intent;

    move-result-object v0

    .line 552
    .local v0, "i":Landroid/content/Intent;
    if-eqz v0, :cond_4

    .line 553
    invoke-virtual {p0, v0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 555
    :cond_4
    return v4

    .line 557
    .end local v0    # "i":Landroid/content/Intent;
    :cond_5
    const/4 v1, 0x0

    return v1
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 262
    invoke-super {p0}, Lmiui/preference/PreferenceActivity;->onResume()V

    .line 263
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmiui/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Lmiui/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 264
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsForeground:Z

    .line 265
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->updateInfo()V

    .line 266
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->updateCheckState()V

    .line 267
    return-void
.end method

.method public onSubscriptionsChanged()V
    .locals 3

    .prologue
    .line 271
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    invoke-virtual {v1, v2}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoForSlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v0

    .line 272
    .local v0, "info":Lmiui/telephony/SubscriptionInfo;
    iput-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimInfoRecord:Lmiui/telephony/SubscriptionInfo;

    .line 273
    iget-boolean v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mIsDisableLteDevice:Z

    if-eqz v1, :cond_0

    .line 274
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->updateNetworkTypeAvailable()V

    .line 276
    :cond_0
    if-eqz v0, :cond_3

    .line 277
    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v1

    iget v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSlotId:I

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v1

    iget v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSubId:I

    if-eq v1, v2, :cond_2

    .line 278
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->finish()V

    .line 280
    :cond_2
    invoke-direct {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->updateCheckState()V

    .line 284
    :goto_0
    return-void

    .line 282
    :cond_3
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->finish()V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v1, 0x0

    .line 405
    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimName:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v2}, Lcom/android/phone/settings/CustomEditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 406
    .local v0, "d":Landroid/app/Dialog;
    if-nez v0, :cond_0

    .line 407
    iget-object v2, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mSimNumber:Lcom/android/phone/settings/CustomEditTextPreference;

    invoke-virtual {v2}, Lcom/android/phone/settings/CustomEditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 409
    :cond_0
    instance-of v2, v0, Landroid/app/AlertDialog;

    if-eqz v2, :cond_2

    .line 410
    check-cast v0, Landroid/app/AlertDialog;

    .end local v0    # "d":Landroid/app/Dialog;
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 412
    :cond_2
    return-void
.end method

.method public showProgressDialog(I)V
    .locals 4
    .param p1, "msgId"    # I

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->dismissProgressDialog()V

    .line 483
    new-instance v1, Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;

    invoke-direct {v1, p1}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;-><init>(I)V

    iput-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mProgressDialog:Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;

    .line 485
    :try_start_0
    iget-object v1, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mProgressDialog:Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/phone/settings/MultiSimInfoEditorActivity$ProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 490
    :goto_0
    return-void

    .line 486
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string/jumbo v1, "MultiSimInfoEditorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "error: show dialogfragment"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateEnableSimChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 731
    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/settings/MultiSimInfoEditorActivity;->mEnableSim:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 732
    :cond_0
    return-void
.end method
