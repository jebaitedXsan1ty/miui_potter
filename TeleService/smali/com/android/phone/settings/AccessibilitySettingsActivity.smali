.class public Lcom/android/phone/settings/AccessibilitySettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "AccessibilitySettingsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lcom/android/phone/settings/AccessibilitySettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0b04fd

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 32
    invoke-virtual {p0}, Lcom/android/phone/settings/AccessibilitySettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/android/phone/settings/AccessibilitySettingsFragment;

    invoke-direct {v1}, Lcom/android/phone/settings/AccessibilitySettingsFragment;-><init>()V

    const v2, 0x1020002

    .line 32
    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 34
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 38
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/android/phone/settings/AccessibilitySettingsActivity;->onBackPressed()V

    .line 40
    const/4 v0, 0x1

    return v0

    .line 42
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method
