.class Lcom/android/phone/settings/VolteSwitchView$4$1;
.super Ljava/lang/Object;
.source "VolteSwitchView.java"

# interfaces
.implements Lcom/android/phone/Dual4GManager$CallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/settings/VolteSwitchView$4;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/phone/settings/VolteSwitchView$4;


# direct methods
.method constructor <init>(Lcom/android/phone/settings/VolteSwitchView$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/phone/settings/VolteSwitchView$4;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/settings/VolteSwitchView$4$1;->this$1:Lcom/android/phone/settings/VolteSwitchView$4;

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onSetDual4GResult(ZI)V
    .locals 4
    .param p1, "enabled"    # Z
    .param p2, "resultCode"    # I

    .prologue
    const/4 v3, 0x0

    .line 126
    sget-object v0, Lcom/android/phone/settings/VolteSwitchView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onSetDual4GResult enabled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    if-eqz p2, :cond_0

    .line 128
    iget-object v0, p0, Lcom/android/phone/settings/VolteSwitchView$4$1;->this$1:Lcom/android/phone/settings/VolteSwitchView$4;

    iget-object v0, v0, Lcom/android/phone/settings/VolteSwitchView$4;->this$0:Lcom/android/phone/settings/VolteSwitchView;

    invoke-static {v0}, Lcom/android/phone/settings/VolteSwitchView;->-get4(Lcom/android/phone/settings/VolteSwitchView;)Landroid/preference/PreferenceActivity;

    move-result-object v1

    if-eqz p1, :cond_1

    const v0, 0x7f0b06ef

    :goto_0
    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 131
    :cond_0
    return-void

    .line 128
    :cond_1
    const v0, 0x7f0b06f0

    goto :goto_0
.end method
