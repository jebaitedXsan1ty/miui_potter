.class Lcom/android/phone/NetworkSelectListPreference$1;
.super Landroid/os/Handler;
.source "NetworkSelectListPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/NetworkSelectListPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/NetworkSelectListPreference;


# direct methods
.method constructor <init>(Lcom/android/phone/NetworkSelectListPreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/NetworkSelectListPreference;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    .line 86
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 90
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 118
    :goto_0
    return-void

    .line 92
    :sswitch_0
    iget-object v3, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-static {v3, v2, v4}, Lcom/android/phone/NetworkSelectListPreference;->-wrap3(Lcom/android/phone/NetworkSelectListPreference;Ljava/util/List;I)V

    goto :goto_0

    .line 96
    :sswitch_1
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    const-string/jumbo v3, "hideProgressPanel"

    invoke-static {v2, v3}, Lcom/android/phone/NetworkSelectListPreference;->-wrap2(Lcom/android/phone/NetworkSelectListPreference;Ljava/lang/String;)V

    .line 98
    :try_start_0
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    invoke-static {v2}, Lcom/android/phone/NetworkSelectListPreference;->-wrap1(Lcom/android/phone/NetworkSelectListPreference;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_1
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/phone/NetworkSelectListPreference;->setEnabled(Z)V

    .line 103
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 104
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_0

    .line 105
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    const-string/jumbo v3, "manual network selection: failed!"

    invoke-static {v2, v3}, Lcom/android/phone/NetworkSelectListPreference;->-wrap2(Lcom/android/phone/NetworkSelectListPreference;Ljava/lang/String;)V

    .line 106
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    invoke-static {v2}, Lcom/android/phone/NetworkSelectListPreference;->-get1(Lcom/android/phone/NetworkSelectListPreference;)Lcom/android/phone/NetworkOperators;

    move-result-object v2

    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v2, v3}, Lcom/android/phone/NetworkOperators;->displayNetworkSelectionFailed(Ljava/lang/Throwable;)V

    .line 114
    :goto_2
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    invoke-static {v2}, Lcom/android/phone/NetworkSelectListPreference;->-get1(Lcom/android/phone/NetworkSelectListPreference;)Lcom/android/phone/NetworkOperators;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/phone/NetworkOperators;->getNetworkSelectionMode()V

    goto :goto_0

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "manual network selection: succeeded!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 110
    iget-object v4, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    iget-object v5, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    invoke-static {v5}, Lcom/android/phone/NetworkSelectListPreference;->-get2(Lcom/android/phone/NetworkSelectListPreference;)Lcom/android/internal/telephony/OperatorInfo;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/phone/NetworkSelectListPreference;->-wrap0(Lcom/android/phone/NetworkSelectListPreference;Lcom/android/internal/telephony/OperatorInfo;)Ljava/lang/String;

    move-result-object v4

    .line 109
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/phone/NetworkSelectListPreference;->-wrap2(Lcom/android/phone/NetworkSelectListPreference;Ljava/lang/String;)V

    .line 112
    iget-object v2, p0, Lcom/android/phone/NetworkSelectListPreference$1;->this$0:Lcom/android/phone/NetworkSelectListPreference;

    invoke-static {v2}, Lcom/android/phone/NetworkSelectListPreference;->-get1(Lcom/android/phone/NetworkSelectListPreference;)Lcom/android/phone/NetworkOperators;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/phone/NetworkOperators;->displayNetworkSelectionSucceeded()V

    goto :goto_2

    .line 99
    .end local v0    # "ar":Landroid/os/AsyncResult;
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_1

    .line 90
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch
.end method
