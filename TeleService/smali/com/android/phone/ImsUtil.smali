.class public Lcom/android/phone/ImsUtil;
.super Ljava/lang/Object;
.source "ImsUtil.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static sImsPhoneSupported:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    const-class v1, Lcom/android/phone/ImsUtil;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/phone/ImsUtil;->LOG_TAG:Ljava/lang/String;

    .line 34
    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/phone/ImsUtil;->sImsPhoneSupported:Z

    .line 41
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    .line 42
    .local v0, "app":Lcom/android/phone/PhoneGlobals;
    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/phone/ImsUtil;->sImsPhoneSupported:Z

    .line 30
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method private static getSubId(I)I
    .locals 4
    .param p0, "phoneId"    # I

    .prologue
    .line 157
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[I

    move-result-object v1

    .line 158
    .local v1, "subIds":[I
    const/4 v0, -0x1

    .line 159
    .local v0, "subId":I
    if-eqz v1, :cond_0

    array-length v2, v1

    const/4 v3, 0x1

    if-lt v2, v3, :cond_0

    .line 160
    const/4 v2, 0x0

    aget v0, v1, v2

    .line 163
    :cond_0
    return v0
.end method

.method public static isWfcEnabled(Landroid/content/Context;I)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneId"    # I

    .prologue
    .line 64
    invoke-static {p0, p1}, Lcom/android/ims/ImsManager;->getInstance(Landroid/content/Context;I)Lcom/android/ims/ImsManager;

    move-result-object v0

    .line 65
    .local v0, "imsManager":Lcom/android/ims/ImsManager;
    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->isWfcEnabledByPlatformForSlot()Z

    move-result v1

    .line 66
    .local v1, "isEnabledByPlatform":Z
    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->isWfcEnabledByUserForSlot()Z

    move-result v2

    .line 71
    .local v2, "isEnabledByUser":Z
    if-eqz v1, :cond_0

    .end local v2    # "isEnabledByUser":Z
    :goto_0
    return v2

    .restart local v2    # "isEnabledByUser":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isWfcModeWifiOnly(Landroid/content/Context;I)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneId"    # I

    .prologue
    const/4 v2, 0x0

    .line 102
    invoke-static {p0, p1}, Lcom/android/ims/ImsManager;->getInstance(Landroid/content/Context;I)Lcom/android/ims/ImsManager;

    move-result-object v0

    .line 104
    .local v0, "imsManager":Lcom/android/ims/ImsManager;
    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->getWfcModeForSlot()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    .line 107
    .local v1, "isWifiOnlyMode":Z
    :goto_0
    invoke-static {p0, p1}, Lcom/android/phone/ImsUtil;->isWfcEnabled(Landroid/content/Context;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .end local v1    # "isWifiOnlyMode":Z
    :goto_1
    return v1

    .line 104
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "isWifiOnlyMode":Z
    goto :goto_0

    :cond_1
    move v1, v2

    .line 107
    goto :goto_1
.end method

.method public static isWfcProvisioned(Landroid/content/Context;I)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneId"    # I

    .prologue
    .line 85
    invoke-static {p0, p1}, Lcom/android/ims/ImsManager;->getInstance(Landroid/content/Context;I)Lcom/android/ims/ImsManager;

    move-result-object v0

    .line 86
    .local v0, "imsManager":Lcom/android/ims/ImsManager;
    invoke-virtual {v0}, Lcom/android/ims/ImsManager;->isWfcProvisionedOnDeviceForSlot()Z

    move-result v1

    return v1
.end method

.method public static shouldPromoteWfc(Landroid/content/Context;I)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneId"    # I

    .prologue
    const/4 v3, 0x0

    .line 132
    const-string/jumbo v4, "carrier_config"

    .line 131
    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CarrierConfigManager;

    .line 135
    .local v0, "cfgManager":Landroid/telephony/CarrierConfigManager;
    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/phone/ImsUtil;->getSubId(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v4

    .line 136
    const-string/jumbo v5, "carrier_promote_wfc_on_call_fail_bool"

    .line 135
    invoke-virtual {v4, v5}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    .line 137
    :cond_0
    return v3

    .line 140
    :cond_1
    invoke-static {p0, p1}, Lcom/android/phone/ImsUtil;->isWfcProvisioned(Landroid/content/Context;I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 141
    return v3

    .line 145
    :cond_2
    const-string/jumbo v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 146
    .local v1, "cm":Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_4

    .line 147
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 148
    .local v2, "ni":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 149
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    invoke-static {p0, p1}, Lcom/android/phone/ImsUtil;->isWfcEnabled(Landroid/content/Context;I)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    :cond_3
    return v3

    .line 153
    .end local v2    # "ni":Landroid/net/NetworkInfo;
    :cond_4
    return v3
.end method
