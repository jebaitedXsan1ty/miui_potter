.class Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;
.super Landroid/telephony/PhoneStateListener;
.source "MobileNetworkSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;


# direct methods
.method constructor <init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V
    .locals 0
    .param p1, "this$1"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .line 249
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 7
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 258
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "PhoneStateListener.onCallStateChanged: state="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-wrap3(Ljava/lang/String;)V

    .line 260
    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-virtual {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 261
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 262
    return-void

    .line 265
    :cond_0
    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-get2(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 266
    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-get2(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v4

    .line 268
    .local v4, "subId":I
    :goto_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v1

    .line 269
    .local v1, "carrierConfig":Landroid/os/PersistableBundle;
    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-wrap1(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;Landroid/content/Context;Landroid/os/PersistableBundle;)Z

    move-result v2

    .line 271
    .local v2, "enabled":Z
    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-virtual {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const-string/jumbo v6, "enhanced_4g_lte"

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 272
    .local v3, "pref":Landroid/preference/Preference;
    if-eqz v3, :cond_1

    if-eqz v2, :cond_3

    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;->this$1:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-static {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-wrap0(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Z

    move-result v5

    :goto_1
    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 274
    :cond_1
    return-void

    .line 266
    .end local v1    # "carrierConfig":Landroid/os/PersistableBundle;
    .end local v2    # "enabled":Z
    .end local v3    # "pref":Landroid/preference/Preference;
    .end local v4    # "subId":I
    :cond_2
    const/4 v4, -0x1

    .restart local v4    # "subId":I
    goto :goto_0

    .line 272
    .restart local v1    # "carrierConfig":Landroid/os/PersistableBundle;
    .restart local v2    # "enabled":Z
    .restart local v3    # "pref":Landroid/preference/Preference;
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method
