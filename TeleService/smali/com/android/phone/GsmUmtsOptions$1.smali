.class Lcom/android/phone/GsmUmtsOptions$1;
.super Ljava/lang/Object;
.source "GsmUmtsOptions.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/GsmUmtsOptions;->update(ILcom/android/phone/INetworkQueryService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/GsmUmtsOptions;

.field final synthetic val$subId:I


# direct methods
.method constructor <init>(Lcom/android/phone/GsmUmtsOptions;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/GsmUmtsOptions;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/GsmUmtsOptions$1;->this$0:Lcom/android/phone/GsmUmtsOptions;

    iput p2, p0, Lcom/android/phone/GsmUmtsOptions$1;->val$subId:I

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v3, 0x1

    .line 121
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.APN_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 123
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, ":settings:show_fragment_as_subsetting"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 124
    const-string/jumbo v1, "sub_id"

    iget v2, p0, Lcom/android/phone/GsmUmtsOptions$1;->val$subId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 125
    iget-object v1, p0, Lcom/android/phone/GsmUmtsOptions$1;->this$0:Lcom/android/phone/GsmUmtsOptions;

    invoke-static {v1}, Lcom/android/phone/GsmUmtsOptions;->-get0(Lcom/android/phone/GsmUmtsOptions;)Landroid/preference/PreferenceFragment;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceFragment;->startActivity(Landroid/content/Intent;)V

    .line 126
    return v3
.end method
