.class Lcom/android/phone/CdmaOptions$1;
.super Ljava/lang/Object;
.source "CdmaOptions.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/CdmaOptions;->update(Lcom/android/internal/telephony/Phone;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CdmaOptions;


# direct methods
.method constructor <init>(Lcom/android/phone/CdmaOptions;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/CdmaOptions;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/CdmaOptions$1;->this$0:Lcom/android/phone/CdmaOptions;

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v3, 0x1

    .line 101
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.APN_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 103
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, ":settings:show_fragment_as_subsetting"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 104
    const-string/jumbo v1, "sub_id"

    iget-object v2, p0, Lcom/android/phone/CdmaOptions$1;->this$0:Lcom/android/phone/CdmaOptions;

    invoke-static {v2}, Lcom/android/phone/CdmaOptions;->-get0(Lcom/android/phone/CdmaOptions;)Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 105
    iget-object v1, p0, Lcom/android/phone/CdmaOptions$1;->this$0:Lcom/android/phone/CdmaOptions;

    invoke-static {v1}, Lcom/android/phone/CdmaOptions;->-get1(Lcom/android/phone/CdmaOptions;)Landroid/preference/PreferenceFragment;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceFragment;->startActivity(Landroid/content/Intent;)V

    .line 106
    return v3
.end method
