.class public Lcom/android/phone/DataUsagePreference;
.super Landroid/preference/Preference;
.source "DataUsagePreference.java"


# instance fields
.field private mSubId:I

.field private mTemplate:Landroid/net/NetworkTemplate;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method private getNetworkTemplate(Landroid/app/Activity;I)Landroid/net/NetworkTemplate;
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "subId"    # I

    .prologue
    .line 76
    const-string/jumbo v2, "phone"

    .line 75
    invoke-virtual {p1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 78
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1, p2}, Landroid/telephony/TelephonyManager;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v2

    .line 77
    invoke-static {v2}, Landroid/net/NetworkTemplate;->buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v0

    .line 80
    .local v0, "mobileAll":Landroid/net/NetworkTemplate;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getMergedSubscriberIds()[Ljava/lang/String;

    move-result-object v2

    .line 79
    invoke-static {v0, v2}, Landroid/net/NetworkTemplate;->normalize(Landroid/net/NetworkTemplate;[Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public getIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 65
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.settings.MOBILE_DATA_USAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 66
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "show_drawer_menu"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 68
    const-string/jumbo v1, "network_template"

    iget-object v2, p0, Lcom/android/phone/DataUsagePreference;->mTemplate:Landroid/net/NetworkTemplate;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 69
    const-string/jumbo v1, "sub_id"

    iget v2, p0, Lcom/android/phone/DataUsagePreference;->mSubId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 71
    return-object v0
.end method

.method public initialize(I)V
    .locals 6
    .param p1, "subId"    # I

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/android/phone/DataUsagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 52
    .local v0, "activity":Landroid/app/Activity;
    iput p1, p0, Lcom/android/phone/DataUsagePreference;->mSubId:I

    .line 53
    invoke-direct {p0, v0, p1}, Lcom/android/phone/DataUsagePreference;->getNetworkTemplate(Landroid/app/Activity;I)Landroid/net/NetworkTemplate;

    move-result-object v3

    iput-object v3, p0, Lcom/android/phone/DataUsagePreference;->mTemplate:Landroid/net/NetworkTemplate;

    .line 55
    new-instance v1, Lcom/android/settingslib/net/DataUsageController;

    invoke-direct {v1, v0}, Lcom/android/settingslib/net/DataUsageController;-><init>(Landroid/content/Context;)V

    .line 57
    .local v1, "controller":Lcom/android/settingslib/net/DataUsageController;
    iget-object v3, p0, Lcom/android/phone/DataUsagePreference;->mTemplate:Landroid/net/NetworkTemplate;

    invoke-virtual {v1, v3}, Lcom/android/settingslib/net/DataUsageController;->getDataUsageInfo(Landroid/net/NetworkTemplate;)Lcom/android/settingslib/net/DataUsageController$DataUsageInfo;

    move-result-object v2

    .line 58
    .local v2, "usageInfo":Lcom/android/settingslib/net/DataUsageController$DataUsageInfo;
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 59
    iget-wide v4, v2, Lcom/android/settingslib/net/DataUsageController$DataUsageInfo;->usageLevel:J

    invoke-static {v0, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, v2, Lcom/android/settingslib/net/DataUsageController$DataUsageInfo;->period:Ljava/lang/String;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 58
    const v4, 0x7f0b03b0

    invoke-virtual {v0, v4, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/phone/DataUsagePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 60
    invoke-virtual {p0}, Lcom/android/phone/DataUsagePreference;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/phone/DataUsagePreference;->setIntent(Landroid/content/Intent;)V

    .line 61
    return-void
.end method
