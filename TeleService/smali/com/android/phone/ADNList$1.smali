.class Lcom/android/phone/ADNList$1;
.super Ljava/lang/Object;
.source "ADNList.java"

# interfaces
.implements Landroid/widget/SimpleCursorAdapter$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/phone/ADNList;->newAdapter()Landroid/widget/CursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/ADNList;


# direct methods
.method constructor <init>(Lcom/android/phone/ADNList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/ADNList;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/ADNList$1;->this$0:Lcom/android/phone/ADNList;

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public setViewValue(Landroid/view/View;Landroid/database/Cursor;I)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "columnIndex"    # I

    .prologue
    const/4 v3, 0x1

    .line 176
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, Landroid/view/View;->setTextAlignment(I)V

    .line 177
    if-ne p3, v3, :cond_2

    .line 178
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 179
    .local v1, "num":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 180
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    .line 181
    .local v0, "bf":Landroid/text/BidiFormatter;
    sget-object v2, Landroid/text/TextDirectionHeuristics;->LTR:Landroid/text/TextDirectionHeuristic;

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;Landroid/text/TextDirectionHeuristic;Z)Ljava/lang/String;

    move-result-object v1

    .line 183
    .end local v0    # "bf":Landroid/text/BidiFormatter;
    :cond_0
    instance-of v2, p1, Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 184
    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    :cond_1
    return v3

    .line 188
    .end local v1    # "num":Ljava/lang/String;
    .restart local p1    # "view":Landroid/view/View;
    :cond_2
    const/4 v2, 0x0

    return v2
.end method
