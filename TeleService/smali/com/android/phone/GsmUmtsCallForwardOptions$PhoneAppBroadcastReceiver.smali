.class Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GsmUmtsCallForwardOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/GsmUmtsCallForwardOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhoneAppBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/GsmUmtsCallForwardOptions;


# direct methods
.method private constructor <init>(Lcom/android/phone/GsmUmtsCallForwardOptions;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/GsmUmtsCallForwardOptions;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/GsmUmtsCallForwardOptions;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/GsmUmtsCallForwardOptions;Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/GsmUmtsCallForwardOptions;
    .param p2, "-this1"    # Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;-><init>(Lcom/android/phone/GsmUmtsCallForwardOptions;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 134
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 135
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v2, "android.intent.action.ANY_DATA_STATE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    const-string/jumbo v2, "state"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "state":Ljava/lang/String;
    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$DataState;->DISCONNECTED:Lcom/android/internal/telephony/PhoneConstants$DataState;

    invoke-virtual {v2}, Lcom/android/internal/telephony/PhoneConstants$DataState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    const-string/jumbo v2, "GsmUmtsCallForwardOptions"

    const-string/jumbo v3, "data is disconnected."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-object v2, p0, Lcom/android/phone/GsmUmtsCallForwardOptions$PhoneAppBroadcastReceiver;->this$0:Lcom/android/phone/GsmUmtsCallForwardOptions;

    invoke-virtual {v2}, Lcom/android/phone/GsmUmtsCallForwardOptions;->checkDataStatus()V

    .line 142
    .end local v1    # "state":Ljava/lang/String;
    :cond_0
    return-void
.end method
