.class final enum Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;
.super Ljava/lang/Enum;
.source "IccNetworkDepersonalizationPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/IccNetworkDepersonalizationPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "statusType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

.field public static final enum ENTRY:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

.field public static final enum ERROR:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

.field public static final enum IN_PROGRESS:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

.field public static final enum SUCCESS:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 86
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    const-string/jumbo v1, "ENTRY"

    invoke-direct {v0, v1, v2}, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->ENTRY:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    .line 87
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    const-string/jumbo v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->IN_PROGRESS:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    .line 88
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    const-string/jumbo v1, "ERROR"

    invoke-direct {v0, v1, v4}, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->ERROR:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    .line 89
    new-instance v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    const-string/jumbo v1, "SUCCESS"

    invoke-direct {v0, v1, v5}, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->SUCCESS:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    .line 85
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    sget-object v1, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->ENTRY:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->IN_PROGRESS:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->ERROR:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->SUCCESS:Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->$VALUES:[Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 85
    const-class v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    return-object v0
.end method

.method public static values()[Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;->$VALUES:[Lcom/android/phone/IccNetworkDepersonalizationPanel$statusType;

    return-object v0
.end method
