.class public Lcom/android/phone/VolteEnableManager$VolteEnableHandler;
.super Landroid/os/Handler;
.source "VolteEnableManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/VolteEnableManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "VolteEnableHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/VolteEnableManager;


# direct methods
.method public constructor <init>(Lcom/android/phone/VolteEnableManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/VolteEnableManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->this$0:Lcom/android/phone/VolteEnableManager;

    .line 64
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 65
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 69
    iget v9, p1, Landroid/os/Message;->what:I

    packed-switch v9, :pswitch_data_0

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 71
    :pswitch_0
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v7

    invoke-virtual {v7}, Lmiui/telephony/SubscriptionManager;->getSubscriptionInfoList()Ljava/util/List;

    move-result-object v5

    .line 72
    .local v5, "subInfoImpls":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "subInfo$iterator":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/telephony/SubscriptionInfo;

    .line 73
    .local v3, "subInfo":Lmiui/telephony/SubscriptionInfo;
    iget-object v7, p0, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->this$0:Lcom/android/phone/VolteEnableManager;

    invoke-static {v7, v3}, Lcom/android/phone/VolteEnableManager;->-wrap0(Lcom/android/phone/VolteEnableManager;Lmiui/telephony/SubscriptionInfo;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 74
    iget-object v7, p0, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->this$0:Lcom/android/phone/VolteEnableManager;

    invoke-virtual {v3}, Lmiui/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v8

    invoke-static {v7, v8}, Lcom/android/phone/VolteEnableManager;->-wrap1(Lcom/android/phone/VolteEnableManager;I)Z

    goto :goto_1

    .line 77
    .end local v3    # "subInfo":Lmiui/telephony/SubscriptionInfo;
    :cond_2
    iget-object v7, p0, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->this$0:Lcom/android/phone/VolteEnableManager;

    invoke-static {v7, v5}, Lcom/android/phone/VolteEnableManager;->-set0(Lcom/android/phone/VolteEnableManager;Ljava/util/List;)Ljava/util/List;

    goto :goto_0

    .line 80
    .end local v4    # "subInfo$iterator":Ljava/util/Iterator;
    .end local v5    # "subInfoImpls":Ljava/util/List;, "Ljava/util/List<Lmiui/telephony/SubscriptionInfo;>;"
    :pswitch_1
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v1

    .line 81
    .local v1, "defaultDataSlotId":I
    invoke-static {}, Lcom/android/phone/VolteEnableManager;->-get0()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onDataSlotReady dataSlot="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 84
    .end local v1    # "defaultDataSlotId":I
    :pswitch_2
    invoke-static {}, Lcom/android/phone/VolteEnableManager;->-get0()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "EVENT_DATA_SLOT_CHANGED"

    invoke-static {v9, v10}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isCTDualVolteSupported()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-static {}, Lcom/android/phone/MiuiPhoneUtils;->isDualCTSim()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 86
    invoke-static {}, Lmiui/telephony/DefaultSimManager;->getDefaultDataSlotId()I

    move-result v0

    .line 87
    .local v0, "ddsSlotId":I
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v9

    invoke-virtual {v9, v0}, Lmiui/telephony/SubscriptionManager;->getSubscriptionIdForSlot(I)I

    move-result v2

    .line 88
    .local v2, "subId":I
    invoke-static {v2}, Lcom/android/phone/VolteEnableManager;->getUserSelectedVolteState(I)I

    move-result v6

    .line 90
    .local v6, "userEnabled":I
    const/4 v9, -0x1

    if-ne v6, v9, :cond_3

    .line 91
    iget-object v7, p0, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->this$0:Lcom/android/phone/VolteEnableManager;

    invoke-static {v7}, Lcom/android/phone/VolteEnableManager;->-get1(Lcom/android/phone/VolteEnableManager;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v8, v0}, Lcom/android/services/telephony/ims/ImsAdapter;->setEnhanced4gLteModeSetting(Landroid/content/Context;ZI)V

    .line 96
    :goto_2
    invoke-static {}, Lcom/android/phone/CarrierConfigLoaderInjector;->enableViceSlotVolteForDualCtSim()V

    goto/16 :goto_0

    .line 93
    :cond_3
    iget-object v9, p0, Lcom/android/phone/VolteEnableManager$VolteEnableHandler;->this$0:Lcom/android/phone/VolteEnableManager;

    invoke-static {v9}, Lcom/android/phone/VolteEnableManager;->-get1(Lcom/android/phone/VolteEnableManager;)Landroid/content/Context;

    move-result-object v9

    if-ne v6, v7, :cond_4

    :goto_3
    invoke-static {v9, v7, v0}, Lcom/android/services/telephony/ims/ImsAdapter;->setEnhanced4gLteModeSetting(Landroid/content/Context;ZI)V

    goto :goto_2

    :cond_4
    move v7, v8

    goto :goto_3

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
