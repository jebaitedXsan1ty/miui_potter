.class public Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;
.super Landroid/preference/PreferenceFragment;
.source "MobileNetworkSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/phone/RoamingDialogFragment$RoamingDialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/MobileNetworkSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MobileNetworkFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;,
        Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$2;,
        Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$3;,
        Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$4;,
        Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$5;,
        Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;,
        Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$PhoneChangeReceiver;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-phone-MobileNetworkSettings$TabStateSwitchesValues:[I

.field static final preferredNetworkMode:I


# instance fields
.field private mActiveSubInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/telephony/SubscriptionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAdvancedOptions:Lcom/android/phone/AdvancedOptionsPreference;

.field private mButton4glte:Landroid/preference/SwitchPreference;

.field private mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

.field private mButtonEnabledNetworks:Landroid/preference/ListPreference;

.field private mButtonNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

.field private mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

.field private mCallingCategory:Landroid/preference/PreferenceCategory;

.field mCdmaOptions:Lcom/android/phone/CdmaOptions;

.field private mClickedPreference:Landroid/preference/Preference;

.field private mDataUsagePref:Lcom/android/phone/DataUsagePreference;

.field private mEmptyTabContent:Landroid/widget/TabHost$TabContentFactory;

.field private mEuiccSettingsPref:Landroid/preference/Preference;

.field private mExpandAdvancedFields:Z

.field mGsmUmtsOptions:Lcom/android/phone/GsmUmtsOptions;

.field private mHandler:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;

.field private mIsGlobalCdma:Z

.field private mLteDataServicePref:Landroid/preference/Preference;

.field private mMobileDataPref:Lcom/android/phone/MobileDataPreference;

.field private mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

.field private final mNetworkQueryServiceConnection:Landroid/content/ServiceConnection;

.field private mOkClicked:Z

.field private final mOnSubscriptionsChangeListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private final mPhoneChangeReceiver:Landroid/content/BroadcastReceiver;

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mShow4GForLTE:Z

.field private mSubscriptionManager:Landroid/telephony/SubscriptionManager;

.field private mTabHost:Landroid/widget/TabHost;

.field private mTabListener:Landroid/widget/TabHost$OnTabChangeListener;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mUm:Landroid/os/UserManager;

.field private mUnavailable:Z

.field private mVideoCallingPref:Landroid/preference/SwitchPreference;

.field private mWiFiCallingPref:Landroid/preference/Preference;


# direct methods
.method static synthetic -get0(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Landroid/widget/TabHost;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    return-object v0
.end method

.method private static synthetic -getcom-android-phone-MobileNetworkSettings$TabStateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-com-android-phone-MobileNetworkSettings$TabStateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-com-android-phone-MobileNetworkSettings$TabStateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/phone/MobileNetworkSettings$TabState;->values()[Lcom/android/phone/MobileNetworkSettings$TabState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/phone/MobileNetworkSettings$TabState;->DO_NOTHING:Lcom/android/phone/MobileNetworkSettings$TabState;

    invoke-virtual {v1}, Lcom/android/phone/MobileNetworkSettings$TabState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/phone/MobileNetworkSettings$TabState;->NO_TABS:Lcom/android/phone/MobileNetworkSettings$TabState;

    invoke-virtual {v1}, Lcom/android/phone/MobileNetworkSettings$TabState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/phone/MobileNetworkSettings$TabState;->UPDATE:Lcom/android/phone/MobileNetworkSettings$TabState;

    invoke-virtual {v1}, Lcom/android/phone/MobileNetworkSettings$TabState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    sput-object v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-com-android-phone-MobileNetworkSettings$TabStateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;Lcom/android/phone/INetworkQueryService;)Lcom/android/phone/INetworkQueryService;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;
    .param p1, "-value"    # Lcom/android/phone/INetworkQueryService;

    .prologue
    iput-object p1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->hasActiveSubscriptions()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;Landroid/content/Context;Landroid/os/PersistableBundle;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "carrierConfig"    # Landroid/os/PersistableBundle;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->is4gLtePrefEnabled(Landroid/content/Context;Landroid/os/PersistableBundle;)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap2(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->initializeSubscriptions()V

    return-void
.end method

.method static synthetic -wrap3(Ljava/lang/String;)V
    .locals 0
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    invoke-static {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->setNetworkQueryService()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateBody()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;
    .param p1, "slotId"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updatePhone(I)V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updatePreferredNetworkUIFromDb()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 200
    sget v0, Lcom/android/internal/telephony/Phone;->PREFERRED_NT_MODE:I

    sput v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->preferredNetworkMode:I

    .line 161
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 161
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 198
    new-instance v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$PhoneChangeReceiver;

    invoke-direct {v0, p0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$PhoneChangeReceiver;-><init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$PhoneChangeReceiver;)V

    iput-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhoneChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 249
    new-instance v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;

    invoke-direct {v0, p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$1;-><init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V

    iput-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 284
    iput-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    .line 295
    new-instance v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$2;

    invoke-direct {v0, p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$2;-><init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V

    iput-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryServiceConnection:Landroid/content/ServiceConnection;

    .line 424
    new-instance v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$3;

    invoke-direct {v0, p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$3;-><init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V

    .line 423
    iput-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mOnSubscriptionsChangeListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    .line 547
    new-instance v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$4;

    invoke-direct {v0, p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$4;-><init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V

    iput-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabListener:Landroid/widget/TabHost$OnTabChangeListener;

    .line 571
    new-instance v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$5;

    invoke-direct {v0, p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$5;-><init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;)V

    iput-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mEmptyTabContent:Landroid/widget/TabHost$TabContentFactory;

    .line 161
    return-void
.end method

.method private UpdateEnabledNetworksValueAndSummary(I)V
    .locals 7
    .param p1, "NetworkMode"    # I

    .prologue
    const v1, 0x7f0b0411

    const/16 v6, 0xa

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x7f0b0413

    .line 1399
    packed-switch p1, :pswitch_data_0

    .line 1514
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid Network Mode ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "). Ignore."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1515
    .local v0, "errMsg":Ljava/lang/String;
    invoke-static {v0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->loge(Ljava/lang/String;)V

    .line 1516
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1518
    .end local v0    # "errMsg":Ljava/lang/String;
    :goto_0
    return-void

    .line 1403
    :pswitch_0
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1404
    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1403
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1405
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1410
    :pswitch_1
    iget-boolean v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mIsGlobalCdma:Z

    if-nez v1, :cond_0

    .line 1411
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1412
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1411
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1413
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1415
    :cond_0
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1416
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1415
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1417
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    const v2, 0x7f0b0416

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1421
    :pswitch_2
    iget-boolean v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mIsGlobalCdma:Z

    if-nez v1, :cond_1

    .line 1422
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1423
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1422
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1424
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    const v2, 0x7f0b0414

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1426
    :cond_1
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1427
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1426
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1428
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    const v2, 0x7f0b0416

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1432
    :pswitch_3
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isWorldMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1433
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1434
    const v2, 0x7f0b039c

    .line 1433
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    .line 1435
    invoke-direct {p0, v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->controlCdmaOptions(Z)V

    .line 1436
    invoke-direct {p0, v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->controlGsmOptions(Z)V

    goto :goto_0

    .line 1441
    :cond_2
    :pswitch_4
    iget-boolean v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mIsGlobalCdma:Z

    if-nez v2, :cond_4

    .line 1442
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1443
    const/16 v3, 0x9

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 1442
    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1444
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    iget-boolean v3, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mShow4GForLTE:Z

    if-eqz v3, :cond_3

    .line 1445
    const v1, 0x7f0b0412

    .line 1444
    :cond_3
    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1447
    :cond_4
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1448
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1447
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1449
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    const v2, 0x7f0b0416

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1453
    :pswitch_5
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isWorldMode()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1454
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1455
    const v2, 0x7f0b039d

    .line 1454
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    .line 1456
    invoke-direct {p0, v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->controlCdmaOptions(Z)V

    .line 1457
    invoke-direct {p0, v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->controlGsmOptions(Z)V

    goto/16 :goto_0

    .line 1459
    :cond_5
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1460
    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 1459
    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1461
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1465
    :pswitch_6
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1466
    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1465
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1467
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1472
    :pswitch_7
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1473
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1472
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1474
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1477
    :pswitch_8
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1478
    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1477
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1479
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    const v2, 0x7f0b0415

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1482
    :pswitch_9
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1483
    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1482
    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1484
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1492
    :pswitch_a
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isSupportTdscdma()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1493
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1494
    const/16 v3, 0x16

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 1493
    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1495
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1497
    :cond_6
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isWorldMode()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1498
    invoke-direct {p0, v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->controlCdmaOptions(Z)V

    .line 1499
    invoke-direct {p0, v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->controlGsmOptions(Z)V

    .line 1501
    :cond_7
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 1502
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 1501
    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1503
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_8

    .line 1504
    iget-boolean v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mIsGlobalCdma:Z

    .line 1503
    if-nez v2, :cond_8

    .line 1505
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isWorldMode()Z

    move-result v2

    .line 1503
    if-eqz v2, :cond_9

    .line 1506
    :cond_8
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    const v2, 0x7f0b0416

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1508
    :cond_9
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    iget-boolean v3, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mShow4GForLTE:Z

    if-eqz v3, :cond_a

    .line 1509
    const v1, 0x7f0b0412

    .line 1508
    :cond_a
    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1399
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_5
        :pswitch_3
        :pswitch_a
        :pswitch_4
        :pswitch_4
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_a
        :pswitch_a
        :pswitch_6
        :pswitch_a
    .end packed-switch
.end method

.method private UpdatePreferredNetworkModeSummary(I)V
    .locals 4
    .param p1, "NetworkMode"    # I

    .prologue
    const v3, 0x7f0b039a

    const v2, 0x7f0b0397

    .line 1310
    packed-switch p1, :pswitch_data_0

    .line 1393
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setSummary(I)V

    .line 1396
    :goto_0
    return-void

    .line 1314
    :pswitch_0
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1315
    const v1, 0x7f0b038e

    .line 1314
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1318
    :pswitch_1
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1319
    const v1, 0x7f0b038f

    .line 1318
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1323
    :pswitch_2
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1324
    const v1, 0x7f0b0390

    .line 1323
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1327
    :pswitch_3
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1328
    const v1, 0x7f0b0391

    .line 1327
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1331
    :pswitch_4
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getLteOnCdmaMode()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 1338
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1339
    const v1, 0x7f0b0393

    .line 1338
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1333
    :pswitch_5
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1334
    const v1, 0x7f0b0392

    .line 1333
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1344
    :pswitch_6
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1345
    const v1, 0x7f0b0394

    .line 1344
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1348
    :pswitch_7
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1349
    const v1, 0x7f0b0395

    .line 1348
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1353
    :pswitch_8
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1359
    :pswitch_9
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1360
    const v1, 0x7f0b0398

    .line 1359
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1363
    :pswitch_a
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1364
    const v1, 0x7f0b0399

    .line 1363
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1367
    :pswitch_b
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1368
    const v1, 0x7f0b039e

    .line 1367
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto :goto_0

    .line 1372
    :pswitch_c
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1373
    iget-boolean v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mIsGlobalCdma:Z

    .line 1372
    if-nez v0, :cond_0

    .line 1374
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isWorldMode()Z

    move-result v0

    .line 1372
    if-eqz v0, :cond_1

    .line 1375
    :cond_0
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1378
    :cond_1
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1384
    :pswitch_d
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1385
    const v1, 0x7f0b0396

    .line 1384
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1389
    :pswitch_e
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 1390
    const v1, 0x7f0b039b

    .line 1389
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    goto/16 :goto_0

    .line 1310
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_d
        :pswitch_a
        :pswitch_9
        :pswitch_c
        :pswitch_8
        :pswitch_e
        :pswitch_b
        :pswitch_2
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_e
        :pswitch_9
        :pswitch_d
        :pswitch_c
    .end packed-switch

    .line 1331
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch
.end method

.method private bindNetworkQueryService()V
    .locals 4

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/android/phone/NetworkQueryService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 314
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/android/phone/NetworkQueryService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 315
    const-string/jumbo v2, "com.android.phone.intent.action.LOCAL_BINDER"

    .line 314
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 316
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    .line 314
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 317
    return-void
.end method

.method private buildTabSpec(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 579
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    .line 580
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mEmptyTabContent:Landroid/widget/TabHost$TabContentFactory;

    .line 579
    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    return-object v0
.end method

.method private controlCdmaOptions(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1732
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1733
    .local v0, "prefSet":Landroid/preference/PreferenceScreen;
    if-nez v0, :cond_0

    .line 1734
    return-void

    .line 1736
    :cond_0
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-direct {p0, p0, v0, v2}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateCdmaOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V

    .line 1739
    const-string/jumbo v2, "cdma_system_select_key"

    .line 1738
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/phone/CdmaSystemSelectListPreference;

    .line 1740
    .local v1, "systemSelect":Lcom/android/phone/CdmaSystemSelectListPreference;
    if-eqz v1, :cond_1

    .line 1741
    invoke-virtual {v1, p1}, Lcom/android/phone/CdmaSystemSelectListPreference;->setEnabled(Z)V

    .line 1743
    :cond_1
    return-void
.end method

.method private controlGsmOptions(Z)V
    .locals 7
    .param p1, "enable"    # Z

    .prologue
    const/4 v5, 0x1

    .line 1703
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 1704
    .local v3, "prefSet":Landroid/preference/PreferenceScreen;
    if-nez v3, :cond_0

    .line 1705
    return-void

    .line 1708
    :cond_0
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v4

    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    invoke-direct {p0, p0, v3, v4, v6}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateGsmUmtsOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;ILcom/android/phone/INetworkQueryService;)V

    .line 1711
    const-string/jumbo v4, "category_gsm_apn_key"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 1714
    .local v0, "apnExpand":Landroid/preference/PreferenceCategory;
    const-string/jumbo v4, "network_operators_category_key"

    .line 1713
    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    .line 1715
    .local v2, "networkOperatorCategory":Landroid/preference/PreferenceCategory;
    const-string/jumbo v4, "carrier_settings_key"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1716
    .local v1, "carrierSettings":Landroid/preference/Preference;
    if-eqz v0, :cond_1

    .line 1717
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isWorldMode()Z

    move-result v4

    if-nez v4, :cond_4

    move v4, p1

    :goto_0
    invoke-virtual {v0, v4}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 1719
    :cond_1
    if-eqz v2, :cond_2

    .line 1720
    if-eqz p1, :cond_5

    .line 1721
    invoke-virtual {v2, v5}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 1726
    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    .line 1727
    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 1729
    :cond_3
    return-void

    :cond_4
    move v4, v5

    .line 1717
    goto :goto_0

    .line 1723
    :cond_5
    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method private hasActiveSubscriptions()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 751
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private initializeSubscriptions()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 433
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 434
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 436
    :cond_0
    return-void

    .line 438
    :cond_1
    const/4 v1, 0x0

    .line 439
    .local v1, "currentTab":I
    const-string/jumbo v8, "initializeSubscriptions:+"

    invoke-static {v8}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 443
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v8}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v4

    .line 444
    .local v4, "sil":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubscriptionInfo;>;"
    invoke-direct {p0, v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isUpdateTabsNeeded(Ljava/util/List;)Lcom/android/phone/MobileNetworkSettings$TabState;

    move-result-object v6

    .line 447
    .local v6, "state":Lcom/android/phone/MobileNetworkSettings$TabState;
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 448
    if-eqz v4, :cond_2

    .line 449
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 451
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    .line 452
    const/4 v8, 0x0

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/SubscriptionInfo;

    invoke-virtual {v8}, Landroid/telephony/SubscriptionInfo;->getSimSlotIndex()I

    move-result v1

    .line 456
    :cond_2
    invoke-static {}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->-getcom-android-phone-MobileNetworkSettings$TabStateSwitchesValues()[I

    move-result-object v8

    invoke-virtual {v6}, Lcom/android/phone/MobileNetworkSettings$TabState;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 509
    :cond_3
    :goto_0
    invoke-direct {p0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updatePhone(I)V

    .line 510
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateBody()V

    .line 511
    const-string/jumbo v8, "initializeSubscriptions:-"

    invoke-static {v8}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 512
    return-void

    .line 458
    :pswitch_0
    const-string/jumbo v8, "initializeSubscriptions: UPDATE"

    invoke-static {v8}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 459
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    .line 461
    :goto_1
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x1020012

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TabHost;

    iput-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    .line 462
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->setup()V

    .line 468
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v3

    .line 469
    .local v3, "siIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/telephony/SubscriptionInfo;>;"
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/SubscriptionInfo;

    .line 470
    :goto_2
    const/4 v5, 0x0

    .local v5, "simSlotIndex":I
    :goto_3
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v5, v8, :cond_8

    .line 473
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/telephony/SubscriptionInfo;->getSimSlotIndex()I

    move-result v8

    if-ne v8, v5, :cond_7

    .line 475
    invoke-virtual {v2}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 476
    .local v7, "tabName":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/SubscriptionInfo;

    .line 482
    :goto_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "initializeSubscriptions:tab="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " name="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 485
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9, v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->buildTabSpec(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 471
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 459
    .end local v3    # "siIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/telephony/SubscriptionInfo;>;"
    .end local v5    # "simSlotIndex":I
    .end local v7    # "tabName":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 469
    .restart local v3    # "siIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/telephony/SubscriptionInfo;>;"
    :cond_5
    const/4 v2, 0x0

    .local v2, "si":Landroid/telephony/SubscriptionInfo;
    goto :goto_2

    .line 476
    .end local v2    # "si":Landroid/telephony/SubscriptionInfo;
    .restart local v5    # "simSlotIndex":I
    .restart local v7    # "tabName":Ljava/lang/String;
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "si":Landroid/telephony/SubscriptionInfo;
    goto :goto_4

    .line 479
    .end local v2    # "si":Landroid/telephony/SubscriptionInfo;
    .end local v7    # "tabName":Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0094

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "tabName":Ljava/lang/String;
    goto :goto_4

    .line 488
    .end local v7    # "tabName":Ljava/lang/String;
    :cond_8
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    iget-object v9, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabListener:Landroid/widget/TabHost$OnTabChangeListener;

    invoke-virtual {v8, v9}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 489
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v8, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto/16 :goto_0

    .line 493
    .end local v3    # "siIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/telephony/SubscriptionInfo;>;"
    .end local v5    # "simSlotIndex":I
    :pswitch_1
    const-string/jumbo v8, "initializeSubscriptions: NO_TABS"

    invoke-static {v8}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 495
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    if-eqz v8, :cond_3

    .line 496
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 497
    iput-object v10, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    goto/16 :goto_0

    .line 502
    :pswitch_2
    const-string/jumbo v8, "initializeSubscriptions: DO_NOTHING"

    invoke-static {v8}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 503
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    if-eqz v8, :cond_3

    .line 504
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v8}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    goto/16 :goto_0

    .line 456
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private is4gLtePrefEnabled(Landroid/content/Context;Landroid/os/PersistableBundle;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "carrierConfig"    # Landroid/os/PersistableBundle;

    .prologue
    const/4 v0, 0x0

    .line 1233
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-nez v1, :cond_0

    .line 1234
    invoke-static {p1}, Lcom/android/ims/ImsManager;->isNonTtyOrTtyOnVolteEnabled(Landroid/content/Context;)Z

    move-result v1

    .line 1233
    if-eqz v1, :cond_0

    .line 1236
    const-string/jumbo v0, "editable_enhanced_4g_lte_bool"

    .line 1235
    invoke-virtual {p2, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1233
    :cond_0
    return v0
.end method

.method private isSupportTdscdma()Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 1746
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0e0017

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1747
    return v7

    .line 1750
    :cond_0
    iget-object v3, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getOperatorNumeric()Ljava/lang/String;

    move-result-object v2

    .line 1751
    .local v2, "operatorNumeric":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1752
    const v5, 0x7f07004c

    .line 1751
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 1753
    .local v1, "numericArray":[Ljava/lang/String;
    array-length v3, v1

    if-eqz v3, :cond_1

    if-nez v2, :cond_2

    .line 1754
    :cond_1
    return v4

    .line 1756
    :cond_2
    array-length v5, v1

    move v3, v4

    :goto_0
    if-ge v3, v5, :cond_4

    aget-object v0, v1, v3

    .line 1757
    .local v0, "numeric":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1758
    return v7

    .line 1756
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1761
    .end local v0    # "numeric":Ljava/lang/String;
    :cond_4
    return v4
.end method

.method private isUpdateTabsNeeded(Ljava/util/List;)Lcom/android/phone/MobileNetworkSettings$TabState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/SubscriptionInfo;",
            ">;)",
            "Lcom/android/phone/MobileNetworkSettings$TabState;"
        }
    .end annotation

    .prologue
    .local p1, "newSil":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubscriptionInfo;>;"
    const/4 v6, 0x2

    .line 515
    sget-object v4, Lcom/android/phone/MobileNetworkSettings$TabState;->DO_NOTHING:Lcom/android/phone/MobileNetworkSettings$TabState;

    .line 516
    .local v4, "state":Lcom/android/phone/MobileNetworkSettings$TabState;
    if-nez p1, :cond_1

    .line 517
    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v5, v6, :cond_0

    .line 518
    const-string/jumbo v5, "isUpdateTabsNeeded: NO_TABS, size unknown and was tabbed"

    invoke-static {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 519
    sget-object v4, Lcom/android/phone/MobileNetworkSettings$TabState;->NO_TABS:Lcom/android/phone/MobileNetworkSettings$TabState;

    .line 540
    :cond_0
    :goto_0
    const-string/jumbo v6, "NetworkSettings"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "isUpdateTabsNeeded:- "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 541
    const-string/jumbo v7, " newSil.size()="

    .line 540
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 541
    if-eqz p1, :cond_5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    .line 540
    :goto_1
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 542
    const-string/jumbo v7, " mActiveSubInfos.size()="

    .line 540
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 542
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    .line 540
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    return-object v4

    .line 521
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v5, v6, :cond_2

    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v5, v6, :cond_2

    .line 522
    const-string/jumbo v5, "isUpdateTabsNeeded: NO_TABS, size went to small"

    invoke-static {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 523
    sget-object v4, Lcom/android/phone/MobileNetworkSettings$TabState;->NO_TABS:Lcom/android/phone/MobileNetworkSettings$TabState;

    goto :goto_0

    .line 524
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-lt v5, v6, :cond_3

    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v5, v6, :cond_3

    .line 525
    const-string/jumbo v5, "isUpdateTabsNeeded: UPDATE, size changed"

    invoke-static {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 526
    sget-object v4, Lcom/android/phone/MobileNetworkSettings$TabState;->UPDATE:Lcom/android/phone/MobileNetworkSettings$TabState;

    goto :goto_0

    .line 527
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-lt v5, v6, :cond_0

    .line 528
    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 529
    .local v3, "siIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/telephony/SubscriptionInfo;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "newSi$iterator":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/SubscriptionInfo;

    .line 530
    .local v1, "newSi":Landroid/telephony/SubscriptionInfo;
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    .line 531
    .local v0, "curSi":Landroid/telephony/SubscriptionInfo;
    invoke-virtual {v1}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 532
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "isUpdateTabsNeeded: UPDATE, new name="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 533
    invoke-virtual {v1}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v6

    .line 532
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 534
    sget-object v4, Lcom/android/phone/MobileNetworkSettings$TabState;->UPDATE:Lcom/android/phone/MobileNetworkSettings$TabState;

    goto/16 :goto_0

    .line 541
    .end local v0    # "curSi":Landroid/telephony/SubscriptionInfo;
    .end local v1    # "newSi":Landroid/telephony/SubscriptionInfo;
    .end local v2    # "newSi$iterator":Ljava/util/Iterator;
    .end local v3    # "siIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/telephony/SubscriptionInfo;>;"
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_1
.end method

.method private isWorldMode()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1680
    const/4 v2, 0x0

    .line 1681
    .local v2, "worldModeOn":Z
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b04fe

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1683
    .local v1, "configString":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1684
    const-string/jumbo v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1687
    .local v0, "configArray":[Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 1688
    array-length v3, v0

    if-ne v3, v5, :cond_0

    const/4 v3, 0x0

    aget-object v3, v0, v3

    const-string/jumbo v4, "true"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1689
    :cond_0
    array-length v3, v0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    aget-object v3, v0, v5

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    .line 1690
    iget-object v3, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v3, :cond_2

    .line 1691
    aget-object v3, v0, v5

    .line 1692
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getGroupIdLevel1()Ljava/lang/String;

    move-result-object v4

    .line 1691
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    .line 1687
    if-eqz v3, :cond_2

    .line 1693
    :cond_1
    const/4 v2, 0x1

    .line 1697
    .end local v0    # "configArray":[Ljava/lang/String;
    :cond_2
    const-string/jumbo v3, "NetworkSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "isWorldMode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1699
    return v2
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 1651
    const-string/jumbo v0, "NetworkSettings"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1652
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 1655
    const-string/jumbo v0, "NetworkSettings"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1656
    return-void
.end method

.method private sendMetricsEventPreferenceClicked(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)V
    .locals 3
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 1766
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mMobileDataPref:Lcom/android/phone/MobileDataPreference;

    if-ne p2, v0, :cond_1

    .line 1767
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1769
    check-cast p2, Lcom/android/phone/MobileDataPreference;

    .end local p2    # "preference":Landroid/preference/Preference;
    iget-boolean v1, p2, Lcom/android/phone/MobileDataPreference;->mChecked:Z

    .line 1768
    const/16 v2, 0x439

    .line 1767
    invoke-static {v0, v2, v1}, Lcom/android/internal/logging/MetricsLogger;->action(Landroid/content/Context;IZ)V

    .line 1775
    :cond_0
    :goto_0
    return-void

    .line 1770
    .restart local p2    # "preference":Landroid/preference/Preference;
    :cond_1
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mDataUsagePref:Lcom/android/phone/DataUsagePreference;

    if-ne p2, v0, :cond_0

    .line 1771
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1772
    const/16 v1, 0x43a

    .line 1771
    invoke-static {v0, v1}, Lcom/android/internal/logging/MetricsLogger;->action(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private setNetworkQueryService()V
    .locals 2

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 288
    const-string/jumbo v1, "button_network_select_key"

    .line 287
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/phone/NetworkSelectListPreference;

    iput-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    .line 289
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonNetworkSelect:Lcom/android/phone/NetworkSelectListPreference;

    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    invoke-virtual {v0, v1}, Lcom/android/phone/NetworkSelectListPreference;->setNetworkQueryService(Lcom/android/phone/INetworkQueryService;)V

    .line 293
    :cond_0
    return-void
.end method

.method private unbindNetworkQueryService()V
    .locals 2

    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 322
    return-void
.end method

.method private updateBody()V
    .locals 6

    .prologue
    .line 791
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 792
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 793
    .local v3, "prefSet":Landroid/preference/PreferenceScreen;
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    .line 794
    .local v2, "phoneSubId":I
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->hasActiveSubscriptions()Z

    move-result v1

    .line 796
    .local v1, "hasActiveSubscriptions":Z
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 797
    :cond_0
    const-string/jumbo v4, "NetworkSettings"

    const-string/jumbo v5, "updateBody with no valid activity."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    return-void

    .line 801
    :cond_1
    if-nez v3, :cond_2

    .line 802
    const-string/jumbo v4, "NetworkSettings"

    const-string/jumbo v5, "updateBody with no null prefSet."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    return-void

    .line 806
    :cond_2
    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 808
    invoke-direct {p0, v0, v3, v2, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateBodyBasicFields(Landroid/app/Activity;Landroid/preference/PreferenceScreen;IZ)V

    .line 810
    iget-boolean v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mExpandAdvancedFields:Z

    if-eqz v4, :cond_3

    .line 811
    invoke-direct {p0, v0, v3, v2, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateBodyAdvancedFields(Landroid/app/Activity;Landroid/preference/PreferenceScreen;IZ)V

    .line 815
    :goto_0
    return-void

    .line 813
    :cond_3
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mAdvancedOptions:Lcom/android/phone/AdvancedOptionsPreference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private updateBodyAdvancedFields(Landroid/app/Activity;Landroid/preference/PreferenceScreen;IZ)V
    .locals 23
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "prefSet"    # Landroid/preference/PreferenceScreen;
    .param p3, "phoneSubId"    # I
    .param p4, "hasActiveSubscriptions"    # Z

    .prologue
    .line 819
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/Phone;->getLteOnCdmaMode()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    const/4 v9, 0x1

    .line 822
    .local v9, "isLteOnCdma":Z
    :goto_0
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "updateBody: isLteOnCdma="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string/jumbo v21, " phoneSubId="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 825
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 826
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButton4glte:Landroid/preference/SwitchPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 829
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/android/phone/MobileNetworkSettings;->showEuiccSettings(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 830
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mEuiccSettingsPref:Landroid/preference/Preference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 831
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v18

    .line 832
    .local v18, "spn":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 833
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mEuiccSettingsPref:Landroid/preference/Preference;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 840
    .end local v18    # "spn":Ljava/lang/String;
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    .line 841
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "preferred_network_mode"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 842
    sget v22, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->preferredNetworkMode:I

    .line 839
    invoke-static/range {v20 .. v22}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v17

    .line 845
    .local v17, "settingsNetworkMode":I
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v6

    .line 846
    .local v6, "carrierConfig":Landroid/os/PersistableBundle;
    if-eqz v9, :cond_e

    .line 847
    const-string/jumbo v20, "show_cdma_choices_bool"

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    .line 846
    :goto_2
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mIsGlobalCdma:Z

    .line 849
    const-string/jumbo v20, "hide_carrier_network_settings_bool"

    .line 848
    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 851
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 852
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mLteDataServicePref:Landroid/preference/Preference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 978
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    .line 979
    const-string/jumbo v21, "setup_prepaid_data_service_url"

    .line 978
    invoke-static/range {v20 .. v21}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 977
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    .line 980
    .local v11, "missingDataServiceUrl":Z
    if-eqz v9, :cond_1

    if-eqz v11, :cond_20

    .line 981
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mLteDataServicePref:Landroid/preference/Preference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 986
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v0, v6}, Lcom/android/phone/MobileNetworkSettings;->hideEnhanced4gLteSettings(Landroid/content/Context;Landroid/os/PersistableBundle;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 987
    const-string/jumbo v20, "enhanced_4g_lte"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v13

    .line 988
    .local v13, "pref":Landroid/preference/Preference;
    if-eqz v13, :cond_2

    .line 989
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 993
    .end local v13    # "pref":Landroid/preference/Preference;
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateCallingCategory()V

    .line 996
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 997
    const v21, 0x1120035

    .line 996
    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    .line 998
    .local v8, "isCellBroadcastAppLinkEnabled":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mUm:Landroid/os/UserManager;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v20

    if-eqz v20, :cond_3

    xor-int/lit8 v20, v8, 0x1

    if-nez v20, :cond_3

    .line 999
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mUm:Landroid/os/UserManager;

    move-object/from16 v20, v0

    const-string/jumbo v21, "no_config_cell_broadcasts"

    invoke-virtual/range {v20 .. v21}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v20

    .line 998
    if-eqz v20, :cond_4

    .line 1000
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    .line 1001
    .local v15, "root":Landroid/preference/PreferenceScreen;
    const-string/jumbo v20, "cell_broadcast_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    .line 1002
    .local v14, "ps":Landroid/preference/Preference;
    if-eqz v14, :cond_4

    .line 1003
    invoke-virtual {v15, v14}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 1008
    .end local v14    # "ps":Landroid/preference/Preference;
    .end local v15    # "root":Landroid/preference/PreferenceScreen;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1009
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1010
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->UpdatePreferredNetworkModeSummary(I)V

    .line 1011
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->UpdateEnabledNetworksValueAndSummary(I)V

    .line 1013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mHandler:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;

    move-object/from16 v21, v0

    .line 1014
    const/16 v22, 0x0

    .line 1013
    invoke-virtual/range {v21 .. v22}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v21

    move-object/from16 v0, v20

    move/from16 v1, v17

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    .line 1025
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v6}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->is4gLtePrefEnabled(Landroid/content/Context;Landroid/os/PersistableBundle;)Z

    move-result v5

    .line 1028
    .local v5, "canChange4glte":Z
    const-string/jumbo v20, "enhanced_4g_lte_title_variant_bool"

    .line 1027
    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v19

    .line 1029
    .local v19, "useVariant4glteTitle":Z
    if-eqz v19, :cond_21

    .line 1030
    const v7, 0x7f0b03a4

    .line 1033
    .local v7, "enhanced4glteModeTitleId":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 1034
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 1035
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButton4glte:Landroid/preference/SwitchPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/preference/SwitchPreference;->setTitle(I)V

    .line 1036
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButton4glte:Landroid/preference/SwitchPreference;

    move-object/from16 v20, v0

    if-eqz p4, :cond_22

    .end local v5    # "canChange4glte":Z
    :goto_6
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 1037
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mLteDataServicePref:Landroid/preference/Preference;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1039
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v15

    .line 1040
    .restart local v15    # "root":Landroid/preference/PreferenceScreen;
    const-string/jumbo v20, "cell_broadcast_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    .line 1041
    .restart local v14    # "ps":Landroid/preference/Preference;
    if-eqz v14, :cond_5

    .line 1042
    move/from16 v0, p4

    invoke-virtual {v14, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1044
    :cond_5
    const-string/jumbo v20, "category_gsm_apn_key"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    .line 1045
    if-eqz v14, :cond_6

    .line 1046
    move/from16 v0, p4

    invoke-virtual {v14, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1048
    :cond_6
    const-string/jumbo v20, "category_cdma_apn_key"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    .line 1049
    if-eqz v14, :cond_7

    .line 1050
    move/from16 v0, p4

    invoke-virtual {v14, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1052
    :cond_7
    const-string/jumbo v20, "network_operators_category_key"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    .line 1053
    if-eqz v14, :cond_8

    .line 1054
    move/from16 v0, p4

    invoke-virtual {v14, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1056
    :cond_8
    const-string/jumbo v20, "carrier_settings_key"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    .line 1057
    if-eqz v14, :cond_9

    .line 1058
    move/from16 v0, p4

    invoke-virtual {v14, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1060
    :cond_9
    const-string/jumbo v20, "cdma_system_select_key"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    .line 1061
    if-eqz v14, :cond_a

    .line 1062
    move/from16 v0, p4

    invoke-virtual {v14, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1064
    :cond_a
    const-string/jumbo v20, "calling"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    .line 1065
    if-eqz v14, :cond_b

    .line 1066
    move/from16 v0, p4

    invoke-virtual {v14, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1068
    :cond_b
    return-void

    .line 819
    .end local v6    # "carrierConfig":Landroid/os/PersistableBundle;
    .end local v7    # "enhanced4glteModeTitleId":I
    .end local v8    # "isCellBroadcastAppLinkEnabled":Z
    .end local v9    # "isLteOnCdma":Z
    .end local v11    # "missingDataServiceUrl":Z
    .end local v14    # "ps":Landroid/preference/Preference;
    .end local v15    # "root":Landroid/preference/PreferenceScreen;
    .end local v17    # "settingsNetworkMode":I
    .end local v19    # "useVariant4glteTitle":Z
    :cond_c
    const/4 v9, 0x0

    .restart local v9    # "isLteOnCdma":Z
    goto/16 :goto_0

    .line 835
    .restart local v18    # "spn":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mEuiccSettingsPref:Landroid/preference/Preference;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 846
    .end local v18    # "spn":Ljava/lang/String;
    .restart local v6    # "carrierConfig":Landroid/os/PersistableBundle;
    .restart local v17    # "settingsNetworkMode":I
    :cond_e
    const/16 v20, 0x0

    goto/16 :goto_2

    .line 853
    :cond_f
    const-string/jumbo v20, "hide_preferred_network_type_bool"

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_12

    .line 855
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v20

    xor-int/lit8 v20, v20, 0x1

    .line 853
    if-eqz v20, :cond_12

    .line 856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/telephony/ServiceState;->getDataRegState()I

    move-result v20

    if-nez v20, :cond_12

    .line 858
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 859
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v12

    .line 862
    .local v12, "phoneType":I
    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v12, v0, :cond_10

    .line 863
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateCdmaOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V

    .line 872
    :goto_7
    sget v17, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->preferredNetworkMode:I

    goto/16 :goto_3

    .line 864
    :cond_10
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v12, v0, :cond_11

    .line 865
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, v20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateGsmUmtsOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;ILcom/android/phone/INetworkQueryService;)V

    goto :goto_7

    .line 867
    :cond_11
    new-instance v20, Ljava/lang/IllegalStateException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Unexpected phone type: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 874
    .end local v12    # "phoneType":I
    :cond_12
    const-string/jumbo v20, "world_phone_bool"

    .line 873
    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_13

    .line 875
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 880
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateCdmaOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V

    .line 881
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, v20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateGsmUmtsOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;ILcom/android/phone/INetworkQueryService;)V

    goto/16 :goto_3

    .line 883
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 884
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v12

    .line 885
    .restart local v12    # "phoneType":I
    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v12, v0, :cond_17

    .line 887
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    .line 888
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "lte_service_forced"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 889
    const/16 v22, 0x0

    .line 886
    invoke-static/range {v20 .. v22}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v10

    .line 891
    .local v10, "lteForced":I
    if-eqz v9, :cond_14

    .line 892
    if-nez v10, :cond_16

    .line 893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 894
    const v21, 0x7f07005d

    .line 893
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 895
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 896
    const v21, 0x7f07005e

    .line 895
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 925
    :cond_14
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateCdmaOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V

    .line 967
    .end local v10    # "lteForced":I
    :goto_9
    invoke-direct/range {p0 .. p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isWorldMode()Z

    move-result v20

    if-eqz v20, :cond_15

    .line 968
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 969
    const v21, 0x7f070046

    .line 968
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 970
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 971
    const v21, 0x7f070047

    .line 970
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 973
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 974
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "settingsNetworkMode: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 898
    .restart local v10    # "lteForced":I
    :cond_16
    packed-switch v17, :pswitch_data_0

    .line 917
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 918
    const v21, 0x7f07005d

    .line 917
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 919
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 920
    const v21, 0x7f07005e

    .line 919
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto :goto_8

    .line 902
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 903
    const v21, 0x7f07005f

    .line 902
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 904
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 905
    const v21, 0x7f070060

    .line 904
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto/16 :goto_8

    .line 911
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 912
    const v21, 0x7f070061

    .line 911
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 913
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 914
    const v21, 0x7f070062

    .line 913
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto/16 :goto_8

    .line 927
    .end local v10    # "lteForced":I
    :cond_17
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v12, v0, :cond_1f

    .line 928
    invoke-direct/range {p0 .. p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->isSupportTdscdma()Z

    move-result v20

    if-eqz v20, :cond_18

    .line 929
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 930
    const v21, 0x7f070063

    .line 929
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 931
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 932
    const v21, 0x7f070064

    .line 931
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 963
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mNetworkQueryService:Lcom/android/phone/INetworkQueryService;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, v20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateGsmUmtsOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;ILcom/android/phone/INetworkQueryService;)V

    goto/16 :goto_9

    .line 933
    :cond_18
    const-string/jumbo v20, "prefer_2g_bool"

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_19

    .line 934
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0e0016

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v20

    xor-int/lit8 v20, v20, 0x1

    .line 933
    if-eqz v20, :cond_19

    .line 935
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 936
    const v21, 0x7f070058

    .line 935
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 937
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 938
    const v21, 0x7f070059

    .line 937
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto :goto_a

    .line 939
    :cond_19
    const-string/jumbo v20, "prefer_2g_bool"

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_1b

    .line 940
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mShow4GForLTE:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1a

    .line 941
    const v16, 0x7f070054

    .line 943
    .local v16, "select":I
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 945
    const v21, 0x7f070055

    .line 944
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto :goto_a

    .line 942
    .end local v16    # "select":I
    :cond_1a
    const v16, 0x7f070053

    .restart local v16    # "select":I
    goto :goto_b

    .line 946
    .end local v16    # "select":I
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0e0016

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v20

    if-nez v20, :cond_1c

    .line 947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 948
    const v21, 0x7f070056

    .line 947
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 949
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 950
    const v21, 0x7f070057

    .line 949
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto/16 :goto_a

    .line 951
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mIsGlobalCdma:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1d

    .line 952
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 953
    const v21, 0x7f07005d

    .line 952
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 954
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 955
    const v21, 0x7f07005e

    .line 954
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto/16 :goto_a

    .line 957
    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mShow4GForLTE:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1e

    const v16, 0x7f07005b

    .line 959
    .restart local v16    # "select":I
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object/from16 v20, v0

    .line 961
    const v21, 0x7f07005c

    .line 960
    invoke-virtual/range {v20 .. v21}, Landroid/preference/ListPreference;->setEntryValues(I)V

    goto/16 :goto_a

    .line 958
    .end local v16    # "select":I
    :cond_1e
    const v16, 0x7f07005a

    .restart local v16    # "select":I
    goto :goto_c

    .line 965
    .end local v16    # "select":I
    :cond_1f
    new-instance v20, Ljava/lang/IllegalStateException;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "Unexpected phone type: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 983
    .end local v12    # "phoneType":I
    .restart local v11    # "missingDataServiceUrl":Z
    :cond_20
    const-string/jumbo v20, "NetworkSettings"

    const-string/jumbo v21, "keep ltePref"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1031
    .restart local v5    # "canChange4glte":Z
    .restart local v8    # "isCellBroadcastAppLinkEnabled":Z
    .restart local v19    # "useVariant4glteTitle":Z
    :cond_21
    const v7, 0x7f0b03a3

    .restart local v7    # "enhanced4glteModeTitleId":I
    goto/16 :goto_5

    .line 1036
    :cond_22
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 898
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private updateBodyBasicFields(Landroid/app/Activity;Landroid/preference/PreferenceScreen;IZ)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "prefSet"    # Landroid/preference/PreferenceScreen;
    .param p3, "phoneSubId"    # I
    .param p4, "hasActiveSubscriptions"    # Z

    .prologue
    const/4 v4, 0x0

    .line 756
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 758
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 759
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 761
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 764
    :cond_0
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mMobileDataPref:Lcom/android/phone/MobileDataPreference;

    invoke-virtual {p2, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 765
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    invoke-virtual {p2, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 766
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mDataUsagePref:Lcom/android/phone/DataUsagePreference;

    invoke-virtual {p2, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 769
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mMobileDataPref:Lcom/android/phone/MobileDataPreference;

    invoke-virtual {v2, p3}, Lcom/android/phone/MobileDataPreference;->initialize(I)V

    .line 770
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mDataUsagePref:Lcom/android/phone/DataUsagePreference;

    invoke-virtual {v2, p3}, Lcom/android/phone/DataUsagePreference;->initialize(I)V

    .line 772
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mMobileDataPref:Lcom/android/phone/MobileDataPreference;

    invoke-virtual {v2, p4}, Lcom/android/phone/MobileDataPreference;->setEnabled(Z)V

    .line 773
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    invoke-virtual {v2, p4}, Lcom/android/phone/RestrictedSwitchPreference;->setEnabled(Z)V

    .line 774
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mDataUsagePref:Lcom/android/phone/DataUsagePreference;

    invoke-virtual {v2, p4}, Lcom/android/phone/DataUsagePreference;->setEnabled(Z)V

    .line 777
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    iget-object v3, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getDataRoamingEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/phone/RestrictedSwitchPreference;->setChecked(Z)V

    .line 778
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    invoke-virtual {v2, v4}, Lcom/android/phone/RestrictedSwitchPreference;->setDisabledByAdmin(Z)V

    .line 779
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    invoke-virtual {v2}, Lcom/android/phone/RestrictedSwitchPreference;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 781
    const-string/jumbo v2, "no_data_roaming"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    .line 780
    invoke-static {v1, v2, v3}, Lcom/android/settingslib/RestrictedLockUtils;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 782
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    invoke-virtual {v2, v4}, Lcom/android/phone/RestrictedSwitchPreference;->setEnabled(Z)V

    .line 788
    :cond_1
    :goto_0
    return-void

    .line 784
    :cond_2
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    .line 785
    const-string/jumbo v3, "no_data_roaming"

    .line 784
    invoke-virtual {v2, v3}, Lcom/android/phone/RestrictedSwitchPreference;->checkRestrictionAndSetDisabled(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateCallingCategory()V
    .locals 2

    .prologue
    .line 1633
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    if-nez v0, :cond_0

    .line 1634
    return-void

    .line 1637
    :cond_0
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateWiFiCallState()V

    .line 1638
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateVideoCallState()V

    .line 1643
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1644
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 1648
    :goto_0
    return-void

    .line 1646
    :cond_1
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method private updateCdmaOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V
    .locals 1
    .param p1, "prefFragment"    # Landroid/preference/PreferenceFragment;
    .param p2, "prefScreen"    # Landroid/preference/PreferenceScreen;
    .param p3, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 1794
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCdmaOptions:Lcom/android/phone/CdmaOptions;

    if-nez v0, :cond_0

    .line 1795
    new-instance v0, Lcom/android/phone/CdmaOptions;

    invoke-direct {v0, p1, p2, p3}, Lcom/android/phone/CdmaOptions;-><init>(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;Lcom/android/internal/telephony/Phone;)V

    iput-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCdmaOptions:Lcom/android/phone/CdmaOptions;

    .line 1799
    :goto_0
    return-void

    .line 1797
    :cond_0
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCdmaOptions:Lcom/android/phone/CdmaOptions;

    invoke-virtual {v0, p3}, Lcom/android/phone/CdmaOptions;->update(Lcom/android/internal/telephony/Phone;)V

    goto :goto_0
.end method

.method private updateGsmUmtsOptions(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;ILcom/android/phone/INetworkQueryService;)V
    .locals 1
    .param p1, "prefFragment"    # Landroid/preference/PreferenceFragment;
    .param p2, "prefScreen"    # Landroid/preference/PreferenceScreen;
    .param p3, "subId"    # I
    .param p4, "queryService"    # Lcom/android/phone/INetworkQueryService;

    .prologue
    .line 1782
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mGsmUmtsOptions:Lcom/android/phone/GsmUmtsOptions;

    if-nez v0, :cond_0

    .line 1783
    new-instance v0, Lcom/android/phone/GsmUmtsOptions;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/android/phone/GsmUmtsOptions;-><init>(Landroid/preference/PreferenceFragment;Landroid/preference/PreferenceScreen;ILcom/android/phone/INetworkQueryService;)V

    iput-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mGsmUmtsOptions:Lcom/android/phone/GsmUmtsOptions;

    .line 1787
    :goto_0
    return-void

    .line 1785
    :cond_0
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mGsmUmtsOptions:Lcom/android/phone/GsmUmtsOptions;

    invoke-virtual {v0, p3, p4}, Lcom/android/phone/GsmUmtsOptions;->update(ILcom/android/phone/INetworkQueryService;)V

    goto :goto_0
.end method

.method private updatePhone(I)V
    .locals 4
    .param p1, "slotId"    # I

    .prologue
    .line 558
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v1, p1}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoForSimSlotIndex(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v0

    .line 560
    .local v0, "sir":Landroid/telephony/SubscriptionInfo;
    if-eqz v0, :cond_0

    .line 562
    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v1

    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v1

    .line 561
    invoke-static {v1}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 564
    :cond_0
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    if-nez v1, :cond_1

    .line 566
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 568
    :cond_1
    const-string/jumbo v1, "NetworkSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePhone:- slotId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " sir="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    return-void
.end method

.method private updatePreferredNetworkUIFromDb()V
    .locals 5

    .prologue
    .line 1291
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v0

    .line 1294
    .local v0, "phoneSubId":I
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1295
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "preferred_network_mode"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1296
    sget v4, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->preferredNetworkMode:I

    .line 1293
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 1299
    .local v1, "settingsNetworkMode":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePreferredNetworkUIFromDb: settingsNetworkMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 1303
    invoke-direct {p0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->UpdatePreferredNetworkModeSummary(I)V

    .line 1304
    invoke-direct {p0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->UpdateEnabledNetworksValueAndSummary(I)V

    .line 1306
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1307
    return-void
.end method

.method private updateVideoCallState()V
    .locals 6

    .prologue
    .line 1598
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    if-nez v4, :cond_1

    .line 1599
    :cond_0
    return-void

    .line 1602
    :cond_1
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v4

    .line 1603
    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v5

    .line 1602
    invoke-virtual {v4, v5}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    .line 1605
    .local v0, "carrierConfig":Landroid/os/PersistableBundle;
    const/4 v3, 0x0

    .line 1607
    .local v3, "removePref":Z
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/ims/ImsManager;->isVtEnabledByPlatform(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1608
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/android/ims/ImsManager;->isVtProvisionedOnDevice(Landroid/content/Context;)Z

    move-result v4

    .line 1607
    if-eqz v4, :cond_5

    .line 1610
    const-string/jumbo v4, "ignore_data_enabled_changed_for_video_calls"

    .line 1609
    invoke-virtual {v0, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1611
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v4, v4, Lcom/android/internal/telephony/Phone;->mDcTracker:Lcom/android/internal/telephony/dataconnection/DcTracker;

    invoke-virtual {v4}, Lcom/android/internal/telephony/dataconnection/DcTracker;->isDataEnabled()Z

    move-result v4

    .line 1607
    if-eqz v4, :cond_5

    .line 1612
    :cond_2
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButton4glte:Landroid/preference/SwitchPreference;

    invoke-virtual {v4}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v2

    .line 1613
    .local v2, "enhanced4gLteEnabled":Z
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    invoke-virtual {v4, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 1614
    if-eqz v2, :cond_4

    .line 1615
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v4

    iget-object v4, v4, Lcom/android/phone/PhoneGlobals;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    .line 1616
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getOpPackageName()Ljava/lang/String;

    move-result-object v5

    .line 1615
    invoke-virtual {v4, v5}, Lcom/android/phone/PhoneInterfaceManager;->isVideoCallingEnabled(Ljava/lang/String;)Z

    move-result v1

    .line 1617
    :goto_0
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    invoke-virtual {v4, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 1618
    if-eqz v2, :cond_3

    .line 1619
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    invoke-virtual {v4, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1625
    .end local v2    # "enhanced4gLteEnabled":Z
    :cond_3
    :goto_1
    if-eqz v3, :cond_6

    .line 1626
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 1630
    :goto_2
    return-void

    .line 1616
    .restart local v2    # "enhanced4gLteEnabled":Z
    :cond_4
    const/4 v1, 0x0

    .local v1, "currentValue":Z
    goto :goto_0

    .line 1622
    .end local v1    # "currentValue":Z
    .end local v2    # "enhanced4gLteEnabled":Z
    :cond_5
    const/4 v3, 0x1

    goto :goto_1

    .line 1628
    :cond_6
    iget-object v4, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    iget-object v5, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_2
.end method

.method private updateWiFiCallState()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 1540
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mWiFiCallingPref:Landroid/preference/Preference;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    if-nez v8, :cond_1

    .line 1541
    :cond_0
    return-void

    .line 1544
    :cond_1
    const/4 v3, 0x0

    .line 1546
    .local v3, "removePref":Z
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/telecom/TelecomManager;->getSimCallManager()Landroid/telecom/PhoneAccountHandle;

    move-result-object v6

    .line 1548
    .local v6, "simCallManager":Landroid/telecom/PhoneAccountHandle;
    if-eqz v6, :cond_4

    .line 1550
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 1549
    invoke-static {v8, v6}, Lcom/android/phone/settings/PhoneAccountSettingsFragment;->buildPhoneAccountConfigureIntent(Landroid/content/Context;Landroid/telecom/PhoneAccountHandle;)Landroid/content/Intent;

    move-result-object v0

    .line 1551
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_3

    .line 1552
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1553
    .local v2, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v2, v0, v11}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 1554
    .local v5, "resolutions":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 1555
    iget-object v9, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mWiFiCallingPref:Landroid/preference/Preference;

    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v8, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v9, v8}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1556
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mWiFiCallingPref:Landroid/preference/Preference;

    invoke-virtual {v8, v10}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1557
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mWiFiCallingPref:Landroid/preference/Preference;

    invoke-virtual {v8, v0}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1590
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    .end local v5    # "resolutions":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :goto_0
    if-eqz v3, :cond_8

    .line 1591
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    iget-object v9, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mWiFiCallingPref:Landroid/preference/Preference;

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 1595
    :goto_1
    return-void

    .line 1559
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v2    # "pm":Landroid/content/pm/PackageManager;
    .restart local v5    # "resolutions":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 1562
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    .end local v5    # "resolutions":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_3
    const/4 v3, 0x1

    goto :goto_0

    .line 1564
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/android/ims/ImsManager;->isWfcEnabledByPlatform(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1565
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/android/ims/ImsManager;->isWfcProvisionedOnDevice(Landroid/content/Context;)Z

    move-result v8

    xor-int/lit8 v8, v8, 0x1

    .line 1564
    if-eqz v8, :cond_6

    .line 1566
    :cond_5
    const/4 v3, 0x1

    goto :goto_0

    .line 1568
    :cond_6
    const v4, 0x10406bd

    .line 1569
    .local v4, "resId":I
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/android/ims/ImsManager;->isWfcEnabledByUser(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1570
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    invoke-virtual {v8}, Lcom/android/phone/RestrictedSwitchPreference;->isChecked()Z

    move-result v1

    .line 1571
    .local v1, "isRoaming":Z
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v8}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v1}, Lcom/android/ims/ImsManager;->getWfcMode(Landroid/content/Context;Z)I

    move-result v7

    .line 1572
    .local v7, "wfcMode":I
    packed-switch v7, :pswitch_data_0

    .line 1584
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Unexpected WFC mode value: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 1587
    .end local v1    # "isRoaming":Z
    .end local v7    # "wfcMode":I
    :cond_7
    :goto_2
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mWiFiCallingPref:Landroid/preference/Preference;

    invoke-virtual {v8, v4}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0

    .line 1574
    .restart local v1    # "isRoaming":Z
    .restart local v7    # "wfcMode":I
    :pswitch_0
    const v4, 0x104069c

    .line 1575
    goto :goto_2

    .line 1577
    :pswitch_1
    const v4, 0x104069b

    .line 1579
    goto :goto_2

    .line 1581
    :pswitch_2
    const v4, 0x104069d

    .line 1582
    goto :goto_2

    .line 1593
    .end local v1    # "isRoaming":Z
    .end local v4    # "resId":I
    .end local v7    # "wfcMode":I
    :cond_8
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    iget-object v9, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mWiFiCallingPref:Landroid/preference/Preference;

    invoke-virtual {v8, v9}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 1572
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 680
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 681
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->initializeSubscriptions()V

    .line 682
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1522
    packed-switch p1, :pswitch_data_0

    .line 1537
    :cond_0
    :goto_0
    return-void

    .line 1525
    :pswitch_0
    const-string/jumbo v1, "exit_ecm_result"

    const/4 v2, 0x0

    .line 1524
    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1526
    .local v0, "isChoiceYes":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1528
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCdmaOptions:Lcom/android/phone/CdmaOptions;

    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mClickedPreference:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Lcom/android/phone/CdmaOptions;->showDialog(Landroid/preference/Preference;)V

    goto :goto_0

    .line 1522
    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    const/4 v11, 0x0

    .line 594
    const-string/jumbo v7, "NetworkSettings"

    const-string/jumbo v8, "onCreate:+"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 597
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 598
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 599
    :cond_0
    const-string/jumbo v7, "NetworkSettings"

    const-string/jumbo v8, "onCreate:- with no valid activity."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    return-void

    .line 603
    :cond_1
    new-instance v7, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;

    invoke-direct {v7, p0, v9}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;-><init>(Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;)V

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mHandler:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;

    .line 604
    const-string/jumbo v7, "user"

    invoke-virtual {v0, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/UserManager;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mUm:Landroid/os/UserManager;

    .line 605
    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v7

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    .line 607
    const-string/jumbo v7, "phone"

    .line 606
    invoke-virtual {v0, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 609
    if-eqz p1, :cond_2

    .line 610
    const-string/jumbo v7, "expand_advanced_fields"

    invoke-virtual {p1, v7, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mExpandAdvancedFields:Z

    .line 613
    :cond_2
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->bindNetworkQueryService()V

    .line 615
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mUm:Landroid/os/UserManager;

    const-string/jumbo v8, "no_config_mobile_networks"

    invoke-virtual {v7, v8}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 616
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mUnavailable:Z

    .line 617
    const v7, 0x7f040066

    invoke-virtual {v0, v7}, Landroid/app/Activity;->setContentView(I)V

    .line 618
    return-void

    .line 621
    :cond_3
    const v7, 0x7f060028

    invoke-virtual {p0, v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->addPreferencesFromResource(I)V

    .line 623
    const-string/jumbo v7, "enhanced_4g_lte"

    invoke-virtual {p0, v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/SwitchPreference;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButton4glte:Landroid/preference/SwitchPreference;

    .line 624
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButton4glte:Landroid/preference/SwitchPreference;

    invoke-virtual {v7, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 626
    const-string/jumbo v7, "calling"

    invoke-virtual {p0, v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceCategory;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCallingCategory:Landroid/preference/PreferenceCategory;

    .line 627
    const-string/jumbo v7, "wifi_calling_key"

    invoke-virtual {p0, v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mWiFiCallingPref:Landroid/preference/Preference;

    .line 628
    const-string/jumbo v7, "video_calling_key"

    invoke-virtual {p0, v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/SwitchPreference;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    .line 629
    const-string/jumbo v7, "mobile_data_enable"

    invoke-virtual {p0, v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Lcom/android/phone/MobileDataPreference;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mMobileDataPref:Lcom/android/phone/MobileDataPreference;

    .line 630
    const-string/jumbo v7, "data_usage_summary"

    invoke-virtual {p0, v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Lcom/android/phone/DataUsagePreference;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mDataUsagePref:Lcom/android/phone/DataUsagePreference;

    .line 633
    :try_start_0
    const-string/jumbo v7, "com.android.systemui"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/app/Activity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v1

    .line 634
    .local v1, "con":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const-string/jumbo v8, "config_show4GForLTE"

    .line 635
    const-string/jumbo v9, "bool"

    const-string/jumbo v10, "com.android.systemui"

    .line 634
    invoke-virtual {v7, v8, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 636
    .local v3, "id":I
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    iput-boolean v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mShow4GForLTE:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 643
    .end local v1    # "con":Landroid/content/Context;
    .end local v3    # "id":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    .line 646
    .local v6, "prefSet":Landroid/preference/PreferenceScreen;
    const-string/jumbo v7, "button_roaming_key"

    .line 645
    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Lcom/android/phone/RestrictedSwitchPreference;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    .line 648
    const-string/jumbo v7, "preferred_network_mode_key"

    .line 647
    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/ListPreference;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    .line 650
    const-string/jumbo v7, "enabled_networks_key"

    .line 649
    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/ListPreference;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    .line 652
    const-string/jumbo v7, "advanced_options"

    .line 651
    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Lcom/android/phone/AdvancedOptionsPreference;

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mAdvancedOptions:Lcom/android/phone/AdvancedOptionsPreference;

    .line 653
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    invoke-virtual {v7, p0}, Lcom/android/phone/RestrictedSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 655
    const-string/jumbo v7, "cdma_lte_data_service_key"

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mLteDataServicePref:Landroid/preference/Preference;

    .line 657
    const-string/jumbo v7, "carrier_settings_euicc_key"

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mEuiccSettingsPref:Landroid/preference/Preference;

    .line 658
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mEuiccSettingsPref:Landroid/preference/Preference;

    invoke-virtual {v7, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 661
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v7}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoCountMax()I

    move-result v5

    .line 662
    .local v5, "max":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mActiveSubInfos:Ljava/util/List;

    .line 664
    new-instance v4, Landroid/content/IntentFilter;

    .line 665
    const-string/jumbo v7, "android.intent.action.RADIO_TECHNOLOGY"

    .line 664
    invoke-direct {v4, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 666
    .local v4, "intentFilter":Landroid/content/IntentFilter;
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhoneChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v7, v4}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 668
    const-string/jumbo v7, "NetworkSettings"

    const-string/jumbo v8, "onCreate:-"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    return-void

    .line 637
    .end local v4    # "intentFilter":Landroid/content/IntentFilter;
    .end local v5    # "max":I
    .end local v6    # "prefSet":Landroid/preference/PreferenceScreen;
    :catch_0
    move-exception v2

    .line 638
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v7, "NetworkSettings"

    const-string/jumbo v8, "NameNotFoundException for show4GFotLTE"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    iput-boolean v11, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mShow4GForLTE:Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 674
    const v0, 0x1090049

    .line 675
    const/4 v1, 0x0

    .line 674
    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 695
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->unbindNetworkQueryService()V

    .line 696
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 697
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 698
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhoneChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 700
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1660
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1661
    .local v0, "itemId":I
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 1673
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 1674
    const/4 v1, 0x1

    return v1

    .line 1676
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 1072
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 1073
    const-string/jumbo v0, "onPause:+"

    invoke-static {v0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 1075
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 1077
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    .line 1078
    iget-object v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mOnSubscriptionsChangeListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    .line 1077
    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->removeOnSubscriptionsChangedListener(Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 1079
    const-string/jumbo v0, "onPause:-"

    invoke-static {v0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 1080
    return-void
.end method

.method public onPositiveButtonClick(Landroid/app/DialogFragment;)V
    .locals 2
    .param p1, "dialog"    # Landroid/app/DialogFragment;

    .prologue
    const/4 v1, 0x1

    .line 326
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->setDataRoamingEnabled(Z)V

    .line 327
    iget-object v0, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    invoke-virtual {v0, v1}, Lcom/android/phone/RestrictedSwitchPreference;->setChecked(Z)V

    .line 328
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 12
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "objValue"    # Ljava/lang/Object;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1091
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v5

    .line 1092
    .local v5, "phoneSubId":I
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    if-ne p1, v7, :cond_1

    .line 1095
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    move-object v7, p2

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1097
    check-cast p2, Ljava/lang/String;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1099
    .local v0, "buttonNetworkMode":I
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 1100
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "preferred_network_mode"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1101
    sget v9, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->preferredNetworkMode:I

    .line 1098
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 1102
    .local v6, "settingsNetworkMode":I
    if-eq v0, v6, :cond_0

    .line 1105
    packed-switch v0, :pswitch_data_0

    .line 1133
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Invalid Network Mode ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ") chosen. Ignore."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->loge(Ljava/lang/String;)V

    .line 1134
    return v11

    .line 1130
    :pswitch_0
    move v4, v0

    .line 1138
    .local v4, "modemNetworkMode":I
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 1139
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "preferred_network_mode"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1137
    invoke-static {v7, v8, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1142
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mHandler:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;

    invoke-virtual {v8, v10}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v7, v0, v8}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    .line 1227
    .end local v0    # "buttonNetworkMode":I
    .end local v4    # "modemNetworkMode":I
    .end local v6    # "settingsNetworkMode":I
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateBody()V

    .line 1229
    return v11

    .line 1145
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_1
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    if-ne p1, v7, :cond_2

    .line 1146
    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    move-object v7, p2

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 1148
    check-cast p2, Ljava/lang/String;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1149
    .restart local v0    # "buttonNetworkMode":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "buttonNetworkMode: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 1151
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 1152
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "preferred_network_mode"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1153
    sget v9, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->preferredNetworkMode:I

    .line 1150
    invoke-static {v7, v8, v9}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 1154
    .restart local v6    # "settingsNetworkMode":I
    if-eq v0, v6, :cond_0

    .line 1157
    packed-switch v0, :pswitch_data_1

    .line 1179
    :pswitch_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "Invalid Network Mode ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ") chosen. Ignore."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->loge(Ljava/lang/String;)V

    .line 1180
    return v11

    .line 1176
    :pswitch_2
    move v4, v0

    .line 1183
    .restart local v4    # "modemNetworkMode":I
    invoke-direct {p0, v0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->UpdateEnabledNetworksValueAndSummary(I)V

    .line 1186
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v7}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 1187
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "preferred_network_mode"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1185
    invoke-static {v7, v8, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1190
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v8, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mHandler:Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;

    invoke-virtual {v8, v10}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment$MyHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v7, v0, v8}, Lcom/android/internal/telephony/Phone;->setPreferredNetworkType(ILandroid/os/Message;)V

    goto/16 :goto_0

    .line 1193
    .end local v0    # "buttonNetworkMode":I
    .end local v4    # "modemNetworkMode":I
    .end local v6    # "settingsNetworkMode":I
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_2
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButton4glte:Landroid/preference/SwitchPreference;

    if-ne p1, v7, :cond_3

    move-object v2, p1

    .line 1194
    check-cast v2, Landroid/preference/SwitchPreference;

    .line 1195
    .local v2, "enhanced4gModePref":Landroid/preference/SwitchPreference;
    invoke-virtual {v2}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v7

    xor-int/lit8 v1, v7, 0x1

    .line 1196
    .local v1, "enhanced4gMode":Z
    invoke-virtual {v2, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 1197
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    .line 1198
    invoke-virtual {v2}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v8

    .line 1197
    invoke-static {v7, v8}, Lcom/android/ims/ImsManager;->setEnhanced4gLteModeSetting(Landroid/content/Context;Z)V

    goto/16 :goto_0

    .line 1199
    .end local v1    # "enhanced4gMode":Z
    .end local v2    # "enhanced4gModePref":Landroid/preference/SwitchPreference;
    :cond_3
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    if-ne p1, v7, :cond_5

    .line 1200
    const-string/jumbo v7, "onPreferenceTreeClick: preference == mButtonDataRoam."

    invoke-static {v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->log(Ljava/lang/String;)V

    .line 1203
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    invoke-virtual {v7}, Lcom/android/phone/RestrictedSwitchPreference;->isChecked()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1205
    iput-boolean v10, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mOkClicked:Z

    .line 1206
    new-instance v3, Lcom/android/phone/RoamingDialogFragment;

    invoke-direct {v3}, Lcom/android/phone/RoamingDialogFragment;-><init>()V

    .line 1207
    .local v3, "fragment":Lcom/android/phone/RoamingDialogFragment;
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string/jumbo v8, "RoamingDialogFragment"

    invoke-virtual {v3, v7, v8}, Lcom/android/phone/RoamingDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1209
    return v10

    .line 1211
    .end local v3    # "fragment":Lcom/android/phone/RoamingDialogFragment;
    :cond_4
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v7, v10}, Lcom/android/internal/telephony/Phone;->setDataRoamingEnabled(Z)V

    .line 1213
    return v11

    .line 1214
    :cond_5
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    if-ne p1, v7, :cond_0

    .line 1217
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButton4glte:Landroid/preference/SwitchPreference;

    invoke-virtual {v7}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1218
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v7

    iget-object v7, v7, Lcom/android/phone/PhoneGlobals;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "objValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/android/phone/PhoneInterfaceManager;->enableVideoCalling(Z)V

    .line 1219
    return v11

    .line 1221
    .restart local p2    # "objValue":Ljava/lang/Object;
    :cond_6
    const-string/jumbo v7, "mVideoCallingPref should be disabled if mButton4glte is not checked."

    invoke-static {v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->loge(Ljava/lang/String;)V

    .line 1222
    iget-object v7, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    invoke-virtual {v7, v10}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 1223
    return v10

    .line 1105
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1157
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 11
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v10, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 345
    invoke-direct {p0, p1, p2}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->sendMetricsEventPreferenceClicked(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)V

    .line 348
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    .line 349
    .local v2, "phoneSubId":I
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "enhanced_4g_lte"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 350
    return v9

    .line 351
    :cond_0
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mGsmUmtsOptions:Lcom/android/phone/GsmUmtsOptions;

    if-eqz v6, :cond_1

    .line 352
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mGsmUmtsOptions:Lcom/android/phone/GsmUmtsOptions;

    invoke-virtual {v6, p2}, Lcom/android/phone/GsmUmtsOptions;->preferenceTreeClick(Landroid/preference/Preference;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 353
    return v9

    .line 354
    :cond_1
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCdmaOptions:Lcom/android/phone/CdmaOptions;

    if-eqz v6, :cond_3

    .line 355
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mCdmaOptions:Lcom/android/phone/CdmaOptions;

    invoke-virtual {v6, p2}, Lcom/android/phone/CdmaOptions;->preferenceTreeClick(Landroid/preference/Preference;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 356
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->isInEcm()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 358
    iput-object p2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mClickedPreference:Landroid/preference/Preference;

    .line 362
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v7, "com.android.internal.intent.action.ACTION_SHOW_NOTICE_ECM_BLOCK_OTHERS"

    invoke-direct {v6, v7, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 363
    const/16 v7, 0x11

    .line 361
    invoke-virtual {p0, v6, v7}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 365
    :cond_2
    return v9

    .line 366
    :cond_3
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    if-ne p2, v6, :cond_4

    .line 369
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 370
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "preferred_network_mode"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 371
    sget v8, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->preferredNetworkMode:I

    .line 368
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 372
    .local v3, "settingsNetworkMode":I
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonPreferredNetworkMode:Landroid/preference/ListPreference;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 373
    return v9

    .line 374
    .end local v3    # "settingsNetworkMode":I
    :cond_4
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mLteDataServicePref:Landroid/preference/Preference;

    if-ne p2, v6, :cond_8

    .line 376
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 377
    const-string/jumbo v7, "setup_prepaid_data_service_url"

    .line 375
    invoke-static {v6, v7}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 378
    .local v4, "tmpl":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 379
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    .line 380
    .local v0, "imsi":Ljava/lang/String;
    if-nez v0, :cond_5

    .line 381
    const-string/jumbo v0, ""

    .line 383
    :cond_5
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    const/4 v5, 0x0

    .line 385
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v6, "android.intent.action.VIEW"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 386
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->startActivity(Landroid/content/Intent;)V

    .line 390
    .end local v0    # "imsi":Ljava/lang/String;
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    return v9

    .line 384
    .restart local v0    # "imsi":Ljava/lang/String;
    :cond_6
    new-array v6, v9, [Ljava/lang/CharSequence;

    aput-object v0, v6, v8

    invoke-static {v4, v6}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .local v5, "url":Ljava/lang/String;
    goto :goto_0

    .line 388
    .end local v0    # "imsi":Ljava/lang/String;
    .end local v5    # "url":Ljava/lang/String;
    :cond_7
    const-string/jumbo v6, "NetworkSettings"

    const-string/jumbo v7, "Missing SETUP_PREPAID_DATA_SERVICE_URL"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 391
    .end local v4    # "tmpl":Ljava/lang/String;
    :cond_8
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    if-ne p2, v6, :cond_9

    .line 393
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 394
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "preferred_network_mode"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 395
    sget v8, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->preferredNetworkMode:I

    .line 392
    invoke-static {v6, v7, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 396
    .restart local v3    # "settingsNetworkMode":I
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonEnabledNetworks:Landroid/preference/ListPreference;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 397
    return v9

    .line 398
    .end local v3    # "settingsNetworkMode":I
    :cond_9
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    if-ne p2, v6, :cond_a

    .line 400
    return v9

    .line 401
    :cond_a
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mEuiccSettingsPref:Landroid/preference/Preference;

    if-ne p2, v6, :cond_b

    .line 402
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v6, "android.telephony.euicc.action.MANAGE_EMBEDDED_SUBSCRIPTIONS"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 403
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->startActivity(Landroid/content/Intent;)V

    .line 404
    return v9

    .line 405
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_b
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mWiFiCallingPref:Landroid/preference/Preference;

    if-eq p2, v6, :cond_c

    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mVideoCallingPref:Landroid/preference/SwitchPreference;

    if-ne p2, v6, :cond_d

    .line 407
    :cond_c
    return v8

    .line 406
    :cond_d
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mMobileDataPref:Lcom/android/phone/MobileDataPreference;

    if-eq p2, v6, :cond_c

    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mDataUsagePref:Lcom/android/phone/DataUsagePreference;

    if-eq p2, v6, :cond_c

    .line 408
    iget-object v6, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mAdvancedOptions:Lcom/android/phone/AdvancedOptionsPreference;

    if-ne p2, v6, :cond_e

    .line 409
    iput-boolean v9, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mExpandAdvancedFields:Z

    .line 410
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateBody()V

    .line 411
    return v9

    .line 416
    :cond_e
    invoke-virtual {p1, v8}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 418
    return v8
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 704
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 705
    const-string/jumbo v2, "NetworkSettings"

    const-string/jumbo v3, "onResume:+"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    iget-boolean v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mUnavailable:Z

    if-eqz v2, :cond_0

    .line 708
    const-string/jumbo v2, "NetworkSettings"

    const-string/jumbo v3, "onResume:- ignore mUnavailable == false"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    return-void

    .line 712
    :cond_0
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 713
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 714
    :cond_1
    const-string/jumbo v2, "NetworkSettings"

    const-string/jumbo v3, "onResume:- with no valid activity."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    return-void

    .line 719
    :cond_2
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    .line 724
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButtonDataRoam:Lcom/android/phone/RestrictedSwitchPreference;

    iget-object v3, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getDataRoamingEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/phone/RestrictedSwitchPreference;->setChecked(Z)V

    .line 726
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const-string/jumbo v3, "preferred_network_mode_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-nez v2, :cond_3

    .line 727
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const-string/jumbo v3, "enabled_networks_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 728
    :cond_3
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updatePreferredNetworkUIFromDb()V

    .line 731
    :cond_4
    invoke-static {v0}, Lcom/android/ims/ImsManager;->isVolteEnabledByPlatform(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 732
    invoke-static {v0}, Lcom/android/ims/ImsManager;->isVolteProvisionedOnDevice(Landroid/content/Context;)Z

    move-result v2

    .line 731
    if-eqz v2, :cond_5

    .line 733
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v4, 0x20

    invoke-virtual {v2, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 737
    :cond_5
    invoke-static {v0}, Lcom/android/ims/ImsManager;->isEnhanced4gLteModeSettingEnabledByUser(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 738
    invoke-static {v0}, Lcom/android/ims/ImsManager;->isNonTtyOrTtyOnVolteEnabled(Landroid/content/Context;)Z

    move-result v1

    .line 739
    :goto_0
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mButton4glte:Landroid/preference/SwitchPreference;

    invoke-virtual {v2, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 742
    invoke-direct {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->updateCallingCategory()V

    .line 744
    iget-object v2, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    iget-object v3, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mOnSubscriptionsChangeListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    invoke-virtual {v2, v3}, Landroid/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 746
    const-string/jumbo v2, "NetworkSettings"

    const-string/jumbo v3, "onResume:-"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    return-void

    .line 737
    :cond_6
    const/4 v1, 0x0

    .local v1, "enh4glteMode":Z
    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 585
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 589
    const-string/jumbo v0, "expand_advanced_fields"

    iget-boolean v1, p0, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->mExpandAdvancedFields:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 590
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 332
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 333
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 335
    :cond_0
    return-void
.end method
