.class public Lcom/android/phone/CallBarringHelper;
.super Ljava/lang/Object;
.source "CallBarringHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCallBarringOption(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;Lcom/android/internal/telephony/Phone;)V
    .locals 3
    .param p0, "facility"    # Ljava/lang/String;
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "onComplete"    # Landroid/os/Message;
    .param p3, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 20
    if-eqz p3, :cond_2

    invoke-static {p3}, Lcom/android/phone/CallBarringHelper;->isPhoneTypeGsm(Lcom/android/internal/telephony/Phone;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 21
    invoke-static {p0}, Lcom/android/phone/CallBarringHelper;->isValidFacilityString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 22
    invoke-static {p3, p0}, Lcom/android/phone/PhoneProxy;->isCallBarringFacilitySupportedOverImsPhone(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 23
    const-string/jumbo v1, "Trying IMS PS get call barring"

    invoke-static {v1}, Lcom/android/phone/CallBarringHelper;->log(Ljava/lang/String;)V

    .line 24
    invoke-virtual {p3}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/imsphone/ImsPhone;

    .line 25
    .local v0, "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    invoke-virtual {v0, p0, p2}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getCallBarring(Ljava/lang/String;Landroid/os/Message;)V

    .line 35
    .end local v0    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    invoke-static {p3}, Lcom/android/phone/PhoneProxy;->getCommandsInterface(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/CommandsInterface;

    move-result-object v1

    .line 28
    const/4 v2, 0x0

    .line 27
    invoke-interface {v1, p0, p1, v2, p2}, Lcom/android/internal/telephony/CommandsInterface;->queryFacilityLock(Ljava/lang/String;Ljava/lang/String;ILandroid/os/Message;)V

    goto :goto_0

    .line 32
    :cond_2
    const-string/jumbo v1, " getCallBarringOption is not supported for CDMA phone"

    invoke-static {v1}, Lcom/android/phone/CallBarringHelper;->log(Ljava/lang/String;)V

    .line 33
    invoke-static {p2}, Lcom/android/phone/CallBarringHelper;->replyFailureToUi(Landroid/os/Message;)V

    goto :goto_0
.end method

.method private static isPhoneTypeGsm(Lcom/android/internal/telephony/Phone;)Z
    .locals 3
    .param p0, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 65
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static isValidFacilityString(Ljava/lang/String;)Z
    .locals 2
    .param p0, "facility"    # Ljava/lang/String;

    .prologue
    .line 69
    const-string/jumbo v0, "AO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    const-string/jumbo v0, "OI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-nez v0, :cond_0

    .line 71
    const-string/jumbo v0, "OX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-nez v0, :cond_0

    .line 72
    const-string/jumbo v0, "AI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-nez v0, :cond_0

    .line 73
    const-string/jumbo v0, "IR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-nez v0, :cond_0

    .line 74
    const-string/jumbo v0, "AB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-nez v0, :cond_0

    .line 75
    const-string/jumbo v0, "AG"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-nez v0, :cond_0

    .line 76
    const-string/jumbo v0, "AC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-nez v0, :cond_0

    .line 77
    const-string/jumbo v0, "SC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-nez v0, :cond_0

    .line 78
    const-string/jumbo v0, "FD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 69
    if-eqz v0, :cond_1

    .line 79
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 81
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " Invalid facility String : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/CallBarringHelper;->log(Ljava/lang/String;)V

    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 86
    const-string/jumbo v0, "CallBarring"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    return-void
.end method

.method private static replyFailureToUi(Landroid/os/Message;)V
    .locals 3
    .param p0, "onComplete"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 57
    if-eqz p0, :cond_0

    .line 58
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/android/internal/telephony/CommandException;->fromRilErrno(I)Lcom/android/internal/telephony/CommandException;

    move-result-object v0

    .line 59
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {p0, v2, v0}, Landroid/os/AsyncResult;->forMessage(Landroid/os/Message;Ljava/lang/Object;Ljava/lang/Throwable;)Landroid/os/AsyncResult;

    .line 60
    invoke-virtual {p0}, Landroid/os/Message;->sendToTarget()V

    .line 62
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method public static setCallBarringOption(Ljava/lang/String;ZLjava/lang/String;Landroid/os/Message;Lcom/android/internal/telephony/Phone;)V
    .locals 7
    .param p0, "facility"    # Ljava/lang/String;
    .param p1, "lockState"    # Z
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "onComplete"    # Landroid/os/Message;
    .param p4, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 39
    if-eqz p4, :cond_2

    invoke-static {p4}, Lcom/android/phone/CallBarringHelper;->isPhoneTypeGsm(Lcom/android/internal/telephony/Phone;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 40
    invoke-static {p0}, Lcom/android/phone/CallBarringHelper;->isValidFacilityString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-static {p4, p0}, Lcom/android/phone/PhoneProxy;->isCallBarringFacilitySupportedOverImsPhone(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    const-string/jumbo v0, "Trying IMS PS set call barring"

    invoke-static {v0}, Lcom/android/phone/CallBarringHelper;->log(Ljava/lang/String;)V

    .line 43
    invoke-virtual {p4}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v6

    check-cast v6, Lcom/android/internal/telephony/imsphone/ImsPhone;

    .line 44
    .local v6, "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    invoke-virtual {v6, p0, p1, p2, p3}, Lcom/android/internal/telephony/imsphone/ImsPhone;->setCallBarring(Ljava/lang/String;ZLjava/lang/String;Landroid/os/Message;)V

    .line 54
    .end local v6    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    invoke-static {p4}, Lcom/android/phone/PhoneProxy;->getCommandsInterface(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/CommandsInterface;

    move-result-object v0

    .line 47
    const/4 v4, 0x1

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, p3

    .line 46
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/telephony/CommandsInterface;->setFacilityLock(Ljava/lang/String;ZLjava/lang/String;ILandroid/os/Message;)V

    goto :goto_0

    .line 51
    :cond_2
    const-string/jumbo v0, " setCallBarringOption is not supported for CDMA phone"

    invoke-static {v0}, Lcom/android/phone/CallBarringHelper;->log(Ljava/lang/String;)V

    .line 52
    invoke-static {p3}, Lcom/android/phone/CallBarringHelper;->replyFailureToUi(Landroid/os/Message;)V

    goto :goto_0
.end method
