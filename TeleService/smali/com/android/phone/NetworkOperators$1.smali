.class Lcom/android/phone/NetworkOperators$1;
.super Landroid/os/Handler;
.source "NetworkOperators.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/NetworkOperators;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/NetworkOperators;


# direct methods
.method constructor <init>(Lcom/android/phone/NetworkOperators;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/phone/NetworkOperators;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    .line 120
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 124
    iget v4, p1, Landroid/os/Message;->what:I

    sparse-switch v4, :sswitch_data_0

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 126
    :sswitch_0
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    invoke-static {v4}, Lcom/android/phone/NetworkOperators;->-get0(Lcom/android/phone/NetworkOperators;)Landroid/preference/TwoStatePreference;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/preference/TwoStatePreference;->setEnabled(Z)V

    .line 127
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    invoke-static {v4}, Lcom/android/phone/NetworkOperators;->-wrap0(Lcom/android/phone/NetworkOperators;)V

    .line 129
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 130
    .local v0, "ar":Landroid/os/AsyncResult;
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v4, :cond_1

    .line 131
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    const-string/jumbo v5, "automatic network selection: failed!"

    invoke-static {v4, v5}, Lcom/android/phone/NetworkOperators;->-wrap1(Lcom/android/phone/NetworkOperators;Ljava/lang/String;)V

    .line 132
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v4, v5}, Lcom/android/phone/NetworkOperators;->displayNetworkSelectionFailed(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 134
    :cond_1
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    const-string/jumbo v5, "automatic network selection: succeeded!"

    invoke-static {v4, v5}, Lcom/android/phone/NetworkOperators;->-wrap1(Lcom/android/phone/NetworkOperators;Ljava/lang/String;)V

    .line 135
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    invoke-virtual {v4}, Lcom/android/phone/NetworkOperators;->displayNetworkSelectionSucceeded()V

    goto :goto_0

    .line 140
    .end local v0    # "ar":Landroid/os/AsyncResult;
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 141
    .restart local v0    # "ar":Landroid/os/AsyncResult;
    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v4, :cond_2

    .line 142
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    const-string/jumbo v5, "get network selection mode: failed!"

    invoke-static {v4, v5}, Lcom/android/phone/NetworkOperators;->-wrap1(Lcom/android/phone/NetworkOperators;Ljava/lang/String;)V

    goto :goto_0

    .line 143
    :cond_2
    iget-object v4, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 145
    :try_start_0
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v3, [I

    .line 146
    .local v3, "modes":[I
    const/4 v4, 0x0

    aget v4, v3, v4

    if-nez v4, :cond_4

    const/4 v1, 0x1

    .line 148
    .local v1, "autoSelect":Z
    :goto_1
    iget-object v5, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "get network selection mode: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 149
    if-eqz v1, :cond_5

    const-string/jumbo v4, "auto"

    .line 148
    :goto_2
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 149
    const-string/jumbo v6, " selection"

    .line 148
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/android/phone/NetworkOperators;->-wrap1(Lcom/android/phone/NetworkOperators;Ljava/lang/String;)V

    .line 151
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    invoke-static {v4}, Lcom/android/phone/NetworkOperators;->-get0(Lcom/android/phone/NetworkOperators;)Landroid/preference/TwoStatePreference;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 152
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    invoke-static {v4}, Lcom/android/phone/NetworkOperators;->-get0(Lcom/android/phone/NetworkOperators;)Landroid/preference/TwoStatePreference;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    .line 154
    :cond_3
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    invoke-static {v4}, Lcom/android/phone/NetworkOperators;->-get1(Lcom/android/phone/NetworkOperators;)Lcom/android/phone/NetworkSelectListPreference;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 155
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    invoke-static {v4}, Lcom/android/phone/NetworkOperators;->-get1(Lcom/android/phone/NetworkOperators;)Lcom/android/phone/NetworkSelectListPreference;

    move-result-object v4

    xor-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v5}, Lcom/android/phone/NetworkSelectListPreference;->setEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 157
    .end local v1    # "autoSelect":Z
    .end local v3    # "modes":[I
    :catch_0
    move-exception v2

    .line 158
    .local v2, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/android/phone/NetworkOperators$1;->this$0:Lcom/android/phone/NetworkOperators;

    const-string/jumbo v5, "get network selection mode: unable to parse result."

    invoke-static {v4, v5}, Lcom/android/phone/NetworkOperators;->-wrap2(Lcom/android/phone/NetworkOperators;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 146
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "modes":[I
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "autoSelect":Z
    goto :goto_1

    .line 149
    :cond_5
    :try_start_1
    const-string/jumbo v4, "manual"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 124
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch
.end method
