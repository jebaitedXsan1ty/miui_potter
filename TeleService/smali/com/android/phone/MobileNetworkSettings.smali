.class public Lcom/android/phone/MobileNetworkSettings;
.super Landroid/app/Activity;
.source "MobileNetworkSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;,
        Lcom/android/phone/MobileNetworkSettings$TabState;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static hideEnhanced4gLteSettings(Landroid/content/Context;Landroid/os/PersistableBundle;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "carrierConfig"    # Landroid/os/PersistableBundle;

    .prologue
    .line 154
    invoke-static {p0}, Lcom/android/ims/ImsManager;->isVolteEnabledByPlatform(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-static {p0}, Lcom/android/ims/ImsManager;->isVolteProvisionedOnDevice(Landroid/content/Context;)Z

    move-result v0

    .line 154
    :goto_0
    if-eqz v0, :cond_1

    .line 157
    const-string/jumbo v0, "hide_enhanced_4g_lte_bool"

    .line 156
    invoke-virtual {p1, v0}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 154
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static showEuiccSettings(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 136
    const-string/jumbo v4, "euicc_service"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/euicc/EuiccManager;

    .line 137
    .local v1, "euiccManager":Landroid/telephony/euicc/EuiccManager;
    invoke-virtual {v1}, Landroid/telephony/euicc/EuiccManager;->isEnabled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 138
    return v3

    .line 140
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 141
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string/jumbo v4, "euicc_provisioned"

    invoke-static {v0, v4, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-nez v4, :cond_1

    .line 143
    const-string/jumbo v4, "development_settings_enabled"

    .line 142
    invoke-static {v0, v4, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_2

    .line 141
    :cond_1
    :goto_0
    return v2

    :cond_2
    move v2, v3

    .line 142
    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x7f0d00ce

    .line 102
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 103
    const v2, 0x7f04003e

    invoke-virtual {p0, v2}, Lcom/android/phone/MobileNetworkSettings;->setContentView(I)V

    .line 105
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 106
    .local v1, "fragmentManager":Landroid/app/FragmentManager;
    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 107
    .local v0, "fragment":Landroid/app/Fragment;
    if-nez v0, :cond_0

    .line 108
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 109
    new-instance v3, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;

    invoke-direct {v3}, Lcom/android/phone/MobileNetworkSettings$MobileNetworkFragment;-><init>()V

    .line 108
    invoke-virtual {v2, v4, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 112
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 116
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 117
    .local v0, "itemId":I
    packed-switch v0, :pswitch_data_0

    .line 123
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 120
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/phone/MobileNetworkSettings;->finish()V

    .line 121
    const/4 v1, 0x1

    return v1

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
