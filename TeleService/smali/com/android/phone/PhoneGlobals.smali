.class public Lcom/android/phone/PhoneGlobals;
.super Landroid/content/ContextWrapper;
.source "PhoneGlobals.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/PhoneGlobals$1;,
        Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;,
        Lcom/android/phone/PhoneGlobals$SipReceiver;,
        Lcom/android/phone/PhoneGlobals$WakeState;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-phone-PhoneGlobals$WakeStateSwitchesValues:[I

.field private static final DBG:Z

.field private static sMe:Lcom/android/phone/PhoneGlobals;

.field static sVoiceCapable:Z


# instance fields
.field callController:Lcom/android/phone/CallController;

.field private callGatewayManager:Lcom/android/phone/CallGatewayManager;

.field callerInfoCache:Lcom/android/phone/CallerInfoCache;

.field cdmaPhoneCallState:Lcom/android/phone/CdmaPhoneCallState;

.field configLoader:Lcom/android/phone/CarrierConfigLoader;

.field mCM:Lcom/android/internal/telephony/CallManager;

.field private final mCarrierVvmPackageInstalledReceiver:Lcom/android/phone/vvm/CarrierVvmPackageInstalledReceiver;

.field private mDataDisconnectedDueToRoaming:Z

.field private final mDataRoamingNotifLog:Landroid/util/LocalLog;

.field private mDefaultDataSubId:I

.field mHandler:Landroid/os/Handler;

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mNoDataDueToRoaming:Z

.field private mPUKEntryActivity:Landroid/app/Activity;

.field private mPUKEntryProgressDialog:Lmiui/app/ProgressDialog;

.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private final mSettingsObserver:Lcom/android/internal/telephony/SettingsObserver;

.field private final mSipReceiver:Lcom/android/phone/PhoneGlobals$SipReceiver;

.field private mUpdateLock:Landroid/os/UpdateLock;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeState:Lcom/android/phone/PhoneGlobals$WakeState;

.field notificationMgr:Lcom/android/phone/NotificationMgr;

.field notifier:Lcom/android/phone/CallNotifier;

.field private phoneInEcm:Lcom/android/internal/telephony/Phone;

.field public phoneMgr:Lcom/android/phone/PhoneInterfaceManager;


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/phone/PhoneGlobals;->DBG:Z

    return v0
.end method

.method static synthetic -get1(Lcom/android/phone/PhoneGlobals;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    iget-boolean v0, p0, Lcom/android/phone/PhoneGlobals;->mDataDisconnectedDueToRoaming:Z

    return v0
.end method

.method static synthetic -get2(Lcom/android/phone/PhoneGlobals;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    iget v0, p0, Lcom/android/phone/PhoneGlobals;->mDefaultDataSubId:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/phone/PhoneGlobals;)Landroid/app/Activity;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mPUKEntryActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/phone/PhoneGlobals;)Lmiui/app/ProgressDialog;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mPUKEntryProgressDialog:Lmiui/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/phone/PhoneGlobals;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->phoneInEcm:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic -get6()Lcom/android/phone/PhoneGlobals;
    .locals 1

    sget-object v0, Lcom/android/phone/PhoneGlobals;->sMe:Lcom/android/phone/PhoneGlobals;

    return-object v0
.end method

.method private static synthetic -getcom-android-phone-PhoneGlobals$WakeStateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/phone/PhoneGlobals;->-com-android-phone-PhoneGlobals$WakeStateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/phone/PhoneGlobals;->-com-android-phone-PhoneGlobals$WakeStateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/phone/PhoneGlobals$WakeState;->values()[Lcom/android/phone/PhoneGlobals$WakeState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/phone/PhoneGlobals$WakeState;->FULL:Lcom/android/phone/PhoneGlobals$WakeState;

    invoke-virtual {v1}, Lcom/android/phone/PhoneGlobals$WakeState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/phone/PhoneGlobals$WakeState;->PARTIAL:Lcom/android/phone/PhoneGlobals$WakeState;

    invoke-virtual {v1}, Lcom/android/phone/PhoneGlobals$WakeState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/phone/PhoneGlobals$WakeState;->SLEEP:Lcom/android/phone/PhoneGlobals$WakeState;

    invoke-virtual {v1}, Lcom/android/phone/PhoneGlobals$WakeState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    sput-object v0, Lcom/android/phone/PhoneGlobals;->-com-android-phone-PhoneGlobals$WakeStateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/phone/PhoneGlobals;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/phone/PhoneGlobals;->mDataDisconnectedDueToRoaming:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/phone/PhoneGlobals;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/phone/PhoneGlobals;->mDefaultDataSubId:I

    return p1
.end method

.method static synthetic -set2(Lcom/android/phone/PhoneGlobals;Landroid/app/Activity;)Landroid/app/Activity;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;
    .param p1, "-value"    # Landroid/app/Activity;

    .prologue
    iput-object p1, p0, Lcom/android/phone/PhoneGlobals;->mPUKEntryActivity:Landroid/app/Activity;

    return-object p1
.end method

.method static synthetic -set3(Lcom/android/phone/PhoneGlobals;Lmiui/app/ProgressDialog;)Lmiui/app/ProgressDialog;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;
    .param p1, "-value"    # Lmiui/app/ProgressDialog;

    .prologue
    iput-object p1, p0, Lcom/android/phone/PhoneGlobals;->mPUKEntryProgressDialog:Lmiui/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic -set4(Lcom/android/phone/PhoneGlobals;Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;
    .param p1, "-value"    # Lcom/android/internal/telephony/Phone;

    .prologue
    iput-object p1, p0, Lcom/android/phone/PhoneGlobals;->phoneInEcm:Lcom/android/internal/telephony/Phone;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/phone/PhoneGlobals;Landroid/content/Context;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "newMode"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/phone/PhoneGlobals;->handleAirplaneModeChange(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/phone/PhoneGlobals;Landroid/content/Intent;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/PhoneGlobals;->handleServiceStateChanged(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/phone/PhoneGlobals;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/PhoneGlobals;->initForNewRadioTechnology()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/phone/PhoneGlobals;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;
    .param p1, "r"    # Landroid/os/AsyncResult;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/phone/PhoneGlobals;->onMMIComplete(Landroid/os/AsyncResult;)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/phone/PhoneGlobals;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/PhoneGlobals;->registerSettingsObserver()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/phone/PhoneGlobals;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/PhoneGlobals;

    .prologue
    invoke-direct {p0}, Lcom/android/phone/PhoneGlobals;->updateDataRoamingStatus()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 101
    sput-boolean v0, Lcom/android/phone/PhoneGlobals;->DBG:Z

    .line 154
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/phone/PhoneGlobals;->sVoiceCapable:Z

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 279
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 166
    iput-boolean v0, p0, Lcom/android/phone/PhoneGlobals;->mNoDataDueToRoaming:Z

    .line 167
    iput-boolean v0, p0, Lcom/android/phone/PhoneGlobals;->mDataDisconnectedDueToRoaming:Z

    .line 169
    sget-object v0, Lcom/android/phone/PhoneGlobals$WakeState;->SLEEP:Lcom/android/phone/PhoneGlobals$WakeState;

    iput-object v0, p0, Lcom/android/phone/PhoneGlobals;->mWakeState:Lcom/android/phone/PhoneGlobals$WakeState;

    .line 178
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/PhoneGlobals;->mDefaultDataSubId:I

    .line 179
    new-instance v0, Landroid/util/LocalLog;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Landroid/util/LocalLog;-><init>(I)V

    iput-object v0, p0, Lcom/android/phone/PhoneGlobals;->mDataRoamingNotifLog:Landroid/util/LocalLog;

    .line 182
    new-instance v0, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;

    invoke-direct {v0, p0, v2}, Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;-><init>(Lcom/android/phone/PhoneGlobals;Lcom/android/phone/PhoneGlobals$PhoneAppBroadcastReceiver;)V

    iput-object v0, p0, Lcom/android/phone/PhoneGlobals;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 184
    new-instance v0, Lcom/android/phone/PhoneGlobals$SipReceiver;

    invoke-direct {v0, p0, v2}, Lcom/android/phone/PhoneGlobals$SipReceiver;-><init>(Lcom/android/phone/PhoneGlobals;Lcom/android/phone/PhoneGlobals$SipReceiver;)V

    iput-object v0, p0, Lcom/android/phone/PhoneGlobals;->mSipReceiver:Lcom/android/phone/PhoneGlobals$SipReceiver;

    .line 187
    new-instance v0, Lcom/android/phone/vvm/CarrierVvmPackageInstalledReceiver;

    invoke-direct {v0}, Lcom/android/phone/vvm/CarrierVvmPackageInstalledReceiver;-><init>()V

    .line 186
    iput-object v0, p0, Lcom/android/phone/PhoneGlobals;->mCarrierVvmPackageInstalledReceiver:Lcom/android/phone/vvm/CarrierVvmPackageInstalledReceiver;

    .line 191
    new-instance v0, Lcom/android/phone/PhoneGlobals$1;

    invoke-direct {v0, p0}, Lcom/android/phone/PhoneGlobals$1;-><init>(Lcom/android/phone/PhoneGlobals;)V

    iput-object v0, p0, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    .line 280
    sput-object p0, Lcom/android/phone/PhoneGlobals;->sMe:Lcom/android/phone/PhoneGlobals;

    .line 281
    new-instance v0, Lcom/android/internal/telephony/SettingsObserver;

    iget-object v1, p0, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p1, v1}, Lcom/android/internal/telephony/SettingsObserver;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/phone/PhoneGlobals;->mSettingsObserver:Lcom/android/internal/telephony/SettingsObserver;

    .line 282
    return-void
.end method

.method public static getInstance()Lcom/android/phone/PhoneGlobals;
    .locals 2

    .prologue
    .line 427
    sget-object v0, Lcom/android/phone/PhoneGlobals;->sMe:Lcom/android/phone/PhoneGlobals;

    if-nez v0, :cond_0

    .line 428
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No PhoneGlobals here!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 430
    :cond_0
    sget-object v0, Lcom/android/phone/PhoneGlobals;->sMe:Lcom/android/phone/PhoneGlobals;

    return-object v0
.end method

.method public static getPhone()Lcom/android/internal/telephony/Phone;
    .locals 1

    .prologue
    .line 447
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0
.end method

.method public static getPhone(I)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "subId"    # I

    .prologue
    .line 451
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getPhoneId(I)I

    move-result v0

    invoke-static {v0}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0
.end method

.method private handleAirplaneModeChange(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "newMode"    # I

    .prologue
    const/4 v4, 0x1

    .line 641
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 642
    const-string/jumbo v3, "cell_on"

    .line 641
    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 643
    .local v0, "cellState":I
    if-ne p2, v4, :cond_0

    const/4 v1, 0x1

    .line 644
    .local v1, "isAirplaneNewlyOn":Z
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 656
    :goto_1
    :pswitch_0
    return-void

    .line 643
    .end local v1    # "isAirplaneNewlyOn":Z
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "isAirplaneNewlyOn":Z
    goto :goto_0

    .line 650
    :pswitch_1
    invoke-direct {p0, p1, v1}, Lcom/android/phone/PhoneGlobals;->maybeTurnCellOff(Landroid/content/Context;Z)V

    goto :goto_1

    .line 653
    :pswitch_2
    invoke-direct {p0, p1, v1}, Lcom/android/phone/PhoneGlobals;->maybeTurnCellOn(Landroid/content/Context;Z)V

    goto :goto_1

    .line 644
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleServiceStateChanged(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, -0x1

    .line 864
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 865
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 866
    invoke-static {v0}, Landroid/telephony/ServiceState;->newFromBundle(Landroid/os/Bundle;)Landroid/telephony/ServiceState;

    move-result-object v2

    .line 867
    .local v2, "ss":Landroid/telephony/ServiceState;
    if-eqz v2, :cond_0

    .line 868
    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getState()I

    move-result v3

    .line 869
    .local v3, "state":I
    const-string/jumbo v5, "subscription"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 873
    .local v4, "subId":I
    const-string/jumbo v5, "slot"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 875
    .local v1, "slotId":I
    iget-object v5, p0, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    invoke-virtual {v5, v3, v4, v1}, Lcom/android/phone/NotificationMgr;->updateNetworkSelection(III)V

    .line 881
    iget v5, p0, Lcom/android/phone/PhoneGlobals;->mDefaultDataSubId:I

    if-ne v4, v5, :cond_0

    .line 882
    invoke-direct {p0}, Lcom/android/phone/PhoneGlobals;->updateDataRoamingStatus()V

    .line 886
    .end local v1    # "slotId":I
    .end local v2    # "ss":Landroid/telephony/ServiceState;
    .end local v3    # "state":I
    .end local v4    # "subId":I
    :cond_0
    return-void
.end method

.method private initForNewRadioTechnology()V
    .locals 2

    .prologue
    .line 636
    sget-boolean v0, Lcom/android/phone/PhoneGlobals;->DBG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "initForNewRadioTechnology..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->notifier:Lcom/android/phone/CallNotifier;

    invoke-virtual {v0}, Lcom/android/phone/CallNotifier;->updateCallNotifierRegistrationsAfterRadioTechnologyChange()V

    .line 638
    return-void
.end method

.method private isCellOffInAirplaneMode(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 662
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 663
    const-string/jumbo v2, "airplane_mode_radios"

    .line 662
    invoke-static {v1, v2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 664
    .local v0, "airplaneModeRadios":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 665
    const-string/jumbo v1, "cell"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 664
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private maybeTurnCellOff(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isAirplaneNewlyOn"    # Z

    .prologue
    .line 688
    if-eqz p2, :cond_0

    .line 691
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-static {v0}, Lcom/android/phone/PhoneUtils;->isInEmergencyCall(Lcom/android/internal/telephony/CallManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 693
    invoke-static {p0}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->setAirplaneMode(Z)V

    .line 694
    const v0, 0x7f0b04ad

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 696
    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "Ignoring airplane mode: emergency call. Turning airplane off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    :cond_0
    :goto_0
    return-void

    .line 697
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/phone/PhoneGlobals;->isCellOffInAirplaneMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 698
    invoke-direct {p0, p1}, Lcom/android/phone/PhoneGlobals;->setRadioPowerOff(Landroid/content/Context;)V

    goto :goto_0

    .line 700
    :cond_2
    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "Ignoring airplane mode: settings prevent cell radio power off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private maybeTurnCellOn(Landroid/content/Context;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isAirplaneNewlyOn"    # Z

    .prologue
    .line 706
    if-nez p2, :cond_0

    .line 707
    invoke-direct {p0, p1}, Lcom/android/phone/PhoneGlobals;->setRadioPowerOn(Landroid/content/Context;)V

    .line 709
    :cond_0
    return-void
.end method

.method private onMMIComplete(Landroid/os/AsyncResult;)V
    .locals 4
    .param p1, "r"    # Landroid/os/AsyncResult;

    .prologue
    const/4 v3, 0x0

    .line 631
    iget-object v0, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v0, Lcom/android/internal/telephony/MmiCode;

    .line 632
    .local v0, "mmiCode":Lcom/android/internal/telephony/MmiCode;
    invoke-interface {v0}, Lcom/android/internal/telephony/MmiCode;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    invoke-static {v1, v2, v0, v3, v3}, Lcom/android/phone/PhoneUtils;->displayMMIComplete(Lcom/android/internal/telephony/Phone;Landroid/content/Context;Lcom/android/internal/telephony/MmiCode;Landroid/os/Message;Landroid/app/AlertDialog;)V

    .line 633
    return-void
.end method

.method private registerSettingsObserver()V
    .locals 6

    .prologue
    .line 467
    iget-object v3, p0, Lcom/android/phone/PhoneGlobals;->mSettingsObserver:Lcom/android/internal/telephony/SettingsObserver;

    invoke-virtual {v3}, Lcom/android/internal/telephony/SettingsObserver;->unobserve()V

    .line 468
    const-string/jumbo v0, "data_roaming"

    .line 469
    .local v0, "dataRoamingSetting":Ljava/lang/String;
    const-string/jumbo v1, "mobile_data"

    .line 470
    .local v1, "mobileDataSetting":Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimCount()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 471
    iget v2, p0, Lcom/android/phone/PhoneGlobals;->mDefaultDataSubId:I

    .line 472
    .local v2, "subId":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 473
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 474
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 479
    .end local v2    # "subId":I
    :cond_0
    iget-object v3, p0, Lcom/android/phone/PhoneGlobals;->mSettingsObserver:Lcom/android/internal/telephony/SettingsObserver;

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 480
    const/16 v5, 0xe

    .line 479
    invoke-virtual {v3, v4, v5}, Lcom/android/internal/telephony/SettingsObserver;->observe(Landroid/net/Uri;I)V

    .line 483
    iget-object v3, p0, Lcom/android/phone/PhoneGlobals;->mSettingsObserver:Lcom/android/internal/telephony/SettingsObserver;

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 484
    const/16 v5, 0xf

    .line 483
    invoke-virtual {v3, v4, v5}, Lcom/android/internal/telephony/SettingsObserver;->observe(Landroid/net/Uri;I)V

    .line 485
    return-void
.end method

.method private setRadioPowerOff(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 669
    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "Turning radio off - airplane"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "cell_on"

    .line 671
    const/4 v2, 0x2

    .line 670
    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 672
    invoke-virtual {p0}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "enable_cellular_on_boot"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 673
    const-string/jumbo v0, "persist.radio.airplane_mode_on"

    const-string/jumbo v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    invoke-static {v3}, Lcom/android/phone/PhoneUtils;->setRadioPower(Z)V

    .line 675
    return-void
.end method

.method private setRadioPowerOn(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 678
    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "Turning radio on - airplane"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "cell_on"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 681
    invoke-virtual {p0}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "enable_cellular_on_boot"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 683
    const-string/jumbo v0, "persist.radio.airplane_mode_on"

    const-string/jumbo v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    invoke-static {v2}, Lcom/android/phone/PhoneUtils;->setRadioPower(Z)V

    .line 685
    return-void
.end method

.method private updateDataRoamingStatus()V
    .locals 6

    .prologue
    .line 895
    iget v3, p0, Lcom/android/phone/PhoneGlobals;->mDefaultDataSubId:I

    invoke-static {v3}, Lcom/android/phone/PhoneGlobals;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 896
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v1, :cond_0

    .line 897
    const-string/jumbo v3, "PhoneGlobals"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Can\'t get phone with sub id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/phone/PhoneGlobals;->mDefaultDataSubId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    return-void

    .line 901
    :cond_0
    new-instance v2, Lcom/android/internal/telephony/dataconnection/DataConnectionReasons;

    invoke-direct {v2}, Lcom/android/internal/telephony/dataconnection/DataConnectionReasons;-><init>()V

    .line 902
    .local v2, "reasons":Lcom/android/internal/telephony/dataconnection/DataConnectionReasons;
    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/Phone;->isDataAllowed(Lcom/android/internal/telephony/dataconnection/DataConnectionReasons;)Z

    move-result v0

    .line 903
    .local v0, "dataAllowed":Z
    iget-object v3, p0, Lcom/android/phone/PhoneGlobals;->mDataRoamingNotifLog:Landroid/util/LocalLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "dataAllowed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", reasons="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 905
    iget-boolean v3, p0, Lcom/android/phone/PhoneGlobals;->mNoDataDueToRoaming:Z

    if-nez v3, :cond_2

    .line 906
    xor-int/lit8 v3, v0, 0x1

    .line 905
    if-eqz v3, :cond_2

    .line 907
    sget-object v3, Lcom/android/internal/telephony/dataconnection/DataConnectionReasons$DataDisallowedReasonType;->ROAMING_DISABLED:Lcom/android/internal/telephony/dataconnection/DataConnectionReasons$DataDisallowedReasonType;

    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/dataconnection/DataConnectionReasons;->containsOnly(Lcom/android/internal/telephony/dataconnection/DataConnectionReasons$DataDisallowedReasonType;)Z

    move-result v3

    .line 905
    if-eqz v3, :cond_2

    .line 910
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/phone/PhoneGlobals;->mNoDataDueToRoaming:Z

    .line 911
    const-string/jumbo v3, "PhoneGlobals"

    const-string/jumbo v4, "Show roaming disconnected notification"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    iget-object v3, p0, Lcom/android/phone/PhoneGlobals;->mDataRoamingNotifLog:Landroid/util/LocalLog;

    const-string/jumbo v4, "Show"

    invoke-virtual {v3, v4}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 913
    iget-object v3, p0, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 922
    :cond_1
    :goto_0
    return-void

    .line 914
    :cond_2
    iget-boolean v3, p0, Lcom/android/phone/PhoneGlobals;->mNoDataDueToRoaming:Z

    if-eqz v3, :cond_1

    if-nez v0, :cond_3

    .line 915
    sget-object v3, Lcom/android/internal/telephony/dataconnection/DataConnectionReasons$DataDisallowedReasonType;->ROAMING_DISABLED:Lcom/android/internal/telephony/dataconnection/DataConnectionReasons$DataDisallowedReasonType;

    invoke-virtual {v2, v3}, Lcom/android/internal/telephony/dataconnection/DataConnectionReasons;->containsOnly(Lcom/android/internal/telephony/dataconnection/DataConnectionReasons$DataDisallowedReasonType;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 914
    if-eqz v3, :cond_1

    .line 917
    :cond_3
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/phone/PhoneGlobals;->mNoDataDueToRoaming:Z

    .line 918
    const-string/jumbo v3, "PhoneGlobals"

    const-string/jumbo v4, "Dismiss roaming disconnected notification"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    iget-object v3, p0, Lcom/android/phone/PhoneGlobals;->mDataRoamingNotifLog:Landroid/util/LocalLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Hide. data allowed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", reasons="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 920
    iget-object v3, p0, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "printWriter"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 980
    new-instance v0, Lcom/android/internal/util/IndentingPrintWriter;

    const-string/jumbo v1, "  "

    invoke-direct {v0, p2, v1}, Lcom/android/internal/util/IndentingPrintWriter;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    .line 981
    .local v0, "pw":Lcom/android/internal/util/IndentingPrintWriter;
    const-string/jumbo v1, "------- PhoneGlobals -------"

    invoke-virtual {v0, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 982
    invoke-virtual {v0}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    .line 983
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mNoDataDueToRoaming="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/phone/PhoneGlobals;->mNoDataDueToRoaming:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 984
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mDefaultDataSubId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/PhoneGlobals;->mDefaultDataSubId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 985
    const-string/jumbo v1, "mDataRoamingNotifLog:"

    invoke-virtual {v0, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 986
    invoke-virtual {v0}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()V

    .line 987
    iget-object v1, p0, Lcom/android/phone/PhoneGlobals;->mDataRoamingNotifLog:Landroid/util/LocalLog;

    invoke-virtual {v1, p1, v0, p3}, Landroid/util/LocalLog;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 988
    invoke-virtual {v0}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    .line 989
    invoke-virtual {v0}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()V

    .line 990
    const-string/jumbo v1, "------- End PhoneGlobals -------"

    invoke-virtual {v0, v1}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    .line 991
    return-void
.end method

.method getCallManager()Lcom/android/internal/telephony/CallManager;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    return-object v0
.end method

.method public getCarrierConfig()Landroid/os/PersistableBundle;
    .locals 1

    .prologue
    .line 459
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultSubscriptionId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    return-object v0
.end method

.method public getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;
    .locals 1
    .param p1, "subId"    # I

    .prologue
    .line 463
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->configLoader:Lcom/android/phone/CarrierConfigLoader;

    invoke-virtual {v0, p1}, Lcom/android/phone/CarrierConfigLoader;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v0

    return-object v0
.end method

.method getKeyguardManager()Landroid/app/KeyguardManager;
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mKeyguardManager:Landroid/app/KeyguardManager;

    return-object v0
.end method

.method getPUKEntryActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mPUKEntryActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public getPhoneInEcm()Lcom/android/internal/telephony/Phone;
    .locals 1

    .prologue
    .line 925
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->phoneInEcm:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method public onCreate()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v9, 0x0

    .line 287
    invoke-virtual {p0}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 290
    .local v6, "resolver":Landroid/content/ContentResolver;
    invoke-static {}, Lcom/miui/livetalk/LivetalkApplication;->onCreateApplication()V

    .line 296
    invoke-virtual {p0}, Lcom/android/phone/PhoneGlobals;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x11200ca

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    .line 295
    sput-boolean v8, Lcom/android/phone/PhoneGlobals;->sVoiceCapable:Z

    .line 302
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    if-nez v8, :cond_2

    .line 304
    invoke-static {p0}, Lcom/android/internal/telephony/PhoneFactory;->makeDefaultPhones(Landroid/content/Context;)V

    .line 307
    new-instance v3, Landroid/content/Intent;

    const-class v8, Lcom/android/phone/TelephonyDebugService;

    invoke-direct {v3, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 308
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/android/phone/PhoneGlobals;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 310
    invoke-static {}, Lcom/android/internal/telephony/CallManager;->getInstance()Lcom/android/internal/telephony/CallManager;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    .line 311
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v10

    array-length v11, v10

    move v8, v9

    :goto_0
    if-ge v8, v11, :cond_0

    aget-object v5, v10, v8

    .line 312
    .local v5, "phone":Lcom/android/internal/telephony/Phone;
    iget-object v12, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v12, v5}, Lcom/android/internal/telephony/CallManager;->registerPhone(Lcom/android/internal/telephony/Phone;)Z

    .line 311
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 317
    .end local v5    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_0
    invoke-static {p0}, Lcom/android/phone/NotificationMgr;->init(Lcom/android/phone/PhoneGlobals;)Lcom/android/phone/NotificationMgr;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    .line 320
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    const/16 v10, 0xd

    invoke-virtual {v8, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 323
    new-instance v8, Lcom/android/phone/CdmaPhoneCallState;

    invoke-direct {v8}, Lcom/android/phone/CdmaPhoneCallState;-><init>()V

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->cdmaPhoneCallState:Lcom/android/phone/CdmaPhoneCallState;

    .line 324
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->cdmaPhoneCallState:Lcom/android/phone/CdmaPhoneCallState;

    invoke-virtual {v8}, Lcom/android/phone/CdmaPhoneCallState;->CdmaPhoneCallStateInit()V

    .line 327
    const-string/jumbo v8, "power"

    invoke-virtual {p0, v8}, Lcom/android/phone/PhoneGlobals;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/PowerManager;

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->mPowerManager:Landroid/os/PowerManager;

    .line 328
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mPowerManager:Landroid/os/PowerManager;

    const-string/jumbo v10, "PhoneGlobals"

    const/16 v11, 0x1a

    invoke-virtual {v8, v11, v10}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 330
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mPowerManager:Landroid/os/PowerManager;

    .line 331
    const-string/jumbo v10, "PhoneGlobals"

    .line 330
    const v11, 0x20000001

    invoke-virtual {v8, v11, v10}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 333
    const-string/jumbo v8, "keyguard"

    invoke-virtual {p0, v8}, Lcom/android/phone/PhoneGlobals;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/KeyguardManager;

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 337
    new-instance v8, Landroid/os/UpdateLock;

    const-string/jumbo v10, "phone"

    invoke-direct {v8, v10}, Landroid/os/UpdateLock;-><init>(Ljava/lang/String;)V

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->mUpdateLock:Landroid/os/UpdateLock;

    .line 339
    sget-boolean v8, Lcom/android/phone/PhoneGlobals;->DBG:Z

    if-eqz v8, :cond_1

    const-string/jumbo v8, "PhoneGlobals"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "onCreate: mUpdateLock: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/phone/PhoneGlobals;->mUpdateLock:Landroid/os/UpdateLock;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :cond_1
    new-instance v1, Lcom/android/phone/CallLogger;

    new-instance v8, Lcom/android/phone/common/CallLogAsync;

    invoke-direct {v8}, Lcom/android/phone/common/CallLogAsync;-><init>()V

    invoke-direct {v1, p0, v8}, Lcom/android/phone/CallLogger;-><init>(Lcom/android/phone/PhoneGlobals;Lcom/android/phone/common/CallLogAsync;)V

    .line 343
    .local v1, "callLogger":Lcom/android/phone/CallLogger;
    invoke-static {}, Lcom/android/phone/CallGatewayManager;->getInstance()Lcom/android/phone/CallGatewayManager;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->callGatewayManager:Lcom/android/phone/CallGatewayManager;

    .line 348
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->callGatewayManager:Lcom/android/phone/CallGatewayManager;

    invoke-static {p0, v1, v8}, Lcom/android/phone/CallController;->init(Lcom/android/phone/PhoneGlobals;Lcom/android/phone/CallLogger;Lcom/android/phone/CallGatewayManager;)Lcom/android/phone/CallController;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->callController:Lcom/android/phone/CallController;

    .line 354
    invoke-static {p0}, Lcom/android/phone/CallerInfoCache;->init(Landroid/content/Context;)Lcom/android/phone/CallerInfoCache;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->callerInfoCache:Lcom/android/phone/CallerInfoCache;

    .line 356
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/android/phone/PhoneInterfaceManager;->init(Lcom/android/phone/PhoneGlobals;Lcom/android/internal/telephony/Phone;)Lcom/android/phone/PhoneInterfaceManager;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    .line 358
    invoke-static {p0}, Lcom/android/phone/CarrierConfigLoader;->init(Landroid/content/Context;)Lcom/android/phone/CarrierConfigLoader;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->configLoader:Lcom/android/phone/CarrierConfigLoader;

    .line 364
    invoke-static {p0}, Lcom/android/phone/CallNotifier;->init(Lcom/android/phone/PhoneGlobals;)Lcom/android/phone/CallNotifier;

    move-result-object v8

    iput-object v8, p0, Lcom/android/phone/PhoneGlobals;->notifier:Lcom/android/phone/CallNotifier;

    .line 366
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x3

    invoke-static {v8, v10}, Lcom/android/phone/PhoneUtils;->registerIccStatus(Landroid/os/Handler;I)V

    .line 369
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    iget-object v10, p0, Lcom/android/phone/PhoneGlobals;->mHandler:Landroid/os/Handler;

    const/16 v11, 0x34

    invoke-virtual {v8, v10, v11, v13}, Lcom/android/internal/telephony/CallManager;->registerForMmiComplete(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 372
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-static {v8}, Lcom/android/phone/PhoneUtils;->initializeConnectionHandler(Lcom/android/internal/telephony/CallManager;)V

    .line 376
    new-instance v4, Landroid/content/IntentFilter;

    const-string/jumbo v8, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v4, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 377
    .local v4, "intentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v8, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 378
    const-string/jumbo v8, "android.intent.action.RADIO_TECHNOLOGY"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 379
    const-string/jumbo v8, "android.intent.action.SERVICE_STATE"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 380
    const-string/jumbo v8, "android.intent.action.EMERGENCY_CALLBACK_MODE_CHANGED"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 381
    const-string/jumbo v8, "android.intent.action.ACTION_DEFAULT_DATA_SUBSCRIPTION_CHANGED"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 382
    const-string/jumbo v8, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 383
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v4}, Lcom/android/phone/PhoneGlobals;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 385
    new-instance v7, Landroid/content/IntentFilter;

    const-string/jumbo v8, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v7, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 386
    .local v7, "sipIntentFilter":Landroid/content/IntentFilter;
    const-string/jumbo v8, "android.net.sip.SIP_SERVICE_UP"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 387
    const-string/jumbo v8, "com.android.phone.SIP_CALL_OPTION_CHANGED"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 388
    const-string/jumbo v8, "com.android.phone.SIP_REMOVE_PHONE"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 389
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mSipReceiver:Lcom/android/phone/PhoneGlobals$SipReceiver;

    invoke-virtual {p0, v8, v7}, Lcom/android/phone/PhoneGlobals;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 391
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mCarrierVvmPackageInstalledReceiver:Lcom/android/phone/vvm/CarrierVvmPackageInstalledReceiver;

    invoke-virtual {v8, p0}, Lcom/android/phone/vvm/CarrierVvmPackageInstalledReceiver;->register(Landroid/content/Context;)V

    .line 394
    const v8, 0x7f060028

    invoke-static {p0, v8, v9}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;IZ)V

    .line 396
    const v8, 0x7f060006

    invoke-static {p0, v8, v9}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;IZ)V

    .line 401
    iget-object v8, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-static {v8}, Lcom/android/phone/PhoneUtils;->setAudioMode(Lcom/android/internal/telephony/CallManager;)V

    .line 405
    .end local v1    # "callLogger":Lcom/android/phone/CallLogger;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "intentFilter":Landroid/content/IntentFilter;
    .end local v7    # "sipIntentFilter":Landroid/content/IntentFilter;
    :cond_2
    const-string/jumbo v8, "content://icc/adn"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    .line 411
    invoke-virtual {p0}, Lcom/android/phone/PhoneGlobals;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x7f0e0009

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 413
    invoke-virtual {p0}, Lcom/android/phone/PhoneGlobals;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 414
    const-string/jumbo v10, "hearing_aid"

    .line 412
    invoke-static {v8, v10, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 416
    .local v2, "hac":I
    const-string/jumbo v8, "audio"

    invoke-virtual {p0, v8}, Lcom/android/phone/PhoneGlobals;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 417
    .local v0, "audioManager":Landroid/media/AudioManager;
    const-string/jumbo v9, "HACSetting"

    .line 418
    const/4 v8, 0x1

    if-ne v2, v8, :cond_4

    .line 419
    const-string/jumbo v8, "ON"

    .line 417
    :goto_1
    invoke-virtual {v0, v9, v8}, Landroid/media/AudioManager;->setParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    .end local v2    # "hac":I
    :cond_3
    return-void

    .line 419
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    .restart local v2    # "hac":I
    :cond_4
    const-string/jumbo v8, "OFF"

    goto :goto_1
.end method

.method public refreshMwiIndicator(I)V
    .locals 1
    .param p1, "subId"    # I

    .prologue
    .line 934
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    invoke-virtual {v0, p1}, Lcom/android/phone/NotificationMgr;->refreshMwi(I)V

    .line 935
    return-void
.end method

.method requestWakeState(Lcom/android/phone/PhoneGlobals$WakeState;)V
    .locals 2
    .param p1, "ws"    # Lcom/android/phone/PhoneGlobals$WakeState;

    .prologue
    .line 531
    monitor-enter p0

    .line 532
    :try_start_0
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mWakeState:Lcom/android/phone/PhoneGlobals$WakeState;

    if-eq v0, p1, :cond_2

    .line 533
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->-getcom-android-phone-PhoneGlobals$WakeStateSwitchesValues()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/phone/PhoneGlobals$WakeState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 553
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 561
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/android/phone/PhoneGlobals;->mWakeState:Lcom/android/phone/PhoneGlobals$WakeState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    .line 564
    return-void

    .line 537
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 538
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 539
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 545
    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 546
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 547
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 533
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method setPukEntryActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 500
    iput-object p1, p0, Lcom/android/phone/PhoneGlobals;->mPUKEntryActivity:Landroid/app/Activity;

    .line 501
    return-void
.end method

.method setPukEntryProgressDialog(Lmiui/app/ProgressDialog;)V
    .locals 0
    .param p1, "dialog"    # Lmiui/app/ProgressDialog;

    .prologue
    .line 517
    iput-object p1, p0, Lcom/android/phone/PhoneGlobals;->mPUKEntryProgressDialog:Lmiui/app/ProgressDialog;

    .line 518
    return-void
.end method

.method public stopSignalInfoTone()V
    .locals 1

    .prologue
    .line 995
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->notifier:Lcom/android/phone/CallNotifier;

    invoke-virtual {v0}, Lcom/android/phone/CallNotifier;->stopSignalInfoTone()V

    .line 996
    return-void
.end method

.method updateWakeState()V
    .locals 7

    .prologue
    .line 591
    iget-object v5, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v5}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v4

    .line 601
    .local v4, "state":Lcom/android/internal/telephony/PhoneConstants$State;
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    if-ne v4, v5, :cond_0

    invoke-static {p0}, Lcom/android/phone/PhoneUtils;->isSpeakerOn(Landroid/content/Context;)Z

    move-result v2

    .line 618
    :goto_0
    sget-object v5, Lcom/android/internal/telephony/PhoneConstants$State;->RINGING:Lcom/android/internal/telephony/PhoneConstants$State;

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 619
    .local v1, "isRinging":Z
    :goto_1
    iget-object v5, p0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v5}, Lcom/android/internal/telephony/CallManager;->getFgPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v5

    sget-object v6, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    if-ne v5, v6, :cond_2

    const/4 v0, 0x1

    .line 620
    .local v0, "isDialing":Z
    :goto_2
    if-nez v1, :cond_3

    move v3, v0

    .line 622
    :goto_3
    if-eqz v3, :cond_4

    sget-object v5, Lcom/android/phone/PhoneGlobals$WakeState;->FULL:Lcom/android/phone/PhoneGlobals$WakeState;

    :goto_4
    invoke-virtual {p0, v5}, Lcom/android/phone/PhoneGlobals;->requestWakeState(Lcom/android/phone/PhoneGlobals$WakeState;)V

    .line 623
    return-void

    .line 601
    .end local v0    # "isDialing":Z
    .end local v1    # "isRinging":Z
    :cond_0
    const/4 v2, 0x0

    .local v2, "isSpeakerInUse":Z
    goto :goto_0

    .line 618
    .end local v2    # "isSpeakerInUse":Z
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "isRinging":Z
    goto :goto_1

    .line 619
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "isDialing":Z
    goto :goto_2

    .line 620
    :cond_3
    const/4 v3, 0x1

    .local v3, "keepScreenOn":Z
    goto :goto_3

    .line 622
    .end local v3    # "keepScreenOn":Z
    :cond_4
    sget-object v5, Lcom/android/phone/PhoneGlobals$WakeState;->SLEEP:Lcom/android/phone/PhoneGlobals$WakeState;

    goto :goto_4
.end method

.method wakeUpScreen()V
    .locals 4

    .prologue
    .line 571
    monitor-enter p0

    .line 572
    :try_start_0
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mWakeState:Lcom/android/phone/PhoneGlobals$WakeState;

    sget-object v1, Lcom/android/phone/PhoneGlobals$WakeState;->SLEEP:Lcom/android/phone/PhoneGlobals$WakeState;

    if-ne v0, v1, :cond_1

    .line 573
    sget-boolean v0, Lcom/android/phone/PhoneGlobals;->DBG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "PhoneGlobals"

    const-string/jumbo v1, "pulse screen lock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    :cond_0
    iget-object v0, p0, Lcom/android/phone/PhoneGlobals;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-string/jumbo v1, "android.phone:WAKE"

    invoke-virtual {v0, v2, v3, v1}, Landroid/os/PowerManager;->wakeUp(JLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    .line 577
    return-void

    .line 571
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
