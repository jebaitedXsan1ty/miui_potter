.class public Lcom/android/phone/MiuiPhoneApp;
.super Lmiui/external/Application;
.source "MiuiPhoneApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/phone/MiuiPhoneApp$Delegate;
    }
.end annotation


# static fields
.field private static final IS_INTERNATIONAL_BUILD:Z


# instance fields
.field private mDcStatMonitor:Lcom/android/phone/utils/DcStatMonitor;

.field mMiuiNotificationMgr:Lcom/android/phone/MiuiNotificationMgr;

.field mPhoneGlobals:Lcom/android/phone/PhoneGlobals;

.field private mSimCardStatMonitor:Lcom/android/phone/utils/SimCardStatMonitor;

.field mTelephonyGlobals:Lcom/android/services/telephony/TelephonyGlobals;


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Lcom/android/phone/MiuiPhoneApp;->IS_INTERNATIONAL_BUILD:Z

    return v0
.end method

.method static synthetic -set0(Lcom/android/phone/MiuiPhoneApp;Lcom/android/phone/utils/DcStatMonitor;)Lcom/android/phone/utils/DcStatMonitor;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MiuiPhoneApp;
    .param p1, "-value"    # Lcom/android/phone/utils/DcStatMonitor;

    .prologue
    iput-object p1, p0, Lcom/android/phone/MiuiPhoneApp;->mDcStatMonitor:Lcom/android/phone/utils/DcStatMonitor;

    return-object p1
.end method

.method static synthetic -set1(Lcom/android/phone/MiuiPhoneApp;Lcom/android/phone/utils/SimCardStatMonitor;)Lcom/android/phone/utils/SimCardStatMonitor;
    .locals 0
    .param p0, "-this"    # Lcom/android/phone/MiuiPhoneApp;
    .param p1, "-value"    # Lcom/android/phone/utils/SimCardStatMonitor;

    .prologue
    iput-object p1, p0, Lcom/android/phone/MiuiPhoneApp;->mSimCardStatMonitor:Lcom/android/phone/utils/SimCardStatMonitor;

    return-object p1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    .line 38
    sput-boolean v0, Lcom/android/phone/MiuiPhoneApp;->IS_INTERNATIONAL_BUILD:Z

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lmiui/external/Application;-><init>()V

    .line 42
    return-void
.end method


# virtual methods
.method public onCreateApplicationDelegate()Lmiui/external/ApplicationDelegate;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lcom/android/phone/MiuiPhoneApp$Delegate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/phone/MiuiPhoneApp$Delegate;-><init>(Lcom/android/phone/MiuiPhoneApp;Lcom/android/phone/MiuiPhoneApp$Delegate;)V

    return-object v0
.end method
