.class final Lcom/android/services/telephony/PstnIncomingCallNotifier;
.super Ljava/lang/Object;
.source "PstnIncomingCallNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/PstnIncomingCallNotifier$1;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method static synthetic -wrap0(Lcom/android/services/telephony/PstnIncomingCallNotifier;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/PstnIncomingCallNotifier;
    .param p1, "asyncResult"    # Landroid/os/AsyncResult;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->handleCdmaCallWaiting(Landroid/os/AsyncResult;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/PstnIncomingCallNotifier;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/PstnIncomingCallNotifier;
    .param p1, "asyncResult"    # Landroid/os/AsyncResult;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->handleNewRingingConnection(Landroid/os/AsyncResult;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/services/telephony/PstnIncomingCallNotifier;Landroid/os/AsyncResult;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/PstnIncomingCallNotifier;
    .param p1, "asyncResult"    # Landroid/os/AsyncResult;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->handleNewUnknownConnection(Landroid/os/AsyncResult;)V

    return-void
.end method

.method constructor <init>(Lcom/android/internal/telephony/Phone;)V
    .locals 1
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Lcom/android/services/telephony/PstnIncomingCallNotifier$1;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/PstnIncomingCallNotifier$1;-><init>(Lcom/android/services/telephony/PstnIncomingCallNotifier;)V

    iput-object v0, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mHandler:Landroid/os/Handler;

    .line 89
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iput-object p1, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 93
    invoke-direct {p0}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->registerForNotifications()V

    .line 94
    return-void
.end method

.method private addNewUnknownCall(Lcom/android/internal/telephony/Connection;)V
    .locals 10
    .param p1, "connection"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 200
    const-string/jumbo v5, "addNewUnknownCall, connection is: %s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p1, v6, v8

    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/phone/PhoneGlobals;->stopSignalInfoTone()V

    .line 204
    invoke-direct {p0, p1}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->maybeSwapAnyWithUnknownConnection(Lcom/android/internal/telephony/Connection;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 205
    const-string/jumbo v5, "determined new connection is: %s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p1, v6, v8

    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 207
    .local v2, "extras":Landroid/os/Bundle;
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getNumberPresentation()I

    move-result v5

    if-ne v5, v7, :cond_0

    .line 208
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    .line 207
    if-eqz v5, :cond_0

    .line 209
    const-string/jumbo v5, "tel"

    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v9}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 210
    .local v4, "uri":Landroid/net/Uri;
    const-string/jumbo v5, "android.telecom.extra.UNKNOWN_CALL_HANDLE"

    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 216
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_0
    instance-of v5, p1, Lcom/android/internal/telephony/imsphone/ImsExternalConnection;

    if-eqz v5, :cond_1

    move-object v1, p1

    .line 217
    check-cast v1, Lcom/android/internal/telephony/imsphone/ImsExternalConnection;

    .line 218
    .local v1, "externalConnection":Lcom/android/internal/telephony/imsphone/ImsExternalConnection;
    const-string/jumbo v5, "android.telephony.ImsExternalCallTracker.extra.EXTERNAL_CALL_ID"

    .line 219
    invoke-virtual {v1}, Lcom/android/internal/telephony/imsphone/ImsExternalConnection;->getCallId()I

    move-result v6

    .line 218
    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    .end local v1    # "externalConnection":Lcom/android/internal/telephony/imsphone/ImsExternalConnection;
    :cond_1
    const-string/jumbo v5, "android.telecom.extra.CALL_CREATED_TIME_MILLIS"

    .line 224
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 223
    invoke-virtual {v2, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 226
    invoke-direct {p0}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->findCorrectPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    .line 227
    .local v3, "handle":Landroid/telecom/PhoneAccountHandle;
    if-nez v3, :cond_2

    .line 229
    :try_start_0
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->hangup()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    .end local v2    # "extras":Landroid/os/Bundle;
    .end local v3    # "handle":Landroid/telecom/PhoneAccountHandle;
    :goto_0
    return-void

    .line 230
    .restart local v2    # "extras":Landroid/os/Bundle;
    .restart local v3    # "handle":Landroid/telecom/PhoneAccountHandle;
    :catch_0
    move-exception v0

    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    goto :goto_0

    .line 234
    .end local v0    # "e":Lcom/android/internal/telephony/CallStateException;
    :cond_2
    iget-object v5, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v5

    invoke-virtual {v5, v3, v2}, Landroid/telecom/TelecomManager;->addNewUnknownCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto :goto_0

    .line 237
    .end local v2    # "extras":Landroid/os/Bundle;
    .end local v3    # "handle":Landroid/telecom/PhoneAccountHandle;
    :cond_3
    const-string/jumbo v5, "swapped an old connection, new one is: %s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p1, v6, v8

    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private findCorrectPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 278
    invoke-static {v7}, Lcom/android/services/telephony/TelecomAccountRegistry;->getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;

    move-result-object v2

    .line 280
    .local v2, "telecomAccountRegistry":Lcom/android/services/telephony/TelecomAccountRegistry;
    iget-object v3, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-static {v3}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 281
    .local v1, "handle":Landroid/telecom/PhoneAccountHandle;
    invoke-virtual {v2, v1}, Lcom/android/services/telephony/TelecomAccountRegistry;->hasAccountEntryForPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 282
    return-object v1

    .line 289
    :cond_0
    iget-object v3, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    const-string/jumbo v4, ""

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandleWithPrefix(Lcom/android/internal/telephony/Phone;Ljava/lang/String;Z)Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    .line 290
    .local v0, "emergencyHandle":Landroid/telecom/PhoneAccountHandle;
    invoke-virtual {v2, v0}, Lcom/android/services/telephony/TelecomAccountRegistry;->hasAccountEntryForPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 291
    const-string/jumbo v3, "Receiving MT call in ECM. Using Emergency PhoneAccount Instead."

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 292
    return-object v0

    .line 294
    :cond_1
    const-string/jumbo v3, "PhoneAccount not found."

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 295
    return-object v7
.end method

.method private handleCdmaCallWaiting(Landroid/os/AsyncResult;)V
    .locals 9
    .param p1, "asyncResult"    # Landroid/os/AsyncResult;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 145
    const-string/jumbo v5, "handleCdmaCallWaiting"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;

    .line 147
    .local v1, "ccwi":Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;
    iget-object v5, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 148
    .local v0, "call":Lcom/android/internal/telephony/Call;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v5

    sget-object v6, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    if-ne v5, v6, :cond_0

    .line 149
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    .line 150
    .local v2, "connection":Lcom/android/internal/telephony/Connection;
    if-eqz v2, :cond_0

    .line 151
    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v3

    .line 152
    .local v3, "number":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->getNumberPresentation()I

    move-result v4

    .line 154
    .local v4, "presentation":I
    if-eq v4, v7, :cond_1

    .line 155
    iget v5, v1, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;->numberPresentation:I

    if-ne v4, v5, :cond_1

    .line 158
    const-string/jumbo v5, "handleCdmaCallWaiting: inform telecom of waiting call; presentation = %d"

    new-array v6, v7, [Ljava/lang/Object;

    .line 159
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    .line 158
    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    invoke-direct {p0, v2}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->sendIncomingCallIntent(Lcom/android/internal/telephony/Connection;)V

    .line 173
    .end local v2    # "connection":Lcom/android/internal/telephony/Connection;
    .end local v3    # "number":Ljava/lang/String;
    .end local v4    # "presentation":I
    :cond_0
    :goto_0
    return-void

    .line 161
    .restart local v2    # "connection":Lcom/android/internal/telephony/Connection;
    .restart local v3    # "number":Ljava/lang/String;
    .restart local v4    # "presentation":I
    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, v1, Lcom/android/internal/telephony/cdma/CdmaCallWaitingNotification;->number:Ljava/lang/String;

    invoke-static {v3, v5}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 164
    const-string/jumbo v5, "handleCdmaCallWaiting: inform telecom of waiting call; number = %s"

    new-array v6, v7, [Ljava/lang/Object;

    .line 165
    invoke-static {v3}, Lcom/android/services/telephony/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    .line 164
    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 166
    invoke-direct {p0, v2}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->sendIncomingCallIntent(Lcom/android/internal/telephony/Connection;)V

    goto :goto_0

    .line 168
    :cond_2
    const-string/jumbo v5, "handleCdmaCallWaiting: presentation or number do not match, not informing telecom of call: %s"

    new-array v6, v7, [Ljava/lang/Object;

    .line 169
    aput-object v1, v6, v8

    .line 168
    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private handleNewRingingConnection(Landroid/os/AsyncResult;)V
    .locals 4
    .param p1, "asyncResult"    # Landroid/os/AsyncResult;

    .prologue
    .line 127
    const-string/jumbo v2, "handleNewRingingConnection"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, Lcom/android/internal/telephony/Connection;

    .line 129
    .local v1, "connection":Lcom/android/internal/telephony/Connection;
    if-eqz v1, :cond_0

    .line 130
    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 133
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Call$State;->isRinging()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    invoke-static {v1}, Lcom/android/services/telephony/LivetalkTelephonyHelper;->maybeLivetalkCallbackConnection(Lcom/android/internal/telephony/Connection;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 137
    invoke-direct {p0, v1}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->sendIncomingCallIntent(Lcom/android/internal/telephony/Connection;)V

    .line 142
    .end local v0    # "call":Lcom/android/internal/telephony/Call;
    :cond_0
    return-void
.end method

.method private handleNewUnknownConnection(Landroid/os/AsyncResult;)V
    .locals 6
    .param p1, "asyncResult"    # Landroid/os/AsyncResult;

    .prologue
    const/4 v5, 0x0

    .line 176
    const-string/jumbo v3, "handleNewUnknownConnection"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    iget-object v3, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    instance-of v3, v3, Lcom/android/internal/telephony/Connection;

    if-nez v3, :cond_0

    .line 178
    const-string/jumbo v3, "handleNewUnknownConnection called with non-Connection object"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 179
    return-void

    .line 181
    :cond_0
    iget-object v1, p1, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, Lcom/android/internal/telephony/Connection;

    .line 182
    .local v1, "connection":Lcom/android/internal/telephony/Connection;
    if-eqz v1, :cond_3

    .line 186
    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    .line 187
    .local v2, "state":Lcom/android/internal/telephony/Call$State;
    sget-object v3, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    if-ne v2, v3, :cond_2

    .line 188
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Skipping new unknown connection because it is idle. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    return-void

    .line 192
    :cond_2
    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 193
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call$State;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 194
    invoke-direct {p0, v1}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->addNewUnknownCall(Lcom/android/internal/telephony/Connection;)V

    .line 197
    .end local v0    # "call":Lcom/android/internal/telephony/Call;
    .end local v2    # "state":Lcom/android/internal/telephony/Call$State;
    :cond_3
    return-void
.end method

.method private maybeSwapAnyWithUnknownConnection(Lcom/android/internal/telephony/Connection;)Z
    .locals 6
    .param p1, "unknown"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v5, 0x0

    .line 308
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v4

    if-nez v4, :cond_1

    .line 309
    invoke-static {v5}, Lcom/android/services/telephony/TelecomAccountRegistry;->getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;

    move-result-object v0

    .line 310
    .local v0, "registry":Lcom/android/services/telephony/TelecomAccountRegistry;
    if-eqz v0, :cond_1

    .line 311
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry;->getTelephonyConnectionService()Lcom/android/services/telephony/TelephonyConnectionService;

    move-result-object v1

    .line 312
    .local v1, "service":Lcom/android/services/telephony/TelephonyConnectionService;
    if-eqz v1, :cond_1

    .line 313
    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "telephonyConnection$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/Connection;

    .line 315
    .local v2, "telephonyConnection":Landroid/telecom/Connection;
    instance-of v4, v2, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v4, :cond_0

    .line 317
    check-cast v2, Lcom/android/services/telephony/TelephonyConnection;

    .line 316
    .end local v2    # "telephonyConnection":Landroid/telecom/Connection;
    invoke-direct {p0, v2, p1}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->maybeSwapWithUnknownConnection(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Connection;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 319
    const/4 v4, 0x1

    return v4

    .line 326
    .end local v0    # "registry":Lcom/android/services/telephony/TelecomAccountRegistry;
    .end local v1    # "service":Lcom/android/services/telephony/TelephonyConnectionService;
    .end local v3    # "telephonyConnection$iterator":Ljava/util/Iterator;
    :cond_1
    const/4 v4, 0x0

    return v4
.end method

.method private maybeSwapWithUnknownConnection(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Connection;)Z
    .locals 7
    .param p1, "telephonyConnection"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "unknown"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 332
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    .line 333
    .local v0, "original":Lcom/android/internal/telephony/Connection;
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_5

    .line 334
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    .line 333
    if-eqz v3, :cond_5

    .line 338
    instance-of v3, p2, Lcom/android/internal/telephony/imsphone/ImsExternalConnection;

    if-eqz v3, :cond_0

    .line 339
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v3

    instance-of v3, v3, Lcom/android/internal/telephony/imsphone/ImsExternalConnection;

    xor-int/lit8 v3, v3, 0x1

    .line 338
    if-eqz v3, :cond_0

    .line 341
    const-string/jumbo v3, "maybeSwapWithUnknownConnection - not swapping regular connection with external connection."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 343
    return v5

    .line 348
    :cond_0
    instance-of v3, v0, Lcom/android/internal/telephony/GsmCdmaConnection;

    if-eqz v3, :cond_2

    instance-of v3, p2, Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;

    if-eqz v3, :cond_2

    .line 349
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    .line 350
    .local v1, "parentCall":Lcom/android/internal/telephony/Call;
    if-eqz v1, :cond_2

    .line 351
    iget-object v3, v1, Lcom/android/internal/telephony/Call;->mConnections:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 352
    iget-object v3, v1, Lcom/android/internal/telephony/Call;->mConnections:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 353
    sget-object v3, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    iput-object v3, v1, Lcom/android/internal/telephony/Call;->mState:Lcom/android/internal/telephony/Call$State;

    :cond_1
    move-object v3, v0

    .line 355
    check-cast v3, Lcom/android/internal/telephony/GsmCdmaConnection;

    invoke-virtual {v3}, Lcom/android/internal/telephony/GsmCdmaConnection;->dispose()V

    .line 359
    .end local v1    # "parentCall":Lcom/android/internal/telephony/Call;
    :cond_2
    invoke-virtual {p1, p2}, Lcom/android/services/telephony/TelephonyConnection;->setOriginalConnection(Lcom/android/internal/telephony/Connection;)V

    .line 363
    instance-of v3, v0, Lcom/android/internal/telephony/imsphone/ImsExternalConnection;

    if-eqz v3, :cond_3

    .line 364
    return v6

    .line 369
    :cond_3
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 370
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    instance-of v3, v3, Lcom/android/internal/telephony/GsmCdmaPhone;

    .line 369
    if-eqz v3, :cond_4

    .line 372
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    check-cast v2, Lcom/android/internal/telephony/GsmCdmaPhone;

    .line 373
    .local v2, "phone":Lcom/android/internal/telephony/GsmCdmaPhone;
    invoke-virtual {v2}, Lcom/android/internal/telephony/GsmCdmaPhone;->getCallTracker()Lcom/android/internal/telephony/CallTracker;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/CallTracker;->cleanupCalls()V

    .line 374
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "maybeSwapWithUnknownConnection - Invoking call tracker cleanup for connection: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 377
    .end local v2    # "phone":Lcom/android/internal/telephony/GsmCdmaPhone;
    :cond_4
    return v6

    .line 379
    :cond_5
    return v5
.end method

.method private registerForNotifications()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 104
    iget-object v0, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v0, :cond_0

    .line 105
    const-string/jumbo v0, "Registering: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    iget-object v0, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/internal/telephony/Phone;->registerForNewRingingConnection(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/internal/telephony/Phone;->registerForCallWaiting(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2, v4}, Lcom/android/internal/telephony/Phone;->registerForUnknownConnection(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 110
    :cond_0
    return-void
.end method

.method private sendIncomingCallIntent(Lcom/android/internal/telephony/Connection;)V
    .locals 8
    .param p1, "connection"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v6, 0x0

    .line 246
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/phone/PhoneGlobals;->stopSignalInfoTone()V

    .line 247
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 248
    .local v1, "extras":Landroid/os/Bundle;
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getNumberPresentation()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 249
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    .line 248
    if-eqz v4, :cond_0

    .line 250
    const-string/jumbo v4, "tel"

    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v6}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 251
    .local v3, "uri":Landroid/net/Uri;
    const-string/jumbo v4, "android.telecom.extra.INCOMING_CALL_ADDRESS"

    invoke-virtual {v1, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 255
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_0
    const-string/jumbo v4, "android.telecom.extra.CALL_CREATED_TIME_MILLIS"

    .line 256
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 255
    invoke-virtual {v1, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 258
    invoke-direct {p0}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->findCorrectPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    .line 259
    .local v2, "handle":Landroid/telecom/PhoneAccountHandle;
    if-nez v2, :cond_1

    .line 261
    :try_start_0
    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->hangup()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :goto_0
    return-void

    .line 262
    :catch_0
    move-exception v0

    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    goto :goto_0

    .line 266
    .end local v0    # "e":Lcom/android/internal/telephony/CallStateException;
    :cond_1
    iget-object v4, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v4

    invoke-virtual {v4, v2, v1}, Landroid/telecom/TelecomManager;->addNewIncomingCall(Landroid/telecom/PhoneAccountHandle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private unregisterForNotifications()V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v0, :cond_0

    .line 114
    const-string/jumbo v0, "Unregistering: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForNewRingingConnection(Landroid/os/Handler;)V

    .line 116
    iget-object v0, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForCallWaiting(Landroid/os/Handler;)V

    .line 117
    iget-object v0, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/PstnIncomingCallNotifier;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForUnknownConnection(Landroid/os/Handler;)V

    .line 119
    :cond_0
    return-void
.end method


# virtual methods
.method teardown()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/android/services/telephony/PstnIncomingCallNotifier;->unregisterForNotifications()V

    .line 98
    return-void
.end method
