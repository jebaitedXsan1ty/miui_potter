.class public Lcom/android/services/telephony/DisconnectCauseUtil;
.super Ljava/lang/Object;
.source "DisconnectCauseUtil.java"


# static fields
.field public static mNotificationCode:I

.field public static mNotificationType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/16 v0, 0xff

    .line 33
    sput v0, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationCode:I

    .line 34
    sput v0, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationType:I

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toTelecomDisconnectCause(I)Landroid/telecom/DisconnectCause;
    .locals 1
    .param p0, "telephonyDisconnectCause"    # I

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(II)Landroid/telecom/DisconnectCause;

    move-result-object v0

    return-object v0
.end method

.method public static toTelecomDisconnectCause(II)Landroid/telecom/DisconnectCause;
    .locals 1
    .param p0, "telephonyDisconnectCause"    # I
    .param p1, "phoneId"    # I

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v0

    return-object v0
.end method

.method public static toTelecomDisconnectCause(ILjava/lang/String;)Landroid/telecom/DisconnectCause;
    .locals 1
    .param p0, "telephonyDisconnectCause"    # I
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v0

    return-object v0
.end method

.method public static toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;
    .locals 7
    .param p0, "telephonyDisconnectCause"    # I
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "phoneId"    # I

    .prologue
    .line 98
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v6

    .line 99
    .local v6, "context":Landroid/content/Context;
    new-instance v0, Landroid/telecom/DisconnectCause;

    .line 100
    invoke-static {p0}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCauseCode(I)I

    move-result v1

    .line 101
    invoke-static {v6, p0}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCauseLabel(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 102
    invoke-static {v6, p0, p2}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCauseDescription(Landroid/content/Context;II)Ljava/lang/CharSequence;

    move-result-object v3

    .line 103
    invoke-static {v6, p0, p1, p2}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectReason(Landroid/content/Context;ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 104
    invoke-static {p0}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCauseTone(I)I

    move-result v5

    .line 99
    invoke-direct/range {v0 .. v5}, Landroid/telecom/DisconnectCause;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static toTelecomDisconnectCause(ILjava/lang/String;III)Landroid/telecom/DisconnectCause;
    .locals 1
    .param p0, "telephonyDisconnectCause"    # I
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "code"    # I
    .param p4, "phoneId"    # I

    .prologue
    .line 70
    sput p3, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationCode:I

    .line 71
    sput p2, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationType:I

    .line 72
    invoke-static {p0, p1, p4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v0

    return-object v0
.end method

.method private static toTelecomDisconnectCauseCode(I)I
    .locals 4
    .param p0, "telephonyDisconnectCause"    # I

    .prologue
    const/4 v3, 0x0

    .line 113
    packed-switch p0, :pswitch_data_0

    .line 251
    :pswitch_0
    const-string/jumbo v0, "DisconnectCauseUtil.toTelecomDisconnectCauseCode"

    .line 252
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unrecognized Telephony DisconnectCause "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 251
    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    return v3

    .line 115
    :pswitch_1
    const/4 v0, 0x2

    return v0

    .line 118
    :pswitch_2
    const/4 v0, 0x3

    return v0

    .line 121
    :pswitch_3
    const/4 v0, 0x4

    return v0

    .line 124
    :pswitch_4
    const/4 v0, 0x5

    return v0

    .line 127
    :pswitch_5
    const/4 v0, 0x6

    return v0

    .line 130
    :pswitch_6
    const/4 v0, 0x7

    return v0

    .line 142
    :pswitch_7
    const/16 v0, 0x8

    return v0

    .line 189
    :pswitch_8
    const/4 v0, 0x1

    return v0

    .line 238
    :pswitch_9
    const/16 v0, 0x9

    return v0

    .line 242
    :pswitch_a
    return v3

    .line 245
    :pswitch_b
    const/16 v0, 0xc

    return v0

    .line 248
    :pswitch_c
    const/16 v0, 0xb

    return v0

    .line 113
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_a
        :pswitch_a
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_8
        :pswitch_3
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_b
        :pswitch_c
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method private static toTelecomDisconnectCauseDescription(Landroid/content/Context;II)Ljava/lang/CharSequence;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "telephonyDisconnectCause"    # I
    .param p2, "phoneId"    # I

    .prologue
    const v3, 0x7f0b04a9

    const v2, 0x7f0b04a8

    .line 362
    if-nez p0, :cond_0

    .line 363
    const-string/jumbo v1, ""

    return-object v1

    .line 366
    :cond_0
    const/4 v0, 0x0

    .line 367
    .local v0, "resourceId":Ljava/lang/Integer;
    packed-switch p1, :pswitch_data_0

    .line 752
    .end local v0    # "resourceId":Ljava/lang/Integer;
    :cond_1
    :goto_0
    :pswitch_0
    if-nez v0, :cond_9

    const-string/jumbo v1, ""

    :goto_1
    return-object v1

    .line 372
    .restart local v0    # "resourceId":Ljava/lang/Integer;
    :pswitch_1
    sget v1, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 373
    sget v1, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationCode:I

    .line 374
    const/16 v2, 0xa

    .line 373
    if-ne v1, v2, :cond_1

    .line 375
    const v1, 0x7f0b03a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 385
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_2
    sget v1, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationType:I

    if-nez v1, :cond_2

    .line 386
    sget v1, Lcom/android/services/telephony/DisconnectCauseUtil;->mNotificationCode:I

    .line 387
    const/4 v2, 0x6

    .line 386
    if-ne v1, v2, :cond_2

    .line 388
    const v1, 0x7f0b039f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 390
    .local v0, "resourceId":Ljava/lang/Integer;
    :cond_2
    const v1, 0x7f0b025a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 396
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_3
    const v1, 0x7f0b0507

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 400
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_4
    const v1, 0x7f0b0251

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 404
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_5
    const v1, 0x7f0b025b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 408
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_6
    const v1, 0x7f0b025c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 412
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_7
    const v1, 0x7f0b025d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 416
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_8
    const v1, 0x7f0b0252

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 420
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_9
    const v1, 0x7f0b0253

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 424
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_a
    const v1, 0x7f0b0254

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 428
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_b
    const v1, 0x7f0b0255

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 432
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_c
    const v1, 0x7f0b0256

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 436
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_d
    const v1, 0x7f0b0257

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 440
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_e
    const v1, 0x7f0b0258

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 444
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_f
    const v1, 0x7f0b0259

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 452
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_10
    const v1, 0x7f0b049e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 461
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_11
    invoke-static {p0, p2}, Lcom/android/phone/ImsUtil;->shouldPromoteWfc(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 462
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 463
    .local v0, "resourceId":Ljava/lang/Integer;
    :cond_3
    invoke-static {p0, p2}, Lcom/android/phone/ImsUtil;->isWfcModeWifiOnly(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 464
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 465
    .local v0, "resourceId":Ljava/lang/Integer;
    :cond_4
    invoke-static {p0, p2}, Lcom/android/phone/ImsUtil;->isWfcEnabled(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 466
    const v1, 0x7f0b0498

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 468
    .local v0, "resourceId":Ljava/lang/Integer;
    :cond_5
    const v1, 0x7f0b0497

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 473
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_12
    const v1, 0x7f0b050e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 477
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_13
    const v1, 0x7f0b050d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 482
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_14
    const v1, 0x7f0b0499

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 488
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_15
    const v1, 0x7f0b049a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 493
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_16
    invoke-static {p0, p2}, Lcom/android/phone/ImsUtil;->shouldPromoteWfc(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 494
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 495
    .local v0, "resourceId":Ljava/lang/Integer;
    :cond_6
    invoke-static {p0, p2}, Lcom/android/phone/ImsUtil;->isWfcModeWifiOnly(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 496
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 497
    .local v0, "resourceId":Ljava/lang/Integer;
    :cond_7
    invoke-static {p0, p2}, Lcom/android/phone/ImsUtil;->isWfcEnabled(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 498
    const v1, 0x7f0b049c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 500
    .local v0, "resourceId":Ljava/lang/Integer;
    :cond_8
    const v1, 0x7f0b049b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 508
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_17
    const v1, 0x7f0b049d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 514
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_18
    const v1, 0x7f0b025f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 518
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_19
    const v1, 0x7f0b0260

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 522
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_1a
    const v1, 0x7f0b0261

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 526
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_1b
    const v1, 0x7f0b0265

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 529
    .end local v0    # "resourceId":Ljava/lang/Integer;
    :pswitch_1c
    const v1, 0x7f0b0244

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 533
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_1d
    const v1, 0x7f0b0202

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 537
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_1e
    const v1, 0x7f0b0212

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 541
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_1f
    const v1, 0x7f0b0213

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 545
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_20
    const v1, 0x7f0b0215

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 549
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_21
    const v1, 0x7f0b0217

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 553
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_22
    const v1, 0x7f0b021b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 557
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_23
    const v1, 0x7f0b024d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 560
    .end local v0    # "resourceId":Ljava/lang/Integer;
    :pswitch_24
    const v1, 0x7f0b020e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 564
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_25
    const v1, 0x7f0b025e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 568
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_26
    const v1, 0x7f0b0204

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 572
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_27
    const v1, 0x7f0b0208

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 576
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_28
    const v1, 0x7f0b0209

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 580
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_29
    const v1, 0x7f0b020d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 584
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_2a
    const v1, 0x7f0b021a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 588
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_2b
    const v1, 0x7f0b0205

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 592
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_2c
    const v1, 0x7f0b0206

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 596
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_2d
    const v1, 0x7f0b020a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 600
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_2e
    const v1, 0x7f0b020b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 604
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_2f
    const v1, 0x7f0b020c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 608
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_30
    const v1, 0x7f0b020f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 612
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_31
    const v1, 0x7f0b0210

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 616
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_32
    const v1, 0x7f0b0211

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 620
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_33
    const v1, 0x7f0b0203

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 624
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_34
    const v1, 0x7f0b0214

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 628
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_35
    const v1, 0x7f0b0216

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 632
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_36
    const v1, 0x7f0b0218

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 636
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_37
    const v1, 0x7f0b0219

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 640
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_38
    const v1, 0x7f0b021c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 644
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_39
    const v1, 0x7f0b021d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 648
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_3a
    const v1, 0x7f0b021e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 652
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_3b
    const v1, 0x7f0b021f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 656
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_3c
    const v1, 0x7f0b0220

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 660
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_3d
    const v1, 0x7f0b0221

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 664
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_3e
    const v1, 0x7f0b0222

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 668
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_3f
    const v1, 0x7f0b0223

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 672
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_40
    const v1, 0x7f0b0224

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 676
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_41
    const v1, 0x7f0b0225

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 680
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_42
    const v1, 0x7f0b0226

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 684
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_43
    const v1, 0x7f0b0227

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 688
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_44
    const v1, 0x7f0b0228

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 692
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_45
    const v1, 0x7f0b0229

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 696
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_46
    const v1, 0x7f0b022a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 700
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_47
    const v1, 0x7f0b022b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 704
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_48
    const v1, 0x7f0b022c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 708
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_49
    const v1, 0x7f0b022d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 712
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_4a
    const v1, 0x7f0b022e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 716
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_4b
    const v1, 0x7f0b0207

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 720
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_4c
    const v1, 0x7f0b022f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 731
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_4d
    const v1, 0x7f0b0266

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 735
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_4e
    const v1, 0x7f0b0267

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 738
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_4f
    const v1, 0x10403b4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 742
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_50
    const v1, 0x7f0b050b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 746
    .local v0, "resourceId":Ljava/lang/Integer;
    :pswitch_51
    const v1, 0x7f0b050c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 752
    .end local v0    # "resourceId":Ljava/lang/Integer;
    :cond_9
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 367
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4b
        :pswitch_0
        :pswitch_1c
        :pswitch_0
        :pswitch_0
        :pswitch_24
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_23
        :pswitch_0
        :pswitch_11
        :pswitch_16
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
        :pswitch_25
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_17
        :pswitch_0
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_3
        :pswitch_19
        :pswitch_1a
        :pswitch_0
        :pswitch_1b
        :pswitch_4d
        :pswitch_4e
        :pswitch_0
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_1e
        :pswitch_1f
        :pswitch_34
        :pswitch_20
        :pswitch_35
        :pswitch_21
        :pswitch_36
        :pswitch_37
        :pswitch_22
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_0
        :pswitch_12
        :pswitch_1d
        :pswitch_26
        :pswitch_2c
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4c
        :pswitch_0
        :pswitch_13
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method private static toTelecomDisconnectCauseLabel(Landroid/content/Context;I)Ljava/lang/CharSequence;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "telephonyDisconnectCause"    # I

    .prologue
    .line 263
    if-nez p0, :cond_0

    .line 264
    const-string/jumbo v1, ""

    return-object v1

    .line 267
    :cond_0
    const/4 v0, 0x0

    .line 268
    .local v0, "resourceId":Ljava/lang/Integer;
    sparse-switch p1, :sswitch_data_0

    .line 354
    .end local v0    # "resourceId":Ljava/lang/Integer;
    :goto_0
    if-nez v0, :cond_1

    const-string/jumbo v1, ""

    :goto_1
    return-object v1

    .line 270
    .restart local v0    # "resourceId":Ljava/lang/Integer;
    :sswitch_0
    const v1, 0x7f0b0244

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 274
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_1
    const v1, 0x7f0b0245

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 278
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_2
    const v1, 0x7f0b0246

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 282
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_3
    const v1, 0x7f0b0247

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 286
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_4
    const v1, 0x7f0b0248

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 290
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_5
    const v1, 0x7f0b0249

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 294
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_6
    const v1, 0x7f0b024b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 298
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_7
    const v1, 0x7f0b024a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 303
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_8
    const v1, 0x7f0b024c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 307
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_9
    const v1, 0x7f0b024d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 311
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_a
    const v1, 0x7f0b024e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 315
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_b
    const v1, 0x7f0b050e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 319
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_c
    const v1, 0x7f0b050d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 323
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_d
    const v1, 0x7f0b024f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 327
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_e
    const v1, 0x7f0b0250

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto :goto_0

    .line 332
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_f
    const v1, 0x7f0b025e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 336
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_10
    const v1, 0x7f0b0261

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 340
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_11
    const v1, 0x7f0b0265

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 344
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_12
    const v1, 0x7f0b0266

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 348
    .local v0, "resourceId":Ljava/lang/Integer;
    :sswitch_13
    const v1, 0x7f0b0268

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .local v0, "resourceId":Ljava/lang/Integer;
    goto/16 :goto_0

    .line 354
    .end local v0    # "resourceId":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 268
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x7 -> :sswitch_f
        0x8 -> :sswitch_4
        0x9 -> :sswitch_3
        0xa -> :sswitch_5
        0xb -> :sswitch_7
        0xc -> :sswitch_6
        0xd -> :sswitch_2
        0xe -> :sswitch_8
        0xf -> :sswitch_9
        0x11 -> :sswitch_a
        0x12 -> :sswitch_e
        0x13 -> :sswitch_d
        0x19 -> :sswitch_f
        0x1b -> :sswitch_8
        0x33 -> :sswitch_10
        0x35 -> :sswitch_11
        0x36 -> :sswitch_12
        0x37 -> :sswitch_13
        0x5f -> :sswitch_b
        0x6e -> :sswitch_c
    .end sparse-switch
.end method

.method private static toTelecomDisconnectCauseTone(I)I
    .locals 1
    .param p0, "telephonyDisconnectCause"    # I

    .prologue
    .line 799
    sparse-switch p0, :sswitch_data_0

    .line 835
    const/4 v0, -0x1

    return v0

    .line 801
    :sswitch_0
    const/16 v0, 0x11

    return v0

    .line 804
    :sswitch_1
    const/16 v0, 0x12

    return v0

    .line 807
    :sswitch_2
    const/16 v0, 0x26

    return v0

    .line 810
    :sswitch_3
    const/16 v0, 0x25

    return v0

    .line 814
    :sswitch_4
    const/16 v0, 0x5f

    return v0

    .line 824
    :sswitch_5
    const/16 v0, 0x15

    return v0

    .line 830
    :sswitch_6
    const/16 v0, 0x1b

    return v0

    .line 799
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_6
        0x3 -> :sswitch_6
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x12 -> :sswitch_4
        0x19 -> :sswitch_5
        0x1b -> :sswitch_4
        0x1c -> :sswitch_3
        0x1d -> :sswitch_2
        0x24 -> :sswitch_6
        0x32 -> :sswitch_6
        0x3c -> :sswitch_5
        0x51 -> :sswitch_5
        0x61 -> :sswitch_5
        0x62 -> :sswitch_5
        0x63 -> :sswitch_5
        0x65 -> :sswitch_5
        0x66 -> :sswitch_5
    .end sparse-switch
.end method

.method private static toTelecomDisconnectReason(Landroid/content/Context;ILjava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "telephonyDisconnectCause"    # I
    .param p2, "reason"    # Ljava/lang/String;
    .param p3, "phoneId"    # I

    .prologue
    .line 768
    if-nez p0, :cond_0

    .line 769
    const-string/jumbo v1, ""

    return-object v1

    .line 772
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 787
    :cond_1
    invoke-static {p1}, Landroid/telephony/DisconnectCause;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 788
    .local v0, "causeAsString":Ljava/lang/String;
    if-nez p2, :cond_2

    .line 789
    return-object v0

    .line 778
    .end local v0    # "causeAsString":Ljava/lang/String;
    :sswitch_0
    invoke-static {p0, p3}, Lcom/android/phone/ImsUtil;->shouldPromoteWfc(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 779
    const-string/jumbo v1, "REASON_WIFI_ON_BUT_WFC_OFF"

    return-object v1

    .line 783
    :sswitch_1
    const-string/jumbo v1, "REASON_IMS_ACCESS_BLOCKED"

    return-object v1

    .line 791
    .restart local v0    # "causeAsString":Ljava/lang/String;
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 772
    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x12 -> :sswitch_0
        0x6d -> :sswitch_1
    .end sparse-switch
.end method
