.class final synthetic Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/function/Predicate;


# instance fields
.field private final synthetic $id:B

.field private final synthetic -$f0:Ljava/lang/Object;


# direct methods
.method private final synthetic $m$0(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/Object;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->-$f0:Ljava/lang/Object;

    check-cast v0, Landroid/telecom/Conferenceable;

    check-cast p1, Landroid/telecom/Conferenceable;

    .end local p1    # "arg0":Ljava/lang/Object;
    invoke-static {v0, p1}, Lcom/android/services/telephony/ImsConferenceController;->lambda$-com_android_services_telephony_ImsConferenceController_10011(Landroid/telecom/Conferenceable;Landroid/telecom/Conferenceable;)Z

    move-result v0

    return v0
.end method

.method private final synthetic $m$1(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/Object;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->-$f0:Ljava/lang/Object;

    check-cast v0, Landroid/telecom/PhoneAccountHandle;

    check-cast p1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .end local p1    # "arg0":Ljava/lang/Object;
    invoke-static {v0, p1}, Lcom/android/services/telephony/TelecomAccountRegistry;->lambda$-com_android_services_telephony_TelecomAccountRegistry_27893(Landroid/telecom/PhoneAccountHandle;Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;)Z

    move-result v0

    return v0
.end method

.method private final synthetic $m$2(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/Object;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->-$f0:Ljava/lang/Object;

    check-cast v0, Landroid/telecom/Connection;

    check-cast p1, Landroid/telecom/Connection;

    .end local p1    # "arg0":Ljava/lang/Object;
    invoke-static {v0, p1}, Lcom/android/services/telephony/TelephonyConferenceController;->lambda$-com_android_services_telephony_TelephonyConferenceController_6074(Landroid/telecom/Connection;Landroid/telecom/Connection;)Z

    move-result v0

    return v0
.end method

.method private final synthetic $m$3(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/Object;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->-$f0:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "arg0":Ljava/lang/Object;
    invoke-static {v0, p1}, Lcom/android/services/telephony/TelephonyConnection;->lambda$-com_android_services_telephony_TelephonyConnection_54807(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public synthetic constructor <init>(BLjava/lang/Object;)V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte p1, p0, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->$id:B

    iput-object p2, p0, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->-$f0:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final test(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    iget-byte v0, p0, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->$id:B

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->$m$0(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->$m$1(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->$m$2(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;->$m$3(Ljava/lang/Object;)Z

    move-result v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
