.class Lcom/android/services/telephony/TelephonyConnectionService$1;
.super Ljava/lang/Object;
.source "TelephonyConnectionService.java"

# interfaces
.implements Lcom/android/services/telephony/TelephonyConnectionServiceProxy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelephonyConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/TelephonyConnectionService;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/TelephonyConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnectionService$1;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public addConference(Lcom/android/services/telephony/ImsConference;)V
    .locals 1
    .param p1, "mImsConference"    # Lcom/android/services/telephony/ImsConference;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$1;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnectionService;->addConference(Landroid/telecom/Conference;)V

    .line 98
    return-void
.end method

.method public addConference(Lcom/android/services/telephony/TelephonyConference;)V
    .locals 1
    .param p1, "mTelephonyConference"    # Lcom/android/services/telephony/TelephonyConference;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$1;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnectionService;->addConference(Landroid/telecom/Conference;)V

    .line 94
    return-void
.end method

.method public addConnectionToConferenceController(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 1
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$1;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnectionService;->addConnectionToConferenceController(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 118
    return-void
.end method

.method public addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;)V
    .locals 1
    .param p1, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;
    .param p2, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$1;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-virtual {v0, p1, p2}, Lcom/android/services/telephony/TelephonyConnectionService;->addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;)V

    .line 108
    return-void
.end method

.method public addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;Landroid/telecom/Conference;)V
    .locals 1
    .param p1, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;
    .param p2, "connection"    # Landroid/telecom/Connection;
    .param p3, "conference"    # Landroid/telecom/Conference;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$1;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/services/telephony/TelephonyConnectionService;->addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;Landroid/telecom/Conference;)V

    .line 114
    return-void
.end method

.method public getAllConnections()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/telecom/Connection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$1;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public removeConnection(Landroid/telecom/Connection;)V
    .locals 1
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$1;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnectionService;->removeConnection(Landroid/telecom/Connection;)V

    .line 102
    return-void
.end method
