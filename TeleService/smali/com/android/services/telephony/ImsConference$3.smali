.class Lcom/android/services/telephony/ImsConference$3;
.super Landroid/telecom/Connection$Listener;
.source "ImsConference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/ImsConference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/ImsConference;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/ImsConference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/ImsConference;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    .line 116
    invoke-direct {p0}, Landroid/telecom/Connection$Listener;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onConferenceParticipantsChanged(Landroid/telecom/Connection;Ljava/util/List;)V
    .locals 5
    .param p1, "c"    # Landroid/telecom/Connection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/telecom/Connection;",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/ConferenceParticipant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p2, "participants":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/ConferenceParticipant;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 152
    :cond_0
    return-void

    .line 154
    :cond_1
    const-string/jumbo v1, "onConferenceParticipantsChanged: %d participants"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p0, v1, v2}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, p1

    .line 155
    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    .line 156
    .local v0, "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-static {v1, v0, p2}, Lcom/android/services/telephony/ImsConference;->-wrap2(Lcom/android/services/telephony/ImsConference;Lcom/android/services/telephony/TelephonyConnection;Ljava/util/List;)V

    .line 157
    return-void
.end method

.method public onConnectionCapabilitiesChanged(Landroid/telecom/Connection;I)V
    .locals 6
    .param p1, "c"    # Landroid/telecom/Connection;
    .param p2, "connectionCapabilities"    # I

    .prologue
    .line 175
    const-string/jumbo v2, "onConnectionCapabilitiesChanged: Connection: %s, connectionCapabilities: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 176
    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 175
    invoke-static {p0, v2, v3}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-virtual {v2}, Lcom/android/services/telephony/ImsConference;->getConnectionCapabilities()I

    move-result v0

    .line 178
    .local v0, "capabilites":I
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-static {v2}, Lcom/android/services/telephony/ImsConference;->-get0(Lcom/android/services/telephony/ImsConference;)Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    .line 180
    :goto_0
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    iget-object v3, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-static {v3, v0, p2, v1}, Lcom/android/services/telephony/ImsConference;->-wrap0(Lcom/android/services/telephony/ImsConference;IIZ)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/services/telephony/ImsConference;->setConnectionCapabilities(I)V

    .line 182
    return-void

    .line 179
    :cond_0
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-static {v2}, Lcom/android/services/telephony/ImsConference;->-get0(Lcom/android/services/telephony/ImsConference;)Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/services/telephony/TelephonyConnection;->isCarrierVideoConferencingSupported()Z

    move-result v1

    .local v1, "isVideoConferencingSupported":Z
    goto :goto_0
.end method

.method public onConnectionPropertiesChanged(Landroid/telecom/Connection;I)V
    .locals 5
    .param p1, "c"    # Landroid/telecom/Connection;
    .param p2, "connectionProperties"    # I

    .prologue
    .line 186
    const-string/jumbo v1, "onConnectionPropertiesChanged: Connection: %s, connectionProperties: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 187
    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 186
    invoke-static {p0, v1, v2}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-virtual {v1}, Lcom/android/services/telephony/ImsConference;->getConnectionProperties()I

    move-result v0

    .line 189
    .local v0, "properties":I
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    iget-object v2, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-static {v2, v0, p2}, Lcom/android/services/telephony/ImsConference;->-wrap1(Lcom/android/services/telephony/ImsConference;II)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/services/telephony/ImsConference;->setConnectionProperties(I)V

    .line 190
    return-void
.end method

.method public onDisconnected(Landroid/telecom/Connection;Landroid/telecom/DisconnectCause;)V
    .locals 1
    .param p1, "c"    # Landroid/telecom/Connection;
    .param p2, "disconnectCause"    # Landroid/telecom/DisconnectCause;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-virtual {v0, p2}, Lcom/android/services/telephony/ImsConference;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 138
    return-void
.end method

.method public onExtrasChanged(Landroid/telecom/Connection;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "c"    # Landroid/telecom/Connection;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onExtrasChanged: c="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Extras="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 204
    .local v0, "newExtras":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-virtual {v1}, Lcom/android/services/telephony/ImsConference;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 205
    const-string/jumbo v2, "telephony."

    .line 204
    invoke-static {p2, v1, v2}, Lcom/android/phone/common/PhoneConstants;->copyMiuiCallExtras(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    .line 206
    invoke-virtual {v0, p2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 207
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-virtual {v1, v0}, Lcom/android/services/telephony/ImsConference;->putExtras(Landroid/os/Bundle;)V

    .line 209
    return-void
.end method

.method public onExtrasRemoved(Landroid/telecom/Connection;Ljava/util/List;)V
    .locals 2
    .param p1, "c"    # Landroid/telecom/Connection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/telecom/Connection;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p2, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onExtrasRemoved: c="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 214
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-virtual {v0, p2}, Lcom/android/services/telephony/ImsConference;->removeExtras(Ljava/util/List;)V

    .line 215
    return-void
.end method

.method public onStateChanged(Landroid/telecom/Connection;I)V
    .locals 1
    .param p1, "c"    # Landroid/telecom/Connection;
    .param p2, "state"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-virtual {v0, p2}, Lcom/android/services/telephony/ImsConference;->setState(I)V

    .line 127
    return-void
.end method

.method public onStatusHintsChanged(Landroid/telecom/Connection;Landroid/telecom/StatusHints;)V
    .locals 2
    .param p1, "c"    # Landroid/telecom/Connection;
    .param p2, "statusHints"    # Landroid/telecom/StatusHints;

    .prologue
    .line 194
    const-string/jumbo v0, "onStatusHintsChanged"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-static {v0}, Lcom/android/services/telephony/ImsConference;->-wrap8(Lcom/android/services/telephony/ImsConference;)V

    .line 196
    return-void
.end method

.method public onVideoProviderChanged(Landroid/telecom/Connection;Landroid/telecom/Connection$VideoProvider;)V
    .locals 3
    .param p1, "c"    # Landroid/telecom/Connection;
    .param p2, "videoProvider"    # Landroid/telecom/Connection$VideoProvider;

    .prologue
    .line 168
    const-string/jumbo v0, "onVideoProviderChanged: Connection: %s, VideoProvider: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 169
    const/4 v2, 0x1

    aput-object p2, v1, v2

    .line 168
    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-virtual {v0, p1, p2}, Lcom/android/services/telephony/ImsConference;->setVideoProvider(Landroid/telecom/Connection;Landroid/telecom/Connection$VideoProvider;)V

    .line 171
    return-void
.end method

.method public onVideoStateChanged(Landroid/telecom/Connection;I)V
    .locals 4
    .param p1, "c"    # Landroid/telecom/Connection;
    .param p2, "videoState"    # I

    .prologue
    .line 161
    const-string/jumbo v0, "onVideoStateChanged video state %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference$3;->this$0:Lcom/android/services/telephony/ImsConference;

    invoke-virtual {v0, p1, p2}, Lcom/android/services/telephony/ImsConference;->setVideoState(Landroid/telecom/Connection;I)V

    .line 163
    return-void
.end method
