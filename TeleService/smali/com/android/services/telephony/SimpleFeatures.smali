.class public Lcom/android/services/telephony/SimpleFeatures;
.super Lcom/android/phone/BaseSimpleFeatures;
.source "SimpleFeatures.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/android/phone/BaseSimpleFeatures;-><init>()V

    return-void
.end method

.method public static ShowAdminSupportDetailsIntent(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    new-instance v0, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    invoke-direct {v0}, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;-><init>()V

    .line 135
    invoke-static {p0, v0}, Lcom/android/settingslib/RestrictedLockUtils;->sendShowAdminSupportDetailsIntent(Landroid/content/Context;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    .line 137
    return-void
.end method

.method public static accessMiuiConferenceExtra(Landroid/telecom/Conference;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;
    .locals 2
    .param p0, "conference"    # Landroid/telecom/Conference;

    .prologue
    .line 59
    new-instance v0, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    new-instance v1, Lcom/android/services/telephony/SimpleFeatures$2;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/SimpleFeatures$2;-><init>(Landroid/telecom/Conference;)V

    invoke-direct {v0, v1}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;-><init>(Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor$ExtraContainer;)V

    return-object v0
.end method

.method public static accessMiuiConnectionExtra(Landroid/telecom/Connection;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;
    .locals 2
    .param p0, "conn"    # Landroid/telecom/Connection;

    .prologue
    .line 44
    new-instance v0, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    new-instance v1, Lcom/android/services/telephony/SimpleFeatures$1;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/SimpleFeatures$1;-><init>(Landroid/telecom/Connection;)V

    invoke-direct {v0, v1}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;-><init>(Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor$ExtraContainer;)V

    return-object v0
.end method

.method public static checkDataRoamingRestriction(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 128
    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    .line 129
    .local v0, "um":Landroid/os/UserManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->getUserHandle()I

    move-result v2

    invoke-static {v2}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v1

    .line 130
    .local v1, "user":Landroid/os/UserHandle;
    const-string/jumbo v2, "no_data_roaming"

    invoke-virtual {v0, v2, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    const-string/jumbo v2, "no_data_roaming"

    invoke-virtual {v0, v2, v1}, Landroid/os/UserManager;->hasBaseUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 130
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static hasBaseUserRestriction(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    const-string/jumbo v0, "no_data_roaming"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 123
    invoke-static {p0, v0, v1}, Lcom/android/settingslib/RestrictedLockUtils;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static isHidePhoneAccountSettings(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    .line 163
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x1a

    if-lt v5, v6, :cond_1

    .line 164
    invoke-static {p0}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v4

    .line 166
    .local v4, "telecomManager":Landroid/telecom/TelecomManager;
    invoke-virtual {v4, v7}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts(Z)Ljava/util/List;

    move-result-object v1

    .line 167
    .local v1, "accountHandles":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "handle$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/PhoneAccountHandle;

    .line 168
    .local v2, "handle":Landroid/telecom/PhoneAccountHandle;
    invoke-virtual {v4, v2}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 169
    .local v0, "account":Landroid/telecom/PhoneAccount;
    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Landroid/telecom/PhoneAccount;->hasCapabilities(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 170
    const/4 v5, 0x0

    return v5

    .line 174
    .end local v0    # "account":Landroid/telecom/PhoneAccount;
    .end local v1    # "accountHandles":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    .end local v2    # "handle":Landroid/telecom/PhoneAccountHandle;
    .end local v3    # "handle$iterator":Ljava/util/Iterator;
    .end local v4    # "telecomManager":Landroid/telecom/TelecomManager;
    :cond_1
    return v7
.end method

.method public static isHideVoicemailSettings(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 141
    const-class v3, Landroid/telecom/TelecomManager;

    .line 140
    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telecom/TelecomManager;

    invoke-virtual {v3}, Landroid/telecom/TelecomManager;->getDefaultDialerPackage()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "defaultDialer":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 143
    return v6

    .line 146
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 147
    const/16 v4, 0x80

    .line 146
    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v2, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 148
    .local v2, "metadata":Landroid/os/Bundle;
    if-eqz v2, :cond_1

    .line 149
    const-string/jumbo v3, "android.telephony.HIDE_VOICEMAIL_SETTINGS_MENU"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    .line 148
    if-eqz v3, :cond_2

    .line 150
    :cond_1
    const-string/jumbo v3, "SimpleFeatures"

    const-string/jumbo v4, "isHideVoicemailSettings(): not disabled by default dialer"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    return v6

    .line 153
    :cond_2
    const-string/jumbo v3, "SimpleFeatures"

    const-string/jumbo v4, "isHideVoicemailSettings(): disabled by default dialer"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    const/4 v3, 0x1

    return v3

    .line 155
    .end local v2    # "metadata":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 157
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string/jumbo v3, "SimpleFeatures"

    const-string/jumbo v4, "isHideVoicemailSettings(): not controlled by default dialer"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    return v6
.end method

.method public static putImsConferenceExtra(Lcom/android/services/telephony/ImsConference;Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 5
    .param p0, "conference"    # Lcom/android/services/telephony/ImsConference;
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 75
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 76
    :cond_0
    return-void

    .line 80
    :cond_1
    const-string/jumbo v1, "merged"

    .line 81
    .local v1, "imsConferenceType":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 82
    const-string/jumbo v1, "dialing"

    .line 86
    :cond_2
    :goto_0
    invoke-static {p0}, Lcom/android/services/telephony/SimpleFeatures;->accessMiuiConferenceExtra(Landroid/telecom/Conference;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v2

    const-string/jumbo v3, "telephony.IMS_CONFERENCE_TYPE"

    invoke-virtual {v2, v3, v1}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->commit()V

    .line 89
    const/4 v0, 0x0

    .line 90
    .local v0, "conferenceAddress":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string/jumbo v2, "merged"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 91
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 92
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 97
    .end local v0    # "conferenceAddress":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-static {p0}, Lcom/android/services/telephony/SimpleFeatures;->accessMiuiConferenceExtra(Landroid/telecom/Conference;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v2

    const-string/jumbo v3, "telephony.IMS_CONFERENCE_ADDRESS"

    invoke-virtual {v2, v3, v0}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->commit()V

    .line 98
    const-string/jumbo v2, "SimpleFeatures"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "putImsConferenceExtra "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    return-void

    .line 83
    :cond_4
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 84
    const-string/jumbo v1, "incoming"

    goto :goto_0

    .line 94
    .restart local v0    # "conferenceAddress":Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->getOrigDialString()Ljava/lang/String;

    move-result-object v0

    .local v0, "conferenceAddress":Ljava/lang/String;
    goto :goto_1
.end method

.method public static updateAddresIfCallForwdNumExist(Lcom/android/services/telephony/TelephonyConnection;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 8
    .param p0, "conn"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p1, "address"    # Landroid/net/Uri;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 102
    sget-boolean v4, Lmiui/telephony/MiuiTelephony;->IS_QCOM:Z

    if-nez v4, :cond_0

    .line 103
    return-object p1

    .line 105
    :cond_0
    move-object v2, p1

    .line 106
    .local v2, "newAddress":Landroid/net/Uri;
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 107
    :goto_0
    const/4 v0, -0x1

    .line 108
    .local v0, "callForwardFlagIndex":I
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    .line 109
    const/16 v4, 0x26

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 111
    :cond_1
    if-lez v0, :cond_2

    .line 112
    const-string/jumbo v4, "tel"

    invoke-virtual {v3, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v6}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 113
    invoke-static {p0}, Lcom/android/services/telephony/SimpleFeatures;->accessMiuiConnectionExtra(Landroid/telecom/Connection;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v4

    const-string/jumbo v5, "telephony.IS_FORWARDED_CALL"

    invoke-virtual {v4, v5, v7}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 114
    .local v1, "isForwardCall":Z
    if-nez v1, :cond_2

    .line 115
    invoke-static {p0}, Lcom/android/services/telephony/SimpleFeatures;->accessMiuiConnectionExtra(Landroid/telecom/Connection;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v4

    const-string/jumbo v5, "telephony.IS_FORWARDED_CALL"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->putBoolean(Ljava/lang/String;Z)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->commit()V

    .line 116
    const-string/jumbo v4, "SimpleFeatures"

    const-string/jumbo v5, "isCallForwardNumberExist result = ture"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/android/services/telephony/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    .end local v1    # "isForwardCall":Z
    :cond_2
    return-object v2

    .line 106
    .end local v0    # "callForwardFlagIndex":I
    :cond_3
    const/4 v3, 0x0

    .local v3, "num":Ljava/lang/String;
    goto :goto_0
.end method
