.class public interface abstract Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;
.super Ljava/lang/Object;
.source "TelephonyConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelephonyConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PhoneFactoryProxy"
.end annotation


# virtual methods
.method public abstract getDefaultPhone()Lcom/android/internal/telephony/Phone;
.end method

.method public abstract getPhone(I)Lcom/android/internal/telephony/Phone;
.end method

.method public abstract getPhones()[Lcom/android/internal/telephony/Phone;
.end method
