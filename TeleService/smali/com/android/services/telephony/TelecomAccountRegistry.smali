.class final Lcom/android/services/telephony/TelecomAccountRegistry;
.super Ljava/lang/Object;
.source "TelecomAccountRegistry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/TelecomAccountRegistry$1;,
        Lcom/android/services/telephony/TelecomAccountRegistry$2;,
        Lcom/android/services/telephony/TelecomAccountRegistry$3;,
        Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;,
        Lcom/android/services/telephony/TelecomAccountRegistry$Count;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/android/services/telephony/TelecomAccountRegistry;


# instance fields
.field private mAccounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mAccountsLock:Ljava/lang/Object;

.field private final mContext:Landroid/content/Context;

.field private mIsPrimaryUser:Z

.field private mOnSubscriptionsChangedListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mServiceState:I

.field private final mSubscriptionManager:Landroid/telephony/SubscriptionManager;

.field private final mTelecomManager:Landroid/telecom/TelecomManager;

.field private mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionService;

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private final mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static synthetic -get0(Lcom/android/services/telephony/TelecomAccountRegistry;)Ljava/util/List;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/services/telephony/TelecomAccountRegistry;)Ljava/lang/Object;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/content/Context;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/services/telephony/TelecomAccountRegistry;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    iget-boolean v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mIsPrimaryUser:Z

    return v0
.end method

.method static synthetic -get4(Lcom/android/services/telephony/TelecomAccountRegistry;)I
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    iget v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mServiceState:I

    return v0
.end method

.method static synthetic -get5(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/telephony/SubscriptionManager;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/telecom/TelecomManager;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/services/telephony/TelecomAccountRegistry;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/services/telephony/TelecomAccountRegistry;Z)Z
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;
    .param p1, "-value"    # Z

    .prologue
    iput-boolean p1, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mIsPrimaryUser:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/services/telephony/TelecomAccountRegistry;I)I
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;
    .param p1, "-value"    # I

    .prologue
    iput p1, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mServiceState:I

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/services/telephony/TelecomAccountRegistry;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/TelecomAccountRegistry;->setupAccounts()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/TelecomAccountRegistry;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelecomAccountRegistry;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/TelecomAccountRegistry;->tearDownAccounts()V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 516
    new-instance v0, Lcom/android/services/telephony/TelecomAccountRegistry$1;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelecomAccountRegistry$1;-><init>(Lcom/android/services/telephony/TelecomAccountRegistry;)V

    .line 515
    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mOnSubscriptionsChangedListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    .line 525
    new-instance v0, Lcom/android/services/telephony/TelecomAccountRegistry$2;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelecomAccountRegistry$2;-><init>(Lcom/android/services/telephony/TelecomAccountRegistry;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;

    .line 541
    new-instance v0, Lcom/android/services/telephony/TelecomAccountRegistry$3;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelecomAccountRegistry$3;-><init>(Lcom/android/services/telephony/TelecomAccountRegistry;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 558
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    .line 559
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    .line 560
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mServiceState:I

    .line 561
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mIsPrimaryUser:Z

    .line 568
    iput-object p1, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    .line 569
    invoke-static {p1}, Landroid/telecom/TelecomManager;->from(Landroid/content/Context;)Landroid/telecom/TelecomManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    .line 570
    invoke-static {p1}, Landroid/telephony/TelephonyManager;->from(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 571
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    .line 572
    return-void
.end method

.method private cleanupPhoneAccounts()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 753
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    const-class v6, Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 756
    .local v4, "telephonyComponentName":Landroid/content/ComponentName;
    iget-object v5, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 757
    const v6, 0x7f0e0025

    .line 756
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 758
    .local v1, "emergencyCallsOnlyEmergencyAccount":Z
    if-eqz v1, :cond_1

    .line 759
    iget-object v5, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v5}, Landroid/telecom/TelecomManager;->getAllPhoneAccountHandles()Ljava/util/List;

    move-result-object v0

    .line 762
    .local v0, "accountHandles":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    :goto_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "handle$iterator":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telecom/PhoneAccountHandle;

    .line 763
    .local v2, "handle":Landroid/telecom/PhoneAccountHandle;
    invoke-virtual {v2}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 764
    invoke-virtual {p0, v2}, Lcom/android/services/telephony/TelecomAccountRegistry;->hasAccountEntryForPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    .line 763
    if-eqz v5, :cond_0

    .line 765
    const-string/jumbo v5, "Unregistering phone account %s."

    new-array v6, v8, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 766
    iget-object v5, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v5, v2}, Landroid/telecom/TelecomManager;->unregisterPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_1

    .line 760
    .end local v0    # "accountHandles":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    .end local v2    # "handle":Landroid/telecom/PhoneAccountHandle;
    .end local v3    # "handle$iterator":Ljava/util/Iterator;
    :cond_1
    iget-object v5, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v5, v8}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts(Z)Ljava/util/List;

    move-result-object v0

    .restart local v0    # "accountHandles":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    goto :goto_0

    .line 769
    .restart local v3    # "handle$iterator":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method static final declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-class v1, Lcom/android/services/telephony/TelecomAccountRegistry;

    monitor-enter v1

    .line 575
    :try_start_0
    sget-object v0, Lcom/android/services/telephony/TelecomAccountRegistry;->sInstance:Lcom/android/services/telephony/TelecomAccountRegistry;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 576
    new-instance v0, Lcom/android/services/telephony/TelecomAccountRegistry;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelecomAccountRegistry;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/services/telephony/TelecomAccountRegistry;->sInstance:Lcom/android/services/telephony/TelecomAccountRegistry;

    .line 578
    :cond_0
    sget-object v0, Lcom/android/services/telephony/TelecomAccountRegistry;->sInstance:Lcom/android/services/telephony/TelecomAccountRegistry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isNonSimAccountFound()Z
    .locals 5

    .prologue
    .line 904
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v3}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 905
    .local v2, "phoneAccounts":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/telecom/PhoneAccountHandle;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 906
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 907
    .local v1, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v3, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 908
    .local v0, "phoneAccount":Landroid/telecom/PhoneAccount;
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3, v0}, Landroid/telephony/TelephonyManager;->getSubIdForPhoneAccount(Landroid/telecom/PhoneAccount;)I

    move-result v3

    .line 909
    const/4 v4, -0x1

    .line 908
    if-ne v3, v4, :cond_0

    .line 910
    const/4 v3, 0x1

    return v3

    .line 913
    .end local v0    # "phoneAccount":Landroid/telecom/PhoneAccount;
    .end local v1    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :cond_1
    const/4 v3, 0x0

    return v3
.end method

.method private isRadioInValidState([Lcom/android/internal/telephony/Phone;)Z
    .locals 8
    .param p1, "phones"    # [Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 917
    const-string/jumbo v4, "persist.radio.apm_sim_not_pwdn"

    invoke-static {v4, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v7, :cond_0

    const/4 v2, 0x1

    .line 918
    .local v2, "isApmSimNotPwrDown":Z
    :goto_0
    iget-object v4, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 919
    const-string/jumbo v5, "airplane_mode_on"

    .line 918
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 922
    .local v1, "isAPMOn":I
    if-ne v1, v7, :cond_1

    xor-int/lit8 v4, v2, 0x1

    if-eqz v4, :cond_1

    .line 923
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "isRadioInValidState, isApmSimNotPwrDown = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 924
    const-string/jumbo v5, ", isAPMOn:"

    .line 923
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 925
    return v6

    .line 917
    .end local v1    # "isAPMOn":I
    .end local v2    # "isApmSimNotPwrDown":Z
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "isApmSimNotPwrDown":Z
    goto :goto_0

    .line 929
    .restart local v1    # "isAPMOn":I
    :cond_1
    iget-object v4, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v3

    .line 930
    .local v3, "numPhones":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_3

    .line 931
    aget-object v4, p1, v0

    if-eqz v4, :cond_2

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->isShuttingDown()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 932
    const-string/jumbo v4, " isRadioInValidState: device shutdown in progress "

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 933
    return v6

    .line 930
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 936
    :cond_3
    return v7
.end method

.method static synthetic lambda$-com_android_services_telephony_TelecomAccountRegistry_27893(Landroid/telecom/PhoneAccountHandle;Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;)Z
    .locals 1
    .param p0, "handle"    # Landroid/telecom/PhoneAccountHandle;
    .param p1, "entry"    # Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .prologue
    .line 653
    invoke-virtual {p1}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private setupAccounts()V
    .locals 33

    .prologue
    .line 774
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v20

    .line 775
    .local v20, "phones":[Lcom/android/internal/telephony/Phone;
    const-string/jumbo v26, "Found %d phones.  Attempting to register."

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    const/16 v29, 0x0

    aput-object v28, v27, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 777
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_1

    .line 778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    const/16 v28, 0x1

    invoke-static/range {v26 .. v28}, Lmiui/telephony/VirtualSimUtils;->getVirtualSimSlot(Landroid/content/Context;ZI)I

    move-result v14

    .line 781
    .local v14, "onlySupportDataPhoneId":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    .line 782
    const v27, 0x7f0e0024

    .line 781
    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v18

    .line 783
    .local v18, "phoneAccountsEnabled":Z
    const/4 v7, 0x0

    .line 784
    .local v7, "activeCount":I
    const/4 v8, -0x1

    .line 786
    .local v8, "activeSubscriptionId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    move-object/from16 v27, v0

    monitor-enter v27

    .line 787
    if-eqz v18, :cond_5

    .line 790
    const/4 v6, 0x1

    .line 791
    .local v6, "PROVISIONED":I
    const/4 v5, -0x1

    .line 792
    .local v5, "INVALID_STATE":I
    const/4 v12, 0x0

    .line 794
    .local v12, "isInEcm":Z
    const/16 v26, 0x0

    :try_start_0
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v28, v0

    .end local v12    # "isInEcm":Z
    :goto_1
    move/from16 v0, v26

    move/from16 v1, v28

    if-ge v0, v1, :cond_5

    aget-object v15, v20, v26

    .line 796
    .local v15, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v15}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v29

    move/from16 v0, v29

    if-ne v14, v0, :cond_2

    .line 794
    :cond_0
    :goto_2
    add-int/lit8 v26, v26, 0x1

    goto :goto_1

    .line 779
    .end local v5    # "INVALID_STATE":I
    .end local v6    # "PROVISIONED":I
    .end local v7    # "activeCount":I
    .end local v8    # "activeSubscriptionId":I
    .end local v14    # "onlySupportDataPhoneId":I
    .end local v15    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v18    # "phoneAccountsEnabled":Z
    :cond_1
    const/4 v14, -0x1

    .restart local v14    # "onlySupportDataPhoneId":I
    goto :goto_0

    .line 800
    .restart local v5    # "INVALID_STATE":I
    .restart local v6    # "PROVISIONED":I
    .restart local v7    # "activeCount":I
    .restart local v8    # "activeSubscriptionId":I
    .restart local v15    # "phone":Lcom/android/internal/telephony/Phone;
    .restart local v18    # "phoneAccountsEnabled":Z
    :cond_2
    const/16 v21, 0x1

    .line 801
    .local v21, "provisionStatus":I
    invoke-virtual {v15}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v23

    .line 802
    .local v23, "subscriptionId":I
    invoke-virtual {v15}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v22

    .line 803
    .local v22, "slotId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v29, v0

    .line 804
    const-string/jumbo v29, "ril.cdma.inecmmode"

    const-string/jumbo v30, "false"

    .line 803
    move/from16 v0, v22

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 806
    .local v12, "isInEcm":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v29

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-le v0, v1, :cond_3

    .line 808
    const-string/jumbo v29, "extphone"

    invoke-static/range {v29 .. v29}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v29

    .line 807
    invoke-static/range {v29 .. v29}, Lorg/codeaurora/internal/IExtTelephony$Stub;->asInterface(Landroid/os/IBinder;)Lorg/codeaurora/internal/IExtTelephony;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 812
    .local v13, "mExtTelephony":Lorg/codeaurora/internal/IExtTelephony;
    :try_start_1
    move/from16 v0, v22

    invoke-interface {v13, v0}, Lorg/codeaurora/internal/IExtTelephony;->getCurrentUiccCardProvisioningStatus(I)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v21

    .line 824
    .end local v13    # "mExtTelephony":Lorg/codeaurora/internal/IExtTelephony;
    :cond_3
    :goto_3
    :try_start_2
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v30, "Phone with subscription id: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    .line 825
    const-string/jumbo v30, " slotId: "

    .line 824
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    .line 825
    const-string/jumbo v30, " provisionStatus: "

    .line 824
    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x0

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 828
    if-ltz v23, :cond_4

    const/16 v29, 0x1

    move/from16 v0, v21

    move/from16 v1, v29

    if-ne v0, v1, :cond_4

    .line 829
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->isActiveSubId(I)Z

    move-result v29

    .line 828
    if-eqz v29, :cond_4

    .line 830
    add-int/lit8 v7, v7, 0x1

    .line 831
    move/from16 v8, v23

    .line 832
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    move-object/from16 v29, v0

    new-instance v30, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    const/16 v31, 0x0

    .line 833
    const/16 v32, 0x0

    .line 832
    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move/from16 v2, v31

    move/from16 v3, v32

    invoke-direct {v0, v1, v15, v2, v3}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;-><init>(Lcom/android/services/telephony/TelecomAccountRegistry;Lcom/android/internal/telephony/Phone;ZZ)V

    invoke-interface/range {v29 .. v30}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 786
    .end local v5    # "INVALID_STATE":I
    .end local v6    # "PROVISIONED":I
    .end local v12    # "isInEcm":Z
    .end local v15    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v21    # "provisionStatus":I
    .end local v22    # "slotId":I
    .end local v23    # "subscriptionId":I
    :catchall_0
    move-exception v26

    monitor-exit v27

    throw v26

    .line 817
    .restart local v5    # "INVALID_STATE":I
    .restart local v6    # "PROVISIONED":I
    .restart local v12    # "isInEcm":Z
    .restart local v13    # "mExtTelephony":Lorg/codeaurora/internal/IExtTelephony;
    .restart local v15    # "phone":Lcom/android/internal/telephony/Phone;
    .restart local v21    # "provisionStatus":I
    .restart local v22    # "slotId":I
    .restart local v23    # "subscriptionId":I
    :catch_0
    move-exception v11

    .line 818
    .local v11, "ex":Ljava/lang/NullPointerException;
    const/16 v21, -0x1

    .line 819
    :try_start_3
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v30, "Failed to get status , slotId: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string/jumbo v30, " Exception: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x0

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 813
    .end local v11    # "ex":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v10

    .line 814
    .local v10, "ex":Landroid/os/RemoteException;
    const/16 v21, -0x1

    .line 815
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v30, "Failed to get status , slotId: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string/jumbo v30, " Exception: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x0

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 838
    .end local v10    # "ex":Landroid/os/RemoteException;
    .end local v13    # "mExtTelephony":Lorg/codeaurora/internal/IExtTelephony;
    :cond_4
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/android/phone/PhoneGlobals;->getPhoneInEcm()Lcom/android/internal/telephony/Phone;

    move-result-object v19

    .line 839
    .local v19, "phoneInEcm":Lcom/android/internal/telephony/Phone;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v29

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-le v0, v1, :cond_0

    if-eqz v12, :cond_0

    .line 840
    if-eqz v19, :cond_0

    .line 841
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v29

    invoke-virtual {v15}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v30

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_0

    .line 842
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    move-object/from16 v29, v0

    new-instance v30, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    const/16 v31, 0x1

    .line 843
    const/16 v32, 0x0

    .line 842
    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;-><init>(Lcom/android/services/telephony/TelecomAccountRegistry;Lcom/android/internal/telephony/Phone;ZZ)V

    invoke-interface/range {v29 .. v30}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 852
    .end local v5    # "INVALID_STATE":I
    .end local v6    # "PROVISIONED":I
    .end local v12    # "isInEcm":Z
    .end local v15    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v19    # "phoneInEcm":Lcom/android/internal/telephony/Phone;
    .end local v21    # "provisionStatus":I
    .end local v22    # "slotId":I
    .end local v23    # "subscriptionId":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->isEmpty()Z

    move-result v26

    if-eqz v26, :cond_6

    .line 853
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    move-object/from16 v26, v0

    new-instance v28, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .line 854
    invoke-static {}, Lcom/android/phone/PhoneUtils;->getPrimaryStackPhoneId()I

    move-result v29

    .line 853
    invoke-static/range {v29 .. v29}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v29

    .line 854
    const/16 v30, 0x1

    .line 855
    const/16 v31, 0x0

    .line 853
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v29

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;-><init>(Lcom/android/services/telephony/TelecomAccountRegistry;Lcom/android/internal/telephony/Phone;ZZ)V

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_6
    monitor-exit v27

    .line 866
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry;->cleanupPhoneAccounts()V

    .line 872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/telecom/TelecomManager;->getUserSelectedOutgoingPhoneAccount()Landroid/telecom/PhoneAccountHandle;

    move-result-object v9

    .line 874
    .local v9, "defaultPhoneAccount":Landroid/telecom/PhoneAccountHandle;
    new-instance v24, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    const-class v27, Lcom/android/services/telephony/TelephonyConnectionService;

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 876
    .local v24, "telephonyComponentName":Landroid/content/ComponentName;
    if-eqz v9, :cond_8

    .line 877
    invoke-virtual {v9}, Landroid/telecom/PhoneAccountHandle;->getComponentName()Landroid/content/ComponentName;

    move-result-object v26

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v26

    .line 876
    if-eqz v26, :cond_8

    .line 878
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/services/telephony/TelecomAccountRegistry;->hasAccountEntryForPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v26

    xor-int/lit8 v26, v26, 0x1

    .line 876
    if-eqz v26, :cond_8

    .line 880
    invoke-virtual {v9}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v17

    .line 881
    .local v17, "phoneAccountId":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_7

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 884
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    invoke-static/range {v26 .. v26}, Lcom/android/phone/PhoneGlobals;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v26

    .line 883
    invoke-static/range {v26 .. v26}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v25

    .line 886
    .local v25, "upgradedPhoneAccount":Landroid/telecom/PhoneAccountHandle;
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/TelecomAccountRegistry;->hasAccountEntryForPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 887
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->setUserSelectedOutgoingPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    .line 900
    .end local v17    # "phoneAccountId":Ljava/lang/String;
    .end local v25    # "upgradedPhoneAccount":Landroid/telecom/PhoneAccountHandle;
    :cond_7
    :goto_4
    return-void

    .line 890
    :cond_8
    if-nez v9, :cond_7

    .line 891
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v26

    sget-object v27, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->ONE:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    invoke-virtual/range {v27 .. v27}, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->ordinal()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_7

    .line 892
    sget-object v26, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->ONE:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    invoke-virtual/range {v26 .. v26}, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->ordinal()I

    move-result v26

    move/from16 v0, v26

    if-ne v7, v0, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelecomAccountRegistry;->isNonSimAccountFound()Z

    move-result v26

    xor-int/lit8 v26, v26, 0x1

    .line 890
    if-eqz v26, :cond_7

    .line 893
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/services/telephony/TelecomAccountRegistry;->isRadioInValidState([Lcom/android/internal/telephony/Phone;)Z

    move-result v26

    .line 890
    if-eqz v26, :cond_7

    .line 895
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/services/telephony/TelecomAccountRegistry;->subscriptionIdToPhoneAccountHandle(I)Landroid/telecom/PhoneAccountHandle;

    move-result-object v16

    .line 896
    .local v16, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    if-eqz v16, :cond_7

    .line 897
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/telecom/TelecomManager;->setUserSelectedOutgoingPhoneAccount(Landroid/telecom/PhoneAccountHandle;)V

    goto :goto_4
.end method

.method private subscriptionIdToPhoneAccountHandle(I)Landroid/telecom/PhoneAccountHandle;
    .locals 5
    .param p1, "subId"    # I

    .prologue
    .line 941
    iget-object v4, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v4}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 943
    .local v2, "phoneAccounts":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/telecom/PhoneAccountHandle;>;"
    iget-object v4, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v4}, Landroid/telecom/TelecomManager;->getCallCapablePhoneAccounts()Ljava/util/List;

    move-result-object v3

    .line 944
    .local v3, "phoneAccountsList":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/PhoneAccountHandle;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 945
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/PhoneAccountHandle;

    .line 946
    .local v1, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    iget-object v4, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelecomManager:Landroid/telecom/TelecomManager;

    invoke-virtual {v4, v1}, Landroid/telecom/TelecomManager;->getPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Landroid/telecom/PhoneAccount;

    move-result-object v0

    .line 947
    .local v0, "phoneAccount":Landroid/telecom/PhoneAccount;
    iget-object v4, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4, v0}, Landroid/telephony/TelephonyManager;->getSubIdForPhoneAccount(Landroid/telecom/PhoneAccount;)I

    move-result v4

    if-ne p1, v4, :cond_0

    .line 948
    return-object v1

    .line 951
    .end local v0    # "phoneAccount":Landroid/telecom/PhoneAccount;
    .end local v1    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :cond_1
    const/4 v4, 0x0

    return-object v4
.end method

.method private tearDownAccounts()V
    .locals 4

    .prologue
    .line 955
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 956
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "entry$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .line 957
    .local v0, "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->teardown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 955
    .end local v0    # "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    .end local v1    # "entry$iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 959
    .restart local v1    # "entry$iterator":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v3

    .line 961
    return-void
.end method


# virtual methods
.method getAddress(Landroid/telecom/PhoneAccountHandle;)Landroid/net/Uri;
    .locals 4
    .param p1, "handle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 695
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 696
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "entry$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .line 697
    .local v0, "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 698
    invoke-static {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->-get0(Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;)Landroid/telecom/PhoneAccount;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telecom/PhoneAccount;->getAddress()Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    monitor-exit v3

    return-object v2

    .end local v0    # "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    :cond_1
    monitor-exit v3

    .line 702
    const/4 v2, 0x0

    return-object v2

    .line 695
    .end local v1    # "entry$iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method getSubscriptionManager()Landroid/telephony/SubscriptionManager;
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mSubscriptionManager:Landroid/telephony/SubscriptionManager;

    return-object v0
.end method

.method getTelephonyConnectionService()Lcom/android/services/telephony/TelephonyConnectionService;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionService;

    return-object v0
.end method

.method hasAccountEntryForPhoneAccount(Landroid/telecom/PhoneAccountHandle;)Z
    .locals 4
    .param p1, "handle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 737
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 738
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "entry$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .line 739
    .local v0, "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 740
    const/4 v2, 0x1

    monitor-exit v3

    return v2

    .end local v0    # "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    :cond_1
    monitor-exit v3

    .line 744
    const/4 v2, 0x0

    return v2

    .line 737
    .end local v1    # "entry$iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method isMergeCallSupported(Landroid/telecom/PhoneAccountHandle;)Z
    .locals 4
    .param p1, "handle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 615
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 616
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "entry$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .line 617
    .local v0, "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 618
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isMergeCallSupported()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    monitor-exit v3

    return v2

    .end local v0    # "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    :cond_1
    monitor-exit v3

    .line 622
    const/4 v2, 0x0

    return v2

    .line 615
    .end local v1    # "entry$iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method isMergeImsCallSupported(Landroid/telecom/PhoneAccountHandle;)Z
    .locals 4
    .param p1, "handle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 671
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 672
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "entry$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .line 673
    .local v0, "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 674
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isMergeImsCallSupported()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    monitor-exit v3

    return v2

    .end local v0    # "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    :cond_1
    monitor-exit v3

    .line 678
    const/4 v2, 0x0

    return v2

    .line 671
    .end local v1    # "entry$iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method isMergeOfWifiCallsAllowedWhenVoWifiOff(Landroid/telecom/PhoneAccountHandle;)Z
    .locals 5
    .param p1, "handle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 651
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 652
    :try_start_0
    iget-object v1, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v1

    .line 653
    new-instance v3, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;

    const/4 v4, 0x1

    invoke-direct {v3, v4, p1}, Lcom/android/services/telephony/-$Lambda$ZgzB1HZ6OPvL3dFy4NqHgn3You4;-><init>(BLjava/lang/Object;)V

    .line 652
    invoke-interface {v1, v3}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/stream/Stream;->findFirst()Ljava/util/Optional;

    move-result-object v0

    .line 655
    .local v0, "result":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;>;"
    invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 656
    invoke-virtual {v0}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isMergeOfWifiCallsAllowedWhenVoWifiOff()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit v2

    return v1

    .line 658
    :cond_0
    const/4 v1, 0x0

    monitor-exit v2

    return v1

    .line 651
    .end local v0    # "result":Ljava/util/Optional;, "Ljava/util/Optional<Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method isVideoConferencingSupported(Landroid/telecom/PhoneAccountHandle;)Z
    .locals 4
    .param p1, "handle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 633
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 634
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "entry$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .line 635
    .local v0, "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 636
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isVideoConferencingSupported()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    monitor-exit v3

    return v2

    .end local v0    # "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    :cond_1
    monitor-exit v3

    .line 640
    const/4 v2, 0x0

    return v2

    .line 633
    .end local v1    # "entry$iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method isVideoPauseSupported(Landroid/telecom/PhoneAccountHandle;)Z
    .locals 4
    .param p1, "handle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 597
    iget-object v3, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccountsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 598
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mAccounts:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "entry$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;

    .line 599
    .local v0, "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->getPhoneAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/telecom/PhoneAccountHandle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 600
    invoke-virtual {v0}, Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;->isVideoPauseSupported()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    monitor-exit v3

    return v2

    .end local v0    # "entry":Lcom/android/services/telephony/TelecomAccountRegistry$AccountEntry;
    :cond_1
    monitor-exit v3

    .line 604
    const/4 v2, 0x0

    return v2

    .line 597
    .end local v1    # "entry$iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method setTelephonyConnectionService(Lcom/android/services/telephony/TelephonyConnectionService;)V
    .locals 0
    .param p1, "telephonyConnectionService"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    .line 582
    iput-object p1, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionService;

    .line 583
    return-void
.end method

.method setupOnBoot()V
    .locals 4

    .prologue
    .line 716
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    .line 717
    iget-object v1, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mOnSubscriptionsChangedListener:Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;

    .line 716
    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->addOnSubscriptionsChangedListener(Landroid/telephony/SubscriptionManager$OnSubscriptionsChangedListener;)V

    .line 721
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 725
    iget-object v0, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/services/telephony/TelecomAccountRegistry;->mUserSwitchedReceiver:Landroid/content/BroadcastReceiver;

    .line 726
    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.intent.action.USER_SWITCHED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 725
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 727
    return-void
.end method
