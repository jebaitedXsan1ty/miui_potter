.class final enum Lcom/android/services/telephony/TelecomAccountRegistry$Count;
.super Ljava/lang/Enum;
.source "TelecomAccountRegistry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelecomAccountRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Count"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/services/telephony/TelecomAccountRegistry$Count;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/services/telephony/TelecomAccountRegistry$Count;

.field public static final enum ONE:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

.field public static final enum TWO:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

.field public static final enum ZERO:Lcom/android/services/telephony/TelecomAccountRegistry$Count;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 85
    new-instance v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    const-string/jumbo v1, "ZERO"

    invoke-direct {v0, v1, v2}, Lcom/android/services/telephony/TelecomAccountRegistry$Count;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->ZERO:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    .line 86
    new-instance v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    const-string/jumbo v1, "ONE"

    invoke-direct {v0, v1, v3}, Lcom/android/services/telephony/TelecomAccountRegistry$Count;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->ONE:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    .line 87
    new-instance v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    const-string/jumbo v1, "TWO"

    invoke-direct {v0, v1, v4}, Lcom/android/services/telephony/TelecomAccountRegistry$Count;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->TWO:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    .line 84
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    sget-object v1, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->ZERO:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->ONE:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->TWO:Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->$VALUES:[Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/services/telephony/TelecomAccountRegistry$Count;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 84
    const-class v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    return-object v0
.end method

.method public static values()[Lcom/android/services/telephony/TelecomAccountRegistry$Count;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/android/services/telephony/TelecomAccountRegistry$Count;->$VALUES:[Lcom/android/services/telephony/TelecomAccountRegistry$Count;

    return-object v0
.end method
