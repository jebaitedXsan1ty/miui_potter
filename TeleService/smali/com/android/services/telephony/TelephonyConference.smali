.class public Lcom/android/services/telephony/TelephonyConference;
.super Landroid/telecom/Conference;
.source "TelephonyConference.java"


# direct methods
.method public constructor <init>(Landroid/telecom/PhoneAccountHandle;)V
    .locals 1
    .param p1, "phoneAccount"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/telecom/Conference;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    .line 38
    const v0, 0x8000c3

    .line 37
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConference;->setConnectionCapabilities(I)V

    .line 43
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConference;->setActive()V

    .line 44
    return-void
.end method

.method private disconnectCall(Landroid/telecom/Connection;)Z
    .locals 5
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    const/4 v4, 0x0

    .line 65
    const-string/jumbo v2, "onDisconnect"

    invoke-direct {p0, p1, v2}, Lcom/android/services/telephony/TelephonyConference;->getMultipartyCallForConnection(Landroid/telecom/Connection;Ljava/lang/String;)Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 66
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_0

    .line 67
    const-string/jumbo v2, "Found multiparty call to hangup for conference."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    :try_start_0
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->hangup()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    const/4 v2, 0x1

    return v2

    .line 71
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v2, "Exception thrown trying to hangup conference"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    .end local v1    # "e":Lcom/android/internal/telephony/CallStateException;
    :cond_0
    return v4
.end method

.method private getFirstConnection()Lcom/android/services/telephony/TelephonyConnection;
    .locals 2

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConference;->getConnections()Ljava/util/List;

    move-result-object v0

    .line 209
    .local v0, "connections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    const/4 v1, 0x0

    return-object v1

    .line 212
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/services/telephony/TelephonyConnection;

    return-object v1
.end method

.method private getMultipartyCallForConnection(Landroid/telecom/Connection;Ljava/lang/String;)Lcom/android/internal/telephony/Call;
    .locals 4
    .param p1, "connection"    # Landroid/telecom/Connection;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 187
    invoke-virtual {p0, p1}, Lcom/android/services/telephony/TelephonyConference;->getOriginalConnection(Landroid/telecom/Connection;)Lcom/android/internal/telephony/Connection;

    move-result-object v1

    .line 188
    .local v1, "radioConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v1, :cond_0

    .line 189
    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 190
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->isMultiparty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    return-object v0

    .line 194
    .end local v0    # "call":Lcom/android/internal/telephony/Call;
    :cond_0
    return-object v3
.end method


# virtual methods
.method protected getOriginalConnection(Landroid/telecom/Connection;)Lcom/android/internal/telephony/Connection;
    .locals 1
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 200
    instance-of v0, p1, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_0

    .line 201
    check-cast p1, Lcom/android/services/telephony/TelephonyConnection;

    .end local p1    # "connection":Landroid/telecom/Connection;
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    return-object v0

    .line 203
    .restart local p1    # "connection":Landroid/telecom/Connection;
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPrimaryConnection()Landroid/telecom/Connection;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 163
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConference;->getConnections()Ljava/util/List;

    move-result-object v2

    .line 164
    .local v2, "connections":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/Connection;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 165
    :cond_0
    return-object v6

    .line 169
    :cond_1
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telecom/Connection;

    .line 172
    .local v3, "primaryConnection":Landroid/telecom/Connection;
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "connection$iterator":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 174
    .local v0, "connection":Landroid/telecom/Connection;
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConference;->getOriginalConnection(Landroid/telecom/Connection;)Lcom/android/internal/telephony/Connection;

    move-result-object v4

    .line 176
    .local v4, "radioConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->isMultiparty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 177
    move-object v3, v0

    .line 182
    .end local v0    # "connection":Landroid/telecom/Connection;
    .end local v4    # "radioConnection":Lcom/android/internal/telephony/Connection;
    :cond_3
    return-object v3
.end method

.method public onAddParticipant(Ljava/lang/String;)V
    .locals 3
    .param p1, "participant"    # Ljava/lang/String;

    .prologue
    .line 96
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    const-string/jumbo v1, "Add participant not supported for GSM conference call."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    return-void
.end method

.method public onConnectionAdded(Landroid/telecom/Connection;)V
    .locals 1
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 154
    instance-of v0, p1, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_0

    .line 155
    check-cast p1, Lcom/android/services/telephony/TelephonyConnection;

    .end local p1    # "connection":Landroid/telecom/Connection;
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->wasImsConnection()Z

    move-result v0

    .line 154
    if-eqz v0, :cond_0

    .line 156
    const/16 v0, 0x80

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/TelephonyConference;->removeCapability(I)V

    .line 158
    :cond_0
    return-void
.end method

.method public onDisconnect()V
    .locals 3

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConference;->getConnections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "connection$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 52
    .local v0, "connection":Landroid/telecom/Connection;
    invoke-direct {p0, v0}, Lcom/android/services/telephony/TelephonyConference;->disconnectCall(Landroid/telecom/Connection;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    .end local v0    # "connection":Landroid/telecom/Connection;
    :cond_1
    return-void
.end method

.method public onHold()V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConference;->getFirstConnection()Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v0

    .line 117
    .local v0, "connection":Lcom/android/services/telephony/TelephonyConnection;
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->performHold()V

    .line 120
    :cond_0
    return-void
.end method

.method public onMerge(Landroid/telecom/Connection;)V
    .locals 4
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 102
    :try_start_0
    check-cast p1, Lcom/android/services/telephony/TelephonyConnection;

    .end local p1    # "connection":Landroid/telecom/Connection;
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 103
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->conference()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_0
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v2, "Exception thrown trying to merge call into a conference"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v2, v3}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onPlayDtmfTone(C)V
    .locals 1
    .param p1, "c"    # C

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConference;->getFirstConnection()Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v0

    .line 136
    .local v0, "connection":Lcom/android/services/telephony/TelephonyConnection;
    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnection;->onPlayDtmfTone(C)V

    .line 139
    :cond_0
    return-void
.end method

.method public onSeparate(Landroid/telecom/Connection;)V
    .locals 4
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/android/services/telephony/TelephonyConference;->getOriginalConnection(Landroid/telecom/Connection;)Lcom/android/internal/telephony/Connection;

    move-result-object v1

    .line 88
    .local v1, "radioConnection":Lcom/android/internal/telephony/Connection;
    :try_start_0
    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->separate()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v2, "Exception thrown trying to separate a conference call"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v2, v3}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onStopDtmfTone()V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConference;->getFirstConnection()Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v0

    .line 144
    .local v0, "connection":Lcom/android/services/telephony/TelephonyConnection;
    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->onStopDtmfTone()V

    .line 147
    :cond_0
    return-void
.end method

.method public onUnhold()V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConference;->getFirstConnection()Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v0

    .line 128
    .local v0, "connection":Lcom/android/services/telephony/TelephonyConnection;
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->performUnhold()V

    .line 131
    :cond_0
    return-void
.end method
