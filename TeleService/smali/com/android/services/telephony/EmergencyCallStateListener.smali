.class public Lcom/android/services/telephony/EmergencyCallStateListener;
.super Ljava/lang/Object;
.source "EmergencyCallStateListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/EmergencyCallStateListener$1;,
        Lcom/android/services/telephony/EmergencyCallStateListener$Callback;
    }
.end annotation


# static fields
.field private static MAX_NUM_RETRIES:I = 0x0

.field public static final MSG_RETRY_TIMEOUT:I = 0x3

.field public static final MSG_SERVICE_STATE_CHANGED:I = 0x2

.field public static final MSG_START_SEQUENCE:I = 0x1

.field private static TIME_BETWEEN_RETRIES_MILLIS:J


# instance fields
.field private mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

.field private final mHandler:Landroid/os/Handler;

.field private mNumRetriesSoFar:I

.field private mPhone:Lcom/android/internal/telephony/Phone;


# direct methods
.method static synthetic -wrap0(Lcom/android/services/telephony/EmergencyCallStateListener;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/EmergencyCallStateListener;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->onRetryTimeout()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/EmergencyCallStateListener;Landroid/telephony/ServiceState;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/EmergencyCallStateListener;
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/EmergencyCallStateListener;->onServiceStateChanged(Landroid/telephony/ServiceState;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/services/telephony/EmergencyCallStateListener;Lcom/android/internal/telephony/Phone;Lcom/android/services/telephony/EmergencyCallStateListener$Callback;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/EmergencyCallStateListener;
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "callback"    # Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/services/telephony/EmergencyCallStateListener;->startSequenceInternal(Lcom/android/internal/telephony/Phone;Lcom/android/services/telephony/EmergencyCallStateListener$Callback;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x5

    sput v0, Lcom/android/services/telephony/EmergencyCallStateListener;->MAX_NUM_RETRIES:I

    .line 49
    const-wide/16 v0, 0x2710

    sput-wide v0, Lcom/android/services/telephony/EmergencyCallStateListener;->TIME_BETWEEN_RETRIES_MILLIS:J

    .line 36
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/android/services/telephony/EmergencyCallStateListener$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/services/telephony/EmergencyCallStateListener$1;-><init>(Lcom/android/services/telephony/EmergencyCallStateListener;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mHandler:Landroid/os/Handler;

    .line 36
    return-void
.end method

.method private cancelRetryTimer()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 266
    return-void
.end method

.method private isOkToCall(I)Z
    .locals 2
    .param p1, "serviceState"    # I

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v0

    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    if-eq v0, v1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getServiceStateTracker()Lcom/android/internal/telephony/ServiceStateTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/ServiceStateTracker;->isRadioOn()Z

    move-result v0

    .line 183
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private onComplete(Z)V
    .locals 3
    .param p1, "isRadioReady"    # Z

    .prologue
    const/4 v2, 0x0

    .line 285
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    if-eqz v1, :cond_0

    .line 286
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    .line 287
    .local v0, "tempCallback":Lcom/android/services/telephony/EmergencyCallStateListener$Callback;
    iput-object v2, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    .line 288
    invoke-interface {v0, p0, p1}, Lcom/android/services/telephony/EmergencyCallStateListener$Callback;->onComplete(Lcom/android/services/telephony/EmergencyCallStateListener;Z)V

    .line 290
    .end local v0    # "tempCallback":Lcom/android/services/telephony/EmergencyCallStateListener$Callback;
    :cond_0
    return-void
.end method

.method private onRetryTimeout()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 191
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    .line 192
    .local v0, "serviceState":I
    const-string/jumbo v1, "onRetryTimeout():  phone state = %s, service state = %d, retries = %d."

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    .line 193
    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget v3, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mNumRetriesSoFar:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    .line 192
    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 201
    invoke-direct {p0, v0}, Lcom/android/services/telephony/EmergencyCallStateListener;->isOkToCall(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    const-string/jumbo v1, "onRetryTimeout: Radio is on. Cleaning up."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    invoke-direct {p0, v6}, Lcom/android/services/telephony/EmergencyCallStateListener;->onComplete(Z)V

    .line 206
    invoke-virtual {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->cleanup()V

    .line 223
    :goto_0
    return-void

    .line 211
    :cond_0
    iget v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mNumRetriesSoFar:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mNumRetriesSoFar:I

    .line 212
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mNumRetriesSoFar is now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mNumRetriesSoFar:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 214
    iget v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mNumRetriesSoFar:I

    sget v2, Lcom/android/services/telephony/EmergencyCallStateListener;->MAX_NUM_RETRIES:I

    if-le v1, v2, :cond_1

    .line 215
    const-string/jumbo v1, "Hit MAX_NUM_RETRIES; giving up."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    invoke-virtual {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->cleanup()V

    goto :goto_0

    .line 218
    :cond_1
    const-string/jumbo v1, "Trying (again) to turn on the radio."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1, v6}, Lcom/android/internal/telephony/Phone;->setRadioPower(Z)V

    .line 220
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->startRetryTimer()V

    goto :goto_0
.end method

.method private onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 5
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 148
    const-string/jumbo v0, "onServiceStateChanged(), new state = %s, Phone = %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v3

    .line 149
    iget-object v2, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 148
    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/services/telephony/EmergencyCallStateListener;->isOkToCall(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    return-void

    .line 167
    :cond_0
    const-string/jumbo v0, "onServiceStateChanged: ok to call!"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169
    invoke-direct {p0, v4}, Lcom/android/services/telephony/EmergencyCallStateListener;->onComplete(Z)V

    .line 170
    invoke-virtual {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->cleanup()V

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_1
    const-string/jumbo v0, "onServiceStateChanged: not ready to call yet, keep waiting."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private registerForServiceStateChanged()V
    .locals 4

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->unregisterForServiceStateChanged()V

    .line 273
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/Phone;->registerForServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 274
    return-void
.end method

.method private startRetryTimer()V
    .locals 4

    .prologue
    .line 260
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->cancelRetryTimer()V

    .line 261
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mHandler:Landroid/os/Handler;

    sget-wide v2, Lcom/android/services/telephony/EmergencyCallStateListener;->TIME_BETWEEN_RETRIES_MILLIS:J

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 262
    return-void
.end method

.method private startSequenceInternal(Lcom/android/internal/telephony/Phone;Lcom/android/services/telephony/EmergencyCallStateListener$Callback;)V
    .locals 2
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "callback"    # Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "startSequenceInternal: Phone "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    invoke-virtual {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->cleanup()V

    .line 133
    iput-object p1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 134
    iput-object p2, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    .line 136
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->registerForServiceStateChanged()V

    .line 140
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->startRetryTimer()V

    .line 141
    return-void
.end method

.method private unregisterForServiceStateChanged()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForServiceStateChanged(Landroid/os/Handler;)V

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 282
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 245
    const-string/jumbo v0, "cleanup()"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    invoke-direct {p0, v2}, Lcom/android/services/telephony/EmergencyCallStateListener;->onComplete(Z)V

    .line 251
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->unregisterForServiceStateChanged()V

    .line 252
    invoke-direct {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->cancelRetryTimer()V

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 256
    iput v2, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mNumRetriesSoFar:I

    .line 257
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 309
    if-ne p0, p1, :cond_0

    return v1

    .line 310
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/services/telephony/EmergencyCallStateListener;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->equals(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    :cond_1
    return v2

    :cond_2
    move-object v0, p1

    .line 312
    check-cast v0, Lcom/android/services/telephony/EmergencyCallStateListener;

    .line 314
    .local v0, "that":Lcom/android/services/telephony/EmergencyCallStateListener;
    iget v3, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mNumRetriesSoFar:I

    iget v4, v0, Lcom/android/services/telephony/EmergencyCallStateListener;->mNumRetriesSoFar:I

    if-eq v3, v4, :cond_3

    .line 315
    return v2

    .line 317
    :cond_3
    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    iget-object v4, v0, Lcom/android/services/telephony/EmergencyCallStateListener;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_6

    .line 318
    :cond_4
    return v2

    .line 317
    :cond_5
    iget-object v3, v0, Lcom/android/services/telephony/EmergencyCallStateListener;->mCallback:Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    if-nez v3, :cond_4

    .line 320
    :cond_6
    iget-object v3, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v3, :cond_8

    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v2, v0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v1, v2}, Lcom/android/internal/telephony/Phone;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_7
    :goto_0
    return v1

    :cond_8
    iget-object v3, v0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public setMaxNumRetries(I)V
    .locals 0
    .param p1, "retries"    # I

    .prologue
    .line 299
    sput p1, Lcom/android/services/telephony/EmergencyCallStateListener;->MAX_NUM_RETRIES:I

    .line 300
    return-void
.end method

.method public setTimeBetweenRetriesMillis(J)V
    .locals 1
    .param p1, "timeMs"    # J

    .prologue
    .line 304
    sput-wide p1, Lcom/android/services/telephony/EmergencyCallStateListener;->TIME_BETWEEN_RETRIES_MILLIS:J

    .line 305
    return-void
.end method

.method public waitForRadioOn(Lcom/android/internal/telephony/Phone;Lcom/android/services/telephony/EmergencyCallStateListener$Callback;)V
    .locals 3
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "callback"    # Lcom/android/services/telephony/EmergencyCallStateListener$Callback;

    .prologue
    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "waitForRadioOn: Phone "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mPhone:Lcom/android/internal/telephony/Phone;

    if-eqz v1, :cond_0

    .line 111
    return-void

    .line 114
    :cond_0
    invoke-static {}, Lcom/android/internal/os/SomeArgs;->obtain()Lcom/android/internal/os/SomeArgs;

    move-result-object v0

    .line 115
    .local v0, "args":Lcom/android/internal/os/SomeArgs;
    iput-object p1, v0, Lcom/android/internal/os/SomeArgs;->arg1:Ljava/lang/Object;

    .line 116
    iput-object p2, v0, Lcom/android/internal/os/SomeArgs;->arg2:Ljava/lang/Object;

    .line 117
    iget-object v1, p0, Lcom/android/services/telephony/EmergencyCallStateListener;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 118
    return-void
.end method
