.class public Lcom/android/services/telephony/LivetalkTelephonyHelper;
.super Ljava/lang/Object;
.source "LivetalkTelephonyHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cacheCallbackInfo(Ljava/lang/String;ZZ)V
    .locals 1
    .param p0, "callbackNumber"    # Ljava/lang/String;
    .param p1, "isCallbackNumber"    # Z
    .param p2, "isCallBackState"    # Z

    .prologue
    .line 139
    invoke-static {}, Lcom/miui/livetalk/LivetalkApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 140
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0, p0, p1, p2}, Lcom/miui/livetalk/LivetalkPrefs;->cacheCallBackInfo(Landroid/content/Context;Ljava/lang/String;ZZ)V

    .line 141
    return-void
.end method

.method private static isLivetalkDialing(Landroid/telecom/Connection;)Z
    .locals 8
    .param p0, "connection"    # Landroid/telecom/Connection;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 82
    if-eqz p0, :cond_0

    instance-of v4, p0, Lcom/android/services/telephony/TelephonyConnection;

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    .line 83
    :cond_0
    return v3

    :cond_1
    move-object v1, p0

    .line 85
    check-cast v1, Lcom/android/services/telephony/TelephonyConnection;

    .line 86
    .local v1, "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v0

    .line 87
    .local v0, "state":I
    const-string/jumbo v4, "LivetalkTelephonyHelper"

    const-string/jumbo v5, "isLivetalkDialing state: %s"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/android/services/telephony/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    iget-boolean v4, v1, Lcom/android/services/telephony/TelephonyConnection;->isLiveTalkConn:Z

    if-eqz v4, :cond_4

    .line 89
    if-eqz v0, :cond_2

    if-ne v0, v2, :cond_3

    .line 88
    :cond_2
    :goto_0
    return v2

    .line 89
    :cond_3
    const/4 v4, 0x3

    if-eq v0, v4, :cond_2

    move v2, v3

    goto :goto_0

    :cond_4
    move v2, v3

    .line 88
    goto :goto_0
.end method

.method public static maybeLivetalkCallbackConnection(Lcom/android/internal/telephony/Connection;)Z
    .locals 15
    .param p0, "connection"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 19
    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v10

    xor-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_1

    .line 20
    :cond_0
    return v13

    .line 23
    :cond_1
    const/4 v4, 0x0

    .line 24
    .local v4, "isLivatalkCallBackState":Z
    const/4 v5, 0x0

    .line 25
    .local v5, "livetalkConnection":Lcom/android/services/telephony/TelephonyConnection;
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/android/services/telephony/TelecomAccountRegistry;->getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;

    move-result-object v6

    .line 26
    .local v6, "registry":Lcom/android/services/telephony/TelecomAccountRegistry;
    if-eqz v6, :cond_3

    .line 27
    invoke-virtual {v6}, Lcom/android/services/telephony/TelecomAccountRegistry;->getTelephonyConnectionService()Lcom/android/services/telephony/TelephonyConnectionService;

    move-result-object v7

    .line 28
    .local v7, "service":Lcom/android/services/telephony/TelephonyConnectionService;
    if-eqz v7, :cond_3

    .line 29
    invoke-virtual {v7}, Lcom/android/services/telephony/TelephonyConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "telephonyConnection$iterator":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telecom/Connection;

    .line 30
    .local v8, "telephonyConnection":Landroid/telecom/Connection;
    invoke-static {v8}, Lcom/android/services/telephony/LivetalkTelephonyHelper;->isLivetalkDialing(Landroid/telecom/Connection;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 31
    move-object v0, v8

    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    move-object v5, v0

    .line 32
    .local v5, "livetalkConnection":Lcom/android/services/telephony/TelephonyConnection;
    const/4 v4, 0x1

    .line 39
    .end local v5    # "livetalkConnection":Lcom/android/services/telephony/TelephonyConnection;
    .end local v7    # "service":Lcom/android/services/telephony/TelephonyConnectionService;
    .end local v8    # "telephonyConnection":Landroid/telecom/Connection;
    .end local v9    # "telephonyConnection$iterator":Ljava/util/Iterator;
    :cond_3
    if-eqz v4, :cond_6

    .line 40
    const-string/jumbo v10, "LivetalkTelephonyHelper"

    const-string/jumbo v11, "in callback state"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/android/services/telephony/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    invoke-virtual {p0}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "address":Ljava/lang/String;
    const-string/jumbo v10, "LivetalkTelephonyHelper"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "original callback address:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v1}, Lcom/miui/livetalk/phone/NumberUtil;->getLogNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/android/services/telephony/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    invoke-static {v1}, Lcom/miui/livetalk/dial/LivetalkCall;->isCallbackNumber(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 45
    :cond_4
    const-string/jumbo v10, "LivetalkTelephonyHelper"

    const-string/jumbo v11, "is callback number, accept callback"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/android/services/telephony/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    invoke-static {}, Lcom/miui/livetalk/dial/LivetalkCall;->onBackCallAccepted()V

    .line 48
    invoke-virtual {v5, p0}, Lcom/android/services/telephony/TelephonyConnection;->setOriginalConnection(Lcom/android/internal/telephony/Connection;)V

    .line 49
    invoke-virtual {v5}, Lcom/android/services/telephony/TelephonyConnection;->onAnswer()V

    .line 50
    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-static {v1, v10, v11}, Lcom/android/services/telephony/LivetalkTelephonyHelper;->cacheCallbackInfo(Ljava/lang/String;ZZ)V

    .line 60
    :goto_0
    return v14

    .line 52
    :cond_5
    const-string/jumbo v10, "LivetalkTelephonyHelper"

    const-string/jumbo v11, "not callback number, hangup"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/android/services/telephony/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 54
    const/4 v10, 0x0

    const/4 v11, 0x1

    :try_start_1
    invoke-static {v1, v10, v11}, Lcom/android/services/telephony/LivetalkTelephonyHelper;->cacheCallbackInfo(Ljava/lang/String;ZZ)V

    .line 55
    invoke-virtual {p0}, Lcom/android/internal/telephony/Connection;->hangup()V
    :try_end_1
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 56
    :catch_0
    move-exception v2

    .local v2, "e":Lcom/android/internal/telephony/CallStateException;
    goto :goto_0

    .line 63
    .end local v1    # "address":Ljava/lang/String;
    .end local v2    # "e":Lcom/android/internal/telephony/CallStateException;
    :cond_6
    :try_start_2
    invoke-virtual {p0}, Lcom/android/internal/telephony/Connection;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 64
    .restart local v1    # "address":Ljava/lang/String;
    invoke-static {v1}, Lcom/miui/livetalk/dial/LivetalkCall;->isCallbackNumber(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 65
    const-string/jumbo v10, "LivetalkTelephonyHelper"

    const-string/jumbo v11, "not callback state but is callback number, hangup"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/android/services/telephony/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 67
    const/4 v10, 0x1

    const/4 v11, 0x0

    :try_start_3
    invoke-static {v1, v10, v11}, Lcom/android/services/telephony/LivetalkTelephonyHelper;->cacheCallbackInfo(Ljava/lang/String;ZZ)V

    .line 68
    invoke-virtual {p0}, Lcom/android/internal/telephony/Connection;->hangup()V
    :try_end_3
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 72
    :goto_1
    return v14

    .line 74
    .end local v1    # "address":Ljava/lang/String;
    .end local v4    # "isLivatalkCallBackState":Z
    .end local v6    # "registry":Lcom/android/services/telephony/TelecomAccountRegistry;
    :catch_1
    move-exception v3

    .line 75
    .local v3, "e":Ljava/lang/Exception;
    const-string/jumbo v10, "LivetalkTelephonyHelper"

    const-string/jumbo v11, "maybeLivetalkCallbackConnection"

    new-array v12, v13, [Ljava/lang/Object;

    invoke-static {v10, v3, v11, v12}, Lcom/android/services/telephony/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_7
    return v13

    .line 69
    .restart local v1    # "address":Ljava/lang/String;
    .restart local v4    # "isLivatalkCallBackState":Z
    .restart local v6    # "registry":Lcom/android/services/telephony/TelecomAccountRegistry;
    :catch_2
    move-exception v2

    .restart local v2    # "e":Lcom/android/internal/telephony/CallStateException;
    goto :goto_1
.end method

.method public static onTeleDisconnectCallback(ZILcom/android/services/telephony/TelephonyConnection;)V
    .locals 22
    .param p0, "isLivetalkConn"    # Z
    .param p1, "slotId"    # I
    .param p2, "connection"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 111
    if-nez p2, :cond_0

    .line 112
    const-string/jumbo v2, "LivetalkTelephonyHelper"

    const-string/jumbo v6, "onTeleDisconnectCallback: TelephonyConnection is null"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v2, v6, v0}, Lcom/android/services/telephony/Log;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    return-void

    .line 116
    :cond_0
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 117
    .local v16, "startTime":J
    const-wide/16 v4, 0x0

    .line 118
    .local v4, "durationMillis":J
    const/4 v7, 0x0

    .line 119
    .local v7, "isInComing":Z
    const-wide/16 v8, 0x0

    .line 120
    .local v8, "connectTime":J
    const-wide/16 v10, 0x0

    .line 121
    .local v10, "disconnectTime":J
    const-wide/16 v12, 0x0

    .line 122
    .local v12, "callStartTime":J
    invoke-virtual/range {p2 .. p2}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v15

    .line 123
    .local v15, "originalConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v15, :cond_1

    .line 124
    invoke-virtual {v15}, Lcom/android/internal/telephony/Connection;->getDurationMillis()J

    move-result-wide v4

    .line 125
    invoke-virtual {v15}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v7

    .line 126
    .local v7, "isInComing":Z
    invoke-virtual {v15}, Lcom/android/internal/telephony/Connection;->getConnectTime()J

    move-result-wide v8

    .line 127
    invoke-virtual {v15}, Lcom/android/internal/telephony/Connection;->getDisconnectTime()J

    move-result-wide v10

    .line 128
    invoke-virtual {v15}, Lcom/android/internal/telephony/Connection;->getCreateTime()J

    move-result-wide v12

    .line 130
    .end local v7    # "isInComing":Z
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/android/services/telephony/TelephonyConnection;->getAddress()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .local v3, "calloutNumber":Ljava/lang/String;
    move/from16 v2, p0

    move/from16 v6, p1

    .line 131
    invoke-static/range {v2 .. v13}, Lcom/miui/livetalk/dial/LivetalkCall;->onTeleDisconnect(ZLjava/lang/String;JIZJJJ)V

    .line 132
    const-string/jumbo v2, "LivetalkTelephonyHelper"

    const-string/jumbo v6, "onTeleDisconnectCallback use %s ms"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v16

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    const/16 v20, 0x0

    aput-object v19, v18, v20

    move-object/from16 v0, v18

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v2, v6, v0}, Lcom/android/services/telephony/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    .end local v3    # "calloutNumber":Ljava/lang/String;
    .end local v4    # "durationMillis":J
    .end local v8    # "connectTime":J
    .end local v10    # "disconnectTime":J
    .end local v12    # "callStartTime":J
    .end local v15    # "originalConnection":Lcom/android/internal/telephony/Connection;
    .end local v16    # "startTime":J
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v14

    .line 134
    .local v14, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "LivetalkTelephonyHelper"

    const-string/jumbo v6, "onTeleDisconnectCallback"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v14, v18, v19

    move-object/from16 v0, v18

    invoke-static {v2, v14, v6, v0}, Lcom/android/services/telephony/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static useLivetalkOutgoingCall(Landroid/content/Context;Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;Landroid/os/Bundle;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 93
    if-nez p1, :cond_0

    .line 94
    const/4 v6, 0x0

    return v6

    .line 97
    :cond_0
    const/4 v3, 0x0

    .line 99
    .local v3, "useLivetalk":Z
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 100
    .local v4, "startTime":J
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getAddress()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "number":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    .line 102
    .local v2, "phoneId":I
    invoke-static {p0, v1, v2, p3}, Lcom/miui/livetalk/dial/LivetalkCall;->useLivetalkCall(Landroid/content/Context;Ljava/lang/String;ILandroid/os/Bundle;)Z

    move-result v3

    .line 103
    .local v3, "useLivetalk":Z
    const-string/jumbo v6, "LivetalkTelephonyHelper"

    const-string/jumbo v7, "useLivetalkOutgoingCall use %s ms"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v4

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const/4 v10, 0x0

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/android/services/telephony/Log;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    .end local v1    # "number":Ljava/lang/String;
    .end local v2    # "phoneId":I
    .end local v3    # "useLivetalk":Z
    .end local v4    # "startTime":J
    :goto_0
    return v3

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v6, "LivetalkTelephonyHelper"

    const-string/jumbo v7, "useLivetalkOutgoingCall"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v0, v7, v8}, Lcom/android/services/telephony/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
