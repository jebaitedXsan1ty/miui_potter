.class public Lcom/android/services/telephony/TelephonyConnectionService;
.super Landroid/telecom/ConnectionService;
.source "TelephonyConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/TelephonyConnectionService$1;,
        Lcom/android/services/telephony/TelephonyConnectionService$2;,
        Lcom/android/services/telephony/TelephonyConnectionService$3;,
        Lcom/android/services/telephony/TelephonyConnectionService$4;,
        Lcom/android/services/telephony/TelephonyConnectionService$5;,
        Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;,
        Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;,
        Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;,
        Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;,
        Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;
    }
.end annotation


# static fields
.field private static final CDMA_ACTIVATION_CODE_REGEX_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private final mCdmaConferenceController:Lcom/android/services/telephony/CdmaConferenceController;

.field private mConnectionRemovedListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mEmergencyCallHelper:Lcom/android/services/telephony/EmergencyCallHelper;

.field private mEmergencyRetryCache:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/services/telephony/TelephonyConnection;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Phone;",
            ">;>;"
        }
    .end annotation
.end field

.field private mEmergencyTonePlayer:Lcom/android/services/telephony/EmergencyTonePlayer;

.field private mExpectedComponentName:Landroid/content/ComponentName;

.field private final mImsConferenceController:Lcom/android/services/telephony/ImsConferenceController;

.field private mIsPermDiscCauseReceived:[Z

.field private mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

.field private mSubscriptionManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;

.field private final mTelephonyConferenceController:Lcom/android/services/telephony/TelephonyConferenceController;

.field private final mTelephonyConnectionListener:Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

.field private final mTelephonyConnectionServiceProxy:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

.field private mTelephonyManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;


# direct methods
.method static synthetic -get0(Lcom/android/services/telephony/TelephonyConnectionService;)Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/services/telephony/TelephonyConnectionService;Landroid/telecom/ConnectionRequest;Ljava/lang/String;ZLandroid/net/Uri;Lcom/android/internal/telephony/Phone;)Landroid/telecom/Connection;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnectionService;
    .param p1, "request"    # Landroid/telecom/ConnectionRequest;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "isEmergencyNumber"    # Z
    .param p4, "handle"    # Landroid/net/Uri;
    .param p5, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    invoke-direct/range {p0 .. p5}, Lcom/android/services/telephony/TelephonyConnectionService;->getTelephonyConnection(Landroid/telecom/ConnectionRequest;Ljava/lang/String;ZLandroid/net/Uri;Lcom/android/internal/telephony/Phone;)Landroid/telecom/Connection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/TelephonyConnectionService;Landroid/telecom/PhoneAccountHandle;)Z
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnectionService;
    .param p1, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/TelephonyConnectionService;->isValidPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap2(Lcom/android/services/telephony/TelephonyConnectionService;Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnectionService;
    .param p1, "accountHandle"    # Landroid/telecom/PhoneAccountHandle;
    .param p2, "isEmergency"    # Z

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/services/telephony/TelephonyConnectionService;->getPhoneForAccount(Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap3(Lcom/android/services/telephony/TelephonyConnectionService;Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;Landroid/telecom/ConnectionRequest;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnectionService;
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p3, "request"    # Landroid/telecom/ConnectionRequest;

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lcom/android/services/telephony/TelephonyConnectionService;->placeOutgoingConnection(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;Landroid/telecom/ConnectionRequest;)V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/services/telephony/TelephonyConnectionService;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnectionService;->resetDisconnectCause()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/services/telephony/TelephonyConnectionService;Lcom/android/services/telephony/TelephonyConnection;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnectionService;
    .param p1, "c"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "isPermanentFailure"    # Z

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/services/telephony/TelephonyConnectionService;->retryOutgoingOriginalConnection(Lcom/android/services/telephony/TelephonyConnection;Z)V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/services/telephony/TelephonyConnectionService;Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/TelephonyConnectionService;
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/services/telephony/TelephonyConnectionService;->updatePhoneAccount(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-string/jumbo v0, "\\*228[0-9]{0,2}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 81
    sput-object v0, Lcom/android/services/telephony/TelephonyConnectionService;->CDMA_ACTIVATION_CODE_REGEX_PATTERN:Ljava/util/regex/Pattern;

    .line 78
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/telecom/ConnectionService;-><init>()V

    .line 86
    new-instance v0, Lcom/android/services/telephony/TelephonyConnectionService$1;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelephonyConnectionService$1;-><init>(Lcom/android/services/telephony/TelephonyConnectionService;)V

    .line 85
    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConnectionServiceProxy:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    .line 122
    new-instance v0, Lcom/android/services/telephony/TelephonyConferenceController;

    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConnectionServiceProxy:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    invoke-direct {v0, v1}, Lcom/android/services/telephony/TelephonyConferenceController;-><init>(Lcom/android/services/telephony/TelephonyConnectionServiceProxy;)V

    .line 121
    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConferenceController:Lcom/android/services/telephony/TelephonyConferenceController;

    .line 124
    new-instance v0, Lcom/android/services/telephony/CdmaConferenceController;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/CdmaConferenceController;-><init>(Lcom/android/services/telephony/TelephonyConnectionService;)V

    .line 123
    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mCdmaConferenceController:Lcom/android/services/telephony/CdmaConferenceController;

    .line 126
    new-instance v0, Lcom/android/services/telephony/ImsConferenceController;

    invoke-static {p0}, Lcom/android/services/telephony/TelecomAccountRegistry;->getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;

    move-result-object v1

    .line 127
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConnectionServiceProxy:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    .line 126
    invoke-direct {v0, v1, v2}, Lcom/android/services/telephony/ImsConferenceController;-><init>(Lcom/android/services/telephony/TelecomAccountRegistry;Lcom/android/services/telephony/TelephonyConnectionServiceProxy;)V

    .line 125
    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mImsConferenceController:Lcom/android/services/telephony/ImsConferenceController;

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mExpectedComponentName:Landroid/content/ComponentName;

    .line 134
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v0

    .line 133
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mIsPermDiscCauseReceived:[Z

    .line 166
    new-instance v0, Lcom/android/services/telephony/TelephonyConnectionService$2;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelephonyConnectionService$2;-><init>(Lcom/android/services/telephony/TelephonyConnectionService;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mSubscriptionManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;

    .line 189
    new-instance v0, Lcom/android/services/telephony/TelephonyConnectionService$3;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelephonyConnectionService$3;-><init>(Lcom/android/services/telephony/TelephonyConnectionService;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;

    .line 210
    new-instance v0, Lcom/android/services/telephony/TelephonyConnectionService$4;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelephonyConnectionService$4;-><init>(Lcom/android/services/telephony/TelephonyConnectionService;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    .line 246
    new-instance v0, Lcom/android/services/telephony/TelephonyConnectionService$5;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/TelephonyConnectionService$5;-><init>(Lcom/android/services/telephony/TelephonyConnectionService;)V

    .line 245
    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConnectionListener:Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    .line 264
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 263
    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mConnectionRemovedListeners:Ljava/util/List;

    .line 78
    return-void
.end method

.method private addConnectionRemovedListener(Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;)V
    .locals 1
    .param p1, "l"    # Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;

    .prologue
    .line 1424
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mConnectionRemovedListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1425
    return-void
.end method

.method private allowsMute(Lcom/android/internal/telephony/Phone;)Z
    .locals 2
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 1364
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1365
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->isInEcm()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1366
    const/4 v0, 0x0

    return v0

    .line 1370
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private blockCallForwardingNumberWhileRoaming(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Z
    .locals 7
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 940
    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    .line 941
    :cond_0
    return v4

    .line 943
    :cond_1
    const/4 v0, 0x0

    .line 945
    .local v0, "blockPrefixes":[Ljava/lang/String;
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v5, "carrier_config"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 944
    check-cast v1, Landroid/telephony/CarrierConfigManager;

    .line 946
    .local v1, "cfgManager":Landroid/telephony/CarrierConfigManager;
    if-eqz v1, :cond_2

    .line 947
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v3

    .line 948
    const-string/jumbo v5, "call_forwarding_blocks_while_roaming_string_array"

    .line 947
    invoke-virtual {v3, v5}, Landroid/os/PersistableBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 951
    .end local v0    # "blockPrefixes":[Ljava/lang/String;
    :cond_2
    if-eqz v0, :cond_4

    .line 952
    array-length v5, v0

    move v3, v4

    :goto_0
    if-ge v3, v5, :cond_4

    aget-object v2, v0, v3

    .line 953
    .local v2, "prefix":Ljava/lang/String;
    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 954
    const/4 v3, 0x1

    return v3

    .line 952
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 958
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_4
    return v4
.end method

.method private canAddCall()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 539
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v2

    .line 540
    .local v2, "connections":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/telecom/Connection;>;"
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "connection$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 541
    .local v0, "connection":Landroid/telecom/Connection;
    invoke-virtual {v0}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 542
    invoke-virtual {v0}, Landroid/telecom/Connection;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "android.telecom.extra.DISABLE_ADD_CALL"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 541
    if-eqz v3, :cond_0

    .line 543
    return v5

    .line 546
    .end local v0    # "connection":Landroid/telecom/Connection;
    :cond_1
    const/4 v3, 0x1

    return v3
.end method

.method private checkAdditionalOutgoingCallLimits(Lcom/android/internal/telephony/Phone;)Landroid/telecom/Connection;
    .locals 8
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v7, 0x0

    .line 1448
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 1451
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getAllConferences()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "conference$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/Conference;

    .line 1452
    .local v1, "conference":Landroid/telecom/Conference;
    instance-of v3, v1, Lcom/android/services/telephony/CdmaConference;

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 1453
    check-cast v0, Lcom/android/services/telephony/CdmaConference;

    .line 1457
    .local v0, "cdmaConf":Lcom/android/services/telephony/CdmaConference;
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lcom/android/services/telephony/CdmaConference;->can(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1458
    new-instance v3, Landroid/telecom/DisconnectCause;

    .line 1461
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0508

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1462
    const-string/jumbo v5, "merge-capable call exists, prevent flash command."

    .line 1459
    const/16 v6, 0x8

    .line 1458
    invoke-direct {v3, v6, v7, v4, v5}, Landroid/telecom/DisconnectCause;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 1468
    .end local v0    # "cdmaConf":Lcom/android/services/telephony/CdmaConference;
    .end local v1    # "conference":Landroid/telecom/Conference;
    .end local v2    # "conference$iterator":Ljava/util/Iterator;
    :cond_1
    return-object v7
.end method

.method private createConnectionFor(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/Connection;ZLandroid/telecom/PhoneAccountHandle;Ljava/lang/String;Landroid/net/Uri;I)Lcom/android/services/telephony/TelephonyConnection;
    .locals 7
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p2, "originalConnection"    # Lcom/android/internal/telephony/Connection;
    .param p3, "isOutgoing"    # Z
    .param p4, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;
    .param p5, "telecomCallId"    # Ljava/lang/String;
    .param p6, "address"    # Landroid/net/Uri;
    .param p7, "videoState"    # I

    .prologue
    .line 1149
    const/4 v0, 0x0

    .line 1150
    .local v0, "returnConnection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v6

    .line 1151
    .local v6, "phoneType":I
    const/4 v1, 0x1

    if-ne v6, v1, :cond_2

    .line 1152
    new-instance v0, Lcom/android/services/telephony/GsmConnection;

    .end local v0    # "returnConnection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-direct {v0, p2, p5, p3}, Lcom/android/services/telephony/GsmConnection;-><init>(Lcom/android/internal/telephony/Connection;Ljava/lang/String;Z)V

    .line 1158
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 1160
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConnectionListener:Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->addTelephonyConnectionListener(Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;)Lcom/android/services/telephony/TelephonyConnection;

    .line 1162
    invoke-static {p0}, Lcom/android/services/telephony/TelecomAccountRegistry;->getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/android/services/telephony/TelecomAccountRegistry;->isVideoPauseSupported(Landroid/telecom/PhoneAccountHandle;)Z

    move-result v1

    .line 1161
    invoke-virtual {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->setVideoPauseSupported(Z)V

    .line 1164
    invoke-direct {p0, v0}, Lcom/android/services/telephony/TelephonyConnectionService;->addConnectionRemovedListener(Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;)V

    .line 1166
    :cond_1
    return-object v0

    .line 1153
    .restart local v0    # "returnConnection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_2
    const/4 v1, 0x2

    if-ne v6, v1, :cond_0

    .line 1154
    invoke-direct {p0, p1}, Lcom/android/services/telephony/TelephonyConnectionService;->allowsMute(Lcom/android/internal/telephony/Phone;)Z

    move-result v3

    .line 1155
    .local v3, "allowsMute":Z
    new-instance v0, Lcom/android/services/telephony/CdmaConnection;

    .end local v0    # "returnConnection":Lcom/android/services/telephony/TelephonyConnection;
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyTonePlayer:Lcom/android/services/telephony/EmergencyTonePlayer;

    move-object v1, p2

    move v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/android/services/telephony/CdmaConnection;-><init>(Lcom/android/internal/telephony/Connection;Lcom/android/services/telephony/EmergencyTonePlayer;ZZLjava/lang/String;)V

    .local v0, "returnConnection":Lcom/android/services/telephony/TelephonyConnection;
    goto :goto_0
.end method

.method private fireOnConnectionRemoved(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 3
    .param p1, "conn"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 1434
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mConnectionRemovedListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "l$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;

    .line 1435
    .local v0, "l":Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;
    invoke-interface {v0, p1}, Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;->onConnectionRemoved(Lcom/android/services/telephony/TelephonyConnection;)V

    goto :goto_0

    .line 1437
    .end local v0    # "l":Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;
    :cond_0
    return-void
.end method

.method private getPhoneForAccount(Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;
    .locals 10
    .param p1, "accountHandle"    # Landroid/telecom/PhoneAccountHandle;
    .param p2, "isEmergency"    # Z

    .prologue
    const/4 v6, 0x0

    .line 1183
    const/4 v1, 0x0

    .line 1189
    .local v1, "chosenPhone":Lcom/android/internal/telephony/Phone;
    invoke-static {p1}, Lcom/android/phone/PhoneUtils;->getSubIdForPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)I

    move-result v4

    .line 1190
    .local v4, "subId":I
    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 1191
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mSubscriptionManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;

    invoke-interface {v5, v4}, Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;->getPhoneId(I)I

    move-result v3

    .line 1192
    .local v3, "phoneId":I
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v5, v3}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 1205
    .end local v1    # "chosenPhone":Lcom/android/internal/telephony/Phone;
    .end local v3    # "phoneId":I
    :cond_0
    if-eqz p2, :cond_2

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/telephony/ServiceState;->getState()I

    move-result v5

    if-eqz v5, :cond_2

    .line 1207
    :cond_1
    const-string/jumbo v5, "getPhoneForAccount: phone for phone acct handle %s is out of service or invalid for emergency call."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    .line 1208
    aput-object p1, v7, v6

    .line 1207
    invoke-static {p0, v5, v7}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1209
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getFirstPhoneForEmergencyCall()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 1210
    .local v1, "chosenPhone":Lcom/android/internal/telephony/Phone;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "getPhoneForAccount: using subId: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1211
    if-nez v1, :cond_5

    const-string/jumbo v5, "null"

    .line 1210
    :goto_0
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p0, v5, v6}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1214
    .end local v1    # "chosenPhone":Lcom/android/internal/telephony/Phone;
    :cond_2
    return-object v1

    .line 1194
    .local v1, "chosenPhone":Lcom/android/internal/telephony/Phone;
    :cond_3
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v5}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v7

    array-length v8, v7

    move v5, v6

    :goto_1
    if-ge v5, v8, :cond_0

    aget-object v2, v7, v5

    .line 1195
    .local v2, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 1196
    .local v0, "call":Lcom/android/internal/telephony/Call;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/internal/telephony/Call$State;->isRinging()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1197
    return-object v2

    .line 1194
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1211
    .end local v0    # "call":Lcom/android/internal/telephony/Call;
    .end local v2    # "phone":Lcom/android/internal/telephony/Phone;
    .local v1, "chosenPhone":Lcom/android/internal/telephony/Phone;
    :cond_5
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0
.end method

.method private getPhoneForRedial(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;
    .locals 5
    .param p1, "phoneUsed"    # Lcom/android/internal/telephony/Phone;

    .prologue
    const/4 v4, 0x0

    .line 1008
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyRetryCache:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1009
    .local v0, "cachedPhones":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/internal/telephony/Phone;>;"
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyRetryCache:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 1010
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1011
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/Phone;

    .line 1017
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 1018
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "getPhoneForRedial, removing Phone["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1019
    const-string/jumbo v3, "] from the available Phone cache."

    .line 1018
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1020
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 1025
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyRetryCache:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_2
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyRetryCache:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/telephony/Phone;

    goto :goto_0
.end method

.method private getTelephonyConnection(Landroid/telecom/ConnectionRequest;Ljava/lang/String;ZLandroid/net/Uri;Lcom/android/internal/telephony/Phone;)Landroid/telecom/Connection;
    .locals 25
    .param p1, "request"    # Landroid/telecom/ConnectionRequest;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "isEmergencyNumber"    # Z
    .param p4, "handle"    # Landroid/net/Uri;
    .param p5, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 554
    if-nez p5, :cond_4

    .line 556
    invoke-virtual/range {p0 .. p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "airplane_mode_on"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 558
    const-string/jumbo v3, "Phone is null"

    const/16 v4, 0x11

    .line 557
    invoke-static {v4, v3}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;)Landroid/telecom/DisconnectCause;

    move-result-object v3

    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 561
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    .line 562
    .local v14, "context":Landroid/content/Context;
    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0021

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 565
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v3}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v16

    .line 566
    .local v16, "defaultPhone":Lcom/android/internal/telephony/Phone;
    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/telephony/Phone;->getIccCard()Lcom/android/internal/telephony/IccCard;

    move-result-object v19

    .line 567
    .local v19, "icc":Lcom/android/internal/telephony/IccCard;
    sget-object v20, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    .line 568
    .local v20, "simState":Lcom/android/internal/telephony/IccCardConstants$State;
    if-eqz v19, :cond_1

    .line 569
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/IccCard;->getState()Lcom/android/internal/telephony/IccCardConstants$State;

    move-result-object v20

    .line 571
    :cond_1
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    move-object/from16 v0, v20

    if-ne v0, v3, :cond_3

    .line 572
    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 573
    const v4, 0x7f0b0270

    .line 572
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 574
    .local v23, "simUnlockUiPackage":Ljava/lang/String;
    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 575
    const v4, 0x7f0b0271

    .line 574
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 576
    .local v22, "simUnlockUiClass":Ljava/lang/String;
    if-eqz v23, :cond_2

    if-eqz v22, :cond_2

    .line 577
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    new-instance v4, Landroid/content/ComponentName;

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-direct {v4, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v21

    .line 579
    .local v21, "simUnlockIntent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 581
    :try_start_0
    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 589
    .end local v21    # "simUnlockIntent":Landroid/content/Intent;
    :cond_2
    :goto_0
    const-string/jumbo v3, "SIM_STATE_PIN_REQUIRED"

    .line 588
    const/16 v4, 0x12

    .line 590
    const/4 v5, 0x0

    .line 587
    invoke-static {v4, v3, v5}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 586
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 582
    .restart local v21    # "simUnlockIntent":Landroid/content/Intent;
    :catch_0
    move-exception v17

    .line 583
    .local v17, "exception":Landroid/content/ActivityNotFoundException;
    const-string/jumbo v3, "Unable to find SIM unlock UI activity."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1, v3, v4}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 594
    .end local v16    # "defaultPhone":Lcom/android/internal/telephony/Phone;
    .end local v17    # "exception":Landroid/content/ActivityNotFoundException;
    .end local v19    # "icc":Lcom/android/internal/telephony/IccCard;
    .end local v20    # "simState":Lcom/android/internal/telephony/IccCardConstants$State;
    .end local v21    # "simUnlockIntent":Landroid/content/Intent;
    .end local v22    # "simUnlockUiClass":Ljava/lang/String;
    .end local v23    # "simUnlockUiPackage":Ljava/lang/String;
    :cond_3
    const-string/jumbo v3, "onCreateOutgoingConnection, phone is null"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 597
    const-string/jumbo v3, "Phone is null"

    const/16 v4, 0x12

    .line 598
    const/4 v5, 0x0

    .line 596
    invoke-static {v4, v3, v5}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 595
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 603
    .end local v14    # "context":Landroid/content/Context;
    :cond_4
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    move-result v24

    .line 604
    .local v24, "state":I
    const/4 v3, 0x1

    move/from16 v0, v24

    if-ne v0, v3, :cond_6

    .line 605
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getDataNetworkType()I

    move-result v15

    .line 606
    .local v15, "dataNetType":I
    const/16 v3, 0xd

    if-eq v15, v3, :cond_5

    .line 607
    const/16 v3, 0x13

    if-ne v15, v3, :cond_6

    .line 608
    :cond_5
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getDataRegState()I

    move-result v24

    .line 614
    .end local v15    # "dataNetType":I
    :cond_6
    if-nez p3, :cond_8

    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->isInEcm()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 615
    const/4 v11, 0x1

    .line 617
    .local v11, "allowNonEmergencyCalls":Z
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "carrier_config"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    .line 616
    check-cast v12, Landroid/telephony/CarrierConfigManager;

    .line 618
    .local v12, "cfgManager":Landroid/telephony/CarrierConfigManager;
    if-eqz v12, :cond_7

    .line 619
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v3

    invoke-virtual {v12, v3}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v3

    .line 620
    const-string/jumbo v4, "allow_non_emergency_calls_in_ecm_bool"

    .line 619
    invoke-virtual {v3, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    .line 623
    .end local v11    # "allowNonEmergencyCalls":Z
    :cond_7
    if-nez v11, :cond_8

    .line 627
    const-string/jumbo v3, "Cannot make non-emergency call in ECM mode."

    .line 628
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 626
    const/16 v5, 0x22

    .line 625
    invoke-static {v5, v3, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 624
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 632
    .end local v12    # "cfgManager":Landroid/telephony/CarrierConfigManager;
    :cond_8
    if-nez p3, :cond_b

    .line 634
    move-object/from16 v0, p5

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/android/internal/telephony/gsm/GsmMmiCodeInjector;->isBlockedMmi(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 635
    const/4 v3, 0x3

    move-object/from16 v0, p5

    invoke-static {v0, v3}, Lcom/android/phone/MiuiPhoneUtils;->maybeShowDialogForSuppService(Lcom/android/internal/telephony/Phone;I)Z

    move-result v3

    .line 634
    if-eqz v3, :cond_9

    .line 636
    new-instance v3, Landroid/telecom/DisconnectCause;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 637
    :cond_9
    invoke-static/range {p5 .. p5}, Lcom/android/phone/MiuiPhoneUtils;->maybeShowTurnOffVolteForCall(Lcom/android/internal/telephony/Phone;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 638
    new-instance v3, Landroid/telecom/DisconnectCause;

    const/4 v4, 0x4

    invoke-direct {v3, v4}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 641
    :cond_a
    packed-switch v24, :pswitch_data_0

    .line 663
    const-string/jumbo v3, "onCreateOutgoingConnection, unknown service state: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 667
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unknown service state "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 668
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 666
    const/16 v5, 0x2b

    .line 665
    invoke-static {v5, v3, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 664
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 646
    :pswitch_0
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->isUtEnabled()Z

    move-result v3

    if-eqz v3, :cond_c

    const-string/jumbo v3, "#"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 647
    const-string/jumbo v3, "onCreateOutgoingConnection dial for UT"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 672
    :cond_b
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    .line 673
    .restart local v14    # "context":Landroid/content/Context;
    invoke-virtual/range {p1 .. p1}, Landroid/telecom/ConnectionRequest;->getVideoState()I

    move-result v3

    invoke-static {v3}, Landroid/telecom/VideoProfile;->isVideo(I)Z

    move-result v3

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/android/services/telephony/TelephonyConnectionService;->isTtyModeEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 674
    xor-int/lit8 v3, p3, 0x1

    .line 673
    if-eqz v3, :cond_d

    .line 677
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v3

    .line 676
    const/16 v4, 0x32

    .line 675
    invoke-static {v4, v3}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(II)Landroid/telecom/DisconnectCause;

    move-result-object v3

    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 653
    .end local v14    # "context":Landroid/content/Context;
    :cond_c
    const-string/jumbo v3, "ServiceState.STATE_OUT_OF_SERVICE"

    .line 654
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 652
    const/16 v5, 0x12

    .line 651
    invoke-static {v5, v3, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 650
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 660
    :pswitch_2
    const-string/jumbo v3, "ServiceState.STATE_POWER_OFF"

    .line 661
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 659
    const/16 v5, 0x11

    .line 658
    invoke-static {v5, v3, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 657
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 681
    .restart local v14    # "context":Landroid/content/Context;
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1}, Lcom/android/services/telephony/TelephonyConnectionService;->checkAdditionalOutgoingCallLimits(Lcom/android/internal/telephony/Phone;)Landroid/telecom/Connection;

    move-result-object v18

    .line 682
    .local v18, "failedConnection":Landroid/telecom/Connection;
    if-eqz v18, :cond_e

    .line 683
    return-object v18

    .line 687
    :cond_e
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/android/services/telephony/TelephonyConnectionService;->blockCallForwardingNumberWhileRoaming(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 691
    const-string/jumbo v3, "Call forwarding while roaming"

    .line 692
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 690
    const/16 v5, 0x39

    .line 689
    invoke-static {v5, v3, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 688
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 697
    :cond_f
    invoke-virtual/range {p1 .. p1}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v7

    .line 698
    invoke-virtual/range {p1 .. p1}, Landroid/telecom/ConnectionRequest;->getTelecomCallId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Landroid/telecom/ConnectionRequest;->getVideoState()I

    move-result v10

    .line 697
    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p5

    invoke-direct/range {v3 .. v10}, Lcom/android/services/telephony/TelephonyConnectionService;->createConnectionFor(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/Connection;ZLandroid/telecom/PhoneAccountHandle;Ljava/lang/String;Landroid/net/Uri;I)Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v13

    .line 699
    .local v13, "connection":Lcom/android/services/telephony/TelephonyConnection;
    if-nez v13, :cond_10

    .line 703
    const-string/jumbo v3, "Invalid phone type"

    .line 704
    invoke-virtual/range {p5 .. p5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 702
    const/16 v5, 0x2b

    .line 701
    invoke-static {v5, v3, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 700
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 707
    :cond_10
    if-nez p3, :cond_11

    .line 708
    invoke-virtual/range {p1 .. p1}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v3}, Lcom/android/phone/settings/AutoIpSetting;->handleIpCall(Landroid/net/Uri;Lcom/android/internal/telephony/Phone;Ljava/lang/String;Landroid/os/Bundle;)Landroid/net/Uri;

    move-result-object p4

    .line 711
    :cond_11
    const/4 v3, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v3}, Lcom/android/services/telephony/TelephonyConnection;->setAddress(Landroid/net/Uri;I)V

    .line 712
    invoke-virtual {v13}, Lcom/android/services/telephony/TelephonyConnection;->setInitializing()V

    .line 713
    invoke-virtual/range {p1 .. p1}, Landroid/telecom/ConnectionRequest;->getVideoState()I

    move-result v3

    invoke-virtual {v13, v3}, Lcom/android/services/telephony/TelephonyConnection;->setVideoState(I)V

    .line 715
    return-object v13

    .line 641
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private isAvailableForEmergencyCalls(Lcom/android/internal/telephony/Phone;)Z
    .locals 1
    .param p1, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 1351
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1352
    invoke-virtual {p1}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->isEmergencyOnly()Z

    move-result v0

    .line 1351
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isOriginalConnectionKnown(Lcom/android/internal/telephony/Connection;)Z
    .locals 4
    .param p1, "originalConnection"    # Lcom/android/internal/telephony/Connection;

    .prologue
    .line 1171
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getAllConnections()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "connection$iterator":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Connection;

    .line 1172
    .local v0, "connection":Landroid/telecom/Connection;
    instance-of v3, v0, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v3, :cond_0

    move-object v2, v0

    .line 1173
    check-cast v2, Lcom/android/services/telephony/TelephonyConnection;

    .line 1174
    .local v2, "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual {v2}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 1175
    const/4 v3, 0x1

    return v3

    .line 1179
    .end local v0    # "connection":Landroid/telecom/Connection;
    .end local v2    # "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_1
    const/4 v3, 0x0

    return v3
.end method

.method private isRadioOn()Z
    .locals 6

    .prologue
    .line 970
    const/4 v1, 0x0

    .line 971
    .local v1, "result":Z
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v2}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v3

    const/4 v2, 0x0

    array-length v4, v3

    .end local v1    # "result":Z
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 972
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->isRadioOn()Z

    move-result v5

    or-int/2addr v1, v5

    .line 971
    .local v1, "result":Z
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 974
    .end local v0    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v1    # "result":Z
    :cond_0
    return v1
.end method

.method private isTtyModeEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 1473
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1474
    const-string/jumbo v2, "preferred_tty_mode"

    .line 1472
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isValidPhoneAccountHandle(Landroid/telecom/PhoneAccountHandle;)Z
    .locals 2
    .param p1, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 530
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 531
    invoke-virtual {p1}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 530
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$-com_android_services_telephony_TelephonyConnectionService_63182(Lcom/android/internal/telephony/Phone;ILcom/android/services/telephony/TelephonyConnectionService$SlotStatus;Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;)I
    .locals 5
    .param p0, "firstOccupiedSlot"    # Lcom/android/internal/telephony/Phone;
    .param p1, "defaultPhoneId"    # I
    .param p2, "o1"    # Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;
    .param p3, "o2"    # Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 1293
    iget-boolean v1, p2, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->isLocked:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p3, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->isLocked:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1294
    return v3

    .line 1296
    :cond_0
    iget-boolean v1, p3, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->isLocked:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p2, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->isLocked:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 1297
    return v4

    .line 1300
    :cond_1
    iget v1, p2, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->capabilities:I

    invoke-static {v1}, Ljava/lang/Integer;->bitCount(I)I

    move-result v1

    .line 1301
    iget v2, p3, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->capabilities:I

    invoke-static {v2}, Ljava/lang/Integer;->bitCount(I)I

    move-result v2

    .line 1300
    sub-int v0, v1, v2

    .line 1302
    .local v0, "compare":I
    if-nez v0, :cond_5

    .line 1304
    iget v1, p2, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->capabilities:I

    invoke-static {v1}, Landroid/telephony/RadioAccessFamily;->getHighestRafCapability(I)I

    move-result v1

    .line 1305
    iget v2, p3, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->capabilities:I

    invoke-static {v2}, Landroid/telephony/RadioAccessFamily;->getHighestRafCapability(I)I

    move-result v2

    .line 1304
    sub-int v0, v1, v2

    .line 1306
    if-nez v0, :cond_5

    .line 1307
    if-eqz p0, :cond_3

    .line 1311
    iget v1, p2, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->slotId:I

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 1312
    return v4

    .line 1313
    :cond_2
    iget v1, p3, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->slotId:I

    invoke-virtual {p0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    if-ne v1, v2, :cond_5

    .line 1314
    return v3

    .line 1319
    :cond_3
    iget v1, p2, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->slotId:I

    if-ne v1, p1, :cond_4

    .line 1320
    return v4

    .line 1321
    :cond_4
    iget v1, p3, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->slotId:I

    if-ne v1, p1, :cond_5

    .line 1322
    return v3

    .line 1327
    :cond_5
    return v0
.end method

.method private makeCachedConnectionPhonePair(Lcom/android/services/telephony/TelephonyConnection;)Landroid/util/Pair;
    .locals 7
    .param p1, "c"    # Lcom/android/services/telephony/TelephonyConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/services/telephony/TelephonyConnection;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/android/services/telephony/TelephonyConnection;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/telephony/Phone;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 986
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;

    invoke-interface {v2}, Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;->getPhoneCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 988
    .local v1, "phones":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/Phone;>;"
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v2}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getPhones()[Lcom/android/internal/telephony/Phone;

    move-result-object v3

    const/4 v2, 0x0

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 989
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    iget-object v5, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mIsPermDiscCauseReceived:[Z

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v6

    aget-boolean v5, v5, v6

    if-nez v5, :cond_0

    .line 990
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 988
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 993
    .end local v0    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_1
    new-instance v2, Landroid/util/Pair;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v2, v3, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method private maybeSendInternationalCallEvent(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 5
    .param p1, "telephonyConnection"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    const/4 v4, 0x0

    .line 1484
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1486
    :cond_0
    return-void

    .line 1485
    :cond_1
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1488
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 1489
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    instance-of v2, v1, Lcom/android/internal/telephony/GsmCdmaPhone;

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 1490
    check-cast v0, Lcom/android/internal/telephony/GsmCdmaPhone;

    .line 1491
    .local v0, "gsmCdmaPhone":Lcom/android/internal/telephony/GsmCdmaPhone;
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->isOutgoingCall()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1493
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->getOrigDialString()Ljava/lang/String;

    move-result-object v2

    .line 1492
    invoke-virtual {v0, v2}, Lcom/android/internal/telephony/GsmCdmaPhone;->isNotificationOfWfcCallRequired(Ljava/lang/String;)Z

    move-result v2

    .line 1491
    if-eqz v2, :cond_2

    .line 1496
    const-string/jumbo v2, "placeOutgoingConnection - sending international call on WFC confirmation event"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1499
    const-string/jumbo v2, "android.telephony.event.EVENT_NOTIFY_INTERNATIONAL_CALL_ON_WFC"

    .line 1498
    invoke-virtual {p1, v2, v4}, Lcom/android/services/telephony/TelephonyConnection;->sendConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1502
    .end local v0    # "gsmCdmaPhone":Lcom/android/internal/telephony/GsmCdmaPhone;
    :cond_2
    return-void
.end method

.method private maybeSendPhoneAccountUpdateEvent(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 2
    .param p1, "telephonyConnection"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 1505
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1506
    :cond_0
    return-void

    .line 1509
    :cond_1
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 1508
    invoke-direct {p0, p1, v0}, Lcom/android/services/telephony/TelephonyConnectionService;->updatePhoneAccount(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;)V

    .line 1510
    return-void
.end method

.method private placeOutgoingConnection(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;ILandroid/os/Bundle;)V
    .locals 10
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p3, "videoState"    # I
    .param p4, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 1073
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getAddress()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    .line 1074
    .local v4, "number":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 1075
    const-string/jumbo v8, "add_participant"

    const/4 v9, 0x0

    .line 1074
    invoke-virtual {p4, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 1076
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "placeOutgoingConnection isAddParticipant = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v8, v9}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1078
    const/4 v5, 0x0

    .line 1080
    .local v5, "originalConnection":Lcom/android/internal/telephony/Connection;
    if-eqz p2, :cond_4

    .line 1081
    if-eqz v3, :cond_1

    .line 1082
    :try_start_0
    invoke-virtual {p2, v4}, Lcom/android/internal/telephony/Phone;->addParticipant(Ljava/lang/String;)V

    .line 1083
    return-void

    .line 1074
    .end local v5    # "originalConnection":Lcom/android/internal/telephony/Connection;
    :cond_0
    const/4 v3, 0x0

    .local v3, "isAddParticipant":Z
    goto :goto_0

    .line 1086
    .end local v3    # "isAddParticipant":Z
    .restart local v5    # "originalConnection":Lcom/android/internal/telephony/Connection;
    :cond_1
    invoke-static {p3}, Landroid/telecom/VideoProfile;->isAudioOnly(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1087
    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, p1, p2, p4}, Lcom/android/services/telephony/LivetalkTelephonyHelper;->useLivetalkOutgoingCall(Landroid/content/Context;Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;Landroid/os/Bundle;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1088
    invoke-virtual {p2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v8

    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, Lcom/android/services/telephony/TelephonyConnection;->setUseLivetalk(ZI)V

    .line 1089
    const-string/jumbo v8, "placeOutgoingConnection use livetalk"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v8, v9}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1090
    return-void

    .line 1093
    :cond_2
    invoke-virtual {p2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {p1, v9, v8}, Lcom/android/services/telephony/TelephonyConnection;->setUseLivetalk(ZI)V

    .line 1097
    :cond_3
    const/4 v8, 0x0

    invoke-virtual {p2, v4, v8, p3, p4}, Lcom/android/internal/telephony/Phone;->dial(Ljava/lang/String;Lcom/android/internal/telephony/UUSInfo;ILandroid/os/Bundle;)Lcom/android/internal/telephony/Connection;
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 1113
    .end local v5    # "originalConnection":Lcom/android/internal/telephony/Connection;
    :cond_4
    if-nez v5, :cond_9

    .line 1114
    const/16 v7, 0x2b

    .line 1116
    .local v7, "telephonyDisconnectCause":I
    invoke-virtual {p2}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    .line 1117
    const-string/jumbo v8, "dialed MMI code"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v8, v9}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1118
    invoke-virtual {p2}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v6

    .line 1119
    .local v6, "subId":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "subId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v8, v9}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1120
    const/16 v7, 0x27

    .line 1123
    new-instance v2, Landroid/content/Intent;

    const-class v8, Lcom/android/phone/MiuiMMIDialogActivity;

    invoke-direct {v2, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1124
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v8, 0x10800000

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1126
    invoke-static {v6}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1127
    const-string/jumbo v8, "subscription"

    invoke-virtual {v2, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1131
    :cond_5
    sget-object v8, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p0, v2, v8}, Lcom/android/services/telephony/TelephonyConnectionService;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1133
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v6    # "subId":I
    :cond_6
    const-string/jumbo v8, "placeOutgoingConnection, phone.dial returned null"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v8, v9}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1135
    const-string/jumbo v8, "Connection is null"

    invoke-virtual {p2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v9

    .line 1134
    invoke-static {v7, v8, v9}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 1139
    .end local v7    # "telephonyDisconnectCause":I
    :goto_1
    return-void

    .line 1100
    .restart local v5    # "originalConnection":Lcom/android/internal/telephony/Connection;
    :catch_0
    move-exception v1

    .line 1101
    .local v1, "e":Lcom/android/internal/telephony/CallStateException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "placeOutgoingConnection, phone.dial exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p0, v1, v8, v9}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1102
    const/16 v0, 0x2b

    .line 1103
    .local v0, "cause":I
    invoke-virtual {v1}, Lcom/android/internal/telephony/CallStateException;->getError()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_8

    .line 1104
    const/16 v0, 0x12

    .line 1109
    :cond_7
    :goto_2
    invoke-virtual {v1}, Lcom/android/internal/telephony/CallStateException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v9

    .line 1108
    invoke-static {v0, v8, v9}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 1110
    return-void

    .line 1105
    :cond_8
    invoke-virtual {v1}, Lcom/android/internal/telephony/CallStateException;->getError()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_7

    .line 1106
    const/16 v0, 0x11

    goto :goto_2

    .line 1137
    .end local v0    # "cause":I
    .end local v1    # "e":Lcom/android/internal/telephony/CallStateException;
    .end local v5    # "originalConnection":Lcom/android/internal/telephony/Connection;
    :cond_9
    invoke-virtual {p1, v5}, Lcom/android/services/telephony/TelephonyConnection;->setOriginalConnection(Lcom/android/internal/telephony/Connection;)V

    goto :goto_1
.end method

.method private placeOutgoingConnection(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;Landroid/telecom/ConnectionRequest;)V
    .locals 2
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "phone"    # Lcom/android/internal/telephony/Phone;
    .param p3, "request"    # Landroid/telecom/ConnectionRequest;

    .prologue
    .line 1068
    invoke-virtual {p3}, Landroid/telecom/ConnectionRequest;->getVideoState()I

    move-result v0

    invoke-virtual {p3}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/services/telephony/TelephonyConnectionService;->placeOutgoingConnection(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;ILandroid/os/Bundle;)V

    .line 1069
    return-void
.end method

.method private removeConnectionRemovedListener(Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;)V
    .locals 1
    .param p1, "l"    # Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;

    .prologue
    .line 1428
    if-eqz p1, :cond_0

    .line 1429
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mConnectionRemovedListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1431
    :cond_0
    return-void
.end method

.method private resetDisconnectCause()V
    .locals 3

    .prologue
    .line 978
    const/4 v0, 0x0

    .local v0, "phoneId":I
    :goto_0
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;

    invoke-interface {v1}, Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;->getPhoneCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 979
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mIsPermDiscCauseReceived:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 978
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 981
    :cond_0
    return-void
.end method

.method private retryOutgoingOriginalConnection(Lcom/android/services/telephony/TelephonyConnection;Z)V
    .locals 7
    .param p1, "c"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "isPermanentFailure"    # Z

    .prologue
    const/4 v6, 0x0

    .line 1030
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    .line 1032
    .local v2, "phoneId":I
    iget-object v4, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mIsPermDiscCauseReceived:[Z

    aput-boolean p2, v4, v2

    .line 1034
    invoke-direct {p0, p1}, Lcom/android/services/telephony/TelephonyConnectionService;->makeCachedConnectionPhonePair(Lcom/android/services/telephony/TelephonyConnection;)Landroid/util/Pair;

    move-result-object v4

    iput-object v4, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyRetryCache:Landroid/util/Pair;

    .line 1035
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/services/telephony/TelephonyConnectionService;->getPhoneForRedial(Lcom/android/internal/telephony/Phone;)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 1036
    .local v1, "newPhoneToUse":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_1

    .line 1037
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getVideoState()I

    move-result v3

    .line 1038
    .local v3, "videoState":I
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1039
    .local v0, "connExtras":Landroid/os/Bundle;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "retryOutgoingOriginalConnection, redialing on Phone Id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1040
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->clearOriginalConnection()V

    .line 1041
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    if-eq v2, v4, :cond_0

    invoke-direct {p0, p1, v1}, Lcom/android/services/telephony/TelephonyConnectionService;->updatePhoneAccount(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;)V

    .line 1042
    :cond_0
    invoke-direct {p0, p1, v1, v3, v0}, Lcom/android/services/telephony/TelephonyConnectionService;->placeOutgoingConnection(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;ILandroid/os/Bundle;)V

    .line 1051
    .end local v0    # "connExtras":Landroid/os/Bundle;
    .end local v3    # "videoState":I
    :goto_0
    return-void

    .line 1045
    :cond_1
    const-string/jumbo v4, "retryOutgoingOriginalConnection, no more Phones to use. Disconnecting."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1046
    new-instance v4, Landroid/telecom/DisconnectCause;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p1, v4}, Lcom/android/services/telephony/TelephonyConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 1047
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->clearOriginalConnection()V

    .line 1048
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnectionService;->resetDisconnectCause()V

    .line 1049
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->destroy()V

    goto :goto_0
.end method

.method private updatePhoneAccount(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;)V
    .locals 4
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "phone"    # Lcom/android/internal/telephony/Phone;

    .prologue
    .line 1054
    invoke-static {p2}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v1

    .line 1059
    .local v1, "pHandle":Landroid/telecom/PhoneAccountHandle;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updatePhoneAccount setPhoneAccountHandle, account = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1060
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1061
    .local v0, "extrasAccountHandle":Landroid/os/Bundle;
    const-string/jumbo v2, "emr_dial_account"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1062
    const-string/jumbo v2, "org.codeaurora.event.PHONE_ACCOUNT_CHANGED"

    invoke-virtual {p1, v2, v0}, Lcom/android/services/telephony/TelephonyConnection;->sendConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1064
    return-void
.end method


# virtual methods
.method public addConnectionToConferenceController(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 4
    .param p1, "connection"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    const/4 v3, 0x0

    .line 1397
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->isImsConnection()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Adding IMS connection to conference controller: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1399
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mImsConferenceController:Lcom/android/services/telephony/ImsConferenceController;

    invoke-virtual {v1, p1}, Lcom/android/services/telephony/ImsConferenceController;->add(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 1400
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConferenceController:Lcom/android/services/telephony/TelephonyConferenceController;

    invoke-virtual {v1, p1}, Lcom/android/services/telephony/TelephonyConferenceController;->remove(Landroid/telecom/Connection;)V

    .line 1401
    instance-of v1, p1, Lcom/android/services/telephony/CdmaConnection;

    if-eqz v1, :cond_0

    .line 1402
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mCdmaConferenceController:Lcom/android/services/telephony/CdmaConferenceController;

    check-cast p1, Lcom/android/services/telephony/CdmaConnection;

    .end local p1    # "connection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-virtual {v1, p1}, Lcom/android/services/telephony/CdmaConferenceController;->remove(Lcom/android/services/telephony/CdmaConnection;)V

    .line 1421
    :cond_0
    :goto_0
    return-void

    .line 1405
    .restart local p1    # "connection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_1
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v0

    .line 1406
    .local v0, "phoneType":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1407
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Adding GSM connection to conference controller: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1408
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConferenceController:Lcom/android/services/telephony/TelephonyConferenceController;

    invoke-virtual {v1, p1}, Lcom/android/services/telephony/TelephonyConferenceController;->add(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 1409
    instance-of v1, p1, Lcom/android/services/telephony/CdmaConnection;

    if-eqz v1, :cond_2

    .line 1410
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mCdmaConferenceController:Lcom/android/services/telephony/CdmaConferenceController;

    move-object v1, p1

    check-cast v1, Lcom/android/services/telephony/CdmaConnection;

    invoke-virtual {v2, v1}, Lcom/android/services/telephony/CdmaConferenceController;->remove(Lcom/android/services/telephony/CdmaConnection;)V

    .line 1418
    :cond_2
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Removing connection from IMS conference controller: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1419
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mImsConferenceController:Lcom/android/services/telephony/ImsConferenceController;

    invoke-virtual {v1, p1}, Lcom/android/services/telephony/ImsConferenceController;->remove(Landroid/telecom/Connection;)V

    goto :goto_0

    .line 1412
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1413
    instance-of v1, p1, Lcom/android/services/telephony/CdmaConnection;

    .line 1412
    if-eqz v1, :cond_2

    .line 1414
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Adding CDMA connection to conference controller: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1415
    iget-object v2, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mCdmaConferenceController:Lcom/android/services/telephony/CdmaConferenceController;

    move-object v1, p1

    check-cast v1, Lcom/android/services/telephony/CdmaConnection;

    invoke-virtual {v2, v1}, Lcom/android/services/telephony/CdmaConferenceController;->add(Lcom/android/services/telephony/CdmaConnection;)V

    .line 1416
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConferenceController:Lcom/android/services/telephony/TelephonyConferenceController;

    invoke-virtual {v1, p1}, Lcom/android/services/telephony/TelephonyConferenceController;->remove(Landroid/telecom/Connection;)V

    goto :goto_1
.end method

.method public getFirstPhoneForEmergencyCall()Lcom/android/internal/telephony/Phone;
    .locals 15

    .prologue
    .line 1231
    iget-object v13, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mSubscriptionManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;

    invoke-interface {v13}, Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;->getDefaultVoicePhoneId()I

    move-result v8

    .line 1232
    .local v8, "phoneId":I
    const/4 v13, -0x1

    if-eq v8, v13, :cond_0

    .line 1233
    iget-object v13, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v13, v8}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 1234
    .local v0, "defaultPhone":Lcom/android/internal/telephony/Phone;
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/services/telephony/TelephonyConnectionService;->isAvailableForEmergencyCalls(Lcom/android/internal/telephony/Phone;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1235
    return-object v0

    .line 1239
    .end local v0    # "defaultPhone":Lcom/android/internal/telephony/Phone;
    :cond_0
    const/4 v3, 0x0

    .line 1240
    .local v3, "firstPhoneWithSim":Lcom/android/internal/telephony/Phone;
    iget-object v13, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;

    invoke-interface {v13}, Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;->getPhoneCount()I

    move-result v7

    .line 1241
    .local v7, "phoneCount":I
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 1242
    .local v9, "phoneSlotStatus":Ljava/util/List;, "Ljava/util/List<Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;>;"
    const/4 v4, 0x0

    .end local v3    # "firstPhoneWithSim":Lcom/android/internal/telephony/Phone;
    .local v4, "i":I
    :goto_0
    if-ge v4, v7, :cond_6

    .line 1243
    iget-object v13, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v13, v4}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v6

    .line 1244
    .local v6, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v6, :cond_2

    .line 1242
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1248
    :cond_2
    invoke-direct {p0, v6}, Lcom/android/services/telephony/TelephonyConnectionService;->isAvailableForEmergencyCalls(Lcom/android/internal/telephony/Phone;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1250
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "getFirstPhoneForEmergencyCall, radio on & in service, Phone Id:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {p0, v13, v14}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1251
    return-object v6

    .line 1255
    :cond_3
    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getRadioAccessFamily()I

    move-result v10

    .line 1256
    .local v10, "radioAccessFamily":I
    new-instance v12, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;

    invoke-direct {v12, v4, v10}, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;-><init>(II)V

    .line 1257
    .local v12, "status":Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;
    invoke-interface {v9, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1258
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "getFirstPhoneForEmergencyCall, RAF:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 1259
    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v14

    .line 1258
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 1259
    const-string/jumbo v14, " saved for Phone Id:"

    .line 1258
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {p0, v13, v14}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1262
    iget-object v13, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mSubscriptionManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;

    invoke-interface {v13, v4}, Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;->getSimStateForSlotIdx(I)I

    move-result v11

    .line 1263
    .local v11, "simState":I
    const/4 v13, 0x2

    if-eq v11, v13, :cond_4

    .line 1264
    const/4 v13, 0x3

    if-ne v11, v13, :cond_5

    .line 1265
    :cond_4
    const/4 v13, 0x1

    iput-boolean v13, v12, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->isLocked:Z

    .line 1268
    :cond_5
    if-nez v3, :cond_1

    iget-object v13, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;

    invoke-interface {v13, v4}, Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;->hasIccCard(I)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 1272
    move-object v3, v6

    .line 1273
    .local v3, "firstPhoneWithSim":Lcom/android/internal/telephony/Phone;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "getFirstPhoneForEmergencyCall, SIM card inserted, Phone Id:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 1274
    invoke-virtual {v6}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v14

    .line 1273
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {p0, v13, v14}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1278
    .end local v3    # "firstPhoneWithSim":Lcom/android/internal/telephony/Phone;
    .end local v6    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v10    # "radioAccessFamily":I
    .end local v11    # "simState":I
    .end local v12    # "status":Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;
    :cond_6
    if-nez v3, :cond_7

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_7

    .line 1280
    const-string/jumbo v13, "getFirstPhoneForEmergencyCall, return default phone"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {p0, v13, v14}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1281
    iget-object v13, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v13}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v13

    return-object v13

    .line 1284
    :cond_7
    iget-object v13, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v13}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v1

    .line 1285
    .local v1, "defaultPhoneId":I
    move-object v2, v3

    .line 1286
    .local v2, "firstOccupiedSlot":Lcom/android/internal/telephony/Phone;
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_a

    .line 1288
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v13

    const/4 v14, 0x1

    if-le v13, v14, :cond_8

    .line 1289
    new-instance v13, Lcom/android/services/telephony/-$Lambda$Njr3YxkjEOpkgwwRMCF9rAaEy74;

    invoke-direct {v13, v1, v2}, Lcom/android/services/telephony/-$Lambda$Njr3YxkjEOpkgwwRMCF9rAaEy74;-><init>(ILjava/lang/Object;)V

    invoke-static {v9, v13}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1330
    :cond_8
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-interface {v9, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;

    iget v5, v13, Lcom/android/services/telephony/TelephonyConnectionService$SlotStatus;->slotId:I

    .line 1333
    .local v5, "mostCapablePhoneId":I
    invoke-static {}, Lmiui/telephony/TelephonyManagerEx;->getDefault()Lmiui/telephony/TelephonyManagerEx;

    move-result-object v13

    invoke-virtual {v13}, Lmiui/telephony/TelephonyManagerEx;->hasIccCard()Z

    move-result v13

    if-nez v13, :cond_9

    .line 1334
    const/4 v5, 0x0

    .line 1337
    :cond_9
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "getFirstPhoneForEmergencyCall, Using Phone Id: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 1338
    const-string/jumbo v14, "with highest capability"

    .line 1337
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {p0, v13, v14}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1339
    iget-object v13, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v13, v5}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v13

    return-object v13

    .line 1342
    .end local v5    # "mostCapablePhoneId":I
    :cond_a
    return-object v3
.end method

.method public onAddParticipant(Landroid/telecom/Connection;Ljava/lang/String;)V
    .locals 1
    .param p1, "connection"    # Landroid/telecom/Connection;
    .param p2, "participant"    # Ljava/lang/String;

    .prologue
    .line 963
    instance-of v0, p1, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_0

    .line 964
    check-cast p1, Lcom/android/services/telephony/TelephonyConnection;

    .end local p1    # "connection":Landroid/telecom/Connection;
    invoke-virtual {p1, p2}, Lcom/android/services/telephony/TelephonyConnection;->performAddParticipant(Ljava/lang/String;)V

    .line 967
    :cond_0
    return-void
.end method

.method public onConference(Landroid/telecom/Connection;Landroid/telecom/Connection;)V
    .locals 3
    .param p1, "connection1"    # Landroid/telecom/Connection;
    .param p2, "connection2"    # Landroid/telecom/Connection;

    .prologue
    .line 929
    instance-of v0, p1, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_0

    .line 930
    check-cast p1, Lcom/android/services/telephony/TelephonyConnection;

    .end local p1    # "connection1":Landroid/telecom/Connection;
    invoke-virtual {p1, p2}, Lcom/android/services/telephony/TelephonyConnection;->performConference(Landroid/telecom/Connection;)V

    .line 937
    .end local p2    # "connection2":Landroid/telecom/Connection;
    :goto_0
    return-void

    .line 931
    .restart local p1    # "connection1":Landroid/telecom/Connection;
    .restart local p2    # "connection2":Landroid/telecom/Connection;
    :cond_0
    instance-of v0, p2, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_1

    .line 932
    check-cast p2, Lcom/android/services/telephony/TelephonyConnection;

    .end local p2    # "connection2":Landroid/telecom/Connection;
    invoke-virtual {p2, p1}, Lcom/android/services/telephony/TelephonyConnection;->performConference(Landroid/telecom/Connection;)V

    goto :goto_0

    .line 934
    .restart local p2    # "connection2":Landroid/telecom/Connection;
    :cond_1
    const-string/jumbo v0, "onConference - cannot merge connections Connection1: %s, Connection2: %2"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 935
    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    .line 934
    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 276
    invoke-super {p0}, Landroid/telecom/ConnectionService;->onCreate()V

    .line 277
    invoke-static {p0}, Lcom/android/services/telephony/Log;->initLogging(Landroid/content/Context;)V

    .line 278
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mExpectedComponentName:Landroid/content/ComponentName;

    .line 279
    new-instance v0, Lcom/android/services/telephony/EmergencyTonePlayer;

    invoke-direct {v0, p0}, Lcom/android/services/telephony/EmergencyTonePlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyTonePlayer:Lcom/android/services/telephony/EmergencyTonePlayer;

    .line 280
    invoke-static {p0}, Lcom/android/services/telephony/TelecomAccountRegistry;->getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/services/telephony/TelecomAccountRegistry;->setTelephonyConnectionService(Lcom/android/services/telephony/TelephonyConnectionService;)V

    .line 281
    return-void
.end method

.method public onCreateConnectionComplete(Landroid/telecom/Connection;)V
    .locals 2
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 792
    instance-of v1, p1, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 793
    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    .line 794
    .local v0, "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    invoke-direct {p0, v0}, Lcom/android/services/telephony/TelephonyConnectionService;->maybeSendInternationalCallEvent(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 795
    invoke-direct {p0, v0}, Lcom/android/services/telephony/TelephonyConnectionService;->maybeSendPhoneAccountUpdateEvent(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 797
    .end local v0    # "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_0
    return-void
.end method

.method public onCreateIncomingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/Connection;
    .locals 12
    .param p1, "connectionManagerPhoneAccount"    # Landroid/telecom/PhoneAccountHandle;
    .param p2, "request"    # Landroid/telecom/ConnectionRequest;

    .prologue
    const/4 v3, 0x0

    .line 722
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onCreateIncomingConnection, request: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 725
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v8

    .line 726
    .local v8, "accountHandle":Landroid/telecom/PhoneAccountHandle;
    const/4 v11, 0x0

    .line 727
    .local v11, "isEmergency":Z
    if-eqz v8, :cond_0

    const-string/jumbo v0, "E"

    .line 728
    invoke-virtual {v8}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v4

    .line 727
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 729
    const-string/jumbo v0, "Emergency PhoneAccountHandle is being used for incoming call... Treat as an Emergency Call."

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 731
    const/4 v11, 0x1

    .line 733
    :cond_0
    invoke-direct {p0, v8, v11}, Lcom/android/services/telephony/TelephonyConnectionService;->getPhoneForAccount(Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 734
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v1, :cond_1

    .line 738
    const-string/jumbo v0, "Phone is null"

    .line 737
    const/16 v4, 0x24

    .line 736
    invoke-static {v4, v0, v3}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v0

    .line 735
    invoke-static {v0}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v0

    return-object v0

    .line 742
    :cond_1
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v9

    .line 743
    .local v9, "call":Lcom/android/internal/telephony/Call;
    invoke-virtual {v9}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Call$State;->isRinging()Z

    move-result v0

    if-nez v0, :cond_2

    .line 744
    const-string/jumbo v0, "onCreateIncomingConnection, no ringing call"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v3}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 748
    const-string/jumbo v0, "Found no ringing call"

    .line 749
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v3

    .line 747
    const/4 v4, 0x1

    .line 746
    invoke-static {v4, v0, v3}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v0

    .line 745
    invoke-static {v0}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v0

    return-object v0

    .line 753
    :cond_2
    invoke-virtual {v9}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v0

    sget-object v4, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    if-ne v0, v4, :cond_3

    .line 754
    invoke-virtual {v9}, Lcom/android/internal/telephony/Call;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    .line 755
    .local v2, "originalConnection":Lcom/android/internal/telephony/Connection;
    :goto_0
    invoke-direct {p0, v2}, Lcom/android/services/telephony/TelephonyConnectionService;->isOriginalConnectionKnown(Lcom/android/internal/telephony/Connection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 756
    const-string/jumbo v0, "onCreateIncomingConnection, original connection already registered"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v3}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 757
    invoke-static {}, Landroid/telecom/Connection;->createCanceledConnection()Landroid/telecom/Connection;

    move-result-object v0

    return-object v0

    .line 754
    .end local v2    # "originalConnection":Lcom/android/internal/telephony/Connection;
    :cond_3
    invoke-virtual {v9}, Lcom/android/internal/telephony/Call;->getEarliestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    .restart local v2    # "originalConnection":Lcom/android/internal/telephony/Connection;
    goto :goto_0

    .line 762
    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->getVideoState()I

    move-result v7

    .line 767
    .local v7, "videoState":I
    :goto_1
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v4

    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getTelecomCallId()Ljava/lang/String;

    move-result-object v5

    .line 768
    invoke-virtual {p2}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v6

    move-object v0, p0

    .line 766
    invoke-direct/range {v0 .. v7}, Lcom/android/services/telephony/TelephonyConnectionService;->createConnectionFor(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/Connection;ZLandroid/telecom/PhoneAccountHandle;Ljava/lang/String;Landroid/net/Uri;I)Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v10

    .line 769
    .local v10, "connection":Landroid/telecom/Connection;
    if-nez v10, :cond_6

    .line 770
    invoke-static {}, Landroid/telecom/Connection;->createCanceledConnection()Landroid/telecom/Connection;

    move-result-object v0

    return-object v0

    .line 763
    .end local v7    # "videoState":I
    .end local v10    # "connection":Landroid/telecom/Connection;
    :cond_5
    const/4 v7, 0x0

    .restart local v7    # "videoState":I
    goto :goto_1

    .restart local v10    # "connection":Landroid/telecom/Connection;
    :cond_6
    move-object v0, v10

    .line 773
    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->isMultiparty()Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, v10

    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->isImsConnection()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 774
    invoke-static {v10}, Lcom/android/services/telephony/SimpleFeatures;->accessMiuiConnectionExtra(Landroid/telecom/Connection;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v0

    const-string/jumbo v4, "telephony.IMS_CONFERENCE_TYPE"

    .line 775
    const-string/jumbo v5, "incoming"

    .line 774
    invoke-virtual {v0, v4, v5}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->putString(Ljava/lang/String;Ljava/lang/String;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->commit()V

    .line 776
    const-string/jumbo v0, "onCreateIncomingConnection: ims conference"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v3}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 779
    :cond_7
    return-object v10
.end method

.method public onCreateOutgoingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/Connection;
    .locals 30
    .param p1, "connectionManagerPhoneAccount"    # Landroid/telecom/PhoneAccountHandle;
    .param p2, "request"    # Landroid/telecom/ConnectionRequest;

    .prologue
    .line 296
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onCreateOutgoingConnection, request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v20

    .line 299
    .local v20, "bundle":Landroid/os/Bundle;
    if-eqz v20, :cond_1

    .line 300
    const-string/jumbo v3, "org.codeaurora.extra.SKIP_SCHEMA_PARSING"

    const/4 v4, 0x0

    .line 299
    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 301
    const-string/jumbo v3, "org.codeaurora.extra.DIAL_CONFERENCE_URI"

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v26

    .line 302
    :goto_0
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v18

    .line 303
    .local v18, "handle":Landroid/net/Uri;
    if-nez v26, :cond_2

    if-nez v18, :cond_2

    .line 304
    const-string/jumbo v3, "onCreateOutgoingConnection, handle is null"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    const-string/jumbo v3, "No phone number supplied"

    .line 307
    const/16 v4, 0x26

    .line 309
    const/4 v8, 0x0

    .line 306
    invoke-static {v4, v3, v8}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 305
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 299
    .end local v18    # "handle":Landroid/net/Uri;
    :cond_0
    const/16 v26, 0x1

    .local v26, "isSkipSchemaOrConfUri":Z
    goto :goto_0

    .end local v26    # "isSkipSchemaOrConfUri":Z
    :cond_1
    const/16 v26, 0x0

    .restart local v26    # "isSkipSchemaOrConfUri":Z
    goto :goto_0

    .line 312
    .end local v26    # "isSkipSchemaOrConfUri":Z
    .restart local v18    # "handle":Landroid/net/Uri;
    :cond_2
    if-nez v18, :cond_3

    sget-object v18, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 313
    :cond_3
    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v29

    .line 315
    .local v29, "scheme":Ljava/lang/String;
    const-string/jumbo v3, "voicemail"

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 318
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/android/services/telephony/TelephonyConnectionService;->getPhoneForAccount(Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;

    move-result-object v19

    .line 319
    .local v19, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v19, :cond_4

    .line 320
    const-string/jumbo v3, "onCreateOutgoingConnection, phone is null"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 324
    const-string/jumbo v3, "Phone is null"

    .line 323
    const/16 v4, 0x12

    .line 325
    const/4 v8, 0x0

    .line 322
    invoke-static {v4, v3, v8}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 321
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 327
    :cond_4
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getVoiceMailNumber()Ljava/lang/String;

    move-result-object v27

    .line 328
    .local v27, "number":Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 329
    const-string/jumbo v3, "onCreateOutgoingConnection, no voicemail number set."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 333
    const-string/jumbo v3, "Voicemail scheme provided but no voicemail number set."

    .line 334
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 332
    const/16 v8, 0x28

    .line 331
    invoke-static {v8, v3, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 330
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 338
    :cond_5
    const-string/jumbo v3, "tel"

    const/4 v4, 0x0

    move-object/from16 v0, v27

    invoke-static {v3, v0, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 387
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lmiui/telephony/TelephonyManagerEx;->isLocalEmergencyNumber(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 388
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/android/services/telephony/TelephonyConnectionService;->getPhoneForAccount(Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;

    move-result-object v19

    .line 393
    if-eqz v19, :cond_7

    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I

    move-result v3

    if-eqz v3, :cond_8

    .line 395
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->convertToEmergencyNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 396
    .local v22, "convertedNumber":Ljava/lang/String;
    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 397
    const-string/jumbo v3, "onCreateOutgoingConnection, converted to emergency number"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 398
    move-object/from16 v27, v22

    .line 399
    const-string/jumbo v3, "tel"

    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-static {v3, v0, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 403
    .end local v22    # "convertedNumber":Ljava/lang/String;
    :cond_8
    move-object/from16 v5, v27

    .line 408
    .local v5, "numberToDial":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lmiui/telephony/TelephonyManagerEx;->isLocalEmergencyNumber(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    .line 409
    .local v6, "isEmergencyNumber":Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 410
    const-string/jumbo v4, "airplane_mode_on"

    const/4 v8, 0x0

    .line 409
    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_10

    const/16 v25, 0x1

    .line 412
    .local v25, "isAirplaneModeOn":Z
    :goto_1
    if-eqz v6, :cond_11

    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelephonyConnectionService;->isRadioOn()Z

    move-result v3

    if-eqz v3, :cond_9

    if-eqz v25, :cond_11

    .line 413
    :cond_9
    move-object/from16 v7, v18

    .line 416
    .local v7, "emergencyHandle":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v3}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v13

    .line 418
    .local v13, "defaultPhoneType":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    invoke-interface {v3}, Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v8

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    .line 417
    invoke-direct/range {v3 .. v8}, Lcom/android/services/telephony/TelephonyConnectionService;->getTelephonyConnection(Landroid/telecom/ConnectionRequest;Ljava/lang/String;ZLandroid/net/Uri;Lcom/android/internal/telephony/Phone;)Landroid/telecom/Connection;

    move-result-object v10

    .line 419
    .local v10, "emergencyConnection":Landroid/telecom/Connection;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyCallHelper:Lcom/android/services/telephony/EmergencyCallHelper;

    if-nez v3, :cond_a

    .line 420
    new-instance v3, Lcom/android/services/telephony/EmergencyCallHelper;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/services/telephony/EmergencyCallHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyCallHelper:Lcom/android/services/telephony/EmergencyCallHelper;

    .line 424
    :cond_a
    if-eqz v25, :cond_b

    .line 425
    invoke-static {v10}, Lcom/android/services/telephony/SimpleFeatures;->accessMiuiConnectionExtra(Landroid/telecom/Connection;)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v3

    .line 426
    const-string/jumbo v4, "telephony.IS_ECC_TURNON_RADIO"

    const/4 v8, 0x1

    .line 425
    invoke-virtual {v3, v4, v8}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->putBoolean(Ljava/lang/String;Z)Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/common/PhoneConstants$MiuiCallExtraAccessor;->commit()V

    .line 430
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/services/telephony/TelephonyConnectionService;->mEmergencyCallHelper:Lcom/android/services/telephony/EmergencyCallHelper;

    new-instance v8, Lcom/android/services/telephony/TelephonyConnectionService$6;

    move-object/from16 v9, p0

    move-object/from16 v11, p2

    move v12, v6

    move-object v14, v5

    move-object v15, v7

    invoke-direct/range {v8 .. v15}, Lcom/android/services/telephony/TelephonyConnectionService$6;-><init>(Lcom/android/services/telephony/TelephonyConnectionService;Landroid/telecom/Connection;Landroid/telecom/ConnectionRequest;ZILjava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v3, v8}, Lcom/android/services/telephony/EmergencyCallHelper;->enableEmergencyCalling(Lcom/android/services/telephony/EmergencyCallStateListener$Callback;)V

    .line 499
    return-object v10

    .line 340
    .end local v5    # "numberToDial":Ljava/lang/String;
    .end local v6    # "isEmergencyNumber":Z
    .end local v7    # "emergencyHandle":Landroid/net/Uri;
    .end local v10    # "emergencyConnection":Landroid/telecom/Connection;
    .end local v13    # "defaultPhoneType":I
    .end local v19    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v25    # "isAirplaneModeOn":Z
    .end local v27    # "number":Ljava/lang/String;
    :cond_c
    if-nez v26, :cond_d

    const-string/jumbo v3, "tel"

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_d

    .line 341
    const-string/jumbo v3, "onCreateOutgoingConnection, Handle %s is not type tel"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v29, v4, v8

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 345
    const-string/jumbo v3, "Handle scheme is not type tel"

    .line 344
    const/4 v4, 0x7

    .line 346
    const/4 v8, 0x0

    .line 343
    invoke-static {v4, v3, v8}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 342
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 349
    :cond_d
    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v27

    .line 350
    .restart local v27    # "number":Ljava/lang/String;
    if-nez v26, :cond_e

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 351
    const-string/jumbo v3, "onCreateOutgoingConnection, unable to parse number"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 355
    const-string/jumbo v3, "Unable to parse number"

    .line 354
    const/4 v4, 0x7

    .line 356
    const/4 v8, 0x0

    .line 353
    invoke-static {v4, v3, v8}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 352
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 359
    :cond_e
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/android/services/telephony/TelephonyConnectionService;->getPhoneForAccount(Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;

    move-result-object v19

    .line 360
    .restart local v19    # "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v19, :cond_6

    sget-object v3, Lcom/android/services/telephony/TelephonyConnectionService;->CDMA_ACTIVATION_CODE_REGEX_PATTERN:Ljava/util/regex/Pattern;

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 364
    const/16 v23, 0x0

    .line 366
    .local v23, "disableActivation":Z
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "carrier_config"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    .line 365
    check-cast v21, Landroid/telephony/CarrierConfigManager;

    .line 367
    .local v21, "cfgManager":Landroid/telephony/CarrierConfigManager;
    if-eqz v21, :cond_f

    .line 368
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/telephony/CarrierConfigManager;->getConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v3

    .line 369
    const-string/jumbo v4, "disable_cdma_activation_code_bool"

    .line 368
    invoke-virtual {v3, v4}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v23

    .line 372
    .end local v23    # "disableActivation":Z
    :cond_f
    if-eqz v23, :cond_6

    .line 377
    const-string/jumbo v3, "Tried to dial *228"

    .line 378
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v4

    .line 375
    const/16 v8, 0x31

    .line 374
    invoke-static {v8, v3, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v3

    .line 373
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 409
    .end local v21    # "cfgManager":Landroid/telephony/CarrierConfigManager;
    .restart local v5    # "numberToDial":Ljava/lang/String;
    .restart local v6    # "isEmergencyNumber":Z
    :cond_10
    const/16 v25, 0x0

    .restart local v25    # "isAirplaneModeOn":Z
    goto/16 :goto_1

    .line 501
    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/TelephonyConnectionService;->canAddCall()Z

    move-result v3

    if-nez v3, :cond_12

    xor-int/lit8 v3, v6, 0x1

    if-eqz v3, :cond_12

    .line 502
    const-string/jumbo v3, "onCreateOutgoingConnection, cannot add call ."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 504
    new-instance v3, Landroid/telecom/DisconnectCause;

    .line 505
    invoke-virtual/range {p0 .. p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 506
    const v8, 0x7f0b049f

    .line 505
    invoke-virtual {v4, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 507
    invoke-virtual/range {p0 .. p0}, Lcom/android/services/telephony/TelephonyConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 508
    const v9, 0x7f0b049f

    .line 507
    invoke-virtual {v8, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    .line 509
    const-string/jumbo v9, "Add call restricted due to ongoing video call"

    .line 504
    const/4 v11, 0x1

    invoke-direct {v3, v11, v4, v8, v9}, Landroid/telecom/DisconnectCause;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 503
    invoke-static {v3}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v3

    return-object v3

    .line 516
    :cond_12
    move-object/from16 v0, p2

    invoke-static {v0, v6}, Lcom/android/phone/MiuiPhoneUtils;->getPhoneForEmergencyCall(Landroid/telecom/ConnectionRequest;Z)Lcom/android/internal/telephony/Phone;

    move-result-object v24

    .line 517
    .local v24, "ePhone":Lcom/android/internal/telephony/Phone;
    if-eqz v24, :cond_14

    move-object/from16 v19, v24

    :goto_2
    move-object/from16 v14, p0

    move-object/from16 v15, p2

    move-object/from16 v16, v5

    move/from16 v17, v6

    .line 518
    invoke-direct/range {v14 .. v19}, Lcom/android/services/telephony/TelephonyConnectionService;->getTelephonyConnection(Landroid/telecom/ConnectionRequest;Ljava/lang/String;ZLandroid/net/Uri;Lcom/android/internal/telephony/Phone;)Landroid/telecom/Connection;

    move-result-object v28

    .line 522
    .local v28, "resultConnection":Landroid/telecom/Connection;
    move-object/from16 v0, v28

    instance-of v3, v0, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v3, :cond_13

    move-object/from16 v3, v28

    .line 523
    check-cast v3, Lcom/android/services/telephony/TelephonyConnection;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-direct {v0, v3, v1, v2}, Lcom/android/services/telephony/TelephonyConnectionService;->placeOutgoingConnection(Lcom/android/services/telephony/TelephonyConnection;Lcom/android/internal/telephony/Phone;Landroid/telecom/ConnectionRequest;)V

    .line 525
    :cond_13
    return-object v28

    .line 517
    .end local v28    # "resultConnection":Landroid/telecom/Connection;
    :cond_14
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6}, Lcom/android/services/telephony/TelephonyConnectionService;->getPhoneForAccount(Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;

    move-result-object v19

    goto :goto_2
.end method

.method public onCreateUnknownConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/ConnectionRequest;)Landroid/telecom/Connection;
    .locals 25
    .param p1, "connectionManagerPhoneAccount"    # Landroid/telecom/PhoneAccountHandle;
    .param p2, "request"    # Landroid/telecom/ConnectionRequest;

    .prologue
    .line 809
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onCreateUnknownConnection, request: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 812
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v10

    .line 813
    .local v10, "accountHandle":Landroid/telecom/PhoneAccountHandle;
    const/16 v21, 0x0

    .line 814
    .local v21, "isEmergency":Z
    if-eqz v10, :cond_0

    const-string/jumbo v2, "E"

    .line 815
    invoke-virtual {v10}, Landroid/telecom/PhoneAccountHandle;->getId()Ljava/lang/String;

    move-result-object v5

    .line 814
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 816
    const-string/jumbo v2, "Emergency PhoneAccountHandle is being used for unknown call... Treat as an Emergency Call."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 818
    const/16 v21, 0x1

    .line 820
    :cond_0
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v10, v1}, Lcom/android/services/telephony/TelephonyConnectionService;->getPhoneForAccount(Landroid/telecom/PhoneAccountHandle;Z)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    .line 821
    .local v3, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v3, :cond_1

    .line 825
    const-string/jumbo v2, "Phone is null"

    .line 824
    const/16 v5, 0x24

    .line 826
    const/4 v6, 0x0

    .line 823
    invoke-static {v5, v2, v6}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(ILjava/lang/String;I)Landroid/telecom/DisconnectCause;

    move-result-object v2

    .line 822
    invoke-static {v2}, Landroid/telecom/Connection;->createFailedConnection(Landroid/telecom/DisconnectCause;)Landroid/telecom/Connection;

    move-result-object v2

    return-object v2

    .line 828
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v17

    .line 830
    .local v17, "extras":Landroid/os/Bundle;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 834
    .local v11, "allConnections":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/Connection;>;"
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz v17, :cond_2

    .line 835
    const-string/jumbo v2, "android.telephony.ImsExternalCallTracker.extra.EXTERNAL_CALL_ID"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    .line 834
    if-eqz v2, :cond_2

    .line 837
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v20

    check-cast v20, Lcom/android/internal/telephony/imsphone/ImsPhone;

    .line 838
    .local v20, "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    invoke-virtual/range {v20 .. v20}, Lcom/android/internal/telephony/imsphone/ImsPhone;->getExternalCallTracker()Lcom/android/internal/telephony/imsphone/ImsExternalCallTracker;

    move-result-object v16

    .line 839
    .local v16, "externalCallTracker":Lcom/android/internal/telephony/imsphone/ImsExternalCallTracker;
    const-string/jumbo v2, "android.telephony.ImsExternalCallTracker.extra.EXTERNAL_CALL_ID"

    .line 840
    const/4 v5, -0x1

    .line 839
    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v15

    .line 842
    .local v15, "externalCallId":I
    if-eqz v16, :cond_2

    .line 844
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/android/internal/telephony/imsphone/ImsExternalCallTracker;->getConnectionById(I)Lcom/android/internal/telephony/Connection;

    move-result-object v13

    .line 846
    .local v13, "connection":Lcom/android/internal/telephony/Connection;
    if-eqz v13, :cond_2

    .line 847
    invoke-interface {v11, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 852
    .end local v13    # "connection":Lcom/android/internal/telephony/Connection;
    .end local v15    # "externalCallId":I
    .end local v16    # "externalCallTracker":Lcom/android/internal/telephony/imsphone/ImsExternalCallTracker;
    .end local v20    # "imsPhone":Lcom/android/internal/telephony/imsphone/ImsPhone;
    :cond_2
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 853
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v22

    .line 854
    .local v22, "ringingCall":Lcom/android/internal/telephony/Call;
    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/Call;->hasConnections()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 855
    invoke-virtual/range {v22 .. v22}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 857
    :cond_3
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v18

    .line 858
    .local v18, "foregroundCall":Lcom/android/internal/telephony/Call;
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    sget-object v5, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    if-eq v2, v5, :cond_4

    .line 859
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/Call;->hasConnections()Z

    move-result v2

    .line 858
    if-eqz v2, :cond_4

    .line 860
    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 862
    :cond_4
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 863
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getImsPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getForegroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v19

    .line 864
    .local v19, "imsFgCall":Lcom/android/internal/telephony/Call;
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    sget-object v5, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    if-eq v2, v5, :cond_5

    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Call;->hasConnections()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 866
    invoke-virtual/range {v19 .. v19}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 869
    .end local v19    # "imsFgCall":Lcom/android/internal/telephony/Call;
    :cond_5
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v12

    .line 870
    .local v12, "backgroundCall":Lcom/android/internal/telephony/Call;
    invoke-virtual {v12}, Lcom/android/internal/telephony/Call;->hasConnections()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 871
    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getBackgroundCall()Lcom/android/internal/telephony/Call;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->getConnections()Ljava/util/List;

    move-result-object v2

    invoke-interface {v11, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 875
    .end local v12    # "backgroundCall":Lcom/android/internal/telephony/Call;
    .end local v18    # "foregroundCall":Lcom/android/internal/telephony/Call;
    .end local v22    # "ringingCall":Lcom/android/internal/telephony/Call;
    :cond_6
    const/4 v4, 0x0

    .line 876
    .local v4, "unknownConnection":Lcom/android/internal/telephony/Connection;
    invoke-interface {v11}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v24

    .local v24, "telephonyConnection$iterator":Ljava/util/Iterator;
    :cond_7
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/android/internal/telephony/Connection;

    .line 877
    .local v23, "telephonyConnection":Lcom/android/internal/telephony/Connection;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/android/services/telephony/TelephonyConnectionService;->isOriginalConnectionKnown(Lcom/android/internal/telephony/Connection;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 878
    move-object/from16 v4, v23

    .line 879
    .local v4, "unknownConnection":Lcom/android/internal/telephony/Connection;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onCreateUnknownConnection: conn = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 884
    .end local v4    # "unknownConnection":Lcom/android/internal/telephony/Connection;
    .end local v23    # "telephonyConnection":Lcom/android/internal/telephony/Connection;
    :cond_8
    if-nez v4, :cond_9

    .line 885
    const-string/jumbo v2, "onCreateUnknownConnection, did not find previously unknown connection."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 886
    invoke-static {}, Landroid/telecom/Connection;->createCanceledConnection()Landroid/telecom/Connection;

    move-result-object v2

    return-object v2

    .line 891
    :cond_9
    if-eqz v4, :cond_a

    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->getVideoState()I

    move-result v9

    .line 896
    .local v9, "videoState":I
    :goto_0
    invoke-virtual {v4}, Lcom/android/internal/telephony/Connection;->isIncoming()Z

    move-result v2

    xor-int/lit8 v5, v2, 0x1

    .line 897
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getAccountHandle()Landroid/telecom/PhoneAccountHandle;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getTelecomCallId()Ljava/lang/String;

    move-result-object v7

    .line 898
    invoke-virtual/range {p2 .. p2}, Landroid/telecom/ConnectionRequest;->getAddress()Landroid/net/Uri;

    move-result-object v8

    move-object/from16 v2, p0

    .line 895
    invoke-direct/range {v2 .. v9}, Lcom/android/services/telephony/TelephonyConnectionService;->createConnectionFor(Lcom/android/internal/telephony/Phone;Lcom/android/internal/telephony/Connection;ZLandroid/telecom/PhoneAccountHandle;Ljava/lang/String;Landroid/net/Uri;I)Lcom/android/services/telephony/TelephonyConnection;

    move-result-object v14

    .line 900
    .local v14, "connection":Lcom/android/services/telephony/TelephonyConnection;
    if-nez v14, :cond_b

    .line 901
    invoke-static {}, Landroid/telecom/Connection;->createCanceledConnection()Landroid/telecom/Connection;

    move-result-object v2

    return-object v2

    .line 892
    .end local v9    # "videoState":I
    .end local v14    # "connection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_a
    const/4 v9, 0x0

    .restart local v9    # "videoState":I
    goto :goto_0

    .line 903
    .restart local v14    # "connection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_b
    invoke-virtual {v14}, Lcom/android/services/telephony/TelephonyConnection;->updateState()V

    .line 904
    return-object v14
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 288
    invoke-super {p0}, Landroid/telecom/ConnectionService;->onDestroy()V

    .line 289
    invoke-static {p0}, Lcom/android/services/telephony/TelecomAccountRegistry;->getInstance(Landroid/content/Context;)Lcom/android/services/telephony/TelecomAccountRegistry;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/TelecomAccountRegistry;->setTelephonyConnectionService(Lcom/android/services/telephony/TelephonyConnectionService;)V

    .line 290
    return-void
.end method

.method public removeConnection(Landroid/telecom/Connection;)V
    .locals 2
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 1375
    invoke-super {p0, p1}, Landroid/telecom/ConnectionService;->removeConnection(Landroid/telecom/Connection;)V

    .line 1376
    instance-of v1, p1, Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 1377
    check-cast v0, Lcom/android/services/telephony/TelephonyConnection;

    .line 1378
    .local v0, "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    iget-object v1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConnectionListener:Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/TelephonyConnection;->removeTelephonyConnectionListener(Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;)Lcom/android/services/telephony/TelephonyConnection;

    move-object v1, p1

    .line 1379
    check-cast v1, Lcom/android/services/telephony/TelephonyConnection;

    invoke-direct {p0, v1}, Lcom/android/services/telephony/TelephonyConnectionService;->removeConnectionRemovedListener(Lcom/android/services/telephony/TelephonyConnectionService$ConnectionRemovedListener;)V

    .line 1380
    check-cast p1, Lcom/android/services/telephony/TelephonyConnection;

    .end local p1    # "connection":Landroid/telecom/Connection;
    invoke-direct {p0, p1}, Lcom/android/services/telephony/TelephonyConnectionService;->fireOnConnectionRemoved(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 1382
    .end local v0    # "telephonyConnection":Lcom/android/services/telephony/TelephonyConnection;
    :cond_0
    return-void
.end method

.method public setPhoneFactoryProxy(Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;)V
    .locals 0
    .param p1, "proxy"    # Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mPhoneFactoryProxy:Lcom/android/services/telephony/TelephonyConnectionService$PhoneFactoryProxy;

    .line 240
    return-void
.end method

.method public setSubscriptionManagerProxy(Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;)V
    .locals 0
    .param p1, "proxy"    # Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mSubscriptionManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$SubscriptionManagerProxy;

    .line 230
    return-void
.end method

.method public setTelephonyManagerProxy(Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;)V
    .locals 0
    .param p1, "proxy"    # Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyManagerProxy:Lcom/android/services/telephony/TelephonyConnectionService$TelephonyManagerProxy;

    .line 235
    return-void
.end method

.method public triggerConferenceRecalculate()V
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConferenceController:Lcom/android/services/telephony/TelephonyConferenceController;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConferenceController;->shouldRecalculate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 802
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService;->mTelephonyConferenceController:Lcom/android/services/telephony/TelephonyConferenceController;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConferenceController;->recalculate()V

    .line 804
    :cond_0
    return-void
.end method
