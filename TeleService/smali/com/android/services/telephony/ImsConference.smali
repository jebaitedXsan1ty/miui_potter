.class public Lcom/android/services/telephony/ImsConference;
.super Landroid/telecom/Conference;
.source "ImsConference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/ImsConference$1;,
        Lcom/android/services/telephony/ImsConference$2;,
        Lcom/android/services/telephony/ImsConference$3;,
        Lcom/android/services/telephony/ImsConference$4;
    }
.end annotation


# instance fields
.field private mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

.field private mConferenceHostAddress:[Landroid/net/Uri;

.field private final mConferenceHostListener:Landroid/telecom/Connection$Listener;

.field private mConferenceHostPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

.field private final mConferenceParticipantConnections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/android/services/telephony/ConferenceParticipantConnection;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private final mParticipantListener:Landroid/telecom/Connection$Listener;

.field private mPendingParticipantsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTelecomAccountRegistry:Lcom/android/services/telephony/TelecomAccountRegistry;

.field private final mTelephonyConnectionListener:Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

.field private mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

.field private final mUpdateSyncRoot:Ljava/lang/Object;


# direct methods
.method static synthetic -get0(Lcom/android/services/telephony/ImsConference;)Lcom/android/services/telephony/TelephonyConnection;
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;

    .prologue
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/services/telephony/ImsConference;IIZ)I
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;
    .param p1, "conferenceCapabilities"    # I
    .param p2, "capabilities"    # I
    .param p3, "isVideoConferencingSupported"    # Z

    .prologue
    invoke-direct {p0, p1, p2, p3}, Lcom/android/services/telephony/ImsConference;->applyHostCapabilities(IIZ)I

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/ImsConference;II)I
    .locals 1
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;
    .param p1, "conferenceProperties"    # I
    .param p2, "properties"    # I

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/services/telephony/ImsConference;->applyHostProperties(II)I

    move-result v0

    return v0
.end method

.method static synthetic -wrap2(Lcom/android/services/telephony/ImsConference;Lcom/android/services/telephony/TelephonyConnection;Ljava/util/List;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;
    .param p1, "parent"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "participants"    # Ljava/util/List;

    .prologue
    invoke-direct {p0, p1, p2}, Lcom/android/services/telephony/ImsConference;->handleConferenceParticipantsUpdate(Lcom/android/services/telephony/TelephonyConnection;Ljava/util/List;)V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/services/telephony/ImsConference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->handleOriginalConnectionChange()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/services/telephony/ImsConference;Z)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;
    .param p1, "success"    # Z

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/ImsConference;->processAddParticipantResponse(Z)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/services/telephony/ImsConference;Ljava/lang/String;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;
    .param p1, "dialString"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/ImsConference;->processAddParticipantsList(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/services/telephony/ImsConference;Lcom/android/services/telephony/ConferenceParticipantConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;
    .param p1, "participant"    # Lcom/android/services/telephony/ConferenceParticipantConnection;

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/ImsConference;->removeConferenceParticipant(Lcom/android/services/telephony/ConferenceParticipantConnection;)V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/services/telephony/ImsConference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->updateManageConference()V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/services/telephony/ImsConference;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/ImsConference;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->updateStatusHints()V

    return-void
.end method

.method public constructor <init>(Lcom/android/services/telephony/TelecomAccountRegistry;Lcom/android/services/telephony/TelephonyConnectionServiceProxy;Lcom/android/services/telephony/TelephonyConnection;Landroid/telecom/PhoneAccountHandle;)V
    .locals 7
    .param p1, "telecomAccountRegistry"    # Lcom/android/services/telephony/TelecomAccountRegistry;
    .param p2, "telephonyConnectionService"    # Lcom/android/services/telephony/TelephonyConnectionServiceProxy;
    .param p3, "conferenceHost"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p4, "phoneAccountHandle"    # Landroid/telecom/PhoneAccountHandle;

    .prologue
    .line 345
    invoke-direct {p0, p4}, Landroid/telecom/Conference;-><init>(Landroid/telecom/PhoneAccountHandle;)V

    .line 81
    new-instance v1, Lcom/android/services/telephony/ImsConference$1;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/ImsConference$1;-><init>(Lcom/android/services/telephony/ImsConference;)V

    iput-object v1, p0, Lcom/android/services/telephony/ImsConference;->mParticipantListener:Landroid/telecom/Connection$Listener;

    .line 102
    new-instance v1, Lcom/android/services/telephony/ImsConference$2;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/ImsConference$2;-><init>(Lcom/android/services/telephony/ImsConference;)V

    .line 101
    iput-object v1, p0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionListener:Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    .line 116
    new-instance v1, Lcom/android/services/telephony/ImsConference$3;

    invoke-direct {v1, p0}, Lcom/android/services/telephony/ImsConference$3;-><init>(Lcom/android/services/telephony/ImsConference;)V

    .line 115
    iput-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHostListener:Landroid/telecom/Connection$Listener;

    .line 248
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    .line 257
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/services/telephony/ImsConference;->mUpdateSyncRoot:Ljava/lang/Object;

    .line 263
    new-instance v1, Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    .line 265
    new-instance v1, Lcom/android/services/telephony/ImsConference$4;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v1, p0, v6}, Lcom/android/services/telephony/ImsConference$4;-><init>(Lcom/android/services/telephony/ImsConference;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/services/telephony/ImsConference;->mHandler:Landroid/os/Handler;

    .line 347
    iput-object p1, p0, Lcom/android/services/telephony/ImsConference;->mTelecomAccountRegistry:Lcom/android/services/telephony/TelecomAccountRegistry;

    .line 351
    invoke-virtual {p3}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getConnectTime()J

    move-result-wide v4

    .line 352
    .local v4, "connectTime":J
    invoke-virtual {p3}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getConnectTimeReal()J

    move-result-wide v2

    .line 353
    .local v2, "connectElapsedTime":J
    invoke-virtual {p0, v4, v5}, Lcom/android/services/telephony/ImsConference;->setConnectionTime(J)V

    .line 354
    invoke-virtual {p0, v2, v3}, Lcom/android/services/telephony/ImsConference;->setConnectionElapsedTime(J)V

    .line 356
    invoke-virtual {p3, v4, v5}, Lcom/android/services/telephony/TelephonyConnection;->setConnectTimeMillis(J)V

    .line 357
    invoke-virtual {p3, v2, v3}, Lcom/android/services/telephony/TelephonyConnection;->setConnectElapsedTimeMillis(J)V

    .line 359
    iput-object p2, p0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    .line 360
    invoke-direct {p0, p3}, Lcom/android/services/telephony/ImsConference;->setConferenceHost(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 362
    const v0, 0x2200040

    .line 365
    .local v0, "capabilities":I
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->canHoldImsCalls()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366
    const v0, 0x2200043

    .line 369
    :cond_0
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionCapabilities()I

    move-result v1

    .line 370
    iget-object v6, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v6}, Lcom/android/services/telephony/TelephonyConnection;->isCarrierVideoConferencingSupported()Z

    move-result v6

    .line 368
    invoke-direct {p0, v0, v1, v6}, Lcom/android/services/telephony/ImsConference;->applyHostCapabilities(IIZ)I

    move-result v0

    .line 371
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/ImsConference;->setConnectionCapabilities(I)V

    .line 373
    return-void
.end method

.method private addParticipantInternal(Ljava/lang/String;)Z
    .locals 5
    .param p1, "participant"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 550
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v2}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 551
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onAddParticipant mConferenceHost = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 552
    const-string/jumbo v3, " Phone = "

    .line 551
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 553
    if-eqz v1, :cond_1

    .line 555
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 554
    invoke-virtual {v1, p1, v2}, Lcom/android/internal/telephony/Phone;->addParticipant(Ljava/lang/String;Landroid/os/Message;)V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 556
    const/4 v2, 0x1

    return v2

    .line 550
    :cond_0
    const/4 v1, 0x0

    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    goto :goto_0

    .line 558
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v2, "Exception thrown trying to add a participant into conference"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v2, v3}, Landroid/telecom/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 561
    .end local v0    # "e":Lcom/android/internal/telephony/CallStateException;
    :cond_1
    return v4
.end method

.method private applyHostCapabilities(IIZ)I
    .locals 6
    .param p1, "conferenceCapabilities"    # I
    .param p2, "capabilities"    # I
    .param p3, "isVideoConferencingSupported"    # Z

    .prologue
    const/high16 v5, 0x800000

    const/16 v2, 0x300

    const/high16 v4, 0x80000

    const/16 v3, 0xc00

    const/4 v0, 0x0

    .line 388
    invoke-static {p2, v2}, Lcom/android/services/telephony/ImsConference;->can(II)Z

    move-result v1

    .line 386
    invoke-direct {p0, p1, v2, v1}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    .line 390
    if-eqz p3, :cond_1

    .line 393
    invoke-static {p2, v3}, Lcom/android/services/telephony/ImsConference;->can(II)Z

    move-result v1

    .line 391
    invoke-direct {p0, p1, v3, v1}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    .line 396
    invoke-static {p2, v4}, Lcom/android/services/telephony/ImsConference;->can(II)Z

    move-result v1

    .line 394
    invoke-direct {p0, p1, v4, v1}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    .line 409
    :goto_0
    invoke-static {p2, v5}, Lcom/android/services/telephony/ImsConference;->can(II)Z

    move-result v1

    .line 407
    invoke-direct {p0, p1, v5, v1}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    .line 413
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getVideoPauseSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->isVideoCapable()Z

    move-result v0

    .line 412
    :cond_0
    const/high16 v1, 0x100000

    .line 411
    invoke-direct {p0, p1, v1, v0}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    .line 415
    return p1

    .line 400
    :cond_1
    const-string/jumbo v1, "applyHostCapabilities : video conferencing not supported"

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 401
    invoke-direct {p0, p1, v3, v0}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    .line 403
    invoke-direct {p0, p1, v4, v0}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    goto :goto_0
.end method

.method private applyHostProperties(II)I
    .locals 4
    .param p1, "conferenceProperties"    # I
    .param p2, "properties"    # I

    .prologue
    const/16 v3, 0x10

    const/16 v2, 0x8

    const/4 v1, 0x4

    .line 428
    invoke-static {p2, v1}, Lcom/android/services/telephony/ImsConference;->can(II)Z

    move-result v0

    .line 426
    invoke-direct {p0, p1, v1, v0}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    .line 432
    invoke-static {p2, v2}, Lcom/android/services/telephony/ImsConference;->can(II)Z

    move-result v0

    .line 430
    invoke-direct {p0, p1, v2, v0}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    .line 436
    invoke-static {p2, v3}, Lcom/android/services/telephony/ImsConference;->can(II)Z

    move-result v0

    .line 434
    invoke-direct {p0, p1, v3, v0}, Lcom/android/services/telephony/ImsConference;->changeBitmask(IIZ)I

    move-result p1

    .line 438
    return p1
.end method

.method private canHoldImsCalls()Z
    .locals 2

    .prologue
    .line 1141
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 1143
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    const-string/jumbo v1, "allow_hold_in_ims_call"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private changeBitmask(IIZ)I
    .locals 1
    .param p1, "bitmask"    # I
    .param p2, "bitfield"    # I
    .param p3, "enabled"    # Z

    .prologue
    .line 630
    if-eqz p3, :cond_0

    .line 631
    or-int v0, p1, p2

    return v0

    .line 633
    :cond_0
    not-int v0, p2

    and-int/2addr v0, p1

    return v0
.end method

.method private createConferenceParticipantConnection(Lcom/android/services/telephony/TelephonyConnection;Landroid/telecom/ConferenceParticipant;)V
    .locals 6
    .param p1, "parent"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "participant"    # Landroid/telecom/ConferenceParticipant;

    .prologue
    .line 853
    new-instance v0, Lcom/android/services/telephony/ConferenceParticipantConnection;

    .line 854
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    .line 853
    invoke-direct {v0, v1, p2}, Lcom/android/services/telephony/ConferenceParticipantConnection;-><init>(Lcom/android/internal/telephony/Connection;Landroid/telecom/ConferenceParticipant;)V

    .line 855
    .local v0, "connection":Lcom/android/services/telephony/ConferenceParticipantConnection;
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mParticipantListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/ConferenceParticipantConnection;->addConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 856
    invoke-virtual {p1}, Lcom/android/services/telephony/TelephonyConnection;->getConnectTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setConnectTimeMillis(J)V

    .line 858
    const-string/jumbo v1, "createConferenceParticipantConnection: participant=%s, connection=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 859
    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    .line 858
    invoke-static {p0, v1, v2}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 861
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mUpdateSyncRoot:Ljava/lang/Object;

    monitor-enter v2

    .line 862
    :try_start_0
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    new-instance v3, Landroid/util/Pair;

    invoke-virtual {p2}, Landroid/telecom/ConferenceParticipant;->getHandle()Landroid/net/Uri;

    move-result-object v4

    .line 863
    invoke-virtual {p2}, Landroid/telecom/ConferenceParticipant;->getEndpoint()Landroid/net/Uri;

    move-result-object v5

    .line 862
    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    .line 866
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHostPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    invoke-interface {v1, v2, v0, p0}, Lcom/android/services/telephony/TelephonyConnectionServiceProxy;->addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;Landroid/telecom/Conference;)V

    .line 868
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/ImsConference;->addConnection(Landroid/telecom/Connection;)Z

    .line 869
    return-void

    .line 861
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private disconnectConferenceParticipants()V
    .locals 5

    .prologue
    .line 890
    const-string/jumbo v2, "disconnectConferenceParticipants"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 892
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mUpdateSyncRoot:Ljava/lang/Object;

    monitor-enter v3

    .line 894
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    .line 893
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "connection$iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/ConferenceParticipantConnection;

    .line 896
    .local v0, "connection":Lcom/android/services/telephony/ConferenceParticipantConnection;
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mParticipantListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {v0, v2}, Lcom/android/services/telephony/ConferenceParticipantConnection;->removeConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 899
    new-instance v2, Landroid/telecom/DisconnectCause;

    const/4 v4, 0x4

    invoke-direct {v2, v4}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 900
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    invoke-interface {v2, v0}, Lcom/android/services/telephony/TelephonyConnectionServiceProxy;->removeConnection(Landroid/telecom/Connection;)V

    .line 901
    invoke-virtual {v0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->destroy()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 892
    .end local v0    # "connection":Lcom/android/services/telephony/ConferenceParticipantConnection;
    .end local v1    # "connection$iterator":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 903
    .restart local v1    # "connection$iterator":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v3

    .line 905
    return-void
.end method

.method private getCarrierConfig()Landroid/os/PersistableBundle;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1147
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v1, :cond_0

    .line 1148
    return-object v2

    .line 1151
    :cond_0
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 1152
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    if-nez v0, :cond_1

    .line 1153
    return-object v2

    .line 1155
    :cond_1
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/phone/PhoneGlobals;->getCarrierConfigForSubId(I)Landroid/os/PersistableBundle;

    move-result-object v1

    return-object v1
.end method

.method private handleConferenceParticipantsUpdate(Lcom/android/services/telephony/TelephonyConnection;Ljava/util/List;)V
    .locals 25
    .param p1, "parent"    # Lcom/android/services/telephony/TelephonyConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/services/telephony/TelephonyConnection;",
            "Ljava/util/List",
            "<",
            "Landroid/telecom/ConferenceParticipant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 745
    .local p2, "participants":Ljava/util/List;, "Ljava/util/List<Landroid/telecom/ConferenceParticipant;>;"
    if-nez p2, :cond_0

    .line 746
    return-void

    .line 749
    :cond_0
    const-string/jumbo v20, "handleConferenceParticipantsUpdate: size=%d"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    const/16 v23, 0x0

    aput-object v22, v21, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/ImsConference;->mUpdateSyncRoot:Ljava/lang/Object;

    move-object/from16 v21, v0

    monitor-enter v21

    .line 756
    const/4 v11, 0x0

    .line 757
    .local v11, "newParticipantsAdded":Z
    const/4 v12, 0x0

    .line 758
    .local v12, "oldParticipantsRemoved":Z
    :try_start_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v20

    move/from16 v0, v20

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 759
    .local v10, "newParticipants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/telecom/ConferenceParticipant;>;"
    new-instance v16, Ljava/util/HashSet;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v20

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 762
    .local v16, "participantUserEntities":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/util/Pair<Landroid/net/Uri;Landroid/net/Uri;>;>;"
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "participant$iterator":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/telecom/ConferenceParticipant;

    .line 763
    .local v13, "participant":Landroid/telecom/ConferenceParticipant;
    new-instance v19, Landroid/util/Pair;

    invoke-virtual {v13}, Landroid/telecom/ConferenceParticipant;->getHandle()Landroid/net/Uri;

    move-result-object v20

    .line 764
    invoke-virtual {v13}, Landroid/telecom/ConferenceParticipant;->getEndpoint()Landroid/net/Uri;

    move-result-object v22

    .line 763
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 766
    .local v19, "userEntity":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/net/Uri;Landroid/net/Uri;>;"
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_3

    .line 771
    const/4 v5, 0x0

    .line 772
    .local v5, "disableFilter":Z
    invoke-virtual/range {p1 .. p1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v17

    .line 773
    .local v17, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v17, :cond_2

    .line 774
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 775
    .local v4, "context":Landroid/content/Context;
    invoke-virtual/range {v17 .. v17}, Lcom/android/internal/telephony/Phone;->getSubId()I

    move-result v18

    .line 776
    .local v18, "subId":I
    move/from16 v0, v18

    invoke-static {v4, v0}, Landroid/telephony/SubscriptionManager;->getResourcesForSubId(Landroid/content/Context;I)Landroid/content/res/Resources;

    move-result-object v20

    .line 777
    const v22, 0x7f0e002a

    .line 776
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    .line 782
    .end local v4    # "context":Landroid/content/Context;
    .end local v5    # "disableFilter":Z
    .end local v18    # "subId":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/ImsConference;->mConferenceHostAddress:[Landroid/net/Uri;

    move-object/from16 v20, v0

    invoke-virtual {v13}, Landroid/telecom/ConferenceParticipant;->getHandle()Landroid/net/Uri;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/android/services/telephony/ImsConference;->isParticipantHost([Landroid/net/Uri;Landroid/net/Uri;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 783
    invoke-virtual/range {p1 .. p1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v20

    if-eqz v20, :cond_1

    .line 785
    const-string/jumbo v20, "Create participant connection, participant = %s"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v13, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 786
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/android/services/telephony/ImsConference;->createConferenceParticipantConnection(Lcom/android/services/telephony/TelephonyConnection;Landroid/telecom/ConferenceParticipant;)V

    .line 787
    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 788
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 792
    .end local v17    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/services/telephony/ConferenceParticipantConnection;

    .line 793
    .local v3, "connection":Lcom/android/services/telephony/ConferenceParticipantConnection;
    const-string/jumbo v20, "handleConferenceParticipantsUpdate: updateState, participant = %s"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    .line 794
    const/16 v23, 0x0

    aput-object v13, v22, v23

    .line 793
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 795
    invoke-virtual {v13}, Landroid/telecom/ConferenceParticipant;->getState()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->updateState(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 755
    .end local v3    # "connection":Lcom/android/services/telephony/ConferenceParticipantConnection;
    .end local v10    # "newParticipants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/telecom/ConferenceParticipant;>;"
    .end local v13    # "participant":Landroid/telecom/ConferenceParticipant;
    .end local v15    # "participant$iterator":Ljava/util/Iterator;
    .end local v16    # "participantUserEntities":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/util/Pair<Landroid/net/Uri;Landroid/net/Uri;>;>;"
    .end local v19    # "userEntity":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/net/Uri;Landroid/net/Uri;>;"
    :catchall_0
    move-exception v20

    monitor-exit v21

    throw v20

    .line 800
    .restart local v10    # "newParticipants":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/telecom/ConferenceParticipant;>;"
    .restart local v15    # "participant$iterator":Ljava/util/Iterator;
    .restart local v16    # "participantUserEntities":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/util/Pair<Landroid/net/Uri;Landroid/net/Uri;>;>;"
    :cond_4
    if-eqz v11, :cond_5

    .line 802
    :try_start_1
    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "newParticipant$iterator":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telecom/ConferenceParticipant;

    .line 804
    .local v8, "newParticipant":Landroid/telecom/ConferenceParticipant;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    move-object/from16 v20, v0

    new-instance v22, Landroid/util/Pair;

    .line 805
    invoke-virtual {v8}, Landroid/telecom/ConferenceParticipant;->getHandle()Landroid/net/Uri;

    move-result-object v23

    .line 806
    invoke-virtual {v8}, Landroid/telecom/ConferenceParticipant;->getEndpoint()Landroid/net/Uri;

    move-result-object v24

    .line 804
    invoke-direct/range {v22 .. v24}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/services/telephony/ConferenceParticipantConnection;

    .line 807
    .restart local v3    # "connection":Lcom/android/services/telephony/ConferenceParticipantConnection;
    invoke-virtual {v8}, Landroid/telecom/ConferenceParticipant;->getState()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->updateState(I)V

    goto :goto_1

    .line 814
    .end local v3    # "connection":Lcom/android/services/telephony/ConferenceParticipantConnection;
    .end local v8    # "newParticipant":Landroid/telecom/ConferenceParticipant;
    .end local v9    # "newParticipant$iterator":Ljava/util/Iterator;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 815
    .local v7, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/util/Pair<Landroid/net/Uri;Landroid/net/Uri;>;Lcom/android/services/telephony/ConferenceParticipantConnection;>;>;"
    :cond_6
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_7

    .line 817
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 819
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/util/Pair<Landroid/net/Uri;Landroid/net/Uri;>;Lcom/android/services/telephony/ConferenceParticipantConnection;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_6

    .line 820
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/services/telephony/ConferenceParticipantConnection;

    .line 821
    .local v14, "participant":Lcom/android/services/telephony/ConferenceParticipantConnection;
    new-instance v20, Landroid/telecom/DisconnectCause;

    const/16 v22, 0x4

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 822
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/ImsConference;->mParticipantListener:Landroid/telecom/Connection$Listener;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->removeConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v14}, Lcom/android/services/telephony/TelephonyConnectionServiceProxy;->removeConnection(Landroid/telecom/Connection;)V

    .line 824
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/services/telephony/ImsConference;->removeConnection(Landroid/telecom/Connection;)V

    .line 825
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    .line 826
    const/4 v12, 0x1

    goto :goto_2

    .line 832
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/util/Pair<Landroid/net/Uri;Landroid/net/Uri;>;Lcom/android/services/telephony/ConferenceParticipantConnection;>;"
    .end local v14    # "participant":Lcom/android/services/telephony/ConferenceParticipantConnection;
    :cond_7
    if-nez v11, :cond_8

    if-eqz v12, :cond_9

    .line 833
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/android/services/telephony/ImsConference;->updateManageConference()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_9
    monitor-exit v21

    .line 836
    return-void
.end method

.method private handleOriginalConnectionChange()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 978
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v4, :cond_0

    .line 979
    const-string/jumbo v4, "handleOriginalConnectionChange; conference host missing."

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Landroid/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 980
    return-void

    .line 984
    :cond_0
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v4}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    .line 986
    .local v2, "originalConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v2, :cond_3

    .line 987
    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->getPhoneType()I

    move-result v4

    if-eq v4, v9, :cond_3

    .line 989
    const-string/jumbo v4, "handleOriginalConnectionChange : handover from IMS connection to new connection: %s"

    .line 988
    new-array v5, v8, [Ljava/lang/Object;

    .line 990
    aput-object v2, v5, v7

    .line 988
    invoke-static {p0, v4, v5}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 992
    const/4 v3, 0x0

    .line 993
    .local v3, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v4}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 994
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v4}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v4

    if-ne v4, v9, :cond_4

    .line 995
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v4}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 999
    .local v1, "imsPhone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-static {v4}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    .line 1008
    .end local v1    # "imsPhone":Lcom/android/internal/telephony/Phone;
    .end local v3    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v4}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v4

    if-ne v4, v8, :cond_2

    .line 1009
    const-string/jumbo v4, "handleOriginalConnectionChange : SRVCC to GSM"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {p0, v4, v5}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1010
    new-instance v0, Lcom/android/services/telephony/GsmConnection;

    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->getTelecomCallId()Ljava/lang/String;

    move-result-object v4

    .line 1011
    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v5}, Lcom/android/services/telephony/TelephonyConnection;->isOutgoingCall()Z

    move-result v5

    .line 1010
    invoke-direct {v0, v2, v4, v5}, Lcom/android/services/telephony/GsmConnection;-><init>(Lcom/android/internal/telephony/Connection;Ljava/lang/String;Z)V

    .line 1013
    .local v0, "c":Lcom/android/services/telephony/GsmConnection;
    invoke-virtual {v0, v8}, Lcom/android/services/telephony/GsmConnection;->setConferenceSupported(Z)V

    .line 1014
    invoke-virtual {v0}, Lcom/android/services/telephony/GsmConnection;->updateState()V

    .line 1016
    invoke-virtual {v2}, Lcom/android/internal/telephony/Connection;->getConnectTime()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/android/services/telephony/GsmConnection;->setConnectTimeMillis(J)V

    .line 1017
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    invoke-interface {v4, v3, v0}, Lcom/android/services/telephony/TelephonyConnectionServiceProxy;->addExistingConnection(Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Connection;)V

    .line 1018
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    invoke-interface {v4, v0}, Lcom/android/services/telephony/TelephonyConnectionServiceProxy;->addConnectionToConferenceController(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 1020
    .end local v0    # "c":Lcom/android/services/telephony/GsmConnection;
    :cond_2
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHostListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {v4, v5}, Lcom/android/services/telephony/TelephonyConnection;->removeConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 1021
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionListener:Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    invoke-virtual {v4, v5}, Lcom/android/services/telephony/TelephonyConnection;->removeTelephonyConnectionListener(Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;)Lcom/android/services/telephony/TelephonyConnection;

    .line 1022
    iput-object v6, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    .line 1023
    new-instance v4, Landroid/telecom/DisconnectCause;

    const/16 v5, 0x9

    invoke-direct {v4, v5}, Landroid/telecom/DisconnectCause;-><init>(I)V

    invoke-virtual {p0, v4}, Lcom/android/services/telephony/ImsConference;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 1024
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->disconnectConferenceParticipants()V

    .line 1025
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->destroy()V

    .line 1028
    :cond_3
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->updateStatusHints()V

    .line 1029
    return-void

    .line 1004
    .restart local v3    # "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    :cond_4
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v4}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    .line 1003
    invoke-static {v4}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    .local v3, "phoneAccountHandle":Landroid/telecom/PhoneAccountHandle;
    goto :goto_0
.end method

.method private isParticipantHost([Landroid/net/Uri;Landroid/net/Uri;)Z
    .locals 13
    .param p1, "hostHandles"    # [Landroid/net/Uri;
    .param p2, "handle"    # Landroid/net/Uri;

    .prologue
    const/4 v12, 0x1

    const/4 v7, 0x0

    .line 919
    if-eqz p1, :cond_0

    array-length v5, p1

    if-nez v5, :cond_1

    .line 920
    :cond_0
    const-string/jumbo v5, "isParticipantHost(N) : host or participant uri null"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {p0, v5, v6}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 921
    return v7

    .line 919
    :cond_1
    if-eqz p2, :cond_0

    .line 937
    invoke-virtual {p2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 938
    .local v3, "number":Ljava/lang/String;
    const-string/jumbo v5, "[@;:]"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 940
    .local v4, "numberParts":[Ljava/lang/String;
    array-length v5, v4

    if-nez v5, :cond_2

    .line 941
    const-string/jumbo v5, "isParticipantHost(N) : no number in participant handle"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {p0, v5, v6}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 942
    return v7

    .line 944
    :cond_2
    aget-object v3, v4, v7

    .line 946
    array-length v8, p1

    move v6, v7

    :goto_0
    if-ge v6, v8, :cond_6

    aget-object v0, p1, v6

    .line 947
    .local v0, "hostHandle":Landroid/net/Uri;
    if-nez v0, :cond_4

    .line 946
    :cond_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    .line 952
    :cond_4
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 957
    .local v1, "hostNumber":Ljava/lang/String;
    invoke-static {v1, v3}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 959
    .local v2, "isHost":Z
    const-string/jumbo v9, "isParticipantHost(%s) : host: %s, participant %s"

    const/4 v5, 0x3

    new-array v10, v5, [Ljava/lang/Object;

    if-eqz v2, :cond_5

    const-string/jumbo v5, "Y"

    :goto_1
    aput-object v5, v10, v7

    .line 960
    invoke-static {v1}, Landroid/telecom/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v10, v12

    invoke-static {v3}, Landroid/telecom/Log;->pii(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v11, 0x2

    aput-object v5, v10, v11

    .line 959
    invoke-static {p0, v9, v10}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 962
    if-eqz v2, :cond_3

    .line 963
    return v12

    .line 959
    :cond_5
    const-string/jumbo v5, "N"

    goto :goto_1

    .line 966
    .end local v0    # "hostHandle":Landroid/net/Uri;
    .end local v1    # "hostNumber":Ljava/lang/String;
    .end local v2    # "isHost":Z
    :cond_6
    return v7
.end method

.method private isVideoCapable()Z
    .locals 2

    .prologue
    .line 1081
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionCapabilities()I

    move-result v0

    .line 1082
    .local v0, "capabilities":I
    const/16 v1, 0x300

    invoke-static {v0, v1}, Lcom/android/services/telephony/ImsConference;->can(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1083
    const/16 v1, 0xc00

    invoke-static {v0, v1}, Lcom/android/services/telephony/ImsConference;->can(II)Z

    move-result v1

    .line 1082
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private processAddParticipantResponse(Z)V
    .locals 3
    .param p1, "success"    # Z

    .prologue
    const/4 v2, 0x0

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "processAddParticipantResponse: success = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " pending = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 316
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 315
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 319
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->processNextParticipant()V

    .line 321
    :cond_0
    return-void
.end method

.method private processAddParticipantsList(Ljava/lang/String;)V
    .locals 7
    .param p1, "dialString"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 284
    const/4 v0, 0x0

    .line 285
    .local v0, "initAdding":Z
    const-string/jumbo v5, ";"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 286
    .local v3, "participantsArr":[Ljava/lang/String;
    if-nez v3, :cond_1

    const/4 v1, 0x0

    .line 287
    .local v1, "numOfParticipants":I
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "processAddParticipantsList: no of particpants = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 288
    const-string/jumbo v6, " pending = "

    .line 287
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 288
    iget-object v6, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 287
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {p0, v5, v6}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    if-lez v1, :cond_3

    .line 290
    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 292
    const/4 v0, 0x1

    .line 294
    :cond_0
    array-length v5, v3

    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v2, v3, v4

    .line 295
    .local v2, "participant":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 286
    .end local v1    # "numOfParticipants":I
    .end local v2    # "participant":Ljava/lang/String;
    :cond_1
    array-length v1, v3

    .restart local v1    # "numOfParticipants":I
    goto :goto_0

    .line 297
    :cond_2
    if-eqz v0, :cond_3

    .line 298
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->processNextParticipant()V

    .line 301
    :cond_3
    return-void
.end method

.method private processNextParticipant()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 304
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/services/telephony/ImsConference;->addParticipantInternal(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 306
    const-string/jumbo v0, "processNextParticipant: sent request"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    const-string/jumbo v0, "processNextParticipant: failed. Clear pending list."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mPendingParticipantsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method private removeConferenceParticipant(Lcom/android/services/telephony/ConferenceParticipantConnection;)V
    .locals 3
    .param p1, "participant"    # Lcom/android/services/telephony/ConferenceParticipantConnection;

    .prologue
    .line 877
    const-string/jumbo v0, "removeConferenceParticipant: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 879
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mParticipantListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {p1, v0}, Lcom/android/services/telephony/ConferenceParticipantConnection;->removeConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 880
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mUpdateSyncRoot:Ljava/lang/Object;

    monitor-enter v1

    .line 881
    :try_start_0
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/android/services/telephony/ConferenceParticipantConnection;->getUserEntity()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    .line 883
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionService:Lcom/android/services/telephony/TelephonyConnectionServiceProxy;

    invoke-interface {v0, p1}, Lcom/android/services/telephony/TelephonyConnectionServiceProxy;->removeConnection(Landroid/telecom/Connection;)V

    .line 884
    return-void

    .line 880
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setConferenceHost(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 6
    .param p1, "conferenceHost"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 689
    sget-boolean v3, Landroid/telecom/Log;->VERBOSE:Z

    if-eqz v3, :cond_0

    .line 690
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setConferenceHost "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 693
    :cond_0
    iput-object p1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    .line 698
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v3}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 699
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v3}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_3

    .line 702
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v3}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 704
    .local v2, "imsPhone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-static {v3}, Lcom/android/phone/PhoneUtils;->makePstnPhoneAccountHandle(Lcom/android/internal/telephony/Phone;)Landroid/telecom/PhoneAccountHandle;

    move-result-object v3

    .line 703
    iput-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHostPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    .line 705
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mTelecomAccountRegistry:Lcom/android/services/telephony/TelecomAccountRegistry;

    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHostPhoneAccountHandle:Landroid/telecom/PhoneAccountHandle;

    invoke-virtual {v3, v4}, Lcom/android/services/telephony/TelecomAccountRegistry;->getAddress(Landroid/telecom/PhoneAccountHandle;)Landroid/net/Uri;

    move-result-object v0

    .line 707
    .local v0, "hostAddress":Landroid/net/Uri;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 710
    .local v1, "hostAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v0, :cond_1

    .line 711
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 715
    :cond_1
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getCurrentSubscriberUris()[Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 717
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getCurrentSubscriberUris()[Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 716
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 720
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Landroid/net/Uri;

    iput-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHostAddress:[Landroid/net/Uri;

    .line 721
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHostAddress:[Landroid/net/Uri;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/net/Uri;

    iput-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHostAddress:[Landroid/net/Uri;

    .line 724
    .end local v0    # "hostAddress":Landroid/net/Uri;
    .end local v1    # "hostAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v2    # "imsPhone":Lcom/android/internal/telephony/Phone;
    :cond_3
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHostListener:Landroid/telecom/Connection$Listener;

    invoke-virtual {v3, v4}, Lcom/android/services/telephony/TelephonyConnection;->addConnectionListener(Landroid/telecom/Connection$Listener;)Landroid/telecom/Connection;

    .line 725
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mTelephonyConnectionListener:Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;

    invoke-virtual {v3, v4}, Lcom/android/services/telephony/TelephonyConnection;->addTelephonyConnectionListener(Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;)Lcom/android/services/telephony/TelephonyConnection;

    .line 726
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->getConnectionCapabilities()I

    move-result v3

    .line 727
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v4}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionCapabilities()I

    move-result v4

    .line 728
    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v5}, Lcom/android/services/telephony/TelephonyConnection;->isCarrierVideoConferencingSupported()Z

    move-result v5

    .line 726
    invoke-direct {p0, v3, v4, v5}, Lcom/android/services/telephony/ImsConference;->applyHostCapabilities(IIZ)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/android/services/telephony/ImsConference;->setConnectionCapabilities(I)V

    .line 729
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->getConnectionProperties()I

    move-result v3

    .line 730
    iget-object v4, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v4}, Lcom/android/services/telephony/TelephonyConnection;->getConnectionProperties()I

    move-result v4

    .line 729
    invoke-direct {p0, v3, v4}, Lcom/android/services/telephony/ImsConference;->applyHostProperties(II)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/android/services/telephony/ImsConference;->setConnectionProperties(I)V

    .line 732
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v3}, Lcom/android/services/telephony/TelephonyConnection;->getState()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/android/services/telephony/ImsConference;->setState(I)V

    .line 733
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->updateStatusHints()V

    .line 734
    return-void
.end method

.method private updateManageConference()V
    .locals 7

    .prologue
    .line 663
    const/16 v3, 0x80

    invoke-virtual {p0, v3}, Lcom/android/services/telephony/ImsConference;->can(I)Z

    move-result v2

    .line 664
    .local v2, "couldManageConference":Z
    iget-object v3, p0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    xor-int/lit8 v0, v3, 0x1

    .line 665
    .local v0, "canManageConference":Z
    const-string/jumbo v4, "updateManageConference was :%s is:%s"

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Object;

    if-eqz v2, :cond_1

    const-string/jumbo v3, "Y"

    :goto_0
    const/4 v6, 0x0

    aput-object v3, v5, v6

    .line 666
    if-eqz v0, :cond_2

    const-string/jumbo v3, "Y"

    :goto_1
    const/4 v6, 0x1

    aput-object v3, v5, v6

    .line 665
    invoke-static {p0, v4, v5}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 668
    if-eq v2, v0, :cond_0

    .line 669
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->getConnectionCapabilities()I

    move-result v1

    .line 671
    .local v1, "capabilities":I
    if-eqz v0, :cond_3

    .line 672
    or-int/lit16 v1, v1, 0x80

    .line 673
    const v3, -0x200001

    and-int/2addr v1, v3

    .line 679
    :goto_2
    invoke-virtual {p0, v1}, Lcom/android/services/telephony/ImsConference;->setConnectionCapabilities(I)V

    .line 681
    .end local v1    # "capabilities":I
    :cond_0
    return-void

    .line 665
    :cond_1
    const-string/jumbo v3, "N"

    goto :goto_0

    .line 666
    :cond_2
    const-string/jumbo v3, "N"

    goto :goto_1

    .line 675
    .restart local v1    # "capabilities":I
    :cond_3
    and-int/lit16 v1, v1, -0x81

    .line 676
    const/high16 v3, 0x200000

    or-int/2addr v1, v3

    goto :goto_2
.end method

.method private updateStatusHints()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1087
    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v5, :cond_0

    .line 1088
    invoke-virtual {p0, v9}, Lcom/android/services/telephony/ImsConference;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 1089
    return-void

    .line 1092
    :cond_0
    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v5}, Lcom/android/services/telephony/TelephonyConnection;->isWifi()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1093
    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v5}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    .line 1094
    .local v2, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v2, :cond_2

    .line 1095
    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1096
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v1, ""

    .line 1097
    .local v1, "displaySubId":Ljava/lang/String;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_1

    .line 1098
    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v5}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v3

    .line 1100
    .local v3, "phoneId":I
    iget-object v5, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v5}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 1099
    invoke-static {v5}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoForSimSlotIndex(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v4

    .line 1102
    .local v4, "sub":Landroid/telephony/SubscriptionInfo;
    if-eqz v4, :cond_1

    .line 1103
    invoke-virtual {v4}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1104
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1107
    .end local v3    # "phoneId":I
    .end local v4    # "sub":Landroid/telephony/SubscriptionInfo;
    :cond_1
    new-instance v5, Landroid/telecom/StatusHints;

    .line 1108
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f0b0500

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1110
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 1111
    const v8, 0x7f02008f

    .line 1109
    invoke-static {v7, v8}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Icon;

    move-result-object v7

    .line 1107
    invoke-direct {v5, v6, v7, v9}, Landroid/telecom/StatusHints;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Icon;Landroid/os/Bundle;)V

    invoke-virtual {p0, v5}, Lcom/android/services/telephony/ImsConference;->setStatusHints(Landroid/telecom/StatusHints;)V

    .line 1117
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "displaySubId":Ljava/lang/String;
    .end local v2    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_2
    :goto_0
    return-void

    .line 1115
    :cond_3
    invoke-virtual {p0, v9}, Lcom/android/services/telephony/ImsConference;->setStatusHints(Landroid/telecom/StatusHints;)V

    goto :goto_0
.end method


# virtual methods
.method public getMaximumConferenceSize()I
    .locals 3

    .prologue
    .line 1174
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 1178
    .local v0, "b":Landroid/os/PersistableBundle;
    if-nez v0, :cond_0

    .line 1179
    const-string/jumbo v1, "getMaximumConferenceSize - failed to get conference size"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Landroid/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1180
    const/4 v1, 0x5

    return v1

    .line 1182
    :cond_0
    const-string/jumbo v1, "ims_conference_size_limit_int"

    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public getNumberOfParticipants()I
    .locals 1

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public getPrimaryConnection()Landroid/telecom/Connection;
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoProvider()Landroid/telecom/Connection$VideoProvider;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 458
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->getVideoProvider()Landroid/telecom/Connection$VideoProvider;

    move-result-object v0

    return-object v0

    .line 461
    :cond_0
    return-object v1
.end method

.method public getVideoState()I
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->getVideoState()I

    move-result v0

    return v0

    .line 474
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isConferenceHost()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 644
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v2, :cond_0

    .line 645
    return v1

    .line 648
    :cond_0
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v2}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    .line 650
    .local v0, "originalConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->isMultiparty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 651
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->isConferenceHost()Z

    move-result v1

    .line 650
    :cond_1
    return v1
.end method

.method public isFullConference()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1197
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->isMaximumConferenceSizeEnforced()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1198
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->getNumberOfParticipants()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->getMaximumConferenceSize()I

    move-result v2

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1197
    :cond_0
    return v0
.end method

.method public isMaximumConferenceSizeEnforced()Z
    .locals 2

    .prologue
    .line 1163
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 1165
    .local v0, "b":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    .line 1166
    const-string/jumbo v1, "is_ims_conference_size_enforced_bool"

    .line 1165
    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onAddParticipant(Ljava/lang/String;)V
    .locals 3
    .param p1, "participant"    # Ljava/lang/String;

    .prologue
    .line 542
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 543
    :cond_0
    return-void

    .line 545
    :cond_1
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 546
    return-void
.end method

.method public onConnectionAdded(Landroid/telecom/Connection;)V
    .locals 0
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 619
    return-void
.end method

.method public onDisconnect()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 487
    const-string/jumbo v2, "onDisconnect: hanging up conference host."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 488
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v2, :cond_0

    .line 489
    return-void

    .line 492
    :cond_0
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->disconnectConferenceParticipants()V

    .line 494
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v2}, Lcom/android/services/telephony/TelephonyConnection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    .line 495
    .local v0, "call":Lcom/android/internal/telephony/Call;
    if-eqz v0, :cond_1

    .line 497
    :try_start_0
    invoke-virtual {v0}, Lcom/android/internal/telephony/Call;->hangup()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 502
    :cond_1
    :goto_0
    return-void

    .line 498
    :catch_0
    move-exception v1

    .line 499
    .local v1, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v2, "Exception thrown trying to hangup conference"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Landroid/telecom/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onHold()V
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v0, :cond_0

    .line 570
    return-void

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->performHold()V

    .line 573
    return-void
.end method

.method public onMerge(Landroid/telecom/Connection;)V
    .locals 4
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 526
    :try_start_0
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v2}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 527
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    if-eqz v1, :cond_0

    .line 528
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->conference()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    :cond_0
    :goto_0
    return-void

    .line 530
    :catch_0
    move-exception v0

    .line 531
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v2, "Exception thrown trying to merge call into a conference"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v2, v3}, Landroid/telecom/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onPlayDtmfTone(C)V
    .locals 1
    .param p1, "c"    # C

    .prologue
    .line 593
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v0, :cond_0

    .line 594
    return-void

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnection;->onPlayDtmfTone(C)V

    .line 597
    return-void
.end method

.method public onSeparate(Landroid/telecom/Connection;)V
    .locals 2
    .param p1, "connection"    # Landroid/telecom/Connection;

    .prologue
    .line 514
    const-string/jumbo v0, "Cannot separate connections from an IMS conference."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->wtf(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 515
    return-void
.end method

.method public onStopDtmfTone()V
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v0, :cond_0

    .line 605
    return-void

    .line 607
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->onStopDtmfTone()V

    .line 608
    return-void
.end method

.method public onUnhold()V
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v0, :cond_0

    .line 581
    return-void

    .line 583
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0}, Lcom/android/services/telephony/TelephonyConnection;->performUnhold()V

    .line 584
    return-void
.end method

.method public setState(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    const/4 v4, 0x0

    .line 1037
    const-string/jumbo v1, "setState %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p0, v1, v2}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1039
    packed-switch p1, :pswitch_data_0

    .line 1074
    :goto_0
    :pswitch_0
    return-void

    .line 1046
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->setDialing()V

    goto :goto_0

    .line 1050
    :pswitch_2
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-nez v1, :cond_0

    .line 1051
    new-instance v0, Landroid/telecom/DisconnectCause;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/telecom/DisconnectCause;-><init>(I)V

    .line 1063
    .local v0, "disconnectCause":Landroid/telecom/DisconnectCause;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/ImsConference;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 1064
    invoke-direct {p0}, Lcom/android/services/telephony/ImsConference;->disconnectConferenceParticipants()V

    .line 1065
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->destroy()V

    goto :goto_0

    .line 1053
    .end local v0    # "disconnectCause":Landroid/telecom/DisconnectCause;
    :cond_0
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1055
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getDisconnectCause()I

    move-result v1

    .line 1056
    iget-object v2, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v2}, Lcom/android/services/telephony/TelephonyConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    .line 1054
    invoke-static {v1, v2}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(II)Landroid/telecom/DisconnectCause;

    move-result-object v0

    .restart local v0    # "disconnectCause":Landroid/telecom/DisconnectCause;
    goto :goto_1

    .line 1059
    .end local v0    # "disconnectCause":Landroid/telecom/DisconnectCause;
    :cond_1
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getDisconnectCause()I

    move-result v1

    .line 1058
    invoke-static {v1, v4}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(II)Landroid/telecom/DisconnectCause;

    move-result-object v0

    .restart local v0    # "disconnectCause":Landroid/telecom/DisconnectCause;
    goto :goto_1

    .line 1068
    .end local v0    # "disconnectCause":Landroid/telecom/DisconnectCause;
    :pswitch_3
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->setActive()V

    goto :goto_0

    .line 1071
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->setOnHold()V

    goto :goto_0

    .line 1039
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1126
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "[ImsConference objId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1127
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1128
    const-string/jumbo v1, " telecomCallID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1129
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->getTelecomCallId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1130
    const-string/jumbo v1, " state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131
    invoke-virtual {p0}, Lcom/android/services/telephony/ImsConference;->getState()I

    move-result v1

    invoke-static {v1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132
    const-string/jumbo v1, " hostConnection:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1134
    const-string/jumbo v1, " participants:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceParticipantConnections:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1136
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1137
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public updateConferenceParticipantsAfterCreation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 324
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    if-eqz v0, :cond_0

    .line 325
    const-string/jumbo v0, "updateConferenceStateAfterCreation :: process participant update"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 326
    iget-object v0, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    .line 327
    iget-object v1, p0, Lcom/android/services/telephony/ImsConference;->mConferenceHost:Lcom/android/services/telephony/TelephonyConnection;

    invoke-virtual {v1}, Lcom/android/services/telephony/TelephonyConnection;->getConferenceParticipants()Ljava/util/List;

    move-result-object v1

    .line 326
    invoke-direct {p0, v0, v1}, Lcom/android/services/telephony/ImsConference;->handleConferenceParticipantsUpdate(Lcom/android/services/telephony/TelephonyConnection;Ljava/util/List;)V

    .line 331
    :goto_0
    return-void

    .line 329
    :cond_0
    const-string/jumbo v0, "updateConferenceStateAfterCreation :: null mConferenceHost"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
