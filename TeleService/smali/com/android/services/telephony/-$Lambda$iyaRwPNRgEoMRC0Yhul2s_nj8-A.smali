.class final synthetic Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/function/Predicate;


# static fields
.field public static final synthetic $INST$0:Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;

.field public static final synthetic $INST$1:Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;


# instance fields
.field private final synthetic $id:B


# direct methods
.method private final synthetic $m$0(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/Object;

    .prologue
    check-cast p1, Landroid/telecom/Conferenceable;

    .end local p1    # "arg0":Ljava/lang/Object;
    invoke-static {p1}, Lcom/android/services/telephony/ImsConferenceController;->lambda$-com_android_services_telephony_ImsConferenceController_11605(Landroid/telecom/Conferenceable;)Z

    move-result v0

    return v0
.end method

.method private final synthetic $m$1(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/Object;

    .prologue
    check-cast p1, Lcom/android/services/telephony/TelephonyConnection;

    .end local p1    # "arg0":Ljava/lang/Object;
    invoke-static {p1}, Lcom/android/services/telephony/TelephonyConferenceController;->lambda$-com_android_services_telephony_TelephonyConferenceController_6731(Lcom/android/services/telephony/TelephonyConnection;)Z

    move-result v0

    return v0
.end method

.method static synthetic constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;-><init>(B)V

    sput-object v0, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;->$INST$0:Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;

    new-instance v0, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;-><init>(B)V

    sput-object v0, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;->$INST$1:Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;

    return-void
.end method

.method private synthetic constructor <init>(B)V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte p1, p0, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;->$id:B

    return-void
.end method


# virtual methods
.method public final test(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    iget-byte v0, p0, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;->$id:B

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;->$m$0(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/services/telephony/-$Lambda$iyaRwPNRgEoMRC0Yhul2s_nj8-A;->$m$1(Ljava/lang/Object;)Z

    move-result v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
