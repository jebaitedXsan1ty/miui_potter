.class Lcom/android/services/telephony/TelephonyConnectionService$5;
.super Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;
.source "TelephonyConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/TelephonyConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/TelephonyConnectionService;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/TelephonyConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/TelephonyConnectionService;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/TelephonyConnectionService$5;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    .line 246
    invoke-direct {p0}, Lcom/android/services/telephony/TelephonyConnection$TelephonyConnectionListener;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onCallDisconnectResetDisconnectCause()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$5;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-static {v0}, Lcom/android/services/telephony/TelephonyConnectionService;->-wrap4(Lcom/android/services/telephony/TelephonyConnectionService;)V

    .line 260
    return-void
.end method

.method public onOriginalConnectionConfigured(Lcom/android/services/telephony/TelephonyConnection;)V
    .locals 1
    .param p1, "c"    # Lcom/android/services/telephony/TelephonyConnection;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$5;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-virtual {v0, p1}, Lcom/android/services/telephony/TelephonyConnectionService;->addConnectionToConferenceController(Lcom/android/services/telephony/TelephonyConnection;)V

    .line 250
    return-void
.end method

.method public onOriginalConnectionRetry(Lcom/android/services/telephony/TelephonyConnection;Z)V
    .locals 1
    .param p1, "c"    # Lcom/android/services/telephony/TelephonyConnection;
    .param p2, "isPermanentFailure"    # Z

    .prologue
    .line 254
    iget-object v0, p0, Lcom/android/services/telephony/TelephonyConnectionService$5;->this$0:Lcom/android/services/telephony/TelephonyConnectionService;

    invoke-static {v0, p1, p2}, Lcom/android/services/telephony/TelephonyConnectionService;->-wrap5(Lcom/android/services/telephony/TelephonyConnectionService;Lcom/android/services/telephony/TelephonyConnection;Z)V

    .line 255
    return-void
.end method
