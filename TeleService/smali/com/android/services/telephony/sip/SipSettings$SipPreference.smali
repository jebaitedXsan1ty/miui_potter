.class Lcom/android/services/telephony/sip/SipSettings$SipPreference;
.super Landroid/preference/Preference;
.source "SipSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/sip/SipSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SipPreference"
.end annotation


# instance fields
.field mProfile:Landroid/net/sip/SipProfile;

.field final synthetic this$0:Lcom/android/services/telephony/sip/SipSettings;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/sip/SipSettings;Landroid/content/Context;Landroid/net/sip/SipProfile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/sip/SipSettings;
    .param p2, "c"    # Landroid/content/Context;
    .param p3, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 78
    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 79
    invoke-virtual {p0, p3}, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->setProfile(Landroid/net/sip/SipProfile;)V

    .line 80
    return-void
.end method


# virtual methods
.method setProfile(Landroid/net/sip/SipProfile;)V
    .locals 2
    .param p1, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->mProfile:Landroid/net/sip/SipProfile;

    .line 88
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    invoke-static {v0, p1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap1(Lcom/android/services/telephony/sip/SipSettings;Landroid/net/sip/SipProfile;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    invoke-static {v0}, Lcom/android/services/telephony/sip/SipSettings;->-get3(Lcom/android/services/telephony/sip/SipSettings;)Lcom/android/services/telephony/sip/SipPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/services/telephony/sip/SipPreferences;->isReceivingCallsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    const v1, 0x7f0b01cb

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->updateSummary(Ljava/lang/String;)V

    .line 92
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    const v1, 0x7f0b01ce

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method updateSummary(Ljava/lang/String;)V
    .locals 6
    .param p1, "registrationStatus"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 95
    iget-object v2, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->mProfile:Landroid/net/sip/SipProfile;

    invoke-virtual {v2}, Landroid/net/sip/SipProfile;->getCallingUid()I

    move-result v0

    .line 101
    .local v0, "profileUid":I
    const-string/jumbo v1, ""

    .line 102
    .local v1, "summary":Ljava/lang/String;
    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    invoke-static {v2}, Lcom/android/services/telephony/sip/SipSettings;->-get4(Lcom/android/services/telephony/sip/SipSettings;)I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 104
    iget-object v2, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 105
    iget-object v4, p0, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    invoke-static {v4, v0}, Lcom/android/services/telephony/sip/SipSettings;->-wrap0(Lcom/android/services/telephony/sip/SipSettings;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 104
    const v4, 0x7f0b01d6

    invoke-virtual {v2, v4, v3}, Lcom/android/services/telephony/sip/SipSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 109
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/SipSettings$SipPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 110
    return-void

    .line 107
    :cond_0
    move-object v1, p1

    goto :goto_0
.end method
