.class Lcom/android/services/telephony/sip/SipSettings$8;
.super Ljava/lang/Object;
.source "SipSettings.java"

# interfaces
.implements Landroid/net/sip/SipRegistrationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/services/telephony/sip/SipSettings;->createRegistrationListener()Landroid/net/sip/SipRegistrationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/services/telephony/sip/SipSettings;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/sip/SipSettings;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/sip/SipSettings;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onRegistering(Ljava/lang/String;)V
    .locals 3
    .param p1, "profileUri"    # Ljava/lang/String;

    .prologue
    .line 367
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 368
    const v2, 0x7f0b01cc

    .line 367
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap6(Lcom/android/services/telephony/sip/SipSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    return-void
.end method

.method public onRegistrationDone(Ljava/lang/String;J)V
    .locals 3
    .param p1, "profileUri"    # Ljava/lang/String;
    .param p2, "expiryTime"    # J

    .prologue
    .line 361
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 362
    const v2, 0x7f0b01d2

    .line 361
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap6(Lcom/android/services/telephony/sip/SipSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    return-void
.end method

.method public onRegistrationFailed(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "profileUri"    # Ljava/lang/String;
    .param p2, "errorCode"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 374
    packed-switch p2, :pswitch_data_0

    .line 401
    :pswitch_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 403
    const/4 v3, 0x0

    aput-object p3, v2, v3

    .line 402
    const v3, 0x7f0b01d3

    .line 401
    invoke-virtual {v1, v3, v2}, Lcom/android/services/telephony/sip/SipSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap6(Lcom/android/services/telephony/sip/SipSettings;Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :goto_0
    return-void

    .line 376
    :pswitch_1
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 377
    const v2, 0x7f0b01cd

    .line 376
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap6(Lcom/android/services/telephony/sip/SipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 380
    :pswitch_2
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 381
    const v2, 0x7f0b01d4

    .line 380
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap6(Lcom/android/services/telephony/sip/SipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 384
    :pswitch_3
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 385
    const v2, 0x7f0b01d5

    .line 384
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap6(Lcom/android/services/telephony/sip/SipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 388
    :pswitch_4
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    invoke-virtual {v0}, Lcom/android/services/telephony/sip/SipSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/net/sip/SipManager;->isSipWifiOnly(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 390
    const v2, 0x7f0b01d0

    .line 389
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap6(Lcom/android/services/telephony/sip/SipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 393
    const v2, 0x7f0b01cf

    .line 392
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap6(Lcom/android/services/telephony/sip/SipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 397
    :pswitch_5
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    iget-object v1, p0, Lcom/android/services/telephony/sip/SipSettings$8;->this$0:Lcom/android/services/telephony/sip/SipSettings;

    .line 398
    const v2, 0x7f0b01d1

    .line 397
    invoke-virtual {v1, v2}, Lcom/android/services/telephony/sip/SipSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/android/services/telephony/sip/SipSettings;->-wrap6(Lcom/android/services/telephony/sip/SipSettings;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 374
    :pswitch_data_0
    .packed-switch -0xc
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
