.class Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;
.super Landroid/preference/Preference;
.source "MiuiSipSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/sip/MiuiSipSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SipPreference"
.end annotation


# instance fields
.field mProfile:Landroid/net/sip/SipProfile;

.field final synthetic this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;


# direct methods
.method constructor <init>(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/content/Context;Landroid/net/sip/SipProfile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/services/telephony/sip/MiuiSipSettings;
    .param p2, "c"    # Landroid/content/Context;
    .param p3, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    .line 100
    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 101
    invoke-virtual {p0, p3}, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->setProfile(Landroid/net/sip/SipProfile;)V

    .line 102
    return-void
.end method


# virtual methods
.method setProfile(Landroid/net/sip/SipProfile;)V
    .locals 2
    .param p1, "p"    # Landroid/net/sip/SipProfile;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->mProfile:Landroid/net/sip/SipProfile;

    .line 110
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    invoke-static {v0, p1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap1(Lcom/android/services/telephony/sip/MiuiSipSettings;Landroid/net/sip/SipProfile;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    invoke-static {v0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-get3(Lcom/android/services/telephony/sip/MiuiSipSettings;)Lcom/android/services/telephony/sip/SipSharedPreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/services/telephony/sip/SipSharedPreferences;->isReceivingCallsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    const v1, 0x7f0b01cb

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->updateSummary(Ljava/lang/String;)V

    .line 114
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    const v1, 0x7f0b01ce

    invoke-virtual {v0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method updateSummary(Ljava/lang/String;)V
    .locals 6
    .param p1, "registrationStatus"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 117
    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->mProfile:Landroid/net/sip/SipProfile;

    invoke-virtual {v2}, Landroid/net/sip/SipProfile;->getCallingUid()I

    move-result v0

    .line 123
    .local v0, "profileUid":I
    const-string/jumbo v1, ""

    .line 124
    .local v1, "summary":Ljava/lang/String;
    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    invoke-static {v2}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-get4(Lcom/android/services/telephony/sip/MiuiSipSettings;)I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 126
    iget-object v2, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 127
    iget-object v4, p0, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->this$0:Lcom/android/services/telephony/sip/MiuiSipSettings;

    invoke-static {v4, v0}, Lcom/android/services/telephony/sip/MiuiSipSettings;->-wrap0(Lcom/android/services/telephony/sip/MiuiSipSettings;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 126
    const v4, 0x7f0b01d6

    invoke-virtual {v2, v4, v3}, Lcom/android/services/telephony/sip/MiuiSipSettings;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 131
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/services/telephony/sip/MiuiSipSettings$SipPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 132
    return-void

    .line 129
    :cond_0
    move-object v1, p1

    goto :goto_0
.end method
