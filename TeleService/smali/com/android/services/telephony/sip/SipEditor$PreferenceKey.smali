.class final enum Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
.super Ljava/lang/Enum;
.source "SipEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/services/telephony/sip/SipEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "PreferenceKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

.field public static final enum AuthUserName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

.field public static final enum DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

.field public static final enum DomainAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

.field public static final enum Password:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

.field public static final enum Port:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

.field public static final enum ProxyAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

.field public static final enum SendKeepAlive:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

.field public static final enum Transport:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

.field public static final enum Username:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;


# instance fields
.field final defaultSummary:I

.field final initValue:I

.field preference:Landroid/preference/Preference;

.field final text:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 106
    new-instance v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v1, "Username"

    const v3, 0x7f0b01f8

    const v5, 0x7f0b01e4

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Username:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 107
    new-instance v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v4, "Password"

    const v6, 0x7f0b01f9

    const v8, 0x7f0b01e5

    move v5, v9

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Password:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 108
    new-instance v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v4, "DomainAddress"

    const v6, 0x7f0b01f7

    .line 109
    const v8, 0x7f0b01e6

    move v5, v10

    move v7, v2

    .line 108
    invoke-direct/range {v3 .. v8}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DomainAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 110
    new-instance v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v4, "DisplayName"

    const v6, 0x7f0b01fa

    const v8, 0x7f0b01e7

    move v5, v11

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 111
    new-instance v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v4, "ProxyAddress"

    const v6, 0x7f0b01fb

    const v8, 0x7f0b01e8

    move v5, v12

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ProxyAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 112
    new-instance v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v4, "Port"

    const/4 v5, 0x5

    const v6, 0x7f0b01fc

    const v7, 0x7f0b01e9

    const v8, 0x7f0b01e9

    invoke-direct/range {v3 .. v8}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Port:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 113
    new-instance v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v4, "Transport"

    const/4 v5, 0x6

    const v6, 0x7f0b01fd

    const v7, 0x7f0b01ea

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Transport:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 114
    new-instance v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v4, "SendKeepAlive"

    const/4 v5, 0x7

    const v6, 0x7f0b01fe

    const v7, 0x7f0b01f3

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->SendKeepAlive:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 115
    new-instance v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const-string/jumbo v4, "AuthUserName"

    const/16 v5, 0x8

    const v6, 0x7f0b0200

    const v8, 0x7f0b01e8

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->AuthUserName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 105
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Username:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Password:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    aput-object v1, v0, v9

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DomainAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    aput-object v1, v0, v10

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    aput-object v1, v0, v11

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->ProxyAddress:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    aput-object v1, v0, v12

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Port:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Transport:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->SendKeepAlive:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->AuthUserName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->$VALUES:[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .param p3, "text"    # I
    .param p4, "initValue"    # I
    .param p5, "defaultSummary"    # I

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 129
    iput p3, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->text:I

    .line 130
    iput p4, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->initValue:I

    .line 131
    iput p5, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->defaultSummary:I

    .line 132
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 105
    const-class v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    return-object v0
.end method

.method public static values()[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->$VALUES:[Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    return-object v0
.end method


# virtual methods
.method getValue()Ljava/lang/String;
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    instance-of v0, v0, Landroid/preference/EditTextPreference;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/EditTextPreference;

    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    instance-of v0, v0, Landroid/preference/ListPreference;

    if-eqz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 140
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getValue() for the preference "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method setValue(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    instance-of v1, v1, Landroid/preference/EditTextPreference;

    if-eqz v1, :cond_1

    .line 145
    invoke-virtual {p0}, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "oldValue":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    check-cast v1, Landroid/preference/EditTextPreference;

    invoke-virtual {v1, p1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 147
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Password:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    .line 157
    .end local v0    # "oldValue":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 158
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    iget v2, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->defaultSummary:I

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    .line 167
    :goto_1
    return-void

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    instance-of v1, v1, Landroid/preference/ListPreference;

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    check-cast v1, Landroid/preference/ListPreference;

    invoke-virtual {v1, p1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :cond_2
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->Password:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    if-ne p0, v1, :cond_3

    .line 160
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    invoke-static {p1}, Lcom/android/services/telephony/sip/SipEditor;->-wrap1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 161
    :cond_3
    sget-object v1, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->DisplayName:Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;

    if-ne p0, v1, :cond_4

    .line 162
    invoke-static {}, Lcom/android/services/telephony/sip/SipEditor;->-wrap0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 161
    if-eqz v1, :cond_4

    .line 163
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    iget v2, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->defaultSummary:I

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_1

    .line 165
    :cond_4
    iget-object v1, p0, Lcom/android/services/telephony/sip/SipEditor$PreferenceKey;->preference:Landroid/preference/Preference;

    invoke-virtual {v1, p1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
