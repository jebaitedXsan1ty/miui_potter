.class final Lcom/android/services/telephony/CdmaConnection;
.super Lcom/android/services/telephony/TelephonyConnection;
.source "CdmaConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/services/telephony/CdmaConnection$1;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-internal-telephony-Call$StateSwitchesValues:[I


# instance fields
.field private mAllowMute:Z

.field private mDtmfBurstConfirmationPending:Z

.field private final mDtmfQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field private final mEmergencyTonePlayer:Lcom/android/services/telephony/EmergencyTonePlayer;

.field private final mHandler:Landroid/os/Handler;

.field private mIsCallWaiting:Z

.field private mIsConnectionTimeReset:Z


# direct methods
.method private static synthetic -getcom-android-internal-telephony-Call$StateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/services/telephony/CdmaConnection;->-com-android-internal-telephony-Call$StateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/services/telephony/CdmaConnection;->-com-android-internal-telephony-Call$StateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/internal/telephony/Call$State;->values()[Lcom/android/internal/telephony/Call$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DISCONNECTED:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/internal/telephony/Call$State;->DISCONNECTING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/internal/telephony/Call$State;->HOLDING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/internal/telephony/Call$State;->IDLE:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/internal/telephony/Call$State;->INCOMING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    sput-object v0, Lcom/android/services/telephony/CdmaConnection;->-com-android-internal-telephony-Call$StateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1

    :catch_8
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -wrap0(Lcom/android/services/telephony/CdmaConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/CdmaConnection;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConnection;->handleBurstDtmfConfirmation()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/services/telephony/CdmaConnection;)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/CdmaConnection;

    .prologue
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConnection;->handleCdmaConnectionTimeReset()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/services/telephony/CdmaConnection;I)V
    .locals 0
    .param p0, "-this"    # Lcom/android/services/telephony/CdmaConnection;
    .param p1, "telephonyDisconnectCause"    # I

    .prologue
    invoke-direct {p0, p1}, Lcom/android/services/telephony/CdmaConnection;->hangupCallWaiting(I)V

    return-void
.end method

.method constructor <init>(Lcom/android/internal/telephony/Connection;Lcom/android/services/telephony/EmergencyTonePlayer;ZZLjava/lang/String;)V
    .locals 4
    .param p1, "connection"    # Lcom/android/internal/telephony/Connection;
    .param p2, "emergencyTonePlayer"    # Lcom/android/services/telephony/EmergencyTonePlayer;
    .param p3, "allowMute"    # Z
    .param p4, "isOutgoing"    # Z
    .param p5, "telecomCallId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1, p5, p4}, Lcom/android/services/telephony/TelephonyConnection;-><init>(Lcom/android/internal/telephony/Connection;Ljava/lang/String;Z)V

    .line 47
    new-instance v2, Lcom/android/services/telephony/CdmaConnection$1;

    invoke-direct {v2, p0}, Lcom/android/services/telephony/CdmaConnection$1;-><init>(Lcom/android/services/telephony/CdmaConnection;)V

    iput-object v2, p0, Lcom/android/services/telephony/CdmaConnection;->mHandler:Landroid/os/Handler;

    .line 74
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfQueue:Ljava/util/Queue;

    .line 78
    iput-boolean v1, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfBurstConfirmationPending:Z

    .line 80
    iput-boolean v1, p0, Lcom/android/services/telephony/CdmaConnection;->mIsConnectionTimeReset:Z

    .line 89
    iput-object p2, p0, Lcom/android/services/telephony/CdmaConnection;->mEmergencyTonePlayer:Lcom/android/services/telephony/EmergencyTonePlayer;

    .line 90
    iput-boolean p3, p0, Lcom/android/services/telephony/CdmaConnection;->mAllowMute:Z

    .line 91
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/android/services/telephony/CdmaConnection;->mIsCallWaiting:Z

    .line 92
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    instance-of v0, v1, Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;

    .line 94
    .local v0, "isImsCall":Z
    iget-boolean v1, p0, Lcom/android/services/telephony/CdmaConnection;->mIsCallWaiting:Z

    if-eqz v1, :cond_1

    xor-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_1

    .line 95
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConnection;->startCallWaitingTimer()V

    .line 97
    :cond_1
    return-void
.end method

.method private handleBurstDtmfConfirmation()V
    .locals 7

    .prologue
    .line 277
    const/4 v1, 0x0

    .line 278
    .local v1, "dtmfDigits":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfQueue:Ljava/util/Queue;

    monitor-enter v3

    .line 279
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfBurstConfirmationPending:Z

    .line 280
    iget-object v2, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 281
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 282
    .local v0, "builder":Ljava/lang/StringBuilder;
    :goto_0
    iget-object v2, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 283
    iget-object v2, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 278
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "dtmfDigits":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 285
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    .restart local v1    # "dtmfDigits":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 290
    .local v1, "dtmfDigits":Ljava/lang/String;
    const-string/jumbo v2, "%d dtmf character[s] removed from the queue"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static {p0, v2, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 292
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "dtmfDigits":Ljava/lang/String;
    :cond_1
    if-eqz v1, :cond_2

    .line 293
    invoke-direct {p0, v1}, Lcom/android/services/telephony/CdmaConnection;->sendBurstDtmfStringLocked(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    monitor-exit v3

    .line 296
    return-void
.end method

.method private handleCdmaConnectionTimeReset()V
    .locals 6

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    instance-of v0, v1, Lcom/android/internal/telephony/imsphone/ImsPhoneConnection;

    .line 315
    .local v0, "isImsCall":Z
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/android/services/telephony/CdmaConnection;->mIsConnectionTimeReset:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/services/telephony/CdmaConnection;->mIsOutgoing:Z

    if-eqz v1, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 317
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v1

    sget-object v2, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    if-ne v1, v2, :cond_0

    .line 318
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getDurationMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 319
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/services/telephony/CdmaConnection;->mIsConnectionTimeReset:Z

    .line 320
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->resetConnectionTime()V

    .line 321
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->resetCdmaConnectionTime()V

    .line 323
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->updateConnectionCapabilities()V

    .line 325
    :cond_0
    return-void
.end method

.method private hangupCallWaiting(I)V
    .locals 4
    .param p1, "telephonyDisconnectCause"    # I

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    .line 234
    .local v1, "originalConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v1, :cond_0

    .line 236
    :try_start_0
    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->hangup()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getPhoneId()I

    move-result v2

    .line 240
    invoke-static {p1, v2}, Lcom/android/services/telephony/DisconnectCauseUtil;->toTelecomDisconnectCause(II)Landroid/telecom/DisconnectCause;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/services/telephony/CdmaConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    .line 243
    :cond_0
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v2, "Failed to hangup call waiting call"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v2, v3}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private isEmergency()Z
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getAddress()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/phone/PhoneUtils;->isLocalEmergencyNumber(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private sendBurstDtmfStringLocked(Ljava/lang/String;)V
    .locals 4
    .param p1, "dtmfString"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 271
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 272
    iget-object v1, p0, Lcom/android/services/telephony/CdmaConnection;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 271
    invoke-virtual {v0, p1, v3, v3, v1}, Lcom/android/internal/telephony/Phone;->sendBurstDtmf(Ljava/lang/String;IILandroid/os/Message;)V

    .line 273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfBurstConfirmationPending:Z

    .line 274
    return-void
.end method

.method private sendShortDtmfToNetwork(C)V
    .locals 3
    .param p1, "digit"    # C

    .prologue
    .line 261
    iget-object v1, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfQueue:Ljava/util/Queue;

    monitor-enter v1

    .line 262
    :try_start_0
    iget-boolean v0, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfBurstConfirmationPending:Z

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/android/services/telephony/CdmaConnection;->mDtmfQueue:Ljava/util/Queue;

    new-instance v2, Ljava/lang/Character;

    invoke-direct {v2, p1}, Ljava/lang/Character;-><init>(C)V

    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    .line 268
    return-void

    .line 265
    :cond_0
    :try_start_1
    invoke-static {p1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/services/telephony/CdmaConnection;->sendBurstDtmfStringLocked(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private startCallWaitingTimer()V
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lcom/android/services/telephony/CdmaConnection;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x4e20

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 230
    return-void
.end method

.method private useBurstDtmf()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 249
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->isImsConnection()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 250
    const-string/jumbo v2, "in ims call, return false"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Lcom/android/services/telephony/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    return v1

    .line 254
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 255
    const-string/jumbo v3, "dtmf_tone_type"

    .line 253
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 257
    .local v0, "dtmfTypeSetting":I
    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method


# virtual methods
.method protected buildConnectionCapabilities()I
    .locals 2

    .prologue
    .line 183
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->buildConnectionCapabilities()I

    move-result v0

    .line 184
    .local v0, "capabilities":I
    iget-boolean v1, p0, Lcom/android/services/telephony/CdmaConnection;->mAllowMute:Z

    if-eqz v1, :cond_0

    .line 185
    or-int/lit8 v0, v0, 0x40

    .line 188
    :cond_0
    or-int/lit8 v0, v0, 0x2

    .line 189
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->isReallyActive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    or-int/lit8 v0, v0, 0x1

    .line 193
    :cond_1
    return v0
.end method

.method public cloneConnection()Lcom/android/services/telephony/TelephonyConnection;
    .locals 6

    .prologue
    .line 155
    new-instance v0, Lcom/android/services/telephony/CdmaConnection;

    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    .line 156
    iget-object v2, p0, Lcom/android/services/telephony/CdmaConnection;->mEmergencyTonePlayer:Lcom/android/services/telephony/EmergencyTonePlayer;

    iget-boolean v3, p0, Lcom/android/services/telephony/CdmaConnection;->mAllowMute:Z

    iget-boolean v4, p0, Lcom/android/services/telephony/CdmaConnection;->mIsOutgoing:Z

    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getTelecomCallId()Ljava/lang/String;

    move-result-object v5

    .line 155
    invoke-direct/range {v0 .. v5}, Lcom/android/services/telephony/CdmaConnection;-><init>(Lcom/android/internal/telephony/Connection;Lcom/android/services/telephony/EmergencyTonePlayer;ZZLjava/lang/String;)V

    .line 157
    .local v0, "cdmaConnection":Lcom/android/services/telephony/CdmaConnection;
    return-object v0
.end method

.method protected close()V
    .locals 2

    .prologue
    .line 337
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/services/telephony/CdmaConnection;->mIsConnectionTimeReset:Z

    .line 338
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 339
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/CdmaConnection;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/android/internal/telephony/Phone;->unregisterForLineControlInfo(Landroid/os/Handler;)V

    .line 341
    :cond_0
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->close()V

    .line 342
    return-void
.end method

.method forceAsDialing(Z)V
    .locals 1
    .param p1, "isDialing"    # Z

    .prologue
    .line 206
    if-eqz p1, :cond_0

    .line 207
    sget-object v0, Lcom/android/internal/telephony/Call$State;->DIALING:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {p0, v0}, Lcom/android/services/telephony/CdmaConnection;->setStateOverride(Lcom/android/internal/telephony/Call$State;)V

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->resetStateOverride()V

    goto :goto_0
.end method

.method protected handleExitedEcmMode()V
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/services/telephony/CdmaConnection;->mAllowMute:Z

    .line 310
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->handleExitedEcmMode()V

    .line 311
    return-void
.end method

.method isCallWaiting()Z
    .locals 1

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/android/services/telephony/CdmaConnection;->mIsCallWaiting:Z

    return v0
.end method

.method isReallyActive()Z
    .locals 2

    .prologue
    .line 392
    sget-object v0, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getConnectionState()Lcom/android/internal/telephony/Call$State;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/services/telephony/CdmaConnection;->mIsOutgoing:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/services/telephony/CdmaConnection;->mIsConnectionTimeReset:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/services/telephony/CdmaConnection;->isLiveTalkConn:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnswer()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/services/telephony/CdmaConnection;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 143
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->onAnswer()V

    .line 144
    return-void
.end method

.method public onPlayDtmfTone(C)V
    .locals 2
    .param p1, "digit"    # C

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConnection;->useBurstDtmf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string/jumbo v0, "sending dtmf digit as burst"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    invoke-direct {p0, p1}, Lcom/android/services/telephony/CdmaConnection;->sendShortDtmfToNetwork(C)V

    .line 109
    :goto_0
    return-void

    .line 106
    :cond_0
    const-string/jumbo v0, "sending dtmf digit directly"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/telephony/Phone;->startDtmf(C)V

    goto :goto_0
.end method

.method public onReject()V
    .locals 4

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    .line 122
    .local v0, "connection":Lcom/android/internal/telephony/Connection;
    if-eqz v0, :cond_0

    .line 123
    invoke-static {}, Lcom/android/services/telephony/CdmaConnection;->-getcom-android-internal-telephony-Call$StateSwitchesValues()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Call$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 132
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    const-string/jumbo v2, "Rejecting a non-ringing call"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v1, v2, v3}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->onReject()V

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 126
    :pswitch_0
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->onReject()V

    goto :goto_0

    .line 129
    :pswitch_1
    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lcom/android/services/telephony/CdmaConnection;->hangupCallWaiting(I)V

    goto :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onStateChanged(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    const/4 v1, 0x0

    .line 162
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v0

    .line 163
    .local v0, "originalConnection":Lcom/android/internal/telephony/Connection;
    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {v0}, Lcom/android/internal/telephony/Connection;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v2

    sget-object v3, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 163
    :cond_0
    iput-boolean v1, p0, Lcom/android/services/telephony/CdmaConnection;->mIsCallWaiting:Z

    .line 166
    iget-object v1, p0, Lcom/android/services/telephony/CdmaConnection;->mEmergencyTonePlayer:Lcom/android/services/telephony/EmergencyTonePlayer;

    if-eqz v1, :cond_1

    .line 167
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    .line 168
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConnection;->isEmergency()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/android/services/telephony/CdmaConnection;->mEmergencyTonePlayer:Lcom/android/services/telephony/EmergencyTonePlayer;

    invoke-virtual {v1}, Lcom/android/services/telephony/EmergencyTonePlayer;->start()V

    .line 178
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/android/services/telephony/TelephonyConnection;->onStateChanged(I)V

    .line 179
    return-void

    .line 174
    :cond_2
    iget-object v1, p0, Lcom/android/services/telephony/CdmaConnection;->mEmergencyTonePlayer:Lcom/android/services/telephony/EmergencyTonePlayer;

    invoke-virtual {v1}, Lcom/android/services/telephony/EmergencyTonePlayer;->stop()V

    goto :goto_0
.end method

.method public onStopDtmfTone()V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/android/services/telephony/CdmaConnection;->useBurstDtmf()Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/telephony/Phone;->stopDtmf()V

    .line 117
    :cond_0
    return-void
.end method

.method public performConference(Landroid/telecom/Connection;)V
    .locals 2
    .param p1, "otherConnection"    # Landroid/telecom/Connection;

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->isImsConnection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-super {p0, p1}, Lcom/android/services/telephony/TelephonyConnection;->performConference(Landroid/telecom/Connection;)V

    .line 203
    :goto_0
    return-void

    .line 201
    :cond_0
    const-string/jumbo v0, "Non-IMS CDMA Connection attempted to call performConference."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public performHold()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 347
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->isImsConnection()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 348
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->performHold()V

    .line 349
    return-void

    .line 351
    :cond_0
    const-string/jumbo v3, "CDMA: performHold"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 352
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->isReallyActive()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getState()I

    move-result v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_2

    .line 354
    :try_start_0
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    .line 355
    .local v1, "phone":Lcom/android/internal/telephony/Phone;
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->getRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v2

    .line 356
    .local v2, "ringingCall":Lcom/android/internal/telephony/Call;
    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->getState()Lcom/android/internal/telephony/Call$State;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/Call$State;->WAITING:Lcom/android/internal/telephony/Call$State;

    if-eq v3, v4, :cond_1

    .line 357
    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V

    .line 358
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->setOnHold()V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    .end local v1    # "phone":Lcom/android/internal/telephony/Phone;
    .end local v2    # "ringingCall":Lcom/android/internal/telephony/Call;
    :cond_1
    :goto_0
    return-void

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v3, "Exception occurred while trying to put call on hold."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v0, v3, v4}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 364
    .end local v0    # "e":Lcom/android/internal/telephony/CallStateException;
    :cond_2
    const-string/jumbo v3, "Cannot put a call that is not currently active on hold."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public performUnhold()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 370
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->isImsConnection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 371
    invoke-super {p0}, Lcom/android/services/telephony/TelephonyConnection;->performUnhold()V

    .line 372
    return-void

    .line 374
    :cond_0
    const-string/jumbo v1, "CDMA: performUnhold"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 375
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->isReallyActive()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 377
    :try_start_0
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->hasMultipleTopLevelCalls()Z

    move-result v1

    if-nez v1, :cond_1

    .line 378
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getOriginalConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Connection;->getCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Phone;->switchHoldingAndActive()V

    .line 379
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->setActiveInternal()V

    .line 389
    :goto_0
    return-void

    .line 381
    :cond_1
    const-string/jumbo v1, "Skipping unhold command for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 383
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Lcom/android/internal/telephony/CallStateException;
    const-string/jumbo v1, "Exception occurred while trying to release call from hold."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v2}, Lcom/android/services/telephony/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 387
    .end local v0    # "e":Lcom/android/internal/telephony/CallStateException;
    :cond_2
    const-string/jumbo v1, "Cannot release a call that is not already on hold from hold."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p0, v1, v2}, Lcom/android/services/telephony/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method setOriginalConnection(Lcom/android/internal/telephony/Connection;)V
    .locals 4
    .param p1, "originalConnection"    # Lcom/android/internal/telephony/Connection;

    .prologue
    const/4 v3, 0x0

    .line 329
    invoke-super {p0, p1}, Lcom/android/services/telephony/TelephonyConnection;->setOriginalConnection(Lcom/android/internal/telephony/Connection;)V

    .line 330
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {p0}, Lcom/android/services/telephony/CdmaConnection;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iget-object v1, p0, Lcom/android/services/telephony/CdmaConnection;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/telephony/Phone;->registerForLineControlInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 333
    :cond_0
    return-void
.end method
