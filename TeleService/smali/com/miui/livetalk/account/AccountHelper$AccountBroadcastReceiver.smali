.class Lcom/miui/livetalk/account/AccountHelper$AccountBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AccountHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/livetalk/account/AccountHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AccountBroadcastReceiver"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/livetalk/account/AccountHelper$AccountBroadcastReceiver;)V
    .locals 0
    .param p1, "-this0"    # Lcom/miui/livetalk/account/AccountHelper$AccountBroadcastReceiver;

    .prologue
    invoke-direct {p0}, Lcom/miui/livetalk/account/AccountHelper$AccountBroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 60
    const-string/jumbo v4, "AccountHelper"

    const-string/jumbo v5, "onReceive: %s."

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    if-nez p2, :cond_0

    .line 62
    return-void

    .line 64
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "action":Ljava/lang/String;
    const-string/jumbo v4, "android.accounts.LOGIN_ACCOUNTS_POST_CHANGED"

    invoke-static {v1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 66
    const-string/jumbo v4, "extra_update_type"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 67
    .local v3, "type":I
    const-string/jumbo v4, "extra_account"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 68
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_1

    iget-object v4, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string/jumbo v5, "com.xiaomi"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    .line 69
    :cond_1
    const-string/jumbo v4, "AccountHelper"

    const-string/jumbo v5, "It isn\'t a xiaomi account changed."

    invoke-static {v4, v5}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void

    .line 73
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 74
    .local v2, "appContext":Landroid/content/Context;
    if-ne v3, v7, :cond_4

    .line 76
    invoke-static {v2, v0}, Lcom/miui/livetalk/account/AccountHelper;->onXiaomiAccountLogout(Landroid/content/Context;Landroid/accounts/Account;)V

    .line 84
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "appContext":Landroid/content/Context;
    .end local v3    # "type":I
    :cond_3
    :goto_0
    return-void

    .line 77
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v2    # "appContext":Landroid/content/Context;
    .restart local v3    # "type":I
    :cond_4
    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 79
    invoke-static {v2, v0}, Lcom/miui/livetalk/account/AccountHelper;->onXiaomiAccountLogin(Landroid/content/Context;Landroid/accounts/Account;)V

    goto :goto_0

    .line 81
    :cond_5
    const-string/jumbo v4, "AccountHelper"

    const-string/jumbo v5, "Xiaomi account changed, but unknown type: %s."

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/miui/livetalk/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
