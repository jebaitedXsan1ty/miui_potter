.class Lcom/miui/livetalk/phone/PhoneCall$1;
.super Ljava/lang/Object;
.source "PhoneCall.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/livetalk/phone/PhoneCall;->endCall(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/livetalk/phone/PhoneCall;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/miui/livetalk/phone/PhoneCall;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/livetalk/phone/PhoneCall;

    .prologue
    .line 1
    iput-object p1, p0, Lcom/miui/livetalk/phone/PhoneCall$1;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    iput-object p2, p0, Lcom/miui/livetalk/phone/PhoneCall$1;->val$context:Landroid/content/Context;

    .line 378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 381
    const-string/jumbo v1, "PhoneCall"

    const-string/jumbo v2, "endCall()"

    invoke-static {v1, v2}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x14

    if-le v1, v2, :cond_0

    .line 384
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.miui.livetalk.HANG_UP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 388
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    iget-object v1, p0, Lcom/miui/livetalk/phone/PhoneCall$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 389
    return-void

    .line 386
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.android.incallui.ACTION_HANG_UP_ONGOING_CALL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method
