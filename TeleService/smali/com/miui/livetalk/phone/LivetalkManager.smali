.class public Lcom/miui/livetalk/phone/LivetalkManager;
.super Ljava/lang/Object;
.source "LivetalkManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/livetalk/phone/LivetalkManager$Holder;
    }
.end annotation


# static fields
.field public static ROM_SUPPORT:Z


# instance fields
.field private mInit:Z

.field private mLivetalkClientSupport:Z

.field private mPhoneSupport:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    .line 14
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/miui/livetalk/phone/LivetalkManager;->mInit:Z

    .line 30
    iput-boolean v0, p0, Lcom/miui/livetalk/phone/LivetalkManager;->mPhoneSupport:Z

    .line 35
    iput-boolean v0, p0, Lcom/miui/livetalk/phone/LivetalkManager;->mLivetalkClientSupport:Z

    .line 47
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/livetalk/phone/LivetalkManager;)V
    .locals 0
    .param p1, "-this0"    # Lcom/miui/livetalk/phone/LivetalkManager;

    .prologue
    invoke-direct {p0}, Lcom/miui/livetalk/phone/LivetalkManager;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/miui/livetalk/phone/LivetalkManager;
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lcom/miui/livetalk/phone/LivetalkManager$Holder;->-get0()Lcom/miui/livetalk/phone/LivetalkManager;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    monitor-enter p0

    .line 50
    :try_start_0
    iget-boolean v3, p0, Lcom/miui/livetalk/phone/LivetalkManager;->mInit:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    monitor-exit p0

    .line 51
    return-void

    .line 55
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v4, "com.android.phone"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 56
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 57
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 58
    const-string/jumbo v3, "support_livetalk"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/miui/livetalk/phone/LivetalkManager;->mPhoneSupport:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    .end local v0    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_1
    :goto_0
    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string/jumbo v4, "com.miui.milivetalk"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 66
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/miui/livetalk/phone/LivetalkManager;->mLivetalkClientSupport:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 71
    :goto_1
    const/4 v3, 0x1

    :try_start_3
    iput-boolean v3, p0, Lcom/miui/livetalk/phone/LivetalkManager;->mInit:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    .line 72
    return-void

    .line 60
    :catch_0
    move-exception v2

    .line 61
    .local v2, "e":Ljava/lang/Exception;
    :try_start_4
    const-string/jumbo v3, "LivetalkManager"

    const-string/jumbo v4, "init phone support"

    invoke-static {v3, v4, v2}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 67
    :catch_1
    move-exception v2

    .line 68
    .restart local v2    # "e":Ljava/lang/Exception;
    :try_start_5
    const-string/jumbo v3, "LivetalkManager"

    const-string/jumbo v4, "init client support: not support"

    invoke-static {v3, v4}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public static isNetworkConnected(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    const-string/jumbo v3, "connectivity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 136
    .local v0, "connMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 137
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    .line 138
    :goto_0
    if-nez v1, :cond_0

    .line 139
    const-string/jumbo v3, "LivetalkManager"

    const-string/jumbo v4, "network not connected"

    invoke-static {v3, v4}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_0
    return v1

    .line 137
    :cond_1
    const/4 v1, 0x0

    .local v1, "connected":Z
    goto :goto_0
.end method

.method private serviceAvailable(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 75
    sget-boolean v1, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-nez v1, :cond_0

    .line 76
    return v0

    .line 79
    :cond_0
    invoke-static {p1}, Lcom/miui/livetalk/LivetalkSettings;->getPhoneSupportStatus(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 80
    const-string/jumbo v1, "LivetalkManager"

    const-string/jumbo v2, "getPhoneSupportStatus is false"

    invoke-static {v1, v2}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    return v0

    .line 84
    :cond_1
    invoke-static {p1}, Lcom/miui/livetalk/LivetalkSettings;->isServiceAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 85
    const-string/jumbo v1, "LivetalkManager"

    const-string/jumbo v2, "livetalk service status not available"

    invoke-static {v1, v2}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    return v0

    .line 89
    :cond_2
    invoke-direct {p0, p1}, Lcom/miui/livetalk/phone/LivetalkManager;->init(Landroid/content/Context;)V

    .line 90
    iget-boolean v1, p0, Lcom/miui/livetalk/phone/LivetalkManager;->mPhoneSupport:Z

    if-eqz v1, :cond_3

    iget-boolean v0, p0, Lcom/miui/livetalk/phone/LivetalkManager;->mLivetalkClientSupport:Z

    :cond_3
    return v0
.end method

.method private userActivatedService(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 94
    const-string/jumbo v2, "livetalk_enabled"

    invoke-static {p1, v2, v1}, Lcom/miui/livetalk/LivetalkSettings;->getSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private userEnabledLivetalkCall(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 98
    const-string/jumbo v2, "livetalk_switch_state"

    invoke-static {p1, v2, v1}, Lcom/miui/livetalk/LivetalkSettings;->getSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public activatedLivetalkService(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 102
    invoke-direct {p0, p1}, Lcom/miui/livetalk/phone/LivetalkManager;->serviceAvailable(Landroid/content/Context;)Z

    move-result v0

    .line 103
    .local v0, "isServiceAvailable":Z
    if-nez v0, :cond_0

    .line 104
    return v6

    .line 107
    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/livetalk/phone/LivetalkManager;->userActivatedService(Landroid/content/Context;)Z

    move-result v1

    .line 108
    .local v1, "userActivated":Z
    const-string/jumbo v2, "LivetalkManager"

    const-string/jumbo v3, "activatedLivetalkService(): %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    return v1
.end method

.method public enabledLivetalkCall(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 118
    invoke-virtual {p0, p1}, Lcom/miui/livetalk/phone/LivetalkManager;->activatedLivetalkService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    const-string/jumbo v0, "LivetalkManager"

    const-string/jumbo v1, "enabledLivetalkCall()\uff1a activatedLivetalkService is false"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return v2

    .line 121
    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/livetalk/phone/LivetalkManager;->userEnabledLivetalkCall(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122
    const-string/jumbo v0, "LivetalkManager"

    const-string/jumbo v1, "enabledLivetalkCall()\uff1a userEnabledLivetalkCall is false"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    return v2

    .line 124
    :cond_1
    invoke-static {p1}, Lcom/miui/livetalk/LivetalkSettings;->getTotalRemainMins(Landroid/content/Context;)I

    move-result v0

    if-gtz v0, :cond_2

    .line 125
    const-string/jumbo v0, "LivetalkManager"

    const-string/jumbo v1, "enabledLivetalkCall()\uff1aremainMins <= 0"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    return v2

    .line 128
    :cond_2
    const-string/jumbo v0, "LivetalkManager"

    const-string/jumbo v1, "enabledLivetalkCall(): true"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const/4 v0, 0x1

    return v0
.end method
