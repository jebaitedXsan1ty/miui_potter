.class Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;
.super Ljava/lang/Object;
.source "PhoneCall.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/livetalk/phone/PhoneCall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhoneCallConn"
.end annotation


# instance fields
.field protected mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

.field final synthetic this$0:Lcom/miui/livetalk/phone/PhoneCall;


# direct methods
.method private constructor <init>(Lcom/miui/livetalk/phone/PhoneCall;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/livetalk/phone/PhoneCall;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/livetalk/phone/PhoneCall;Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/livetalk/phone/PhoneCall;
    .param p2, "-this1"    # Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;

    .prologue
    invoke-direct {p0, p1}, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;-><init>(Lcom/miui/livetalk/phone/PhoneCall;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 9
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    const/4 v8, 0x1

    .line 99
    const-string/jumbo v0, "PhoneCallConn"

    const-string/jumbo v2, "onServiceConnected()"

    invoke-static {v0, v2}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {p2}, Lcom/miui/livetalk/phone/IPhoneCallService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/miui/livetalk/phone/IPhoneCallService;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->mPhoneCallService:Lcom/miui/livetalk/phone/IPhoneCallService;

    .line 101
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v0, v8}, Lcom/miui/livetalk/phone/PhoneCall;->-set0(Lcom/miui/livetalk/phone/PhoneCall;Z)Z

    .line 102
    invoke-static {}, Lcom/miui/livetalk/LivetalkApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 103
    .local v1, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v0, v1}, Lcom/miui/livetalk/phone/PhoneCall;->-wrap2(Lcom/miui/livetalk/phone/PhoneCall;Landroid/content/Context;)V

    .line 104
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v0}, Lcom/miui/livetalk/phone/PhoneCall;->-get0(Lcom/miui/livetalk/phone/PhoneCall;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    const/4 v7, 0x0

    .line 107
    .local v7, "useLivetalkCall":Z
    :try_start_0
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    iget-object v2, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v2}, Lcom/miui/livetalk/phone/PhoneCall;->-get2(Lcom/miui/livetalk/phone/PhoneCall;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v3}, Lcom/miui/livetalk/phone/PhoneCall;->-get4(Lcom/miui/livetalk/phone/PhoneCall;)I

    move-result v3

    iget-object v4, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v4}, Lcom/miui/livetalk/phone/PhoneCall;->-get3(Lcom/miui/livetalk/phone/PhoneCall;)I

    move-result v4

    iget-object v5, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v5}, Lcom/miui/livetalk/phone/PhoneCall;->-get1(Lcom/miui/livetalk/phone/PhoneCall;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/miui/livetalk/phone/PhoneCall;->-wrap0(Lcom/miui/livetalk/phone/PhoneCall;Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 111
    .end local v7    # "useLivetalkCall":Z
    :goto_0
    if-nez v7, :cond_0

    .line 112
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v0, v1}, Lcom/miui/livetalk/phone/PhoneCall;->-wrap1(Lcom/miui/livetalk/phone/PhoneCall;Landroid/content/Context;)V

    .line 113
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v0, v8}, Lcom/miui/livetalk/phone/PhoneCall;->-set2(Lcom/miui/livetalk/phone/PhoneCall;Z)Z

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/miui/livetalk/phone/PhoneCall;->-set1(Lcom/miui/livetalk/phone/PhoneCall;Z)Z

    .line 117
    :cond_1
    return-void

    .line 108
    .restart local v7    # "useLivetalkCall":Z
    :catch_0
    move-exception v6

    .line 109
    .local v6, "e":Ljava/lang/Exception;
    const-string/jumbo v0, "PhoneCallConn"

    const-string/jumbo v2, "useLivetalkCall"

    invoke-static {v0, v2, v6}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 121
    const-string/jumbo v0, "PhoneCallConn"

    const-string/jumbo v1, "onServiceDisconnected()"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/miui/livetalk/phone/PhoneCall$PhoneCallConn;->this$0:Lcom/miui/livetalk/phone/PhoneCall;

    invoke-static {v0}, Lcom/miui/livetalk/phone/PhoneCall;->-wrap3(Lcom/miui/livetalk/phone/PhoneCall;)V

    .line 123
    return-void
.end method
