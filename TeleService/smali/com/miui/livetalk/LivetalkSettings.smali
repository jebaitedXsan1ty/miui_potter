.class public Lcom/miui/livetalk/LivetalkSettings;
.super Ljava/lang/Object;
.source "LivetalkSettings.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getOldDialEnableRange(Landroid/content/Context;)Landroid/util/Pair;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 373
    const-string/jumbo v2, "internal_dial_enable"

    invoke-static {p0, v2, v3}, Lcom/miui/livetalk/LivetalkSettings;->getSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    .line 374
    .local v0, "oldInland":Z
    :goto_0
    const-string/jumbo v2, "international_dial_enable"

    invoke-static {p0, v2, v3}, Lcom/miui/livetalk/LivetalkSettings;->getSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v3, :cond_1

    const/4 v1, 0x1

    .line 375
    .local v1, "oldInternational":Z
    :goto_1
    new-instance v2, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 373
    .end local v0    # "oldInland":Z
    .end local v1    # "oldInternational":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "oldInland":Z
    goto :goto_0

    .line 374
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "oldInternational":Z
    goto :goto_1
.end method

.method public static getPhoneSupportStatus(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    const-string/jumbo v0, "livetalk_available_status"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/miui/livetalk/LivetalkSettings;->getSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 23
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 24
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-static {v0, p1, p2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getTotalRemainMins(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 352
    const-string/jumbo v0, "livetalk_remain_minutes"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/miui/livetalk/LivetalkSettings;->getSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static isServiceAvailable(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 138
    const-string/jumbo v1, "livetalk_service_status"

    invoke-static {p0, v1, v0}, Lcom/miui/livetalk/LivetalkSettings;->getSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUserActivated(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 156
    const-string/jumbo v2, "livetalk_enabled"

    invoke-static {p0, v2, v1}, Lcom/miui/livetalk/LivetalkSettings;->getSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static setCurrentMiAccountStatus(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "currentAccount"    # Z

    .prologue
    .line 196
    const-string/jumbo v1, "livetalk_use_current_account"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v1, v0}, Lcom/miui/livetalk/LivetalkSettings;->setSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 197
    return-void

    .line 196
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setDefaultAreaCode(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "areaCode"    # Ljava/lang/String;

    .prologue
    .line 393
    const-string/jumbo v0, "defaultAreaCode"

    invoke-static {p0, v0, p1}, Lcom/miui/livetalk/LivetalkSettings;->setSystemSettingsString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    return-void
.end method

.method public static setDialRange(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # I

    .prologue
    .line 361
    const-string/jumbo v0, "livetalk_dial_range"

    invoke-static {p0, v0, p1}, Lcom/miui/livetalk/LivetalkSettings;->setSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 362
    return-void
.end method

.method public static setPhoneSupportStatus(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "status"    # I

    .prologue
    .line 125
    const-string/jumbo v0, "livetalk_available_status"

    invoke-static {p0, v0, p1}, Lcom/miui/livetalk/LivetalkSettings;->setSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 126
    return-void
.end method

.method private static setSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 28
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 29
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-static {v0, p1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 30
    return-void
.end method

.method private static setSystemSettingsString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 39
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-static {v0, p1, p2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 40
    return-void
.end method

.method public static setUserEnableDial(Landroid/content/Context;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enable"    # Z

    .prologue
    .line 178
    const-string/jumbo v1, "livetalk_switch_state"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p0, v1, v0}, Lcom/miui/livetalk/LivetalkSettings;->setSystemSettingsInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 179
    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
