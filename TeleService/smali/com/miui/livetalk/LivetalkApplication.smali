.class public Lcom/miui/livetalk/LivetalkApplication;
.super Ljava/lang/Object;
.source "LivetalkApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/livetalk/LivetalkApplication$DelayedInitializer;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/phone/PhoneGlobals;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private static initService()V
    .locals 4

    .prologue
    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 63
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/miui/livetalk/LivetalkApplication$1;

    invoke-direct {v1}, Lcom/miui/livetalk/LivetalkApplication$1;-><init>()V

    .line 68
    const-wide/32 v2, 0x2bf20

    .line 63
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 69
    const-string/jumbo v1, "LivetalkApplication"

    const-string/jumbo v2, "initService() end"

    invoke-static {v1, v2}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method private static migrateUserData(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 77
    invoke-static {p0}, Lcom/miui/livetalk/LivetalkPrefs;->isMigrateSwitchState(Landroid/content/Context;)Z

    move-result v1

    .line 78
    .local v1, "hasMigrated":Z
    if-nez v1, :cond_3

    .line 79
    const-string/jumbo v5, "LivetalkApplication"

    const-string/jumbo v6, "start migrateUserData"

    invoke-static {v5, v6}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-static {p0}, Lcom/miui/livetalk/LivetalkSettings;->isUserActivated(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 82
    invoke-static {p0, v7}, Lcom/miui/livetalk/LivetalkSettings;->setCurrentMiAccountStatus(Landroid/content/Context;Z)V

    .line 86
    :cond_0
    invoke-static {p0}, Lcom/miui/livetalk/LivetalkSettings;->getOldDialEnableRange(Landroid/content/Context;)Landroid/util/Pair;

    move-result-object v0

    .line 87
    .local v0, "enableDialRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Boolean;>;"
    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 88
    .local v2, "oldInlandState":Z
    iget-object v5, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 89
    .local v3, "oldInternationalState":Z
    if-nez v2, :cond_1

    if-eqz v3, :cond_6

    .line 90
    :cond_1
    invoke-static {p0, v7}, Lcom/miui/livetalk/LivetalkSettings;->setUserEnableDial(Landroid/content/Context;Z)V

    .line 95
    :goto_0
    if-eqz v2, :cond_7

    if-eqz v3, :cond_7

    .line 96
    invoke-static {p0, v9}, Lcom/miui/livetalk/LivetalkSettings;->setDialRange(Landroid/content/Context;I)V

    .line 104
    :cond_2
    :goto_1
    invoke-static {p0}, Lcom/miui/livetalk/LivetalkPrefs;->setSwitchStateMigrated(Landroid/content/Context;)V

    .line 107
    .end local v0    # "enableDialRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Boolean;>;"
    .end local v2    # "oldInlandState":Z
    .end local v3    # "oldInternationalState":Z
    :cond_3
    invoke-static {p0}, Lcom/miui/livetalk/LivetalkPrefs;->getAppVersion(Landroid/content/Context;)I

    move-result v4

    .line 108
    .local v4, "oldVersion":I
    if-ge v4, v7, :cond_4

    .line 109
    invoke-static {p0}, Lcom/miui/livetalk/LivetalkApplication;->upgradeToVersion1(Landroid/content/Context;)V

    .line 112
    :cond_4
    if-ge v4, v8, :cond_5

    .line 113
    invoke-static {p0}, Lcom/miui/livetalk/LivetalkApplication;->upgradeToVersion2(Landroid/content/Context;)V

    .line 116
    :cond_5
    invoke-static {p0, v8}, Lcom/miui/livetalk/LivetalkPrefs;->setAppVersion(Landroid/content/Context;I)V

    .line 117
    return-void

    .line 92
    .end local v4    # "oldVersion":I
    .restart local v0    # "enableDialRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Boolean;>;"
    .restart local v2    # "oldInlandState":Z
    .restart local v3    # "oldInternationalState":Z
    :cond_6
    invoke-static {p0, v9}, Lcom/miui/livetalk/LivetalkSettings;->setUserEnableDial(Landroid/content/Context;Z)V

    goto :goto_0

    .line 97
    :cond_7
    if-eqz v2, :cond_8

    .line 98
    invoke-static {p0, v7}, Lcom/miui/livetalk/LivetalkSettings;->setDialRange(Landroid/content/Context;I)V

    goto :goto_1

    .line 99
    :cond_8
    if-eqz v3, :cond_2

    .line 100
    invoke-static {p0, v8}, Lcom/miui/livetalk/LivetalkSettings;->setDialRange(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method public static onCreateApplication()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 31
    const-string/jumbo v3, "LivetalkApplication"

    const-string/jumbo v6, "onApplicationCreate"

    invoke-static {v3, v6}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    sget-boolean v3, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-nez v3, :cond_0

    .line 33
    const-string/jumbo v3, "LivetalkApplication"

    const-string/jumbo v6, "livetalk rom not support"

    invoke-static {v3, v6}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    return-void

    .line 36
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 40
    .local v4, "startTime":J
    :try_start_0
    invoke-static {}, Lcom/miui/livetalk/util/SystemPropertiesUtil;->isSystemEncryptingState()Z

    move-result v2

    .line 41
    .local v2, "onlyCore":Z
    if-nez v2, :cond_1

    .line 42
    invoke-static {}, Lcom/miui/livetalk/LivetalkApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 43
    .local v0, "appContext":Landroid/content/Context;
    invoke-static {v0}, Lcom/miui/livetalk/LivetalkSettings;->isUserActivated(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 45
    invoke-static {v0}, Lcom/miui/livetalk/LivetalkPrefs;->setSwitchStateMigrated(Landroid/content/Context;)V

    .line 46
    const/4 v3, 0x0

    invoke-static {v0, v3}, Lcom/miui/livetalk/LivetalkPrefs;->setNeedSyncSettings(Landroid/content/Context;Z)V

    .line 47
    const/4 v3, 0x2

    invoke-static {v0, v3}, Lcom/miui/livetalk/LivetalkPrefs;->setAppVersion(Landroid/content/Context;I)V

    .line 53
    :goto_0
    invoke-static {}, Lcom/miui/livetalk/LivetalkApplication;->initService()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .end local v0    # "appContext":Landroid/content/Context;
    .end local v2    # "onlyCore":Z
    :cond_1
    :goto_1
    const-string/jumbo v3, "LivetalkApplication"

    const-string/jumbo v6, "onApplicationCreate use %s ms"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void

    .line 50
    .restart local v0    # "appContext":Landroid/content/Context;
    .restart local v2    # "onlyCore":Z
    :cond_2
    :try_start_1
    invoke-static {v0}, Lcom/miui/livetalk/LivetalkApplication;->migrateUserData(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 55
    .end local v0    # "appContext":Landroid/content/Context;
    .end local v2    # "onlyCore":Z
    :catch_0
    move-exception v1

    .line 56
    .local v1, "ex":Ljava/lang/Exception;
    const-string/jumbo v3, "LivetalkApplication"

    const-string/jumbo v6, "onApplicationCreate"

    invoke-static {v3, v6, v1}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static upgradeToVersion1(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 120
    const-string/jumbo v3, "LivetalkApplication"

    const-string/jumbo v4, "upgradeToVersion1"

    invoke-static {v3, v4}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 123
    .local v1, "spf":Landroid/content/SharedPreferences;
    const-string/jumbo v3, "defaultAreaCode"

    const-string/jumbo v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "spfAreaCode":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 125
    invoke-static {p0, v2}, Lcom/miui/livetalk/LivetalkSettings;->setDefaultAreaCode(Landroid/content/Context;Ljava/lang/String;)V

    .line 126
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 127
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v3, "defaultAreaCode"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 128
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 130
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method private static upgradeToVersion2(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    const-string/jumbo v0, "LivetalkApplication"

    const-string/jumbo v1, "upgradeToVersion2"

    invoke-static {v0, v1}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-static {p0}, Lcom/miui/livetalk/LivetalkPrefs;->cacheGeneralSettings(Landroid/content/Context;)V

    .line 136
    return-void
.end method
