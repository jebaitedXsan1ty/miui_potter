.class public Lcom/miui/livetalk/ui/LivetalkRedirectActivity;
.super Lmiui/app/Activity;
.source "LivetalkRedirectActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lmiui/app/Activity;-><init>()V

    return-void
.end method

.method private redirectIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "oldIntent"    # Landroid/content/Intent;
    .param p3, "newAction"    # Ljava/lang/String;
    .param p4, "newUri"    # Landroid/net/Uri;

    .prologue
    .line 70
    const-string/jumbo v4, "LivetalkRedirect"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "old intent: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/miui/livetalk/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .local v3, "newIntent":Landroid/content/Intent;
    if-eqz p4, :cond_0

    .line 73
    invoke-virtual {v3, p4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 75
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getFlags()I

    move-result v2

    .line 76
    .local v2, "flags":I
    invoke-virtual {v3, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 77
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 78
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 79
    invoke-virtual {v3, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 82
    :cond_1
    :try_start_0
    const-string/jumbo v4, "LivetalkRedirect"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "new intent: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/miui/livetalk/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_0
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "LivetalkRedirect"

    const-string/jumbo v5, "redirectIntent: "

    invoke-static {v4, v5, v0}, Lcom/miui/livetalk/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private resolveIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "oldIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 37
    if-nez p2, :cond_0

    .line 38
    const-string/jumbo v5, "LivetalkRedirect"

    const-string/jumbo v6, "receive null oldIntent"

    invoke-static {v5, v6}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->finish()V

    .line 40
    return-void

    .line 42
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "action":Ljava/lang/String;
    const-string/jumbo v5, "com.miui.livetalk.MY_LIVETALK_VIEW"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 44
    const-string/jumbo v5, "LivetalkRedirect"

    const-string/jumbo v6, "receive main action"

    invoke-static {v5, v6}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string/jumbo v5, "com.miui.milivetalk.HOME"

    invoke-direct {p0, p1, p2, v5, v7}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->redirectIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/net/Uri;)V

    .line 66
    :goto_0
    invoke-virtual {p0}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->finish()V

    .line 67
    return-void

    .line 46
    :cond_1
    const-string/jumbo v5, "com.miui.livetalk.PURCHASE_VIEW"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 47
    const-string/jumbo v5, "LivetalkRedirect"

    const-string/jumbo v6, "receive purchase action"

    invoke-static {v5, v6}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string/jumbo v5, "com.miui.milivetalk.PURCHASE"

    invoke-direct {p0, p1, p2, v5, v7}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->redirectIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 49
    :cond_2
    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 50
    const-string/jumbo v5, "LivetalkRedirect"

    const-string/jumbo v6, "receive view action"

    invoke-static {v5, v6}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 52
    .local v4, "uri":Landroid/net/Uri;
    if-eqz v4, :cond_3

    .line 53
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    .line 54
    .local v3, "scheme":Ljava/lang/String;
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "path":Ljava/lang/String;
    const-string/jumbo v5, "%s://%s%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const-string/jumbo v7, "milivetalk.miui.com"

    const/4 v8, 0x1

    aput-object v7, v6, v8

    const/4 v7, 0x2

    aput-object v2, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 56
    .local v1, "newUri":Landroid/net/Uri;
    const-string/jumbo v5, "android.intent.action.VIEW"

    invoke-direct {p0, p1, p2, v5, v1}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->redirectIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 58
    .end local v1    # "newUri":Landroid/net/Uri;
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "scheme":Ljava/lang/String;
    :cond_3
    const-string/jumbo v5, "LivetalkRedirect"

    const-string/jumbo v6, "receive null uri"

    invoke-static {v5, v6}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string/jumbo v5, "com.miui.milivetalk.HOME"

    invoke-direct {p0, p1, p2, v5, v7}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->redirectIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    .line 62
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_4
    const-string/jumbo v5, "LivetalkRedirect"

    const-string/jumbo v6, "receive invalid oldIntent"

    invoke-static {v5, v6}, Lcom/miui/livetalk/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string/jumbo v5, "com.miui.milivetalk.HOME"

    invoke-direct {p0, p1, p2, v5, v7}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->redirectIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Lmiui/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p0, v0}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->resolveIntent(Landroid/content/Context;Landroid/content/Intent;)V

    .line 27
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->setIntent(Landroid/content/Intent;)V

    .line 32
    invoke-super {p0, p1}, Lmiui/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 33
    invoke-direct {p0, p0, p1}, Lcom/miui/livetalk/ui/LivetalkRedirectActivity;->resolveIntent(Landroid/content/Context;Landroid/content/Intent;)V

    .line 34
    return-void
.end method
