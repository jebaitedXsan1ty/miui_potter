.class public Lcom/miui/livetalk/dial/LivetalkCall;
.super Ljava/lang/Object;
.source "LivetalkCall.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isCallbackNumber(Ljava/lang/String;)Z
    .locals 1
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    .line 61
    sget-boolean v0, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/miui/livetalk/phone/PhoneCall;->getInstance()Lcom/miui/livetalk/phone/PhoneCall;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/livetalk/phone/PhoneCall;->isCallBackNumber(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static onBackCallAccepted()V
    .locals 1

    .prologue
    .line 54
    sget-boolean v0, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-nez v0, :cond_0

    .line 55
    return-void

    .line 57
    :cond_0
    invoke-static {}, Lcom/miui/livetalk/phone/PhoneCall;->getInstance()Lcom/miui/livetalk/phone/PhoneCall;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/livetalk/phone/PhoneCall;->onBackCallAccepted()V

    .line 58
    return-void
.end method

.method public static onTeleDisconnect(ZLjava/lang/String;JIZJJJ)V
    .locals 14
    .param p0, "isLivetalkConn"    # Z
    .param p1, "callOutNumber"    # Ljava/lang/String;
    .param p2, "durationMillis"    # J
    .param p4, "slotId"    # I
    .param p5, "isIncoming"    # Z
    .param p6, "connectTime"    # J
    .param p8, "disconnectTime"    # J
    .param p10, "startTime"    # J

    .prologue
    .line 46
    sget-boolean v0, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-nez v0, :cond_0

    .line 47
    return-void

    .line 49
    :cond_0
    invoke-static {}, Lcom/miui/livetalk/phone/PhoneCall;->getInstance()Lcom/miui/livetalk/phone/PhoneCall;

    move-result-object v1

    move v2, p0

    move-object v3, p1

    move-wide/from16 v4, p2

    move/from16 v6, p4

    move/from16 v7, p5

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    move-wide/from16 v12, p10

    invoke-virtual/range {v1 .. v13}, Lcom/miui/livetalk/phone/PhoneCall;->onPhoneCallHangup(ZLjava/lang/String;JIZJJJ)V

    .line 51
    return-void
.end method

.method public static useLivetalkCall(Landroid/content/Context;Ljava/lang/String;ILandroid/os/Bundle;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "slotId"    # I
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 24
    sget-boolean v2, Lcom/miui/livetalk/phone/LivetalkManager;->ROM_SUPPORT:Z

    if-nez v2, :cond_0

    .line 25
    return v4

    .line 28
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    if-nez p3, :cond_2

    .line 29
    :cond_1
    const-string/jumbo v2, "LivetalkCall"

    const-string/jumbo v3, "useLivetalkCall(): number is null or bundle is null."

    invoke-static {v2, v3}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return v4

    .line 34
    :cond_2
    const-string/jumbo v2, "only_regular_call"

    invoke-virtual {p3, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 35
    .local v0, "isOnlyRegular":Z
    if-eqz v0, :cond_3

    .line 36
    const-string/jumbo v2, "LivetalkCall"

    const-string/jumbo v3, "useLivetalkCall(): false, only regular"

    invoke-static {v2, v3}, Lcom/miui/livetalk/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    return v4

    .line 40
    :cond_3
    const-string/jumbo v2, "android.phone.extra.CONTACT_NAME"

    const-string/jumbo v3, ""

    invoke-virtual {p3, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "name":Ljava/lang/String;
    invoke-static {}, Lcom/miui/livetalk/phone/PhoneCall;->getInstance()Lcom/miui/livetalk/phone/PhoneCall;

    move-result-object v2

    invoke-virtual {v2, p0, p1, p2, v1}, Lcom/miui/livetalk/phone/PhoneCall;->useLivetalkCall(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v2

    return v2
.end method
