.class Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get10(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    const/4 v9, 0x1

    :goto_0
    if-eqz v9, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get14(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v12

    :goto_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0046

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get16(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v1

    if-eqz v9, :cond_0

    neg-int v10, v10

    :cond_0
    const/4 v2, 0x0

    invoke-static {v0, v1, v10, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap15(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/view/View;IZ)V

    return-void

    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get10(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v12

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-set0(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;Z)Z

    if-eqz v12, :cond_4

    new-instance v11, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x1

    invoke-direct {v11, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    if-eqz v9, :cond_6

    const/high16 v4, 0x40000000    # 2.0f

    :goto_2
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v0, 0x12c

    invoke-virtual {v11, v0, v1}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$1;

    invoke-direct {v0, p0, v12}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;Landroid/view/View;)V

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v12, v11}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_4
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v1

    if-eqz v9, :cond_7

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_3
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/UnlockHintSwitcher;->clearAnimation()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v1

    if-eqz v9, :cond_5

    neg-int v10, v10

    :cond_5
    const/4 v2, 0x0

    invoke-static {v0, v1, v10, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap15(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/view/View;IZ)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;

    invoke-direct {v1, p0, p1, v12, v9}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3$2;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;Landroid/view/View;Landroid/view/View;Z)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_6
    const/high16 v4, -0x40000000    # -2.0f

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get34(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_3
.end method
