.class final Lcom/android/keyguard/LockPatternChecker$2;
.super Landroid/os/AsyncTask;
.source "LockPatternChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/LockPatternChecker;->checkPasswordForUsers(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/lang/String;Landroid/content/Context;Lcom/android/keyguard/OnCheckForUsersCallback;)Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mThrottleTimeout:I

.field private mUserIdMatched:I

.field final synthetic val$callback:Lcom/android/keyguard/OnCheckForUsersCallback;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$password:Ljava/lang/String;

.field final synthetic val$utils:Lcom/android/keyguard/LockPatternUtilsWrapper;


# direct methods
.method constructor <init>(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/lang/String;Lcom/android/keyguard/OnCheckForUsersCallback;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$utils:Lcom/android/keyguard/LockPatternUtilsWrapper;

    iput-object p2, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$password:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$callback:Lcom/android/keyguard/OnCheckForUsersCallback;

    iput-object p4, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/16 v0, -0x2710

    iput v0, p0, Lcom/android/keyguard/LockPatternChecker$2;->mUserIdMatched:I

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 10

    const/4 v9, 0x0

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v0

    :try_start_0
    iget-object v6, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$utils:Lcom/android/keyguard/LockPatternUtilsWrapper;

    iget-object v7, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$password:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$callback:Lcom/android/keyguard/OnCheckForUsersCallback;

    invoke-virtual {v6, v7, v0, v8}, Lcom/android/keyguard/LockPatternUtilsWrapper;->checkPassword(Ljava/lang/String;ILcom/android/keyguard/OnCheckForUsersCallback;)Z

    move-result v6

    if-eqz v6, :cond_0

    iput v0, p0, Lcom/android/keyguard/LockPatternChecker$2;->mUserIdMatched:I

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Lcom/android/keyguard/KeyguardRequestThrottledException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    return-object v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/android/keyguard/KeyguardRequestThrottledException;->getTimeoutMs()I

    move-result v6

    iput v6, p0, Lcom/android/keyguard/LockPatternChecker$2;->mThrottleTimeout:I

    :cond_0
    sget-boolean v6, Lmiui/util/OldmanUtil;->IS_ELDER_MODE:Z

    if-eqz v6, :cond_1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    return-object v6

    :cond_1
    iget-object v6, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$context:Landroid/content/Context;

    invoke-static {v6}, Lcom/android/keyguard/MiuiKeyguardUtils;->getUserList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/UserInfo;

    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    if-eq v6, v0, :cond_2

    :try_start_1
    iget-object v6, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$utils:Lcom/android/keyguard/LockPatternUtilsWrapper;

    iget v7, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v6, v7}, Lcom/android/keyguard/LockPatternChecker;->-wrap0(Lcom/android/keyguard/LockPatternUtilsWrapper;I)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$utils:Lcom/android/keyguard/LockPatternUtilsWrapper;

    iget-object v7, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$password:Ljava/lang/String;

    iget v8, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v6, v7, v8}, Lcom/android/keyguard/LockPatternUtilsWrapper;->checkPassword(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    iget v6, v3, Landroid/content/pm/UserInfo;->id:I

    iput v6, p0, Lcom/android/keyguard/LockPatternChecker$2;->mUserIdMatched:I

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    return-object v6

    :cond_3
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    return-object v6

    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/LockPatternChecker$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/LockPatternChecker$2;->val$callback:Lcom/android/keyguard/OnCheckForUsersCallback;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget v2, p0, Lcom/android/keyguard/LockPatternChecker$2;->mUserIdMatched:I

    iget v3, p0, Lcom/android/keyguard/LockPatternChecker$2;->mThrottleTimeout:I

    invoke-interface {v0, v1, v2, v3}, Lcom/android/keyguard/OnCheckForUsersCallback;->onChecked(ZII)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/LockPatternChecker$2;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
