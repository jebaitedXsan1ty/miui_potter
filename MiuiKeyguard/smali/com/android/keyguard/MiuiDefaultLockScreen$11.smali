.class Lcom/android/keyguard/MiuiDefaultLockScreen$11;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;


# direct methods
.method static synthetic -wrap0(Lcom/android/keyguard/MiuiDefaultLockScreen$11;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->updateNotificationList(Ljava/util/List;)V

    return-void
.end method

.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private updateNotificationList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get38(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap10(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;

    invoke-direct {v1, p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$11$2;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$11;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public onChange(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;

    invoke-direct {v1, p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$11;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onRegistered(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->updateNotificationList(Ljava/util/List;)V

    return-void
.end method
