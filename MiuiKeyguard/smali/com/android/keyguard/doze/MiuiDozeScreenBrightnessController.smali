.class public Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;
.super Ljava/lang/Object;
.source "MiuiDozeScreenBrightnessController.java"

# interfaces
.implements Lcom/android/keyguard/doze/DozeMachine$Part;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController$1;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I = null

.field private static final BRIGHTNESS_MAX:I = 0xff

.field private static final BRIGHTNESS_MIN:I = 0xa

.field private static final DEBUG:Z

.field private static final INITIALIZED_DELAY_TIME_DARKEN:J = 0xea60L

.field private static final INITIALIZED_DELAY_TIME_OFF:J = 0xea60L

.field private static final LUX_CHANGE_SLOP:I = 0xc8

.field private static final MAX_REPORT_LATENCY_US:I = 0x11e1a300

.field private static final SCREEN_OFF_LUX:I = 0x64

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private final mDarkenTimeout:Lcom/android/keyguard/util/AlarmTimeout;

.field private mDozeTriggers:Lcom/android/keyguard/doze/DozeTriggers;

.field private mHost:Lcom/android/keyguard/doze/DozeHost;

.field private mInitializedTime:J

.field private mLastLux:F

.field private mLightSensor:Landroid/hardware/Sensor;

.field private final mMachine:Lcom/android/keyguard/doze/DozeMachine;

.field private final mOffTimeout:Lcom/android/keyguard/util/AlarmTimeout;

.field private final mSensorEventListener:Landroid/hardware/SensorEventListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mService:Lcom/android/keyguard/doze/DozeMachine$Service;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mInitializedTime:J

    return-wide v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;)Lcom/android/keyguard/doze/DozeMachine$Service;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mService:Lcom/android/keyguard/doze/DozeMachine$Service;

    return-object v0
.end method

.method private static synthetic -getcom-android-keyguard-doze-DozeMachine$StateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->-com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->-com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/keyguard/doze/DozeMachine$State;->values()[Lcom/android/keyguard/doze/DozeMachine$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSED:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSING:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_PULSE_DONE:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_PULSING:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_REQUEST_PULSE:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->FINISH:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->INITIALIZED:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_8
    :try_start_9
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->UNINITIALIZED:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_9
    sput-object v0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->-com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1

    :catch_9
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;F)F
    .locals 0

    iput p1, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mLastLux:F

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->checkToScreenOff(F)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->TAG:Ljava/lang/String;

    sget-boolean v0, Lcom/android/keyguard/doze/DozeService;->DEBUG:Z

    sput-boolean v0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lcom/android/keyguard/doze/DozeMachine;Landroid/app/AlarmManager;Lcom/android/keyguard/doze/DozeMachine$Service;Lcom/android/keyguard/doze/DozeHost;Landroid/hardware/SensorManager;Lcom/android/keyguard/doze/DozeTriggers;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController$1;-><init>(Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;)V

    iput-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    iput-object p2, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    iput-object p4, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mService:Lcom/android/keyguard/doze/DozeMachine$Service;

    iput-object p5, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mHost:Lcom/android/keyguard/doze/DozeHost;

    iput-object p6, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mSensorManager:Landroid/hardware/SensorManager;

    iput-object p7, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mDozeTriggers:Lcom/android/keyguard/doze/DozeTriggers;

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mLightSensor:Landroid/hardware/Sensor;

    new-instance v0, Lcom/android/keyguard/util/AlarmTimeout;

    new-instance v1, Lcom/android/keyguard/doze/-$Lambda$PCTPG9Gi7OSdy6yKZmRsfC8Odig;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p0}, Lcom/android/keyguard/doze/-$Lambda$PCTPG9Gi7OSdy6yKZmRsfC8Odig;-><init>(BLjava/lang/Object;)V

    const-string/jumbo v2, "DarkenAlarmTimeout"

    invoke-direct {v0, p3, v1, v2, p1}, Lcom/android/keyguard/util/AlarmTimeout;-><init>(Landroid/app/AlarmManager;Landroid/app/AlarmManager$OnAlarmListener;Ljava/lang/String;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mDarkenTimeout:Lcom/android/keyguard/util/AlarmTimeout;

    new-instance v0, Lcom/android/keyguard/util/AlarmTimeout;

    new-instance v1, Lcom/android/keyguard/doze/-$Lambda$PCTPG9Gi7OSdy6yKZmRsfC8Odig;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p0}, Lcom/android/keyguard/doze/-$Lambda$PCTPG9Gi7OSdy6yKZmRsfC8Odig;-><init>(BLjava/lang/Object;)V

    const-string/jumbo v2, "OffAlarmTimeout"

    invoke-direct {v0, p3, v1, v2, p1}, Lcom/android/keyguard/util/AlarmTimeout;-><init>(Landroid/app/AlarmManager;Landroid/app/AlarmManager$OnAlarmListener;Ljava/lang/String;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mOffTimeout:Lcom/android/keyguard/util/AlarmTimeout;

    return-void
.end method

.method private checkToScreenOff(F)V
    .locals 8

    const/high16 v6, 0x42c80000    # 100.0f

    cmpl-float v6, p1, v6

    if-lez v6, :cond_3

    const/4 v2, 0x1

    :goto_0
    xor-int/lit8 v1, v2, 0x1

    iget-object v6, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    invoke-virtual {v6}, Lcom/android/keyguard/doze/DozeMachine;->getState()Lcom/android/keyguard/doze/DozeMachine$State;

    move-result-object v5

    sget-object v6, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSED:Lcom/android/keyguard/doze/DozeMachine$State;

    if-ne v5, v6, :cond_4

    const/4 v3, 0x1

    :goto_1
    sget-object v6, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSING:Lcom/android/keyguard/doze/DozeMachine$State;

    if-ne v5, v6, :cond_5

    const/4 v4, 0x1

    :goto_2
    sget-object v6, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD:Lcom/android/keyguard/doze/DozeMachine$State;

    if-ne v5, v6, :cond_6

    const/4 v0, 0x1

    :goto_3
    if-eqz v2, :cond_7

    if-nez v3, :cond_0

    if-eqz v4, :cond_7

    :cond_0
    sget-boolean v6, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->DEBUG:Z

    if-eqz v6, :cond_1

    sget-object v6, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "Brightness Light, unpausing AOD"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v6, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    sget-object v7, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v6, v7}, Lcom/android/keyguard/doze/DozeMachine;->requestState(Lcom/android/keyguard/doze/DozeMachine$State;)V

    iget-object v6, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mDozeTriggers:Lcom/android/keyguard/doze/DozeTriggers;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/keyguard/doze/DozeTriggers;->disableProxListening(Z)V

    :cond_2
    :goto_4
    return-void

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    :cond_7
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    sget-boolean v6, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->DEBUG:Z

    if-eqz v6, :cond_8

    sget-object v6, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "Brightness Dark, pausing AOD"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v6, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mMachine:Lcom/android/keyguard/doze/DozeMachine;

    sget-object v7, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSING:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v6, v7}, Lcom/android/keyguard/doze/DozeMachine;->requestState(Lcom/android/keyguard/doze/DozeMachine$State;)V

    iget-object v6, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mDozeTriggers:Lcom/android/keyguard/doze/DozeTriggers;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/android/keyguard/doze/DozeTriggers;->disableProxListening(Z)V

    goto :goto_4
.end method

.method private onDarkenTimeout()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mHost:Lcom/android/keyguard/doze/DozeHost;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-interface {v0, v1}, Lcom/android/keyguard/doze/DozeHost;->setAodDimmingScrim(F)V

    return-void
.end method

.method private onOffTimeout()V
    .locals 1

    iget v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mLastLux:F

    invoke-direct {p0, v0}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->checkToScreenOff(F)V

    return-void
.end method


# virtual methods
.method synthetic -com_android_keyguard_doze_MiuiDozeScreenBrightnessController-mthref-0()V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->onDarkenTimeout()V

    return-void
.end method

.method synthetic -com_android_keyguard_doze_MiuiDozeScreenBrightnessController-mthref-1()V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->onOffTimeout()V

    return-void
.end method

.method public transitionTo(Lcom/android/keyguard/doze/DozeMachine$State;Lcom/android/keyguard/doze/DozeMachine$State;)V
    .locals 6

    const-wide/32 v4, 0xea60

    const/4 v2, 0x1

    invoke-static {}, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->-getcom-android-keyguard-doze-DozeMachine$StateSwitchesValues()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mDarkenTimeout:Lcom/android/keyguard/util/AlarmTimeout;

    invoke-virtual {v0, v4, v5, v2}, Lcom/android/keyguard/util/AlarmTimeout;->schedule(JI)V

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mOffTimeout:Lcom/android/keyguard/util/AlarmTimeout;

    invoke-virtual {v0, v4, v5, v2}, Lcom/android/keyguard/util/AlarmTimeout;->schedule(JI)V

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mHost:Lcom/android/keyguard/doze/DozeHost;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/keyguard/doze/DozeHost;->setAodDimmingScrim(F)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mInitializedTime:J

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mLightSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mLightSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    const v4, 0x11e1a300

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;II)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mDarkenTimeout:Lcom/android/keyguard/util/AlarmTimeout;

    invoke-virtual {v0}, Lcom/android/keyguard/util/AlarmTimeout;->cancel()V

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mOffTimeout:Lcom/android/keyguard/util/AlarmTimeout;

    invoke-virtual {v0}, Lcom/android/keyguard/util/AlarmTimeout;->cancel()V

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mService:Lcom/android/keyguard/doze/DozeMachine$Service;

    const/16 v1, 0xff

    invoke-interface {v0, v1}, Lcom/android/keyguard/doze/DozeMachine$Service;->setDozeScreenBrightness(I)V

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mLightSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/doze/MiuiDozeScreenBrightnessController;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
