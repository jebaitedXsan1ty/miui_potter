.class public Lcom/android/keyguard/doze/DozeScreenBrightness;
.super Ljava/lang/Object;
.source "DozeScreenBrightness.java"

# interfaces
.implements Lcom/android/keyguard/doze/DozeMachine$Part;
.implements Landroid/hardware/SensorEventListener;


# static fields
.field private static final synthetic -com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDefaultDozeBrightness:I

.field private final mDozeHost:Lcom/android/keyguard/doze/DozeHost;

.field private final mDozeService:Lcom/android/keyguard/doze/DozeMachine$Service;

.field private final mHandler:Landroid/os/Handler;

.field private mLastSensorValue:I

.field private final mLightSensor:Landroid/hardware/Sensor;

.field private mPaused:Z

.field private mRegistered:Z

.field private final mSensorManager:Landroid/hardware/SensorManager;

.field private final mSensorToBrightness:[I

.field private final mSensorToScrimOpacity:[I


# direct methods
.method private static synthetic -getcom-android-keyguard-doze-DozeMachine$StateSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/keyguard/doze/DozeScreenBrightness;->-com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/keyguard/doze/DozeScreenBrightness;->-com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/keyguard/doze/DozeMachine$State;->values()[Lcom/android/keyguard/doze/DozeMachine$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSED:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSING:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_PULSE_DONE:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_PULSING:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_5
    :try_start_6
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_REQUEST_PULSE:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_6
    :try_start_7
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->FINISH:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_7
    :try_start_8
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->INITIALIZED:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_8
    :try_start_9
    sget-object v1, Lcom/android/keyguard/doze/DozeMachine$State;->UNINITIALIZED:Lcom/android/keyguard/doze/DozeMachine$State;

    invoke-virtual {v1}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_9
    sput-object v0, Lcom/android/keyguard/doze/DozeScreenBrightness;->-com-android-keyguard-doze-DozeMachine$StateSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1

    :catch_9
    move-exception v1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/keyguard/doze/DozeMachine$Service;Landroid/hardware/SensorManager;Landroid/hardware/Sensor;Lcom/android/keyguard/doze/DozeHost;Landroid/os/Handler;I[I[I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mPaused:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mLastSensorValue:I

    iput-object p1, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mDozeService:Lcom/android/keyguard/doze/DozeMachine$Service;

    iput-object p3, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mSensorManager:Landroid/hardware/SensorManager;

    iput-object p4, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mLightSensor:Landroid/hardware/Sensor;

    iput-object p5, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    iput-object p6, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mHandler:Landroid/os/Handler;

    iput p7, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mDefaultDozeBrightness:I

    iput-object p8, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mSensorToBrightness:[I

    iput-object p9, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mSensorToScrimOpacity:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/keyguard/doze/DozeMachine$Service;Landroid/hardware/SensorManager;Landroid/hardware/Sensor;Lcom/android/keyguard/doze/DozeHost;Landroid/os/Handler;Lcom/android/keyguard/doze/AlwaysOnDisplayPolicy;)V
    .locals 11

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10e008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    move-object/from16 v0, p7

    iget-object v9, v0, Lcom/android/keyguard/doze/AlwaysOnDisplayPolicy;->screenBrightnessArray:[I

    move-object/from16 v0, p7

    iget-object v10, v0, Lcom/android/keyguard/doze/AlwaysOnDisplayPolicy;->dimmingScrimArray:[I

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v10}, Lcom/android/keyguard/doze/DozeScreenBrightness;-><init>(Landroid/content/Context;Lcom/android/keyguard/doze/DozeMachine$Service;Landroid/hardware/SensorManager;Landroid/hardware/Sensor;Lcom/android/keyguard/doze/DozeHost;Landroid/os/Handler;I[I[I)V

    return-void
.end method

.method private computeBrightness(I)I
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mSensorToBrightness:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mSensorToBrightness:[I

    aget v0, v0, p1

    return v0
.end method

.method private computeScrimOpacity(I)I
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mSensorToScrimOpacity:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mSensorToScrimOpacity:[I

    aget v0, v0, p1

    return v0
.end method

.method private resetBrightnessToDefault()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mDozeService:Lcom/android/keyguard/doze/DozeMachine$Service;

    iget v1, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mDefaultDozeBrightness:I

    invoke-interface {v0, v1}, Lcom/android/keyguard/doze/DozeMachine$Service;->setDozeScreenBrightness(I)V

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/keyguard/doze/DozeHost;->setAodDimmingScrim(F)V

    return-void
.end method

.method private setLightSensorEnabled(Z)V
    .locals 5

    const/4 v4, -0x1

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mRegistered:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mLightSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mLightSensor:Landroid/hardware/Sensor;

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v0, p0, v1, v3, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mRegistered:Z

    iput v4, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mLastSensorValue:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mRegistered:Z

    iput v4, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mLastSensorValue:I

    goto :goto_0
.end method

.method private setPaused(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mPaused:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mPaused:Z

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeScreenBrightness;->updateBrightnessAndReady()V

    :cond_0
    return-void
.end method

.method private updateBrightnessAndReady()V
    .locals 6

    iget-boolean v3, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mRegistered:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mLastSensorValue:I

    invoke-direct {p0, v3}, Lcom/android/keyguard/doze/DozeScreenBrightness;->computeBrightness(I)I

    move-result v0

    if-lez v0, :cond_3

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mDozeService:Lcom/android/keyguard/doze/DozeMachine$Service;

    invoke-interface {v3, v0}, Lcom/android/keyguard/doze/DozeMachine$Service;->setDozeScreenBrightness(I)V

    :cond_0
    const/4 v2, -0x1

    iget-boolean v3, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mPaused:Z

    if-eqz v3, :cond_4

    const/16 v2, 0xff

    :cond_1
    :goto_1
    if-ltz v2, :cond_2

    iget-object v3, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mDozeHost:Lcom/android/keyguard/doze/DozeHost;

    int-to-float v4, v2

    const/high16 v5, 0x437f0000    # 255.0f

    div-float/2addr v4, v5

    invoke-interface {v3, v4}, Lcom/android/keyguard/doze/DozeHost;->setAodDimmingScrim(F)V

    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    if-eqz v1, :cond_1

    iget v3, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mLastSensorValue:I

    invoke-direct {p0, v3}, Lcom/android/keyguard/doze/DozeScreenBrightness;->computeScrimOpacity(I)I

    move-result v2

    goto :goto_1
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "DozeScreenBrightness.onSensorChanged"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mRegistered:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/keyguard/doze/DozeScreenBrightness;->mLastSensorValue:I

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeScreenBrightness;->updateBrightnessAndReady()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/os/Trace;->endSection()V

    throw v0
.end method

.method public transitionTo(Lcom/android/keyguard/doze/DozeMachine$State;Lcom/android/keyguard/doze/DozeMachine$State;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/keyguard/doze/DozeScreenBrightness;->-getcom-android-keyguard-doze-DozeMachine$StateSwitchesValues()[I

    move-result-object v2

    invoke-virtual {p2}, Lcom/android/keyguard/doze/DozeMachine$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    sget-object v2, Lcom/android/keyguard/doze/DozeMachine$State;->FINISH:Lcom/android/keyguard/doze/DozeMachine$State;

    if-eq p2, v2, :cond_0

    sget-object v2, Lcom/android/keyguard/doze/DozeMachine$State;->DOZE_AOD_PAUSED:Lcom/android/keyguard/doze/DozeMachine$State;

    if-ne p2, v2, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/android/keyguard/doze/DozeScreenBrightness;->setPaused(Z)V

    :cond_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeScreenBrightness;->resetBrightnessToDefault()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v0}, Lcom/android/keyguard/doze/DozeScreenBrightness;->setLightSensorEnabled(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v1}, Lcom/android/keyguard/doze/DozeScreenBrightness;->setLightSensorEnabled(Z)V

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeScreenBrightness;->resetBrightnessToDefault()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v1}, Lcom/android/keyguard/doze/DozeScreenBrightness;->setLightSensorEnabled(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
