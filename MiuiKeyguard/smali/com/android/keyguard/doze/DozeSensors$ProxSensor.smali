.class Lcom/android/keyguard/doze/DozeSensors$ProxSensor;
.super Ljava/lang/Object;
.source "DozeSensors.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/doze/DozeSensors;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProxSensor"
.end annotation


# instance fields
.field mCurrentlyFar:Ljava/lang/Boolean;

.field mDisable:Z

.field final mPolicy:Lcom/android/keyguard/doze/AlwaysOnDisplayPolicy;

.field mRegistered:Z

.field mRequested:Z

.field final synthetic this$0:Lcom/android/keyguard/doze/DozeSensors;


# direct methods
.method public constructor <init>(Lcom/android/keyguard/doze/DozeSensors;Lcom/android/keyguard/doze/AlwaysOnDisplayPolicy;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mPolicy:Lcom/android/keyguard/doze/AlwaysOnDisplayPolicy;

    return-void
.end method

.method private setRegistered(Z)V
    .locals 4

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mRegistered:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeSensors;->-get7(Lcom/android/keyguard/doze/DozeSensors;)Landroid/hardware/SensorManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v1}, Lcom/android/keyguard/doze/DozeSensors;->-get7(Lcom/android/keyguard/doze/DozeSensors;)Landroid/hardware/SensorManager;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v2}, Lcom/android/keyguard/doze/DozeSensors;->-get4(Lcom/android/keyguard/doze/DozeSensors;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v0, p0, v1, v3, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mRegistered:Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeSensors;->-get7(Lcom/android/keyguard/doze/DozeSensors;)Landroid/hardware/SensorManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mRegistered:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mCurrentlyFar:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method private updateRegistered()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mRequested:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mDisable:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->setRegistered(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method synthetic lambda$-com_android_keyguard_doze_DozeSensors$ProxSensor_8115()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mCurrentlyFar:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeSensors;->-get5(Lcom/android/keyguard/doze/DozeSensors;)Ljava/util/function/Consumer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mCurrentlyFar:Ljava/lang/Boolean;

    invoke-interface {v0, v1}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v0

    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mCurrentlyFar:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeSensors;->-get5(Lcom/android/keyguard/doze/DozeSensors;)Ljava/util/function/Consumer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mCurrentlyFar:Ljava/lang/Boolean;

    invoke-interface {v0, v1}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method setDisable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mDisable:Z

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->updateRegistered()V

    return-void
.end method

.method setRequested(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mRequested:Z

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->this$0:Lcom/android/keyguard/doze/DozeSensors;

    invoke-static {v0}, Lcom/android/keyguard/doze/DozeSensors;->-get4(Lcom/android/keyguard/doze/DozeSensors;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/keyguard/doze/-$Lambda$aVbgMnPDX-Zr6SdQ0drhP74gY3E;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p0}, Lcom/android/keyguard/doze/-$Lambda$aVbgMnPDX-Zr6SdQ0drhP74gY3E;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mRequested:Z

    invoke-direct {p0}, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->updateRegistered()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string/jumbo v0, "{registered=%s, requested=%s, currentlyFar=%s}"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget-boolean v2, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mRegistered:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-boolean v2, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mRequested:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/android/keyguard/doze/DozeSensors$ProxSensor;->mCurrentlyFar:Ljava/lang/Boolean;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
