.class Lcom/android/keyguard/PhoneSignalController$1;
.super Landroid/content/BroadcastReceiver;
.source "PhoneSignalController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/PhoneSignalController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/PhoneSignalController;


# direct methods
.method constructor <init>(Lcom/android/keyguard/PhoneSignalController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/PhoneSignalController$1;->this$0:Lcom/android/keyguard/PhoneSignalController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.intent.action.ACTION_SUBINFO_RECORD_UPDATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/keyguard/PhoneSignalController;->-get0()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ACTION_SUBINFO_RECORD_UPDATED"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/keyguard/PhoneSignalController$1;->this$0:Lcom/android/keyguard/PhoneSignalController;

    invoke-static {v1}, Lcom/android/keyguard/PhoneSignalController;->-wrap1(Lcom/android/keyguard/PhoneSignalController;)V

    iget-object v1, p0, Lcom/android/keyguard/PhoneSignalController$1;->this$0:Lcom/android/keyguard/PhoneSignalController;

    invoke-virtual {v1}, Lcom/android/keyguard/PhoneSignalController;->registerPhoneStateListener()V

    :cond_0
    return-void
.end method
