.class Lcom/android/keyguard/MiuiCommonUnlockScreen$13;
.super Landroid/os/AsyncTask;
.source "MiuiCommonUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiCommonUnlockScreen;->asyncHideLockViewOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Long;
    .locals 8

    const-wide/16 v6, 0x0

    iget-object v2, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v2, v2, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v3, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v3, v3, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardLockoutAttemptDeadline(I)J

    move-result-wide v0

    cmp-long v2, v0, v6

    if-eqz v2, :cond_0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get0(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "pref_password_time_out"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "pref_password_time_out_value"

    invoke-interface {v2, v3, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;->doInBackground([Ljava/lang/Void;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Long;)V
    .locals 4

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->hideLockoutView(Z)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen$13;->onPostExecute(Ljava/lang/Long;)V

    return-void
.end method
