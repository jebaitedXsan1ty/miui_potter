.class public Lcom/android/keyguard/settings/MiuiFaceDataManage;
.super Lmiui/preference/PreferenceActivity;
.source "MiuiFaceDataManage.java"


# static fields
.field private static final KEY_DELETE_FACE_DATA:Ljava/lang/String; = "delete_face_data_recoginition"

.field private static final KEY_FACE_DATA_APPLY_LOCK:Ljava/lang/String; = "apply_face_data_lock"

.field private static final KEY_FACE_UNLOCK_BY_NOTIFICATION_SCREEN_ON:Ljava/lang/String; = "face_unlock_by_notification_screen_on"


# instance fields
.field private mDeleteFaceData:Landroid/preference/Preference;

.field private mFaceDataApplyUnlockScreen:Landroid/preference/CheckBoxPreference;

.field private mFaceUnlockByNotification:Z

.field private mFaceUnlockByNotificationPreference:Landroid/preference/CheckBoxPreference;

.field private mHasFaceData:Z

.field private mIsFaceApplyUnlock:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private handleSecurityLockToggle()V
    .locals 3

    new-instance v0, Lcom/android/keyguard/settings/MiuiFaceDataManage$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/settings/MiuiFaceDataManage$1;-><init>(Lcom/android/keyguard/settings/MiuiFaceDataManage;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0079

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b007a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v0, 0x7f050000

    invoke-virtual {p0, v0}, Lcom/android/keyguard/settings/MiuiFaceDataManage;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "delete_face_data_recoginition"

    invoke-virtual {p0, v0}, Lcom/android/keyguard/settings/MiuiFaceDataManage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/settings/MiuiFaceDataManage;->mDeleteFaceData:Landroid/preference/Preference;

    const-string/jumbo v0, "apply_face_data_lock"

    invoke-virtual {p0, v0}, Lcom/android/keyguard/settings/MiuiFaceDataManage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/keyguard/settings/MiuiFaceDataManage;->mFaceDataApplyUnlockScreen:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/keyguard/settings/MiuiFaceDataManage;->mFaceDataApplyUnlockScreen:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/keyguard/settings/MiuiFaceDataManage;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "face_unlcok_apply_for_lock"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v5

    invoke-static {v0, v4, v1, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const-string/jumbo v0, "face_unlock_by_notification_screen_on"

    invoke-virtual {p0, v0}, Lcom/android/keyguard/settings/MiuiFaceDataManage;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/keyguard/settings/MiuiFaceDataManage;->mFaceUnlockByNotificationPreference:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/keyguard/settings/MiuiFaceDataManage;->mFaceUnlockByNotificationPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/keyguard/settings/MiuiFaceDataManage;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "face_unlock_by_notification_screen_on"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v5

    invoke-static {v3, v4, v2, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-ne v3, v1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x1

    const-string/jumbo v4, "delete_face_data_recoginition"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/settings/MiuiFaceDataManage;->handleSecurityLockToggle()V

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v4, "apply_face_data_lock"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/android/keyguard/settings/MiuiFaceDataManage;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "face_unlcok_apply_for_lock"

    iget-object v6, p0, Lcom/android/keyguard/settings/MiuiFaceDataManage;->mFaceDataApplyUnlockScreen:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_1
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    invoke-static {v4, v5, v2, v3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1

    :cond_2
    const-string/jumbo v4, "face_unlock_by_notification_screen_on"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/android/keyguard/settings/MiuiFaceDataManage;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "face_unlock_by_notification_screen_on"

    iget-object v6, p0, Lcom/android/keyguard/settings/MiuiFaceDataManage;->mFaceUnlockByNotificationPreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_3

    :goto_2
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    invoke-static {v4, v5, v2, v3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_2

    :cond_4
    invoke-super {p0, p1, p2}, Lmiui/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_0
.end method
