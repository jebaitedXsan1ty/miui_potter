.class Lcom/android/keyguard/MiuiDefaultLockScreen$7;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v1, v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set9(Lcom/android/keyguard/MiuiDefaultLockScreen;F)F

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get19(Lcom/android/keyguard/MiuiDefaultLockScreen;)F

    move-result v0

    const/high16 v1, 0x40400000    # 3.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get27(Lcom/android/keyguard/MiuiDefaultLockScreen;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0, v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set6(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set8(Lcom/android/keyguard/MiuiDefaultLockScreen;I)I

    :goto_1
    return-void

    :cond_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    if-ne v4, v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set12(Lcom/android/keyguard/MiuiDefaultLockScreen;F)F

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set6(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$7;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0042

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set8(Lcom/android/keyguard/MiuiDefaultLockScreen;I)I

    goto :goto_1
.end method
