.class public interface abstract Lcom/android/keyguard/MiuiKeyguardScreen;
.super Ljava/lang/Object;
.source "MiuiKeyguardScreen.java"


# virtual methods
.method public abstract cleanUp()V
.end method

.method public abstract needsInput()Z
.end method

.method public abstract onFaceUnlockFailed()V
.end method

.method public abstract onFaceUnlockHelp(Ljava/lang/String;)V
.end method

.method public abstract onFaceUnlockStart()V
.end method

.method public abstract onFaceUnlockSuccess()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume(Z)V
.end method

.method public abstract setHidden(Z)V
.end method
