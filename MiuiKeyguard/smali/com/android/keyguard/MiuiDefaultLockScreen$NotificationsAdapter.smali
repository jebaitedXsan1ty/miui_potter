.class Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "MiuiDefaultLockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotificationsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;


# direct methods
.method private constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    check-cast p1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->onBindViewHolder(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;I)V
    .locals 13

    const/4 v12, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x8

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get0(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v2, v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get6(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v2, v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->title:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get2(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v2, v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->content:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get3(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/ImageView;

    move-result-object v3

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v2, v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->profile:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get1(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v2, v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->info:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get44(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/graphics/Typeface;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get6(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get44(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get2(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get44(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get44(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget v10, v2, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->key:I

    iget-object v2, p1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->itemView:Landroid/view/View;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/high16 v4, 0x7f0d0000

    invoke-virtual {v2, v4, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v2, p1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->itemView:Landroid/view/View;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f0d0001

    invoke-virtual {v2, v4, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v2, p1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v2, p1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2, v12}, Landroid/view/View;->setTranslationX(F)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get38(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v2

    if-ne v10, v2, :cond_2

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v9, 0xc8

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const v2, 0x3f8ccccd    # 1.1f

    const v4, 0x3f8ccccd    # 1.1f

    const/high16 v6, 0x3f000000    # 0.5f

    const/high16 v8, 0x3f000000    # 0.5f

    move v3, v1

    move v7, v5

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    new-instance v2, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$1;

    invoke-direct {v2, p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get0(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v11, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v11, v1, v12}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v11, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v11, v5}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    const/4 v1, 0x2

    invoke-virtual {v11, v1}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$2;

    invoke-direct {v1, p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$2;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)V

    invoke-virtual {v11, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    iget-object v1, p1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f020056

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {p1, v5}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-set0(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;Z)Z

    :goto_1
    iget-object v1, p1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->itemView:Landroid/view/View;

    new-instance v2, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;

    invoke-direct {v2, p0, p2, v10}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    const v2, 0x7f0b0033

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get3(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get1(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-static {p1, v7}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-set0(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;Z)Z

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v1, v1, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->time:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v1, v1, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->profile:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get1(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get3(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    iget-object v1, p1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f020055

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :cond_3
    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get3(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get1(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030009

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;

    invoke-direct {v1, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method
