.class Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;
.super Landroid/os/AsyncTask;
.source "MiuiKeyguardMoveLeftView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->updateItemNumString(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 10

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get24(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-wrap0(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v5

    if-eqz v5, :cond_0

    :try_start_0
    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get2()Landroid/net/Uri;

    move-result-object v6

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v7

    invoke-static {v6, v7}, Lcom/android/keyguard/MiuiKeyguardUtils;->maybeAddUserId(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v6

    const-string/jumbo v7, "online_devices_count"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v3

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    const-string/jumbo v6, "count"

    const-string/jumbo v7, ""

    invoke-virtual {v3, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set7(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get21(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get15(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v5

    if-eqz v5, :cond_1

    :try_start_1
    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get0()Landroid/net/Uri;

    move-result-object v6

    const-string/jumbo v7, "device_sum"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    const-string/jumbo v6, "ir_device_sum"

    const-string/jumbo v7, ""

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set6(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get4(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get8(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get19(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get28(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Z

    move-result v5

    if-eqz v5, :cond_3

    :try_start_2
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v5, "source"

    const-string/jumbo v6, "lock_screen"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v5}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get14(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get1()Landroid/net/Uri;

    move-result-object v6

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v7

    invoke-static {v6, v7}, Lcom/android/keyguard/MiuiKeyguardUtils;->maybeAddUserId(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v6

    const-string/jumbo v7, "cards_info"

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    const-string/jumbo v6, "bank_cards_count"

    const-string/jumbo v7, ""

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set0(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Ljava/lang/String;)Ljava/lang/String;

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    const-string/jumbo v6, "default_trans_card_balance"

    const-string/jumbo v7, ""

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-set1(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    :goto_2
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    return-object v5

    :catch_0
    move-exception v2

    const-string/jumbo v5, "MiuiKeyguardMoveLeftView"

    const-string/jumbo v6, "cannot find the path content://com.xiaomi.smarthome.ext_cp"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_1
    move-exception v2

    const-string/jumbo v5, "MiuiKeyguardMoveLeftView"

    const-string/jumbo v6, "cannot find the path content://com.xiaomi.mitv.phone.remotecontroller.provider.LockScreenProvider"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_2
    move-exception v2

    const-string/jumbo v5, "MiuiKeyguardMoveLeftView"

    const-string/jumbo v6, "cannot find the path content://com.miui.tsmclient.provider.public"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get26(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get27(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get22(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get23(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get6(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get7(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get10(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->this$0:Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->-get11(Lcom/android/keyguard/MiuiKeyguardMoveLeftView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView$4;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
