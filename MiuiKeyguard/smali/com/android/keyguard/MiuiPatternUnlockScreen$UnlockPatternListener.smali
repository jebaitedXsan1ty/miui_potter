.class Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;
.super Ljava/lang/Object;
.source "MiuiPatternUnlockScreen.java"

# interfaces
.implements Lcom/android/internal/widget/LockPatternView$OnPatternListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiPatternUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnlockPatternListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;


# direct methods
.method private constructor <init>(Lcom/android/keyguard/MiuiPatternUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/keyguard/MiuiPatternUnlockScreen;Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;-><init>(Lcom/android/keyguard/MiuiPatternUnlockScreen;)V

    return-void
.end method


# virtual methods
.method public onPatternCellAdded(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/16 v1, 0x1b58

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/16 v1, 0x7d0

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock(I)V

    goto :goto_0
.end method

.method public onPatternCleared()V
    .locals 0

    return-void
.end method

.method public onPatternDetected(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    invoke-virtual {v1, v3}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->setPasswordEntryInputEnabled(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    invoke-virtual {v1, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->setPasswordEntryInputEnabled(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    invoke-static {v1, v0, v3, v3, v3}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->-wrap0(Lcom/android/keyguard/MiuiPatternUnlockScreen;IZIZ)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    iget-object v2, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    iget-object v2, v2, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    iget-object v3, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->-get1(Lcom/android/keyguard/MiuiPatternUnlockScreen;)Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener$1;

    invoke-direct {v4, p0, v0}, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener$1;-><init>(Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;I)V

    invoke-static {v2, p1, v3, v4}, Lcom/android/keyguard/LockPatternChecker;->checkPatternForUsers(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/util/List;Landroid/content/Context;Lcom/android/keyguard/OnCheckForUsersCallback;)Landroid/os/AsyncTask;

    move-result-object v2

    iput-object v2, v1, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiPatternUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/16 v2, 0x1b58

    invoke-interface {v1, v2}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock(I)V

    :cond_2
    return-void
.end method

.method public onPatternStart()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->-get2(Lcom/android/keyguard/MiuiPatternUnlockScreen;)Lcom/android/keyguard/MiuiKeyguardLockPatternView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiPatternUnlockScreen$UnlockPatternListener;->this$0:Lcom/android/keyguard/MiuiPatternUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiPatternUnlockScreen;->-get0(Lcom/android/keyguard/MiuiPatternUnlockScreen;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method
