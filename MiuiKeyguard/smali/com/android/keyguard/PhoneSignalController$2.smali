.class Lcom/android/keyguard/PhoneSignalController$2;
.super Landroid/telephony/PhoneStateListener;
.source "PhoneSignalController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/PhoneSignalController;->getPhoneStateListener(JI)Landroid/telephony/PhoneStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/PhoneSignalController;

.field final synthetic val$subId:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/PhoneSignalController;Ljava/lang/Integer;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/PhoneSignalController$2;->this$0:Lcom/android/keyguard/PhoneSignalController;

    iput p3, p0, Lcom/android/keyguard/PhoneSignalController$2;->val$subId:I

    invoke-direct {p0, p2}, Landroid/telephony/PhoneStateListener;-><init>(Ljava/lang/Integer;)V

    return-void
.end method


# virtual methods
.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 6

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLevel()I

    move-result v3

    const/4 v4, 0x1

    if-lt v3, v4, :cond_0

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLevel()I

    move-result v3

    const/4 v4, 0x6

    if-ge v3, v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/android/keyguard/PhoneSignalController$2;->this$0:Lcom/android/keyguard/PhoneSignalController;

    invoke-static {v3}, Lcom/android/keyguard/PhoneSignalController;->-get1(Lcom/android/keyguard/PhoneSignalController;)[Z

    move-result-object v3

    iget v4, p0, Lcom/android/keyguard/PhoneSignalController$2;->val$subId:I

    aget-boolean v3, v3, v4

    if-eq v0, v3, :cond_1

    iget-object v3, p0, Lcom/android/keyguard/PhoneSignalController$2;->this$0:Lcom/android/keyguard/PhoneSignalController;

    invoke-static {v3}, Lcom/android/keyguard/PhoneSignalController;->-get1(Lcom/android/keyguard/PhoneSignalController;)[Z

    move-result-object v3

    iget v4, p0, Lcom/android/keyguard/PhoneSignalController$2;->val$subId:I

    aput-boolean v0, v3, v4

    invoke-static {}, Lcom/android/keyguard/PhoneSignalController;->-get0()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "subid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/keyguard/PhoneSignalController$2;->val$subId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "onSignalStrengthsChanged level="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLevel()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";available="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/keyguard/PhoneSignalController$2;->this$0:Lcom/android/keyguard/PhoneSignalController;

    invoke-static {v3}, Lcom/android/keyguard/PhoneSignalController;->-get2(Lcom/android/keyguard/PhoneSignalController;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;

    iget-object v3, p0, Lcom/android/keyguard/PhoneSignalController$2;->this$0:Lcom/android/keyguard/PhoneSignalController;

    invoke-static {v3, v1}, Lcom/android/keyguard/PhoneSignalController;->-wrap0(Lcom/android/keyguard/PhoneSignalController;Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    return-void
.end method
