.class Lcom/android/keyguard/MiuiDefaultLockScreen$12;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$12;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$12;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->clearAnimation()V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$12$1;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$12$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$12;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$12;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
