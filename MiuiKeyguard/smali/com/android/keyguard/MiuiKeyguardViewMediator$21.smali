.class Lcom/android/keyguard/MiuiKeyguardViewMediator$21;
.super Ljava/lang/Object;
.source "MiuiKeyguardViewMediator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;->startFingerprintIdentify()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$21;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    const-string/jumbo v1, "miui_keyguard_fingerprint"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startFingerprintIdentify  finger state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get24()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$21;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap1(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$21;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->shouldListenForFingerprint()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$21;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    const-string/jumbo v2, "1"

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap26(Lcom/android/keyguard/MiuiKeyguardViewMediator;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$21;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get21(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->startFingerprintIdentify()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setFingerprintRunningState(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "keyguard_fingerprint"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
