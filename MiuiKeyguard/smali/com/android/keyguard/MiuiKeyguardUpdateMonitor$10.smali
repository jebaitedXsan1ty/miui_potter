.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;
.super Landroid/os/AsyncTask;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap1(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set11(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get18(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;->doInBackground([Ljava/lang/Void;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;)V
    .locals 3

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get8(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get19(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;->updateWallPaperInfo(Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;->onPostExecute(Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;)V

    return-void
.end method
