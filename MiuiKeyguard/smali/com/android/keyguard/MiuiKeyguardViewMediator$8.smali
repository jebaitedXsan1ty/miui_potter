.class Lcom/android/keyguard/MiuiKeyguardViewMediator$8;
.super Landroid/database/ContentObserver;
.source "MiuiKeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$8;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$8;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$8;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "face_unlcok_apply_for_lock"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v5

    invoke-static {v3, v4, v0, v5}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    invoke-static {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set1(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
