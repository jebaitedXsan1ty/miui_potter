.class Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;
.super Lcom/android/keyguard/MiuiCommonUnlockScreen;
.source "MiuiNumericPasswordUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;
    }
.end annotation


# instance fields
.field private final mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

.field private mIsLockByFindDevice:Z

.field private mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

.field private mNumericPasswordUnlockScreen:Landroid/view/View;

.field private mPasswordEncryptDots:Landroid/widget/LinearLayout;

.field private mPasswordString:Ljava/lang/StringBuilder;

.field private mSignalAvailable:Z

.field protected final mVibrator:Landroid/os/Vibrator;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Lcom/android/keyguard/MiuiNumericInputView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordEncryptDots:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)Ljava/lang/StringBuilder;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordString:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->clearPassword()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;IZIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->onPasswordChecked(IZIZ)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->verifyPassword()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/android/keyguard/MiuiCommonUnlockScreen;-><init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordString:Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mSignalAvailable:Z

    new-instance v0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$1;-><init>(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

    const-string/jumbo v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$Secure;->FIND_DEVICE_PIN_LOCK:Ljava/lang/String;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mIsLockByFindDevice:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->initView()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private clearPassword()V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordString:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordString:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordEncryptDots:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordEncryptDots:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020069

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private clearPinLockForFindDevice()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mIsLockByFindDevice:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v0}, Lcom/android/keyguard/LockPatternUtilsWrapper;->clearLock()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$Secure;->FIND_DEVICE_PIN_LOCK:Ljava/lang/String;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v2

    invoke-static {v0, v1, v3, v2}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mIsLockByFindDevice:Z

    :cond_0
    return-void
.end method

.method private handleWrongPassword(IZ)V
    .locals 12

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportFailedUnlockAttempt()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    const-wide/16 v10, 0x0

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->clearPassword()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setKeyguardLockoutAttemptDeadline(I)J

    move-result-wide v10

    invoke-virtual {p0, v10, v11}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleAttemptLockout(J)V

    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v1, v10, v2

    if-nez v1, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    const-string/jumbo v1, "miui_keyguard_fingerprint"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "lockout timeoutMs="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ";deadline="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-wide/16 v2, 0x0

    cmp-long v2, v10, v2

    if-lez v2, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v2, v10, v2

    :goto_1
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const v2, -0x42333333    # -0.1f

    const/4 v3, 0x1

    const v4, 0x3dcccccd    # 0.1f

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    new-instance v1, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;

    invoke-direct {v1, p0, p1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$3;-><init>(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;I)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordEncryptDots:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiNumericInputView;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_1
.end method

.method private initView()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03001a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericPasswordUnlockScreen:Landroid/view/View;

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericPasswordUnlockScreen:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->addUnlockView(Landroid/view/View;)V

    const v0, 0x7f0d0080

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiNumericInputView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiNumericInputView;->getEmergencyCall()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->processEmergencyCall(Landroid/view/View;)V

    const v0, 0x7f0d007b

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordEncryptDots:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiNumericInputView;->setNumericInputListener(Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordEncryptDots:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleAttemptLockoutIfNeed()V

    return-void
.end method

.method private onPasswordChecked(IZIZ)V
    .locals 3

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";matched="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";timeoutMs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->isNeedCloseSdcardFs(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleNeedCloseSdcardFs()V

    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->isFBEDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleFirstMatchSpacePassword()V

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleWrongPassword(IZ)V

    return-void

    :cond_1
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v0

    if-eq p1, v0, :cond_3

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->isPhoneCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can\'t switch user to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " when calling"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleSwitchUserWhenCalling()V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->isGreenKidActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can\'t switch user to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " when green kid active"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleSwitchUserWhenGreenkidActive()V

    return-void

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->switchUser(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->keyguardDone(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_PW:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->recordUnlockWay(Ljava/lang/String;)V

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "unlock by numeric password"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->clearPinLockForFindDevice()V

    :goto_0
    return-void

    :cond_4
    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->handleWrongPassword(IZ)V

    goto :goto_0
.end method

.method private verifyPassword()V
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->setPasswordEntryInputEnabled(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordString:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$2;

    invoke-direct {v4, p0, v0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen$2;-><init>(Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;I)V

    invoke-static {v1, v2, v3, v4}, Lcom/android/keyguard/LockPatternChecker;->checkPasswordForUsers(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/lang/String;Landroid/content/Context;Lcom/android/keyguard/OnCheckForUsersCallback;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public applyAnimation()V
    .locals 1

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->applyAnimation()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiNumericInputView;->startAnimation()V

    return-void
.end method

.method public cleanUp()V
    .locals 0

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x7

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x10

    if-gt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    add-int/lit8 v1, v1, -0x7

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;->onInput(I)V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected handleAttemptLockout(J)V
    .locals 5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->showLockoutView(J)V

    return-void
.end method

.method protected hideLockoutView()V
    .locals 2

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->hideLockoutView()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiNumericInputView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericPasswordUnlockScreen:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public needsInput()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onPause()V

    return-void
.end method

.method public onResume(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->clearPassword()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->asyncHideLockViewOnResume()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->onResume()V

    :cond_0
    return-void
.end method

.method protected onScreenOrientationChanged()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordString:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mPasswordString:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->initView()V

    return-void
.end method

.method protected setPasswordEntryInputEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiNumericInputView;->setEnabled(Z)V

    return-void
.end method

.method public showBLEUnlockSucceedView()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mIsLockByFindDevice:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->showBLEUnlockSucceedView()V

    return-void
.end method

.method protected showLockoutView(J)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->showLockoutView(J)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiNumericInputView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericPasswordUnlockScreen:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected updateEmergencyCallButtonVisibility(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mSignalAvailable:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiNumericInputView;->updateEmergencyCallVisibility(Z)V

    return-void
.end method
