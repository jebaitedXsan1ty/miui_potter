.class Lcom/android/keyguard/MiuiLockPatternKeyguardView$1;
.super Ljava/lang/Object;
.source "MiuiLockPatternKeyguardView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiLockPatternKeyguardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$1;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$1;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get6(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    move-result-object v1

    const/4 v0, 0x0

    sget-object v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$1;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getUnlockMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    move-result-object v2

    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne v2, v3, :cond_0

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    const/4 v0, 0x1

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$1;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v2, v1, v4}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$1;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v2, v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v2, v4}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->keyguardDone(Z)V

    :cond_1
    return-void
.end method
