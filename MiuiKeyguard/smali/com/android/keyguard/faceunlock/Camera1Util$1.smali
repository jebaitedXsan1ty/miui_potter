.class Lcom/android/keyguard/faceunlock/Camera1Util$1;
.super Ljava/lang/Object;
.source "Camera1Util.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/faceunlock/Camera1Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/faceunlock/Camera1Util;


# direct methods
.method constructor <init>(Lcom/android/keyguard/faceunlock/Camera1Util;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/faceunlock/Camera1Util$1;->this$0:Lcom/android/keyguard/faceunlock/Camera1Util;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/Camera1Util$1;->this$0:Lcom/android/keyguard/faceunlock/Camera1Util;

    iget-object v0, v0, Lcom/android/keyguard/faceunlock/Camera1Util;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/Camera1Util$1;->this$0:Lcom/android/keyguard/faceunlock/Camera1Util;

    iget-object v0, v0, Lcom/android/keyguard/faceunlock/Camera1Util;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/Camera1Util$1;->this$0:Lcom/android/keyguard/faceunlock/Camera1Util;

    iget-object v0, v0, Lcom/android/keyguard/faceunlock/Camera1Util;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/Camera1Util$1;->this$0:Lcom/android/keyguard/faceunlock/Camera1Util;

    iget-object v0, v0, Lcom/android/keyguard/faceunlock/Camera1Util;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    iget-object v0, p0, Lcom/android/keyguard/faceunlock/Camera1Util$1;->this$0:Lcom/android/keyguard/faceunlock/Camera1Util;

    iput-object v1, v0, Lcom/android/keyguard/faceunlock/Camera1Util;->mCamera:Landroid/hardware/Camera;

    sget-boolean v0, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "face_unlock"

    const-string/jumbo v1, "close camera"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string/jumbo v0, "0"

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->setFaceUnlockCameraProperty(Ljava/lang/String;)V

    return-void
.end method
