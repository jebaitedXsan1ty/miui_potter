.class Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;
.super Ljava/lang/Object;
.source "MiuiMixedPasswordUnlockScreen.java"

# interfaces
.implements Lcom/android/keyguard/OnCheckForUsersCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->verifyPassword(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

.field final synthetic val$userId:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    iput p2, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;->val$userId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChecked(ZII)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->setPasswordEntryInputEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    invoke-virtual {v0, p1, p2}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->needPasswordCheck(ZI)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    invoke-static {v0, p2, p1, p3, v2}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->-wrap0(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;IZIZ)V

    :cond_0
    return-void
.end method

.method public onEarlyMatched()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    iget v1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;->val$userId:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->-wrap0(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;IZIZ)V

    return-void
.end method
