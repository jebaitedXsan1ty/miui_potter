.class Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;
.super Ljava/lang/Object;
.source "MiuiMixedPasswordUnlockScreen.java"

# interfaces
.implements Lmiui/view/MiuiKeyBoardView$OnKeyboardActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKeyBoardDelete()V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->-get0(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    :cond_0
    return-void
.end method

.method public onKeyBoardOK()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->-get0(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->-wrap1(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;Ljava/lang/String;)V

    return-void
.end method

.method public onText(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;->this$0:Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->-get0(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    return-void
.end method
