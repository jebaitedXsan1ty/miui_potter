.class public Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;
.super Ljava/lang/Object;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$2;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$3;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$Injector;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;,
        Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;
    }
.end annotation


# static fields
.field private static final AUTHORITY:Ljava/lang/String;

.field public static final BLE_UNLOCK_SUCCEED_ACTION:Ljava/lang/String; = "miui_keyguard_ble_unlock_succeed"

.field private static final DEBUG:Z = false

.field protected static final DEBUG_SIM_STATES:Z = false

.field private static final FAILED_BIOMETRIC_UNLOCK_ATTEMPTS_BEFORE_BACKUP:I = 0x3

.field public static final LOW_BATTERY_THRESHOLD:I = 0x14

.field private static final METHOD_GET_LOCK_WALLPAPER_INFO:Ljava/lang/String; = "getLockWallpaperInfo"

.field private static final METHOD_RESULT_JSON:Ljava/lang/String; = "result_json"

.field private static final MSG_BATTERY_UPDATE:I = 0x12e

.field private static final MSG_CARRIER_INFO_UPDATE:I = 0x12f

.field private static final MSG_CARRIER_INFO_UPDATE_SUB:I = 0x137

.field private static final MSG_CLOCK_VISIBILITY_CHANGED:I = 0x133

.field private static final MSG_DEVICE_PROVISIONED:I = 0x134

.field protected static final MSG_DPM_STATE_CHANGED:I = 0x135

.field private static final MSG_PHONE_STATE_CHANGED:I = 0x132

.field private static final MSG_RINGER_MODE_CHANGED:I = 0x131

.field private static final MSG_SIM_STATE_CHANGE:I = 0x130

.field private static final MSG_TIME_UPDATE:I = 0x12d

.field protected static final MSG_USER_CHANGED:I = 0x136

.field private static final TAG:Ljava/lang/String; = "MiuiKeyguardUpdateMonitor"

.field private static mIsInDemoMode:Z


# instance fields
.field private final FINGERPRINT_ERROR_LOCKOUT:I

.field private final FINGERPRINT_ERROR_LOCKOUT_PERMANENT_FOR_O:I

.field private mBLEUnlockState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

.field private mBatteryChargingView:Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;

.field private mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

.field private mClockVisible:Z

.field private mContentObserver:Landroid/database/ContentObserver;

.field private final mContext:Landroid/content/Context;

.field private mDemoModeObserver:Landroid/database/ContentObserver;

.field private mDeviceProvisioned:Z

.field private final mDismissRunnable:Ljava/lang/Runnable;

.field private mFailedAttempts:I

.field private mFailedBiometricUnlockAttempts:I

.field private mFingerId:I

.field private mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

.field mFingerprintIdentifyCallback:Lcom/android/keyguard/FingerprintIdentifyCallback;

.field private final mFingerprintStateCallbacks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

.field private mHandler:Landroid/os/Handler;

.field private mInFingerprintErrorLockOut:Z

.field private mInfoCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mIsLockScreenMagazine:Z

.field private mJustWirelessCharging:Z

.field private mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

.field private mLightKeyguardWallpaperBottom:Z

.field private mLightKeyguardWallpaperTop:Z

.field private mLockWallpaperProviderObserver:Landroid/database/ContentObserver;

.field private mLowBatteryThreshold:I

.field private mLowBatteryThresholdObserver:Landroid/database/ContentObserver;

.field private mMiuiWirelessChargeSlowlyView:Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;

.field private mMusicPlaying:Z

.field private final mMusicStateChangeListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mNeedRepositionDevice:Z

.field private mPasswordLockout:Z

.field private mPhoneSignalController:Lcom/android/keyguard/PhoneSignalController;

.field private mPhoneState:I

.field private mRegisterRemoteControllerSucceed:Z

.field private mRingMode:I

.field private final mScreenOffRunnable:Ljava/lang/Runnable;

.field private mScreenOn:Z

.field private mShowBLEUnlockAnimation:Z

.field private mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

.field private mSimStateCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mSkipSimStateChange:Z

.field private mStartingDissmissWirelessAlphaAnim:Z

.field private mTelephonyPlmn:[Ljava/lang/CharSequence;

.field private mTelephonySpn:[Ljava/lang/CharSequence;

.field private mWallpaperChangeCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

.field private final mWallpaperInfoListenerListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;",
            ">;"
        }
    .end annotation
.end field

.field private mWindowManager:Landroid/view/WindowManager;

.field private mWirelessChargeAnimator:Landroid/animation/ObjectAnimator;

.field private mWirelessChargeCallbackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mWirelessChargeView:Landroid/view/View;

.field private mWirelessCharging:Z

.field private mWirelessOnline:Z


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDeviceProvisioned:Z

    return v0
.end method

.method static synthetic -get10(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiKeyguardViewMediator;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    return-object v0
.end method

.method static synthetic -get11(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLowBatteryThreshold:I

    return v0
.end method

.method static synthetic -get12(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMiuiWirelessChargeSlowlyView:Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;

    return-object v0
.end method

.method static synthetic -get13(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOffRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get14(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOn:Z

    return v0
.end method

.method static synthetic -get15(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic -get16(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic -get17(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperChangeCallbacks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic -get18(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    return-object v0
.end method

.method static synthetic -get19(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperInfoListenerListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDismissRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get20(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/view/WindowManager;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic -get21(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/FingerprintHelper;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintStateCallbacks:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInFingerprintErrorLockOut:Z

    return v0
.end method

.method static synthetic -get8(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mIsLockScreenMagazine:Z

    return v0
.end method

.method static synthetic -get9(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mJustWirelessCharging:Z

    return v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDeviceProvisioned:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;I)I
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerId:I

    return p1
.end method

.method static synthetic -set10(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mStartingDissmissWirelessAlphaAnim:Z

    return p1
.end method

.method static synthetic -set11(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    return-object p1
.end method

.method static synthetic -set12(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeAnimator:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic -set13(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic -set2(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/FingerprintHelper;)Lcom/android/keyguard/FingerprintHelper;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    return-object p1
.end method

.method static synthetic -set3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    return-object p1
.end method

.method static synthetic -set4(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInFingerprintErrorLockOut:Z

    return p1
.end method

.method static synthetic -set5(Z)Z
    .locals 0

    sput-boolean p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mIsInDemoMode:Z

    return p0
.end method

.method static synthetic -set6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mIsLockScreenMagazine:Z

    return p1
.end method

.method static synthetic -set7(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mJustWirelessCharging:Z

    return p1
.end method

.method static synthetic -set8(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;I)I
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLowBatteryThreshold:I

    return p1
.end method

.method static synthetic -set9(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;)Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMiuiWirelessChargeSlowlyView:Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->ignoreFingerprintIdentificationResult()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->queryWallpaperInfo()Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap10(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleTimeUpdate()V

    return-void
.end method

.method static synthetic -wrap11(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->processKeyguardWallpaper()V

    return-void
.end method

.method static synthetic -wrap12(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setNeedRepositionDevice(Z)V

    return-void
.end method

.method static synthetic -wrap13(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->showWirelessChargeSlowly()V

    return-void
.end method

.method static synthetic -wrap14(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->startDismissWirelessAlphaAnim()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getTelephonyPlmnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getTelephonySpnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap4(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->checkBatteryStatus(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->checkIfShowWirelessChargeSlowly()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleBatteryUpdate(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleCarrierInfoUpdate(I)V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleClockVisibilityChanged()V

    return-void
.end method

.method static synthetic -wrap9(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleSimStateChange(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mIsInDemoMode:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ro.miui.product.home"

    const-string/jumbo v2, "com.miui.home"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".launcher.settings"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->AUTHORITY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 17

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSkipSimStateChange:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedAttempts:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mShowBLEUnlockAnimation:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLightKeyguardWallpaperTop:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLightKeyguardWallpaperBottom:Z

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimStateCallbacks:Ljava/util/ArrayList;

    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperChangeCallbacks:Ljava/util/ArrayList;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneSignalController:Lcom/android/keyguard/PhoneSignalController;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$1;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLockWallpaperProviderObserver:Landroid/database/ContentObserver;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperInfoListenerListeners:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicStateChangeListeners:Ljava/util/List;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessOnline:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessCharging:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeCallbackList:Ljava/util/List;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mNeedRepositionDevice:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOn:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mStartingDissmissWirelessAlphaAnim:Z

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$2;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDismissRunnable:Ljava/lang/Runnable;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$3;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOffRunnable:Ljava/lang/Runnable;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintStateCallbacks:Ljava/util/Set;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    const/4 v2, 0x7

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->FINGERPRINT_ERROR_LOCKOUT:I

    const/16 v2, 0x9

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->FINGERPRINT_ERROR_LOCKOUT_PERMANENT_FOR_O:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInFingerprintErrorLockOut:Z

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintIdentifyCallback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$5;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "device_provisioned"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDeviceProvisioned:Z

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDeviceProvisioned:Z

    if-nez v2, :cond_0

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$6;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$6;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "device_provisioned"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "device_provisioned"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v15, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDeviceProvisioned:Z

    if-eq v15, v2, :cond_0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDeviceProvisioned:Z

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDeviceProvisioned:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x134

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    const/4 v3, 0x1

    const/16 v4, 0x64

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v14

    new-array v2, v14, [Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    new-array v2, v14, [Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    new-array v2, v14, [Lcom/android/internal/telephony/IccCardConstants$State;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    const/4 v12, 0x0

    :goto_2
    if-ge v12, v14, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getDefaultPlmn()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v3, v2, v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    aput-object v3, v2, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v15, 0x0

    goto :goto_1

    :cond_3
    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v2, "android.intent.action.TIME_TICK"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.action.TIME_SET"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.action.PHONE_STATE"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v2, "android.intent.action.USER_REMOVED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v3, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    const/4 v12, 0x0

    :goto_3
    if-ge v12, v14, :cond_6

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2, v12}, Lmiui/telephony/TelephonyManager;->getSimStateForSlot(I)I

    move-result v16

    const/4 v2, 0x2

    move/from16 v0, v16

    if-ne v2, v0, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    aput-object v3, v2, v12

    :cond_4
    :goto_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :cond_5
    const/4 v2, 0x3

    move/from16 v0, v16

    if-ne v2, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    aput-object v3, v2, v12

    goto :goto_4

    :cond_6
    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$8;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$8;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLowBatteryThresholdObserver:Landroid/database/ContentObserver;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$9;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$9;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDemoModeObserver:Landroid/database/ContentObserver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MiuiSettings$Secure;->UNLOCK_FAILED_ATTEMPTS:Ljava/lang/String;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    const/4 v6, 0x0

    invoke-static {v2, v3, v6, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setFailedAttempts(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLowBatteryThresholdObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "battery_level_low_customized"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLowBatteryThresholdObserver:Landroid/database/ContentObserver;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDemoModeObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "sysui_tuner_demo_on"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDemoModeObserver:Landroid/database/ContentObserver;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "lock_wallpaper_provider_authority"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLockWallpaperProviderObserver:Landroid/database/ContentObserver;

    const/4 v6, 0x0

    const/4 v7, -0x1

    invoke-virtual {v2, v3, v6, v4, v7}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLockWallpaperProviderObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/database/ContentObserver;->onChange(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->processKeyguardWallpaper()V

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$10;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v2, "com.miui.keyguard.setwallpaper"

    invoke-virtual {v9, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v7, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    sget-boolean v2, Lmiui/os/Build;->IS_CM_CUSTOMIZATION:Z

    if-eqz v2, :cond_7

    new-instance v2, Lcom/android/keyguard/PhoneSignalController;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/android/keyguard/PhoneSignalController;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneSignalController:Lcom/android/keyguard/PhoneSignalController;

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerRemoteController()V

    new-instance v13, Lcom/android/keyguard/LockPatternUtilsWrapper;

    new-instance v2, Landroid/security/MiuiLockPatternUtils;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-direct {v13, v2}, Lcom/android/keyguard/LockPatternUtilsWrapper;-><init>(Lcom/android/internal/widget/LockPatternUtils;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {v13}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isSecure()Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->NEED_PASSWORD:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    :goto_5
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    const-string/jumbo v2, "miui_keyguard_fingerprint"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "FingerprintIdentificationState is"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_8
    sget-object v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->WAITING_FOR_INPUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    goto :goto_5
.end method

.method private checkBatteryStatus(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)V
    .locals 9

    const/4 v8, 0x4

    const/4 v5, 0x0

    const/4 v7, 0x2

    iget v0, p1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->plugged:I

    iget v2, p1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->status:I

    invoke-static {p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPluggedIn(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v1

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessOnline:Z

    if-nez v4, :cond_0

    if-eqz v1, :cond_0

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mJustWirelessCharging:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    if-ne v0, v8, :cond_0

    iget v4, p1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    iget-boolean v6, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOn:Z

    invoke-direct {p0, v4, v6}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->showWirelessChargeAnimation(IZ)V

    :cond_0
    if-ne v0, v8, :cond_4

    const/4 v4, 0x1

    :goto_0
    iput-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessOnline:Z

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessOnline:Z

    if-eqz v4, :cond_5

    if-ne v2, v7, :cond_5

    const/4 v3, 0x1

    :goto_1
    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessCharging:Z

    if-eqz v4, :cond_2

    xor-int/lit8 v4, v3, 0x1

    if-eqz v4, :cond_2

    if-ne v2, v7, :cond_6

    const v4, 0x7f0b000b

    :goto_2
    invoke-direct {p0, v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->showToast(I)V

    invoke-direct {p0, v5}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setNeedRepositionDevice(Z)V

    if-ne v2, v7, :cond_1

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMiuiWirelessChargeSlowlyView:Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMiuiWirelessChargeSlowlyView:Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiWirelessChargeSlowlyView;->dismiss()V

    :cond_1
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->startDismissWirelessAlphaAnim()V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOffRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_2
    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessCharging:Z

    if-nez v4, :cond_3

    if-eqz v3, :cond_3

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->checkWirelessChargeEfficiency()V

    :cond_3
    iput-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessCharging:Z

    return-void

    :cond_4
    move v4, v5

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    :cond_6
    const v4, 0x7f0b000a

    goto :goto_2
.end method

.method private checkIfShowWirelessChargeSlowly()V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$18;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$18;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$18;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private checkWirelessChargeEfficiency()V
    .locals 3

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$17;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$17;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$17;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private getDefaultPlmn()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x104030a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getTelephonyPlmnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 4

    const/4 v3, 0x0

    const-string/jumbo v1, "showPlmn"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "plmn"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getDefaultPlmn()Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1

    :cond_1
    return-object v3
.end method

.method private getTelephonySpnFrom(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 4

    const/4 v3, 0x0

    const-string/jumbo v1, "showSpn"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "spn"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    return-object v3
.end method

.method private handleBatteryUpdate(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)V
    .locals 6

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    invoke-static {v2, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isBatteryUpdateInteresting(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->shouldShowBatteryInfo()Z

    move-result v3

    invoke-static {p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPluggedIn(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v4

    iget v5, p1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    invoke-interface {v2, v3, v4, v5}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onRefreshBatteryInfo(ZZI)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private handleCarrierInfoUpdate(I)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->isMultiSimEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    aget-object v3, v3, p1

    invoke-interface {v1, v2, v3, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    aget-object v3, v3, p1

    invoke-interface {v1, v2, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private handleClockVisibilityChanged()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onClockVisibilityChanged()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private handleSimStateChange(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;)V
    .locals 4

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSkipSimStateChange:Z

    if-eqz v3, :cond_0

    return-void

    :cond_0
    iget-object v1, p1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;->simState:Lcom/android/internal/telephony/IccCardConstants$State;

    iget v2, p1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;->subscription:I

    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->UNKNOWN:Lcom/android/internal/telephony/IccCardConstants$State;

    if-eq v1, v3, :cond_2

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v3, v3, v2

    if-eq v1, v3, :cond_2

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aput-object v1, v3, v2

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimStateCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/telephony/TelephonyManager;->isMultiSimEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimStateCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;

    invoke-interface {v3, v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimStateCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;

    invoke-interface {v3, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private handleTimeUpdate()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onTimeChanged()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private ignoreFingerprintIdentificationResult()Z
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MiuiSettings$Secure;->FIND_DEVICE_PIN_LOCK:Ljava/lang/String;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    invoke-static {v2, v3, v5, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-ne v2, v6, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimLocked()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPasswordLockout:Z

    if-eqz v2, :cond_2

    :cond_0
    const-string/jumbo v2, "miui_keyguard_fingerprint"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "ignoreFingerprintIdentificationResult: isSimLocked="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";isLockByFindDevice="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";passwordLockout="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPasswordLockout:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    return v5
.end method

.method private static isBatteryLow(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z
    .locals 2

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isBatteryUpdateInteresting(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z
    .locals 6

    const/4 v5, 0x1

    invoke-static {p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPluggedIn(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v0

    invoke-static {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPluggedIn(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->status:I

    iget v4, p1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->status:I

    if-eq v3, v4, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-ne v2, v0, :cond_0

    if-eqz v1, :cond_3

    :cond_0
    return v5

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    iget v4, p1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    if-eq v3, v4, :cond_4

    return v5

    :cond_4
    if-nez v0, :cond_5

    invoke-static {p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isBatteryLow(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, p1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    iget v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    if-eq v3, v4, :cond_5

    return v5

    :cond_5
    const/4 v3, 0x0

    return v3
.end method

.method private static isPluggedIn(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z
    .locals 2

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->plugged:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->plugged:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    sget-boolean v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mIsInDemoMode:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->plugged:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyNeedRepositionDevice(Z)V
    .locals 4

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeCallbackList:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeCallbackList:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;->onNeedRepositionDevice(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_0
    monitor-exit v3

    return-void
.end method

.method private prepareWirelessChargeView(Z)V
    .locals 6

    const/4 v4, 0x0

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    const v3, 0x7f030003

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    const/16 v3, 0x1300

    invoke-virtual {v2, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    const v3, 0x7f0d001a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;

    iput-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryChargingView:Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d9

    const v4, 0x5030580

    const/4 v5, -0x2

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$13;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$13;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method private processKeyguardWallpaper()V
    .locals 13

    const/4 v12, 0x3

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-object v6, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lmiui/content/res/ThemeResources;->getLockWallpaperCache(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_1

    check-cast v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/lit8 v6, v2, 0x1

    div-int/lit8 v6, v6, 0x3

    div-int/lit8 v9, v1, 0x5

    mul-int/lit8 v10, v2, 0x1

    div-int/lit8 v10, v10, 0x3

    mul-int/lit8 v11, v1, 0x1

    div-int/lit8 v11, v11, 0x5

    invoke-static {v4, v6, v9, v10, v11}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v3

    mul-int/lit8 v6, v1, 0x9

    div-int/lit8 v6, v6, 0xa

    div-int/lit8 v9, v1, 0xa

    invoke-static {v4, v8, v6, v2, v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v3, :cond_0

    invoke-static {v3, v12}, Lmiui/graphics/BitmapFactory;->getBitmapColorMode(Landroid/graphics/Bitmap;I)I

    move-result v6

    if-eqz v6, :cond_2

    move v6, v7

    :goto_0
    iput-boolean v6, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLightKeyguardWallpaperTop:Z

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v0, v12}, Lmiui/graphics/BitmapFactory;->getBitmapColorMode(Landroid/graphics/Bitmap;I)I

    move-result v6

    if-eqz v6, :cond_3

    :goto_1
    iput-boolean v7, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLightKeyguardWallpaperBottom:Z

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    return-void

    :cond_2
    move v6, v8

    goto :goto_0

    :cond_3
    move v7, v8

    goto :goto_1
.end method

.method private queryWallpaperInfo()Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;
    .locals 9

    iget-boolean v5, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mIsLockScreenMagazine:Z

    if-nez v5, :cond_0

    new-instance v5, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    invoke-direct {v5}, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;-><init>()V

    return-object v5

    :cond_0
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "content://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v6

    invoke-static {v5, v6}, Lcom/android/keyguard/MiuiKeyguardUtils;->maybeAddUserId(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v5

    const-string/jumbo v6, "getLockWallpaperInfo"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v1, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    const-string/jumbo v5, "MiuiKeyguardUpdateMonitor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "bundlestring="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "result_json"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "result_json"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    invoke-virtual {v3, v5, v6}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    :cond_1
    if-nez v4, :cond_2

    new-instance v4, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    invoke-direct {v4}, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;-><init>()V

    :cond_2
    iget-boolean v5, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mIsLockScreenMagazine:Z

    iput-boolean v5, v4, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->isLockScreenMagazine:Z

    invoke-virtual {v4}, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->setLinkType()V

    return-object v4

    :catch_0
    move-exception v2

    const-string/jumbo v5, "MiuiKeyguardUpdateMonitor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "call error  uri ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private setNeedRepositionDevice(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mNeedRepositionDevice:Z

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->notifyNeedRepositionDevice(Z)V

    return-void
.end method

.method private showToast(I)V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v1, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x7d9

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setType(I)V

    invoke-virtual {v0}, Landroid/widget/Toast;->getWindowParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private showWirelessChargeAnimation(IZ)V
    .locals 10

    const/4 v9, 0x1

    const/16 v8, 0x24e0

    const/4 v7, 0x0

    iput-boolean v9, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mJustWirelessCharging:Z

    invoke-direct {p0, v7}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->prepareWirelessChargeView(Z)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    const/16 v1, 0x5a0

    const/16 v3, 0x1b58

    const/16 v2, 0x3e8

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryChargingView:Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;

    const-string/jumbo v5, "time"

    const/4 v6, 0x2

    new-array v6, v6, [I

    aput v7, v6, v7

    aput v8, v6, v9

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v4

    int-to-long v6, v8

    invoke-virtual {v4, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeAnimator:Landroid/animation/ObjectAnimator;

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeAnimator:Landroid/animation/ObjectAnimator;

    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    if-nez p2, :cond_0

    iget-boolean v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mNeedRepositionDevice:Z

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOffRunnable:Ljava/lang/Runnable;

    const/16 v6, 0x2418

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$14;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$14;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    new-instance v5, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$15;

    invoke-direct {v5, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$15;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryChargingView:Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;

    invoke-virtual {v4, p1}, Lcom/android/keyguard/MiuiKeyguardWirelessChargingView;->setChargingProgress(I)V

    return-void
.end method

.method private showWirelessChargeSlowly()V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$19;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$19;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private startDismissWirelessAlphaAnim()V
    .locals 8

    const/4 v7, 0x1

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mJustWirelessCharging:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mStartingDissmissWirelessAlphaAnim:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeView:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    aput v5, v4, v6

    const/4 v5, 0x0

    aput v5, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$16;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$16;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iput-boolean v7, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mStartingDissmissWirelessAlphaAnim:Z

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_1
    return-void
.end method


# virtual methods
.method public OnDoubleClickHome()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    invoke-virtual {v0}, Lcom/android/keyguard/FingerprintHelper;->OnDoubleClickHome()V

    :cond_0
    return-void
.end method

.method public clearFailedAttempts()V
    .locals 4

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedAttempts:I

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$Secure;->UNLOCK_FAILED_ATTEMPTS:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v2

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    return-void
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLowBatteryThresholdObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDemoModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method public getBLEUnlockState()Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBLEUnlockState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    return-object v0
.end method

.method public getBatteryLevel()I
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    iget v0, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    return v0
.end method

.method public getFailedAttempts()I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedAttempts:I

    return v0
.end method

.method public getFingerId()I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerId:I

    return v0
.end method

.method public getFingerprintIdentificationState()Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    return-object v0
.end method

.method public getMaxBiometricUnlockAttemptsReached()Z
    .locals 2

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPhoneState()I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneState:I

    return v0
.end method

.method public getSimState(I)Lcom/android/internal/telephony/IccCardConstants$State;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getTelephonyPlmn(I)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getTelephonySpn(I)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getWallpaperInfo()Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    return-object v0
.end method

.method protected handleDevicePolicyManagerStateChanged()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onDevicePolicyManagerStateChanged()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected handleDeviceProvisioned()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onDeviceProvisioned()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContentObserver:Landroid/database/ContentObserver;

    :cond_1
    return-void
.end method

.method protected handlePhoneStateChanged(Ljava/lang/String;)V
    .locals 3

    sget-object v1, Lmiui/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneState:I

    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneState:I

    invoke-interface {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onPhoneStateChanged(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    sget-object v1, Lmiui/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneState:I

    goto :goto_0

    :cond_2
    sget-object v1, Lmiui/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneState:I

    goto :goto_0

    :cond_3
    return-void
.end method

.method protected handleRingerModeChange(I)V
    .locals 2

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mRingMode:I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    invoke-interface {v1, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onRingerModeChanged(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected handleUserChanged(I)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;

    invoke-interface {v1, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onUserChanged(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->notifyMusicStateChange(Z)V

    return-void
.end method

.method public initMusicState()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicPlaying:Z

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelper;->isMusicPlaying(Landroid/content/Context;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicPlaying:Z

    return-void
.end method

.method public isClockVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mClockVisible:Z

    return v0
.end method

.method public isDeviceCharged()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    iget v1, v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->status:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    iget v1, v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    const/16 v2, 0x64

    if-lt v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDevicePluggedIn()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPluggedIn(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v0

    return v0
.end method

.method public isDeviceProvisioned()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mDeviceProvisioned:Z

    return v0
.end method

.method public isLightWallpaperBottom()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLightKeyguardWallpaperBottom:Z

    return v0
.end method

.method public isLightWallpaperTop()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mLightKeyguardWallpaperTop:Z

    return v0
.end method

.method public isMusicActive()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicPlaying:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPasswordLockout()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPasswordLockout:Z

    return v0
.end method

.method public isSimLocked()Z
    .locals 2

    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimLocked(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public isSimLocked(I)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimSecured(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v1, v1, p1

    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSimMissing()Z
    .locals 2

    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimMissing(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public isSimMissing(I)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v1, v1, p1

    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v1, v1, p1

    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSimSecured()Z
    .locals 2

    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimSecured(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public isSimSecured(I)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v1, v1, p1

    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v1, v1, p1

    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyMusicStateChange(Z)V
    .locals 3

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicPlaying:Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicStateChangeListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;->musicStateChange(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onScreenTurnedOff()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOn:Z

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->supportWirelessCharge()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->prepareWirelessChargeView(Z)V

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->startDismissWirelessAlphaAnim()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOffRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onScreenTurnedOn()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mScreenOn:Z

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->supportWirelessCharge()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$12;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$12;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public registerFingerprintStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintStateCallbacks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintStateCallbacks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-interface {p1, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;->onFingerprintStateChanged(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)V

    :cond_0
    return-void
.end method

.method public registerInfoCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;)V
    .locals 5

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->shouldShowBatteryInfo()Z

    move-result v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPluggedIn(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v3

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    iget v4, v4, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    invoke-interface {p1, v2, v3, v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onRefreshBatteryInfo(ZZI)V

    invoke-interface {p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onTimeChanged()V

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mRingMode:I

    invoke-interface {p1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onRingerModeChanged(I)V

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneState:I

    invoke-interface {p1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onPhoneStateChanged(I)V

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/telephony/TelephonyManager;->isMultiSimEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    aget-object v3, v3, v0

    invoke-interface {p1, v2, v3, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonyPlmn:[Ljava/lang/CharSequence;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mTelephonySpn:[Ljava/lang/CharSequence;

    aget-object v3, v3, v1

    invoke-interface {p1, v2, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_1
    invoke-interface {p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;->onClockVisibilityChanged()V

    :cond_2
    return-void
.end method

.method public registerMusicStateChangeListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;)V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicStateChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "MiuiKeyguardUpdateMonitor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "registerMusicStateChangeListener "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicPlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicPlaying:Z

    invoke-interface {p1, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;->musicStateChange(Z)V

    return-void
.end method

.method public registerPhoneSignalChangeCallback(Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneSignalController:Lcom/android/keyguard/PhoneSignalController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneSignalController:Lcom/android/keyguard/PhoneSignalController;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/PhoneSignalController;->registerPhoneSignalChangeCallback(Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;)V

    :cond_0
    return-void
.end method

.method public registerRemoteController()V
    .locals 3

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mRegisterRemoteControllerSucceed:Z

    if-eqz v1, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-static {v1, p0}, Lcom/android/keyguard/KeyguardCompatibilityHelper;->registerRemoteController(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mRegisterRemoteControllerSucceed:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mRegisterRemoteControllerSucceed:Z

    goto :goto_0
.end method

.method public registerSimStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;)V
    .locals 3

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimStateCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimStateCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/telephony/TelephonyManager;->isMultiSimEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v2, v2, v0

    invoke-interface {p1, v2, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/telephony/SubscriptionManager;->getDefaultSlotId()I

    move-result v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v2, v2, v1

    invoke-interface {p1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimStateCallback;->onSimStateChanged(Lcom/android/internal/telephony/IccCardConstants$State;)V

    :cond_1
    return-void
.end method

.method public registerWallpaperChangeCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperChangeCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperChangeCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;->onWallpaperChange(Z)V

    :cond_0
    return-void
.end method

.method public registerWallpaperInfoListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperInfoListenerListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    invoke-interface {p1, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;->updateWallPaperInfo(Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;)V

    return-void
.end method

.method public registerWirelessChargeCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;)V
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeCallbackList:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeCallbackList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mNeedRepositionDevice:Z

    invoke-interface {p1, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;->onNeedRepositionDevice(Z)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public removeCallback(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mInfoCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimStateCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperChangeCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneSignalController:Lcom/android/keyguard/PhoneSignalController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPhoneSignalController:Lcom/android/keyguard/PhoneSignalController;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/PhoneSignalController;->removeCallback(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public reportClockVisible(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mClockVisible:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x133

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public reportFailedAttempt()V
    .locals 4

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedAttempts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedAttempts:I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$Secure;->UNLOCK_FAILED_ATTEMPTS:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v2

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    return-void
.end method

.method public reportFailedBiometricUnlockAttempt()V
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedBiometricUnlockAttempts:I

    return-void
.end method

.method public reportSimUnlocked(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->READY:Lcom/android/internal/telephony/IccCardConstants$State;

    aput-object v1, v0, p1

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSimState:[Lcom/android/internal/telephony/IccCardConstants$State;

    aget-object v1, v1, p1

    invoke-direct {v0, v1, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;-><init>(Lcom/android/internal/telephony/IccCardConstants$State;I)V

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->handleSimStateChange(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;)V

    return-void
.end method

.method public resetFingerLockoutTime()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/keyguard/FingerprintHelper;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/keyguard/FingerprintHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    invoke-virtual {v0}, Lcom/android/keyguard/FingerprintHelper;->resetFingerLockoutTime()V

    return-void
.end method

.method public setBLEUnlockState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;)V
    .locals 3

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBLEUnlockState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->SUCCEED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "miui_keyguard_ble_unlock_succeed"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :cond_0
    return-void
.end method

.method public setFailedAttempts(I)V
    .locals 0

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFailedAttempts:I

    return-void
.end method

.method public setFingerprintIdentificationState(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    return-void
.end method

.method public setPasswordLockout(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mPasswordLockout:Z

    return-void
.end method

.method public setShowBLEUnlockAnimation(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mShowBLEUnlockAnimation:Z

    return-void
.end method

.method public setSkipSimStateChange(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mSkipSimStateChange:Z

    return-void
.end method

.method public shouldShowBatteryInfo()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPluggedIn(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mBatteryStatus:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isBatteryLow(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-boolean v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mIsInDemoMode:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showBLEUnlockAnimation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mShowBLEUnlockAnimation:Z

    return v0
.end method

.method public startFingerprintIdentify()Z
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->stopFingerprintIdentify()V

    new-instance v1, Lcom/android/keyguard/FingerprintHelper;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/keyguard/FingerprintHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    invoke-virtual {v1}, Lcom/android/keyguard/FingerprintHelper;->getFingerprintIds()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string/jumbo v1, "miui_keyguard_fingerprint"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start fingerprint identify failed because ids = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v4

    :cond_1
    const-string/jumbo v1, "miui_keyguard_fingerprint"

    const-string/jumbo v2, "start fingerprint identify successful"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintIdentifyCallback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    invoke-virtual {v1, v2, v0, p0}, Lcom/android/keyguard/FingerprintHelper;->identify(Lcom/android/keyguard/FingerprintIdentifyCallback;Ljava/util/List;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    const/4 v1, 0x1

    return v1
.end method

.method public stopFingerprintIdentify()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "stop fingerprint identify"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    invoke-virtual {v0}, Lcom/android/keyguard/FingerprintHelper;->cancelIdentify()V

    iput-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintHelper:Lcom/android/keyguard/FingerprintHelper;

    :cond_0
    return-void
.end method

.method public unregisterFingerprintStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintStateCallbacks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public unregisterMusicStateChangeListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$MusicStateChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mMusicStateChangeListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public unregisterWallpaperInfoListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWallpaperInfoListenerListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public unregisterWirelessChargeCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WirelessChargeCallback;)V
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeCallbackList:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mWirelessChargeCallbackList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
