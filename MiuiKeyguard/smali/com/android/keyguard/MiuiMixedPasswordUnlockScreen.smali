.class public Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;
.super Lcom/android/keyguard/MiuiCommonUnlockScreen;
.source "MiuiMixedPasswordUnlockScreen.java"


# static fields
.field private static final MINIMUM_PASSWORD_LENGTH_BEFORE_REPORT:I = 0x3


# instance fields
.field private mKeyboardView:Lmiui/view/MiuiKeyBoardView;

.field private mMixedPasswordUnlockScreen:Landroid/view/View;

.field private mPasswordEditText:Landroid/widget/EditText;

.field private final mVibrator:Landroid/os/Vibrator;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;IZIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->onPasswordChecked(IZIZ)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->verifyPassword(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/android/keyguard/MiuiCommonUnlockScreen;-><init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const-string/jumbo v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->initView()V

    return-void
.end method

.method private handleWrongPassword(IZ)V
    .locals 10

    const-wide/16 v4, 0x0

    iget-object v3, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v3}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportFailedUnlockAttempt()V

    iget-object v3, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v6, 0x96

    invoke-virtual {v3, v6, v7}, Landroid/os/Vibrator;->vibrate(J)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const-string/jumbo v6, ""

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const v6, 0x7f0b002c

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setHint(I)V

    const-wide/16 v0, 0x0

    iget-object v3, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v3

    const/4 v6, 0x5

    if-lt v3, v6, :cond_4

    iget-object v3, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v3, v2}, Lcom/android/internal/widget/LockPatternUtils;->setKeyguardLockoutAttemptDeadline(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleAttemptLockout(J)V

    :cond_0
    :goto_0
    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    if-eqz p1, :cond_3

    :cond_1
    const-string/jumbo v3, "miui_keyguard_fingerprint"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "lockout timeoutMs="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ";deadline="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    cmp-long v7, v0, v4

    if-lez v7, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v4, v0, v4

    :cond_2
    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    if-eqz p1, :cond_0

    int-to-long v6, p1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-virtual {p0, v6, v7}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleAttemptLockout(J)V

    int-to-long v6, p1

    invoke-virtual {p0, v6, v7}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->writePasswordOutTime(J)V

    goto :goto_0
.end method

.method private initView()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mContext:Landroid/content/Context;

    const v1, 0x7f030018

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mMixedPasswordUnlockScreen:Landroid/view/View;

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mMixedPasswordUnlockScreen:Landroid/view/View;

    const v1, 0x7f0d0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->processEmergencyCall(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mMixedPasswordUnlockScreen:Landroid/view/View;

    const v1, 0x7f0d0077

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const v1, 0x800081

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mMixedPasswordUnlockScreen:Landroid/view/View;

    const v1, 0x7f0d007a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiui/view/MiuiKeyBoardView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mKeyboardView:Lmiui/view/MiuiKeyBoardView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mKeyboardView:Lmiui/view/MiuiKeyBoardView;

    new-instance v1, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$1;-><init>(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;)V

    invoke-virtual {v0, v1}, Lmiui/view/MiuiKeyBoardView;->addKeyboardListener(Lmiui/view/MiuiKeyBoardView$OnKeyboardActionListener;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mMixedPasswordUnlockScreen:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->addUnlockView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleAttemptLockoutIfNeed()V

    return-void
.end method

.method private onPasswordChecked(IZIZ)V
    .locals 3

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";matched="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";timeoutMs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->isNeedCloseSdcardFs(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleNeedCloseSdcardFs()V

    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->isFBEDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleFirstMatchSpacePassword()V

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleWrongPassword(IZ)V

    return-void

    :cond_1
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v0

    if-eq p1, v0, :cond_3

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->isPhoneCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can\'t switch user to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " when calling"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleSwitchUserWhenCalling()V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->isGreenKidActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can\'t switch user to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " when green kid active"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleWrongPassword(IZ)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleSwitchUserWhenGreenkidActive()V

    return-void

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->switchUser(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->keyguardDone(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_PW:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->recordUnlockWay(Ljava/lang/String;)V

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "unlock by mixed password"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_4
    invoke-direct {p0, p3, p4}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->handleWrongPassword(IZ)V

    goto :goto_0
.end method

.method private verifyPassword(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_1

    invoke-virtual {p0, v3}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->setPasswordEntryInputEnabled(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    invoke-virtual {v1, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    iget-object v2, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;

    invoke-direct {v3, p0, v0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen$2;-><init>(Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;I)V

    invoke-static {v1, p1, v2, v3}, Lcom/android/keyguard/LockPatternChecker;->checkPasswordForUsers(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/lang/String;Landroid/content/Context;Lcom/android/keyguard/OnCheckForUsersCallback;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method


# virtual methods
.method public applyAnimation()V
    .locals 14

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->applyAnimation()V

    new-instance v13, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v13, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/high16 v3, 0x40000000    # 2.0f

    move v2, v1

    move v4, v1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    new-instance v4, Landroid/view/animation/TranslateAnimation;

    move v7, v5

    move v8, v6

    move v9, v5

    move v10, v1

    move v11, v5

    move v12, v6

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v13, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v13, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v13, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mKeyboardView:Lmiui/view/MiuiKeyBoardView;

    invoke-virtual {v1, v13}, Lmiui/view/MiuiKeyBoardView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public cleanUp()V
    .locals 0

    return-void
.end method

.method public bridge synthetic dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected handleAttemptLockout(J)V
    .locals 5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->showLockoutView(J)V

    return-void
.end method

.method public bridge synthetic handleAttemptLockoutIfNeed()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->handleAttemptLockoutIfNeed()V

    return-void
.end method

.method protected hideLockoutView()V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->hideLockoutView()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mKeyboardView:Lmiui/view/MiuiKeyBoardView;

    invoke-virtual {v0, v1}, Lmiui/view/MiuiKeyBoardView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mMixedPasswordUnlockScreen:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public bridge synthetic needPasswordCheck(ZI)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->needPasswordCheck(ZI)Z

    move-result v0

    return v0
.end method

.method public needsInput()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic onFaceUnlockFailed()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onFaceUnlockFailed()V

    return-void
.end method

.method public bridge synthetic onFaceUnlockHelp(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onFaceUnlockHelp(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onFaceUnlockStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onFaceUnlockStart()V

    return-void
.end method

.method public bridge synthetic onFaceUnlockSuccess()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onFaceUnlockSuccess()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onPause()V

    return-void
.end method

.method public onResume(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->asyncHideLockViewOnResume()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->onResume()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const v1, 0x7f0b003e

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method

.method protected onScreenOrientationChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->initView()V

    return-void
.end method

.method public bridge synthetic processEmergencyCall(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->processEmergencyCall(Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic resetFingerprintLockoutText()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->resetFingerprintLockoutText()V

    return-void
.end method

.method public bridge synthetic setHidden(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->setHidden(Z)V

    return-void
.end method

.method protected setPasswordEntryInputEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method public bridge synthetic setVisibility(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->setVisibility(I)V

    return-void
.end method

.method public bridge synthetic showBLEUnlockSucceedView()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->showBLEUnlockSucceedView()V

    return-void
.end method

.method protected showLockoutView(J)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->showLockoutView(J)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mKeyboardView:Lmiui/view/MiuiKeyBoardView;

    invoke-virtual {v0, v1}, Lmiui/view/MiuiKeyBoardView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->mMixedPasswordUnlockScreen:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected updateEmergencyCallButtonVisibility(Z)V
    .locals 2

    const v0, 0x7f0d0079

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public bridge synthetic writePasswordOutTime(J)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->writePasswordOutTime(J)V

    return-void
.end method
