.class Lcom/android/keyguard/MiuiCommonUnlockScreen$18;
.super Ljava/lang/Object;
.source "MiuiCommonUnlockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiCommonUnlockScreen;->showBLEUnlockSucceedView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

.field final synthetic val$dlg:Landroid/app/Dialog;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;Landroid/app/Dialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$18;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iput-object p2, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$18;->val$dlg:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_BAND:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->recordUnlockWay(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$18;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->keyguardDone(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$18;->val$dlg:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method
