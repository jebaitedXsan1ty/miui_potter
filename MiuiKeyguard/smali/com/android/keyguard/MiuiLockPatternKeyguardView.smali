.class public Lcom/android/keyguard/MiuiLockPatternKeyguardView;
.super Lcom/android/keyguard/MiuiKeyguardViewBase;
.source "MiuiLockPatternKeyguardView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiLockPatternKeyguardView$1;,
        Lcom/android/keyguard/MiuiLockPatternKeyguardView$2;,
        Lcom/android/keyguard/MiuiLockPatternKeyguardView$3;,
        Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;,
        Lcom/android/keyguard/MiuiLockPatternKeyguardView$5;,
        Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;,
        Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;,
        Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;
    }
.end annotation


# static fields
.field private static final synthetic -com-android-keyguard-MiuiLockPatternKeyguardView$UnlockModeSwitchesValues:[I = null

.field static final ACTION_EMERGENCY_DIAL:Ljava/lang/String; = "com.android.phone.EmergencyDialer.DIAL"

.field private static final DEBUG:Z = false

.field static final DEBUG_CONFIGURATION:Z = false

.field protected static final EMERGENCY_CALL_TIMEOUT:I = 0x2710

.field public static final FAILED_ATTEMPTS_BEFORE_TIMEOUT:I = 0x5

.field private static final KEYGUARD_SHOWING_PROPERTY:Ljava/lang/String; = "sys.is_keyguard_showing"

.field private static final TAG:Ljava/lang/String; = "LockPatternKeyguardView"

.field private static sIsFirstAppearanceAfterBoot:Z

.field public static sScreenTurnedOnTime:J

.field public static sWakeupByNotification:Z


# instance fields
.field protected final BIOMETRIC_AREA_EMERGENCY_DIALER_TIMEOUT:I

.field private mBackDown:Z

.field private mBackgroundImageView:Landroid/widget/ImageSwitcher;

.field protected mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

.field private final mBiometricUnlockStartupLock:Ljava/lang/Object;

.field protected mConfiguration:Landroid/content/res/Configuration;

.field private mFaceUnlockByNotificationScreenOn:Z

.field private final mFaceUnlockCallback:Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;

.field private mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

.field private mFaceUnlockView:Landroid/view/TextureView;

.field private mGaussBlurView:Landroid/widget/ImageView;

.field mGaussBlurViewFadeAnim:Landroid/animation/ObjectAnimator;

.field private final mImageViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

.field mInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

.field private final mInitTime:J

.field private mIsDefaultLockScreen:Z

.field private mIsLockPasswordEnabled:Z

.field private mIsLockPatternEnabled:Z

.field protected mIsVerifyUnlockOnly:Z

.field mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

.field protected final mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

.field protected mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

.field private mLockScreen:Landroid/view/View;

.field private mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

.field private mNewPassword:Z

.field private mPluggedIn:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field protected mRecreateRunnable:Ljava/lang/Runnable;

.field protected mRequiresSim:Z

.field protected mSavedState:Landroid/os/Parcelable;

.field private mScreenOn:Z

.field private mShowLockBeforeUnlock:Z

.field private mSimSlotIdToUnlock:I

.field protected mSuppressBiometricUnlock:Z

.field private final mSuppressBiometricUnlockInitState:Z

.field private mTorchCover:Landroid/view/View;

.field private mTorchStateReceiver:Landroid/database/ContentObserver;

.field private mUnlockScreen:Landroid/view/View;

.field protected mUnlockScreenMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

.field protected final mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field private final mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

.field protected final mWindowController:Lcom/android/keyguard/MiuiKeyguardWindowController;

.field private mWindowFocused:Z


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockCallback:Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/TextureView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockView:Landroid/view/TextureView;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/LockPatternUtilsWrapper;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mPluggedIn:Z

    return v0
.end method

.method static synthetic -get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    return-object v0
.end method

.method private static synthetic -getcom-android-keyguard-MiuiLockPatternKeyguardView$UnlockModeSwitchesValues()[I
    .locals 3

    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-com-android-keyguard-MiuiLockPatternKeyguardView$UnlockModeSwitchesValues:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-com-android-keyguard-MiuiLockPatternKeyguardView$UnlockModeSwitchesValues:[I

    return-object v0

    :cond_0
    invoke-static {}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->values()[Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->MixedPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->NumericPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_1
    :try_start_2
    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPin:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPuk:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    sput-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-com-android-keyguard-MiuiLockPatternKeyguardView$UnlockModeSwitchesValues:[I

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1

    :catch_5
    move-exception v1

    goto :goto_0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsDefaultLockScreen:Z

    return p1
.end method

.method static synthetic -set1(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsLockPasswordEnabled:Z

    return p1
.end method

.method static synthetic -set2(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsLockPatternEnabled:Z

    return p1
.end method

.method static synthetic -set3(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mNewPassword:Z

    return p1
.end method

.method static synthetic -set4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mPluggedIn:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isPukRequired()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isPukUnlockScreenEnable()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap10(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateWallpaper(Z)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->stuckOnLockScreenBecauseSimMissing()Z

    move-result v0

    return v0
.end method

.method static synthetic -wrap3(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getInitialMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->initBackgroundImage()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/keyguard/MiuiLockPatternKeyguardView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->showBatteryInfoView(I)V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->turnOffTorch()V

    return-void
.end method

.method static synthetic -wrap7(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateGaussBlurView(Z)V

    return-void
.end method

.method static synthetic -wrap8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateScreen(Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V

    return-void
.end method

.method static synthetic -wrap9(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateTorchCover(Z)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sIsFirstAppearanceAfterBoot:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardViewMediator;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardWindowController;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/MiuiKeyguardViewBase;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;)V

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowFocused:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsDefaultLockScreen:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mNewPassword:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlockStartupLock:Ljava/lang/Object;

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->BIOMETRIC_AREA_EMERGENCY_DIALER_TIMEOUT:I

    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$1;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$2;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mImageViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$3;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$3;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$5;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$5;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchStateReceiver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockCallback:Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    const-string/jumbo v0, "keyguard.no_require_sim"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mRequiresSim:Z

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "new_numeric_password_type"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    invoke-static {v0, v3, v2, v4}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mNewPassword:Z

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "face_unlock_by_notification_screen_on"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockByNotificationScreenOn:Z

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mInitTime:J

    iput-object p3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iput-object p4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iput-object p5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    new-instance v0, Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-direct {v0, p5}, Lcom/android/keyguard/LockPatternUtilsWrapper;-><init>(Lcom/android/internal/widget/LockPatternUtils;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    iput-object p6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowController:Lcom/android/keyguard/MiuiKeyguardWindowController;

    sget-boolean v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sIsFirstAppearanceAfterBoot:Z

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    sput-boolean v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sIsFirstAppearanceAfterBoot:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v0}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isLockPatternEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsLockPatternEnabled:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v0}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isLockPasswordEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsLockPasswordEnabled:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDevicePluggedIn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mPluggedIn:Z

    const-string/jumbo v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mScreenOn:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    invoke-virtual {v0, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerInfoCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->keyguardScreenCallback()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-virtual {v0, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setKeyguardScreenCallback(Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->setFocusableInTouchMode(Z)V

    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->setDescendantFocusability(I)V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlockInitState:Z

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getUnlockMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const-string/jumbo v0, "sys.is_keyguard_showing"

    const-string/jumbo v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->addGaussBlurView()V

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$8;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mPowerManager:Landroid/os/PowerManager;

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0
.end method

.method private addGaussBlurView()V
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->addView(Landroid/view/View;I)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateGaussBlurViewAlpha()V

    return-void
.end method

.method private createTorchCover()V
    .locals 6

    const v2, 0x7f0c0045

    const/4 v5, -0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x51

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b002d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x10500f8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v4, v4, v4, v1}, Landroid/view/View;->setPadding(IIII)V

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    const v1, 0x7f02003c

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    new-instance v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$14;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$14;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    new-instance v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$15;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$15;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v4, v4, v4, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method private getInitialMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;
    .locals 4

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "show_lock_before_unlock"

    sget-boolean v2, Landroid/provider/MiuiSettings$System;->SHOW_LOCK_BEFORE_UNLOCK_DEFAULT:Z

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->stuckOnLockScreenBecauseSimMissing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isPukRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isPukUnlockScreenEnable()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isSecure()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    if-eqz v0, :cond_3

    :cond_2
    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    return-object v0

    :cond_3
    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    return-object v0
.end method

.method private getUnlockModeForHighPasswordQuality(I)Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;
    .locals 1

    const/high16 v0, 0x20000

    if-eq v0, p1, :cond_0

    const/high16 v0, 0x30000

    if-ne v0, p1, :cond_1

    :cond_0
    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->NumericPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    return-object v0

    :cond_1
    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->MixedPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    return-object v0
.end method

.method private initBackgroundImage()V
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Landroid/widget/ImageSwitcher;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageSwitcher;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackgroundImageView:Landroid/widget/ImageSwitcher;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackgroundImageView:Landroid/widget/ImageSwitcher;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageSwitcher;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackgroundImageView:Landroid/widget/ImageSwitcher;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mImageViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {v0, v1}, Landroid/widget/ImageSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackgroundImageView:Landroid/widget/ImageSwitcher;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->addView(Landroid/view/View;I)V

    return-void
.end method

.method private isPukRequired()Z
    .locals 3

    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getSimState(I)Lcom/android/internal/telephony/IccCardConstants$State;

    move-result-object v1

    sget-object v2, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method private isPukUnlockScreenEnable()Z
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x112006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    return v1
.end method

.method private maybeStartBiometricUnlock()V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    sget-object v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne v1, v2, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getPhoneState()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getMaxBiometricUnlockAttemptsReached()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    xor-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->start()Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->hide()V

    goto :goto_1
.end method

.method private recreateLockScreen()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    instance-of v0, v0, Lcom/android/keyguard/AwesomeLockScreen;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    instance-of v0, v0, Lcom/android/keyguard/MiuiDefaultLockScreen;

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onPause()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->cleanUp()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeView(Landroid/view/View;)V

    :cond_2
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->createLockScreen()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->addView(Landroid/view/View;)V

    return-void
.end method

.method private removeTorchCoverView()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iput-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    :cond_0
    return-void
.end method

.method private showBatteryInfoView(I)V
    .locals 10

    const/4 v1, -0x1

    const-string/jumbo v2, "is_pad"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    const v3, 0x7f030002

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const/16 v2, 0x1300

    invoke-virtual {v7, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d9

    const v4, 0x5030504

    const/4 v5, 0x1

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const v1, 0x7f0f0002

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    invoke-interface {v9, v7, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0d0019

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/keyguard/MiuiKeyguardBatteryInfoView;

    invoke-virtual {v6, p1}, Lcom/android/keyguard/MiuiKeyguardBatteryInfoView;->setChargingProgress(I)V

    new-instance v8, Lcom/android/keyguard/MiuiLockPatternKeyguardView$11;

    invoke-direct {v8, p0, v9, v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$11;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Landroid/view/WindowManager;Landroid/view/View;)V

    new-instance v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$12;

    invoke-direct {v1, p0, v7, v8}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$12;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Landroid/view/View;Ljava/lang/Runnable;)V

    invoke-virtual {v7, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    new-instance v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$13;

    invoke-direct {v1, p0, v7, v8, v9}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$13;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Landroid/view/View;Ljava/lang/Runnable;Landroid/view/WindowManager;)V

    invoke-virtual {v7, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private startGaussViewAnimation(IFF)V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p2, v2, v3

    const/4 v3, 0x1

    aput p3, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurViewFadeAnim:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurViewFadeAnim:Landroid/animation/ObjectAnimator;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurViewFadeAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method private stuckOnLockScreenBecauseSimMissing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mRequiresSim:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDeviceProvisioned()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimMissing()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private turnOffTorch()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.TOGGLE_TORCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "miui.intent.extra.IS_ENABLE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeTorchCoverView()V

    return-void
.end method

.method private updateGaussBlurView(Z)V
    .locals 6

    const/4 v5, 0x0

    const v4, 0x3eaaaaab

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lmiui/system/R$color;->blur_background_mask:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lmiui/content/res/ThemeResources;->getLockWallpaperCache(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lmiui/system/R$color;->blur_background_mask:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    :cond_1
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    float-to-int v2, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    invoke-static {v0, v5}, Lmiui/util/ScreenshotUtils;->getBlurBackground(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method private updateGaussBlurViewAlpha()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0
.end method

.method private updateScreen(Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V
    .locals 7

    const/4 v4, 0x1

    const/4 v5, 0x0

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    iget-object v6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-eq p1, v3, :cond_9

    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/keyguard/doze/DozeHost;->isDozing()Z

    move-result v3

    :goto_0
    invoke-interface {v6, v3}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->disableNavigation(Z)V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isGxzwSensor()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwManager;->getInstance()Lcom/android/keyguard/MiuiGxzwManager;

    move-result-object v6

    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/keyguard/doze/DozeHost;->isDozing()Z

    move-result v3

    :goto_1
    if-nez v3, :cond_d

    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne p1, v3, :cond_c

    move v3, v4

    :goto_2
    invoke-virtual {v6, v3}, Lcom/android/keyguard/MiuiGxzwManager;->dismissGxzwIconView(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getUnlockMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    move-result-object v1

    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-eq p1, v3, :cond_1

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    if-eqz v3, :cond_5

    :cond_1
    if-nez p2, :cond_2

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-nez v3, :cond_3

    :cond_2
    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isSecure(Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->recreateUnlockScreen(Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;)V

    :cond_3
    if-nez p2, :cond_4

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    if-nez v3, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->recreateLockScreen()V

    :cond_5
    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne p1, v3, :cond_7

    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-eq v1, v3, :cond_7

    if-nez p2, :cond_6

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-nez v3, :cond_e

    :cond_6
    :goto_3
    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->recreateUnlockScreen(Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;)V

    :cond_7
    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne p1, v3, :cond_f

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    :goto_4
    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne p1, v3, :cond_10

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    :goto_5
    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowController:Lcom/android/keyguard/MiuiKeyguardWindowController;

    if-eqz v3, :cond_8

    if-nez v2, :cond_11

    :cond_8
    const-string/jumbo v3, "LockPatternKeyguardView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "mWindowController="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowController:Lcom/android/keyguard/MiuiKeyguardWindowController;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", visibleScreen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", mLockScreen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", mUnlockScreen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", mode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", unlockMode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_9
    move v3, v4

    goto/16 :goto_0

    :cond_a
    move v3, v5

    goto/16 :goto_0

    :cond_b
    move v3, v5

    goto/16 :goto_1

    :cond_c
    move v3, v5

    goto/16 :goto_2

    :cond_d
    move v3, v5

    goto/16 :goto_2

    :cond_e
    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne v1, v3, :cond_6

    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPin:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-eq v1, v3, :cond_6

    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPuk:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne v1, v3, :cond_7

    goto/16 :goto_3

    :cond_f
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    goto/16 :goto_4

    :cond_10
    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    goto/16 :goto_5

    :cond_11
    iget-object v6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowController:Lcom/android/keyguard/MiuiKeyguardWindowController;

    move-object v3, v2

    check-cast v3, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v3}, Lcom/android/keyguard/MiuiKeyguardScreen;->needsInput()Z

    move-result v3

    invoke-interface {v6, v3}, Lcom/android/keyguard/MiuiKeyguardWindowController;->setNeedsInput(Z)V

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mScreenOn:Z

    if-eqz v3, :cond_13

    if-eqz v0, :cond_12

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_12

    move-object v3, v0

    check-cast v3, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v3}, Lcom/android/keyguard/MiuiKeyguardScreen;->onPause()V

    :cond_12
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_13

    move-object v3, v2

    check-cast v3, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v3, v5}, Lcom/android/keyguard/MiuiKeyguardScreen;->onResume(Z)V

    :cond_13
    if-eqz v0, :cond_14

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_14
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->requestLayout()V

    sget-object v3, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne p1, v3, :cond_15

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowController:Lcom/android/keyguard/MiuiKeyguardWindowController;

    invoke-interface {v3, v4}, Lcom/android/keyguard/MiuiKeyguardWindowController;->updateStatusBarColorMode(Z)V

    :cond_15
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v3

    if-nez v3, :cond_16

    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "keyguard screen must be able to take focus when shown "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/view/View;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_16
    iget-boolean v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mScreenOn:Z

    if-eqz v3, :cond_17

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->maybeStartBiometricUnlock()V

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-ne v2, v3, :cond_17

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    instance-of v3, v3, Lcom/android/keyguard/MiuiCommonUnlockScreen;

    if-eqz v3, :cond_17

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v3, Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->startAnimation()V

    :cond_17
    iget-boolean v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    if-nez v3, :cond_18

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-eqz v3, :cond_18

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-ne v3, v2, :cond_18

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    if-eqz v3, :cond_18

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_18
    return-void
.end method

.method private updateTorchCover(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->createTorchCover()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->addView(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeTorchCoverView()V

    goto :goto_0
.end method

.method private updateWallpaper(Z)V
    .locals 14

    const v1, 0x3f8ccccd    # 1.1f

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackgroundImageView:Landroid/widget/ImageSwitcher;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isLiveWallpaper(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsDefaultLockScreen:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->initBackgroundImage()V

    :cond_0
    const/4 v11, 0x0

    const/4 v10, 0x0

    if-eqz p1, :cond_1

    new-instance v11, Landroid/view/animation/AnimationSet;

    invoke-direct {v11, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v0}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v12, 0x3e8

    invoke-virtual {v11, v12, v13}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Lmiui/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v0}, Lmiui/view/animation/CubicEaseOutInterpolator;-><init>()V

    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v0, 0x3e8

    invoke-virtual {v10, v0, v1}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackgroundImageView:Landroid/widget/ImageSwitcher;

    invoke-virtual {v0, v10}, Landroid/widget/ImageSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackgroundImageView:Landroid/widget/ImageSwitcher;

    invoke-virtual {v0, v11}, Landroid/widget/ImageSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackgroundImageView:Landroid/widget/ImageSwitcher;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/ImageSwitcher;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/content/res/ThemeResources;->getLockWallpaperCache(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    if-eqz v9, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackgroundImageView:Landroid/widget/ImageSwitcher;

    invoke-virtual {v0, v9}, Landroid/widget/ImageSwitcher;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_2
    return-void

    :cond_3
    const-string/jumbo v0, "miui_keyguard"

    const-string/jumbo v1, "lock screen wall paper is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public allowScreenRotation()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    instance-of v0, v0, Lcom/android/keyguard/AwesomeLockScreen;

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/AwesomeLockScreen;

    invoke-virtual {v0}, Lcom/android/keyguard/AwesomeLockScreen;->allowScreenRotation()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cleanUp()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onPause()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->cleanUp()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeView(Landroid/view/View;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onPause()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->cleanUp()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeView(Landroid/view/View;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->stopFaceUnlock()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->cleanUp()V

    :cond_2
    const-string/jumbo v0, "sys.is_keyguard_showing"

    const-string/jumbo v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected createFaceUnlockViewIfNeed()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockView:Landroid/view/TextureView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->shouldListenForFaceUnlock()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimLocked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/view/TextureView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockView:Landroid/view/TextureView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockView:Landroid/view/TextureView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockView:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockView:Landroid/view/TextureView;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method protected createKeyguardScreenCallback()Lcom/android/keyguard/MiuiKeyguardScreenCallback;
    .locals 1

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    return-object v0
.end method

.method createLockScreen()Landroid/view/View;
    .locals 6

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isDefaultLockScreenTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-direct/range {v0 .. v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/android/keyguard/AwesomeLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-direct/range {v0 .. v5}, Lcom/android/keyguard/AwesomeLockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V

    return-object v0
.end method

.method createUnlockScreenFor(Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;)Landroid/view/View;
    .locals 9

    const/4 v1, 0x0

    const-string/jumbo v2, "LockPatternKeyguardView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "createUnlockScreenFor("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne p1, v2, :cond_0

    new-instance v0, Lcom/android/keyguard/MiuiPatternUnlockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    iget-boolean v6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct/range {v0 .. v7}, Lcom/android/keyguard/MiuiPatternUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    move-object v1, v0

    :goto_0
    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    return-object v1

    :cond_0
    sget-object v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPuk:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne p1, v2, :cond_1

    new-instance v1, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    iget-object v6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSimSlotIdToUnlock:I

    iget-object v8, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct/range {v1 .. v8}, Lcom/android/keyguard/MiuiSimPUKUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;ILcom/android/keyguard/MiuiKeyguardViewMediator;)V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPin:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne p1, v2, :cond_2

    new-instance v1, Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    iget-object v6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSimSlotIdToUnlock:I

    iget-object v8, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct/range {v1 .. v8}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;ILcom/android/keyguard/MiuiKeyguardViewMediator;)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->NumericPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne p1, v2, :cond_4

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mNewPassword:Z

    if-eqz v2, :cond_3

    new-instance v1, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    iget-boolean v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    iget-object v8, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct/range {v1 .. v8}, Lcom/android/keyguard/MiuiNumericPasswordUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    iget-boolean v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    iget-object v8, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct/range {v1 .. v8}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->MixedPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne p1, v2, :cond_6

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mNewPassword:Z

    if-eqz v2, :cond_5

    new-instance v1, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    iget-boolean v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    iget-object v8, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct/range {v1 .. v8}, Lcom/android/keyguard/MiuiMixedPasswordUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    goto/16 :goto_0

    :cond_5
    new-instance v1, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v6, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    iget-boolean v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    iget-object v8, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct/range {v1 .. v8}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    goto/16 :goto_0

    :cond_6
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "unknown unlock mode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    :cond_0
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->pokeWakelock()V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5

    const/16 v2, 0x18

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackDown:Z

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/PowerManager;->userActivity(JZ)V

    :cond_1
    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    return v1

    :cond_2
    if-eq v0, v2, :cond_0

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackDown:Z

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_0

    if-ne v0, v2, :cond_4

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackDown:Z

    if-eqz v1, :cond_4

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackDown:Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    const-string/jumbo v1, "MiuiLockPatternKeyguardView"

    const-string/jumbo v2, "Unlock Screen by pressing back + volume_up"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v3

    :cond_4
    iput-boolean v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBackDown:Z

    goto :goto_0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    instance-of v0, v0, Lcom/android/keyguard/AwesomeLockScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/AwesomeLockScreen;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/AwesomeLockScreen;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    return-object v0
.end method

.method protected getUnlockMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;
    .locals 7

    const/4 v1, 0x0

    :goto_0
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v4, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getSimState(I)Lcom/android/internal/telephony/IccCardConstants$State;

    move-result-object v3

    sget-object v4, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v3, v4, :cond_0

    iput v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSimSlotIdToUnlock:I

    sget-object v4, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPin:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    return-object v4

    :cond_0
    sget-object v4, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    if-ne v3, v4, :cond_1

    iput v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSimSlotIdToUnlock:I

    sget-object v4, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPuk:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    return-object v4

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v4}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getKeyguardStoredPasswordQuality()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Unknown unlock mode:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :sswitch_0
    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getUnlockModeForHighPasswordQuality(I)Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    move-result-object v0

    :goto_1
    return-object v0

    :sswitch_1
    iget-boolean v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsLockPatternEnabled:Z

    if-eqz v4, :cond_3

    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x10000 -> :sswitch_1
        0x20000 -> :sswitch_0
        0x30000 -> :sswitch_0
        0x40000 -> :sswitch_0
        0x50000 -> :sswitch_0
        0x60000 -> :sswitch_0
    .end sparse-switch
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected isSecure()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getUnlockMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isSecure(Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;)Z

    move-result v0

    return v0
.end method

.method protected isSecure(Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-getcom-android-keyguard-MiuiLockPatternKeyguardView$UnlockModeSwitchesValues()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unknown unlock mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsLockPatternEnabled:Z

    :goto_0
    :pswitch_1
    return v0

    :pswitch_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimSecured()Z

    move-result v0

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimSecured()Z

    move-result v0

    goto :goto_0

    :pswitch_4
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsLockPasswordEnabled:Z

    goto :goto_0

    :pswitch_5
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsLockPasswordEnabled:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public isUnlockScreenMode()Z
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected keyguardScreenCallback()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->createKeyguardScreenCallback()Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getInitialMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateScreen(Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "torch_state"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchStateReceiver:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerWallpaperChangeCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;)V

    invoke-super {p0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->onAttachedToWindow()V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "show_lock_before_unlock"

    sget-boolean v2, Landroid/provider/MiuiSettings$System;->SHOW_LOCK_BEFORE_UNLOCK_DEFAULT:Z

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWallpaperChangeCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->stop()Z

    :cond_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->stopFaceUnlock()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchStateReceiver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-super {p0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->onDetachedFromWindow()V

    return-void
.end method

.method public onScreenTurnedOff()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mScreenOn:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onPause()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onPause()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    instance-of v0, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->resetFingerprintLockoutText()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->stopFaceUnlock()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->stop()Z

    :cond_2
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlockInitState:Z

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getInitialMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    return-void
.end method

.method public onScreenTurnedOn()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/keyguard/doze/DozeHost;->isDozing()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    :cond_0
    const-string/jumbo v2, "face_unlock"

    const-string/jumbo v3, "screen turn on"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sScreenTurnedOnTime:J

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MiuiSettings$Secure;->FIND_DEVICE_PIN_LOCK:Ljava/lang/String;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v4

    invoke-static {v2, v3, v5, v4}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-ne v2, v6, :cond_6

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlockStartupLock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mScreenOn:Z

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowFocused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateGaussBlurViewAlpha()V

    :cond_2
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->show()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    invoke-virtual {v2}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->isFaceUnlockLocked()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v2}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v2, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v2}, Lcom/android/keyguard/MiuiKeyguardScreen;->onFaceUnlockFailed()V

    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->maybeStartBiometricUnlock()V

    :cond_4
    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFingerprintIdentificationState()Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    move-result-object v2

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerPrintLockOut(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v2}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    :cond_5
    return-void

    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_7
    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->shouldListenForFaceUnlock()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    invoke-virtual {v2}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->isValidFeature()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v2}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    goto :goto_1

    :cond_8
    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-static {}, Lcom/android/keyguard/Dependency;->getHost()Lcom/android/keyguard/doze/DozeHost;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/keyguard/doze/DozeHost;->isDozing()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    :cond_9
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->startFaceUnlock()V

    sput-boolean v5, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sWakeupByNotification:Z

    goto :goto_1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlockStartupLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mScreenOn:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowFocused:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    move v0, p1

    :cond_0
    iput-boolean p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowFocused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->stop()Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->hide()V

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->maybeStartBiometricUnlock()V

    goto :goto_0
.end method

.method protected recreateUnlockScreen(Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onPause()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->cleanUp()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->createFaceUnlockViewIfNeed()V

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->createUnlockScreenFor(Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->addView(Landroid/view/View;)V

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setGaussViewAlphaFromLockScreen(F)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurViewFadeAnim:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurViewFadeAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_1
    return-void
.end method

.method public setHidden(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardScreen;->setHidden(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardScreen;->setHidden(Z)V

    :cond_1
    return-void
.end method

.method public setY(F)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->setY(F)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getHeight()I

    move-result v2

    float-to-int v3, p1

    add-int/2addr v2, v3

    mul-int/lit16 v2, v2, 0xff

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getHeight()I

    move-result v3

    div-int v0, v2, v3

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v2, -0x1000000

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    return-void
.end method

.method public show()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mScreenOn:Z

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "torch_state"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeTorchCoverView()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->createTorchCover()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mTorchCover:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0, v2}, Lcom/android/keyguard/MiuiKeyguardScreen;->onResume(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0, v2}, Lcom/android/keyguard/MiuiKeyguardScreen;->onResume(Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->hide()V

    :cond_3
    return-void
.end method

.method protected showAlmostAtWipeDialog(II)V
    .locals 5

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const v3, 0x7f0b0029

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected showDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d9

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public showGaussView()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mGaussBlurView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    const/16 v1, 0x12c

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0, v1, v0, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->startGaussViewAnimation(IFF)V

    return-void
.end method

.method protected showWipeDialog(I)V
    .locals 5

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f0b002a

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public startFaceUnlock()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockView:Landroid/view/TextureView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isPasswordLockout()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sWakeupByNotification:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockByNotificationScreenOn:Z

    if-nez v0, :cond_1

    :cond_0
    sget-boolean v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sWakeupByNotification:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimLocked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mScreenOn:Z

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isScreenTurnOnDelayed()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->shouldListenForFaceUnlock()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    new-instance v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$10;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$10;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->runOnFaceUnlockWorkerThread(Ljava/lang/Runnable;)V

    :cond_3
    return-void
.end method

.method public stopFaceUnlock()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockView:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->setScreenTurnOnDelayed(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mFaceUnlockManager:Lcom/android/keyguard/faceunlock/FaceUnlockManager;

    new-instance v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$9;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$9;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/faceunlock/FaceUnlockManager;->runOnFaceUnlockWorkerThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public unlockScreenExisted()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateStatusBarColorMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mWindowController:Lcom/android/keyguard/MiuiKeyguardWindowController;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardWindowController;->updateStatusBarColorMode(Z)V

    return-void
.end method

.method public verifyUnlock()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isSecure()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->keyguardDone(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->NumericPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->MixedPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->keyguardDone(Z)V

    goto :goto_0

    :cond_1
    iput-boolean v3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    invoke-direct {p0, v0, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateScreen(Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V

    goto :goto_0
.end method

.method public wakeWhenReadyTq(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "torch_state"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->turnOffTorch()V

    return-void

    :cond_0
    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isSecure()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mMode:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isPukRequired()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    invoke-direct {p0, v0, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->updateScreen(Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->pokeWakelock()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->pokeWakelock()V

    goto :goto_0
.end method
