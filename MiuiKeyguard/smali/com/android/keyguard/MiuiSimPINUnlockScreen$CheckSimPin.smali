.class abstract Lcom/android/keyguard/MiuiSimPINUnlockScreen$CheckSimPin;
.super Ljava/lang/Thread;
.source "MiuiSimPINUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiSimPINUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "CheckSimPin"
.end annotation


# instance fields
.field private final mPin:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;


# direct methods
.method protected constructor <init>(Lcom/android/keyguard/MiuiSimPINUnlockScreen;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$CheckSimPin;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$CheckSimPin;->mPin:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method abstract onSimLockChangedResponse(Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;)V
.end method

.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$CheckSimPin;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->-get0(Lcom/android/keyguard/MiuiSimPINUnlockScreen;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$CheckSimPin;->mPin:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$CheckSimPin;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget v2, v2, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimId:I

    invoke-static {v0, v1, v2}, Lcom/android/keyguard/UnlockSimHelper;->checkPin(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen$CheckSimPin;->onSimLockChangedResponse(Lcom/android/keyguard/UnlockSimHelper$UnlockSimResult;)V

    return-void
.end method
