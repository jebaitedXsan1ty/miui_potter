.class Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;
.super Ljava/lang/Object;
.source "MiuiLockPatternKeyguardView.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardScreenCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiLockPatternKeyguardView;->createKeyguardScreenCallback()Lcom/android/keyguard/MiuiKeyguardScreenCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mPendingIntent:Landroid/content/Intent;

.field final synthetic this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V
    .locals 1

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->mPendingIntent:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public disableInfo(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->disableInfo(Z)V

    return-void
.end method

.method public disableNavigation(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->disableNavigation(Z)V

    return-void
.end method

.method public getPendingIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->mPendingIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public goToLockScreen()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-boolean v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iput-boolean v2, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->keyguardDone(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->startFaceUnlock()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->LockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    invoke-static {v0, v1, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V

    goto :goto_0
.end method

.method public goToUnlockScreen()V
    .locals 12

    const/4 v11, 0x0

    const/4 v10, 0x1

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap2(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z

    move-result v7

    if-eqz v7, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->isSecure()Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v7, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_NONE:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/keyguard/AnalyticsHelper;->recordUnlockWay(Ljava/lang/String;)V

    invoke-virtual {p0, v10}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->keyguardDone(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->setAlpha(F)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->mPendingIntent:Landroid/content/Intent;

    if-eqz v7, :cond_3

    const-string/jumbo v7, "android.media.action.STILL_IMAGE_CAMERA"

    iget-object v8, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->mPendingIntent:Landroid/content/Intent;

    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/android/keyguard/MiuiKeyguardUtils;->getCameraIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_3

    :try_start_0
    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get0(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/content/Context;

    move-result-object v7

    sget-object v8, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v7, v2, v8}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->mPendingIntent:Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    :cond_3
    :goto_1
    if-eqz v1, :cond_c

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap2(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap0(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap1(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z

    move-result v7

    xor-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_5

    :cond_4
    return-void

    :cond_5
    if-nez v4, :cond_6

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v7

    invoke-interface {v7, v10}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->keyguardDone(Z)V

    goto :goto_0

    :cond_6
    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getUnlockMode()Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    move-result-object v6

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Landroid/provider/MiuiSettings$Secure;->FIND_DEVICE_PIN_LOCK:Ljava/lang/String;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v9

    invoke-static {v7, v8, v11, v9}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v7

    if-ne v7, v10, :cond_9

    const/4 v3, 0x1

    :goto_2
    const/4 v5, 0x1

    sget-object v7, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->MixedPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-eq v6, v7, :cond_7

    sget-object v7, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne v6, v7, :cond_a

    :cond_7
    :goto_3
    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v7, v7, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getBLEUnlockState()Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    move-result-object v7

    sget-object v8, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->SUCCEED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    if-ne v7, v8, :cond_8

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v7, v7, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->showBLEUnlockAnimation()Z

    move-result v7

    if-eqz v7, :cond_b

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->pokeWakelock()V

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->showGaussView()V

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    sget-object v8, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    invoke-static {v7, v8, v11}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->showBLEUnlockSucceedView()V

    :cond_8
    :goto_4
    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->pokeWakelock()V

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->showGaussView()V

    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    sget-object v8, Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    invoke-static {v7, v8, v11}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V

    goto/16 :goto_0

    :cond_9
    const/4 v3, 0x0

    goto :goto_2

    :cond_a
    sget-object v7, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->NumericPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    if-ne v6, v7, :cond_8

    goto :goto_3

    :cond_b
    if-nez v3, :cond_8

    const/4 v5, 0x0

    invoke-virtual {p0, v10}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->keyguardDone(Z)V

    sget-object v7, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_BAND:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/keyguard/AnalyticsHelper;->recordUnlockWay(Ljava/lang/String;)V

    goto :goto_4

    :cond_c
    iget-object v7, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    new-instance v8, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7$1;

    invoke-direct {v8, p0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7$1;-><init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;)V

    const-wide/16 v10, 0x5dc

    invoke-virtual {v7, v8, v10, v11}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_1
.end method

.method public isSecure()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->isSecure()Z

    move-result v0

    return v0
.end method

.method public isVerifyUnlockOnly()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-boolean v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    return v0
.end method

.method public keyguardDone(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->keyguardDone(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSavedState:Landroid/os/Parcelable;

    return-void
.end method

.method public keyguardDoneDrawing()V
    .locals 0

    return-void
.end method

.method public pokeWakelock()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->pokeWakelock()V

    return-void
.end method

.method public pokeWakelock(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->pokeWakelock(I)V

    return-void
.end method

.method public recreateMe(Landroid/content/res/Configuration;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public reportFailedUnlockAttempt()V
    .locals 6

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v4, v4, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->reportFailedAttempt()V

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v4, v4, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v1

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v4, v4, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v4}, Lcom/android/internal/widget/LockPatternUtils;->getDevicePolicyManager()Landroid/app/admin/DevicePolicyManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/app/admin/DevicePolicyManager;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;)I

    move-result v2

    const/16 v0, 0x8

    if-lez v2, :cond_1

    sub-int v3, v2, v1

    :goto_0
    const/4 v4, 0x5

    if-ge v3, v4, :cond_0

    if-lez v3, :cond_2

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v4, v1, v3}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->showAlmostAtWipeDialog(II)V

    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v4}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/LockPatternUtilsWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/keyguard/LockPatternUtilsWrapper;->reportFailedPasswordAttempt()V

    return-void

    :cond_1
    const v3, 0x7fffffff

    goto :goto_0

    :cond_2
    const-string/jumbo v4, "LockPatternKeyguardView"

    const-string/jumbo v5, "Too many unlock attempts; device will be wiped!"

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v4, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->showWipeDialog(I)V

    goto :goto_1
.end method

.method public reportSuccessfulUnlockAttempt()V
    .locals 1

    invoke-static {}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->setUserAuthenticatedSinceBoot()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->resetFingerLockoutTime()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/LockPatternUtilsWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/LockPatternUtilsWrapper;->reportSuccessfulPasswordAttempt()V

    return-void
.end method

.method public setPendingIntent(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->mPendingIntent:Landroid/content/Intent;

    return-void
.end method

.method public startFaceUnlock()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->startFaceUnlock()V

    return-void
.end method

.method public stopFingerprintIdentification()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->stopFingerprintIdentification()V

    return-void
.end method

.method public takeEmergencyCallAction()V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    const-wide/16 v2, 0x3e8

    invoke-interface {v1, v2, v3}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->show(J)V

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v1}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->stop()Z

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->stopFaceUnlock()V

    const/16 v1, 0x2710

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->pokeWakelock(I)V

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->getCallState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/LockPatternUtilsWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get0(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/LockPatternUtilsWrapper;->resumeCall(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.android.phone.EmergencyDialer.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$7;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method

.method public updatePinUnlockCancel(I)V
    .locals 0

    return-void
.end method

.method public updatePukUnlockCancel(I)V
    .locals 0

    return-void
.end method
