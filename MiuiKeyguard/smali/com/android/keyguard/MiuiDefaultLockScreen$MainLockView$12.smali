.class Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$12;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$12;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public makeView()Landroid/view/View;
    .locals 4

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$12;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$12;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0028

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$12;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get44(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/graphics/Typeface;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$12;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get44(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_0
    return-object v0
.end method
