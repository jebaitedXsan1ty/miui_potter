.class Lcom/android/keyguard/MiuiKeyguardViewMediator$4;
.super Landroid/content/BroadcastReceiver;
.source "MiuiKeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string/jumbo v2, "miui.intent.action.ACTION_SHOW_LOCK_SCREEN"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set13(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap27(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v2, "miui.intent.action.SMART_COVER"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "persist.sys.smartcover_mode"

    const/4 v5, -0x1

    invoke-static {v2, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "is_smart_cover_open"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    xor-int/lit8 v0, v2, 0x1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isSecure()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v2, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap2(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap9(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)V

    :cond_2
    iget-object v5, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    move v2, v3

    :goto_1
    invoke-static {v5, v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set7(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    const/4 v5, 0x4

    if-ne v1, v5, :cond_4

    :goto_2
    invoke-static {v2, v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set6(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v2, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap4(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set5(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$4;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->refreshSmartCover()V

    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_2
.end method
