.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;
.super Ljava/lang/Object;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BatteryStatus"
.end annotation


# instance fields
.field public final health:I

.field public final level:I

.field public final plugged:I

.field public final status:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->status:I

    iput p2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->level:I

    iput p3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->plugged:I

    iput p4, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;->health:I

    return-void
.end method
