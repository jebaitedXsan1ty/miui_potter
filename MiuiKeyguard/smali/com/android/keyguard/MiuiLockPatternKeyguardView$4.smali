.class Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;
.super Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;
.source "MiuiLockPatternKeyguardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiLockPatternKeyguardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public onClockVisibilityChanged()V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->getSystemUiVisibility()I

    move-result v1

    const v2, -0x800001

    and-int/2addr v2, v1

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isClockVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x800000

    :goto_0
    or-int v0, v2, v1

    const-string/jumbo v1, "LockPatternKeyguardView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Set visibility on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->setSystemUiVisibility(I)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPhoneStateChanged(I)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v0, :cond_0

    if-ne p1, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iput-boolean v1, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->stop()Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get3(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/TextureView;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ne p1, v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->stopFaceUnlock()V

    :cond_1
    return-void
.end method

.method public onRefreshBatteryInfo(ZZI)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get7(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z

    move-result v0

    if-eq v0, p2, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->isRunning()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->stop()Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->hide()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get7(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0, p3}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap5(Lcom/android/keyguard/MiuiLockPatternKeyguardView;I)V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0, p2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-set4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z

    return-void
.end method

.method public onUserChanged(I)V
    .locals 5

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mBiometricUnlock:Lcom/android/keyguard/MiuiBiometricSensorUnlock;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiBiometricSensorUnlock;->stop()Z

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->stopFaceUnlock()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/LockPatternUtilsWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->setCurrentUser(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/LockPatternUtilsWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isLockPasswordEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-set1(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get4(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/LockPatternUtilsWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isLockPatternEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-set2(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get0(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "new_numeric_password_type"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v4, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-set3(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$4;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap3(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-wrap8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;Lcom/android/keyguard/MiuiLockPatternKeyguardView$Mode;Z)V

    return-void
.end method
