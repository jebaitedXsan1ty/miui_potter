.class interface abstract Lcom/android/keyguard/MiuiBiometricSensorUnlock;
.super Ljava/lang/Object;
.source "MiuiBiometricSensorUnlock.java"


# virtual methods
.method public abstract cleanUp()V
.end method

.method public abstract getQuality()I
.end method

.method public abstract hide()V
.end method

.method public abstract initializeView(Landroid/view/View;)V
.end method

.method public abstract isRunning()Z
.end method

.method public abstract show(J)V
.end method

.method public abstract start()Z
.end method

.method public abstract stop()Z
.end method
