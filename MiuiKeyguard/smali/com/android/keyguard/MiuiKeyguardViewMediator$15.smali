.class Lcom/android/keyguard/MiuiKeyguardViewMediator$15;
.super Landroid/os/Handler;
.source "MiuiKeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap17(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    return-void

    :pswitch_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap8(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    return-void

    :pswitch_3
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap14(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    return-void

    :pswitch_4
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap18(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    return-void

    :pswitch_5
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap12(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)V

    return-void

    :pswitch_6
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap13(Lcom/android/keyguard/MiuiKeyguardViewMediator;Ljava/lang/Object;)V

    return-void

    :pswitch_7
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap19(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)V

    return-void

    :pswitch_8
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_0

    :goto_1
    invoke-static {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap11(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_9
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap10(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    return-void

    :pswitch_a
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->keyguardDone(Z)V

    return-void

    :pswitch_b
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_1

    :goto_2
    invoke-static {v2, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap16(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    :pswitch_c
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap0(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_d
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$15;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-wrap15(Lcom/android/keyguard/MiuiKeyguardViewMediator;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
    .end packed-switch
.end method
