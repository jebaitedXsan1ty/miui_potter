.class Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    const/16 v10, 0x1f4

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get7(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x10a0001

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v13

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get7(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x10a0000

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v12

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, -0x40000000    # -2.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v1, Landroid/view/animation/TranslateAnimation;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v11, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x1

    invoke-direct {v11, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v14, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x1

    invoke-direct {v14, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    invoke-virtual {v11, v13}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v14, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v14, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v11, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v14, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v14, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    new-instance v2, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8$1;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;)V

    invoke-virtual {v11, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    new-instance v2, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8$2;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8$2;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;)V

    invoke-virtual {v14, v2}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get18(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v2

    invoke-virtual {v2, v14}, Lcom/android/keyguard/UnlockHintSwitcher;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
