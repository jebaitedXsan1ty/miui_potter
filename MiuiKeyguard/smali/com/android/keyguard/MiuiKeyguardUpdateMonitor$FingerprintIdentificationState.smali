.class public final enum Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;
.super Ljava/lang/Enum;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FingerprintIdentificationState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

.field public static final enum FAIL_ONE_TIME:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

.field public static final enum LOCKOUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

.field public static final enum LOCKOUT_PERMANENT_FOR_O:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

.field public static final enum NEED_PASSWORD:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

.field public static final enum PASS:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

.field public static final enum WAITING_FOR_INPUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    const-string/jumbo v1, "WAITING_FOR_INPUT"

    invoke-direct {v0, v1, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->WAITING_FOR_INPUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    const-string/jumbo v1, "FAIL_ONE_TIME"

    invoke-direct {v0, v1, v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->FAIL_ONE_TIME:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    const-string/jumbo v1, "LOCKOUT"

    invoke-direct {v0, v1, v5}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    const-string/jumbo v1, "LOCKOUT_PERMANENT_FOR_O"

    invoke-direct {v0, v1, v6}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT_PERMANENT_FOR_O:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    const-string/jumbo v1, "PASS"

    invoke-direct {v0, v1, v7}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->PASS:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    const-string/jumbo v1, "NEED_PASSWORD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->NEED_PASSWORD:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->WAITING_FOR_INPUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->FAIL_ONE_TIME:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT_PERMANENT_FOR_O:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->PASS:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->NEED_PASSWORD:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->$VALUES:[Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;
    .locals 1

    const-class v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    return-object v0
.end method

.method public static values()[Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->$VALUES:[Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    return-object v0
.end method
