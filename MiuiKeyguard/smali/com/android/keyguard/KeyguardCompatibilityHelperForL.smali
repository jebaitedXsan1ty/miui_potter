.class public Lcom/android/keyguard/KeyguardCompatibilityHelperForL;
.super Ljava/lang/Object;
.source "KeyguardCompatibilityHelperForL.java"


# static fields
.field private static sTrustManager:Landroid/app/trust/TrustManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static is24HourFormat(Landroid/content/Context;I)Z
    .locals 1

    invoke-static {p0, p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;I)Z

    move-result v0

    return v0
.end method

.method public static reportKeyguardShowingChanged(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/android/keyguard/KeyguardCompatibilityHelperForL;->sTrustManager:Landroid/app/trust/TrustManager;

    if-nez v0, :cond_0

    const-string/jumbo v0, "trust"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/trust/TrustManager;

    sput-object v0, Lcom/android/keyguard/KeyguardCompatibilityHelperForL;->sTrustManager:Landroid/app/trust/TrustManager;

    :cond_0
    sget-object v0, Lcom/android/keyguard/KeyguardCompatibilityHelperForL;->sTrustManager:Landroid/app/trust/TrustManager;

    invoke-virtual {v0}, Landroid/app/trust/TrustManager;->reportKeyguardShowingChanged()V

    return-void
.end method
