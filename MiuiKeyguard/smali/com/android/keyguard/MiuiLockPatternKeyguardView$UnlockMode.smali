.class final enum Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;
.super Ljava/lang/Enum;
.source "MiuiLockPatternKeyguardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiLockPatternKeyguardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "UnlockMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

.field public static final enum MixedPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

.field public static final enum NumericPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

.field public static final enum Pattern:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

.field public static final enum SimPin:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

.field public static final enum SimPuk:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

.field public static final enum Unknown:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const-string/jumbo v1, "Pattern"

    invoke-direct {v0, v1, v3}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const-string/jumbo v1, "SimPin"

    invoke-direct {v0, v1, v4}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPin:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const-string/jumbo v1, "SimPuk"

    invoke-direct {v0, v1, v5}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPuk:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const-string/jumbo v1, "NumericPassword"

    invoke-direct {v0, v1, v6}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->NumericPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const-string/jumbo v1, "MixedPassword"

    invoke-direct {v0, v1, v7}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->MixedPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const-string/jumbo v1, "Unknown"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPin:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->SimPuk:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->NumericPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->MixedPassword:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->$VALUES:[Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;
    .locals 1

    const-class v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    return-object v0
.end method

.method public static values()[Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;->$VALUES:[Lcom/android/keyguard/MiuiLockPatternKeyguardView$UnlockMode;

    return-object v0
.end method
