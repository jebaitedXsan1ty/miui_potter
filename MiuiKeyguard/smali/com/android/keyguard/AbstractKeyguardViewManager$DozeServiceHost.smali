.class final Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;
.super Ljava/lang/Object;
.source "AbstractKeyguardViewManager.java"

# interfaces
.implements Lcom/android/keyguard/doze/DozeHost;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/AbstractKeyguardViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DozeServiceHost"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost$1;
    }
.end annotation


# instance fields
.field private mAODView:Lcom/android/keyguard/AODView;

.field private mAnimateWakeup:Z

.field private mAodScrim:Landroid/view/View;

.field private final mCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/keyguard/doze/DozeHost$Callback;",
            ">;"
        }
    .end annotation
.end field

.field private mContainer:Landroid/view/ViewGroup;

.field private mContext:Landroid/content/Context;

.field private mDozingRequested:Z

.field private mHandler:Landroid/os/Handler;

.field private mRemoveAODViewRunnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;


# direct methods
.method public constructor <init>(Lcom/android/keyguard/AbstractKeyguardViewManager;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mCallbacks:Ljava/util/ArrayList;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost$1;-><init>(Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;)V

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mRemoveAODViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->showSecurityIdentityViewAt()Lcom/android/keyguard/AODView;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAODView:Lcom/android/keyguard/AODView;

    return-void
.end method

.method private shouldAnimateWakeup()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAnimateWakeup:Z

    return v0
.end method

.method private showSecurityIdentityViewAt()Lcom/android/keyguard/AODView;
    .locals 8

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const/high16 v2, 0x7f030000

    const/4 v3, 0x0

    invoke-virtual {v7, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/android/keyguard/AODView;

    new-instance v2, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost$2;

    invoke-direct {v2, p0}, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost$2;-><init>(Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;)V

    invoke-virtual {v6, v2}, Lcom/android/keyguard/AODView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7df

    const/16 v4, 0x500

    move v2, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    const-string/jumbo v1, "AOD"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v1}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get5(Lcom/android/keyguard/AbstractKeyguardViewManager;)Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1, v6, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0d0003

    invoke-virtual {v6, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContainer:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setAlpha(F)V

    const v1, 0x7f0d000b

    invoke-virtual {v6, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAodScrim:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Lcom/android/keyguard/AODView;->setVisibility(I)V

    return-object v6
.end method


# virtual methods
.method public abortPulsing()V
    .locals 0

    return-void
.end method

.method public addCallback(Lcom/android/keyguard/doze/DozeHost$Callback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearAODViewRunnable()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mRemoveAODViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public disableInfo(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get0(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->disableInfo(Z)V

    return-void
.end method

.method public dozeTimeTick()V
    .locals 1

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isGxzwSensor()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwManager;->getInstance()Lcom/android/keyguard/MiuiGxzwManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwManager;->dozeTimeTick()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAODView:Lcom/android/keyguard/AODView;

    invoke-virtual {v0}, Lcom/android/keyguard/AODView;->handleUpdateView()V

    return-void
.end method

.method public extendPulse()V
    .locals 0

    return-void
.end method

.method public fireNotificationHeadsUp()V
    .locals 3

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mCallbacks:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/doze/DozeHost$Callback;

    invoke-interface {v0}, Lcom/android/keyguard/doze/DozeHost$Callback;->onNotificationHeadsUp()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public firePowerSaveChanged(Z)V
    .locals 3

    iget-object v2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mCallbacks:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/doze/DozeHost$Callback;

    invoke-interface {v0, p1}, Lcom/android/keyguard/doze/DozeHost$Callback;->onPowerSaveChanged(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public isBlockingDoze()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isDozing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mDozingRequested:Z

    return v0
.end method

.method public isPowerSaveActive()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isProvisioned()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isPulsingBlocked()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDoubleTap(FF)V
    .locals 0

    return-void
.end method

.method public onIgnoreTouchWhilePulsing(Z)V
    .locals 0

    return-void
.end method

.method public pulseWhileDozing(Lcom/android/keyguard/doze/DozeHost$PulseCallback;I)V
    .locals 0

    invoke-interface {p1}, Lcom/android/keyguard/doze/DozeHost$PulseCallback;->onPulseStarted()V

    invoke-interface {p1}, Lcom/android/keyguard/doze/DozeHost$PulseCallback;->onPulseFinished()V

    return-void
.end method

.method public removeAODView()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get0(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mDozingRequested:Z

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->disableInfo(Z)V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAODView:Lcom/android/keyguard/AODView;

    invoke-virtual {v0}, Lcom/android/keyguard/AODView;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAODView:Lcom/android/keyguard/AODView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/keyguard/AODView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get4(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewBase;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "face_unlock"

    const-string/jumbo v1, "screen turn on aod"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isScreenTurnOnDelayed()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sScreenTurnedOnTime:J

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-virtual {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->startFaceUnlock()V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get4(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiKeyguardViewBase;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get4(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->isUnlockScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get0(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->disableNavigation(Z)V

    :cond_2
    return-void
.end method

.method public removeCallback(Lcom/android/keyguard/doze/DozeHost$Callback;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setAnimateWakeup(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAnimateWakeup:Z

    return-void
.end method

.method public setAodDimmingScrim(F)V
    .locals 5

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAodScrim:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAodScrim:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAodScrim:Landroid/view/View;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v3, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAodScrim:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method public setDozeScreenBrightness(I)V
    .locals 0

    return-void
.end method

.method public startDozing()V
    .locals 4

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mDozingRequested:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mDozingRequested:Z

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get0(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewCallback;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mDozingRequested:Z

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->disableInfo(Z)V

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isGxzwSensor()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwManager;->getInstance()Lcom/android/keyguard/MiuiGxzwManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwManager;->startDozing()V

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwManager;->getInstance()Lcom/android/keyguard/MiuiGxzwManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiGxzwManager;->dismissGxzwIconView(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAODView:Lcom/android/keyguard/AODView;

    invoke-virtual {v0, v2}, Lcom/android/keyguard/AODView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mAODView:Lcom/android/keyguard/AODView;

    invoke-virtual {v0}, Lcom/android/keyguard/AODView;->handleUpdateView()V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lmiui/view/animation/SineEaseOutInterpolator;

    invoke-direct {v1}, Lmiui/view/animation/SineEaseOutInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_1
    return-void
.end method

.method public startPendingIntentDismissingKeyguard(Landroid/app/PendingIntent;)V
    .locals 0

    return-void
.end method

.method public stopDozing()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mDozingRequested:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mDozingRequested:Z

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isGxzwSensor()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwManager;->getInstance()Lcom/android/keyguard/MiuiGxzwManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwManager;->stopDozing()V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get4(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewBase;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->this$0:Lcom/android/keyguard/AbstractKeyguardViewManager;

    invoke-static {v0}, Lcom/android/keyguard/AbstractKeyguardViewManager;->-get4(Lcom/android/keyguard/AbstractKeyguardViewManager;)Lcom/android/keyguard/MiuiKeyguardViewBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->isUnlockScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/keyguard/MiuiGxzwManager;->getInstance()Lcom/android/keyguard/MiuiGxzwManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiGxzwManager;->dismissGxzwIconView(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mRemoveAODViewRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isScreenTurnOnDelayed()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mRemoveAODViewRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->removeAODView()V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "PSB.DozeServiceHost[mCallbacks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$DozeServiceHost;->mCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
