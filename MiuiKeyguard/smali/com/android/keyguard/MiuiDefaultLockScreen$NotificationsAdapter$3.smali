.class Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->onBindViewHolder(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

.field final synthetic val$key:I

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;II)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iput p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->val$position:I

    iput p3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->val$key:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->val$position:I

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->val$key:I

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get38(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v2

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->val$key:I

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set17(Lcom/android/keyguard/MiuiDefaultLockScreen;I)I

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v1

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get12(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get31(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get31(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get11(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get25(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->unregisterListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get14(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.miui.app.ExtraStatusBarManager.action_remove_keyguard_notification"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "com.miui.app.ExtraStatusBarManager.extra_notification_key"

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v1

    iget v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->val$position:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget v1, v1, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->key:I

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "com.miui.app.ExtraStatusBarManager.extra_notification_click"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$3;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method
