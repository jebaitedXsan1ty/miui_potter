.class public Lcom/android/keyguard/MiuiGxzwIconView;
.super Landroid/widget/FrameLayout;
.source "MiuiGxzwIconView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiGxzwIconView$1;,
        Lcom/android/keyguard/MiuiGxzwIconView$2;,
        Lcom/android/keyguard/MiuiGxzwIconView$3;,
        Lcom/android/keyguard/MiuiGxzwIconView$4;,
        Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final MSG_SET_GXZW_ICON_TRANSPARENT:I = 0x3e9

.field private static final MSG_SHOW_MORE_PRESSURE:I = 0x3ea

.field private static final POSITION_DELTA:[I

.field private static final TAG:Ljava/lang/String; = "MiuiGxzwViewIcon"

.field private static TYPE_PUT_UP_DETECT:I

.field private static TYPE_STATIONARY_DETECT:I


# instance fields
.field private mCollectGxzwListener:Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;

.field private mCollectingGxzw:Z

.field private mDisplayEventReceiver:Landroid/view/DisplayEventReceiver;

.field private mDozeTimeTickCount:I

.field private mDozing:Z

.field private mEnoughPressure:Z

.field private mFMExtCmdMethod:Ljava/lang/reflect/Method;

.field private mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

.field private mGxzwCollectManager:Ljava/lang/Object;

.field private mGxzwFpView:Landroid/view/View;

.field private mGxzwIconHeight:I

.field private mGxzwIconTransparent:Z

.field private mGxzwIconWidth:I

.field private mGxzwIconX:I

.field private mGxzwIconY:I

.field private mHandler:Landroid/os/Handler;

.field private mHighlightView:Landroid/view/View;

.field private mLockView:Landroid/view/View;

.field private mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

.field private mMiuiKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

.field private mNeedStartCollectGxzw:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private mPressureValue:I

.field private mPutUpSensorListener:Landroid/hardware/SensorEventListener;

.field private mRandom:Ljava/util/Random;

.field private mRegisterStationarySensor:Z

.field private mScreenOn:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mShowing:Z

.field private mStationarySensor:Landroid/hardware/Sensor;

.field private mTouchDown:Z

.field private mTriggerEventListener:Landroid/hardware/TriggerEventListener;


# direct methods
.method static synthetic -get0()I
    .locals 1

    sget v0, Lcom/android/keyguard/MiuiGxzwIconView;->TYPE_STATIONARY_DETECT:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiGxzwIconView;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiGxzwIconView;)Lcom/android/keyguard/MiuiGxzwAnimView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiGxzwIconView;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPressureValue:I

    return v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiGxzwIconView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    return v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiGxzwIconView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mNeedStartCollectGxzw:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiGxzwIconView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->enoughPressure()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiGxzwIconView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->registerStationarySensor()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiGxzwIconView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->setGxzwIconOpaque()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/keyguard/MiuiGxzwIconView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->setGxzwIconTransparent()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/keyguard/MiuiGxzwIconView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->unregisterStationarySensor()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v0, -0x5

    const/4 v1, 0x0

    const/4 v2, 0x5

    filled-new-array {v0, v1, v2}, [I

    move-result-object v0

    sput-object v0, Lcom/android/keyguard/MiuiGxzwIconView;->POSITION_DELTA:[I

    const v0, 0x1fa263e

    sput v0, Lcom/android/keyguard/MiuiGxzwIconView;->TYPE_PUT_UP_DETECT:I

    const/16 v0, 0x1d

    sput v0, Lcom/android/keyguard/MiuiGxzwIconView;->TYPE_STATIONARY_DETECT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mTouchDown:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mEnoughPressure:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectingGxzw:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mRandom:Ljava/util/Random;

    iput v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPressureValue:I

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mNeedStartCollectGxzw:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mScreenOn:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozing:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mRegisterStationarySensor:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconTransparent:Z

    new-instance v0, Lcom/android/keyguard/MiuiGxzwIconView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiGxzwIconView$1;-><init>(Lcom/android/keyguard/MiuiGxzwIconView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwIconView$2;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/keyguard/MiuiGxzwIconView$2;-><init>(Lcom/android/keyguard/MiuiGxzwIconView;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDisplayEventReceiver:Landroid/view/DisplayEventReceiver;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mFMExtCmdMethod:Ljava/lang/reflect/Method;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwIconView$3;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiGxzwIconView$3;-><init>(Lcom/android/keyguard/MiuiGxzwIconView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mTriggerEventListener:Landroid/hardware/TriggerEventListener;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwIconView$4;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiGxzwIconView$4;-><init>(Lcom/android/keyguard/MiuiGxzwIconView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPutUpSensorListener:Landroid/hardware/SensorEventListener;

    iput-object p2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->initView()V

    return-void
.end method

.method private enoughPressure()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mEnoughPressure:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mScreenOn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mEnoughPressure:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mNeedStartCollectGxzw:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->startCollectGxzw()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3ea

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwAnimView;->dismissMorePressure()V

    :cond_0
    return-void
.end method

.method private getRamdonInt()I
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mRandom:Ljava/util/Random;

    sget-object v1, Lcom/android/keyguard/MiuiGxzwIconView;->POSITION_DELTA:[I

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    return v0
.end method

.method private initView()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000f

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0d0050

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiGxzwIconView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwFpView:Landroid/view/View;

    const v0, 0x7f0d0052

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiGxzwIconView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mHighlightView:Landroid/view/View;

    const v0, 0x7f0d0051

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiGxzwIconView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mLockView:Landroid/view/View;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiGxzwAnimView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {p0, p0}, Lcom/android/keyguard/MiuiGxzwIconView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "fingerprint"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwCollectManager:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPowerManager:Landroid/os/PowerManager;

    iput v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconX:I

    iput v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconY:I

    iput v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconWidth:I

    iput v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconHeight:I

    const/16 v0, 0x1304

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiGxzwIconView;->setSystemUiVisibility(I)V

    return-void
.end method

.method private isFingerPrintLockOut()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    sget-object v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    sget-object v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT_PERMANENT_FOR_O:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFingerprintFailOneTime()Z
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->FAIL_ONE_TIME:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notEnoughPressure()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mEnoughPressure:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mEnoughPressure:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->stopCollectGxzw()V

    :cond_0
    return-void
.end method

.method private onTouchDown()V
    .locals 5

    const/16 v4, 0x3ea

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->setGxzwIconOpaque()V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozing:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->registerStationarySensor()V

    :cond_0
    iput v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozeTimeTickCount:I

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->isFingerPrintLockOut()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwAnimView;->showUsePassword()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->dismiss()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mTouchDown:Z

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mNeedStartCollectGxzw:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mHighlightView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDisplayEventReceiver:Landroid/view/DisplayEventReceiver;

    invoke-virtual {v0}, Landroid/view/DisplayEventReceiver;->scheduleVsync()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwAnimView;->dismissTryAgain()V

    return-void
.end method

.method private onTouchUp()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mTouchDown:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mHighlightView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3ea

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwAnimView;->dismissMorePressure()V

    return-void
.end method

.method private reflectCallFMExtCmd(Ljava/lang/Object;II)Z
    .locals 9

    const/4 v3, 0x1

    const/4 v4, 0x0

    :try_start_0
    const-string/jumbo v2, "MiuiGxzwViewIcon"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "reflectCallFMExtCmd: cmd = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ", param = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mFMExtCmdMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string/jumbo v5, "extCmd"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v8, 0x0

    aput-object v7, v6, v8

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v8, 0x1

    aput-object v7, v6, v8

    invoke-virtual {v2, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mFMExtCmdMethod:Ljava/lang/reflect/Method;

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mFMExtCmdMethod:Ljava/lang/reflect/Method;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x1

    aput-object v6, v5, v7

    invoke-virtual {v2, p1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string/jumbo v2, "MiuiGxzwViewIcon"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "extCmd res:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    move v2, v3

    :goto_0
    return v2

    :cond_1
    move v2, v4

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "MiuiGxzwViewIcon"

    const-string/jumbo v3, "fail to extCmd"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return v4
.end method

.method private registerPutUpSensor()V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v1, :cond_0

    const-string/jumbo v1, "MiuiGxzwViewIcon"

    const-string/jumbo v2, "sensor not supported"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    sget v2, Lcom/android/keyguard/MiuiGxzwIconView;->TYPE_PUT_UP_DETECT:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPutUpSensorListener:Landroid/hardware/SensorEventListener;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v0, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "MiuiGxzwViewIcon"

    const-string/jumbo v2, "no put up sensor"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerStationarySensor()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "sensor not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mRegisterStationarySensor:Z

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mRegisterStationarySensor:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    sget v1, Lcom/android/keyguard/MiuiGxzwIconView;->TYPE_STATIONARY_DETECT:I

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mStationarySensor:Landroid/hardware/Sensor;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mStationarySensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mTriggerEventListener:Landroid/hardware/TriggerEventListener;

    iget-object v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mStationarySensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->requestTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "add stationary sensor listener successfully"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_2
    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "no stationary sensor"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setGxzwIconOpaque()V
    .locals 2

    const/4 v1, 0x0

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiGxzwIconView;->updateGxzwIconAlpha(F)V

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconTransparent:Z

    iput v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozeTimeTickCount:I

    return-void
.end method

.method private setGxzwIconTransparent()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiGxzwIconView;->updateGxzwIconAlpha(F)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconTransparent:Z

    return-void
.end method

.method private startCollectGxzw()V
    .locals 4

    const/4 v3, 0x1

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectingGxzw:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectGxzwListener:Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectGxzwListener:Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;

    invoke-interface {v1, v3}, Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;->onStateChange(Z)V

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwCollectManager:Ljava/lang/Object;

    const/16 v2, 0xa

    invoke-direct {p0, v1, v2, v3}, Lcom/android/keyguard/MiuiGxzwIconView;->reflectCallFMExtCmd(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectingGxzw:Z

    :cond_3
    return-void
.end method

.method private stopCollectGxzw()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectingGxzw:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectGxzwListener:Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectGxzwListener:Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;

    invoke-interface {v1, v3}, Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;->onStateChange(Z)V

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwCollectManager:Ljava/lang/Object;

    const/16 v2, 0xa

    invoke-direct {p0, v1, v2, v3}, Lcom/android/keyguard/MiuiGxzwIconView;->reflectCallFMExtCmd(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectingGxzw:Z

    :cond_2
    return-void
.end method

.method private unregisterPutUpSensor()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "sensor not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPutUpSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    return-void
.end method

.method private unregisterStationarySensor()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "sensor not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mRegisterStationarySensor:Z

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mRegisterStationarySensor:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mStationarySensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mTriggerEventListener:Landroid/hardware/TriggerEventListener;

    iget-object v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mStationarySensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->cancelTriggerSensor(Landroid/hardware/TriggerEventListener;Landroid/hardware/Sensor;)Z

    :cond_2
    return-void
.end method

.method private updateGxzwIconAlpha(F)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwFpView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mLockView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiGxzwAnimView;->setAlpha(F)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->notEnoughPressure()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->onTouchUp()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiGxzwAnimView;->dismiss()V

    return-void
.end method

.method public dozeTimeTick()V
    .locals 7

    iget v5, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozeTimeTickCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozeTimeTickCount:I

    iget-boolean v5, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozeTimeTickCount:I

    const/4 v6, 0x2

    if-lt v5, v6, :cond_0

    sget-object v5, Lcom/android/keyguard/MiuiGxzwIconView;->POSITION_DELTA:[I

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getRamdonInt()I

    move-result v6

    aget v0, v5, v6

    sget-object v5, Lcom/android/keyguard/MiuiGxzwIconView;->POSITION_DELTA:[I

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getRamdonInt()I

    move-result v6

    aget v1, v5, v6

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager$LayoutParams;

    iget v5, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconX:I

    add-int v3, v5, v0

    iget v5, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconY:I

    add-int v4, v5, v1

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiGxzwIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-boolean v5, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconTransparent:Z

    if-nez v5, :cond_0

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-direct {p0, v5}, Lcom/android/keyguard/MiuiGxzwIconView;->updateGxzwIconAlpha(F)V

    :cond_0
    return-void
.end method

.method public onFingerprintStateChanged(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)V
    .locals 3

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onFingerprintStateChanged: state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mFpiState:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->isFingerPrintLockOut()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mLockView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwAnimView;->dismissAllText()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->isFingerprintFailOneTime()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwAnimView;->showTryAgain()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwAnimView;->dismissUsePassword()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mLockView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onHelp(Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onHelp: helpString = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onScreenTurnedOff()V
    .locals 2

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "onScreenTurnedOff"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mScreenOn:Z

    return-void
.end method

.method public onScreenTurnedOn()V
    .locals 2

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "onScreenTurnedOn"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mScreenOn:Z

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPressure()F

    move-result v0

    const/high16 v1, 0x45000000    # 2048.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPressureValue:I

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "pressure = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPressureValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPressureValue:I

    const/16 v1, 0x46

    if-lt v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mTouchDown:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->onTouchDown()V

    :cond_1
    :goto_1
    iget v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPressureValue:I

    const/16 v1, 0x96

    if-le v0, v1, :cond_4

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mNeedStartCollectGxzw:Z

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mNeedStartCollectGxzw:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->enoughPressure()V

    :cond_2
    :goto_2
    return v5

    :pswitch_1
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, v5}, Landroid/os/PowerManager;->userActivity(JZ)V

    goto :goto_0

    :pswitch_2
    iput v4, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mPressureValue:I

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mTouchDown:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->onTouchUp()V

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->notEnoughPressure()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setCollectGxzwListener(Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mCollectGxzwListener:Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;

    return-void
.end method

.method public show()V
    .locals 9

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    if-eqz v3, :cond_0

    return-void

    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    iget-object v3, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiGxzwAnimView;->show()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    iget v7, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconX:I

    iget v8, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconY:I

    iget v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconWidth:I

    iget v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconHeight:I

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v3}, Lcom/android/keyguard/MiuiGxzwIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d9

    const v4, 0x1020008

    const/4 v5, -0x2

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/16 v3, 0x33

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iput v7, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v8, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    const-string/jumbo v3, "gxzw_icon"

    invoke-virtual {v0, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    invoke-interface {v6, p0, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public startDozing()V
    .locals 2

    const-string/jumbo v0, "MiuiGxzwViewIcon"

    const-string/jumbo v1, "startDozing"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozing:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwAnimView;->dismissAllText()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->registerPutUpSensor()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->registerStationarySensor()V

    return-void
.end method

.method public stopDozing()V
    .locals 6

    const/4 v5, 0x0

    const-string/jumbo v3, "MiuiGxzwViewIcon"

    const-string/jumbo v4, "stopDozing"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v5, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozing:Z

    iget-object v3, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mMiuiGxzwAnimView:Lcom/android/keyguard/MiuiGxzwAnimView;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiGxzwAnimView;->dismissAllText()V

    iput v5, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mDozeTimeTickCount:I

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mShowing:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconX:I

    iget v2, p0, Lcom/android/keyguard/MiuiGxzwIconView;->mGxzwIconY:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiGxzwIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->setGxzwIconOpaque()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->unregisterPutUpSensor()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwIconView;->unregisterStationarySensor()V

    return-void
.end method
