.class interface abstract Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;
.super Ljava/lang/Object;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "InfoCallback"
.end annotation


# virtual methods
.method public abstract onClockVisibilityChanged()V
.end method

.method public abstract onDevicePolicyManagerStateChanged()V
.end method

.method public abstract onDeviceProvisioned()V
.end method

.method public abstract onPhoneStateChanged(I)V
.end method

.method public abstract onRefreshBatteryInfo(ZZI)V
.end method

.method public abstract onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
.end method

.method public abstract onRefreshCarrierInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
.end method

.method public abstract onRingerModeChanged(I)V
.end method

.method public abstract onTimeChanged()V
.end method

.method public abstract onUserChanged(I)V
.end method
