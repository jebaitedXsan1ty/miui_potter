.class public abstract Lcom/android/keyguard/MiuiKeyguardViewBase;
.super Landroid/widget/FrameLayout;
.source "MiuiKeyguardViewBase.java"


# static fields
.field private static final KEYGUARD_MANAGES_VOLUME:Z = true


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

.field private mTelephonyManager:Lmiui/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    iput-object p2, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

    return-void
.end method

.method private interceptMediaKey(Landroid/view/KeyEvent;)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_5

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return v2

    :sswitch_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    if-nez v1, :cond_1

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mTelephonyManager:Lmiui/telephony/TelephonyManager;

    invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-eqz v1, :cond_2

    return v3

    :cond_2
    :sswitch_1
    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->handleMediaKeyEvent(Landroid/view/KeyEvent;)V

    return v3

    :sswitch_2
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mAudioManager:Landroid/media/AudioManager;

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mAudioManager:Landroid/media/AudioManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/android/keyguard/KeyguardCompatibilityHelper;->adjustAudioLocalOrRemoteStreamVolume(Landroid/content/Context;Landroid/view/KeyEvent;)V

    :cond_4
    return v3

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->handleMediaKeyEvent(Landroid/view/KeyEvent;)V

    return v3

    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x4f -> :sswitch_1
        0x55 -> :sswitch_0
        0x56 -> :sswitch_1
        0x57 -> :sswitch_1
        0x58 -> :sswitch_1
        0x59 -> :sswitch_1
        0x5a -> :sswitch_1
        0x5b -> :sswitch_1
        0x7e -> :sswitch_0
        0x7f -> :sswitch_0
        0x82 -> :sswitch_1
        0xa4 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x4f -> :sswitch_3
        0x55 -> :sswitch_3
        0x56 -> :sswitch_3
        0x57 -> :sswitch_3
        0x58 -> :sswitch_3
        0x59 -> :sswitch_3
        0x5a -> :sswitch_3
        0x5b -> :sswitch_3
        0x7e -> :sswitch_3
        0x7f -> :sswitch_3
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method private shouldEventKeepScreenOnWhileKeyguardShowing(Landroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public abstract allowScreenRotation()Z
.end method

.method public abstract cleanUp()V
.end method

.method public abstract dismiss()V
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->shouldEventKeepScreenOnWhileKeyguardShowing(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->pokeWakelock()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardViewBase;->interceptMediaKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public dispatchSystemUiVisibilityChanged(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchSystemUiVisibilityChanged(I)V

    const/high16 v0, 0x400000

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->setSystemUiVisibility(I)V

    return-void
.end method

.method public getCallback()Lcom/android/keyguard/MiuiKeyguardViewCallback;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewBase;->mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

    return-object v0
.end method

.method handleMediaKeyEvent(Landroid/view/KeyEvent;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardViewBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/keyguard/KeyguardCompatibilityHelper;->dispatchMediaKeyEvent(Landroid/content/Context;Landroid/view/KeyEvent;)V

    return-void
.end method

.method public abstract isUnlockScreenMode()Z
.end method

.method public abstract onScreenTurnedOff()V
.end method

.method public abstract onScreenTurnedOn()V
.end method

.method public abstract reset()V
.end method

.method public abstract setHidden(Z)V
.end method

.method public abstract show()V
.end method

.method public abstract startFaceUnlock()V
.end method

.method public abstract verifyUnlock()V
.end method

.method public abstract wakeWhenReadyTq(I)V
.end method
