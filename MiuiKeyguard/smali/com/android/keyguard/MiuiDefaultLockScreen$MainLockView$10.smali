.class Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setOtherViewVisibility(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

.field final synthetic val$visibility:I


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iput p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->val$visibility:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get11(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/ViewGroup;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->val$visibility:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get19(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->val$visibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get1(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->val$visibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    iget v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->val$visibility:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get11(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/ViewGroup;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->val$visibility:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get19(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->val$visibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get1(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;->val$visibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method
