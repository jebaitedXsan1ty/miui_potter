.class Lcom/android/keyguard/MiuiKeyguardViewMediator$17$1;
.super Ljava/lang/Object;
.source "MiuiKeyguardViewMediator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->onFingerprintStateChanged(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiKeyguardViewMediator$17;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator$17;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17$1;->this$1:Lcom/android/keyguard/MiuiKeyguardViewMediator$17;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17$1;->this$1:Lcom/android/keyguard/MiuiKeyguardViewMediator$17;

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardViewMediator$17;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "is_fingerprint_unlock"

    const/4 v2, 0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_FP:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->recordUnlockWay(Ljava/lang/String;)V

    return-void
.end method
