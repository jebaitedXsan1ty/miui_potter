.class Lcom/android/keyguard/MiuiCommonUnlockScreen$4;
.super Landroid/content/BroadcastReceiver;
.source "MiuiCommonUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiCommonUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getBLEUnlockState()Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    move-result-object v0

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;->SUCCEED:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BLEUnlockState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-wrap1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$4;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->showBLEUnlockSucceedView()V

    :cond_0
    return-void
.end method
