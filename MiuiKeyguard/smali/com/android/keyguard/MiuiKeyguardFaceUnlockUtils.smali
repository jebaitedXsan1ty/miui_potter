.class public Lcom/android/keyguard/MiuiKeyguardFaceUnlockUtils;
.super Ljava/lang/Object;
.source "MiuiKeyguardFaceUnlockUtils.java"


# static fields
.field public static final MG_ATTR_BLUR:I = 0x14

.field public static final MG_ATTR_EYE_CLOSE:I = 0x16

.field public static final MG_ATTR_EYE_OCCLUSION:I = 0x15

.field public static final MG_ATTR_MOUTH_OCCLUSION:I = 0x17

.field public static final MG_UNLOCK_BAD_LIGHT:I = 0x1a

.field public static final MG_UNLOCK_COMPARE_FAILURE:I = 0xc

.field public static final MG_UNLOCK_DARKLIGHT:I = 0x1e

.field public static final MG_UNLOCK_FACE_BAD_QUALITY:I = 0x4

.field public static final MG_UNLOCK_FACE_BLUR:I = 0x1c

.field public static final MG_UNLOCK_FACE_DOWN:I = 0x12

.field public static final MG_UNLOCK_FACE_MULTI:I = 0x1b

.field public static final MG_UNLOCK_FACE_NOT_COMPLETE:I = 0x1d

.field public static final MG_UNLOCK_FACE_NOT_FOUND:I = 0x5

.field public static final MG_UNLOCK_FACE_OFFSET_BOTTOM:I = 0xb

.field public static final MG_UNLOCK_FACE_OFFSET_LEFT:I = 0x8

.field public static final MG_UNLOCK_FACE_OFFSET_RIGHT:I = 0xa

.field public static final MG_UNLOCK_FACE_OFFSET_TOP:I = 0x9

.field public static final MG_UNLOCK_FACE_RISE:I = 0x10

.field public static final MG_UNLOCK_FACE_ROTATED_LEFT:I = 0xf

.field public static final MG_UNLOCK_FACE_ROTATED_RIGHT:I = 0x11

.field public static final MG_UNLOCK_FACE_SCALE_TOO_LARGE:I = 0x7

.field public static final MG_UNLOCK_FACE_SCALE_TOO_SMALL:I = 0x6

.field public static final MG_UNLOCK_FAILURE:I = 0x3

.field public static final MG_UNLOCK_FEATURE_MISS:I = 0x18

.field public static final MG_UNLOCK_FEATURE_VERSION_ERROR:I = 0x19

.field public static final MG_UNLOCK_HALF_SHADOW:I = 0x20

.field public static final MG_UNLOCK_HIGHLIGHT:I = 0x1f

.field public static final MG_UNLOCK_INVALID_ARGUMENT:I = 0x1

.field public static final MG_UNLOCK_INVALID_HANDLE:I = 0x2

.field public static final MG_UNLOCK_KEEP:I = 0x13

.field public static final MG_UNLOCK_LIVENESS_FAILURE:I = 0xe

.field public static final MG_UNLOCK_LIVENESS_WARNING:I = 0xd

.field public static final MG_UNLOCK_OK:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
