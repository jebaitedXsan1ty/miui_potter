.class public final Lcom/android/keyguard/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final aod_bg:I = 0x7f020000

.field public static final battery:I = 0x7f020001

.field public static final battery_fg:I = 0x7f020002

.field public static final battery_ing:I = 0x7f020003

.field public static final ble_unlock_statusbar_icon_unverified:I = 0x7f020004

.field public static final ble_unlock_statusbar_icon_unverified_dark:I = 0x7f020005

.field public static final ble_unlock_statusbar_icon_verified_far:I = 0x7f020006

.field public static final ble_unlock_statusbar_icon_verified_far_dark:I = 0x7f020007

.field public static final ble_unlock_statusbar_icon_verified_near:I = 0x7f020008

.field public static final ble_unlock_statusbar_icon_verified_near_dark:I = 0x7f020009

.field public static final calendar:I = 0x7f02000a

.field public static final camera_img:I = 0x7f02000b

.field public static final camera_img_dark:I = 0x7f02000c

.field public static final camera_preview:I = 0x7f02000d

.field public static final clip_battery:I = 0x7f02000e

.field public static final confirm_pinnumber_button:I = 0x7f02000f

.field public static final dark_drop_down_mark:I = 0x7f020010

.field public static final dialog_bg_light:I = 0x7f020011

.field public static final drop_down_mark:I = 0x7f020012

.field public static final face_unlock_icon:I = 0x7f020013

.field public static final face_unlock_icon_dark:I = 0x7f020014

.field public static final facebookmsg:I = 0x7f020015

.field public static final finger_circle:I = 0x7f020016

.field public static final finger_image:I = 0x7f020017

.field public static final full_smart_cover_miss_call:I = 0x7f020018

.field public static final full_smart_cover_sms:I = 0x7f020019

.field public static final gcalendar:I = 0x7f02001a

.field public static final gmail:I = 0x7f02001b

.field public static final gxzw_lock:I = 0x7f02001c

.field public static final ic_emergency:I = 0x7f02001d

.field public static final ic_lockscreen_emergencycall_normal:I = 0x7f02001e

.field public static final ic_lockscreen_emergencycall_pressed:I = 0x7f02001f

.field public static final intro_bg:I = 0x7f020020

.field public static final keyguard_battery_charging_bg:I = 0x7f020021

.field public static final keyguard_battery_charging_progress:I = 0x7f020022

.field public static final keyguard_charging_info_back_arrow_bg:I = 0x7f020023

.field public static final keyguard_charging_info_back_arrow_n:I = 0x7f020024

.field public static final keyguard_charging_info_back_arrow_p:I = 0x7f020025

.field public static final keyguard_charging_info_bg:I = 0x7f020026

.field public static final keyguard_charging_info_drained_power_percent_icon:I = 0x7f020027

.field public static final keyguard_charging_info_first_list_bg:I = 0x7f020028

.field public static final keyguard_charging_info_last_charged_time_icon:I = 0x7f020029

.field public static final keyguard_charging_info_last_list_bg:I = 0x7f02002a

.field public static final keyguard_charging_info_middle_list_first_item_bg:I = 0x7f02002b

.field public static final keyguard_charging_info_middle_list_second_item_bg:I = 0x7f02002c

.field public static final keyguard_charging_info_middle_list_third_item_bg:I = 0x7f02002d

.field public static final keyguard_charging_info_tip_icon:I = 0x7f02002e

.field public static final keyguard_charging_info_used_time_icon:I = 0x7f02002f

.field public static final keyguard_left_view_bankcard:I = 0x7f020030

.field public static final keyguard_left_view_buscard:I = 0x7f020031

.field public static final keyguard_left_view_list_n:I = 0x7f020032

.field public static final keyguard_left_view_list_p:I = 0x7f020033

.field public static final keyguard_left_view_magazine:I = 0x7f020034

.field public static final keyguard_left_view_manazine:I = 0x7f020035

.field public static final keyguard_left_view_remotecontroller:I = 0x7f020036

.field public static final keyguard_left_view_smarthome:I = 0x7f020037

.field public static final keyguard_left_view_torchlight_n:I = 0x7f020038

.field public static final keyguard_left_view_torchlight_p:I = 0x7f020039

.field public static final line:I = 0x7f02003a

.field public static final loading_item:I = 0x7f02003b

.field public static final lock_screen_torch_cover:I = 0x7f02003c

.field public static final lockscreen_emergency_button:I = 0x7f02003d

.field public static final mail:I = 0x7f02003e

.field public static final miui_add_face_recoginition_img:I = 0x7f02003f

.field public static final miui_add_face_recoginition_suggestion:I = 0x7f020040

.field public static final miui_btn_code_lock_error_holo:I = 0x7f020041

.field public static final miui_btn_code_lock_touched_holo:I = 0x7f020042

.field public static final miui_default_lock_screen_up_arrow:I = 0x7f020043

.field public static final miui_default_lock_screen_up_arrow_dark:I = 0x7f020044

.field public static final miui_keygiaurd_left_view_list:I = 0x7f020045

.field public static final miui_keyguard_ble_unlock_check:I = 0x7f020046

.field public static final miui_keyguard_forget_password_left_normal:I = 0x7f020047

.field public static final miui_keyguard_forget_password_left_pressed:I = 0x7f020048

.field public static final miui_keyguard_forget_password_mi:I = 0x7f020049

.field public static final miui_keyguard_forget_password_right_normal:I = 0x7f02004a

.field public static final miui_keyguard_forget_password_right_pressed:I = 0x7f02004b

.field public static final miui_keyguard_forget_password_single_normal:I = 0x7f02004c

.field public static final miui_keyguard_forget_password_single_pressed:I = 0x7f02004d

.field public static final miui_keyguard_forget_password_suggesstions_one:I = 0x7f02004e

.field public static final miui_keyguard_forget_password_suggesstions_two:I = 0x7f02004f

.field public static final miui_keyguard_forget_password_suggestion_declear:I = 0x7f020050

.field public static final miui_keyguard_forget_password_suggestion_left:I = 0x7f020051

.field public static final miui_keyguard_forget_password_suggestion_right:I = 0x7f020052

.field public static final miui_keyguard_input_bg:I = 0x7f020053

.field public static final miui_keyguard_left_view_torchlight:I = 0x7f020054

.field public static final miui_keyguard_notification_item_bg_n:I = 0x7f020055

.field public static final miui_keyguard_notification_item_bg_p:I = 0x7f020056

.field public static final miui_keyguard_unlock_sim_1:I = 0x7f020057

.field public static final miui_keyguard_unlock_sim_2:I = 0x7f020058

.field public static final miui_numeric_keyboard_0:I = 0x7f020059

.field public static final miui_numeric_keyboard_1:I = 0x7f02005a

.field public static final miui_numeric_keyboard_2:I = 0x7f02005b

.field public static final miui_numeric_keyboard_3:I = 0x7f02005c

.field public static final miui_numeric_keyboard_4:I = 0x7f02005d

.field public static final miui_numeric_keyboard_5:I = 0x7f02005e

.field public static final miui_numeric_keyboard_6:I = 0x7f02005f

.field public static final miui_numeric_keyboard_7:I = 0x7f020060

.field public static final miui_numeric_keyboard_8:I = 0x7f020061

.field public static final miui_numeric_keyboard_9:I = 0x7f020062

.field public static final miui_numeric_keyboard_bg:I = 0x7f020063

.field public static final miui_numeric_keyboard_button:I = 0x7f020064

.field public static final miui_numeric_keyboard_button_p:I = 0x7f020065

.field public static final miui_numeric_keyboard_button_pressed:I = 0x7f020066

.field public static final miui_numeric_keyboard_image:I = 0x7f020067

.field public static final miui_password_unlock_screen_input_field_bg_error:I = 0x7f020068

.field public static final numeric_dot_empty:I = 0x7f020069

.field public static final numeric_dot_occupied:I = 0x7f02006a

.field public static final phone:I = 0x7f02006b

.field public static final placeholder:I = 0x7f02006c

.field public static final qq:I = 0x7f02006d

.field public static final remote_center_img:I = 0x7f02006e

.field public static final remote_center_img_dark:I = 0x7f02006f

.field public static final sim_unlock_confirm_pinnum_disable:I = 0x7f020070

.field public static final sim_unlock_confirm_pinnum_normal:I = 0x7f020071

.field public static final sim_unlock_confirm_pinnum_pressed:I = 0x7f020072

.field public static final smart_cover_battery_bg:I = 0x7f020073

.field public static final smart_cover_battery_charging:I = 0x7f020074

.field public static final smart_cover_battery_low:I = 0x7f020075

.field public static final smart_cover_miss_call:I = 0x7f020076

.field public static final smart_cover_sms:I = 0x7f020077

.field public static final unlock_face_unlock_icon:I = 0x7f020078

.field public static final wallpaper_des_more_bg:I = 0x7f020079

.field public static final wallpaper_des_more_bg_n:I = 0x7f02007a

.field public static final wallpaper_des_more_bg_p:I = 0x7f02007b

.field public static final wallpaper_link:I = 0x7f02007c

.field public static final wallpaper_play:I = 0x7f02007d

.field public static final wechat:I = 0x7f02007e

.field public static final whatsapp:I = 0x7f02007f

.field public static final wireless_charge_lighting:I = 0x7f020080

.field public static final wireless_charge_picture:I = 0x7f020081


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
