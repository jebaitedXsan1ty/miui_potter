.class Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$1;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;->onBindViewHolder(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

.field final synthetic val$holder:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iput-object p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$1;->val$holder:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 9

    const v2, 0x3f828f5c    # 1.02f

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter$1;->val$holder:Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->-get0(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
