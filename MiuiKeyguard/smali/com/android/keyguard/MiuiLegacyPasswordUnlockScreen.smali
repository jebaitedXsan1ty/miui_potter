.class public Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;
.super Lcom/android/keyguard/MiuiCommonUnlockScreen;
.source "MiuiLegacyPasswordUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$1;,
        Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$2;
    }
.end annotation


# static fields
.field private static final MINIMUM_PASSWORD_LENGTH_BEFORE_REPORT:I = 0x3


# instance fields
.field private mPasswordEditText:Landroid/widget/EditText;

.field private final mPasswordEditTextActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private final mPasswordTextWatcher:Landroid/text/TextWatcher;

.field private mUnlockButton:Landroid/widget/Button;

.field private final mVibrator:Landroid/os/Vibrator;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mUnlockButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;IZIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->onPasswordChecked(IZIZ)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->verifyPassword(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/android/keyguard/MiuiCommonUnlockScreen;-><init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    new-instance v0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$1;-><init>(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditTextActionListener:Landroid/widget/TextView$OnEditorActionListener;

    new-instance v0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$2;-><init>(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordTextWatcher:Landroid/text/TextWatcher;

    const-string/jumbo v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->initView()V

    return-void
.end method

.method private initView()V
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mContext:Landroid/content/Context;

    const v2, 0x7f030017

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->processEmergencyCall(Landroid/view/View;)V

    const v1, 0x7f0d0077

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const v1, 0x7f0d0078

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mUnlockButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditTextActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const v2, 0x800081

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->addUnlockView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mUnlockButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mUnlockButton:Landroid/widget/Button;

    new-instance v2, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$3;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$3;-><init>(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->handleAttemptLockoutIfNeed()V

    return-void
.end method

.method private onPasswordChecked(IZIZ)V
    .locals 4

    const-string/jumbo v0, "miui_keyguard_password"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "userid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";matched="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";timeoutMs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->keyguardDone(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportSuccessfulUnlockAttempt()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->reportFailedUnlockAttempt()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const v1, 0x7f020068

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFailedAttempts()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->setKeyguardLockoutAttemptDeadline(I)J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->showLockoutView(J)V

    goto :goto_0
.end method

.method private showSoftInput()V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mLockoutView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_0
    return-void
.end method

.method private verifyPassword(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_1

    invoke-virtual {p0, v3}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->setPasswordEntryInputEnabled(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    invoke-virtual {v1, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    iget-object v2, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$4;

    invoke-direct {v3, p0, v0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen$4;-><init>(Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;I)V

    invoke-static {v1, p1, v2, v3}, Lcom/android/keyguard/LockPatternChecker;->checkPasswordForUsers(Lcom/android/keyguard/LockPatternUtilsWrapper;Ljava/lang/String;Landroid/content/Context;Lcom/android/keyguard/OnCheckForUsersCallback;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPendingLockCheck:Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method


# virtual methods
.method public cleanUp()V
    .locals 0

    return-void
.end method

.method public bridge synthetic dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected handleAttemptLockout(J)V
    .locals 5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->showLockoutView(J)V

    return-void
.end method

.method public bridge synthetic handleAttemptLockoutIfNeed()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->handleAttemptLockoutIfNeed()V

    return-void
.end method

.method protected hideLockoutView()V
    .locals 2

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->hideLockoutView()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method public bridge synthetic needPasswordCheck(ZI)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->needPasswordCheck(ZI)Z

    move-result v0

    return v0
.end method

.method public needsInput()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic onFaceUnlockFailed()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onFaceUnlockFailed()V

    return-void
.end method

.method public bridge synthetic onFaceUnlockHelp(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onFaceUnlockHelp(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onFaceUnlockStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onFaceUnlockStart()V

    return-void
.end method

.method public bridge synthetic onFaceUnlockSuccess()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onFaceUnlockSuccess()V

    return-void
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->onPause()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method public onResume(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->showSoftInput()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->asyncHideLockViewOnResume()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->onResume()V

    :cond_0
    return-void
.end method

.method protected onScreenOrientationChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->initView()V

    return-void
.end method

.method public bridge synthetic processEmergencyCall(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->processEmergencyCall(Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic resetFingerprintLockoutText()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->resetFingerprintLockoutText()V

    return-void
.end method

.method public bridge synthetic setHidden(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->setHidden(Z)V

    return-void
.end method

.method protected setPasswordEntryInputEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->showSoftInput()V

    return-void
.end method

.method public bridge synthetic showBLEUnlockSucceedView()V
    .locals 0

    invoke-super {p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->showBLEUnlockSucceedView()V

    return-void
.end method

.method protected showLockoutView(J)V
    .locals 5

    const/4 v3, 0x0

    invoke-super {p0, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->showLockoutView(J)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->mPasswordEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method protected updateEmergencyCallButtonVisibility(Z)V
    .locals 2

    const v0, 0x7f0d0079

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiLegacyPasswordUnlockScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public bridge synthetic writePasswordOutTime(J)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->writePasswordOutTime(J)V

    return-void
.end method
