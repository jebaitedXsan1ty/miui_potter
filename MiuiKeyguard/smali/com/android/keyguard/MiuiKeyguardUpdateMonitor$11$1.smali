.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;
.super Landroid/os/AsyncTask;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;

.field final synthetic val$succeed:Z


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;

    iput-boolean p2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->val$succeed:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->val$succeed:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmiui/content/res/ThemeResources;->clearLockWallpaperCache()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap11(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;

    iget-object v1, v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap1(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set11(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;)Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->this$1:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;

    iget-object v2, v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get17(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;

    iget-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$11$1;->val$succeed:Z

    invoke-interface {v0, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperChangeCallback;->onWallpaperChange(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method
