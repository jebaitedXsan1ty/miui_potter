.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;
.super Landroid/content/BroadcastReceiver;
.source "MiuiKeyguardUpdateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 15

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v10, "android.intent.action.TIME_TICK"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string/jumbo v10, "android.intent.action.TIME_SET"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string/jumbo v10, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    :cond_0
    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v11

    const/16 v12, 0x12d

    invoke-virtual {v11, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string/jumbo v10, "android.provider.Telephony.SPN_STRINGS_UPDATED"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v10}, Lmiui/telephony/SubscriptionManager;->getSlotIdExtra(Landroid/content/Intent;I)I

    move-result v9

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get15(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)[Ljava/lang/CharSequence;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p2

    invoke-static {v11, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap2(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;

    move-result-object v11

    aput-object v11, v10, v9

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get16(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)[Ljava/lang/CharSequence;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-object/from16 v0, p2

    invoke-static {v11, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Landroid/content/Intent;)Ljava/lang/CharSequence;

    move-result-object v11

    aput-object v11, v10, v9

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    const/16 v11, 0x12f

    invoke-virtual {v10, v11}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    iput v9, v5, Landroid/os/Message;->arg1:I

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    invoke-virtual {v10, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_3
    const-string/jumbo v10, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const-string/jumbo v10, "status"

    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    const-string/jumbo v10, "plugged"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string/jumbo v10, "level"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string/jumbo v10, "health"

    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    new-instance v2, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;

    invoke-direct {v2, v8, v4, v6, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;-><init>(IIII)V

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap4(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$BatteryStatus;)V

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    const/16 v11, 0x12e

    invoke-virtual {v10, v11, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    invoke-virtual {v10, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_4
    const-string/jumbo v10, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v11

    invoke-static/range {p2 .. p2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;->fromIntent(Landroid/content/Intent;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$SimArgs;

    move-result-object v12

    const/16 v13, 0x130

    invoke-virtual {v11, v13, v12}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v10, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v11

    const-string/jumbo v12, "android.media.EXTRA_RINGER_MODE"

    const/4 v13, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    const/16 v13, 0x131

    const/4 v14, 0x0

    invoke-virtual {v11, v13, v12, v14}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_6
    const-string/jumbo v10, "android.intent.action.PHONE_STATE"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    const-string/jumbo v10, "state"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v11

    const/16 v12, 0x132

    invoke-virtual {v11, v12, v7}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v10, "android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v11

    const/16 v12, 0x135

    invoke-virtual {v11, v12}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_8
    const-string/jumbo v10, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const-string/jumbo v10, "android.intent.extra.user_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v11

    const-string/jumbo v12, "android.intent.extra.user_id"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    const/16 v13, 0x136

    const/4 v14, 0x0

    invoke-virtual {v11, v13, v12, v14}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_9
    iget-object v10, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v10}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v10

    iget-object v11, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$7;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v11}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v11

    const-string/jumbo v12, "android.intent.extra.user_handle"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    const/16 v13, 0x136

    const/4 v14, 0x0

    invoke-virtual {v11, v13, v12, v14}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method
