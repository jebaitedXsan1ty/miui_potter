.class Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get8(Lcom/android/keyguard/MiuiDefaultLockScreen;)J

    move-result-wide v6

    sub-long v2, v4, v6

    const-wide/16 v4, 0xc8

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get9(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap14(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get46(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/android/keyguard/AnalyticsHelper;->recordPreviewButtonCount()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get15(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/LoadingContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/keyguard/LoadingContainer;->openAd()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    iget-object v1, v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get14(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string/jumbo v1, "miui_keyguard"

    const-string/jumbo v4, "preview button goto lock screen wall paper"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/keyguard/MiuiDefaultLockScreen;->hideLockScreenInActivityManager()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-wrap3(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.miui.REQUEST_LOCKSCREEN_WALLPAPER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "showTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x258

    add-long/2addr v4, v6

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get7(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/content/Context;

    move-result-object v1

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v4}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method
