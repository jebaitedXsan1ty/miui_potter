.class Lcom/android/keyguard/MiuiKeyguardViewMediator$9;
.super Landroid/database/ContentObserver;
.source "MiuiKeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$9;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$9;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$9;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "force_fsg_nav_bar"

    invoke-static {v1, v2}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-set4(Lcom/android/keyguard/MiuiKeyguardViewMediator;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$9;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get10(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Z

    move-result v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->setIsFullScreenGestureOpened(Z)V

    return-void
.end method
