.class Lcom/android/keyguard/MiuiCommonUnlockScreen$19;
.super Landroid/os/AsyncTask;
.source "MiuiCommonUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiCommonUnlockScreen;->hideLockoutView(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

.field final synthetic val$animated:Z


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iput-boolean p2, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->val$animated:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    check-cast v0, Landroid/security/MiuiLockPatternUtils;

    invoke-virtual {v0}, Landroid/security/MiuiLockPatternUtils;->clearLockoutAttemptDeadline()V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->setPasswordLockout(Z)V

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->val$animated:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get0(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x10a0001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/android/keyguard/MiuiCommonUnlockScreen$19$1;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiCommonUnlockScreen$19$1;-><init>(Lcom/android/keyguard/MiuiCommonUnlockScreen$19;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$19;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mLockoutView:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
