.class public final Lcom/android/keyguard/R$plurals;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "plurals"
.end annotation


# static fields
.field public static final incorrect_pincode_remaining_attempts:I = 0x7f0e0001

.field public static final incorrect_pukcode_remaining_attempts:I = 0x7f0e0000

.field public static final keyguard_charging_info_charging_full_need_hour_time__text:I = 0x7f0e0006

.field public static final keyguard_charging_info_charging_full_need_min_time__text:I = 0x7f0e0005

.field public static final keyguard_charging_info_charging_full_need_time__text:I = 0x7f0e0007

.field public static final keyguard_charging_info_could_use_hour_time_text:I = 0x7f0e0009

.field public static final keyguard_charging_info_could_use_min_time_text:I = 0x7f0e0008

.field public static final keyguard_charging_info_could_use_time_text:I = 0x7f0e000a

.field public static final keyguard_charging_info_drained_hour_time_format:I = 0x7f0e000b

.field public static final keyguard_charging_info_drained_min_time_format:I = 0x7f0e000c

.field public static final keyguard_charging_info_drained_time_format:I = 0x7f0e000d

.field public static final new_notifications_msg:I = 0x7f0e0004

.field public static final phone_locked_timeout_minutes_string:I = 0x7f0e0003

.field public static final phone_locked_timeout_seconds_string:I = 0x7f0e0002


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
