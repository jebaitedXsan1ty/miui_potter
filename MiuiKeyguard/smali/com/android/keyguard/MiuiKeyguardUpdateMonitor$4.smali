.class Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;
.super Ljava/lang/Object;
.source "MiuiKeyguardUpdateMonitor.java"

# interfaces
.implements Lcom/android/keyguard/FingerprintIdentifyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private notifyCallbacks()V
    .locals 4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get4(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get5(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;->onFingerprintStateChanged(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private processFingerprintResultAnalyticsForA4(I)V
    .locals 1

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;

    invoke-direct {v0, p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;I)V

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$2;->start()V

    return-void
.end method

.method private processFingerprintResultAnalyticsForA7(I)V
    .locals 1

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;

    invoke-direct {v0, p0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;I)V

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$3;->start()V

    return-void
.end method


# virtual methods
.method public onError(I)V
    .locals 4

    const/16 v3, 0x9

    const/4 v2, 0x7

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "fingerprint identify callback result: onError"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setFingerprintRunningState(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get5(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    move-result-object v0

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->NEED_PASSWORD:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-eq v0, v1, :cond_0

    if-eq p1, v2, :cond_1

    if-eq p1, v3, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eq p1, v2, :cond_2

    if-ne p1, v3, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne p1, v3, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT_PERMANENT_FOR_O:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    :cond_3
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get7(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z

    move-result v0

    if-nez v0, :cond_4

    if-ne p1, v2, :cond_4

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    new-instance v1, Lcom/android/keyguard/FingerprintHelper;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/keyguard/FingerprintHelper;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set2(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/FingerprintHelper;)Lcom/android/keyguard/FingerprintHelper;

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "fingerprint identify callback result: addLockoutResetCallback"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/FingerprintHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->mFingerprintIdentifyCallback:Lcom/android/keyguard/FingerprintIdentifyCallback;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/FingerprintHelper;->register(Lcom/android/keyguard/FingerprintIdentifyCallback;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set4(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Z)Z

    :cond_4
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->notifyCallbacks()V

    return-void
.end method

.method public onFailed()V
    .locals 4

    const/4 v2, 0x0

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    const-string/jumbo v1, "fingerprint identify callback result: onFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->FAIL_ONE_TIME:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    :cond_1
    const-string/jumbo v0, "capricorn"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "aqua"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->processFingerprintResultAnalyticsForA7(I)V

    :goto_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->notifyCallbacks()V

    return-void

    :cond_3
    const-string/jumbo v0, "scorpio"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->processFingerprintResultAnalyticsForA4(I)V

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_FINGERPRINT_IDENTIFY_RESULT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/AnalyticsHelper;->recordCalculateEvent(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public onHelp(Ljava/lang/String;)V
    .locals 4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get4(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;->onHelp(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onIdentified(I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0, p1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set1(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;I)I

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "fingerprint identify callback result: onIdentified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get5(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-wrap0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setFingerprintRunningState(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get5(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    move-result-object v0

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->NEED_PASSWORD:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/FingerprintHelper;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/FingerprintHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/FingerprintHelper;->cancelIdentify()V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->PASS:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    :cond_2
    :goto_0
    const-string/jumbo v0, "capricorn"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "aqua"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    invoke-direct {p0, v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->processFingerprintResultAnalyticsForA7(I)V

    :goto_1
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->notifyCallbacks()V

    return-void

    :cond_4
    invoke-static {v3}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->setFingerprintRunningState(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get6(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$1;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4$1;-><init>(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_5
    const-string/jumbo v0, "scorpio"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, v4}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->processFingerprintResultAnalyticsForA4(I)V

    goto :goto_1

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_FINGERPRINT_IDENTIFY_RESULT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/AnalyticsHelper;->recordCalculateEvent(Ljava/lang/String;J)V

    goto :goto_1
.end method

.method public onReset()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    sget-object v1, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->WAITING_FOR_INPUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set3(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get10(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isShowingAndNotHidden()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get10(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get0(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isHiddenByThirdApps(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-get10(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Lcom/android/keyguard/MiuiKeyguardViewMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->startFingerprintIdentify()V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->this$0:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->-set4(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Z)Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$4;->notifyCallbacks()V

    return-void
.end method
