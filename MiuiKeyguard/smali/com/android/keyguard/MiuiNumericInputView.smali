.class public Lcom/android/keyguard/MiuiNumericInputView;
.super Landroid/widget/LinearLayout;
.source "MiuiNumericInputView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiNumericInputView$Cell;,
        Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;,
        Lcom/android/keyguard/MiuiNumericInputView$NumericRow;
    }
.end annotation


# static fields
.field public static final KEY_0:I = 0x0

.field public static final KEY_1:I = 0x1

.field public static final KEY_2:I = 0x2

.field public static final KEY_3:I = 0x3

.field public static final KEY_4:I = 0x4

.field public static final KEY_5:I = 0x5

.field public static final KEY_6:I = 0x6

.field public static final KEY_7:I = 0x7

.field public static final KEY_8:I = 0x8

.field public static final KEY_9:I = 0x9

.field public static final KEY_CALL:I = 0xb

.field public static final KEY_DEL:I = 0xa

.field public static final KEY_PLACEHOLDER:I = -0x1

.field private static final sKeyboardAlphabetMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mEmergencyCallView:Landroid/view/View;

.field private mFifthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

.field private mFirstRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

.field private mForthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

.field private mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

.field private mSecondRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

.field private mThirdRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiNumericInputView;)Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericInputView;->mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

    return-object v0
.end method

.method static synthetic -get1()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiNumericInputView;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiNumericInputView;->mEmergencyCallView:Landroid/view/View;

    return-object p1
.end method

.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "+"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "ABC"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "DEF"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "GHI"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "JKL"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "MNO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "PQRS"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "TUV"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/keyguard/MiuiNumericInputView;->sKeyboardAlphabetMap:Ljava/util/Map;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "WXYZ"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiNumericInputView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiNumericInputView;->initView(Landroid/content/Context;)V

    return-void
.end method

.method private createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;
    .locals 10

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v9, Landroid/view/animation/AnimationSet;

    invoke-direct {v9, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v9, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericInputView;->mContext:Landroid/content/Context;

    const v3, 0x10c0008

    invoke-virtual {v9, v0, v3}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/content/Context;I)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Lcom/android/keyguard/MiuiNumericInputView$1;

    invoke-direct {v0, p0, p1}, Lcom/android/keyguard/MiuiNumericInputView$1;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/view/View;)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-object v9
.end method

.method private initView(Landroid/content/Context;)V
    .locals 9

    const/16 v8, 0xa

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x1

    invoke-virtual {p0, v5}, Lcom/android/keyguard/MiuiNumericInputView;->setOrientation(I)V

    new-instance v2, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {v2, p0, p1}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFirstRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFirstRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    const/4 v3, 0x2

    const/4 v4, 0x3

    invoke-static {v2, v5, v3, v4}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->-wrap0(Lcom/android/keyguard/MiuiNumericInputView$NumericRow;III)V

    new-instance v2, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {v2, p0, p1}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mSecondRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mSecondRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    const/4 v3, 0x4

    const/4 v4, 0x5

    const/4 v5, 0x6

    invoke-static {v2, v3, v4, v5}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->-wrap0(Lcom/android/keyguard/MiuiNumericInputView$NumericRow;III)V

    new-instance v2, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {v2, p0, p1}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mThirdRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mThirdRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    const/4 v3, 0x7

    const/16 v4, 0x8

    const/16 v5, 0x9

    invoke-static {v2, v3, v4, v5}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->-wrap0(Lcom/android/keyguard/MiuiNumericInputView$NumericRow;III)V

    const-string/jumbo v2, "connectivity"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v2, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {v2, p0, p1}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mForthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mForthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-static {v2, v6, v7, v8}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->-wrap0(Lcom/android/keyguard/MiuiNumericInputView$NumericRow;III)V

    :goto_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFirstRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiNumericInputView;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mSecondRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiNumericInputView;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mThirdRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiNumericInputView;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mForthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiNumericInputView;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFifthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFifthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiNumericInputView;->addView(Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    new-instance v2, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {v2, p0, p1}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mForthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mForthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-static {v2, v6, v7, v6}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->-wrap0(Lcom/android/keyguard/MiuiNumericInputView$NumericRow;III)V

    new-instance v2, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {v2, p0, p1}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFifthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    iget-object v2, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFifthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    const/16 v3, 0xb

    invoke-static {v2, v3, v6, v8}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->-wrap0(Lcom/android/keyguard/MiuiNumericInputView$NumericRow;III)V

    goto :goto_0
.end method


# virtual methods
.method public getEmergencyCall()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericInputView;->mEmergencyCallView:Landroid/view/View;

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericInputView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setNumericInputListener(Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiNumericInputView;->mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

    return-void
.end method

.method public startAnimation()V
    .locals 10

    const/16 v3, 0x32

    const/16 v0, 0x32

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFirstRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {p0, v7}, Lcom/android/keyguard/MiuiNumericInputView;->createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v2

    const-wide/16 v8, 0x32

    invoke-virtual {v2, v8, v9}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFirstRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {v7, v2}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mSecondRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {p0, v7}, Lcom/android/keyguard/MiuiNumericInputView;->createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v5

    const-wide/16 v8, 0x64

    invoke-virtual {v5, v8, v9}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mSecondRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {v7, v5}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mThirdRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {p0, v7}, Lcom/android/keyguard/MiuiNumericInputView;->createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v6

    const-wide/16 v8, 0x96

    invoke-virtual {v6, v8, v9}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mThirdRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {v7, v6}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mForthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {p0, v7}, Lcom/android/keyguard/MiuiNumericInputView;->createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v4

    const-wide/16 v8, 0xc8

    invoke-virtual {v4, v8, v9}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mForthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {v7, v4}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFifthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFifthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-direct {p0, v7}, Lcom/android/keyguard/MiuiNumericInputView;->createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v1

    const-wide/16 v8, 0xfa

    invoke-virtual {v1, v8, v9}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v7, p0, Lcom/android/keyguard/MiuiNumericInputView;->mFifthRow:Lcom/android/keyguard/MiuiNumericInputView$NumericRow;

    invoke-virtual {v7, v1}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public updateEmergencyCallVisibility(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiNumericInputView;->mEmergencyCallView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiNumericInputView;->mEmergencyCallView:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method
