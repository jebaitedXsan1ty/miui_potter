.class public final Lcom/android/keyguard/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final badget_text_color:I = 0x7f08003c

.field public static final battery_charging_progress_view_msg_text_color:I = 0x7f08000f

.field public static final battery_charging_progress_view_text_color:I = 0x7f08000e

.field public static final clock_text_color:I = 0x7f08003a

.field public static final content_text_color:I = 0x7f08003b

.field public static final face_unlock_input_camera_view_bg_color:I = 0x7f080032

.field public static final face_unlock_input_cameraview_color:I = 0x7f080035

.field public static final face_unlock_input_msg_color:I = 0x7f080034

.field public static final face_unlock_input_title_color:I = 0x7f080033

.field public static final face_unlock_introduction_msg_color:I = 0x7f080037

.field public static final face_unlock_introduction_title_color:I = 0x7f080036

.field public static final face_unlock_suggestion_first_color:I = 0x7f080038

.field public static final face_unlock_suggestion_second_color:I = 0x7f080039

.field public static final full_smart_cover_battery_text_color:I = 0x7f080026

.field public static final full_smart_cover_date_color:I = 0x7f080024

.field public static final full_smart_cover_num_color:I = 0x7f080023

.field public static final full_smart_cover_time_color:I = 0x7f080025

.field public static final keyguard_charging_info_bg_color:I = 0x7f080030

.field public static final keyguard_charging_info_cicle_color:I = 0x7f08002f

.field public static final keyguard_charging_info_temp_high_color:I = 0x7f08002e

.field public static final keyguard_charging_info_text_dark_color:I = 0x7f08002c

.field public static final keyguard_charging_info_text_middle_color:I = 0x7f08002d

.field public static final keyguard_charging_info_vertical_separator_color:I = 0x7f08002b

.field public static final keyguard_charging_view_bg_color:I = 0x7f080027

.field public static final keyguard_charging_view_bg_color_dark:I = 0x7f080028

.field public static final keyguard_charging_view_color:I = 0x7f080029

.field public static final keyguard_charging_view_need_reset_color:I = 0x7f08002a

.field public static final keyguard_forget_password_text_color_light:I = 0x7f080031

.field public static final keyguard_wireless_charging_bg:I = 0x7f08003d

.field public static final lock_screen_numeric_keyboard_alphabet_text_color:I = 0x7f08000d

.field public static final lock_screen_numeric_keyboard_number_text_color:I = 0x7f08000c

.field public static final miui_common_unlock_screen_common_dark_text_color:I = 0x7f080002

.field public static final miui_common_unlock_screen_common_text_color:I = 0x7f080000

.field public static final miui_common_unlock_screen_common_text_color_new:I = 0x7f080001

.field public static final miui_common_unlock_screen_common_time_dark_text_color:I = 0x7f080003

.field public static final miui_default_lock_screen_notification_item_msg_text_color:I = 0x7f080009

.field public static final miui_default_lock_screen_notification_item_time_text_color:I = 0x7f080008

.field public static final miui_default_lock_screen_notification_item_title_text_color:I = 0x7f08000a

.field public static final miui_default_lock_screen_unlock_hint_text_color:I = 0x7f08000b

.field public static final miui_keyguard_ble_unlock_text_color:I = 0x7f080012

.field public static final miui_keyguard_ble_unlock_view_background:I = 0x7f080013

.field public static final miui_keyguard_error_text_color:I = 0x7f080006

.field public static final miui_keyguard_lock_out_forget_password_emergency:I = 0x7f08003e

.field public static final miui_numeric_keyboard_emergency_button_normal_bg:I = 0x7f080010

.field public static final miui_numeric_keyboard_emergency_button_pressed_bg:I = 0x7f080011

.field public static final miui_numeric_keyboard_emergency_text_color:I = 0x7f08003f

.field public static final miui_pattern_lockscreen_heavy_paint_color:I = 0x7f080005

.field public static final miui_pattern_lockscreen_paint_color:I = 0x7f080004

.field public static final mixed_unlock_screen_emergency_call_text_color:I = 0x7f080016

.field public static final pattern_lockscreen_paint_error_color:I = 0x7f080007

.field public static final secure_keyguard_fingerprint_hint_msg_text_color:I = 0x7f080015

.field public static final secure_keyguard_fingerprint_hint_title_text_color:I = 0x7f080014

.field public static final smart_cover_battery_low_text_color:I = 0x7f080022

.field public static final smart_cover_battery_text_color:I = 0x7f080021

.field public static final smart_cover_num_color:I = 0x7f08001f

.field public static final smart_cover_time_color:I = 0x7f080020

.field public static final wallpaper_des_content_text_color:I = 0x7f080018

.field public static final wallpaper_des_more_text_color:I = 0x7f080019

.field public static final wallpaper_des_more_text_dark_color:I = 0x7f08001b

.field public static final wallpaper_des_text_dark_color:I = 0x7f08001a

.field public static final wallpaper_des_title_text_color:I = 0x7f080017

.field public static final wallpaper_global_des_content_text_color:I = 0x7f08001d

.field public static final wallpaper_global_des_from_text_color:I = 0x7f08001e

.field public static final wallpaper_global_des_title_text_color:I = 0x7f08001c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
