.class Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;
.super Ljava/lang/Object;
.source "WallPaperDesGlobalView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->toggleDropDown(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

.field final synthetic val$showMarkView:Z


# direct methods
.method constructor <init>(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;->this$0:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    iput-boolean p2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;->val$showMarkView:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-boolean v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;->val$showMarkView:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;->this$0:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    invoke-static {v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->-get0(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;->this$0:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    invoke-static {v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->-get1(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)Landroid/widget/TextView;

    move-result-object v1

    sub-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;->this$0:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    invoke-static {v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->-get0(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)Landroid/widget/ImageView;

    move-result-object v1

    sub-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$4;->this$0:Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;

    invoke-static {v1}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;->-get1(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method
