.class public Lcom/android/keyguard/wallpaper/WallPaperDesView;
.super Landroid/widget/LinearLayout;
.source "WallPaperDesView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/wallpaper/WallPaperDesView$1;,
        Lcom/android/keyguard/wallpaper/WallPaperDesView$2;
    }
.end annotation


# instance fields
.field mClickListener:Landroid/view/View$OnClickListener;

.field private mContent:Landroid/widget/TextView;

.field private mMiuiKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

.field protected mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field private mPreviewButton:Landroid/view/View;

.field private mTitle:Landroid/widget/TextView;

.field protected mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

.field private final mWallpaperInfoListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/wallpaper/WallPaperDesView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mPreviewButton:Landroid/view/View;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/keyguard/wallpaper/WallPaperDesView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/wallpaper/WallPaperDesView$1;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesView;)V

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/keyguard/wallpaper/WallPaperDesView$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/wallpaper/WallPaperDesView$2;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesView;)V

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfoListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/keyguard/wallpaper/WallPaperDesView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/wallpaper/WallPaperDesView$1;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesView;)V

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/keyguard/wallpaper/WallPaperDesView$2;

    invoke-direct {v0, p0}, Lcom/android/keyguard/wallpaper/WallPaperDesView$2;-><init>(Lcom/android/keyguard/wallpaper/WallPaperDesView;)V

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfoListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;

    return-void
.end method

.method private updateViewText()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v2, v2, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContent:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v2, v2, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->content:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget v1, v1, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->linkType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const v0, 0x7f02007c

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4, v4, v0, v4}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget v1, v1, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->linkType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const v0, 0x7f02007d

    goto :goto_0
.end method


# virtual methods
.method public canDisplay()Z
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v0, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v0, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->content:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfoListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerWallpaperInfoListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfoListener:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->unregisterWallpaperInfoListener(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$WallpaperInfoListener;)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    const v0, 0x7f0d0073

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f0d0074

    invoke-virtual {p0, v0}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContent:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public refreshWallpaperInfo()V
    .locals 4

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getWallpaperInfo()Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v0, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->content:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mWallpaperInfo:Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;

    iget-object v1, v1, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->content:Ljava/lang/String;

    const-string/jumbo v2, "\\s*"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/keyguard/wallpaper/mode/WallpaperInfo;->content:Ljava/lang/String;

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->updateViewText()V

    :cond_1
    return-void
.end method

.method public setKeyguardScreenCallback(Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mMiuiKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    return-void
.end method

.method public setKeyguardUpdateMonitor(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    return-void
.end method

.method public setPreviewButton(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mPreviewButton:Landroid/view/View;

    return-void
.end method

.method public updateColorByWallpaper(Z)V
    .locals 3

    const v2, 0x7f08001a

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/wallpaper/WallPaperDesView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
