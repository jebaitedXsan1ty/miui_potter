.class Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$EaseOutBackInterpolator;
.super Ljava/lang/Object;
.source "WallPaperDesGlobalView.java"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EaseOutBackInterpolator"
.end annotation


# static fields
.field private static final s:F = 2.70158f


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$EaseOutBackInterpolator;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/wallpaper/WallPaperDesGlobalView$EaseOutBackInterpolator;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr p1, v3

    mul-float v0, p1, p1

    const v1, 0x406ce6b0

    mul-float/2addr v1, p1

    const v2, 0x402ce6b0

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    return v0
.end method
