.class public Lcom/android/keyguard/FingerprintHelper;
.super Ljava/lang/Object;
.source "FingerprintHelper.java"


# static fields
.field private static final DELAY_MS:J = 0x12cL

.field private static final DELAY_VIBRATOR_MS:J = 0x3e8L

.field private static final MSG_FAILED:I = 0x65

.field private static final MSG_HELP:I = 0x66

.field private static final MSG_IDENTIFIED:I = 0x64

.field private static sFingerprintIdentifyCallbackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/FingerprintIdentifyCallback;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCancellationSignal:Landroid/os/CancellationSignal;

.field private mContext:Landroid/content/Context;

.field private mDoubleClicked:Z

.field private mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/FingerprintHelper;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/FingerprintHelper;->mDoubleClicked:Z

    return v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/FingerprintHelper;->handleOnAuthenticationHelp(Lcom/android/keyguard/FingerprintIdentifyCallback;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/keyguard/FingerprintHelper;->handleOnFailed(Lcom/android/keyguard/FingerprintIdentifyCallback;)V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/FingerprintHelper;->handleOnIdentified(Lcom/android/keyguard/FingerprintIdentifyCallback;Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/keyguard/FingerprintHelper;->sFingerprintIdentifyCallbackList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    iput-object p1, p0, Lcom/android/keyguard/FingerprintHelper;->mContext:Landroid/content/Context;

    return-void
.end method

.method private handleOnAuthenticationHelp(Lcom/android/keyguard/FingerprintIdentifyCallback;Ljava/lang/String;)V
    .locals 0

    invoke-interface {p1, p2}, Lcom/android/keyguard/FingerprintIdentifyCallback;->onHelp(Ljava/lang/String;)V

    return-void
.end method

.method private handleOnFailed(Lcom/android/keyguard/FingerprintIdentifyCallback;)V
    .locals 0

    invoke-interface {p1}, Lcom/android/keyguard/FingerprintIdentifyCallback;->onFailed()V

    return-void
.end method

.method private handleOnIdentified(Lcom/android/keyguard/FingerprintIdentifyCallback;Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;)V
    .locals 1

    invoke-virtual {p2}, Landroid/hardware/fingerprint/FingerprintManager$AuthenticationResult;->getFingerprint()Landroid/hardware/fingerprint/Fingerprint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/fingerprint/Fingerprint;->getFingerId()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/android/keyguard/FingerprintIdentifyCallback;->onIdentified(I)V

    return-void
.end method

.method private initFingerprintManager()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "fingerprint"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/fingerprint/FingerprintManager;

    iput-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    :cond_0
    return-void
.end method


# virtual methods
.method public OnDoubleClickHome()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/FingerprintHelper;->mDoubleClicked:Z

    return-void
.end method

.method public cancelIdentify()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mCancellationSignal:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    :cond_0
    return-void
.end method

.method public getFingerprintIds()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/keyguard/FingerprintHelper;->initFingerprintManager()V

    iget-object v4, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v4, :cond_0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v4}, Landroid/hardware/fingerprint/FingerprintManager;->getEnrolledFingerprints()Ljava/util/List;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/fingerprint/Fingerprint;

    invoke-virtual {v0}, Landroid/hardware/fingerprint/Fingerprint;->getFingerId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method public identify(Lcom/android/keyguard/FingerprintIdentifyCallback;Ljava/util/List;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/keyguard/FingerprintIdentifyCallback;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "callback can not be null, and ids can not be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/FingerprintHelper;->initFingerprintManager()V

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_2

    return-void

    :cond_2
    new-instance v0, Landroid/os/CancellationSignal;

    invoke-direct {v0}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mCancellationSignal:Landroid/os/CancellationSignal;

    new-instance v4, Lcom/android/keyguard/FingerprintHelper$2;

    invoke-direct {v4, p0, p1}, Lcom/android/keyguard/FingerprintHelper$2;-><init>(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;)V

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    iget-object v2, p0, Lcom/android/keyguard/FingerprintHelper;->mCancellationSignal:Landroid/os/CancellationSignal;

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/hardware/fingerprint/FingerprintManager;->authenticate(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;Landroid/os/CancellationSignal;ILandroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;Landroid/os/Handler;)V

    return-void
.end method

.method public isHardwareDetected()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/FingerprintHelper;->initFingerprintManager()V

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->isHardwareDetected()Z

    move-result v0

    goto :goto_0
.end method

.method public register(Lcom/android/keyguard/FingerprintIdentifyCallback;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/FingerprintHelper;->initFingerprintManager()V

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_1

    return-void

    :cond_1
    sget-object v0, Lcom/android/keyguard/FingerprintHelper;->sFingerprintIdentifyCallbackList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/android/keyguard/FingerprintHelper;->sFingerprintIdentifyCallbackList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    new-instance v1, Lcom/android/keyguard/FingerprintHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/android/keyguard/FingerprintHelper$1;-><init>(Lcom/android/keyguard/FingerprintHelper;Lcom/android/keyguard/FingerprintIdentifyCallback;)V

    invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager;->addLockoutResetCallback(Landroid/hardware/fingerprint/FingerprintManager$LockoutResetCallback;)V

    :cond_2
    return-void
.end method

.method public resetFingerLockoutTime()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/keyguard/FingerprintHelper;->initFingerprintManager()V

    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/FingerprintHelper;->mFingerprintMgr:Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager;->resetTimeout([B)V

    return-void
.end method
