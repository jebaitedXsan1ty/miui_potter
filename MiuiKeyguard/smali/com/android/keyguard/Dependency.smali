.class public Lcom/android/keyguard/Dependency;
.super Ljava/lang/Object;
.source "Dependency.java"


# static fields
.field public static SUPPORT_AOD:Z

.field private static sHost:Lcom/android/keyguard/doze/DozeHost;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "support_aod"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/keyguard/Dependency;->SUPPORT_AOD:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHost()Lcom/android/keyguard/doze/DozeHost;
    .locals 1

    sget-object v0, Lcom/android/keyguard/Dependency;->sHost:Lcom/android/keyguard/doze/DozeHost;

    return-object v0
.end method

.method public static setHost(Lcom/android/keyguard/doze/DozeHost;)V
    .locals 0

    sput-object p0, Lcom/android/keyguard/Dependency;->sHost:Lcom/android/keyguard/doze/DozeHost;

    return-void
.end method
