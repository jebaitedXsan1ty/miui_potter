.class Lcom/android/keyguard/AODView$1;
.super Ljava/lang/Object;
.source "AODView.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/AODView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/AODView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/AODView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/AODView$1;->this$0:Lcom/android/keyguard/AODView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private updateNotificationList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/AODView$1;->this$0:Lcom/android/keyguard/AODView;

    invoke-static {v2}, Lcom/android/keyguard/AODView;->-get1(Lcom/android/keyguard/AODView;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v2, p0, Lcom/android/keyguard/AODView$1;->this$0:Lcom/android/keyguard/AODView;

    invoke-static {v2}, Lcom/android/keyguard/AODView;->-get0(Lcom/android/keyguard/AODView;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/AODView$1;->this$0:Lcom/android/keyguard/AODView;

    invoke-static {v2}, Lcom/android/keyguard/AODView;->-get1(Lcom/android/keyguard/AODView;)Ljava/util/List;

    move-result-object v2

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->pkgName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/AODView$1;->this$0:Lcom/android/keyguard/AODView;

    invoke-static {v2}, Lcom/android/keyguard/AODView;->-get1(Lcom/android/keyguard/AODView;)Ljava/util/List;

    move-result-object v2

    iget-object v3, v0, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->pkgName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/android/keyguard/AODView$1;->this$0:Lcom/android/keyguard/AODView;

    invoke-static {v2}, Lcom/android/keyguard/AODView;->-get1(Lcom/android/keyguard/AODView;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    :cond_1
    iget-object v2, p0, Lcom/android/keyguard/AODView$1;->this$0:Lcom/android/keyguard/AODView;

    invoke-static {v2}, Lcom/android/keyguard/AODView;->-wrap0(Lcom/android/keyguard/AODView;)V

    return-void
.end method


# virtual methods
.method public onChange(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/keyguard/AODView$1;->updateNotificationList(Ljava/util/List;)V

    return-void
.end method

.method public onRegistered(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/keyguard/AODView$1;->updateNotificationList(Ljava/util/List;)V

    return-void
.end method
