.class Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiUnlockScreenVerticalClock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$1;->this$0:Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "time-zone"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$1;->this$0:Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;

    new-instance v2, Lmiui/date/Calendar;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-direct {v2, v3}, Lmiui/date/Calendar;-><init>(Ljava/util/TimeZone;)V

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->-set0(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;Lmiui/date/Calendar;)Lmiui/date/Calendar;

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$1;->this$0:Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;

    invoke-static {v1}, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->-get2(Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock$1;->this$0:Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;

    iget-object v2, v2, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;->mUpdateTimeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
