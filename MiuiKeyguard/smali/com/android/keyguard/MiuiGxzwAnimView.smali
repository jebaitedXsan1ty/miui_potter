.class public Lcom/android/keyguard/MiuiGxzwAnimView;
.super Landroid/widget/FrameLayout;
.source "MiuiGxzwAnimView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiGxzwAnimView$1;,
        Lcom/android/keyguard/MiuiGxzwAnimView$TextState;
    }
.end annotation


# static fields
.field private static final MSG_DISSMISS_USE_PASSWORD:I = 0x3e9


# instance fields
.field private mGxzwAnimHeight:I

.field private mGxzwAnimWidth:I

.field private mGxzwIconHeight:I

.field private mGxzwIconWidth:I

.field private mGxzwIconX:I

.field private mGxzwIconY:I

.field private mHandler:Landroid/os/Handler;

.field private mShowing:Z

.field private mStatusBarHeight:I

.field private mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mShowing:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mStatusBarHeight:I

    sget-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->NONE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwAnimView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiGxzwAnimView$1;-><init>(Lcom/android/keyguard/MiuiGxzwAnimView;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwAnimView;->initView()V

    return-void
.end method

.method private initView()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwAnimView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000e

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v1, 0x7f0d004f

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiGxzwAnimView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwAnimView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput v3, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwIconX:I

    iput v3, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwIconY:I

    iput v3, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwIconWidth:I

    iput v3, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwIconHeight:I

    const v1, 0x7f0c00bc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwAnimWidth:I

    const v1, 0x7f0c00bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwAnimHeight:I

    const/16 v1, 0x1304

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiGxzwAnimView;->setSystemUiVisibility(I)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    iget-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mShowing:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mShowing:Z

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwAnimView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    return-void
.end method

.method public dismissAllText()V
    .locals 2

    sget-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->NONE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public dismissMorePressure()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    sget-object v1, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->MORE_PRESSURE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->NONE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public dismissTryAgain()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    sget-object v1, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->TRY_AGAIN:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->NONE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public dismissUsePassword()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    sget-object v1, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->USE_PASSWORD:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->NONE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public show()V
    .locals 9

    iget-boolean v3, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mShowing:Z

    if-eqz v3, :cond_0

    return-void

    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mShowing:Z

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwAnimView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    iget v3, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwIconX:I

    iget v4, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwAnimWidth:I

    iget v5, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwIconWidth:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    sub-int v7, v3, v4

    iget v3, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwIconY:I

    iget v4, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwAnimHeight:I

    iget v5, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwIconHeight:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    sub-int v8, v3, v4

    iget v1, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwAnimWidth:I

    iget v2, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mGxzwAnimHeight:I

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v3}, Lcom/android/keyguard/MiuiGxzwAnimView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d9

    const v4, 0x1020018

    const/4 v5, -0x2

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/16 v3, 0x33

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iput v7, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v8, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    const-string/jumbo v3, "gxzw_anim"

    invoke-virtual {v0, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    invoke-interface {v6, p0, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public showMorePressure()V
    .locals 2

    sget-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->MORE_PRESSURE:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const v1, 0x7f0b009f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public showTryAgain()V
    .locals 2

    sget-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->TRY_AGAIN:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const v1, 0x7f0b009d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public showUsePassword()V
    .locals 5

    const/16 v4, 0x3e9

    sget-object v0, Lcom/android/keyguard/MiuiGxzwAnimView$TextState;->USE_PASSWORD:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextState:Lcom/android/keyguard/MiuiGxzwAnimView$TextState;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const v1, 0x7f0b009e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwAnimView;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
