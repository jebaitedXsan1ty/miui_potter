.class Lcom/android/keyguard/MiuiSimPINUnlockScreen;
.super Lcom/android/keyguard/MiuiCommonUnlockScreen;
.source "MiuiSimPINUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiSimPINUnlockScreen$1;,
        Lcom/android/keyguard/MiuiSimPINUnlockScreen$CheckSimPin;
    }
.end annotation


# instance fields
.field protected mErrorMsg:Landroid/widget/TextView;

.field private final mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

.field protected mInputPinEditText:Landroid/widget/EditText;

.field protected mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

.field private mSignalAvailable:Z

.field protected mSimId:I

.field protected mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

.field protected mUnlockSimButton:Landroid/widget/Button;

.field protected mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiSimPINUnlockScreen;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;ILcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 8

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v3, p3

    move-object v4, p4

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/android/keyguard/MiuiCommonUnlockScreen;-><init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardScreenCallback;ZLcom/android/keyguard/MiuiKeyguardViewMediator;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSignalAvailable:Z

    new-instance v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen$1;-><init>(Lcom/android/keyguard/MiuiSimPINUnlockScreen;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

    iput p6, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimId:I

    const-string/jumbo v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PowerManager;

    const-string/jumbo v0, "Unlocking sim"

    const/4 v1, 0x6

    invoke-virtual {v7, v1, v0}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->initView()V

    return-void
.end method

.method private checkPin()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->pokeWakelock()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->showSimUnlockProgressDialog()V

    new-instance v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;-><init>(Lcom/android/keyguard/MiuiSimPINUnlockScreen;Lcom/android/keyguard/MiuiSimPINUnlockScreen;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen$4;->start()V

    return-void
.end method

.method private initView()V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mContext:Landroid/content/Context;

    const v3, 0x7f03001d

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d008d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    const v2, 0x7f0d008e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mUnlockSimButton:Landroid/widget/Button;

    const v2, 0x7f0d008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mErrorMsg:Landroid/widget/TextView;

    const v2, 0x7f0d0080

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/keyguard/MiuiNumericInputView;

    iput-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    iget-object v3, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mInputListener:Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;

    invoke-virtual {v2, v3}, Lcom/android/keyguard/MiuiNumericInputView;->setNumericInputListener(Lcom/android/keyguard/MiuiNumericInputView$MiuiNumericInputListener;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiNumericInputView;->getEmergencyCall()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->processEmergencyCall(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mUnlockSimButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mUnlockSimButton:Landroid/widget/Button;

    new-instance v3, Lcom/android/keyguard/MiuiSimPINUnlockScreen$2;

    invoke-direct {v3, p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen$2;-><init>(Lcom/android/keyguard/MiuiSimPINUnlockScreen;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->addUnlockView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->getEditTextWatcher()Landroid/text/TextWatcher;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mDigitalClock:Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;

    invoke-virtual {v2, v6}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->setVisibility(I)V

    const v2, 0x7f0d008c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->getPhoneCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v2, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimId:I

    if-nez v2, :cond_0

    const v2, 0x7f020057

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    const v2, 0x7f020058

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public cleanUp()V
    .locals 0

    return-void
.end method

.method protected displayErrorMsg(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->displayErrorMsg(Ljava/lang/String;)V

    return-void
.end method

.method protected displayErrorMsg(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mErrorMsg:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mErrorMsg:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected getEditTextWatcher()Landroid/text/TextWatcher;
    .locals 1

    new-instance v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$3;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen$3;-><init>(Lcom/android/keyguard/MiuiSimPINUnlockScreen;)V

    return-object v0
.end method

.method protected getFocusedEditText()Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method protected handleAttemptLockout(J)V
    .locals 0

    return-void
.end method

.method protected hideProgressDialog()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    :cond_1
    return-void
.end method

.method public needsInput()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->hideProgressDialog()V

    return-void
.end method

.method public onResume(Z)V
    .locals 0

    return-void
.end method

.method protected onScreenOrientationChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->initView()V

    return-void
.end method

.method protected removeLast(Landroid/widget/EditText;)V
    .locals 3

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/EditText;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    return-void
.end method

.method protected setPasswordEntryInputEnabled(Z)V
    .locals 0

    return-void
.end method

.method protected showSimUnlockProgressDialog()V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mContext:Landroid/content/Context;

    sget v2, Lmiui/R$style;->Theme_Light_Dialog:I

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7d9

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method protected unlockAction()V
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->checkPin()V

    :goto_0
    return-void

    :cond_0
    const v1, 0x7f0b003c

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->displayErrorMsg(I)V

    goto :goto_0
.end method

.method protected updateEmergencyCallButtonVisibility(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mSignalAvailable:Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mNumericInputView:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/MiuiNumericInputView;->updateEmergencyCallVisibility(Z)V

    return-void
.end method
