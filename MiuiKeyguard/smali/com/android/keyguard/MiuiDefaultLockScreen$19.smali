.class Lcom/android/keyguard/MiuiDefaultLockScreen$19;
.super Landroid/os/AsyncTask;
.source "MiuiDefaultLockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;->refreshChargingInfo(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

.field final synthetic val$anim:Z


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iput-boolean p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->val$anim:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v1

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get28(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get0(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getChargingHintText(ZI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get24(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0008

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set1(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/lang/String;)Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get3(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get42(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get0(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setChargingInfo(Ljava/lang/String;II)V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->val$anim:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getDefaultUnlockHintText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$19;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set1(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
