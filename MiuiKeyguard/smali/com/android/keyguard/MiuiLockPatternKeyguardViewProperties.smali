.class public Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;
.super Ljava/lang/Object;
.source "MiuiLockPatternKeyguardViewProperties.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardViewProperties;


# instance fields
.field private final mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

.field private final mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private final mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

.field private final mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;


# direct methods
.method public constructor <init>(Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iput-object p2, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iput-object p3, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    new-instance v0, Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-direct {v0, p1}, Lcom/android/keyguard/LockPatternUtilsWrapper;-><init>(Lcom/android/internal/widget/LockPatternUtils;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    return-void
.end method


# virtual methods
.method public createKeyguardView(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardWindowController;)Lcom/android/keyguard/MiuiKeyguardViewBase;
    .locals 7

    new-instance v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v4, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    iget-object v5, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardViewMediator;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/keyguard/MiuiKeyguardWindowController;)V

    return-object v0
.end method

.method public isSecure()Z
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isSecure(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isSimSecured()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardViewProperties;->mLockPatternUtilsWrapper:Lcom/android/keyguard/LockPatternUtilsWrapper;

    invoke-virtual {v0}, Lcom/android/keyguard/LockPatternUtilsWrapper;->isPermanentlyLocked()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
