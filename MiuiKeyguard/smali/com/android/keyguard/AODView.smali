.class public Lcom/android/keyguard/AODView;
.super Landroid/widget/FrameLayout;
.source "AODView.java"

# interfaces
.implements Lcom/android/keyguard/util/ContentProviderBinder$QueryCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/AODView$1;
    }
.end annotation


# static fields
.field private static final PHONE_PKG:Ljava/lang/String; = "com.android.server.telecom"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private m24HourFormat:Z

.field private mAodBg:Landroid/widget/ImageView;

.field private mBinders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/keyguard/util/ContentProviderBinder;",
            ">;"
        }
    .end annotation
.end field

.field private mClockHourView:Landroid/widget/TextView;

.field private mClockMinuteView:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mDateView:Landroid/widget/TextView;

.field private mFirstIcon:Lcom/android/keyguard/BadgetImageView;

.field private mHost:Lcom/android/keyguard/doze/DozeHost;

.field private mIconMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mMissCallNum:I

.field private mNotificationArray:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

.field protected mNotificationNum:I

.field private mPosictionController:Lcom/android/keyguard/util/AODUpdatePositionController;

.field private mSecondIcon:Lcom/android/keyguard/BadgetImageView;

.field protected mShowMissCall:Z

.field private mTableModeContainer:Landroid/view/View;

.field private mThirdIcon:Lcom/android/keyguard/BadgetImageView;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/AODView;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/AODView;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mNotificationArray:Ljava/util/List;

    return-object v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/AODView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/AODView;->handleUpdateIcons()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/keyguard/AODView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/keyguard/AODView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/AODView;->mBinders:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/AODView;->mNotificationArray:Ljava/util/List;

    new-instance v0, Lcom/android/keyguard/AODView$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/AODView$1;-><init>(Lcom/android/keyguard/AODView;)V

    iput-object v0, p0, Lcom/android/keyguard/AODView;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    iput-object p1, p0, Lcom/android/keyguard/AODView;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/keyguard/util/AODUpdatePositionController;

    invoke-direct {v0, p1}, Lcom/android/keyguard/util/AODUpdatePositionController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/AODView;->mPosictionController:Lcom/android/keyguard/util/AODUpdatePositionController;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "com.tencent.mm"

    const v2, 0x7f02007e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "com.tencent.mobileqq"

    const v2, 0x7f02006d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "com.whatsapp"

    const v2, 0x7f02007f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "com.facebook.orca"

    const v2, 0x7f020015

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "jp.naver.line.android"

    const v2, 0x7f02003a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "com.google.android.gm"

    const v2, 0x7f02001b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "com.android.email"

    const v2, 0x7f02003e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "com.google.android.calendar"

    const v2, 0x7f02001a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "com.android.calendar"

    const v2, 0x7f02000a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    const-string/jumbo v1, "com.android.server.telecom"

    const v2, 0x7f02006b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private bindView(Lcom/android/keyguard/BadgetImageView;I)V
    .locals 5

    const/4 v3, 0x0

    invoke-direct {p0, p2}, Lcom/android/keyguard/AODView;->getPkg(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/android/keyguard/AODView;->mIconMap:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {p1, v3}, Lcom/android/keyguard/BadgetImageView;->setVisibility(I)V

    const-string/jumbo v4, "com.android.server.telecom"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v3, p0, Lcom/android/keyguard/AODView;->mMissCallNum:I

    :cond_0
    invoke-virtual {p1, v3}, Lcom/android/keyguard/BadgetImageView;->setBadget(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/AODView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v3, 0x66

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    invoke-virtual {p1, v0}, Lcom/android/keyguard/BadgetImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_1
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Lcom/android/keyguard/BadgetImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private fillMissedCall()V
    .locals 4

    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    const-string/jumbo v2, "number"

    const/4 v3, 0x0

    aput-object v2, v0, v3

    const-string/jumbo v1, "type=3 AND new=1"

    sget-object v2, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2}, Lcom/android/keyguard/AODView;->addContentProviderBinder(Landroid/net/Uri;)Lcom/android/keyguard/util/ContentProviderBinder$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/keyguard/util/ContentProviderBinder$Builder;->setColumns([Ljava/lang/String;)Lcom/android/keyguard/util/ContentProviderBinder$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/keyguard/util/ContentProviderBinder$Builder;->setWhere(Ljava/lang/String;)Lcom/android/keyguard/util/ContentProviderBinder$Builder;

    return-void
.end method

.method private getPkg(I)Ljava/lang/String;
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mNotificationArray:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/AODView;->mNotificationArray:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private handleUpdateDate()V
    .locals 8

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    new-instance v0, Lmiui/date/Calendar;

    invoke-direct {v0, v3}, Lmiui/date/Calendar;-><init>(Ljava/util/TimeZone;)V

    iget-object v4, p0, Lcom/android/keyguard/AODView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0017

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iget-object v4, p0, Lcom/android/keyguard/AODView;->mDateView:Landroid/widget/TextView;

    new-instance v5, Ljava/util/Date;

    invoke-virtual {v0}, Lmiui/date/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private handleUpdateIcons()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mFirstIcon:Lcom/android/keyguard/BadgetImageView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/keyguard/AODView;->bindView(Lcom/android/keyguard/BadgetImageView;I)V

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mSecondIcon:Lcom/android/keyguard/BadgetImageView;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/keyguard/AODView;->bindView(Lcom/android/keyguard/BadgetImageView;I)V

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mThirdIcon:Lcom/android/keyguard/BadgetImageView;

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/android/keyguard/AODView;->bindView(Lcom/android/keyguard/BadgetImageView;I)V

    return-void
.end method

.method private handleUpdateTime()V
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    new-instance v0, Lmiui/date/Calendar;

    invoke-direct {v0, v4}, Lmiui/date/Calendar;-><init>(Ljava/util/TimeZone;)V

    invoke-virtual {p0}, Lcom/android/keyguard/AODView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/android/keyguard/util/Utils;->getHourMinformat(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    new-instance v3, Ljava/text/SimpleDateFormat;

    invoke-direct {v3, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    new-instance v6, Ljava/util/Date;

    invoke-virtual {v0}, Lmiui/date/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    const/16 v6, 0x12

    invoke-virtual {v0, v6}, Lmiui/date/Calendar;->get(I)I

    move-result v1

    iget-boolean v6, p0, Lcom/android/keyguard/AODView;->m24HourFormat:Z

    if-nez v6, :cond_0

    const/16 v6, 0xc

    if-le v1, v6, :cond_0

    add-int/lit8 v1, v1, -0xc

    :cond_0
    iget-boolean v6, p0, Lcom/android/keyguard/AODView;->m24HourFormat:Z

    if-nez v6, :cond_1

    if-nez v1, :cond_1

    const/16 v1, 0xc

    :cond_1
    iget-object v6, p0, Lcom/android/keyguard/AODView;->mClockHourView:Landroid/widget/TextView;

    const-string/jumbo v7, "%02d"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/android/keyguard/AODView;->mClockMinuteView:Landroid/widget/TextView;

    const-string/jumbo v7, "%02d"

    new-array v8, v11, [Ljava/lang/Object;

    const/16 v9, 0x14

    invoke-virtual {v0, v9}, Lmiui/date/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private makeNormalPanel()V
    .locals 3

    iget-object v1, p0, Lcom/android/keyguard/AODView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string/jumbo v2, "fonts/MIround02-Regular.ttf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    const v1, 0x7f0d0003

    invoke-virtual {p0, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/AODView;->mTableModeContainer:Landroid/view/View;

    const v1, 0x7f0d0005

    invoke-virtual {p0, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/keyguard/AODView;->mClockHourView:Landroid/widget/TextView;

    const v1, 0x7f0d0006

    invoke-virtual {p0, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/keyguard/AODView;->mClockMinuteView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/AODView;->mClockHourView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v1, p0, Lcom/android/keyguard/AODView;->mClockMinuteView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    const v1, 0x7f0d0007

    invoke-virtual {p0, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/keyguard/AODView;->mDateView:Landroid/widget/TextView;

    const v1, 0x7f0d0008

    invoke-virtual {p0, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/BadgetImageView;

    iput-object v1, p0, Lcom/android/keyguard/AODView;->mFirstIcon:Lcom/android/keyguard/BadgetImageView;

    const v1, 0x7f0d0009

    invoke-virtual {p0, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/BadgetImageView;

    iput-object v1, p0, Lcom/android/keyguard/AODView;->mSecondIcon:Lcom/android/keyguard/BadgetImageView;

    const v1, 0x7f0d000a

    invoke-virtual {p0, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/BadgetImageView;

    iput-object v1, p0, Lcom/android/keyguard/AODView;->mThirdIcon:Lcom/android/keyguard/BadgetImageView;

    const v1, 0x7f0d0004

    invoke-virtual {p0, v1}, Lcom/android/keyguard/AODView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/keyguard/AODView;->mAodBg:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/keyguard/AODView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/keyguard/KeyguardCompatibilityHelperForL;->is24HourFormat(Landroid/content/Context;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/keyguard/AODView;->m24HourFormat:Z

    return-void
.end method


# virtual methods
.method public addContentProviderBinder(Landroid/net/Uri;)Lcom/android/keyguard/util/ContentProviderBinder$Builder;
    .locals 2

    new-instance v0, Lcom/android/keyguard/util/ContentProviderBinder;

    iget-object v1, p0, Lcom/android/keyguard/AODView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/keyguard/util/ContentProviderBinder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p0}, Lcom/android/keyguard/util/ContentProviderBinder;->setQueryCompleteListener(Lcom/android/keyguard/util/ContentProviderBinder$QueryCompleteListener;)V

    invoke-virtual {v0, p1}, Lcom/android/keyguard/util/ContentProviderBinder;->setUri(Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/android/keyguard/AODView;->mBinders:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/keyguard/util/ContentProviderBinder$Builder;

    invoke-direct {v1, v0}, Lcom/android/keyguard/util/ContentProviderBinder$Builder;-><init>(Lcom/android/keyguard/util/ContentProviderBinder;)V

    return-object v1
.end method

.method public handleUpdateView()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/AODView;->mPosictionController:Lcom/android/keyguard/util/AODUpdatePositionController;

    iget-object v1, p0, Lcom/android/keyguard/AODView;->mTableModeContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/util/AODUpdatePositionController;->updatePosition(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/android/keyguard/AODView;->handleUpdateTime()V

    invoke-direct {p0}, Lcom/android/keyguard/AODView;->handleUpdateDate()V

    invoke-direct {p0}, Lcom/android/keyguard/AODView;->handleUpdateIcons()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/android/keyguard/AODView;->fillMissedCall()V

    iget-object v2, p0, Lcom/android/keyguard/AODView;->mBinders:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/util/ContentProviderBinder;

    invoke-virtual {v0}, Lcom/android/keyguard/util/ContentProviderBinder;->init()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/AODView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/AODView;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v2, v3}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->registerListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    iget-object v2, p0, Lcom/android/keyguard/AODView;->mBinders:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/util/ContentProviderBinder;

    invoke-virtual {v0}, Lcom/android/keyguard/util/ContentProviderBinder;->finish()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/keyguard/AODView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keyguard/AODView;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v2, v3}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->unregisterListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    invoke-direct {p0}, Lcom/android/keyguard/AODView;->makeNormalPanel()V

    invoke-virtual {p0}, Lcom/android/keyguard/AODView;->handleUpdateView()V

    return-void
.end method

.method public onQueryCompleted(Landroid/net/Uri;I)V
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/android/keyguard/AODView;->mShowMissCall:Z

    iput p2, p0, Lcom/android/keyguard/AODView;->mMissCallNum:I

    :cond_1
    invoke-direct {p0}, Lcom/android/keyguard/AODView;->handleUpdateIcons()V

    return-void
.end method
