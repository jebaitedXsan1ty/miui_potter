.class public Lcom/android/keyguard/AnalyticsHelper;
.super Ljava/lang/Object;
.source "AnalyticsHelper.java"


# static fields
.field private static final APP_ID:Ljava/lang/String; = "1000271"

.field private static final APP_KEY:Ljava/lang/String; = "420100086271"

.field private static CATEGORY_KEYGUARD:Ljava/lang/String; = null

.field private static final FLAG_STAGING_FILE:Ljava/lang/String; = "/data/system/miuikeyguard_staging"

.field public static KEY_CAMERA_FROM_KEYGUARD:Ljava/lang/String;

.field public static KEY_FACE_UNLOCK_CLICK_ADD:Ljava/lang/String;

.field public static KEY_FACE_UNLOCK_CLICK_INTRODUCTION:Ljava/lang/String;

.field public static KEY_FACE_UNLOCK_CLICK_SUGGESTION:Ljava/lang/String;

.field public static KEY_FACE_UNLOCK_FAIL_COUNT:Ljava/lang/String;

.field public static KEY_FACE_UNLOCK_INPUT_SUCCESS:Ljava/lang/String;

.field public static KEY_FACE_UNLOCK_LOCKED:Ljava/lang/String;

.field public static KEY_FACE_UNLOCK_NOTIFICATION_TOGGLE:Ljava/lang/String;

.field public static KEY_FACE_UNLOCK_STATE:Ljava/lang/String;

.field public static KEY_KEYGUARD_AUTO_LOCK_TIME_VALUE:Ljava/lang/String;

.field public static KEY_KEYGUARD_CHARGING_CLICK:Ljava/lang/String;

.field public static KEY_KEYGUARD_CHARGING_SHOW:Ljava/lang/String;

.field public static KEY_KEYGUARD_DOWNLOAD_LOCKSCREEN_MAGAZINE:Ljava/lang/String;

.field public static KEY_KEYGUARD_FINGERPRINT_IDENTIFY_RESULT:Ljava/lang/String;

.field public static KEY_KEYGUARD_FINGERPRINT_STATE:Ljava/lang/String;

.field public static KEY_KEYGUARD_HAS_OWNER_INFO:Ljava/lang/String;

.field public static KEY_KEYGUARD_PREVIEW_BUTTON_CLICK:Ljava/lang/String;

.field public static KEY_KEYGUARD_SCREENON:Ljava/lang/String;

.field public static KEY_KEYGUARD_SCREENON_BY_NOTIFICATION:Ljava/lang/String;

.field public static KEY_KEYGUARD_SCREENON_BY_PICK_UP:Ljava/lang/String;

.field public static KEY_KEYGUARD_SCREEN_ON_BY_NOTIFICATION_TOGGLE:Ljava/lang/String;

.field public static KEY_KEYGUARD_SECURE_TYPE:Ljava/lang/String;

.field public static KEY_KEYGUARD_SLEEP_BY_PUT_DOWN:Ljava/lang/String;

.field public static KEY_KEYGUARD_UNLOCK_WAY:Ljava/lang/String;

.field public static KEY_REMOTECENTER_FROM_KEYGUARD:Ljava/lang/String;

.field public static KEY_TRACK_MUSIC:Ljava/lang/String;

.field public static KEY_TRACK_MUSIC_SHOW_COUNT:Ljava/lang/String;

.field public static UNLOCK_WAY_BAND:Ljava/lang/String;

.field public static UNLOCK_WAY_FACE:Ljava/lang/String;

.field public static UNLOCK_WAY_FP:Ljava/lang/String;

.field public static UNLOCK_WAY_NONE:Ljava/lang/String;

.field public static UNLOCK_WAY_PW:Ljava/lang/String;

.field public static UNLOCK_WAY_SKIP:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "systemui_keyguard"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->CATEGORY_KEYGUARD:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_screenon"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SCREENON:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_screenon_by_notification"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SCREENON_BY_NOTIFICATION:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_screenon_by_pick_up"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SCREENON_BY_PICK_UP:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_sleep_by_put_down"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SLEEP_BY_PUT_DOWN:Ljava/lang/String;

    const-string/jumbo v0, "camera_from_keyguard"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_CAMERA_FROM_KEYGUARD:Ljava/lang/String;

    const-string/jumbo v0, "remotecenter_from_keyguard"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_REMOTECENTER_FROM_KEYGUARD:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_secure_type"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SECURE_TYPE:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_has_owner_info"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_HAS_OWNER_INFO:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_auto_lock"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_AUTO_LOCK_TIME_VALUE:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_screen_on_when_notification"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SCREEN_ON_BY_NOTIFICATION_TOGGLE:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_fingerprint_state_new"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_FINGERPRINT_STATE:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_fp_identify_result"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_FINGERPRINT_IDENTIFY_RESULT:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_charging_show"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_CHARGING_SHOW:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_charging_click"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_CHARGING_CLICK:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_unlock_way"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_UNLOCK_WAY:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_preview_button_click"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_PREVIEW_BUTTON_CLICK:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_download_lockscreen_magazine"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_DOWNLOAD_LOCKSCREEN_MAGAZINE:Ljava/lang/String;

    const-string/jumbo v0, "face_unlock_input_success"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_INPUT_SUCCESS:Ljava/lang/String;

    const-string/jumbo v0, "face_unlock_click_suggestion"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_CLICK_SUGGESTION:Ljava/lang/String;

    const-string/jumbo v0, "face_unlock_click_introduction"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_CLICK_INTRODUCTION:Ljava/lang/String;

    const-string/jumbo v0, "face_unlock_click_add"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_CLICK_ADD:Ljava/lang/String;

    const-string/jumbo v0, "face_unlock_state"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_STATE:Ljava/lang/String;

    const-string/jumbo v0, "face_unlock_state_fail_count"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_FAIL_COUNT:Ljava/lang/String;

    const-string/jumbo v0, "face_unlock_notification_toggle"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_NOTIFICATION_TOGGLE:Ljava/lang/String;

    const-string/jumbo v0, "face_unlock_locked"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_LOCKED:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_track_music_click"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_TRACK_MUSIC:Ljava/lang/String;

    const-string/jumbo v0, "keyguard_track_music_show_count"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_TRACK_MUSIC_SHOW_COUNT:Ljava/lang/String;

    const-string/jumbo v0, "fp"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_FP:Ljava/lang/String;

    const-string/jumbo v0, "pw"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_PW:Ljava/lang/String;

    const-string/jumbo v0, "band"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_BAND:Ljava/lang/String;

    const-string/jumbo v0, "none"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_NONE:Ljava/lang/String;

    const-string/jumbo v0, "skip"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_SKIP:Ljava/lang/String;

    const-string/jumbo v0, "face"

    sput-object v0, Lcom/android/keyguard/AnalyticsHelper;->UNLOCK_WAY_FACE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .locals 4

    const-string/jumbo v1, "1000271"

    const-string/jumbo v2, "420100086271"

    const-string/jumbo v3, "keyguard"

    invoke-static {p0, v1, v2, v3}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->initialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    invoke-static {v1, v2, v3}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->setUploadPolicy(IJ)V

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->enableExceptionCatcher(Z)V

    new-instance v1, Ljava/io/File;

    const-string/jumbo v2, "/data/system/miuikeyguard_staging"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    xor-int/lit8 v1, v0, 0x1

    invoke-static {v1}, Lcom/xiaomi/mistatistic/sdk/CustomSettings;->setUseSystemStatService(Z)V

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->enableLog()V

    :cond_0
    return-void
.end method

.method public static record(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->CATEGORY_KEYGUARD:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static record(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEventAnonymous(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static recordCalculateEvent(Ljava/lang/String;J)V
    .locals 1

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->CATEGORY_KEYGUARD:Ljava/lang/String;

    invoke-static {v0, p0, p1, p2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCalculateEventAnonymous(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public static recordCalculateEvent(Ljava/lang/String;JLjava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->CATEGORY_KEYGUARD:Ljava/lang/String;

    invoke-static {v0, p0, p1, p2, p3}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCalculateEventAnonymous(Ljava/lang/String;Ljava/lang/String;JLjava/util/Map;)V

    return-void
.end method

.method public static recordDownloadLockScreenMagazine()V
    .locals 1

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_DOWNLOAD_LOCKSCREEN_MAGAZINE:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;)V

    return-void
.end method

.method public static recordEnum(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->CATEGORY_KEYGUARD:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/android/keyguard/AnalyticsHelper;->recordEnum(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static recordEnum(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordStringPropertyEventAnonymous(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static recordPreviewButtonCount()V
    .locals 1

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_PREVIEW_BUTTON_CLICK:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;)V

    return-void
.end method

.method public static recordUnlockWay(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "way"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/android/keyguard/AnalyticsHelper;->CATEGORY_KEYGUARD:Ljava/lang/String;

    sget-object v2, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_UNLOCK_WAY:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/keyguard/AnalyticsHelper;->track(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static track(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lcom/xiaomi/mistatistic/sdk/MiStatInterface;->recordCountEventAnonymous(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static trackFaceUnlockFailCount(Z)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "hasface"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/android/keyguard/AnalyticsHelper;->CATEGORY_KEYGUARD:Ljava/lang/String;

    sget-object v2, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_FAIL_COUNT:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/keyguard/AnalyticsHelper;->track(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static trackFaceUnlockLocked()V
    .locals 2

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->CATEGORY_KEYGUARD:Ljava/lang/String;

    sget-object v1, Lcom/android/keyguard/AnalyticsHelper;->KEY_FACE_UNLOCK_LOCKED:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static trackMusicClick(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "action"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "pkg"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/android/keyguard/AnalyticsHelper;->CATEGORY_KEYGUARD:Ljava/lang/String;

    sget-object v2, Lcom/android/keyguard/AnalyticsHelper;->KEY_TRACK_MUSIC:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/keyguard/AnalyticsHelper;->track(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static trackMusicShowCount()V
    .locals 1

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_TRACK_MUSIC_SHOW_COUNT:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;)V

    return-void
.end method
