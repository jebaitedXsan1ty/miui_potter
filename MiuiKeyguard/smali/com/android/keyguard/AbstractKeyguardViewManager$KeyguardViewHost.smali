.class Lcom/android/keyguard/AbstractKeyguardViewManager$KeyguardViewHost;
.super Landroid/widget/FrameLayout;
.source "AbstractKeyguardViewManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/AbstractKeyguardViewManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeyguardViewHost"
.end annotation


# instance fields
.field private final mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$KeyguardViewHost;->mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;Lcom/android/keyguard/AbstractKeyguardViewManager$KeyguardViewHost;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/AbstractKeyguardViewManager$KeyguardViewHost;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewCallback;)V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/android/keyguard/AbstractKeyguardViewManager$KeyguardViewHost;->mCallback:Lcom/android/keyguard/MiuiKeyguardViewCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardViewCallback;->keyguardDoneDrawing()V

    return-void
.end method
