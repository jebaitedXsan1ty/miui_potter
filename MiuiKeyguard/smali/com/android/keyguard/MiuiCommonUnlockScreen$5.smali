.class Lcom/android/keyguard/MiuiCommonUnlockScreen$5;
.super Ljava/lang/Object;
.source "MiuiCommonUnlockScreen.java"

# interfaces
.implements Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiCommonUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiCommonUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFingerprintStateChanged(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)V
    .locals 4

    const v3, 0x7f0b0036

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0, v2}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-set0(Lcom/android/keyguard/MiuiCommonUnlockScreen;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get0(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-wrap2(Lcom/android/keyguard/MiuiCommonUnlockScreen;J)V

    return-void

    :cond_0
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->FAIL_ONE_TIME:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->NEED_PASSWORD:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne p1, v0, :cond_4

    :cond_1
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->FAIL_ONE_TIME:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-eq p1, v0, :cond_2

    invoke-static {p1}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerPrintLockOut(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get7(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/os/Vibrator;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->vibrate(Landroid/os/Vibrator;)V

    :cond_3
    invoke-static {p1}, Lcom/android/keyguard/MiuiKeyguardUtils;->isFingerPrintLockOut(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b0034

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b0035

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-eq p1, v0, :cond_5

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT_PERMANENT_FOR_O:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne p1, v0, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b0037

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->LOCKOUT_PERMANENT_FOR_O:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b0038

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_6
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_7
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->PASS:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne p1, v0, :cond_8

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v1, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v1, v1, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getFingerId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-wrap0(Lcom/android/keyguard/MiuiCommonUnlockScreen;I)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-boolean v0, Lmiui/util/OldmanUtil;->IS_ELDER_MODE:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b004c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_8
    :goto_0
    sget-object v0, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;->WAITING_FOR_INPUT:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintIdentificationState;

    if-ne p1, v0, :cond_9

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b0039

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_9
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiCommonUnlockScreen;->mFaceUnlockIcon:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_a
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get0(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isGreenKidActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b0043

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_b
    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get0(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->isPhoneCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b0042

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_c
    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isUserAuthenticatedSinceBootSecond()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b004a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b004b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get2(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiCommonUnlockScreen$5;->this$0:Lcom/android/keyguard/MiuiCommonUnlockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiCommonUnlockScreen;->-get1(Lcom/android/keyguard/MiuiCommonUnlockScreen;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public onHelp(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
