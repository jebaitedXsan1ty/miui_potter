.class public Lcom/android/keyguard/KeyguardCompatibilityHelperForM;
.super Ljava/lang/Object;
.source "KeyguardCompatibilityHelperForM.java"


# static fields
.field private static final USER_AUTHENTICATED_PROP_KEY:Ljava/lang/String; = "sys.miui.user_authenticated"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMasterStreamType(Landroid/media/AudioManager;)I
    .locals 1

    invoke-virtual {p0}, Landroid/media/AudioManager;->getUiSoundsStreamType()I

    move-result v0

    return v0
.end method

.method public static isInputRestrictedByDeviceProvisioned(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static isKeyguardFingerprintDiabled(Landroid/content/Context;)Z
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x0

    const-string/jumbo v3, "device_policy"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v1, v4, v3}, Landroid/app/admin/DevicePolicyManager;->getKeyguardDisabledFeatures(Landroid/content/ComponentName;I)I

    move-result v0

    :cond_0
    and-int/lit8 v3, v0, 0x20

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method public static isUserAuthenticatedSinceBoot(Landroid/content/Context;)Z
    .locals 2

    const/4 v0, 0x0

    const-string/jumbo v1, "sys.miui.user_authenticated"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForN;->isUserUnlocked(Landroid/content/Context;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public static postScreenTurnedOnCallbackIfNeed(Ljava/lang/Object;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->screenTurnedOnCallback(Ljava/lang/Object;Landroid/os/IBinder;)V

    return-void
.end method

.method public static screenTurnedOnCallback(Ljava/lang/Object;Landroid/os/IBinder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    instance-of v0, p0, Lcom/android/internal/policy/IKeyguardDrawnCallback;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/android/internal/policy/IKeyguardDrawnCallback;

    invoke-interface {p0}, Lcom/android/internal/policy/IKeyguardDrawnCallback;->onDrawn()V

    :cond_0
    return-void
.end method

.method public static setUserAuthenticatedSinceBoot()V
    .locals 2

    const-string/jumbo v0, "sys.miui.user_authenticated"

    const-string/jumbo v1, "true"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static vibrate(Landroid/os/Vibrator;)V
    .locals 0

    return-void
.end method

.method public static wakeUp(JLjava/lang/String;Landroid/content/Context;)V
    .locals 2

    const-string/jumbo v1, "power"

    invoke-virtual {p3, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0, p0, p1, p2}, Landroid/os/PowerManager;->wakeUp(JLjava/lang/String;)V

    return-void
.end method

.method public static wakeUp(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1, p0, p1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->wakeUp(JLjava/lang/String;Landroid/content/Context;)V

    return-void
.end method
