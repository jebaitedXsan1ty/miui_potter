.class public Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;
.super Landroid/widget/FrameLayout;
.source "MiuiDefaultLockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MainLockView"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$1;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$4;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$6;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$7;,
        Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;
    }
.end annotation


# static fields
.field private static final MSG_HIDE_NOTIFICATION_VIEW:I = 0x0

.field private static final MSG_SET_MUSIC_CONTROL_MODE:I = 0x1


# instance fields
.field private mArrowAndHintTextRunnable:Ljava/lang/Runnable;

.field private mBottomViewGroup:Landroid/view/View;

.field private mChargingBackArrowClickListener:Landroid/view/View$OnClickListener;

.field private mChargingBackArrowTouchListener:Landroid/view/View$OnTouchListener;

.field private mChargingEnterAnimRunnable:Ljava/lang/Runnable;

.field private mChargingInfoBackArrow:Landroid/widget/ImageView;

.field private mChargingInfoBottomCicle:Landroid/view/View;

.field private mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

.field private mChargingListAndBackArrow:Landroid/widget/LinearLayout;

.field private mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

.field private mChargingViewClickListener:Landroid/view/View$OnClickListener;

.field private mDeviceManageText:Landroid/widget/TextView;

.field private mDigitalClock:Lcom/android/keyguard/MiuiKeyguardBaseClock;

.field private mFaceUnlockIconView:Landroid/widget/ImageView;

.field private mHandler:Landroid/os/Handler;

.field private mIconViewOnClickListener:Landroid/view/View$OnClickListener;

.field private mLeftActionIconView:Landroid/widget/ImageView;

.field private mNotificationAndMusicControlContainer:Landroid/view/ViewGroup;

.field private mNotificationItemAnimator:Lcom/android/keyguard/NotificationItemAnimator;

.field private mNotificationViewAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v7/widget/RecyclerView$Adapter",
            "<",
            "Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

.field private mRightActionIconView:Landroid/widget/ImageView;

.field private mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

.field private mShowingSlideHint:Z

.field private mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

.field private mUpArrow:Landroid/widget/ImageView;

.field private mWallPaperAndClockAndPreview:Landroid/view/View;

.field private mWallPaperDes:Lcom/android/keyguard/wallpaper/WallPaperDesView;

.field private mWallpaperRefreshOnClickListener:Landroid/view/View$OnClickListener;

.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mArrowAndHintTextRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mBottomViewGroup:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get10(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mLeftActionIconView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -get11(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationAndMusicControlContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic -get12(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/support/v7/widget/RecyclerView$Adapter;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViewAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    return-object v0
.end method

.method static synthetic -get13(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardRecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    return-object v0
.end method

.method static synthetic -get14(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mRightActionIconView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -get15(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/LoadingContainer;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    return-object v0
.end method

.method static synthetic -get16(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowingSlideHint:Z

    return v0
.end method

.method static synthetic -get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    return-object v0
.end method

.method static synthetic -get18(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUpArrow:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -get19(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperAndClockAndPreview:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingEnterAnimRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic -get20(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/wallpaper/WallPaperDesView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperDes:Lcom/android/keyguard/wallpaper/WallPaperDesView;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoBackArrow:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/charge/MiuiKeyguardChargingView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingViewClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get8(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/MiuiKeyguardBaseClock;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mDigitalClock:Lcom/android/keyguard/MiuiKeyguardBaseClock;

    return-object v0
.end method

.method static synthetic -get9(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mFaceUnlockIconView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowingSlideHint:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->chargingInfoDown()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->chargingInfoUp()V

    return-void
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->startEnterAnimation()V

    return-void
.end method

.method static synthetic -wrap3(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->startExitAnimation()V

    return-void
.end method

.method static synthetic -wrap4(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->updateColorByWallpaper()V

    return-void
.end method

.method static synthetic -wrap5(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->updateMusicControllerMode()V

    return-void
.end method

.method static synthetic -wrap6(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->updateNotificationViewState()V

    return-void
.end method

.method public constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Landroid/content/Context;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-boolean v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowingSlideHint:Z

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$1;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$1;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$2;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallpaperRefreshOnClickListener:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$3;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mIconViewOnClickListener:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$4;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$4;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingViewClickListener:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$5;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingEnterAnimRunnable:Ljava/lang/Runnable;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$6;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$6;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingBackArrowTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$7;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$7;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingBackArrowClickListener:Landroid/view/View$OnClickListener;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$8;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mArrowAndHintTextRunnable:Ljava/lang/Runnable;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mContext:Landroid/content/Context;

    sget v3, Lmiui/R$style;->Theme_Light:I

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f03001c

    invoke-static {v1, v2, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d005e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiUnlockScreenVerticalClock;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mDigitalClock:Lcom/android/keyguard/MiuiKeyguardBaseClock;

    const v1, 0x7f0d0082

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperAndClockAndPreview:Landroid/view/View;

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_4

    new-instance v2, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mContext:Landroid/content/Context;

    sget v3, Lmiui/R$style;->Theme_Light:I

    invoke-direct {v2, v1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperAndClockAndPreview:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    const v3, 0x7f030014

    invoke-static {v2, v3, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    :goto_0
    const v1, 0x7f0d006a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/wallpaper/WallPaperDesView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperDes:Lcom/android/keyguard/wallpaper/WallPaperDesView;

    const v1, 0x7f0d001b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/LoadingContainer;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    const v1, 0x7f0d0087

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUpArrow:Landroid/widget/ImageView;

    const v1, 0x7f0d0088

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/UnlockHintSwitcher;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    const v1, 0x7f0d0086

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mLeftActionIconView:Landroid/widget/ImageView;

    const v1, 0x7f0d0089

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mRightActionIconView:Landroid/widget/ImageView;

    const v1, 0x7f0d0084

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    const v1, 0x7f0d0083

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationAndMusicControlContainer:Landroid/view/ViewGroup;

    const v1, 0x7f0d0085

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mBottomViewGroup:Landroid/view/View;

    const v1, 0x7f0d008a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get24(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setNeedRepositionDevice(Z)V

    const v1, 0x7f0d003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingListAndBackArrow:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0042

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoBackArrow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoBackArrow:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingBackArrowTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoBackArrow:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingBackArrowClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0d0019

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingViewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get24(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setNeedRepositionDevice(Z)V

    const v1, 0x7f0d003a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoBottomCicle:Landroid/view/View;

    const v1, 0x7f0d002e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mFaceUnlockIconView:Landroid/widget/ImageView;

    const v1, 0x7f0d008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mDeviceManageText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mDeviceManageText:Landroid/widget/TextView;

    invoke-static {p2, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForO;->updateDisclosure(Landroid/content/Context;Landroid/widget/TextView;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get15(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/LoadingContainer;->setKeyguardUpdateMonitor(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallpaperRefreshOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/LoadingContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0045

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/LoadingContainer;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get2(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/view/View;

    move-result-object v1

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap2(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get22(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardMoveLeftView;

    move-result-object v1

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap2(Lcom/android/keyguard/MiuiDefaultLockScreen;)I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardMoveLeftView;->setTranslationX(F)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mDigitalClock:Lcom/android/keyguard/MiuiKeyguardBaseClock;

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get15(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->setKeyguardUpdateMonitor(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperDes:Lcom/android/keyguard/wallpaper/WallPaperDesView;

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get15(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->setKeyguardUpdateMonitor(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperDes:Lcom/android/keyguard/wallpaper/WallPaperDesView;

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get14(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->setKeyguardScreenCallback(Lcom/android/keyguard/MiuiKeyguardScreenCallback;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperDes:Lcom/android/keyguard/wallpaper/WallPaperDesView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    invoke-virtual {v1, v2}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->setPreviewButton(Landroid/view/View;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mLeftActionIconView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mIconViewOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mRightActionIconView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mIconViewOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mRightActionIconView:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get34(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get16(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mLeftActionIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get33(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mRightActionIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mLeftActionIconView:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->refreshPreviewButton()V

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->addView(Landroid/view/View;)V

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationAndMusicControlContainer:Landroid/view/ViewGroup;

    invoke-static {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    new-instance v2, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$12;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$12;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    invoke-virtual {v1, v2}, Lcom/android/keyguard/UnlockHintSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->initNotificationList()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->updateColorByWallpaper()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->updateMusicControllerView()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    invoke-virtual {p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getDefaultUnlockHintText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    sget-boolean v1, Lmiui/os/Build;->IS_CM_CUSTOMIZATION_TEST:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    new-instance v2, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$13;

    invoke-direct {v2, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$13;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    invoke-virtual {v1, v2}, Lcom/android/keyguard/UnlockHintSwitcher;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mContext:Landroid/content/Context;

    const/high16 v3, 0x10a0000

    invoke-virtual {v1, v2, v3}, Lcom/android/keyguard/UnlockHintSwitcher;->setInAnimation(Landroid/content/Context;I)V

    return-void

    :cond_4
    new-instance v2, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mContext:Landroid/content/Context;

    sget v3, Lmiui/R$style;->Theme_Light:I

    invoke-direct {v2, v1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperAndClockAndPreview:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    const v3, 0x7f030015

    invoke-static {v2, v3, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0
.end method

.method private chargingInfoDown()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    invoke-virtual {v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->startDownAnim()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setOtherViewVisibility(I)V

    return-void
.end method

.method private chargingInfoUp()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    invoke-virtual {v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->onCharingViewClick()V

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->setOtherViewVisibility(I)V

    return-void
.end method

.method private getMaxNotificationCount()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get7(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get28(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->hasNavigationBar()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private initNotificationList()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen;Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationsAdapter;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViewAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    new-instance v0, Lcom/android/keyguard/NotificationItemAnimator;

    invoke-direct {v0}, Lcom/android/keyguard/NotificationItemAnimator;-><init>()V

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationItemAnimator:Lcom/android/keyguard/NotificationItemAnimator;

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    invoke-virtual {v0, v3}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->setHasFixedSize(Z)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    new-instance v1, Lcom/android/keyguard/MiuiNotificationLayoutManager;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/keyguard/MiuiNotificationLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViewAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationItemAnimator:Lcom/android/keyguard/NotificationItemAnimator;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$9;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$9;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->setOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set17(Lcom/android/keyguard/MiuiDefaultLockScreen;I)I

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->updateNotificationViewState()V

    return-void
.end method

.method private setOtherViewVisibility(I)V
    .locals 4

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    aput v2, v1, v3

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v2, v1, v3

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v1, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;

    invoke-direct {v1}, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;

    invoke-direct {v1, p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$10;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$11;

    invoke-direct {v1, p0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$11;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method private startEnterAnimation()V
    .locals 14

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set19(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get16(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v11, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v11, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v11, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/high16 v2, -0x40000000    # -2.0f

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v11, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    const-wide/16 v2, 0x1e

    invoke-virtual {v11, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mLeftActionIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v11}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get33(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v12, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v12, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v12, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v12, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v12, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    const-wide/16 v2, 0x1e

    invoke-virtual {v12, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mRightActionIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v12}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    new-instance v13, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v13, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v13, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const-wide/16 v2, 0x1e

    invoke-virtual {v13, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    invoke-virtual {v1, v13}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v1

    invoke-virtual {v1, v13}, Lmiui/maml/component/MamlView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_3
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get28(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mContext:Landroid/content/Context;

    const/high16 v2, 0x10a0000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v10

    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v9, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v9, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    const-wide/16 v2, 0x1e

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUpArrow:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUpArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mArrowAndHintTextRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_4
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    invoke-virtual {v1}, Lcom/android/keyguard/UnlockHintSwitcher;->clearAnimation()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/keyguard/UnlockHintSwitcher;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    invoke-virtual {v1, v13}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_5
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get30(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    new-instance v1, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$14;

    invoke-direct {v1, p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView$14;-><init>(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {p0, v1, v2, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private startExitAnimation()V
    .locals 12

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set19(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get16(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v9, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/high16 v4, -0x40000000    # -2.0f

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    const/4 v1, 0x1

    invoke-virtual {v9, v1}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mLeftActionIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get33(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v10, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v10, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v10, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mRightActionIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, 0x40000000    # 2.0f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    invoke-virtual {v1, v0}, Lcom/android/keyguard/UnlockHintSwitcher;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v11, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v11, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v11, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const/4 v1, 0x1

    invoke-virtual {v11, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    invoke-virtual {v1, v11}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_2
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v1

    invoke-virtual {v1, v11}, Lmiui/maml/component/MamlView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_3
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    invoke-virtual {v1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    invoke-virtual {v1, v11}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_4
    return-void
.end method

.method private updateColorByWallpaper()V
    .locals 5

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->isTopLightColorMode()Z

    move-result v1

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->isBottomLightColorMode()Z

    move-result v0

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUpArrow:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const v3, 0x7f020043

    :goto_0
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mRightActionIconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    const v3, 0x7f02000b

    :goto_1
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mLeftActionIconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    const v3, 0x7f02006e

    :goto_2
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mFaceUnlockIconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    const v3, 0x7f020013

    :goto_3
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v0, :cond_4

    const v3, 0x7f08000b

    :goto_4
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    invoke-virtual {v3}, Lcom/android/keyguard/UnlockHintSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mUnlockHintTextView:Lcom/android/keyguard/UnlockHintSwitcher;

    invoke-virtual {v3}, Lcom/android/keyguard/UnlockHintSwitcher;->getNextView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mChargingInfoContainer:Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;

    xor-int/lit8 v4, v0, 0x1

    invoke-virtual {v3, v4}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setDarkMode(Z)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mDigitalClock:Lcom/android/keyguard/MiuiKeyguardBaseClock;

    invoke-virtual {v3, v1}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->updateColorByWallpaper(Z)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperDes:Lcom/android/keyguard/wallpaper/WallPaperDesView;

    invoke-virtual {v3, v1}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->updateColorByWallpaper(Z)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    invoke-virtual {v3, v1}, Lcom/android/keyguard/LoadingContainer;->updateColorByWallpaper(Z)V

    iget-object v3, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap19(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    return-void

    :cond_0
    const v3, 0x7f020044

    goto :goto_0

    :cond_1
    const v3, 0x7f02000c

    goto :goto_1

    :cond_2
    const v3, 0x7f02006f

    goto :goto_2

    :cond_3
    const v3, 0x7f020014

    goto :goto_3

    :cond_4
    const v3, 0x7f080002

    goto :goto_4
.end method

.method private updateMusicControllerMode()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/KeyguardMusicController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get7(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/provider/MiuiSettings$System;->getSmallWindowMode()Landroid/provider/MiuiSettings$System$SmallWindowType;

    move-result-object v0

    sget-object v1, Landroid/provider/MiuiSettings$System$SmallWindowType;->X7_STYLE:Landroid/provider/MiuiSettings$System$SmallWindowType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/KeyguardMusicController;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/keyguard/KeyguardMusicController;->setDisplayMode(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/KeyguardMusicController;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/keyguard/KeyguardMusicController;->setDisplayMode(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/KeyguardMusicController;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/keyguard/KeyguardMusicController;->setDisplayMode(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get13(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/KeyguardMusicController;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/keyguard/KeyguardMusicController;->setMusicControlMode(I)V

    goto :goto_1
.end method

.method private updateMusicControllerView()V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mDigitalClock:Lcom/android/keyguard/MiuiKeyguardBaseClock;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperDes:Lcom/android/keyguard/wallpaper/WallPaperDesView;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiui/maml/component/MamlView;->setVisibility(I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->updateMusicControllerMode()V

    :goto_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->refreshPreviewButton()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiui/maml/component/MamlView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mDigitalClock:Lcom/android/keyguard/MiuiKeyguardBaseClock;

    invoke-virtual {v0, v2}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mWallPaperDes:Lcom/android/keyguard/wallpaper/WallPaperDesView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/keyguard/wallpaper/WallPaperDesView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateNotificationViewState()V
    .locals 9

    const/4 v8, 0x0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getMaxNotificationCount()I

    move-result v3

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v4, v3, :cond_1

    const/4 v4, -0x2

    :goto_0
    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    invoke-virtual {v4, v1}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->setTranslationY(F)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get26(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x32

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap17(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0054

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    mul-int/2addr v4, v3

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0053

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationAndMusicControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mDigitalClock:Lcom/android/keyguard/MiuiKeyguardBaseClock;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->hasOwnerInfo()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00b5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :goto_2
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationAndMusicControlContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    invoke-virtual {v4}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    invoke-virtual {v4, v8}, Lcom/android/keyguard/MiuiKeyguardRecyclerView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->updateColorByWallpaper()V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationViews:Lcom/android/keyguard/MiuiKeyguardRecyclerView;

    const-string/jumbo v5, "translationY"

    const/4 v6, 0x2

    new-array v6, v6, [F

    fill-array-data v6, :array_0

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v4, 0x190

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    sget-object v4, Lcom/android/keyguard/Ease$Cubic;->easeInOut:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00b6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_2

    :cond_4
    iput v8, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_2

    nop

    :array_0
    .array-data 4
        0x42c80000    # 100.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public computeScroll()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get37(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/widget/Scroller;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set4(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get37(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/widget/Scroller;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    int-to-float v2, v0

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap4(Lcom/android/keyguard/MiuiDefaultLockScreen;F)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->postInvalidateOnAnimation()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set4(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get36(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Runnable;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get36(Lcom/android/keyguard/MiuiDefaultLockScreen;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1, v3}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set16(Lcom/android/keyguard/MiuiDefaultLockScreen;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mNotificationAndMusicControlContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get23(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/maml/component/MamlView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public refreshPreviewButton()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get5(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get40(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get47(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/keyguard/LoadingContainer;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->mShowPreviewButton:Lcom/android/keyguard/LoadingContainer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/keyguard/LoadingContainer;->setVisibility(I)V

    goto :goto_0
.end method
