.class Lcom/android/keyguard/MiuiGxzwIconView$2;
.super Landroid/view/DisplayEventReceiver;
.source "MiuiGxzwIconView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiGxzwIconView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mVsyncCount:I

.field final synthetic this$0:Lcom/android/keyguard/MiuiGxzwIconView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiGxzwIconView;Landroid/os/Looper;)V
    .locals 1

    iput-object p1, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->this$0:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-direct {p0, p2}, Landroid/view/DisplayEventReceiver;-><init>(Landroid/os/Looper;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->mVsyncCount:I

    return-void
.end method


# virtual methods
.method public onVsync(JII)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->this$0:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->-get4(Lcom/android/keyguard/MiuiGxzwIconView;)Z

    move-result v0

    if-nez v0, :cond_0

    iput v2, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->mVsyncCount:I

    return-void

    :cond_0
    iget v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->mVsyncCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->mVsyncCount:I

    iget v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->mVsyncCount:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    iput v2, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->mVsyncCount:I

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->this$0:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->-get3(Lcom/android/keyguard/MiuiGxzwIconView;)I

    move-result v0

    const/16 v1, 0x96

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->this$0:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-static {v0, v2}, Lcom/android/keyguard/MiuiGxzwIconView;->-set0(Lcom/android/keyguard/MiuiGxzwIconView;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->this$0:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->-wrap0(Lcom/android/keyguard/MiuiGxzwIconView;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwIconView$2;->this$0:Lcom/android/keyguard/MiuiGxzwIconView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/keyguard/MiuiGxzwIconView;->-set0(Lcom/android/keyguard/MiuiGxzwIconView;Z)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiGxzwIconView$2;->scheduleVsync()V

    goto :goto_0
.end method
