.class Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen$11;->onChange(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

.field final synthetic val$list:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen$11;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iput-object p2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->val$list:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private wakeUpWithoutProximityCheck()Z
    .locals 5

    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "sensor"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get7(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v2

    if-nez v2, :cond_0

    xor-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_1

    :cond_0
    const-string/jumbo v2, "miui_keyguard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mInSmartCoverSmallWindowMode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get7(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ";proximitySensorAvailable="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v2, v2, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v2}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get7(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v2

    if-nez v2, :cond_3

    xor-int/lit8 v2, v0, 0x1

    :goto_1
    return v2

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->val$list:Ljava/util/List;

    invoke-static {v4, v6}, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->-wrap0(Lcom/android/keyguard/MiuiDefaultLockScreen$11;Ljava/util/List;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get6(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "miui_keyguard"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "mHidden="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v6, v6, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v6}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get6(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v6, "power"

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    const-string/jumbo v4, "sys.keyguard.screen_off_by_lid"

    const-string/jumbo v6, "false"

    invoke-static {v4, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->val$list:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;

    move-result-object v6

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->val$list:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v4, v4, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->pkgName:Ljava/lang/String;

    invoke-static {v6, v4}, Lmiui/provider/ExtraTelephony;->checkKeyguardForQuiet(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    :goto_0
    const-string/jumbo v6, "miui_keyguard"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "mWakeupForNotification="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v7, v7, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v7}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get45(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v7, ";list.size="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->val$list:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";isScreenOn="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";quietModeEnabled="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";isPsensorDisabled="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v5, v5, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get10(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ";isNonUI="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isNonUI()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get45(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->val$list:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    xor-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_1

    xor-int/lit8 v4, v3, 0x1

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->wakeUpWithoutProximityCheck()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap21(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_3
    move v4, v5

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get29(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/util/ProximitySensorWrapper;

    move-result-object v4

    if-nez v4, :cond_6

    xor-int/lit8 v4, v1, 0x1

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get10(Lcom/android/keyguard/MiuiDefaultLockScreen;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isNonUI()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-wrap21(Lcom/android/keyguard/MiuiDefaultLockScreen;)V

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    new-instance v5, Lmiui/util/ProximitySensorWrapper;

    iget-object v6, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v6, v6, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v6}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get4(Lcom/android/keyguard/MiuiDefaultLockScreen;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lmiui/util/ProximitySensorWrapper;-><init>(Landroid/content/Context;)V

    invoke-static {v4, v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set14(Lcom/android/keyguard/MiuiDefaultLockScreen;Lmiui/util/ProximitySensorWrapper;)Lmiui/util/ProximitySensorWrapper;

    iget-object v4, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v4, v4, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v4}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get29(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/util/ProximitySensorWrapper;

    move-result-object v4

    iget-object v5, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$11$1;->this$1:Lcom/android/keyguard/MiuiDefaultLockScreen$11;

    iget-object v5, v5, Lcom/android/keyguard/MiuiDefaultLockScreen$11;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v5}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get39(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Lmiui/util/ProximitySensorWrapper;->registerListener(Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;)V

    goto :goto_2

    :cond_6
    const-string/jumbo v4, "miui_keyguard"

    const-string/jumbo v5, "not wakeup for notification"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
