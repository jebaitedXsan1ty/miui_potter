.class Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;
.super Landroid/hardware/TriggerEventListener;
.source "MiuiKeyguardViewMediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiKeyguardViewMediator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TriggerListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-direct {p0}, Landroid/hardware/TriggerEventListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTrigger(Landroid/hardware/TriggerEvent;)V
    .locals 2

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SCREENON_BY_PICK_UP:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;)V

    const-string/jumbo v0, "keyguard_screenon_pick_up"

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardViewMediator$TriggerListener;->this$0:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-static {v1}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->-get2(Lcom/android/keyguard/MiuiKeyguardViewMediator;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->wakeUp(Ljava/lang/String;Landroid/content/Context;)V

    return-void
.end method
