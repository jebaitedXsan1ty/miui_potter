.class public Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;
.super Landroid/widget/RelativeLayout;
.source "MiuiKeyguardChargingContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$1;
    }
.end annotation


# instance fields
.field private mBatteryLevel:I

.field private mBgView:Landroid/view/View;

.field public mChargeHandler:Landroid/os/Handler;

.field private mChargingAnimationInDeclining:Z

.field private mChargingCicleView:Landroid/view/View;

.field private mChargingHintView:Landroid/widget/TextView;

.field private mChargingInfoBackArrow:Landroid/widget/ImageView;

.field private mChargingListAndBackArrow:Landroid/view/View;

.field private mChargingListTopMargin:I

.field private mChargingTip:Landroid/widget/TextView;

.field private mChargingTips:[Ljava/lang/String;

.field private mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

.field private mChargingViewBottomMarginDown:I

.field private mChargingViewBottomMarginUp:I

.field private mChargingViewHeight:I

.field private mChargingViewHeightAfterScale:I

.field private mChargingViewTopAfterScale:I

.field private mCharingAnimator:Landroid/animation/ValueAnimator;

.field private mCharingFirstListHeight:I

.field private mCharingSecondListHeight:I

.field private mCharingThirdListHeight:I

.field private mContext:Landroid/content/Context;

.field private mDrainedPowerPercent:Landroid/widget/TextView;

.field private mHeight:I

.field private mInitY:F

.field mIsTempHigh:Z

.field private mLastChargeTime:Landroid/widget/TextView;

.field private mNavigationBarHeight:I

.field private mNeedRepositionDevice:Z

.field private mScreenHeight:I

.field private mUsedTime:Landroid/widget/TextView;

.field private mWidth:I


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mBatteryLevel:I

    return v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mBgView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get10(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mHeight:I

    return v0
.end method

.method static synthetic -get11(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mLastChargeTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get12(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mNeedRepositionDevice:Z

    return v0
.end method

.method static synthetic -get13(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mUsedTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get14(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mWidth:I

    return v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingCicleView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingListAndBackArrow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTip:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTips:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Lcom/android/keyguard/charge/MiuiKeyguardChargingView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    return-object v0
.end method

.method static synthetic -get8(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get9(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mDrainedPowerPercent:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingAnimationInDeclining:Z

    return p1
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;J)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getLastChargeFormat(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;J)J
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getHours(J)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic -wrap2(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;J)J
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getMins(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingAnimationInDeclining:Z

    new-instance v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$1;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargeHandler:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingAnimationInDeclining:Z

    new-instance v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$1;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargeHandler:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingAnimationInDeclining:Z

    new-instance v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$1;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargeHandler:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->init(Landroid/content/Context;)V

    return-void
.end method

.method private getHours(J)J
    .locals 3

    const-wide/32 v0, 0x36ee80

    div-long v0, p1, v0

    return-wide v0
.end method

.method private getLastChargeFormat(J)Ljava/lang/String;
    .locals 13

    const/16 v11, 0xb

    const/4 v10, 0x5

    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v4, v9, v6}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v4, v7, v6}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1, v10}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v4, v10, v6}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v4, v11, v8}, Ljava/util/Calendar;->set(II)V

    const/16 v6, 0xc

    invoke-virtual {v4, v6, v8}, Ljava/util/Calendar;->set(II)V

    const/16 v6, 0xd

    invoke-virtual {v4, v6, v8}, Ljava/util/Calendar;->set(II)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v5, v9, v6}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v5, v7, v6}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1, v10}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v10, v6}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v5, v11, v8}, Ljava/util/Calendar;->set(II)V

    const/16 v6, 0xc

    invoke-virtual {v5, v6, v8}, Ljava/util/Calendar;->set(II)V

    const/16 v6, 0xd

    invoke-virtual {v5, v6, v8}, Ljava/util/Calendar;->set(II)V

    iget-object v6, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    const/4 v7, -0x2

    invoke-static {v6, v7}, Lcom/android/keyguard/KeyguardCompatibilityHelperForL;->is24HourFormat(Landroid/content/Context;I)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v2, 0x20

    :goto_0
    or-int/lit8 v6, v2, 0xc

    invoke-static {p1, p2, v6}, Lmiui/date/DateUtils;->formatDateTime(JI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    cmp-long v6, p1, v6

    if-lez v6, :cond_1

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v0, v7, v8

    const v8, 0x7f0b0050

    invoke-virtual {v6, v8, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    return-object v6

    :cond_0
    const/16 v2, 0x10

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    cmp-long v6, p1, v6

    if-lez v6, :cond_2

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v0, v7, v8

    const v8, 0x7f0b0051

    invoke-virtual {v6, v8, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    return-object v6

    :cond_2
    new-instance v3, Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0052

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private getMins(J)J
    .locals 5

    const-wide/32 v0, 0x36ee80

    rem-long v0, p1, v0

    const-wide/32 v2, 0xea60

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->refreshScreenSize()V

    return-void
.end method

.method private initChargingInfoDes(IIII)V
    .locals 6

    invoke-virtual {p0, p1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/view/View;->setBackgroundResource(I)V

    const v3, 0x7f0d0036

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3, p2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    const v3, 0x7f0d0037

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f0d0038

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-object v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mLastChargeTime:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_1
    iput-object v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mUsedTime:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_2
    iput-object v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mDrainedPowerPercent:Landroid/widget/TextView;

    goto :goto_0

    :pswitch_3
    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTip:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTip:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c008b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v3, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTip:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08002d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0d003e
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private refreshScreenSize()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    const-string/jumbo v2, "is_pad"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mScreenHeight:I

    :goto_0
    return-void

    :cond_0
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mScreenHeight:I

    goto :goto_0

    :cond_1
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mScreenHeight:I

    goto :goto_0
.end method


# virtual methods
.method public getChargingHintText(ZI)Ljava/lang/String;
    .locals 17

    if-nez p1, :cond_0

    const/4 v12, 0x0

    return-object v12

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    const-string/jumbo v12, "content://com.miui.powercenter.provider"

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string/jumbo v13, "getBatteryInfo"

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v12, v9, v13, v14, v15}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_8

    const/4 v8, 0x0

    const/16 v12, 0x64

    move/from16 v0, p2

    if-ne v0, v12, :cond_2

    const-string/jumbo v12, "battery_endurance_time"

    :goto_1
    invoke-virtual {v2, v12}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getHours(J)J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getMins(J)J

    move-result-wide v6

    const-wide/16 v12, 0x0

    cmp-long v12, v4, v12

    if-lez v12, :cond_4

    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-lez v12, :cond_4

    const/16 v12, 0x64

    move/from16 v0, p2

    if-ne v0, v12, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    long-to-int v13, v4

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    const/16 v16, 0x0

    aput-object v15, v14, v16

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    const/16 v16, 0x1

    aput-object v15, v14, v16

    const v15, 0x7f0e000a

    invoke-virtual {v12, v15, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    :cond_1
    :goto_2
    return-object v8

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_2
    const-string/jumbo v12, "left_charge_time"

    goto :goto_1

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    long-to-int v13, v4

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    const/16 v16, 0x0

    aput-object v15, v14, v16

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    const/16 v16, 0x1

    aput-object v15, v14, v16

    const v15, 0x7f0e0007

    invoke-virtual {v12, v15, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :cond_4
    const-wide/16 v12, 0x0

    cmp-long v12, v4, v12

    if-lez v12, :cond_6

    const/16 v12, 0x64

    move/from16 v0, p2

    if-ne v0, v12, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    long-to-int v13, v4

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    const/16 v16, 0x0

    aput-object v15, v14, v16

    const v15, 0x7f0e0009

    invoke-virtual {v12, v15, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    long-to-int v13, v4

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    const/16 v16, 0x0

    aput-object v15, v14, v16

    const v15, 0x7f0e0006

    invoke-virtual {v12, v15, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_2

    :cond_6
    const-wide/16 v12, 0x0

    cmp-long v12, v6, v12

    if-lez v12, :cond_1

    const/16 v12, 0x64

    move/from16 v0, p2

    if-ne v0, v12, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    long-to-int v13, v6

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    const/16 v16, 0x0

    aput-object v15, v14, v16

    const v15, 0x7f0e0008

    invoke-virtual {v12, v15, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_2

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    long-to-int v13, v6

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    const/16 v16, 0x0

    aput-object v15, v14, v16

    const v15, 0x7f0e0005

    invoke-virtual {v12, v15, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_2

    :cond_8
    const/4 v12, 0x0

    return-object v12
.end method

.method public getScreenHeight()I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mScreenHeight:I

    return v0
.end method

.method public isChargingAnimationInDeclining()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingAnimationInDeclining:Z

    return v0
.end method

.method public isFullScreen()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onCharingViewClick()V
    .locals 2

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mWidth:I

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mHeight:I

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->setChargingInfo()V

    new-instance v1, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$2;

    invoke-direct {v1, p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$2;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    invoke-virtual {p0, v1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->refreshScreenSize()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 6

    const v5, 0x7f0b0056

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f0d0019

    invoke-virtual {p0, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v0, p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setChargingContainer(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    iget-boolean v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mNeedRepositionDevice:Z

    invoke-virtual {v0, v1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setNeedRepositionDevice(Z)V

    const v0, 0x7f0d003c

    invoke-virtual {p0, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingListAndBackArrow:Landroid/view/View;

    const v0, 0x7f0d0042

    invoke-virtual {p0, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingInfoBackArrow:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewHeightAfterScale:I

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewTopAfterScale:I

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewBottomMarginUp:I

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingListTopMargin:I

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mCharingFirstListHeight:I

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0085

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mCharingSecondListHeight:I

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mCharingThirdListHeight:I

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10500f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mNavigationBarHeight:I

    :goto_0
    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewBottomMarginDown:I

    const v0, 0x7f0d0039

    invoke-virtual {p0, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mBgView:Landroid/view/View;

    const v0, 0x7f0d003a

    invoke-virtual {p0, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingCicleView:Landroid/view/View;

    const v0, 0x7f0d003b

    invoke-virtual {p0, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    const v0, 0x7f0d003e

    const v1, 0x7f020029

    const v2, 0x7f0b0053

    const v3, 0x7f02002b

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->initChargingInfoDes(IIII)V

    const v0, 0x7f0d003f

    const v1, 0x7f02002f

    const v2, 0x7f0b0054

    const v3, 0x7f02002c

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->initChargingInfoDes(IIII)V

    const v0, 0x7f0d0040

    const v1, 0x7f020027

    const v2, 0x7f0b0055

    const v3, 0x7f02002d

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->initChargingInfoDes(IIII)V

    const v0, 0x7f0d0041

    const v1, 0x7f02002e

    const v2, 0x7f02002a

    invoke-direct {p0, v0, v1, v5, v2}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->initChargingInfoDes(IIII)V

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0057

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0058

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0059

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    const v2, 0x7f0b005a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mContext:Landroid/content/Context;

    const v2, 0x7f0b005b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTips:[Ljava/lang/String;

    return-void

    :cond_0
    iput v4, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mNavigationBarHeight:I

    goto/16 :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mParent:Landroid/view/ViewParent;

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    return v2

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setChargingInfo()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$10;

    invoke-direct {v1, p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$10;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public setChargingInfo(Ljava/lang/String;II)V
    .locals 6

    const/high16 v3, 0x41200000    # 10.0f

    iget-object v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    int-to-float v2, p2

    div-float/2addr v2, v3

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    div-double v0, v2, v4

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-double v2, v2

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mIsTempHigh:Z

    iget-boolean v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mNeedRepositionDevice:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTip:Landroid/widget/TextView;

    const v3, 0x7f0b0009

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_1
    iput p3, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mBatteryLevel:I

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mIsTempHigh:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTip:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTips:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingTips:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setDarkMode(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    iget-object v1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setChargingHint(Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setDarkMode(Z)V

    return-void
.end method

.method public setNeedRepositionDevice(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mNeedRepositionDevice:Z

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v0, p1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setNeedRepositionDevice(Z)V

    :cond_0
    return-void
.end method

.method public startDownAnim()V
    .locals 15

    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingAnimationInDeclining:Z

    iget-object v10, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    const-string/jumbo v11, "Y"

    const/4 v12, 0x2

    new-array v12, v12, [F

    iget-object v13, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v13}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getY()F

    move-result v13

    const/4 v14, 0x0

    aput v13, v12, v14

    iget v13, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mInitY:F

    const/4 v14, 0x1

    aput v13, v12, v14

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    iget-object v10, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    const-string/jumbo v11, "Y"

    const/4 v12, 0x2

    new-array v12, v12, [F

    iget-object v13, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getY()F

    move-result v13

    const/4 v14, 0x0

    aput v13, v12, v14

    iget v13, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mInitY:F

    iget v14, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewHeight:I

    int-to-float v14, v14

    add-float/2addr v13, v14

    const/4 v14, 0x1

    aput v13, v12, v14

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    iget-object v10, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    sget-object v11, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v12, 0x2

    new-array v12, v12, [F

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    aput v13, v12, v14

    const/4 v13, 0x0

    const/4 v14, 0x1

    aput v13, v12, v14

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v10, 0x64

    invoke-virtual {v2, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v10, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingListAndBackArrow:Landroid/view/View;

    const-string/jumbo v11, "Y"

    const/4 v12, 0x2

    new-array v12, v12, [F

    iget-object v13, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingListAndBackArrow:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getY()F

    move-result v13

    const/4 v14, 0x0

    aput v13, v12, v14

    iget v13, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mScreenHeight:I

    int-to-float v13, v13

    const/4 v14, 0x1

    aput v13, v12, v14

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    iget-object v10, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingListAndBackArrow:Landroid/view/View;

    sget-object v11, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v12, 0x2

    new-array v12, v12, [F

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    aput v13, v12, v14

    const/4 v13, 0x0

    const/4 v14, 0x1

    aput v13, v12, v14

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    iget-object v10, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mBgView:Landroid/view/View;

    sget-object v11, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v12, 0x2

    new-array v12, v12, [F

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    aput v13, v12, v14

    const/4 v13, 0x0

    const/4 v14, 0x1

    aput v13, v12, v14

    invoke-static {v10, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v10, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;

    invoke-direct {v10}, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;-><init>()V

    invoke-virtual {v1, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v10, 0x12c

    invoke-virtual {v1, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v10, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$6;

    invoke-direct {v10, p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$6;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    invoke-virtual {v1, v10}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v10, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v10}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getHeight()I

    move-result v7

    iget-object v10, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v10}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getWidth()I

    move-result v8

    const/4 v10, 0x2

    new-array v10, v10, [I

    iget-object v11, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v11}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getHeight()I

    move-result v11

    const/4 v12, 0x0

    aput v11, v10, v12

    iget v11, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewHeight:I

    const/4 v12, 0x1

    aput v11, v10, v12

    invoke-static {v10}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v9

    const-wide/16 v10, 0x12c

    invoke-virtual {v9, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v10, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;

    invoke-direct {v10}, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;-><init>()V

    invoke-virtual {v9, v10}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v10, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$7;

    invoke-direct {v10, p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$7;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    invoke-virtual {v9, v10}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v9}, Landroid/animation/ValueAnimator;->start()V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    const-wide/16 v10, 0x12c

    invoke-virtual {v0, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v10, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;

    invoke-direct {v10}, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;-><init>()V

    invoke-virtual {v0, v10}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const/4 v10, 0x5

    new-array v10, v10, [Landroid/animation/Animator;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    const/4 v11, 0x1

    aput-object v4, v10, v11

    const/4 v11, 0x2

    aput-object v6, v10, v11

    const/4 v11, 0x3

    aput-object v3, v10, v11

    const/4 v11, 0x4

    aput-object v2, v10, v11

    invoke-virtual {v0, v10}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    new-instance v10, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$8;

    invoke-direct {v10, p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$8;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    invoke-virtual {v0, v10}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public startEnterAnim(Z)V
    .locals 13

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingCicleView:Landroid/view/View;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setVisibility(I)V

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9, p1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setChargingLevelForAnima(IZ)V

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingCicleView:Landroid/view/View;

    const-string/jumbo v9, "Y"

    const/4 v10, 0x2

    new-array v10, v10, [F

    invoke-virtual {p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getHeight()I

    move-result v11

    int-to-float v11, v11

    const/4 v12, 0x0

    aput v11, v10, v12

    iget-object v11, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v11}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getY()F

    move-result v11

    iget-object v12, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v12}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getHeight()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    add-float/2addr v11, v12

    iget-object v12, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingCicleView:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    sub-float/2addr v11, v12

    const/4 v12, 0x1

    aput v11, v10, v12

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    const-wide/16 v8, 0xc8

    invoke-virtual {v7, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingCicleView:Landroid/view/View;

    sget-object v9, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    aput v11, v10, v12

    const/high16 v11, 0x40800000    # 4.0f

    const/4 v12, 0x1

    aput v11, v10, v12

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    const-wide/16 v8, 0x12c

    invoke-virtual {v5, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingCicleView:Landroid/view/View;

    sget-object v9, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    aput v11, v10, v12

    const/high16 v11, 0x40800000    # 4.0f

    const/4 v12, 0x1

    aput v11, v10, v12

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingCicleView:Landroid/view/View;

    sget-object v9, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    aput v11, v10, v12

    const/4 v11, 0x0

    const/4 v12, 0x1

    aput v11, v10, v12

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    const-wide/16 v8, 0xc8

    invoke-virtual {v4, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setAlpha(F)V

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setScaleX(F)V

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->setScaleY(F)V

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    sget-object v9, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_0

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v8, 0x12c

    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    sget-object v9, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_1

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    const-wide/16 v8, 0x12c

    invoke-virtual {v3, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v8, p0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    sget-object v9, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_2

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v8, 0x12c

    invoke-virtual {v3, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    new-instance v8, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;

    invoke-direct {v8}, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;-><init>()V

    invoke-virtual {v0, v8}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const/4 v8, 0x5

    new-array v8, v8, [Landroid/animation/Animator;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    aput-object v6, v8, v9

    const/4 v9, 0x2

    aput-object v2, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    const/4 v9, 0x4

    aput-object v1, v8, v9

    invoke-virtual {v0, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v8

    const-wide/16 v10, 0xc8

    invoke-virtual {v8, v10, v11}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    new-instance v8, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$9;

    invoke-direct {v8, p0, p1}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$9;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;Z)V

    invoke-virtual {v0, v8}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public startUpAnim()V
    .locals 19

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewTopAfterScale:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewHeightAfterScale:I

    add-int/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewBottomMarginUp:I

    add-int/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    invoke-virtual {v15}, Landroid/widget/TextView;->getHeight()I

    move-result v15

    add-int/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingListTopMargin:I

    add-int v3, v14, v15

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string/jumbo v15, "window"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/WindowManager;

    new-instance v11, Landroid/graphics/Point;

    invoke-direct {v11}, Landroid/graphics/Point;-><init>()V

    invoke-interface {v13}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v14

    invoke-virtual {v14, v11}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v14, v11, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mNavigationBarHeight:I

    add-int/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v15}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getHeight()I

    move-result v15

    sub-int/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewBottomMarginDown:I

    sub-int/2addr v14, v15

    int-to-float v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mInitY:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v14}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getHeight()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewHeight:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mBgView:Landroid/view/View;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    new-instance v14, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$3;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$3;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    sget-object v15, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Void;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$3;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v15}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getY()F

    move-result v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewHeight:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    add-float v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewBottomMarginUp:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    add-float v15, v15, v16

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setY(F)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mBgView:Landroid/view/View;

    sget-object v15, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    aput v17, v16, v18

    const/high16 v17, 0x3f800000    # 1.0f

    const/16 v18, 0x1

    aput v17, v16, v18

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    new-instance v14, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;

    invoke-direct {v14}, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;-><init>()V

    invoke-virtual {v4, v14}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v14, 0xc8

    invoke-virtual {v4, v14, v15}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    const-string/jumbo v15, "Y"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getY()F

    move-result v17

    const/16 v18, 0x0

    aput v17, v16, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewTopAfterScale:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/16 v18, 0x1

    aput v17, v16, v18

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    const-string/jumbo v15, "Y"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingHintView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getY()F

    move-result v17

    const/16 v18, 0x0

    aput v17, v16, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewTopAfterScale:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewHeightAfterScale:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewBottomMarginUp:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/16 v18, 0x1

    aput v17, v16, v18

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingListAndBackArrow:Landroid/view/View;

    const-string/jumbo v15, "Y"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mScreenHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/16 v18, 0x0

    aput v17, v16, v18

    int-to-float v0, v3

    move/from16 v17, v0

    const/16 v18, 0x1

    aput v17, v16, v18

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingListAndBackArrow:Landroid/view/View;

    sget-object v15, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    aput v17, v16, v18

    const/high16 v17, 0x3f800000    # 1.0f

    const/16 v18, 0x1

    aput v17, v16, v18

    invoke-static/range {v14 .. v16}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v14}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getHeight()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v14}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getWidth()I

    move-result v10

    const/4 v14, 0x2

    new-array v14, v14, [I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingView:Lcom/android/keyguard/charge/MiuiKeyguardChargingView;

    invoke-virtual {v15}, Lcom/android/keyguard/charge/MiuiKeyguardChargingView;->getHeight()I

    move-result v15

    const/16 v16, 0x0

    aput v15, v14, v16

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;->mChargingViewHeightAfterScale:I

    const/16 v16, 0x1

    aput v15, v14, v16

    invoke-static {v14}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v12

    const-wide/16 v14, 0x12c

    invoke-virtual {v12, v14, v15}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v14, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;

    invoke-direct {v14}, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;-><init>()V

    invoke-virtual {v12, v14}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v14, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$4;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$4;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    invoke-virtual {v12, v14}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v12}, Landroid/animation/ValueAnimator;->start()V

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    const-wide/16 v14, 0x12c

    invoke-virtual {v2, v14, v15}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v14, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;

    invoke-direct {v14}, Lmiui/maml/animation/interpolater/CubicEaseOutInterpolater;-><init>()V

    invoke-virtual {v2, v14}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const/4 v14, 0x4

    new-array v14, v14, [Landroid/animation/Animator;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    const/4 v15, 0x1

    aput-object v6, v14, v15

    const/4 v15, 0x2

    aput-object v8, v14, v15

    const/4 v15, 0x3

    aput-object v5, v14, v15

    invoke-virtual {v2, v14}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    new-instance v14, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$5;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer$5;-><init>(Lcom/android/keyguard/charge/MiuiKeyguardChargingContainer;)V

    invoke-virtual {v2, v14}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method
