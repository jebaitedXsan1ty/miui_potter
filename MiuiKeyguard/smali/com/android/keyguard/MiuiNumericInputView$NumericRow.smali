.class Lcom/android/keyguard/MiuiNumericInputView$NumericRow;
.super Landroid/widget/LinearLayout;
.source "MiuiNumericInputView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiNumericInputView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NumericRow"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiNumericInputView;


# direct methods
.method static synthetic -wrap0(Lcom/android/keyguard/MiuiNumericInputView$NumericRow;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->addCell(III)V

    return-void
.end method

.method public constructor <init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;)V
    .locals 3

    iput-object p1, p0, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->this$0:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->setLayoutDirection(I)V

    const/16 v1, 0x30

    invoke-virtual {p0, v1}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->setGravity(I)V

    return-void
.end method

.method private addCell(III)V
    .locals 7

    new-instance v0, Lcom/android/keyguard/MiuiNumericInputView$Cell;

    iget-object v5, p0, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->this$0:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, v5, v6, p1}, Lcom/android/keyguard/MiuiNumericInputView$Cell;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;I)V

    new-instance v2, Lcom/android/keyguard/MiuiNumericInputView$Cell;

    iget-object v5, p0, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->this$0:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v2, v5, v6, p2}, Lcom/android/keyguard/MiuiNumericInputView$Cell;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;I)V

    new-instance v3, Lcom/android/keyguard/MiuiNumericInputView$Cell;

    iget-object v5, p0, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->this$0:Lcom/android/keyguard/MiuiNumericInputView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v5, v6, p3}, Lcom/android/keyguard/MiuiNumericInputView$Cell;-><init>(Lcom/android/keyguard/MiuiNumericInputView;Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c002f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v1, v5, v4, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v1}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v1}, Lcom/android/keyguard/MiuiNumericInputView$Cell;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->addView(Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->addView(Landroid/view/View;)V

    invoke-virtual {p0, v3}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiNumericInputView$NumericRow;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
