.class Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MiuiDefaultLockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NotificationViewHolder"
.end annotation


# instance fields
.field private mIconView:Landroid/widget/ImageView;

.field private mInfoView:Landroid/widget/TextView;

.field private mMsgView:Landroid/widget/TextView;

.field private mProfileView:Landroid/widget/ImageView;

.field private mSelected:Z

.field private mTimeView:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mIconView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mInfoView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mMsgView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mProfileView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mSelected:Z

    return v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mTimeView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mTitleView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -set0(Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mSelected:Z

    return p1
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mSelected:Z

    const v0, 0x7f0d002f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mIconView:Landroid/widget/ImageView;

    const v0, 0x7f0d0034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mTitleView:Landroid/widget/TextView;

    const v0, 0x7f0d0035

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mMsgView:Landroid/widget/TextView;

    const v0, 0x7f0d0031

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mTimeView:Landroid/widget/TextView;

    const v0, 0x7f0d0032

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mProfileView:Landroid/widget/ImageView;

    const v0, 0x7f0d0033

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$NotificationViewHolder;->mInfoView:Landroid/widget/TextView;

    return-void
.end method
