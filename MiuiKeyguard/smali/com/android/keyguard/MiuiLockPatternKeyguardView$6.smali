.class Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;
.super Ljava/lang/Object;
.source "MiuiLockPatternKeyguardView.java"

# interfaces
.implements Lcom/android/keyguard/faceunlock/FaceUnlockManager$FaceUnlockCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keyguard/MiuiLockPatternKeyguardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->goToUnlockScreen()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get5(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get5(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onFaceUnlockFailed()V

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onFaceUnlockFailed()V

    :cond_2
    return-void
.end method

.method public onHelp(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get5(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get5(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardScreen;->onFaceUnlockHelp(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0, p1}, Lcom/android/keyguard/MiuiKeyguardScreen;->onFaceUnlockHelp(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get5(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get5(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onFaceUnlockStart()V

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    invoke-static {v0}, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->-get8(Lcom/android/keyguard/MiuiLockPatternKeyguardView;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/keyguard/MiuiKeyguardScreen;

    invoke-interface {v0}, Lcom/android/keyguard/MiuiKeyguardScreen;->onFaceUnlockStart()V

    :cond_1
    return-void
.end method

.method public onSuccess()V
    .locals 2

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v1, "dipper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardViewMediator:Lcom/android/keyguard/MiuiKeyguardViewMediator;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardViewMediator;->isScreenOn()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isScreenTurnOnDelayed()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const-string/jumbo v0, "face_unlock"

    const-string/jumbo v1, "face unlock returned because screen turn off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiLockPatternKeyguardView$6;->this$0:Lcom/android/keyguard/MiuiLockPatternKeyguardView;

    iget-object v0, v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/keyguard/MiuiKeyguardScreenCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/keyguard/MiuiKeyguardScreenCallback;->keyguardDone(Z)V

    return-void
.end method
