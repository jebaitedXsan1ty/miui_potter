.class Lcom/android/keyguard/MiuiDefaultLockScreen$21;
.super Ljava/lang/Object;
.source "MiuiDefaultLockScreen.java"

# interfaces
.implements Lcom/android/keyguard/PhoneSignalController$PhoneSignalChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiDefaultLockScreen;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiDefaultLockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$21;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSignalChange(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$21;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0, p1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-set18(Lcom/android/keyguard/MiuiDefaultLockScreen;Z)Z

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$21;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$21;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$21;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen;->-get20(Lcom/android/keyguard/MiuiDefaultLockScreen;)Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;

    move-result-object v0

    invoke-static {v0}, Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;->-get17(Lcom/android/keyguard/MiuiDefaultLockScreen$MainLockView;)Lcom/android/keyguard/UnlockHintSwitcher;

    move-result-object v0

    iget-object v1, p0, Lcom/android/keyguard/MiuiDefaultLockScreen$21;->this$0:Lcom/android/keyguard/MiuiDefaultLockScreen;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiDefaultLockScreen;->getDefaultUnlockHintText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/keyguard/UnlockHintSwitcher;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
