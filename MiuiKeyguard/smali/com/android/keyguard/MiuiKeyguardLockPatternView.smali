.class public Lcom/android/keyguard/MiuiKeyguardLockPatternView;
.super Landroid/widget/LinearLayout;
.source "MiuiKeyguardLockPatternView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;
    }
.end annotation


# static fields
.field private static final FADING_SPEED:I = 0x320

.field private static final MILLIS_PER_CIRCLE_ANIMATING:I = 0x2bc

.field private static final PROFILE_DRAWING:Z


# instance fields
.field private mAnimatingPeriodStart:J

.field private mBitmapBtnError:Landroid/graphics/Bitmap;

.field private mBitmapBtnTouched:Landroid/graphics/Bitmap;

.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private final mCircleMatrix:Landroid/graphics/Matrix;

.field private final mCurrentPath:Landroid/graphics/Path;

.field private mDiameterFactor:F

.field private mDrawingProfilingStarted:Z

.field private mEnableHapticFeedback:Z

.field private final mErrorStrokeAlpha:I

.field private final mFadeStrokeAlpha:I

.field private mFadingPathCheckPointAlpha:[I

.field private mHitFactor:F

.field private mInProgressX:F

.field private mInProgressY:F

.field private mInStealthMode:Z

.field private mInputEnabled:Z

.field private final mInvalidate:Landroid/graphics/Rect;

.field private mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

.field private mPaint:Landroid/graphics/Paint;

.field private mPathFadingEndColor:I

.field private mPathFadingPaint:Landroid/graphics/Paint;

.field private mPathPaint:Landroid/graphics/Paint;

.field private mPathPaintColor:I

.field private mPathPaintErrorColor:I

.field private mPattern:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;"
        }
    .end annotation
.end field

.field private mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

.field private mPatternDrawLookup:[[Z

.field private mPatternInProgress:Z

.field private mSquareHeight:F

.field private mSquareWidth:F

.field private final mStrokeAlpha:I

.field private mTouchDownTimestamp:J


# direct methods
.method static synthetic -get0(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapBtnError:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic -get1(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapBtnTouched:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic -get10(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)F
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareWidth:F

    return v0
.end method

.method static synthetic -get2(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapHeight:I

    return v0
.end method

.method static synthetic -get3(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapWidth:I

    return v0
.end method

.method static synthetic -get4(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mCircleMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic -get5(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInStealthMode:Z

    return v0
.end method

.method static synthetic -get6(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic -get7(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Lcom/android/internal/widget/LockPatternView$DisplayMode;
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    return-object v0
.end method

.method static synthetic -get8(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternInProgress:Z

    return v0
.end method

.method static synthetic -get9(Lcom/android/keyguard/MiuiKeyguardLockPatternView;)F
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareHeight:F

    return v0
.end method

.method static synthetic -wrap0(Lcom/android/keyguard/MiuiKeyguardLockPatternView;II)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->resolveMeasured(II)I

    move-result v0

    return v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 11

    const/16 v8, 0x9

    const/high16 v7, -0x40800000    # -1.0f

    const/4 v10, 0x3

    const/4 v5, 0x0

    const/4 v9, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v5, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mDrawingProfilingStarted:Z

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaint:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    filled-new-array {v10, v10}, [I

    move-result-object v6

    invoke-static {v4, v6}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[Z

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDrawLookup:[[Z

    iput v7, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    iput v7, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    sget-object v4, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    iput-boolean v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInputEnabled:Z

    iput-boolean v5, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInStealthMode:Z

    iput-boolean v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mEnableHapticFeedback:Z

    iput-boolean v5, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternInProgress:Z

    const v4, 0x3d4ccccd    # 0.05f

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mDiameterFactor:F

    const/16 v4, 0x20

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mStrokeAlpha:I

    const/16 v4, 0xdd

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadeStrokeAlpha:I

    const/16 v4, 0x80

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mErrorStrokeAlpha:I

    const v4, 0x3f19999a    # 0.6f

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mHitFactor:F

    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mCurrentPath:Landroid/graphics/Path;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInvalidate:Landroid/graphics/Rect;

    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mCircleMatrix:Landroid/graphics/Matrix;

    new-array v4, v8, [I

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    invoke-virtual {p0, v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f080004

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaintColor:I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f080007

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaintErrorColor:I

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f080005

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingEndColor:I

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c003c

    invoke-virtual {v4, v6, v3, v9}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    invoke-virtual {v3}, Landroid/util/TypedValue;->getFloat()F

    move-result v4

    iput v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mDiameterFactor:F

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    const v4, 0x7f020041

    invoke-direct {p0, v4}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapBtnError:Landroid/graphics/Bitmap;

    const v4, 0x7f020042

    invoke-direct {p0, v4}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getBitmapFor(I)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapBtnTouched:Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    new-array v1, v4, [Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapBtnError:Landroid/graphics/Bitmap;

    aput-object v4, v1, v5

    iget-object v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapBtnTouched:Landroid/graphics/Bitmap;

    aput-object v4, v1, v9

    array-length v6, v1

    move v4, v5

    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v0, v1, v4

    iget v7, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapWidth:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapWidth:I

    iget v7, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapHeight:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapHeight:I

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setOrientation(I)V

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v10, :cond_1

    invoke-direct {p0, v2}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->addCellsRow(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v5}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setWillNotDraw(Z)V

    return-void
.end method

.method private addCellToPattern(Lcom/android/internal/widget/LockPatternView$Cell;)V
    .locals 3

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDrawLookup:[[Z

    invoke-virtual {p1}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p1}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v1

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->notifyCellAdded()V

    return-void
.end method

.method private addCellsRow(I)V
    .locals 8

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0020

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    new-instance v4, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v3, 0x0

    :goto_0
    const/4 v5, 0x3

    if-ge v3, v5, :cond_0

    new-instance v1, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, p0, v5, p1, v3}, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;-><init>(Lcom/android/keyguard/MiuiKeyguardLockPatternView;Landroid/content/Context;II)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v4}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->addView(Landroid/view/View;)V

    return-void
.end method

.method private checkForNewHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p2}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getRowHit(F)I

    move-result v1

    if-gez v1, :cond_0

    return-object v3

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getColumnHit(F)I

    move-result v0

    if-gez v0, :cond_1

    return-object v3

    :cond_1
    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDrawLookup:[[Z

    aget-object v2, v2, v1

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_2

    return-object v3

    :cond_2
    invoke-static {v1, v0}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v2

    return-object v2
.end method

.method private clearPatternDrawLookup()V
    .locals 5

    const/4 v4, 0x3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDrawLookup:[[Z

    aget-object v2, v2, v0

    const/4 v3, 0x0

    aput-boolean v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;
    .locals 10

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v9, Landroid/view/animation/AnimationSet;

    invoke-direct {v9, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v9, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mContext:Landroid/content/Context;

    const v3, 0x10c0008

    invoke-virtual {v9, v0, v3}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/content/Context;I)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView$1;

    invoke-direct {v0, p0, p1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView$1;-><init>(Lcom/android/keyguard/MiuiKeyguardLockPatternView;Landroid/view/View;)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-object v9
.end method

.method private detectAndAddHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;
    .locals 13

    const/4 v12, 0x2

    const/4 v10, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->checkForNewHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v0

    if-eqz v0, :cond_7

    const/4 v4, 0x0

    iget-object v7, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v8

    invoke-virtual {v6}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v11

    sub-int v2, v8, v11

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v8

    invoke-virtual {v6}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v11

    sub-int v1, v8, v11

    invoke-virtual {v6}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v5

    invoke-virtual {v6}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-ne v8, v12, :cond_0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-eq v8, v9, :cond_0

    invoke-virtual {v6}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v11

    if-lez v2, :cond_6

    move v8, v9

    :goto_0
    add-int v5, v11, v8

    :cond_0
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-ne v8, v12, :cond_2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-eq v8, v9, :cond_2

    invoke-virtual {v6}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v8

    if-lez v1, :cond_1

    move v10, v9

    :cond_1
    add-int v3, v8, v10

    :cond_2
    invoke-static {v5, v3}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v4

    :cond_3
    if-eqz v4, :cond_4

    iget-object v8, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDrawLookup:[[Z

    invoke-virtual {v4}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v10

    aget-object v8, v8, v10

    invoke-virtual {v4}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v10

    aget-boolean v8, v8, v10

    xor-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_4

    invoke-direct {p0, v4}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->addCellToPattern(Lcom/android/internal/widget/LockPatternView$Cell;)V

    :cond_4
    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->addCellToPattern(Lcom/android/internal/widget/LockPatternView$Cell;)V

    iget-boolean v8, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mEnableHapticFeedback:Z

    if-eqz v8, :cond_5

    const/4 v8, 0x3

    invoke-virtual {p0, v9, v8}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->performHapticFeedback(II)Z

    :cond_5
    return-object v0

    :cond_6
    move v8, v10

    goto :goto_0

    :cond_7
    return-object v8
.end method

.method private drawFadingPath(Landroid/graphics/Canvas;)V
    .locals 34

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v15

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mTouchDownTimestamp:J

    sub-long v22, v18, v8

    const-wide/16 v8, 0x320

    mul-long v8, v8, v22

    const-wide/16 v10, 0x3e8

    div-long v24, v8, v10

    const-wide/16 v30, 0x0

    const/16 v32, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v26

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v27

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareWidth:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mDiameterFactor:F

    mul-float/2addr v8, v9

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/16 v21, 0x0

    :goto_0
    move/from16 v0, v21

    if-ge v0, v15, :cond_1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    const/4 v8, 0x0

    aput v8, v7, v21

    if-lez v21, :cond_0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    add-int/lit8 v8, v21, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/internal/widget/LockPatternView$Cell;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v7

    invoke-virtual/range {v29 .. v29}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v8

    sub-float v17, v7, v8

    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v7

    invoke-virtual/range {v29 .. v29}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v8

    sub-float v20, v7, v8

    mul-float v7, v17, v17

    mul-float v8, v20, v20

    add-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v7, v8

    int-to-long v8, v7

    add-long v30, v30, v8

    :cond_0
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    :cond_1
    move-wide/from16 v0, v30

    long-to-double v8, v0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    sub-float v7, v7, v26

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    sub-float v10, v10, v26

    mul-float/2addr v7, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    sub-float v10, v10, v27

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    sub-float v11, v11, v27

    mul-float/2addr v10, v11

    add-float/2addr v7, v10

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    add-double/2addr v8, v10

    double-to-long v0, v8

    move-wide/from16 v30, v0

    const/16 v21, 0x1

    :goto_1
    move/from16 v0, v21

    if-ge v0, v15, :cond_4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    add-int/lit8 v8, v21, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/android/internal/widget/LockPatternView$Cell;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v7

    invoke-virtual/range {v29 .. v29}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v8

    sub-float v17, v7, v8

    invoke-virtual/range {v16 .. v16}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v7

    invoke-virtual/range {v29 .. v29}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v8

    sub-float v20, v7, v8

    mul-float v7, v17, v17

    mul-float v8, v20, v20

    add-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v0, v8

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-long v8, v0

    cmp-long v7, v24, v8

    if-ltz v7, :cond_2

    move/from16 v0, v28

    int-to-long v8, v0

    sub-long v24, v24, v8

    :goto_2
    move/from16 v0, v28

    int-to-long v8, v0

    sub-long v30, v30, v8

    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    :cond_2
    const-wide/16 v8, 0x0

    cmp-long v7, v24, v8

    if-lez v7, :cond_3

    move-wide/from16 v0, v24

    long-to-float v7, v0

    const/high16 v8, 0x3f800000    # 1.0f

    mul-float/2addr v7, v8

    move/from16 v0, v28

    int-to-float v8, v0

    div-float v32, v7, v8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    move/from16 v0, v28

    int-to-long v8, v0

    sub-long v8, v8, v24

    const-wide/16 v10, 0xdd

    mul-long/2addr v8, v10

    sub-long v10, v30, v24

    div-long/2addr v8, v10

    long-to-int v8, v8

    aput v8, v7, v21

    const-wide/16 v24, 0x0

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    add-int/lit8 v9, v21, -0x1

    aget v8, v8, v9

    int-to-long v8, v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    add-int/lit8 v11, v21, -0x1

    aget v10, v10, v11

    rsub-int v10, v10, 0xdd

    mul-int v10, v10, v28

    int-to-long v10, v10

    div-long v10, v10, v30

    add-long/2addr v8, v10

    long-to-int v8, v8

    aput v8, v7, v21

    goto :goto_2

    :cond_4
    const/16 v21, 0x1

    :goto_3
    move/from16 v0, v21

    if-ge v0, v15, :cond_7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    aget v7, v7, v21

    if-eqz v7, :cond_5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    add-int/lit8 v8, v21, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    add-int/lit8 v8, v21, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v7}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    add-int/lit8 v8, v21, -0x1

    aget v7, v7, v8

    if-nez v7, :cond_6

    new-instance v2, Landroid/graphics/LinearGradient;

    const/4 v7, 0x3

    new-array v7, v7, [I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingEndColor:I

    const v9, 0xffffff

    and-int/2addr v8, v9

    const/4 v9, 0x0

    aput v8, v7, v9

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingEndColor:I

    const v9, 0xffffff

    and-int/2addr v8, v9

    const/4 v9, 0x1

    aput v8, v7, v9

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    aget v8, v8, v21

    shl-int/lit8 v8, v8, 0x18

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingEndColor:I

    const v10, 0xffffff

    and-int/2addr v9, v10

    or-int/2addr v8, v9

    const/4 v9, 0x2

    aput v8, v7, v9

    const/4 v8, 0x3

    new-array v8, v8, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v9, v8, v10

    const/4 v9, 0x1

    aput v32, v8, v9

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x2

    aput v9, v8, v10

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    move v8, v3

    move v9, v4

    move v10, v5

    move v11, v6

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_5
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_3

    :cond_6
    new-instance v2, Landroid/graphics/LinearGradient;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    add-int/lit8 v8, v21, -0x1

    aget v7, v7, v8

    shl-int/lit8 v7, v7, 0x18

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingEndColor:I

    const v9, 0xffffff

    and-int/2addr v8, v9

    or-int/2addr v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    aget v8, v8, v21

    shl-int/lit8 v8, v8, 0x18

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingEndColor:I

    const v10, 0xffffff

    and-int/2addr v9, v10

    or-int/2addr v8, v9

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    goto :goto_4

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    move-object/from16 v33, v0

    new-instance v7, Landroid/graphics/LinearGradient;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mFadingPathCheckPointAlpha:[I

    add-int/lit8 v9, v15, -0x1

    aget v8, v8, v9

    shl-int/lit8 v8, v8, 0x18

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingEndColor:I

    const v12, 0xffffff

    and-int/2addr v9, v12

    or-int v12, v8, v9

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingEndColor:I

    sget-object v14, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move/from16 v8, v26

    move/from16 v9, v27

    invoke-direct/range {v7 .. v14}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathFadingPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    move/from16 v8, v26

    move/from16 v9, v27

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->invalidate()V

    return-void
.end method

.method private getBitmapFor(I)Landroid/graphics/Bitmap;
    .locals 1

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private getCenterXForColumn(I)F
    .locals 3

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaddingLeft:I

    int-to-float v0, v0

    int-to-float v1, p1

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareWidth:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareWidth:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private getCenterYForRow(I)F
    .locals 3

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaddingTop:I

    int-to-float v0, v0

    int-to-float v1, p1

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareHeight:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareHeight:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private getColumnHit(F)I
    .locals 8

    iget v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareWidth:F

    iget v5, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mHitFactor:F

    mul-float v1, v4, v5

    iget v5, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaddingLeft:I

    int-to-float v5, v5

    sub-float v6, v4, v1

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float v3, v5, v6

    const/4 v2, 0x0

    :goto_0
    const/4 v5, 0x3

    if-ge v2, v5, :cond_1

    int-to-float v5, v2

    mul-float/2addr v5, v4

    add-float v0, v3, v5

    cmpl-float v5, p1, v0

    if-ltz v5, :cond_0

    add-float v5, v0, v1

    cmpg-float v5, p1, v5

    if-gtz v5, :cond_0

    return v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, -0x1

    return v5
.end method

.method private getRowHit(F)I
    .locals 8

    iget v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareHeight:F

    iget v5, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mHitFactor:F

    mul-float v0, v4, v5

    iget v5, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaddingTop:I

    int-to-float v5, v5

    sub-float v6, v4, v0

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float v3, v5, v6

    const/4 v2, 0x0

    :goto_0
    const/4 v5, 0x3

    if-ge v2, v5, :cond_1

    int-to-float v5, v2

    mul-float/2addr v5, v4

    add-float v1, v3, v5

    cmpl-float v5, p1, v1

    if-ltz v5, :cond_0

    add-float v5, v1, v0

    cmpg-float v5, p1, v5

    if-gtz v5, :cond_0

    return v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, -0x1

    return v5
.end method

.method private handleActionDown(Landroid/view/MotionEvent;)V
    .locals 6

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->resetPattern()V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->detectAndAddHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternInProgress:Z

    sget-object v3, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    iput-object v3, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->notifyPatternStarted()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->invalidate()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mTouchDownTimestamp:J

    :goto_0
    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    iput v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    return-void

    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternInProgress:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->notifyPatternCleared()V

    goto :goto_0
.end method

.method private handleActionMove(Landroid/view/MotionEvent;)V
    .locals 12

    const/4 v11, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    const/4 v4, 0x0

    :goto_0
    add-int/lit8 v8, v2, 0x1

    if-ge v4, v8, :cond_4

    if-ge v4, v2, :cond_2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v6

    :goto_1
    if-ge v4, v2, :cond_3

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v7

    :goto_2
    invoke-direct {p0, v6, v7}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->detectAndAddHit(FF)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v3

    iget-object v8, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v3, :cond_0

    if-ne v5, v11, :cond_0

    iput-boolean v11, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternInProgress:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->notifyPatternStarted()V

    :cond_0
    iget v8, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    sub-float v8, v6, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v8, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    sub-float v8, v7, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float v8, v0, v1

    iget v9, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareWidth:F

    const v10, 0x3c23d70a    # 0.01f

    mul-float/2addr v9, v10

    cmpl-float v8, v8, v9

    if-lez v8, :cond_1

    iput v6, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    iput v7, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->invalidate()V

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    goto :goto_2

    :cond_4
    return-void
.end method

.method private handleActionUp(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternInProgress:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->notifyPatternDetected()V

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->invalidate()V

    :cond_0
    return-void
.end method

.method private notifyCellAdded()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/android/internal/widget/LockPatternView$OnPatternListener;->onPatternCellAdded(Ljava/util/List;)V

    :cond_0
    const v0, 0x7f0b0025

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->sendAccessEvent(I)V

    return-void
.end method

.method private notifyPatternCleared()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    invoke-interface {v0}, Lcom/android/internal/widget/LockPatternView$OnPatternListener;->onPatternCleared()V

    :cond_0
    const v0, 0x7f0b0024

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->sendAccessEvent(I)V

    return-void
.end method

.method private notifyPatternDetected()V
    .locals 2

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/android/internal/widget/LockPatternView$OnPatternListener;->onPatternDetected(Ljava/util/List;)V

    :cond_0
    const v0, 0x7f0b0026

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->sendAccessEvent(I)V

    return-void
.end method

.method private notifyPatternStarted()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    invoke-interface {v0}, Lcom/android/internal/widget/LockPatternView$OnPatternListener;->onPatternStart()V

    :cond_0
    const v0, 0x7f0b0023

    invoke-direct {p0, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->sendAccessEvent(I)V

    return-void
.end method

.method private resetPattern()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->clearPatternDrawLookup()V

    sget-object v0, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    iput-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->invalidate()V

    return-void
.end method

.method private resolveMeasured(II)I
    .locals 3

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :sswitch_0
    move v0, p2

    goto :goto_0

    :sswitch_1
    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method private sendAccessEvent(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->sendAccessibilityEvent(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public clearPattern()V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->resetPattern()V

    return-void
.end method

.method public disableInput()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInputEnabled:Z

    return-void
.end method

.method public enableInput()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInputEnabled:Z

    return-void
.end method

.method protected getSuggestedMinimumHeight()I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapWidth:I

    mul-int/lit8 v0, v0, 0x3

    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .locals 1

    iget v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mBitmapWidth:I

    mul-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public isInStealthMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInStealthMode:Z

    return v0
.end method

.method public isTactileFeedbackEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mEnableHapticFeedback:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDrawLookup:[[Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v27, v0

    sget-object v28, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Animate:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_2

    add-int/lit8 v27, v7, 0x1

    move/from16 v0, v27

    mul-int/lit16 v0, v0, 0x2bc

    move/from16 v21, v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v28

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mAnimatingPeriodStart:J

    move-wide/from16 v30, v0

    sub-long v28, v28, v30

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    rem-int v25, v27, v21

    move/from16 v0, v25

    div-int/lit16 v0, v0, 0x2bc

    move/from16 v19, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->clearPatternDrawLookup()V

    const/4 v14, 0x0

    :goto_0
    move/from16 v0, v19

    if-ge v14, v0, :cond_0

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v27

    aget-object v27, v10, v27

    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v28

    const/16 v29, 0x1

    aput-boolean v29, v27, v28

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_0
    if-lez v19, :cond_4

    move/from16 v0, v19

    if-ge v0, v7, :cond_3

    const/16 v17, 0x1

    :goto_1
    if-eqz v17, :cond_1

    move/from16 v0, v25

    rem-int/lit16 v0, v0, 0x2bc

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    const/high16 v28, 0x442f0000    # 700.0f

    div-float v23, v27, v28

    add-int/lit8 v27, v19, -0x1

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v8}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v5

    invoke-virtual {v8}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v6

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v27

    sub-float v27, v27, v5

    mul-float v12, v23, v27

    invoke-virtual/range {v18 .. v18}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v27

    sub-float v27, v27, v6

    mul-float v13, v23, v27

    add-float v27, v5, v12

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    add-float v27, v6, v13

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->invalidate()V

    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareWidth:F

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mDiameterFactor:F

    move/from16 v27, v0

    mul-float v24, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mCurrentPath:Landroid/graphics/Path;

    invoke-virtual {v9}, Landroid/graphics/Path;->rewind()V

    const/4 v14, 0x0

    :goto_2
    const/16 v27, 0x3

    move/from16 v0, v27

    if-ge v14, v0, :cond_6

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getChildAt(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    const/4 v15, 0x0

    :goto_3
    const/16 v27, 0x3

    move/from16 v0, v27

    if-ge v15, v0, :cond_5

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;

    aget-object v27, v10, v14

    aget-boolean v27, v27, v15

    move/from16 v0, v27

    invoke-virtual {v4, v0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView$CellView;->setPartOfPattern(Z)V

    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    :cond_3
    const/16 v17, 0x0

    goto/16 :goto_1

    :cond_4
    const/16 v17, 0x0

    goto/16 :goto_1

    :cond_5
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInStealthMode:Z

    move/from16 v27, v0

    if-eqz v27, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v27, v0

    sget-object v28, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_d

    :cond_7
    const/4 v11, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/Paint;->getFlags()I

    move-result v27

    and-int/lit8 v27, v27, 0x2

    if-eqz v27, :cond_e

    const/16 v20, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v27, v0

    sget-object v29, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    if-ne v0, v1, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaintErrorColor:I

    move/from16 v27, v0

    :goto_6
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v27, v0

    sget-object v29, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    if-ne v0, v1, :cond_10

    const/16 v27, 0x80

    :goto_7
    move-object/from16 v0, v28

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v27, v0

    sget-object v28, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_b

    const/4 v2, 0x0

    const/4 v14, 0x0

    :goto_8
    if-ge v14, v7, :cond_8

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v27

    aget-object v27, v10, v27

    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v28

    aget-boolean v27, v27, v28

    if-nez v27, :cond_11

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternInProgress:Z

    move/from16 v27, v0

    if-nez v27, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v27, v0

    sget-object v28, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Animate:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_a

    :cond_9
    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaint:Landroid/graphics/Paint;

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v9, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v27, v0

    sget-object v28, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_c

    if-lez v7, :cond_c

    if-eqz v11, :cond_c

    invoke-direct/range {p0 .. p1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->drawFadingPath(Landroid/graphics/Canvas;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    return-void

    :cond_d
    const/4 v11, 0x0

    goto/16 :goto_4

    :cond_e
    const/16 v20, 0x0

    goto/16 :goto_5

    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPathPaintColor:I

    move/from16 v27, v0

    goto/16 :goto_6

    :cond_10
    const/16 v27, 0x20

    goto/16 :goto_7

    :cond_11
    const/4 v2, 0x1

    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v5

    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v27

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v6

    if-nez v14, :cond_12

    invoke-virtual {v9, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_9
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_8

    :cond_12
    invoke-virtual {v9, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_9
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    :pswitch_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 0

    invoke-super {p0, p1, p1}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 5

    const/high16 v4, 0x40400000    # 3.0f

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaddingLeft:I

    sub-int v2, p1, v2

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaddingRight:I

    sub-int v1, v2, v3

    int-to-float v2, v1

    div-float/2addr v2, v4

    iput v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareWidth:F

    iget v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaddingTop:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPaddingBottom:I

    sub-int v0, v2, v3

    int-to-float v2, v0

    div-float/2addr v2, v4

    iput v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mSquareHeight:F

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInputEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return v2

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    return v2

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->handleActionDown(Landroid/view/MotionEvent;)V

    return v1

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->handleActionUp(Landroid/view/MotionEvent;)V

    return v1

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->handleActionMove(Landroid/view/MotionEvent;)V

    return v1

    :pswitch_3
    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->resetPattern()V

    iput-boolean v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternInProgress:Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->notifyPatternCleared()V

    return v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V
    .locals 5

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDisplayMode:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    sget-object v1, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Animate:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "you must have a pattern to animate if you want to set the display mode to animate"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mAnimatingPeriodStart:J

    iget-object v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterXForColumn(I)F

    move-result v1

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressX:F

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getCenterYForRow(I)F

    move-result v1

    iput v1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInProgressY:F

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->clearPatternDrawLookup()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->invalidate()V

    return-void
.end method

.method public setInStealthMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mInStealthMode:Z

    return-void
.end method

.method public setOnPatternListener(Lcom/android/internal/widget/LockPatternView$OnPatternListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mOnPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    return-void
.end method

.method public setPattern(Lcom/android/internal/widget/LockPatternView$DisplayMode;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/widget/LockPatternView$DisplayMode;",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPattern:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->clearPatternDrawLookup()V

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView$Cell;

    iget-object v2, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mPatternDrawLookup:[[Z

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getRow()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView$Cell;->getColumn()I

    move-result v3

    const/4 v4, 0x1

    aput-boolean v4, v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    return-void
.end method

.method public setTactileFeedbackEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->mEnableHapticFeedback:Z

    return-void
.end method

.method public startAnimation()V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v2, 0x32

    const/16 v0, 0x32

    invoke-virtual {p0, v8}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v1

    const-wide/16 v6, 0x32

    invoke-virtual {v1, v6, v7}, Landroid/view/animation/Animation;->setStartOffset(J)V

    invoke-virtual {p0, v8}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0, v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v3

    const-wide/16 v6, 0x64

    invoke-virtual {v3, v6, v7}, Landroid/view/animation/Animation;->setStartOffset(J)V

    invoke-virtual {p0, v9}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0, v10}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->createRowAnimation(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v4

    const-wide/16 v6, 0x96

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setStartOffset(J)V

    invoke-virtual {p0, v10}, Lcom/android/keyguard/MiuiKeyguardLockPatternView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
