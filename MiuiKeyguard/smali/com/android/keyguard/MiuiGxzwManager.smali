.class public Lcom/android/keyguard/MiuiGxzwManager;
.super Landroid/os/Binder;
.source "MiuiGxzwManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keyguard/MiuiGxzwManager$1;
    }
.end annotation


# static fields
.field private static final CMD_APP_AUTHEN:I = 0x1

.field private static final CMD_APP_CANCEL:I = 0x2

.field private static final CMD_VENDOR_ERROR:I = 0x4

.field private static final CMD_VENDOR_LOCK_CANCEL:I = 0x5

.field private static final CMD_VENDOR_SUCESS:I = 0x3

.field private static final CODE_PROCESS_CMD:I = 0x1

.field private static final DEBUG:Z = true

.field private static final DEFAULT_PARAM:I = 0x0

.field private static final INTERFACE_DESCRIPTOR:Ljava/lang/String; = "interface_descriptor"

.field private static final MSG_DISMISS:I = 0x3ea

.field private static final MSG_SHOW:I = 0x3e9

.field private static final SERVICE_NAME:Ljava/lang/String; = "service_name"

.field private static final TAG:Ljava/lang/String; = "MiuiGxzwManager"

.field private static sService:Lcom/android/keyguard/MiuiGxzwManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

.field private mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

.field private mShouldShowGxzwIconView:Z

.field private mShowing:Z


# direct methods
.method static synthetic -wrap0(Lcom/android/keyguard/MiuiGxzwManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwManager;->dismissGxzwView()V

    return-void
.end method

.method static synthetic -wrap1(Lcom/android/keyguard/MiuiGxzwManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwManager;->showGxzwView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 2

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShowing:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShouldShowGxzwIconView:Z

    new-instance v0, Lcom/android/keyguard/MiuiGxzwManager$1;

    invoke-direct {v0, p0}, Lcom/android/keyguard/MiuiGxzwManager$1;-><init>(Lcom/android/keyguard/MiuiGxzwManager;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/android/keyguard/MiuiGxzwManager;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwOverlayView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/keyguard/MiuiGxzwOverlayView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

    new-instance v0, Lcom/android/keyguard/MiuiGxzwIconView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p3}, Lcom/android/keyguard/MiuiGxzwIconView;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    iput-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    iget-object v1, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiGxzwIconView;->setCollectGxzwListener(Lcom/android/keyguard/MiuiGxzwIconView$CollectGxzwListener;)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {p2, v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerFingerprintStateCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$FingerprintStateCallback;)V

    return-void
.end method

.method private dealCallback(II)I
    .locals 5

    const/16 v4, 0x3e9

    const/16 v3, 0x3ea

    const-string/jumbo v0, "miui_keyguard_fingerprint"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "dealCallback, cmd: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " param: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_3
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private dismissGxzwView()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShowing:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->dismiss()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->dismiss()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShowing:Z

    return-void
.end method

.method public static getInstance()Lcom/android/keyguard/MiuiGxzwManager;
    .locals 1

    sget-object v0, Lcom/android/keyguard/MiuiGxzwManager;->sService:Lcom/android/keyguard/MiuiGxzwManager;

    return-object v0
.end method

.method public static init(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V
    .locals 3

    :try_start_0
    new-instance v1, Lcom/android/keyguard/MiuiGxzwManager;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/keyguard/MiuiGxzwManager;-><init>(Landroid/content/Context;Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;Lcom/android/keyguard/MiuiKeyguardViewMediator;)V

    sput-object v1, Lcom/android/keyguard/MiuiGxzwManager;->sService:Lcom/android/keyguard/MiuiGxzwManager;

    const-string/jumbo v1, "service_name"

    sget-object v2, Lcom/android/keyguard/MiuiGxzwManager;->sService:Lcom/android/keyguard/MiuiGxzwManager;

    invoke-static {v1, v2}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    const-string/jumbo v1, "MiuiGxzwManager"

    const-string/jumbo v2, "add MiuiGxzwManager successfully"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string/jumbo v1, "MiuiGxzwManager"

    const-string/jumbo v2, "add MiuiGxzwManager fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showGxzwView()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShowing:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->show()V

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShouldShowGxzwIconView:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->show()V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShowing:Z

    return-void
.end method


# virtual methods
.method public dismissGxzwIconView(Z)V
    .locals 1

    xor-int/lit8 v0, p1, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShouldShowGxzwIconView:Z

    iget-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShowing:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->dismiss()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->show()V

    goto :goto_0
.end method

.method public dozeTimeTick()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->dozeTimeTick()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->dozeTimeTick()V

    return-void
.end method

.method public keyguardDone()V
    .locals 1

    invoke-direct {p0}, Lcom/android/keyguard/MiuiGxzwManager;->dismissGxzwView()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mShouldShowGxzwIconView:Z

    return-void
.end method

.method public onScreenTurnedOff()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->onScreenTurnedOff()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->onScreenTurnedOff()V

    return-void
.end method

.method public onScreenTurnedOn()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->onScreenTurnedOn()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->onScreenTurnedOn()V

    return-void
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string/jumbo v3, "MiuiGxzwManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onTransact, code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    return v3

    :pswitch_0
    const-string/jumbo v3, "interface_descriptor"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/keyguard/MiuiGxzwManager;->dealCallback(II)I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v3, 0x1

    return v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public startDozing()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->startDozing()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->startDozing()V

    return-void
.end method

.method public stopDozing()V
    .locals 1

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwOverlayView:Lcom/android/keyguard/MiuiGxzwOverlayView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwOverlayView;->stopDozing()V

    iget-object v0, p0, Lcom/android/keyguard/MiuiGxzwManager;->mMiuiGxzwIconView:Lcom/android/keyguard/MiuiGxzwIconView;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiGxzwIconView;->stopDozing()V

    return-void
.end method
