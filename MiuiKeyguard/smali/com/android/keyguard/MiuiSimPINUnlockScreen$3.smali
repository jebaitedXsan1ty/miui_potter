.class Lcom/android/keyguard/MiuiSimPINUnlockScreen$3;
.super Ljava/lang/Object;
.source "MiuiSimPINUnlockScreen.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keyguard/MiuiSimPINUnlockScreen;->getEditTextWatcher()Landroid/text/TextWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;


# direct methods
.method constructor <init>(Lcom/android/keyguard/MiuiSimPINUnlockScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$3;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$3;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mInputPinEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$3;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mUnlockSimButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/keyguard/MiuiSimPINUnlockScreen$3;->this$0:Lcom/android/keyguard/MiuiSimPINUnlockScreen;

    iget-object v0, v0, Lcom/android/keyguard/MiuiSimPINUnlockScreen;->mUnlockSimButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
