.class public Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;
.super Lcom/android/keyguard/MiuiKeyguardBaseClock;
.source "MiuiUnlockScreenDigitalClock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$1;,
        Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$2;,
        Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$3;,
        Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4;,
        Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;,
        Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$6;,
        Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$7;,
        Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;
    }
.end annotation


# instance fields
.field private m24HourFormat:Z

.field private mAttached:Z

.field private mBatteryInfo:Landroid/widget/TextView;

.field private final mBatteryInfoAndDateTransition:Ljava/lang/Runnable;

.field private mBatteryInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

.field private mCalendar:Lmiui/date/Calendar;

.field private mDate:Landroid/widget/TextView;

.field private mDigitalTimeLayout:Landroid/widget/LinearLayout;

.field private mDotDigital:Landroid/widget/TextView;

.field private mFirstDigital:Landroid/widget/TextSwitcher;

.field private mFirstDigitalValue:I

.field private mFouthDigital:Landroid/widget/TextSwitcher;

.field private mFouthDigitalValue:I

.field private final mHandler:Landroid/os/Handler;

.field private mInSmartCoverSmallWindowMode:Z

.field private final mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mLightColorMode:Z

.field private final mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

.field private mNotificationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;",
            ">;"
        }
    .end annotation
.end field

.field private mOwnerInfo:Landroid/widget/TextView;

.field private mSecondDigital:Landroid/widget/TextSwitcher;

.field private mSecondDigitalValue:I

.field private mShowBatteryInfo:Z

.field private mSimCardInfo:Landroid/widget/TextView;

.field private final mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

.field private mThinFontTypeface:Landroid/graphics/Typeface;

.field private mThirdDigital:Landroid/widget/TextSwitcher;

.field private mThirdDigitalValue:I

.field private final mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

.field private mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

.field mUpdateTimeRunnable:Ljava/lang/Runnable;


# direct methods
.method static synthetic -get0(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get1(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get2(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDigitalTimeLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic -get3(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get4(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mOwnerInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get5(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSimCardInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic -get6(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)Landroid/graphics/Typeface;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method static synthetic -set0(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;Lmiui/date/Calendar;)Lmiui/date/Calendar;
    .locals 0

    iput-object p1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mCalendar:Lmiui/date/Calendar;

    return-object p1
.end method

.method static synthetic -set1(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mInSmartCoverSmallWindowMode:Z

    return p1
.end method

.method static synthetic -set2(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mNotificationList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;ZZI)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateBatteryLevelText(ZZI)V

    return-void
.end method

.method static synthetic -wrap1(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateTime(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/keyguard/MiuiKeyguardBaseClock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mLightColorMode:Z

    iput v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigitalValue:I

    iput v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSecondDigitalValue:I

    iput v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThirdDigitalValue:I

    iput v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFouthDigitalValue:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$1;

    invoke-direct {v0, p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$1;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$2;

    invoke-direct {v0, p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$2;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mUpdateTimeRunnable:Ljava/lang/Runnable;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    new-instance v0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$3;

    invoke-direct {v0, p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$3;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    new-instance v0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4;

    invoke-direct {v0, p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$4;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;

    invoke-direct {v0, p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$5;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    new-instance v0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$6;

    invoke-direct {v0, p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$6;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfoAndDateTransition:Ljava/lang/Runnable;

    new-instance v0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$7;

    invoke-direct {v0, p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$7;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    const-string/jumbo v0, "miui-thin"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    return-void
.end method

.method private updateBatteryLevelText(ZZI)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_3

    iput-boolean v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mShowBatteryInfo:Z

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDeviceCharged()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    const v1, 0x7f0b0005

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f0b0004

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x14

    if-ge p3, v0, :cond_0

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_3
    iput-boolean v4, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mShowBatteryInfo:Z

    goto :goto_0
.end method

.method private updateTime(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateTime(ZZ)V

    return-void
.end method

.method private updateTimeDigitalText(Landroid/widget/TextSwitcher;I)V
    .locals 3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextSwitcher;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/widget/TextSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mLightColorMode:Z

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public getTypeface()Landroid/graphics/Typeface;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 7

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mAttached:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mAttached:Z

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v0, "android.intent.action.TIME_TICK"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v0, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->registerInfoCallback(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallback;)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->registerListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    const-string/jumbo v0, "is_small_window"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

    const/4 v2, 0x0

    invoke-virtual {v0, v6, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-boolean v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mLightColorMode:Z

    invoke-virtual {p0, v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateColorByWallpaper(Z)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfoCallback:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor$InfoCallbackImpl;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSmartCoverSettingsContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->unregisterListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    invoke-super {p0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->onDetachedFromWindow()V

    iget-boolean v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mAttached:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mAttached:Z

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDotDigital:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0}, Lcom/android/keyguard/MiuiKeyguardBaseClock;->onFinishInflate()V

    const v2, 0x7f0d0044

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDigitalTimeLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0d0045

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextSwitcher;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigital:Landroid/widget/TextSwitcher;

    const v2, 0x7f0d0046

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextSwitcher;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSecondDigital:Landroid/widget/TextSwitcher;

    const v2, 0x7f0d0048

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextSwitcher;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThirdDigital:Landroid/widget/TextSwitcher;

    const v2, 0x7f0d0049

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextSwitcher;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFouthDigital:Landroid/widget/TextSwitcher;

    const v2, 0x7f0d0047

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDotDigital:Landroid/widget/TextView;

    const v2, 0x7f0d004a

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDate:Landroid/widget/TextView;

    const v2, 0x7f0d004b

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    const v2, 0x7f0d004c

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSimCardInfo:Landroid/widget/TextView;

    const v2, 0x7f0d004d

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mOwnerInfo:Landroid/widget/TextView;

    new-instance v2, Lmiui/date/Calendar;

    invoke-direct {v2}, Lmiui/date/Calendar;-><init>()V

    iput-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mCalendar:Lmiui/date/Calendar;

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/keyguard/KeyguardCompatibilityHelperForL;->is24HourFormat(Landroid/content/Context;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->m24HourFormat:Z

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/provider/MiuiSettings$System;->isInSmallWindowMode(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mInSmartCoverSmallWindowMode:Z

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_0

    const-string/jumbo v2, "miui"

    invoke-static {v2, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDate:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_0
    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigital:Landroid/widget/TextSwitcher;

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSecondDigital:Landroid/widget/TextSwitcher;

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThirdDigital:Landroid/widget/TextSwitcher;

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFouthDigital:Landroid/widget/TextSwitcher;

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mTimeDigitalTextViewFactory:Landroid/widget/ViewSwitcher$ViewFactory;

    invoke-virtual {v2, v3}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDotDigital:Landroid/widget/TextView;

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDotDigital:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThinFontTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDotDigital:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0056

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateOwnerInfo()V

    iget-boolean v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDisplaySimCardInfo:Z

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mPhoneCount:I

    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->initCarrier(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateSimCardInfo()V

    new-instance v2, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$8;

    invoke-direct {v2, p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$8;-><init>(Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;)V

    invoke-virtual {p0, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateBatteryInfoAndDate()V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mUpdateTimeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateTime(Z)V

    return-void
.end method

.method public setKeyguardUpdateMonitor(Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;)V
    .locals 3

    iput-object p1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v0}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->shouldShowBatteryInfo()Z

    move-result v0

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v1}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->isDevicePluggedIn()Z

    move-result v1

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mUpdateMonitor:Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;

    invoke-virtual {v2}, Lcom/android/keyguard/MiuiKeyguardUpdateMonitor;->getBatteryLevel()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateBatteryLevelText(ZZI)V

    return-void
.end method

.method public updateBatteryInfoAndDate()V
    .locals 4

    const/4 v2, 0x4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mShowBatteryInfo:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "is_pad"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfoAndDateTransition:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfoAndDateTransition:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateColorByWallpaper(Z)V
    .locals 4

    const v3, 0x7f080002

    const/high16 v2, 0x7f080000

    iput-boolean p1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mLightColorMode:Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDotDigital:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDate:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSimCardInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateTime(ZZ)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDotDigital:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDate:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mBatteryInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSimCardInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public updateOwnerInfo()V
    .locals 9

    const/4 v8, 0x0

    new-instance v1, Landroid/security/MiuiLockPatternUtils;

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v3

    invoke-static {v1, v3}, Lcom/android/keyguard/KeyguardCompatibilityHelperForN;->getOwnerInfo(Landroid/security/MiuiLockPatternUtils;I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mInSmartCoverSmallWindowMode:Z

    if-eqz v3, :cond_1

    invoke-static {}, Landroid/provider/MiuiSettings$System;->getSmallWindowMode()Landroid/provider/MiuiSettings$System$SmallWindowType;

    move-result-object v3

    sget-object v4, Landroid/provider/MiuiSettings$System$SmallWindowType;->A1_STYLE:Landroid/provider/MiuiSettings$System$SmallWindowType;

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mNotificationList:Ljava/util/List;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mNotificationList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mNotificationList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mNotificationList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const v7, 0x7f0e0004

    invoke-virtual {v4, v7, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mOwnerInfo:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mOwnerInfo:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public updateSimCardInfo()V
    .locals 4

    iget-boolean v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDisplaySimCardInfo:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSimCardInfo:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const-string/jumbo v1, " | "

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mRealCarrier:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/keyguard/MiuiKeyguardUtils;->join(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSimCardInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSimCardInfo:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0096

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSimCardInfo:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateTime(ZZ)V
    .locals 12

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, -0x3d4c0000    # -90.0f

    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v6}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;-><init>(FFIFIF)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v1, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;

    const/high16 v2, 0x43870000    # 270.0f

    const/high16 v3, 0x43b40000    # 360.0f

    const/4 v4, 0x1

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v6, 0x1

    const/high16 v7, 0x3f000000    # 0.5f

    invoke-direct/range {v1 .. v7}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;-><init>(FFIFIF)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    :cond_0
    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigital:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v1}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigital:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v0}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSecondDigital:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v1}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSecondDigital:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v0}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThirdDigital:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v1}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThirdDigital:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v0}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFouthDigital:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v1}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFouthDigital:Landroid/widget/TextSwitcher;

    invoke-virtual {v2, v0}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mCalendar:Lmiui/date/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lmiui/date/Calendar;->setTimeInMillis(J)Lmiui/date/Calendar;

    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mCalendar:Lmiui/date/Calendar;

    const/16 v3, 0x12

    invoke-virtual {v2, v3}, Lmiui/date/Calendar;->get(I)I

    move-result v9

    iget-boolean v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->m24HourFormat:Z

    if-nez v2, :cond_1

    const/16 v2, 0xc

    if-le v9, v2, :cond_1

    add-int/lit8 v9, v9, -0xc

    :cond_1
    iget-boolean v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->m24HourFormat:Z

    if-nez v2, :cond_2

    if-nez v9, :cond_2

    const/16 v9, 0xc

    :cond_2
    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mCalendar:Lmiui/date/Calendar;

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Lmiui/date/Calendar;->get(I)I

    move-result v10

    const/4 v11, 0x0

    iget v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigitalValue:I

    div-int/lit8 v3, v9, 0xa

    if-ne v2, v3, :cond_3

    if-eqz p2, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigital:Landroid/widget/TextSwitcher;

    div-int/lit8 v3, v9, 0xa

    invoke-direct {p0, v2, v3}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateTimeDigitalText(Landroid/widget/TextSwitcher;I)V

    div-int/lit8 v2, v9, 0xa

    iput v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigitalValue:I

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigital:Landroid/widget/TextSwitcher;

    iget v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFirstDigitalValue:I

    if-nez v2, :cond_c

    iget-boolean v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->m24HourFormat:Z

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_c

    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/TextSwitcher;->setVisibility(I)V

    const/4 v11, 0x1

    :cond_4
    iget v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSecondDigitalValue:I

    rem-int/lit8 v3, v9, 0xa

    if-ne v2, v3, :cond_5

    if-eqz p2, :cond_6

    :cond_5
    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSecondDigital:Landroid/widget/TextSwitcher;

    rem-int/lit8 v3, v9, 0xa

    invoke-direct {p0, v2, v3}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateTimeDigitalText(Landroid/widget/TextSwitcher;I)V

    rem-int/lit8 v2, v9, 0xa

    iput v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mSecondDigitalValue:I

    const/4 v11, 0x1

    :cond_6
    iget v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThirdDigitalValue:I

    div-int/lit8 v3, v10, 0xa

    if-ne v2, v3, :cond_7

    if-eqz p2, :cond_8

    :cond_7
    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThirdDigital:Landroid/widget/TextSwitcher;

    div-int/lit8 v3, v10, 0xa

    invoke-direct {p0, v2, v3}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateTimeDigitalText(Landroid/widget/TextSwitcher;I)V

    div-int/lit8 v2, v10, 0xa

    iput v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mThirdDigitalValue:I

    const/4 v11, 0x1

    :cond_8
    iget v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFouthDigitalValue:I

    rem-int/lit8 v3, v10, 0xa

    if-ne v2, v3, :cond_9

    if-eqz p2, :cond_a

    :cond_9
    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFouthDigital:Landroid/widget/TextSwitcher;

    rem-int/lit8 v3, v10, 0xa

    invoke-direct {p0, v2, v3}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->updateTimeDigitalText(Landroid/widget/TextSwitcher;I)V

    rem-int/lit8 v2, v10, 0xa

    iput v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mFouthDigitalValue:I

    const/4 v11, 0x1

    :cond_a
    if-eqz v11, :cond_b

    iget-boolean v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->m24HourFormat:Z

    if-eqz v2, :cond_d

    const v8, 0x7f0b0017

    :goto_1
    iget-object v2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mDate:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mCalendar:Lmiui/date/Calendar;

    iget-object v4, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/date/Calendar;->format(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_b
    return-void

    :cond_c
    const/4 v2, 0x0

    goto :goto_0

    :cond_d
    const v8, 0x7f0b0018

    goto :goto_1
.end method
