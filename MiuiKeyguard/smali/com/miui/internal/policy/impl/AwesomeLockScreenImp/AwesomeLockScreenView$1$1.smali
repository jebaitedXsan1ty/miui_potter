.class Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;
.super Ljava/lang/Object;
.source "AwesomeLockScreenView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->onChange(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

.field final synthetic val$list:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iput-object p2, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->val$list:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private wakeUpWithoutProximityCheck()Z
    .locals 5

    iget-object v2, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v2, v2, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v2}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "sensor"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    const-string/jumbo v2, "miui_keyguard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "AwesomeLockScreenView=;proximitySensorAvailable="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    xor-int/lit8 v2, v0, 0x1

    return v2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 10

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v5}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Landroid/content/Context;

    move-result-object v5

    const-string/jumbo v8, "power"

    invoke-virtual {v5, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    invoke-virtual {v3}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    const-string/jumbo v5, "sys.keyguard.screen_off_by_lid"

    const-string/jumbo v8, "false"

    invoke-static {v5, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->val$list:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v5}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Landroid/content/Context;

    move-result-object v8

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->val$list:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;

    iget-object v5, v5, Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationItem;->pkgName:Ljava/lang/String;

    invoke-static {v8, v5}, Lmiui/provider/ExtraTelephony;->checkKeyguardForQuiet(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    :goto_0
    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v5}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string/jumbo v8, "POWER_SAVE_MODE_OPEN"

    const/4 v9, -0x2

    invoke-static {v5, v8, v7, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v5

    if-ne v5, v6, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string/jumbo v8, "miui_keyguard"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "AwesomeLockScreenView,mWakeupForNotification="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v9, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v9, v9, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v9}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get4(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Z

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v9, ";list.size="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->val$list:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3

    move v5, v6

    :goto_2
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ";isScreenOn="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ";quietModeEnabled="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ";inPowerSaverMode="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ";isPsensorDisabled="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v6, v6, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v6}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get1(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ";isNonUI="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isNonUI()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v5}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get4(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->val$list:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    xor-int/lit8 v5, v1, 0x1

    if-eqz v5, :cond_0

    xor-int/lit8 v5, v4, 0x1

    if-eqz v5, :cond_0

    xor-int/lit8 v5, v0, 0x1

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->wakeUpWithoutProximityCheck()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v5}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-wrap0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)V

    :cond_0
    :goto_3
    return-void

    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_3
    move v5, v7

    goto/16 :goto_2

    :cond_4
    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v5}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get2(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Lmiui/util/ProximitySensorWrapper;

    move-result-object v5

    if-nez v5, :cond_6

    xor-int/lit8 v5, v2, 0x1

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v5}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get1(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {}, Lcom/android/keyguard/MiuiKeyguardUtils;->isNonUI()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v5}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-wrap0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)V

    goto :goto_3

    :cond_5
    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    new-instance v6, Lmiui/util/ProximitySensorWrapper;

    iget-object v7, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v7, v7, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v7}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lmiui/util/ProximitySensorWrapper;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-set0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;Lmiui/util/ProximitySensorWrapper;)Lmiui/util/ProximitySensorWrapper;

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v5, v5, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v5}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get2(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Lmiui/util/ProximitySensorWrapper;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1$1;->this$1:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    iget-object v6, v6, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;->this$0:Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;

    invoke-static {v6}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->-get3(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

    move-result-object v6

    invoke-virtual {v5, v6}, Lmiui/util/ProximitySensorWrapper;->registerListener(Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;)V

    goto :goto_3

    :cond_6
    const-string/jumbo v5, "miui_keyguard"

    const-string/jumbo v6, "not wakeup for notification"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
