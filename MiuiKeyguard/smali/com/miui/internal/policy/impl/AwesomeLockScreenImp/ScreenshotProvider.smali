.class public Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/ScreenshotProvider;
.super Lmiui/maml/elements/BitmapProvider;
.source "ScreenshotProvider.java"


# static fields
.field private static final KEYGUARD_LAYER:I = 0xb

.field public static final TAG_NAME:Ljava/lang/String; = "Screenshot"

.field private static final TYPE_LAYER_MULTIPLIER:I = 0x2710


# direct methods
.method public constructor <init>(Lmiui/maml/ScreenElementRoot;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiui/maml/elements/BitmapProvider;-><init>(Lmiui/maml/ScreenElementRoot;)V

    return-void
.end method


# virtual methods
.method public reset()V
    .locals 7

    invoke-super {p0}, Lmiui/maml/elements/BitmapProvider;->reset()V

    iget-object v3, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/ScreenshotProvider;->mRoot:Lmiui/maml/ScreenElementRoot;

    const-string/jumbo v4, "__is_secure"

    invoke-virtual {v3, v4}, Lmiui/maml/ScreenElementRoot;->getRawAttr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/ScreenshotProvider;->mRoot:Lmiui/maml/ScreenElementRoot;

    invoke-virtual {v3}, Lmiui/maml/ScreenElementRoot;->getContext()Lmiui/maml/ScreenContext;

    move-result-object v3

    iget-object v3, v3, Lmiui/maml/ScreenContext;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lmiui/content/res/ThemeResources;->getLockWallpaperCache(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v3, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/ScreenshotProvider;->mVersionedBitmap:Lmiui/maml/elements/BitmapProvider$VersionedBitmap;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/maml/elements/BitmapProvider$VersionedBitmap;->setBitmap(Landroid/graphics/Bitmap;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v2, 0x1adaf

    iget-object v3, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/ScreenshotProvider;->mVersionedBitmap:Lmiui/maml/elements/BitmapProvider$VersionedBitmap;

    iget-object v4, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/ScreenshotProvider;->mRoot:Lmiui/maml/ScreenElementRoot;

    invoke-virtual {v4}, Lmiui/maml/ScreenElementRoot;->getScreenWidth()I

    move-result v4

    iget-object v5, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/ScreenshotProvider;->mRoot:Lmiui/maml/ScreenElementRoot;

    invoke-virtual {v5}, Lmiui/maml/ScreenElementRoot;->getScreenHeight()I

    move-result v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6, v2}, Lcom/android/keyguard/KeyguardCompatibilityHelper;->screenshot(IIII)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/maml/elements/BitmapProvider$VersionedBitmap;->setBitmap(Landroid/graphics/Bitmap;)Z

    goto :goto_0
.end method
