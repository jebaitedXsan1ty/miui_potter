.class public Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;
.super Lmiui/maml/component/MamlView;
.source "AwesomeLockScreenView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;,
        Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$2;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "AwesomeLockScreenView"


# instance fields
.field private mHasNavigationBar:Z

.field private mIsPsensorDisabled:Z

.field private final mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

.field private mProximitySensorWrapper:Lmiui/util/ProximitySensorWrapper;

.field private final mSensorListener:Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

.field private final mWakeupForNotification:Z


# direct methods
.method static synthetic -get0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic -get1(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mIsPsensorDisabled:Z

    return v0
.end method

.method static synthetic -get2(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Lmiui/util/ProximitySensorWrapper;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mProximitySensorWrapper:Lmiui/util/ProximitySensorWrapper;

    return-object v0
.end method

.method static synthetic -get3(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mSensorListener:Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

    return-object v0
.end method

.method static synthetic -get4(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mWakeupForNotification:Z

    return v0
.end method

.method static synthetic -set0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;Lmiui/util/ProximitySensorWrapper;)Lmiui/util/ProximitySensorWrapper;
    .locals 0

    iput-object p1, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mProximitySensorWrapper:Lmiui/util/ProximitySensorWrapper;

    return-object p1
.end method

.method static synthetic -wrap0(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->wakeupScreenByNotification()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/LockScreenRoot;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Lmiui/maml/component/MamlView;-><init>(Landroid/content/Context;Lmiui/maml/ScreenElementRoot;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mProximitySensorWrapper:Lmiui/util/ProximitySensorWrapper;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mIsPsensorDisabled:Z

    new-instance v0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;

    invoke-direct {v0, p0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$1;-><init>(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    new-instance v0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$2;

    invoke-direct {v0, p0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView$2;-><init>(Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;)V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mSensorListener:Lmiui/util/ProximitySensorWrapper$ProximitySensorChangeListener;

    iget-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "wakeup_for_keyguard_notification"

    invoke-static {}, Lcom/android/keyguard/LockPatternUtilsWrapper;->getCurrentUser()I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, v2}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mWakeupForNotification:Z

    invoke-direct {p0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->initInner()V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardUtils;->isPsensorDisabled(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mIsPsensorDisabled:Z

    return-void
.end method

.method private initInner()V
    .locals 3

    const-string/jumbo v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Landroid/view/IWindowManager;->hasNavigationBar()Z

    move-result v2

    iput-boolean v2, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mHasNavigationBar:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private wakeupScreenByNotification()V
    .locals 2

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/keyguard/MiuiLockPatternKeyguardView;->sWakeupByNotification:Z

    const-string/jumbo v0, "keyguard_screenon_notification"

    iget-object v1, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/keyguard/KeyguardCompatibilityHelperForM;->wakeUp(Ljava/lang/String;Landroid/content/Context;)V

    sget-object v0, Lcom/android/keyguard/AnalyticsHelper;->KEY_KEYGUARD_SCREENON_BY_NOTIFICATION:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/keyguard/AnalyticsHelper;->record(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Lmiui/maml/component/MamlView;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->registerListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->getInstance(Landroid/content/Context;)Lcom/android/keyguard/MiuiKeyguardNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mNotificationChangeListener:Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/keyguard/MiuiKeyguardNotificationManager;->unregisterListener(Lcom/android/keyguard/MiuiKeyguardNotificationManager$NotificationChangeListener;)V

    invoke-super {p0}, Lmiui/maml/component/MamlView;->onDetachedFromWindow()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v3, 0x3

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v2, :cond_1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->setAction(I)V

    const-string/jumbo v1, "AwesomeLockScreenView"

    const-string/jumbo v2, "touch point count > 1, set to ACTION_CANCEL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lmiui/maml/component/MamlView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    :cond_1
    iget-boolean v1, p0, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->mHasNavigationBar:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eq v0, v3, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0
.end method

.method public rebindRoot()V
    .locals 0

    invoke-virtual {p0}, Lcom/miui/internal/policy/impl/AwesomeLockScreenImp/AwesomeLockScreenView;->init()V

    return-void
.end method
