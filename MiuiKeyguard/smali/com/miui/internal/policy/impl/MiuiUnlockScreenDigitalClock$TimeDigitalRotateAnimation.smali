.class Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;
.super Landroid/view/animation/Animation;
.source "MiuiUnlockScreenDigitalClock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TimeDigitalRotateAnimation"
.end annotation


# instance fields
.field private mCamera:Landroid/graphics/Camera;

.field private mCenterX:F

.field private final mCenterXType:I

.field private final mCenterXValue:F

.field private mCenterY:F

.field private final mCenterYType:I

.field private final mCenterYValue:F

.field private final mFromDegrees:F

.field private final mToDegrees:F


# direct methods
.method public constructor <init>(FFIFIF)V
    .locals 0

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    iput p1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mFromDegrees:F

    iput p2, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mToDegrees:F

    iput p3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterXType:I

    iput p5, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterYType:I

    iput p4, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterXValue:F

    iput p6, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterYValue:F

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5

    iget v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mFromDegrees:F

    iget v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mToDegrees:F

    sub-float/2addr v3, v1

    mul-float/2addr v3, p1

    add-float v0, v1, v3

    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v3}, Landroid/graphics/Camera;->save()V

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v3, v0}, Landroid/graphics/Camera;->rotateX(F)V

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v3, v2}, Landroid/graphics/Camera;->getMatrix(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCamera:Landroid/graphics/Camera;

    invoke-virtual {v3}, Landroid/graphics/Camera;->restore()V

    iget v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterX:F

    neg-float v3, v3

    iget v4, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterY:F

    neg-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget v3, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterX:F

    iget v4, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterY:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void
.end method

.method public initialize(IIII)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    new-instance v0, Landroid/graphics/Camera;

    invoke-direct {v0}, Landroid/graphics/Camera;-><init>()V

    iput-object v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCamera:Landroid/graphics/Camera;

    iget v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterXType:I

    iget v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterXValue:F

    invoke-virtual {p0, v0, v1, p1, p3}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->resolveSize(IFII)F

    move-result v0

    iput v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterX:F

    iget v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterYType:I

    iget v1, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterYValue:F

    invoke-virtual {p0, v0, v1, p2, p4}, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->resolveSize(IFII)F

    move-result v0

    iput v0, p0, Lcom/miui/internal/policy/impl/MiuiUnlockScreenDigitalClock$TimeDigitalRotateAnimation;->mCenterY:F

    return-void
.end method
