.class public Lmiui/bluetooth/ble/MiBleProfile;
.super Ljava/lang/Object;
.source "MiBleProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/bluetooth/ble/MiBleProfile$1;,
        Lmiui/bluetooth/ble/MiBleProfile$2;,
        Lmiui/bluetooth/ble/MiBleProfile$3;,
        Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;,
        Lmiui/bluetooth/ble/MiBleProfile$IPropertyNotifyCallback;
    }
.end annotation


# static fields
.field public static final ACTION_MIBLE_SERVICE:Ljava/lang/String; = "miui.bluetooth.mible.Service"

.field public static final ACTION_SELECT_DEVICE:Ljava/lang/String; = "miui.bluetooth.action.PICK_DEVICE"

.field protected static final DBG:Z = true

.field public static final EXTRA_MIBLE_PROPERTY:Ljava/lang/String; = "miui.bluetooth.extra.MIBLE_PROPERTY"

.field private static final MSG_PROPERTY:I = 0x2

.field private static final MSG_STATUS:I = 0x1

.field public static final PROPERTY_ALARM_CLOCK:I = 0x6a

.field public static final PROPERTY_ALERT_NOTIFICATION:I = 0x8

.field public static final PROPERTY_BATTERY:I = 0x6

.field public static final PROPERTY_DEVICE_CONTROL:I = 0x2

.field public static final PROPERTY_DEVICE_INFO:I = 0x65

.field public static final PROPERTY_FIRMWARE:I = 0x3

.field public static final PROPERTY_IMMEDIATE_ALERT:I = 0x5

.field public static final PROPERTY_LINK_LOSS:I = 0x7

.field public static final PROPERTY_MI_BAND_EVENT:I = 0x6c

.field public static final PROPERTY_MI_KEY:I = 0x6b

.field public static final PROPERTY_PAY:I = 0x4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PROPERTY_SPORT_ACTIVITIES:I = 0x68

.field public static final PROPERTY_SPORT_STEPS:I = 0x67

.field public static final PROPERTY_THEME_COLOR:I = 0x69

.field public static final PROPERTY_UNDEFINED:I = 0x0

.field public static final PROPERTY_UNLOCK:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PROPERTY_USER_INFO:I = 0x66

.field public static final SERVICE_VERSION_UNKNOWN:I = -0x1

.field public static final STATUS_CONNECTED:I = 0x2

.field public static final STATUS_CONNECTING:I = 0x1

.field public static final STATUS_DISCONNECTED:I = 0x0

.field public static final STATUS_DISCONNECTING:I = 0x3

.field public static final STATUS_ERROR:I = -0x1

.field public static final STATUS_READY:I = 0x4

.field protected static final TAG:Ljava/lang/String; = "MiBleProfile"


# instance fields
.field protected mCallback:Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;

.field protected final mClientId:Landroid/os/ParcelUuid;

.field private mConnectWhenBind:Z

.field protected mContext:Landroid/content/Context;

.field protected mDevice:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mProfileState:I

.field private mPropertyCallback:Lmiui/bluetooth/ble/IBluetoothMiBlePropertyCallback;

.field private mPropertyCallbacks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lmiui/bluetooth/ble/MiBleProfile$IPropertyNotifyCallback;",
            ">;"
        }
    .end annotation
.end field

.field protected mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

.field private mServiceCallback:Lmiui/bluetooth/ble/IBluetoothMiBleCallback;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private final mToken:Landroid/os/IBinder;


# direct methods
.method static synthetic -get0(Lmiui/bluetooth/ble/MiBleProfile;)Z
    .locals 1

    iget-boolean v0, p0, Lmiui/bluetooth/ble/MiBleProfile;->mConnectWhenBind:Z

    return v0
.end method

.method static synthetic -get1(Lmiui/bluetooth/ble/MiBleProfile;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic -get2(Lmiui/bluetooth/ble/MiBleProfile;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lmiui/bluetooth/ble/MiBleProfile;->mPropertyCallbacks:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic -get3(Lmiui/bluetooth/ble/MiBleProfile;)Lmiui/bluetooth/ble/IBluetoothMiBleCallback;
    .locals 1

    iget-object v0, p0, Lmiui/bluetooth/ble/MiBleProfile;->mServiceCallback:Lmiui/bluetooth/ble/IBluetoothMiBleCallback;

    return-object v0
.end method

.method static synthetic -get4(Lmiui/bluetooth/ble/MiBleProfile;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lmiui/bluetooth/ble/MiBleProfile;->mToken:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic -set0(Lmiui/bluetooth/ble/MiBleProfile;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mConnectWhenBind:Z

    return p1
.end method

.method static synthetic -set1(Lmiui/bluetooth/ble/MiBleProfile;I)I
    .locals 0

    iput p1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mProfileState:I

    return p1
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiui/bluetooth/ble/MiBleProfile;-><init>(Landroid/content/Context;Ljava/lang/String;Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Landroid/os/ParcelUuid;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    iput-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mClientId:Landroid/os/ParcelUuid;

    new-instance v1, Landroid/os/Binder;

    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    iput-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mToken:Landroid/os/IBinder;

    iput-boolean v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mConnectWhenBind:Z

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mPropertyCallbacks:Landroid/util/SparseArray;

    iput v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mProfileState:I

    new-instance v1, Lmiui/bluetooth/ble/MiBleProfile$1;

    invoke-direct {v1, p0}, Lmiui/bluetooth/ble/MiBleProfile$1;-><init>(Lmiui/bluetooth/ble/MiBleProfile;)V

    iput-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mServiceConnection:Landroid/content/ServiceConnection;

    new-instance v1, Lmiui/bluetooth/ble/MiBleProfile$2;

    invoke-direct {v1, p0}, Lmiui/bluetooth/ble/MiBleProfile$2;-><init>(Lmiui/bluetooth/ble/MiBleProfile;)V

    iput-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mServiceCallback:Lmiui/bluetooth/ble/IBluetoothMiBleCallback;

    new-instance v1, Lmiui/bluetooth/ble/MiBleProfile$3;

    invoke-direct {v1, p0}, Lmiui/bluetooth/ble/MiBleProfile$3;-><init>(Lmiui/bluetooth/ble/MiBleProfile;)V

    iput-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mPropertyCallback:Lmiui/bluetooth/ble/IBluetoothMiBlePropertyCallback;

    iput-object p2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    iput-object p1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mCallback:Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;

    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Landroid/os/Looper;->prepare()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lmiui/bluetooth/ble/MiBleProfile$4;

    invoke-direct {v2, p0}, Lmiui/bluetooth/ble/MiBleProfile$4;-><init>(Lmiui/bluetooth/ble/MiBleProfile;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiBleProfile"

    const-string/jumbo v2, "prepare looper failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public connect()V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v7, 0x1

    iget-object v4, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    if-nez v4, :cond_2

    iput-boolean v7, p0, Lmiui/bluetooth/ble/MiBleProfile;->mConnectWhenBind:Z

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "miui.bluetooth.mible.Service"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "com.xiaomi.bluetooth"

    const-string/jumbo v4, "com.android.bluetooth.ble.BluetoothMiBleService"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lmiui/bluetooth/ble/MiBleProfile;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lmiui/bluetooth/ble/MiBleProfile;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v4, v1, v5, v7}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v2, "com.android.bluetooth"

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v4, "miui.bluetooth.mible.Service"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "com.android.bluetooth.ble.BluetoothMiBleService"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lmiui/bluetooth/ble/MiBleProfile;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lmiui/bluetooth/ble/MiBleProfile;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v4, v1, v5, v7}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    :cond_0
    if-nez v3, :cond_1

    const-string/jumbo v4, "MiBleProfile"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "connect: bind service error"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v7, v8, v9}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_0
    iget-object v4, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    iget-object v5, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    iget-object v6, p0, Lmiui/bluetooth/ble/MiBleProfile;->mClientId:Landroid/os/ParcelUuid;

    invoke-interface {v4, v5, v6}, Lmiui/bluetooth/ble/IBluetoothMiBle;->connect(Ljava/lang/String;Landroid/os/ParcelUuid;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v4, "MiBleProfile"

    const-string/jumbo v5, "connect: "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v4, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v7, v8, v9}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public disconnect()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x1

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    if-nez v2, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mToken:Landroid/os/IBinder;

    iget-object v4, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    iget-object v5, p0, Lmiui/bluetooth/ble/MiBleProfile;->mClientId:Landroid/os/ParcelUuid;

    invoke-interface {v2, v3, v4, v5}, Lmiui/bluetooth/ble/IBluetoothMiBle;->unregisterClient(Landroid/os/IBinder;Ljava/lang/String;Landroid/os/ParcelUuid;)V

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string/jumbo v2, "MiBleProfile"

    const-string/jumbo v3, "disconnect: "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v8, v6, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v2, "MiBleProfile"

    const-string/jumbo v3, "disconnect: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v8, v6, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public getDeviceAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    return-object v0
.end method

.method public getProperty(I)[B
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mClientId:Landroid/os/ParcelUuid;

    invoke-interface {v1, v2, v3, p1}, Lmiui/bluetooth/ble/IBluetoothMiBle;->getProperty(Ljava/lang/String;Landroid/os/ParcelUuid;I)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiBleProfile"

    const-string/jumbo v2, "getProperty: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    return-object v4
.end method

.method public getRssi()I
    .locals 4

    invoke-virtual {p0}, Lmiui/bluetooth/ble/MiBleProfile;->isReady()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mClientId:Landroid/os/ParcelUuid;

    invoke-interface {v1, v2, v3}, Lmiui/bluetooth/ble/IBluetoothMiBle;->getRssi(Ljava/lang/String;Landroid/os/ParcelUuid;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiBleProfile"

    const-string/jumbo v2, "getRssi: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/high16 v1, -0x80000000

    return v1
.end method

.method public getServiceVersion()I
    .locals 3

    iget-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    invoke-interface {v1}, Lmiui/bluetooth/ble/IBluetoothMiBle;->getServiceVersion()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiBleProfile"

    const-string/jumbo v2, "getServiceVersion"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 v1, -0x1

    return v1
.end method

.method public isReady()Z
    .locals 2

    iget v0, p0, Lmiui/bluetooth/ble/MiBleProfile;->mProfileState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSupportProperty(I)Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    invoke-interface {v2, v3, p1}, Lmiui/bluetooth/ble/IBluetoothMiBle;->supportProperty(Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    return v1

    :catch_0
    move-exception v0

    const-string/jumbo v2, "MiBleProfile"

    const-string/jumbo v3, "isSupportProperty: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return v1
.end method

.method public registerPropertyNotifyCallback(ILmiui/bluetooth/ble/MiBleProfile$IPropertyNotifyCallback;)Z
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mPropertyCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    iget-object v4, p0, Lmiui/bluetooth/ble/MiBleProfile;->mClientId:Landroid/os/ParcelUuid;

    iget-object v5, p0, Lmiui/bluetooth/ble/MiBleProfile;->mPropertyCallback:Lmiui/bluetooth/ble/IBluetoothMiBlePropertyCallback;

    invoke-interface {v2, v3, v4, p1, v5}, Lmiui/bluetooth/ble/IBluetoothMiBle;->registerPropertyCallback(Ljava/lang/String;Landroid/os/ParcelUuid;ILmiui/bluetooth/ble/IBluetoothMiBlePropertyCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    if-nez v1, :cond_1

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mPropertyCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    const-string/jumbo v2, "MiBleProfile"

    const-string/jumbo v3, "registerPropertyNotifyCallback: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public setProfileStateChangeCallback(Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;)V
    .locals 0

    iput-object p1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mCallback:Lmiui/bluetooth/ble/MiBleProfile$IProfileStateChangeCallback;

    return-void
.end method

.method public setProperty(I[B)Z
    .locals 4

    iget-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mClientId:Landroid/os/ParcelUuid;

    invoke-interface {v1, v2, v3, p1, p2}, Lmiui/bluetooth/ble/IBluetoothMiBle;->setProperty(Ljava/lang/String;Landroid/os/ParcelUuid;I[B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MiBleProfile"

    const-string/jumbo v2, "setProperty: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public unregisterPropertyNotifyCallback(I)Z
    .locals 6

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mPropertyCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    const/4 v1, 0x1

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mPropertyCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmiui/bluetooth/ble/MiBleProfile;->mService:Lmiui/bluetooth/ble/IBluetoothMiBle;

    iget-object v3, p0, Lmiui/bluetooth/ble/MiBleProfile;->mDevice:Ljava/lang/String;

    iget-object v4, p0, Lmiui/bluetooth/ble/MiBleProfile;->mClientId:Landroid/os/ParcelUuid;

    iget-object v5, p0, Lmiui/bluetooth/ble/MiBleProfile;->mPropertyCallback:Lmiui/bluetooth/ble/IBluetoothMiBlePropertyCallback;

    invoke-interface {v2, v3, v4, p1, v5}, Lmiui/bluetooth/ble/IBluetoothMiBle;->unregisterPropertyCallback(Ljava/lang/String;Landroid/os/ParcelUuid;ILmiui/bluetooth/ble/IBluetoothMiBlePropertyCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    const-string/jumbo v2, "MiBleProfile"

    const-string/jumbo v3, "unregisterPropertyNotifyCallback: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
