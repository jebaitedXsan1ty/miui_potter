.class public final Lorg/apache/thrift/transport/d;
.super Lorg/apache/thrift/transport/a;
.source "TMemoryInputTransport.java"


# instance fields
.field private eXw:[B

.field private eXx:I

.field private eXy:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/thrift/transport/a;-><init>()V

    return-void
.end method


# virtual methods
.method public erl()I
    .locals 2

    iget v0, p0, Lorg/apache/thrift/transport/d;->eXy:I

    iget v1, p0, Lorg/apache/thrift/transport/d;->eXx:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public erm()[B
    .locals 1

    iget-object v0, p0, Lorg/apache/thrift/transport/d;->eXw:[B

    return-object v0
.end method

.method public ern([BII)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "No writing allowed!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public ero()I
    .locals 1

    iget v0, p0, Lorg/apache/thrift/transport/d;->eXx:I

    return v0
.end method

.method public erp(I)V
    .locals 1

    iget v0, p0, Lorg/apache/thrift/transport/d;->eXx:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/thrift/transport/d;->eXx:I

    return-void
.end method

.method public err([B)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/apache/thrift/transport/d;->ers([BII)V

    return-void
.end method

.method public ers([BII)V
    .locals 1

    iput-object p1, p0, Lorg/apache/thrift/transport/d;->eXw:[B

    iput p2, p0, Lorg/apache/thrift/transport/d;->eXx:I

    add-int v0, p2, p3

    iput v0, p0, Lorg/apache/thrift/transport/d;->eXy:I

    return-void
.end method

.method public read([BII)I
    .locals 2

    invoke-virtual {p0}, Lorg/apache/thrift/transport/d;->erl()I

    move-result v0

    if-gt p3, v0, :cond_0

    :goto_0
    if-gtz p3, :cond_1

    :goto_1
    return p3

    :cond_0
    move p3, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/thrift/transport/d;->eXw:[B

    iget v1, p0, Lorg/apache/thrift/transport/d;->eXx:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p0, p3}, Lorg/apache/thrift/transport/d;->erp(I)V

    goto :goto_1
.end method
