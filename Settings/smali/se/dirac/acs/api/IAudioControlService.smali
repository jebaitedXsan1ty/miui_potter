.class public interface abstract Lse/dirac/acs/api/IAudioControlService;
.super Ljava/lang/Object;
.source "IAudioControlService.java"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract etk(Lse/dirac/acs/api/OutputSettings;)Z
.end method

.method public abstract etl(J)Z
.end method

.method public abstract etm(Lse/dirac/acs/api/IAudioControlServiceCallback;)V
.end method

.method public abstract etn()Ljava/lang/String;
.end method

.method public abstract eto(Lse/dirac/acs/api/Output;)Lse/dirac/acs/api/OutputSettings;
.end method

.method public abstract etp(J)[B
.end method

.method public abstract etq(Ljava/lang/String;Lse/dirac/acs/api/Output;)Ljava/util/List;
.end method

.method public abstract etr()I
.end method

.method public abstract ets(Ljava/lang/String;Ljava/lang/String;)Lse/dirac/acs/api/Device;
.end method

.method public abstract ett(Lse/dirac/acs/api/IAudioControlServiceCallback;)V
.end method

.method public abstract etu(JLjava/lang/String;)Lse/dirac/acs/api/Device;
.end method

.method public abstract etv()Ljava/lang/String;
.end method

.method public abstract etw(Ljava/lang/String;)Z
.end method

.method public abstract etx(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract ety()I
.end method

.method public abstract etz(Lse/dirac/acs/api/Output;)V
.end method
