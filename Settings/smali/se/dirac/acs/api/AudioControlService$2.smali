.class Lse/dirac/acs/api/AudioControlService$2;
.super Lse/dirac/acs/api/IAudioControlServiceCallback$Stub;
.source "AudioControlService.java"


# instance fields
.field final synthetic eZi:Lse/dirac/acs/api/AudioControlService;


# direct methods
.method constructor <init>(Lse/dirac/acs/api/AudioControlService;)V
    .locals 0

    iput-object p1, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-direct {p0}, Lse/dirac/acs/api/IAudioControlServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public etA()V
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, "se.dirac.acs-api"

    const-string/jumbo v1, "Sync done callback received"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0}, Lse/dirac/acs/api/AudioControlService;->esM(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0}, Lse/dirac/acs/api/AudioControlService;->esM(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;->esZ(Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0, v2}, Lse/dirac/acs/api/AudioControlService;->esW(Lse/dirac/acs/api/AudioControlService;Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;)Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v1, v0}, Lse/dirac/acs/api/AudioControlService;->esQ(Lse/dirac/acs/api/AudioControlService;Ljava/lang/RuntimeException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0, v2}, Lse/dirac/acs/api/AudioControlService;->esW(Lse/dirac/acs/api/AudioControlService;Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;)Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v1, v2}, Lse/dirac/acs/api/AudioControlService;->esW(Lse/dirac/acs/api/AudioControlService;Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;)Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;

    throw v0
.end method

.method public etB(Lse/dirac/acs/api/Output;Lse/dirac/acs/api/OutputSettings;)V
    .locals 2

    const-string/jumbo v0, "se.dirac.acs-api"

    const-string/jumbo v1, "Settings changed callback received"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0}, Lse/dirac/acs/api/AudioControlService;->esV(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$SettingsChangedFunctor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0}, Lse/dirac/acs/api/AudioControlService;->esV(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$SettingsChangedFunctor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lse/dirac/acs/api/AudioControlService$SettingsChangedFunctor;->etb(Lse/dirac/acs/api/Output;Lse/dirac/acs/api/OutputSettings;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v1, v0}, Lse/dirac/acs/api/AudioControlService;->esQ(Lse/dirac/acs/api/AudioControlService;Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method public etC(I)V
    .locals 2

    const-string/jumbo v0, "se.dirac.acs-api"

    const-string/jumbo v1, "Routing change callback received"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0}, Lse/dirac/acs/api/AudioControlService;->esK(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$RoutingChangedFunctor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0}, Lse/dirac/acs/api/AudioControlService;->esK(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$RoutingChangedFunctor;

    move-result-object v0

    invoke-interface {v0, p1}, Lse/dirac/acs/api/AudioControlService$RoutingChangedFunctor;->esX(I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v1, v0}, Lse/dirac/acs/api/AudioControlService;->esQ(Lse/dirac/acs/api/AudioControlService;Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method public etD(J[I)V
    .locals 5

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0}, Lse/dirac/acs/api/AudioControlService;->esO(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$DeviceListChangedFunctor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-virtual {v0, p1, p2}, Lse/dirac/acs/api/AudioControlService;->esT(J)Lse/dirac/acs/api/Device;

    move-result-object v0

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v1}, Lse/dirac/acs/api/AudioControlService;->esO(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$DeviceListChangedFunctor;

    move-result-object v1

    iget-object v2, v0, Lse/dirac/acs/api/Device;->eYD:Ljava/util/List;

    sget-object v3, Lse/dirac/acs/api/AudioControlService$DeviceListChangedFunctor$Event;->eYL:Lse/dirac/acs/api/AudioControlService$DeviceListChangedFunctor$Event;

    invoke-interface {v1, v0, v2, v3}, Lse/dirac/acs/api/AudioControlService$DeviceListChangedFunctor;->eta(Lse/dirac/acs/api/Device;Ljava/util/List;Lse/dirac/acs/api/AudioControlService$DeviceListChangedFunctor$Event;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v1, v0}, Lse/dirac/acs/api/AudioControlService;->esQ(Lse/dirac/acs/api/AudioControlService;Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method public etE(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "se.dirac.acs-api"

    const-string/jumbo v1, "Set user callback received"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0}, Lse/dirac/acs/api/AudioControlService;->esR(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$SetUserFunctor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v0}, Lse/dirac/acs/api/AudioControlService;->esR(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$SetUserFunctor;

    move-result-object v0

    invoke-interface {v0, p1}, Lse/dirac/acs/api/AudioControlService$SetUserFunctor;->esY(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService$2;->eZi:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v1, v0}, Lse/dirac/acs/api/AudioControlService;->esQ(Lse/dirac/acs/api/AudioControlService;Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method
