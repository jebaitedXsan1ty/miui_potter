.class Lse/dirac/acs/api/AudioControlService$3;
.super Ljava/lang/Object;
.source "AudioControlService.java"

# interfaces
.implements Lse/dirac/acs/api/AsyncHelper$Function;


# instance fields
.field final synthetic eZk:Lse/dirac/acs/api/AudioControlService;


# direct methods
.method constructor <init>(Lse/dirac/acs/api/AudioControlService;)V
    .locals 0

    iput-object p1, p0, Lse/dirac/acs/api/AudioControlService$3;->eZk:Lse/dirac/acs/api/AudioControlService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic etN(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lse/dirac/acs/api/OutputSettings;

    invoke-virtual {p0, p1}, Lse/dirac/acs/api/AudioControlService$3;->etT(Lse/dirac/acs/api/OutputSettings;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public etT(Lse/dirac/acs/api/OutputSettings;)Ljava/lang/Boolean;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$3;->eZk:Lse/dirac/acs/api/AudioControlService;

    invoke-virtual {v0, p1}, Lse/dirac/acs/api/AudioControlService;->esP(Lse/dirac/acs/api/OutputSettings;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService$3;->eZk:Lse/dirac/acs/api/AudioControlService;

    invoke-static {v1, v0}, Lse/dirac/acs/api/AudioControlService;->esQ(Lse/dirac/acs/api/AudioControlService;Ljava/lang/RuntimeException;)V

    const/4 v0, 0x0

    return-object v0
.end method
