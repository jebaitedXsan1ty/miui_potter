.class public Lse/dirac/acs/api/AudioControlService;
.super Ljava/lang/Object;
.source "AudioControlService.java"

# interfaces
.implements Ljava/lang/AutoCloseable;


# instance fields
.field private eYf:Lse/dirac/acs/api/AudioControlService$SetUserFunctor;

.field private final eYg:Lse/dirac/acs/api/AsyncHelper;

.field private eYh:Lse/dirac/acs/api/AudioControlService$DeviceListChangedFunctor;

.field private final eYi:Landroid/content/Context;

.field private eYj:Lse/dirac/acs/api/AudioControlService$RoutingChangedFunctor;

.field private eYk:Lse/dirac/acs/api/IAudioControlService;

.field private eYl:Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;

.field private eYm:Lse/dirac/acs/api/AudioControlService$SettingsChangedFunctor;

.field private eYn:Lse/dirac/acs/api/IAudioControlServiceCallback;

.field private final locale:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lse/dirac/acs/api/IAudioControlService;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lse/dirac/acs/api/AudioControlService$2;

    invoke-direct {v0, p0}, Lse/dirac/acs/api/AudioControlService$2;-><init>(Lse/dirac/acs/api/AudioControlService;)V

    iput-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYn:Lse/dirac/acs/api/IAudioControlServiceCallback;

    new-instance v0, Lse/dirac/acs/api/AsyncHelper;

    new-instance v1, Lse/dirac/acs/api/AudioControlService$3;

    invoke-direct {v1, p0}, Lse/dirac/acs/api/AudioControlService$3;-><init>(Lse/dirac/acs/api/AudioControlService;)V

    invoke-direct {v0, v1}, Lse/dirac/acs/api/AsyncHelper;-><init>(Lse/dirac/acs/api/AsyncHelper$Function;)V

    iput-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYg:Lse/dirac/acs/api/AsyncHelper;

    iput-object v2, p0, Lse/dirac/acs/api/AudioControlService;->eYf:Lse/dirac/acs/api/AudioControlService$SetUserFunctor;

    iput-object v2, p0, Lse/dirac/acs/api/AudioControlService;->eYm:Lse/dirac/acs/api/AudioControlService$SettingsChangedFunctor;

    iput-object v2, p0, Lse/dirac/acs/api/AudioControlService;->eYj:Lse/dirac/acs/api/AudioControlService$RoutingChangedFunctor;

    iput-object p2, p0, Lse/dirac/acs/api/AudioControlService;->eYk:Lse/dirac/acs/api/IAudioControlService;

    iput-object p1, p0, Lse/dirac/acs/api/AudioControlService;->eYi:Landroid/content/Context;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lse/dirac/acs/api/AudioControlService;->locale:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYn:Lse/dirac/acs/api/IAudioControlServiceCallback;

    invoke-interface {p2, v0}, Lse/dirac/acs/api/IAudioControlService;->etm(Lse/dirac/acs/api/IAudioControlServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Exception thrown in registerCallback"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lse/dirac/acs/api/IAudioControlService;Lse/dirac/acs/api/AudioControlService$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lse/dirac/acs/api/AudioControlService;-><init>(Landroid/content/Context;Lse/dirac/acs/api/IAudioControlService;)V

    return-void
.end method

.method private esI(Ljava/lang/String;Lse/dirac/acs/api/Output;)Ljava/util/List;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYk:Lse/dirac/acs/api/IAudioControlService;

    invoke-interface {v0, p1, p2}, Lse/dirac/acs/api/IAudioControlService;->etq(Ljava/lang/String;Lse/dirac/acs/api/Output;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Exception thrown in listDevices"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private esJ(Ljava/lang/RuntimeException;)V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService;->eYi:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lse/dirac/acs/api/AudioControlService$4;

    invoke-direct {v1, p0, p1}, Lse/dirac/acs/api/AudioControlService$4;-><init>(Lse/dirac/acs/api/AudioControlService;Ljava/lang/RuntimeException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic esK(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$RoutingChangedFunctor;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYj:Lse/dirac/acs/api/AudioControlService$RoutingChangedFunctor;

    return-object v0
.end method

.method static synthetic esM(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYl:Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;

    return-object v0
.end method

.method public static esN()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "se.dirac.acs"

    const-string/jumbo v2, "se.dirac.acs.AudioControlService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic esO(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$DeviceListChangedFunctor;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYh:Lse/dirac/acs/api/AudioControlService$DeviceListChangedFunctor;

    return-object v0
.end method

.method static synthetic esQ(Lse/dirac/acs/api/AudioControlService;Ljava/lang/RuntimeException;)V
    .locals 0

    invoke-direct {p0, p1}, Lse/dirac/acs/api/AudioControlService;->esJ(Ljava/lang/RuntimeException;)V

    return-void
.end method

.method static synthetic esR(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$SetUserFunctor;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYf:Lse/dirac/acs/api/AudioControlService$SetUserFunctor;

    return-object v0
.end method

.method public static esU(Landroid/content/Context;Lse/dirac/acs/api/AudioControlService$Connection;)Z
    .locals 3

    invoke-static {}, Lse/dirac/acs/api/AudioControlService;->esN()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {p1, p0}, Lse/dirac/acs/api/AudioControlService$Connection;->ete(Lse/dirac/acs/api/AudioControlService$Connection;Landroid/content/Context;)Landroid/content/Context;

    invoke-static {p1}, Lse/dirac/acs/api/AudioControlService$Connection;->etf(Lse/dirac/acs/api/AudioControlService$Connection;)Landroid/content/ServiceConnection;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    return v0
.end method

.method static synthetic esV(Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService$SettingsChangedFunctor;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYm:Lse/dirac/acs/api/AudioControlService$SettingsChangedFunctor;

    return-object v0
.end method

.method static synthetic esW(Lse/dirac/acs/api/AudioControlService;Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;)Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;
    .locals 0

    iput-object p1, p0, Lse/dirac/acs/api/AudioControlService;->eYl:Lse/dirac/acs/api/AudioControlService$SyncDoneFunctor;

    return-object p1
.end method


# virtual methods
.method public close()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYk:Lse/dirac/acs/api/IAudioControlService;

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService;->eYn:Lse/dirac/acs/api/IAudioControlServiceCallback;

    invoke-interface {v0, v1}, Lse/dirac/acs/api/IAudioControlService;->ett(Lse/dirac/acs/api/IAudioControlServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Exception thrown in unregisterCallback"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public esL(Lse/dirac/acs/api/Output;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->locale:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lse/dirac/acs/api/AudioControlService;->esI(Ljava/lang/String;Lse/dirac/acs/api/Output;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public esP(Lse/dirac/acs/api/OutputSettings;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService;->eYk:Lse/dirac/acs/api/IAudioControlService;

    invoke-interface {v1, p1}, Lse/dirac/acs/api/IAudioControlService;->etk(Lse/dirac/acs/api/OutputSettings;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Exception thrown in setOutput"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public esS(Lse/dirac/acs/api/Output;)Lse/dirac/acs/api/OutputSettings;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYk:Lse/dirac/acs/api/IAudioControlService;

    invoke-interface {v0, p1}, Lse/dirac/acs/api/IAudioControlService;->eto(Lse/dirac/acs/api/Output;)Lse/dirac/acs/api/OutputSettings;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Exception thrown in getOutput"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public esT(J)Lse/dirac/acs/api/Device;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService;->eYk:Lse/dirac/acs/api/IAudioControlService;

    iget-object v1, p0, Lse/dirac/acs/api/AudioControlService;->locale:Ljava/lang/String;

    invoke-interface {v0, p1, p2, v1}, Lse/dirac/acs/api/IAudioControlService;->etu(JLjava/lang/String;)Lse/dirac/acs/api/Device;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Exception thrown in getDevice call"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
