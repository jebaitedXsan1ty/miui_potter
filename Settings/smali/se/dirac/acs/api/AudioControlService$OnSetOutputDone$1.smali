.class Lse/dirac/acs/api/AudioControlService$OnSetOutputDone$1;
.super Ljava/lang/Object;
.source "AudioControlService.java"

# interfaces
.implements Lse/dirac/acs/api/AsyncHelper$Function;


# instance fields
.field final synthetic eZg:Lse/dirac/acs/api/AudioControlService$OnSetOutputDone;


# direct methods
.method constructor <init>(Lse/dirac/acs/api/AudioControlService$OnSetOutputDone;)V
    .locals 0

    iput-object p1, p0, Lse/dirac/acs/api/AudioControlService$OnSetOutputDone$1;->eZg:Lse/dirac/acs/api/AudioControlService$OnSetOutputDone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic etN(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lse/dirac/acs/api/AudioControlService$OnSetOutputDone$1;->etS(Ljava/lang/Boolean;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public etS(Ljava/lang/Boolean;)Ljava/lang/Void;
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_0

    const-string/jumbo v0, "se.dirac.acs-api"

    const-string/jumbo v1, "expecting exception in main thread"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v2

    :cond_0
    iget-object v0, p0, Lse/dirac/acs/api/AudioControlService$OnSetOutputDone$1;->eZg:Lse/dirac/acs/api/AudioControlService$OnSetOutputDone;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lse/dirac/acs/api/AudioControlService$OnSetOutputDone;->etc(Z)V

    goto :goto_0
.end method
