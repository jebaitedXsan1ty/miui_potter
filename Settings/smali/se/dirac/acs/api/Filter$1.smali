.class Lse/dirac/acs/api/Filter$1;
.super Ljava/lang/Object;
.source "Filter.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lse/dirac/acs/api/Filter$1;->createFromParcel(Landroid/os/Parcel;)Lse/dirac/acs/api/Filter;

    move-result-object v0

    return-object v0
.end method

.method public createFromParcel(Landroid/os/Parcel;)Lse/dirac/acs/api/Filter;
    .locals 1

    invoke-static {p1}, Lse/dirac/acs/api/Filter;->etO(Landroid/os/Parcel;)Lse/dirac/acs/api/Filter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lse/dirac/acs/api/Filter$1;->newArray(I)[Lse/dirac/acs/api/Filter;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lse/dirac/acs/api/Filter;
    .locals 1

    new-array v0, p1, [Lse/dirac/acs/api/Filter;

    return-object v0
.end method
