.class public final enum Lse/dirac/acs/api/SyncRequestReply;
.super Ljava/lang/Enum;
.source "SyncRequestReply.java"


# static fields
.field public static final enum eZl:Lse/dirac/acs/api/SyncRequestReply;

.field public static final enum eZm:Lse/dirac/acs/api/SyncRequestReply;

.field public static final enum eZn:Lse/dirac/acs/api/SyncRequestReply;

.field private static final synthetic eZo:[Lse/dirac/acs/api/SyncRequestReply;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lse/dirac/acs/api/SyncRequestReply;

    const-string/jumbo v1, "STARTED"

    invoke-direct {v0, v1, v2, v2}, Lse/dirac/acs/api/SyncRequestReply;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/dirac/acs/api/SyncRequestReply;->eZl:Lse/dirac/acs/api/SyncRequestReply;

    new-instance v0, Lse/dirac/acs/api/SyncRequestReply;

    const-string/jumbo v1, "NOT_STARTED"

    invoke-direct {v0, v1, v3, v3}, Lse/dirac/acs/api/SyncRequestReply;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/dirac/acs/api/SyncRequestReply;->eZm:Lse/dirac/acs/api/SyncRequestReply;

    new-instance v0, Lse/dirac/acs/api/SyncRequestReply;

    const-string/jumbo v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v4, v4}, Lse/dirac/acs/api/SyncRequestReply;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/dirac/acs/api/SyncRequestReply;->eZn:Lse/dirac/acs/api/SyncRequestReply;

    const/4 v0, 0x3

    new-array v0, v0, [Lse/dirac/acs/api/SyncRequestReply;

    sget-object v1, Lse/dirac/acs/api/SyncRequestReply;->eZl:Lse/dirac/acs/api/SyncRequestReply;

    aput-object v1, v0, v2

    sget-object v1, Lse/dirac/acs/api/SyncRequestReply;->eZm:Lse/dirac/acs/api/SyncRequestReply;

    aput-object v1, v0, v3

    sget-object v1, Lse/dirac/acs/api/SyncRequestReply;->eZn:Lse/dirac/acs/api/SyncRequestReply;

    aput-object v1, v0, v4

    sput-object v0, Lse/dirac/acs/api/SyncRequestReply;->eZo:[Lse/dirac/acs/api/SyncRequestReply;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lse/dirac/acs/api/SyncRequestReply;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lse/dirac/acs/api/SyncRequestReply;
    .locals 1

    const-class v0, Lse/dirac/acs/api/SyncRequestReply;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lse/dirac/acs/api/SyncRequestReply;

    return-object v0
.end method

.method public static values()[Lse/dirac/acs/api/SyncRequestReply;
    .locals 1

    sget-object v0, Lse/dirac/acs/api/SyncRequestReply;->eZo:[Lse/dirac/acs/api/SyncRequestReply;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse/dirac/acs/api/SyncRequestReply;

    return-object v0
.end method
