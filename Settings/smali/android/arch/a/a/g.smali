.class public Landroid/arch/a/a/g;
.super Ljava/lang/Object;
.source "SafeIterableMap.java"

# interfaces
.implements Ljava/lang/Iterable;


# instance fields
.field private eWR:Landroid/arch/a/a/e;

.field private eWS:Landroid/arch/a/a/e;

.field private eWT:I

.field private eWU:Ljava/util/WeakHashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Landroid/arch/a/a/g;->eWU:Ljava/util/WeakHashMap;

    const/4 v0, 0x0

    iput v0, p0, Landroid/arch/a/a/g;->eWT:I

    return-void
.end method

.method static synthetic eqo(Landroid/arch/a/a/g;)Landroid/arch/a/a/e;
    .locals 1

    iget-object v0, p0, Landroid/arch/a/a/g;->eWR:Landroid/arch/a/a/e;

    return-object v0
.end method


# virtual methods
.method public eqn()Ljava/util/Iterator;
    .locals 3

    new-instance v0, Landroid/arch/a/a/b;

    iget-object v1, p0, Landroid/arch/a/a/g;->eWS:Landroid/arch/a/a/e;

    iget-object v2, p0, Landroid/arch/a/a/g;->eWR:Landroid/arch/a/a/e;

    invoke-direct {v0, v1, v2}, Landroid/arch/a/a/b;-><init>(Landroid/arch/a/a/e;Landroid/arch/a/a/e;)V

    iget-object v1, p0, Landroid/arch/a/a/g;->eWU:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public eqp()Ljava/util/Map$Entry;
    .locals 1

    iget-object v0, p0, Landroid/arch/a/a/g;->eWS:Landroid/arch/a/a/e;

    return-object v0
.end method

.method public eqq()Ljava/util/Map$Entry;
    .locals 1

    iget-object v0, p0, Landroid/arch/a/a/g;->eWR:Landroid/arch/a/a/e;

    return-object v0
.end method

.method public eqr()Landroid/arch/a/a/h;
    .locals 3

    new-instance v0, Landroid/arch/a/a/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/arch/a/a/h;-><init>(Landroid/arch/a/a/g;Landroid/arch/a/a/f;)V

    iget-object v1, p0, Landroid/arch/a/a/g;->eWU:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, p0, :cond_3

    instance-of v0, p1, Landroid/arch/a/a/g;

    if-eqz v0, :cond_4

    check-cast p1, Landroid/arch/a/a/g;

    invoke-virtual {p0}, Landroid/arch/a/a/g;->size()I

    move-result v0

    invoke-virtual {p1}, Landroid/arch/a/a/g;->size()I

    move-result v3

    if-ne v0, v3, :cond_5

    invoke-virtual {p0}, Landroid/arch/a/a/g;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-virtual {p1}, Landroid/arch/a/a/g;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_2
    move v0, v2

    :goto_0
    return v0

    :cond_3
    return v1

    :cond_4
    return v2

    :cond_5
    return v2

    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    if-eqz v0, :cond_8

    :cond_7
    if-eqz v0, :cond_0

    invoke-interface {v0, v5}, Ljava/util/Map$Entry;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_1
    return v2

    :cond_8
    if-eqz v5, :cond_7

    goto :goto_1

    :cond_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 3

    new-instance v0, Landroid/arch/a/a/d;

    iget-object v1, p0, Landroid/arch/a/a/g;->eWR:Landroid/arch/a/a/e;

    iget-object v2, p0, Landroid/arch/a/a/g;->eWS:Landroid/arch/a/a/e;

    invoke-direct {v0, v1, v2}, Landroid/arch/a/a/d;-><init>(Landroid/arch/a/a/e;Landroid/arch/a/a/e;)V

    iget-object v1, p0, Landroid/arch/a/a/g;->eWU:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public size()I
    .locals 1

    iget v0, p0, Landroid/arch/a/a/g;->eWT:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/arch/a/a/g;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, ", "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
