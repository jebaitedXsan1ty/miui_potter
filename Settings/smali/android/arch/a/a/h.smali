.class Landroid/arch/a/a/h;
.super Ljava/lang/Object;
.source "SafeIterableMap.java"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private eWV:Z

.field final synthetic eWW:Landroid/arch/a/a/g;

.field private eWX:Landroid/arch/a/a/e;


# direct methods
.method private constructor <init>(Landroid/arch/a/a/g;)V
    .locals 1

    iput-object p1, p0, Landroid/arch/a/a/h;->eWW:Landroid/arch/a/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/arch/a/a/h;->eWV:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/arch/a/a/g;Landroid/arch/a/a/f;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/arch/a/a/h;-><init>(Landroid/arch/a/a/g;)V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Landroid/arch/a/a/h;->eWV:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Landroid/arch/a/a/h;->eWX:Landroid/arch/a/a/e;

    if-nez v2, :cond_3

    :goto_0
    move v1, v0

    :cond_0
    return v1

    :cond_1
    iget-object v2, p0, Landroid/arch/a/a/h;->eWW:Landroid/arch/a/a/g;

    invoke-static {v2}, Landroid/arch/a/a/g;->eqo(Landroid/arch/a/a/g;)Landroid/arch/a/a/e;

    move-result-object v2

    if-nez v2, :cond_2

    :goto_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Landroid/arch/a/a/h;->eWX:Landroid/arch/a/a/e;

    iget-object v2, v2, Landroid/arch/a/a/e;->eWO:Landroid/arch/a/a/e;

    if-nez v2, :cond_0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Landroid/arch/a/a/h;->next()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/util/Map$Entry;
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-boolean v1, p0, Landroid/arch/a/a/h;->eWV:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/arch/a/a/h;->eWX:Landroid/arch/a/a/e;

    if-nez v1, :cond_1

    :goto_0
    iput-object v0, p0, Landroid/arch/a/a/h;->eWX:Landroid/arch/a/a/e;

    :goto_1
    iget-object v0, p0, Landroid/arch/a/a/h;->eWX:Landroid/arch/a/a/e;

    return-object v0

    :cond_0
    iput-boolean v2, p0, Landroid/arch/a/a/h;->eWV:Z

    iget-object v0, p0, Landroid/arch/a/a/h;->eWW:Landroid/arch/a/a/g;

    invoke-static {v0}, Landroid/arch/a/a/g;->eqo(Landroid/arch/a/a/g;)Landroid/arch/a/a/e;

    move-result-object v0

    iput-object v0, p0, Landroid/arch/a/a/h;->eWX:Landroid/arch/a/a/e;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Landroid/arch/a/a/h;->eWX:Landroid/arch/a/a/e;

    iget-object v0, v0, Landroid/arch/a/a/e;->eWO:Landroid/arch/a/a/e;

    goto :goto_0
.end method
