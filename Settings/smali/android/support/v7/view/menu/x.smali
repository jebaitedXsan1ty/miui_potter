.class abstract Landroid/support/v7/view/menu/x;
.super Ljava/lang/Object;
.source "MenuPopup.java"

# interfaces
.implements Landroid/support/v7/view/menu/m;
.implements Landroid/support/v7/view/menu/i;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private ett:Landroid/graphics/Rect;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static dKN(Landroid/support/v7/view/menu/p;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/p;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Landroid/support/v7/view/menu/p;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/MenuItem;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected static dKO(Landroid/widget/ListAdapter;Landroid/view/ViewGroup;Landroid/content/Context;I)I
    .locals 10

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-interface {p0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v9

    move v6, v0

    move v3, v0

    move-object v5, v4

    move v2, v0

    move-object v0, p1

    :goto_0
    if-ge v6, v9, :cond_2

    invoke-interface {p0, v6}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v1

    if-eq v1, v3, :cond_4

    move v3, v1

    move-object v1, v4

    :goto_1
    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    :cond_0
    invoke-interface {p0, v6, v1, v0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7, v8}, Landroid/view/View;->measure(II)V

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    if-lt v1, p3, :cond_1

    return p3

    :cond_1
    if-le v1, v2, :cond_3

    :goto_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v1

    goto :goto_0

    :cond_2
    return v2

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    move-object v1, v5

    goto :goto_1
.end method

.method protected static dKQ(Landroid/widget/ListAdapter;)Landroid/support/v7/view/menu/L;
    .locals 1

    instance-of v0, p0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {p0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/L;

    return-object v0

    :cond_0
    check-cast p0, Landroid/support/v7/view/menu/L;

    return-object p0
.end method


# virtual methods
.method public dBW(Landroid/content/Context;Landroid/support/v7/view/menu/p;)V
    .locals 0

    return-void
.end method

.method public dBY(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dBZ(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract dJA(Landroid/view/View;)V
.end method

.method public abstract dJu(I)V
.end method

.method public abstract dJv(Z)V
.end method

.method public abstract dJw(I)V
.end method

.method public abstract dJx(Landroid/support/v7/view/menu/p;)V
.end method

.method public dKP()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/x;->ett:Landroid/graphics/Rect;

    return-object v0
.end method

.method protected dKR()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public dKS(Landroid/graphics/Rect;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/x;->ett:Landroid/graphics/Rect;

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-static {v0}, Landroid/support/v7/view/menu/x;->dKQ(Landroid/widget/ListAdapter;)Landroid/support/v7/view/menu/L;

    move-result-object v1

    iget-object v2, v1, Landroid/support/v7/view/menu/L;->evw:Landroid/support/v7/view/menu/p;

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    invoke-virtual {p0}, Landroid/support/v7/view/menu/x;->dKR()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v0, p0, v1}, Landroid/support/v7/view/menu/p;->dJY(Landroid/view/MenuItem;Landroid/support/v7/view/menu/i;I)Z

    return-void

    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method

.method public abstract setForceShowIcon(Z)V
.end method

.method public abstract setGravity(I)V
.end method

.method public abstract setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
.end method
