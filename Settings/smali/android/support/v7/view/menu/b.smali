.class final Landroid/support/v7/view/menu/b;
.super Landroid/support/v7/view/menu/x;
.source "StandardMenuPopup.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/support/v7/view/menu/i;
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field private final ese:I

.field private esf:Landroid/view/View;

.field private final esg:Z

.field private final esh:Landroid/support/v7/view/menu/p;

.field esi:Landroid/view/View;

.field private final esj:Landroid/view/View$OnAttachStateChangeListener;

.field private esk:I

.field private final esl:Landroid/support/v7/view/menu/L;

.field private esm:Landroid/support/v7/view/menu/c;

.field private esn:Z

.field final eso:Landroid/support/v7/widget/cF;

.field private esp:Z

.field private esq:Landroid/widget/PopupWindow$OnDismissListener;

.field private final esr:I

.field private final ess:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private est:Landroid/view/ViewTreeObserver;

.field private final esu:I

.field private esv:I

.field private esw:Z

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/view/View;IIZ)V
    .locals 5

    invoke-direct {p0}, Landroid/support/v7/view/menu/x;-><init>()V

    new-instance v0, Landroid/support/v7/view/menu/l;

    invoke-direct {v0, p0}, Landroid/support/v7/view/menu/l;-><init>(Landroid/support/v7/view/menu/b;)V

    iput-object v0, p0, Landroid/support/v7/view/menu/b;->ess:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    new-instance v0, Landroid/support/v7/view/menu/o;

    invoke-direct {v0, p0}, Landroid/support/v7/view/menu/o;-><init>(Landroid/support/v7/view/menu/b;)V

    iput-object v0, p0, Landroid/support/v7/view/menu/b;->esj:Landroid/view/View$OnAttachStateChangeListener;

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/view/menu/b;->esk:I

    iput-object p1, p0, Landroid/support/v7/view/menu/b;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/support/v7/view/menu/b;->esh:Landroid/support/v7/view/menu/p;

    iput-boolean p6, p0, Landroid/support/v7/view/menu/b;->esg:Z

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    new-instance v1, Landroid/support/v7/view/menu/L;

    iget-boolean v2, p0, Landroid/support/v7/view/menu/b;->esg:Z

    invoke-direct {v1, p2, v0, v2}, Landroid/support/v7/view/menu/L;-><init>(Landroid/support/v7/view/menu/p;Landroid/view/LayoutInflater;Z)V

    iput-object v1, p0, Landroid/support/v7/view/menu/b;->esl:Landroid/support/v7/view/menu/L;

    iput p4, p0, Landroid/support/v7/view/menu/b;->esr:I

    iput p5, p0, Landroid/support/v7/view/menu/b;->esu:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Landroid/support/v7/b/d;->dOU:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/menu/b;->ese:I

    iput-object p3, p0, Landroid/support/v7/view/menu/b;->esf:Landroid/view/View;

    new-instance v0, Landroid/support/v7/widget/cF;

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->mContext:Landroid/content/Context;

    iget v2, p0, Landroid/support/v7/view/menu/b;->esr:I

    iget v3, p0, Landroid/support/v7/view/menu/b;->esu:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, Landroid/support/v7/widget/cF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {p2, p0, p1}, Landroid/support/v7/view/menu/p;->dKE(Landroid/support/v7/view/menu/i;Landroid/content/Context;)V

    return-void
.end method

.method private dJB()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/b;->dAs()Z

    move-result v0

    if-eqz v0, :cond_0

    return v2

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/view/menu/b;->esp:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esf:Landroid/view/View;

    if-nez v0, :cond_2

    :cond_1
    return v3

    :cond_2
    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esf:Landroid/view/View;

    iput-object v0, p0, Landroid/support/v7/view/menu/b;->esi:Landroid/view/View;

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/cF;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/cF;->dAM(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/cF;->dAG(Z)V

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->esi:Landroid/view/View;

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    if-nez v0, :cond_7

    move v0, v2

    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    iget-object v4, p0, Landroid/support/v7/view/menu/b;->ess:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_3
    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esj:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->dAD(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    iget v1, p0, Landroid/support/v7/view/menu/b;->esk:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->dAz(I)V

    iget-boolean v0, p0, Landroid/support/v7/view/menu/b;->esn:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esl:Landroid/support/v7/view/menu/L;

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->mContext:Landroid/content/Context;

    iget v4, p0, Landroid/support/v7/view/menu/b;->ese:I

    invoke-static {v0, v6, v1, v4}, Landroid/support/v7/view/menu/b;->dKO(Landroid/widget/ListAdapter;Landroid/view/ViewGroup;Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/menu/b;->esv:I

    iput-boolean v2, p0, Landroid/support/v7/view/menu/b;->esn:Z

    :cond_4
    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    iget v1, p0, Landroid/support/v7/view/menu/b;->esv:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->dAp(I)V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->dAE(I)V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {p0}, Landroid/support/v7/view/menu/b;->dKP()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->dAB(Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->show()V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-boolean v0, p0, Landroid/support/v7/view/menu/b;->esw:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esh:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/p;->dKy()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/v7/b/g;->dQM:I

    invoke-virtual {v0, v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_5

    iget-object v5, p0, Landroid/support/v7/view/menu/b;->esh:Landroid/support/v7/view/menu/p;

    invoke-virtual {v5}, Landroid/support/v7/view/menu/p;->dKy()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    invoke-virtual {v4, v0, v6, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    :cond_6
    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->esl:Landroid/support/v7/view/menu/L;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cF;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->show()V

    return v2

    :cond_7
    move v0, v3

    goto/16 :goto_0
.end method

.method static synthetic dJC(Landroid/support/v7/view/menu/b;Landroid/view/ViewTreeObserver;)Landroid/view/ViewTreeObserver;
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    return-object p1
.end method

.method static synthetic dJy(Landroid/support/v7/view/menu/b;)Landroid/view/ViewTreeObserver;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    return-object v0
.end method

.method static synthetic dJz(Landroid/support/v7/view/menu/b;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->ess:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method


# virtual methods
.method public dAs()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/view/menu/b;->esp:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->dAs()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dBV(Landroid/support/v7/view/menu/c;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/b;->esm:Landroid/support/v7/view/menu/c;

    return-void
.end method

.method public dBX(Z)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/view/menu/b;->esn:Z

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esl:Landroid/support/v7/view/menu/L;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esl:Landroid/support/v7/view/menu/L;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/L;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public dCa(Landroid/support/v7/view/menu/h;)Z
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/support/v7/view/menu/h;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/support/v7/view/menu/G;

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->mContext:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v7/view/menu/b;->esi:Landroid/view/View;

    iget-boolean v4, p0, Landroid/support/v7/view/menu/b;->esg:Z

    iget v5, p0, Landroid/support/v7/view/menu/b;->esr:I

    iget v6, p0, Landroid/support/v7/view/menu/b;->esu:I

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/view/menu/G;-><init>(Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/view/View;ZII)V

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->esm:Landroid/support/v7/view/menu/c;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/G;->dLL(Landroid/support/v7/view/menu/c;)V

    invoke-static {p1}, Landroid/support/v7/view/menu/x;->dKN(Landroid/support/v7/view/menu/p;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/G;->setForceShowIcon(Z)V

    iget v1, p0, Landroid/support/v7/view/menu/b;->esk:I

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/G;->setGravity(I)V

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->esq:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/G;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iput-object v8, p0, Landroid/support/v7/view/menu/b;->esq:Landroid/widget/PopupWindow$OnDismissListener;

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->esh:Landroid/support/v7/view/menu/p;

    invoke-virtual {v1, v7}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v1}, Landroid/support/v7/widget/cF;->dAv()I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v2}, Landroid/support/v7/widget/cF;->dAo()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/view/menu/G;->dLK(II)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esm:Landroid/support/v7/view/menu/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esm:Landroid/support/v7/view/menu/c;

    invoke-interface {v0, p1}, Landroid/support/v7/view/menu/c;->dCC(Landroid/support/v7/view/menu/p;)Z

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    return v7
.end method

.method public dCb()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dCc(Landroid/support/v7/view/menu/p;Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esh:Landroid/support/v7/view/menu/p;

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/view/menu/b;->dismiss()V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esm:Landroid/support/v7/view/menu/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esm:Landroid/support/v7/view/menu/c;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/view/menu/c;->dCD(Landroid/support/v7/view/menu/p;Z)V

    :cond_1
    return-void
.end method

.method public dJA(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/b;->esf:Landroid/view/View;

    return-void
.end method

.method public dJu(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/cF;->dAu(I)V

    return-void
.end method

.method public dJv(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/view/menu/b;->esw:Z

    return-void
.end method

.method public dJw(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/cF;->dAF(I)V

    return-void
.end method

.method public dJx(Landroid/support/v7/view/menu/p;)V
    .locals 0

    return-void
.end method

.method public dismiss()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/b;->dAs()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->dismiss()V

    :cond_0
    return-void
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->getListView()Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method public onDismiss()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/menu/b;->esp:Z

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esh:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/p;->close()V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esi:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->ess:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iput-object v2, p0, Landroid/support/v7/view/menu/b;->est:Landroid/view/ViewTreeObserver;

    :cond_1
    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esi:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/view/menu/b;->esj:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esq:Landroid/widget/PopupWindow$OnDismissListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esq:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    :cond_2
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/16 v0, 0x52

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/b;->dismiss()V

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setForceShowIcon(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/b;->esl:Landroid/support/v7/view/menu/L;

    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/L;->setForceShowIcon(Z)V

    return-void
.end method

.method public setGravity(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/view/menu/b;->esk:I

    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/b;->esq:Landroid/widget/PopupWindow$OnDismissListener;

    return-void
.end method

.method public show()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v7/view/menu/b;->dJB()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "StandardMenuPopup cannot be used without an anchor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
