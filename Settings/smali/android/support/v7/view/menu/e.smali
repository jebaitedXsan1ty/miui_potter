.class Landroid/support/v7/view/menu/e;
.super Landroid/support/v7/view/menu/E;
.source "SubMenuWrapperICS.java"

# interfaces
.implements Landroid/view/SubMenu;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/support/v4/b/a/b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/view/menu/E;-><init>(Landroid/content/Context;Landroid/support/v4/b/a/a;)V

    return-void
.end method


# virtual methods
.method public clearHeader()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/e;->dJD()Landroid/support/v4/b/a/b;

    move-result-object v0

    invoke-interface {v0}, Landroid/support/v4/b/a/b;->clearHeader()V

    return-void
.end method

.method public dJD()Landroid/support/v4/b/a/b;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/e;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/b;

    return-object v0
.end method

.method public getItem()Landroid/view/MenuItem;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/e;->dJD()Landroid/support/v4/b/a/b;

    move-result-object v0

    invoke-interface {v0}, Landroid/support/v4/b/a/b;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/e;->dJE(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/e;->dJD()Landroid/support/v4/b/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/b;->setHeaderIcon(I)Landroid/view/SubMenu;

    return-object p0
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/e;->dJD()Landroid/support/v4/b/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/b;->setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    return-object p0
.end method

.method public setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/e;->dJD()Landroid/support/v4/b/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/b;->setHeaderTitle(I)Landroid/view/SubMenu;

    return-object p0
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/e;->dJD()Landroid/support/v4/b/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/b;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    return-object p0
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/e;->dJD()Landroid/support/v4/b/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/b;->setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;

    return-object p0
.end method

.method public setIcon(I)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/e;->dJD()Landroid/support/v4/b/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/b;->setIcon(I)Landroid/view/SubMenu;

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/e;->dJD()Landroid/support/v4/b/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/b;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    return-object p0
.end method
