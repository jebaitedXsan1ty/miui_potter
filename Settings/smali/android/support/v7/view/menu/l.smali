.class final Landroid/support/v7/view/menu/l;
.super Ljava/lang/Object;
.source "StandardMenuPopup.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic esD:Landroid/support/v7/view/menu/b;


# direct methods
.method constructor <init>(Landroid/support/v7/view/menu/b;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/l;->esD:Landroid/support/v7/view/menu/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/l;->esD:Landroid/support/v7/view/menu/b;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/b;->dAs()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/l;->esD:Landroid/support/v7/view/menu/b;

    iget-object v0, v0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->dAI()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/l;->esD:Landroid/support/v7/view/menu/b;

    iget-object v0, v0, Landroid/support/v7/view/menu/b;->esi:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/l;->esD:Landroid/support/v7/view/menu/b;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/b;->dismiss()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/view/menu/l;->esD:Landroid/support/v7/view/menu/b;

    iget-object v0, v0, Landroid/support/v7/view/menu/b;->eso:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->show()V

    goto :goto_0
.end method
