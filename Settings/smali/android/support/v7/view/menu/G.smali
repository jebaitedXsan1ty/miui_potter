.class public Landroid/support/v7/view/menu/G;
.super Ljava/lang/Object;
.source "MenuPopupHelper.java"


# instance fields
.field private euR:Landroid/view/View;

.field private euS:Landroid/widget/PopupWindow$OnDismissListener;

.field private final euT:Z

.field private final euU:Landroid/support/v7/view/menu/p;

.field private final euV:I

.field private final euW:Landroid/widget/PopupWindow$OnDismissListener;

.field private euX:Z

.field private euY:I

.field private euZ:Landroid/support/v7/view/menu/x;

.field private final eva:I

.field private evb:Landroid/support/v7/view/menu/c;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/view/View;ZI)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/view/menu/G;-><init>(Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/view/View;ZII)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/view/View;ZII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x800003

    iput v0, p0, Landroid/support/v7/view/menu/G;->euY:I

    new-instance v0, Landroid/support/v7/view/menu/f;

    invoke-direct {v0, p0}, Landroid/support/v7/view/menu/f;-><init>(Landroid/support/v7/view/menu/G;)V

    iput-object v0, p0, Landroid/support/v7/view/menu/G;->euW:Landroid/widget/PopupWindow$OnDismissListener;

    iput-object p1, p0, Landroid/support/v7/view/menu/G;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/support/v7/view/menu/G;->euU:Landroid/support/v7/view/menu/p;

    iput-object p3, p0, Landroid/support/v7/view/menu/G;->euR:Landroid/view/View;

    iput-boolean p4, p0, Landroid/support/v7/view/menu/G;->euT:Z

    iput p5, p0, Landroid/support/v7/view/menu/G;->euV:I

    iput p6, p0, Landroid/support/v7/view/menu/G;->eva:I

    return-void
.end method

.method private dLJ(IIZZ)V
    .locals 6

    invoke-virtual {p0}, Landroid/support/v7/view/menu/G;->dLI()Landroid/support/v7/view/menu/x;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/support/v7/view/menu/x;->dJv(Z)V

    if-eqz p3, :cond_1

    iget v1, p0, Landroid/support/v7/view/menu/G;->euY:I

    iget-object v2, p0, Landroid/support/v7/view/menu/G;->euR:Landroid/view/View;

    invoke-static {v2}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v2

    invoke-static {v1, v2}, Landroid/support/v4/view/aj;->dRE(II)I

    move-result v1

    and-int/lit8 v1, v1, 0x7

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Landroid/support/v7/view/menu/G;->euR:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr p1, v1

    :cond_0
    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/x;->dJw(I)V

    invoke-virtual {v0, p2}, Landroid/support/v7/view/menu/x;->dJu(I)V

    iget-object v1, p0, Landroid/support/v7/view/menu/G;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x42400000    # 48.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    new-instance v2, Landroid/graphics/Rect;

    sub-int v3, p1, v1

    sub-int v4, p2, v1

    add-int v5, p1, v1

    add-int/2addr v1, p2

    invoke-direct {v2, v3, v4, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v2}, Landroid/support/v7/view/menu/x;->dKS(Landroid/graphics/Rect;)V

    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/view/menu/x;->show()V

    return-void
.end method

.method private dLN()Landroid/support/v7/view/menu/x;
    .locals 7

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    :goto_0
    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/view/menu/G;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Landroid/support/v7/b/d;->dOT:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    new-instance v0, Landroid/support/v7/view/menu/C;

    iget-object v1, p0, Landroid/support/v7/view/menu/G;->mContext:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/view/menu/G;->euR:Landroid/view/View;

    iget v3, p0, Landroid/support/v7/view/menu/G;->euV:I

    iget v4, p0, Landroid/support/v7/view/menu/G;->eva:I

    iget-boolean v5, p0, Landroid/support/v7/view/menu/G;->euT:Z

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/view/menu/C;-><init>(Landroid/content/Context;Landroid/view/View;IIZ)V

    :goto_2
    iget-object v1, p0, Landroid/support/v7/view/menu/G;->euU:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/x;->dJx(Landroid/support/v7/view/menu/p;)V

    iget-object v1, p0, Landroid/support/v7/view/menu/G;->euW:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/x;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v1, p0, Landroid/support/v7/view/menu/G;->euR:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/x;->dJA(Landroid/view/View;)V

    iget-object v1, p0, Landroid/support/v7/view/menu/G;->evb:Landroid/support/v7/view/menu/c;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/x;->dBV(Landroid/support/v7/view/menu/c;)V

    iget-boolean v1, p0, Landroid/support/v7/view/menu/G;->euX:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/x;->setForceShowIcon(Z)V

    iget v1, p0, Landroid/support/v7/view/menu/G;->euY:I

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/x;->setGravity(I)V

    return-object v0

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    new-instance v0, Landroid/support/v7/view/menu/b;

    iget-object v1, p0, Landroid/support/v7/view/menu/G;->mContext:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/view/menu/G;->euU:Landroid/support/v7/view/menu/p;

    iget-object v3, p0, Landroid/support/v7/view/menu/G;->euR:Landroid/view/View;

    iget v4, p0, Landroid/support/v7/view/menu/G;->euV:I

    iget v5, p0, Landroid/support/v7/view/menu/G;->eva:I

    iget-boolean v6, p0, Landroid/support/v7/view/menu/G;->euT:Z

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/view/menu/b;-><init>(Landroid/content/Context;Landroid/support/v7/view/menu/p;Landroid/view/View;IIZ)V

    goto :goto_2
.end method


# virtual methods
.method public dLI()Landroid/support/v7/view/menu/x;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    if-nez v0, :cond_0

    invoke-direct {p0}, Landroid/support/v7/view/menu/G;->dLN()Landroid/support/v7/view/menu/x;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    return-object v0
.end method

.method public dLK(II)Z
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/G;->dLM()Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euR:Landroid/view/View;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    invoke-direct {p0, p1, p2, v1, v1}, Landroid/support/v7/view/menu/G;->dLJ(IIZZ)V

    return v1
.end method

.method public dLL(Landroid/support/v7/view/menu/c;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/view/menu/G;->evb:Landroid/support/v7/view/menu/c;

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/x;->dBV(Landroid/support/v7/view/menu/c;)V

    :cond_0
    return-void
.end method

.method public dLM()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/x;->dAs()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dLO(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/G;->euR:Landroid/view/View;

    return-void
.end method

.method public dLP()Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/view/menu/G;->dLM()Z

    move-result v0

    if-eqz v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euR:Landroid/view/View;

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-direct {p0, v1, v1, v1, v1}, Landroid/support/v7/view/menu/G;->dLJ(IIZZ)V

    return v2
.end method

.method public dismiss()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/view/menu/G;->dLM()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/x;->dismiss()V

    :cond_0
    return-void
.end method

.method protected onDismiss()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euS:Landroid/widget/PopupWindow$OnDismissListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euS:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    :cond_0
    return-void
.end method

.method public setForceShowIcon(Z)V
    .locals 1

    iput-boolean p1, p0, Landroid/support/v7/view/menu/G;->euX:Z

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/G;->euZ:Landroid/support/v7/view/menu/x;

    invoke-virtual {v0, p1}, Landroid/support/v7/view/menu/x;->setForceShowIcon(Z)V

    :cond_0
    return-void
.end method

.method public setGravity(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/view/menu/G;->euY:I

    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/G;->euS:Landroid/widget/PopupWindow$OnDismissListener;

    return-void
.end method

.method public show()V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/view/menu/G;->dLP()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "MenuPopupHelper cannot be used without an anchor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
