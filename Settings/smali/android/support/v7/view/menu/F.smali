.class public Landroid/support/v7/view/menu/F;
.super Landroid/support/v7/view/menu/g;
.source "MenuItemWrapperICS.java"

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field private euB:Ljava/lang/reflect/Method;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/support/v4/b/a/c;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/view/menu/g;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public collapseActionView()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->collapseActionView()Z

    move-result v0

    return v0
.end method

.method dKM(Landroid/view/ActionProvider;)Landroid/support/v7/view/menu/D;
    .locals 2

    new-instance v0, Landroid/support/v7/view/menu/D;

    iget-object v1, p0, Landroid/support/v7/view/menu/F;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p1}, Landroid/support/v7/view/menu/D;-><init>(Landroid/support/v7/view/menu/F;Landroid/content/Context;Landroid/view/ActionProvider;)V

    return-object v0
.end method

.method public dLE(Z)V
    .locals 5

    :try_start_0
    iget-object v0, p0, Landroid/support/v7/view/menu/F;->euB:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "setExclusiveCheckable"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/menu/F;->euB:Ljava/lang/reflect/Method;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/F;->euB:Ljava/lang/reflect/Method;

    iget-object v1, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "MenuItemWrapper"

    const-string/jumbo v2, "Error while calling setExclusiveCheckable"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public expandActionView()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->expandActionView()Z

    move-result v0

    return v0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->dJs()Landroid/support/v4/view/ai;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/view/menu/D;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v7/view/menu/D;

    iget-object v0, v0, Landroid/support/v7/view/menu/D;->euz:Landroid/view/ActionProvider;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getActionView()Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/view/menu/N;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v7/view/menu/N;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/N;->dLV()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public getAlphabeticModifiers()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getAlphabeticModifiers()I

    move-result v0

    return v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getAlphabeticShortcut()C

    move-result v0

    return v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getGroupId()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getGroupId()I

    move-result v0

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getIconTintList()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getIconTintList()Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public getIconTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getIconTintMode()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getItemId()I

    move-result v0

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    return-object v0
.end method

.method public getNumericModifiers()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getNumericModifiers()I

    move-result v0

    return v0
.end method

.method public getNumericShortcut()C
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getNumericShortcut()C

    move-result v0

    return v0
.end method

.method public getOrder()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getOrder()I

    move-result v0

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/view/menu/F;->dJH(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getTitleCondensed()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTooltipText()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getTooltipText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public hasSubMenu()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->hasSubMenu()Z

    move-result v0

    return v0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->isActionViewExpanded()Z

    move-result v0

    return v0
.end method

.method public isCheckable()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->isCheckable()Z

    move-result v0

    return v0
.end method

.method public isChecked()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->isChecked()Z

    move-result v0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->isVisible()Z

    move-result v0

    return v0
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v7/view/menu/F;->dKM(Landroid/view/ActionProvider;)Landroid/support/v7/view/menu/D;

    move-result-object v1

    :cond_0
    invoke-interface {v0, v1}, Landroid/support/v4/b/a/c;->dJt(Landroid/support/v4/view/ai;)Landroid/support/v4/b/a/c;

    return-object p0
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 3

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setActionView(I)Landroid/view/MenuItem;

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0}, Landroid/support/v4/b/a/c;->getActionView()Landroid/view/View;

    move-result-object v1

    instance-of v0, v1, Landroid/view/CollapsibleActionView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    new-instance v2, Landroid/support/v7/view/menu/N;

    invoke-direct {v2, v1}, Landroid/support/v7/view/menu/N;-><init>(Landroid/view/View;)V

    invoke-interface {v0, v2}, Landroid/support/v4/b/a/c;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    :cond_0
    return-object p0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    instance-of v0, p1, Landroid/view/CollapsibleActionView;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/v7/view/menu/N;

    invoke-direct {v0, p1}, Landroid/support/v7/view/menu/N;-><init>(Landroid/view/View;)V

    move-object p1, v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setAlphabeticShortcut(CI)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/b/a/c;->setAlphabeticShortcut(CI)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setCheckable(Z)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setChecked(Z)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setContentDescription(Ljava/lang/CharSequence;)Landroid/support/v4/b/a/c;

    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setEnabled(Z)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setIcon(I)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setIconTintList(Landroid/content/res/ColorStateList;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setIconTintList(Landroid/content/res/ColorStateList;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setIconTintMode(Landroid/graphics/PorterDuff$Mode;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setIconTintMode(Landroid/graphics/PorterDuff$Mode;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setNumericShortcut(C)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setNumericShortcut(CI)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/b/a/c;->setNumericShortcut(CI)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    if-eqz p1, :cond_0

    new-instance v1, Landroid/support/v7/view/menu/A;

    invoke-direct {v1, p0, p1}, Landroid/support/v7/view/menu/A;-><init>(Landroid/support/v7/view/menu/F;Landroid/view/MenuItem$OnActionExpandListener;)V

    :cond_0
    invoke-interface {v0, v1}, Landroid/support/v4/b/a/c;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    if-eqz p1, :cond_0

    new-instance v1, Landroid/support/v7/view/menu/K;

    invoke-direct {v1, p0, p1}, Landroid/support/v7/view/menu/K;-><init>(Landroid/support/v7/view/menu/F;Landroid/view/MenuItem$OnMenuItemClickListener;)V

    :cond_0
    invoke-interface {v0, v1}, Landroid/support/v4/b/a/c;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/b/a/c;->setShortcut(CC)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setShortcut(CCII)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/support/v4/b/a/c;->setShortcut(CCII)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setShowAsAction(I)V

    return-void
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setTitle(I)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setTooltipText(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setTooltipText(Ljava/lang/CharSequence;)Landroid/support/v4/b/a/c;

    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/view/menu/F;->etZ:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/b/a/c;

    invoke-interface {v0, p1}, Landroid/support/v4/b/a/c;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method
