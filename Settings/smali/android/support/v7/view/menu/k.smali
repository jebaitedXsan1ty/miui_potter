.class final Landroid/support/v7/view/menu/k;
.super Ljava/lang/Object;
.source "CascadingMenuPopup.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic esC:Landroid/support/v7/view/menu/C;


# direct methods
.method constructor <init>(Landroid/support/v7/view/menu/C;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/k;->esC:Landroid/support/v7/view/menu/C;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/k;->esC:Landroid/support/v7/view/menu/C;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/C;->dAs()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/k;->esC:Landroid/support/v7/view/menu/C;

    iget-object v0, v0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/k;->esC:Landroid/support/v7/view/menu/C;

    iget-object v0, v0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget-object v0, v0, Landroid/support/v7/view/menu/u;->ets:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->dAI()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/k;->esC:Landroid/support/v7/view/menu/C;

    iget-object v0, v0, Landroid/support/v7/view/menu/C;->eue:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/k;->esC:Landroid/support/v7/view/menu/C;

    invoke-virtual {v0}, Landroid/support/v7/view/menu/C;->dismiss()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/view/menu/k;->esC:Landroid/support/v7/view/menu/C;

    iget-object v0, v0, Landroid/support/v7/view/menu/C;->euu:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/view/menu/u;

    iget-object v0, v0, Landroid/support/v7/view/menu/u;->ets:Landroid/support/v7/widget/cF;

    invoke-virtual {v0}, Landroid/support/v7/widget/cF;->show()V

    goto :goto_0
.end method
