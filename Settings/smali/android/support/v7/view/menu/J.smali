.class final Landroid/support/v7/view/menu/J;
.super Ljava/lang/Object;
.source "CascadingMenuPopup.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic evp:Landroid/view/MenuItem;

.field final synthetic evq:Landroid/support/v7/view/menu/O;

.field final synthetic evr:Landroid/support/v7/view/menu/p;

.field final synthetic evs:Landroid/support/v7/view/menu/u;


# direct methods
.method constructor <init>(Landroid/support/v7/view/menu/O;Landroid/support/v7/view/menu/u;Landroid/view/MenuItem;Landroid/support/v7/view/menu/p;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/view/menu/J;->evq:Landroid/support/v7/view/menu/O;

    iput-object p2, p0, Landroid/support/v7/view/menu/J;->evs:Landroid/support/v7/view/menu/u;

    iput-object p3, p0, Landroid/support/v7/view/menu/J;->evp:Landroid/view/MenuItem;

    iput-object p4, p0, Landroid/support/v7/view/menu/J;->evr:Landroid/support/v7/view/menu/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/view/menu/J;->evs:Landroid/support/v7/view/menu/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/view/menu/J;->evq:Landroid/support/v7/view/menu/O;

    iget-object v0, v0, Landroid/support/v7/view/menu/O;->evB:Landroid/support/v7/view/menu/C;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/view/menu/C;->euk:Z

    iget-object v0, p0, Landroid/support/v7/view/menu/J;->evs:Landroid/support/v7/view/menu/u;

    iget-object v0, v0, Landroid/support/v7/view/menu/u;->etq:Landroid/support/v7/view/menu/p;

    invoke-virtual {v0, v2}, Landroid/support/v7/view/menu/p;->dKA(Z)V

    iget-object v0, p0, Landroid/support/v7/view/menu/J;->evq:Landroid/support/v7/view/menu/O;

    iget-object v0, v0, Landroid/support/v7/view/menu/O;->evB:Landroid/support/v7/view/menu/C;

    iput-boolean v2, v0, Landroid/support/v7/view/menu/C;->euk:Z

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/menu/J;->evp:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/J;->evp:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/view/menu/J;->evr:Landroid/support/v7/view/menu/p;

    iget-object v1, p0, Landroid/support/v7/view/menu/J;->evp:Landroid/view/MenuItem;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/view/menu/p;->dKp(Landroid/view/MenuItem;I)Z

    :cond_1
    return-void
.end method
