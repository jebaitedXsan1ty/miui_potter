.class Landroid/support/v7/view/f;
.super Ljava/lang/Object;
.source "SupportMenuInflater.java"


# instance fields
.field private evP:Z

.field private evQ:Ljava/lang/String;

.field private evR:Z

.field private evS:I

.field private evT:I

.field private evU:Z

.field private evV:Landroid/content/res/ColorStateList;

.field evW:Landroid/support/v4/view/ai;

.field private evX:I

.field private evY:I

.field private evZ:Z

.field final synthetic ewa:Landroid/support/v7/view/b;

.field private ewb:I

.field private ewc:C

.field private ewd:I

.field private ewe:I

.field private ewf:Landroid/graphics/PorterDuff$Mode;

.field private ewg:Ljava/lang/CharSequence;

.field private ewh:I

.field private ewi:Ljava/lang/String;

.field private ewj:I

.field private ewk:Z

.field private ewl:Ljava/lang/CharSequence;

.field private ewm:I

.field private ewn:Z

.field private ewo:I

.field private ewp:Landroid/view/Menu;

.field private ewq:Ljava/lang/CharSequence;

.field private ewr:I

.field private ews:C

.field private ewt:Ljava/lang/String;

.field private ewu:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/support/v7/view/b;Landroid/view/Menu;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Landroid/support/v7/view/f;->ewa:Landroid/support/v7/view/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/support/v7/view/f;->evV:Landroid/content/res/ColorStateList;

    iput-object v0, p0, Landroid/support/v7/view/f;->ewf:Landroid/graphics/PorterDuff$Mode;

    iput-object p2, p0, Landroid/support/v7/view/f;->ewp:Landroid/view/Menu;

    invoke-virtual {p0}, Landroid/support/v7/view/f;->dMp()V

    return-void
.end method

.method private dMj(Ljava/lang/String;)C
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    return v0
.end method

.method private dMk(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    :try_start_0
    iget-object v0, p0, Landroid/support/v7/view/f;->ewa:Landroid/support/v7/view/b;

    iget-object v0, v0, Landroid/support/v7/view/b;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    invoke-virtual {v0, p3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "SupportMenuInflater"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Cannot instantiate class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method private dMl(Landroid/view/MenuItem;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Landroid/support/v7/view/f;->evZ:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Landroid/support/v7/view/f;->ewk:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Landroid/support/v7/view/f;->ewn:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v3

    iget v0, p0, Landroid/support/v7/view/f;->ewr:I

    if-lt v0, v1, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v3, p0, Landroid/support/v7/view/f;->ewg:Ljava/lang/CharSequence;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iget v3, p0, Landroid/support/v7/view/f;->ewh:I

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget v0, p0, Landroid/support/v7/view/f;->evX:I

    if-ltz v0, :cond_0

    iget v0, p0, Landroid/support/v7/view/f;->evX:I

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setShowAsAction(I)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/view/f;->ewi:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/view/f;->ewa:Landroid/support/v7/view/b;

    iget-object v0, v0, Landroid/support/v7/view/b;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->isRestricted()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "The android:onClick attribute cannot be used within a restricted context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/support/v7/view/d;

    iget-object v3, p0, Landroid/support/v7/view/f;->ewa:Landroid/support/v7/view/b;

    invoke-virtual {v3}, Landroid/support/v7/view/b;->dMf()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/view/f;->ewi:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Landroid/support/v7/view/d;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_3
    instance-of v0, p1, Landroid/support/v7/view/menu/z;

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Landroid/support/v7/view/menu/z;

    :cond_4
    iget v0, p0, Landroid/support/v7/view/f;->ewr:I

    const/4 v3, 0x2

    if-lt v0, v3, :cond_5

    instance-of v0, p1, Landroid/support/v7/view/menu/z;

    if-eqz v0, :cond_a

    move-object v0, p1

    check-cast v0, Landroid/support/v7/view/menu/z;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/z;->dLi(Z)V

    :cond_5
    :goto_1
    iget-object v0, p0, Landroid/support/v7/view/f;->ewt:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/view/f;->ewt:Ljava/lang/String;

    sget-object v2, Landroid/support/v7/view/b;->evD:[Ljava/lang/Class;

    iget-object v3, p0, Landroid/support/v7/view/f;->ewa:Landroid/support/v7/view/b;

    iget-object v3, v3, Landroid/support/v7/view/b;->evF:[Ljava/lang/Object;

    invoke-direct {p0, v0, v2, v3}, Landroid/support/v7/view/f;->dMk(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    :goto_2
    iget v0, p0, Landroid/support/v7/view/f;->evT:I

    if-lez v0, :cond_6

    if-nez v1, :cond_b

    iget v0, p0, Landroid/support/v7/view/f;->evT:I

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setActionView(I)Landroid/view/MenuItem;

    :cond_6
    :goto_3
    iget-object v0, p0, Landroid/support/v7/view/f;->evW:Landroid/support/v4/view/ai;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/view/f;->evW:Landroid/support/v4/view/ai;

    invoke-static {p1, v0}, Landroid/support/v4/view/aa;->dRu(Landroid/view/MenuItem;Landroid/support/v4/view/ai;)Landroid/view/MenuItem;

    :cond_7
    iget-object v0, p0, Landroid/support/v7/view/f;->ewu:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/support/v4/view/aa;->dRr(Landroid/view/MenuItem;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Landroid/support/v7/view/f;->ewq:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/support/v4/view/aa;->dRt(Landroid/view/MenuItem;Ljava/lang/CharSequence;)V

    iget-char v0, p0, Landroid/support/v7/view/f;->ews:C

    iget v1, p0, Landroid/support/v7/view/f;->evY:I

    invoke-static {p1, v0, v1}, Landroid/support/v4/view/aa;->dRv(Landroid/view/MenuItem;CI)V

    iget-char v0, p0, Landroid/support/v7/view/f;->ewc:C

    iget v1, p0, Landroid/support/v7/view/f;->ewo:I

    invoke-static {p1, v0, v1}, Landroid/support/v4/view/aa;->dRw(Landroid/view/MenuItem;CI)V

    iget-object v0, p0, Landroid/support/v7/view/f;->ewf:Landroid/graphics/PorterDuff$Mode;

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v7/view/f;->ewf:Landroid/graphics/PorterDuff$Mode;

    invoke-static {p1, v0}, Landroid/support/v4/view/aa;->dRx(Landroid/view/MenuItem;Landroid/graphics/PorterDuff$Mode;)V

    :cond_8
    iget-object v0, p0, Landroid/support/v7/view/f;->evV:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/view/f;->evV:Landroid/content/res/ColorStateList;

    invoke-static {p1, v0}, Landroid/support/v4/view/aa;->dRs(Landroid/view/MenuItem;Landroid/content/res/ColorStateList;)V

    :cond_9
    return-void

    :cond_a
    instance-of v0, p1, Landroid/support/v7/view/menu/F;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Landroid/support/v7/view/menu/F;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/F;->dLE(Z)V

    goto :goto_1

    :cond_b
    const-string/jumbo v0, "SupportMenuInflater"

    const-string/jumbo v1, "Ignoring attribute \'itemActionViewLayout\'. Action view already specified."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_c
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public dMi(Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/view/f;->ewa:Landroid/support/v7/view/b;

    iget-object v0, v0, Landroid/support/v7/view/b;->mContext:Landroid/content/Context;

    sget-object v1, Landroid/support/v7/b/j;->dSW:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Landroid/support/v7/b/j;->dSZ:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/view/f;->ewd:I

    sget v1, Landroid/support/v7/b/j;->dTa:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/view/f;->ewj:I

    sget v1, Landroid/support/v7/b/j;->dTb:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/view/f;->ewm:I

    sget v1, Landroid/support/v7/b/j;->dSX:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/view/f;->ewb:I

    sget v1, Landroid/support/v7/b/j;->dTc:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/view/f;->evU:Z

    sget v1, Landroid/support/v7/b/j;->dSY:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/view/f;->evP:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public dMm(Landroid/util/AttributeSet;)V
    .locals 9

    const/16 v8, 0x1000

    const/4 v1, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/view/f;->ewa:Landroid/support/v7/view/b;

    iget-object v0, v0, Landroid/support/v7/view/b;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/support/v7/b/j;->dTd:[I

    invoke-virtual {v0, p1, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    sget v0, Landroid/support/v7/b/j;->dTn:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/f;->ewe:I

    sget v0, Landroid/support/v7/b/j;->dTo:I

    iget v4, p0, Landroid/support/v7/view/f;->ewj:I

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    sget v4, Landroid/support/v7/b/j;->dTr:I

    iget v5, p0, Landroid/support/v7/view/f;->ewm:I

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    const/high16 v5, -0x10000

    and-int/2addr v0, v5

    const v5, 0xffff

    and-int/2addr v4, v5

    or-int/2addr v0, v4

    iput v0, p0, Landroid/support/v7/view/f;->evS:I

    sget v0, Landroid/support/v7/b/j;->dTs:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/f;->ewl:Ljava/lang/CharSequence;

    sget v0, Landroid/support/v7/b/j;->dTt:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/f;->ewg:Ljava/lang/CharSequence;

    sget v0, Landroid/support/v7/b/j;->dTm:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/f;->ewh:I

    sget v0, Landroid/support/v7/b/j;->dTi:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/view/f;->dMj(Ljava/lang/String;)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/view/f;->ews:C

    sget v0, Landroid/support/v7/b/j;->dTh:I

    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/f;->evY:I

    sget v0, Landroid/support/v7/b/j;->dTp:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/view/f;->dMj(Ljava/lang/String;)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/view/f;->ewc:C

    sget v0, Landroid/support/v7/b/j;->dTy:I

    invoke-virtual {v3, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/f;->ewo:I

    sget v0, Landroid/support/v7/b/j;->dTj:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Landroid/support/v7/b/j;->dTj:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Landroid/support/v7/view/f;->ewr:I

    :goto_1
    sget v0, Landroid/support/v7/b/j;->dTk:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/view/f;->evZ:Z

    sget v0, Landroid/support/v7/b/j;->dTu:I

    iget-boolean v4, p0, Landroid/support/v7/view/f;->evU:Z

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/view/f;->ewk:Z

    sget v0, Landroid/support/v7/b/j;->dTl:I

    iget-boolean v4, p0, Landroid/support/v7/view/f;->evP:Z

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/view/f;->ewn:Z

    sget v0, Landroid/support/v7/b/j;->dTz:I

    invoke-virtual {v3, v0, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/f;->evX:I

    sget v0, Landroid/support/v7/b/j;->dTq:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/f;->ewi:Ljava/lang/String;

    sget v0, Landroid/support/v7/b/j;->dTe:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/view/f;->evT:I

    sget v0, Landroid/support/v7/b/j;->dTg:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/f;->ewt:Ljava/lang/String;

    sget v0, Landroid/support/v7/b/j;->dTf:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/f;->evQ:Ljava/lang/String;

    iget-object v0, p0, Landroid/support/v7/view/f;->evQ:Ljava/lang/String;

    if-eqz v0, :cond_2

    :goto_2
    if-eqz v1, :cond_3

    iget v0, p0, Landroid/support/v7/view/f;->evT:I

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/view/f;->ewt:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/view/f;->evQ:Ljava/lang/String;

    sget-object v1, Landroid/support/v7/view/b;->evC:[Ljava/lang/Class;

    iget-object v4, p0, Landroid/support/v7/view/f;->ewa:Landroid/support/v7/view/b;

    iget-object v4, v4, Landroid/support/v7/view/b;->evG:[Ljava/lang/Object;

    invoke-direct {p0, v0, v1, v4}, Landroid/support/v7/view/f;->dMk(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ai;

    iput-object v0, p0, Landroid/support/v7/view/f;->evW:Landroid/support/v4/view/ai;

    :goto_3
    sget v0, Landroid/support/v7/b/j;->dTv:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/f;->ewu:Ljava/lang/CharSequence;

    sget v0, Landroid/support/v7/b/j;->dTA:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/f;->ewq:Ljava/lang/CharSequence;

    sget v0, Landroid/support/v7/b/j;->dTx:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Landroid/support/v7/b/j;->dTx:I

    invoke-virtual {v3, v0, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/view/f;->ewf:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v0, v1}, Landroid/support/v7/widget/cx;->dHt(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/f;->ewf:Landroid/graphics/PorterDuff$Mode;

    :goto_4
    sget v0, Landroid/support/v7/b/j;->dTw:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_6

    sget v0, Landroid/support/v7/b/j;->dTw:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/view/f;->evV:Landroid/content/res/ColorStateList;

    :goto_5
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    iput-boolean v2, p0, Landroid/support/v7/view/f;->evR:Z

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    iget v0, p0, Landroid/support/v7/view/f;->ewb:I

    iput v0, p0, Landroid/support/v7/view/f;->ewr:I

    goto/16 :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_4

    const-string/jumbo v0, "SupportMenuInflater"

    const-string/jumbo v1, "Ignoring attribute \'actionProviderClass\'. Action view already specified."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iput-object v6, p0, Landroid/support/v7/view/f;->evW:Landroid/support/v4/view/ai;

    goto :goto_3

    :cond_5
    iput-object v6, p0, Landroid/support/v7/view/f;->ewf:Landroid/graphics/PorterDuff$Mode;

    goto :goto_4

    :cond_6
    iput-object v6, p0, Landroid/support/v7/view/f;->evV:Landroid/content/res/ColorStateList;

    goto :goto_5
.end method

.method public dMn()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/view/f;->evR:Z

    return v0
.end method

.method public dMo()Landroid/view/SubMenu;
    .locals 5

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/f;->evR:Z

    iget-object v0, p0, Landroid/support/v7/view/f;->ewp:Landroid/view/Menu;

    iget v1, p0, Landroid/support/v7/view/f;->ewd:I

    iget v2, p0, Landroid/support/v7/view/f;->ewe:I

    iget v3, p0, Landroid/support/v7/view/f;->evS:I

    iget-object v4, p0, Landroid/support/v7/view/f;->ewl:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/support/v7/view/f;->dMl(Landroid/view/MenuItem;)V

    return-object v0
.end method

.method public dMp()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/view/f;->ewd:I

    iput v0, p0, Landroid/support/v7/view/f;->ewj:I

    iput v0, p0, Landroid/support/v7/view/f;->ewm:I

    iput v0, p0, Landroid/support/v7/view/f;->ewb:I

    iput-boolean v1, p0, Landroid/support/v7/view/f;->evU:Z

    iput-boolean v1, p0, Landroid/support/v7/view/f;->evP:Z

    return-void
.end method

.method public dMq()V
    .locals 5

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/view/f;->evR:Z

    iget-object v0, p0, Landroid/support/v7/view/f;->ewp:Landroid/view/Menu;

    iget v1, p0, Landroid/support/v7/view/f;->ewd:I

    iget v2, p0, Landroid/support/v7/view/f;->ewe:I

    iget v3, p0, Landroid/support/v7/view/f;->evS:I

    iget-object v4, p0, Landroid/support/v7/view/f;->ewl:Ljava/lang/CharSequence;

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/view/f;->dMl(Landroid/view/MenuItem;)V

    return-void
.end method
