.class public abstract Landroid/support/v7/widget/q;
.super Ljava/lang/Object;
.source "RecyclerView.java"


# instance fields
.field private eai:Z

.field private eaj:Landroid/view/View;

.field private eak:Landroid/support/v7/widget/a;

.field private eal:Z

.field private eam:I

.field private final ean:Landroid/support/v7/widget/R;

.field private eao:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/q;->eam:I

    new-instance v0, Landroid/support/v7/widget/R;

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/R;-><init>(II)V

    iput-object v0, p0, Landroid/support/v7/widget/q;->ean:Landroid/support/v7/widget/R;

    return-void
.end method

.method private duA(II)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/q;->eao:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v1, p0, Landroid/support/v7/widget/q;->eai:Z

    if-eqz v1, :cond_0

    iget v1, p0, Landroid/support/v7/widget/q;->eam:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/q;->stop()V

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v7/widget/q;->eal:Z

    iget-object v1, p0, Landroid/support/v7/widget/q;->eaj:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/q;->eaj:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/q;->duG(Landroid/view/View;)I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/q;->eam:I

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Landroid/support/v7/widget/q;->eaj:Landroid/view/View;

    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v3, p0, Landroid/support/v7/widget/q;->ean:Landroid/support/v7/widget/R;

    invoke-virtual {p0, v1, v2, v3}, Landroid/support/v7/widget/q;->duB(Landroid/view/View;Landroid/support/v7/widget/e;Landroid/support/v7/widget/R;)V

    iget-object v1, p0, Landroid/support/v7/widget/q;->ean:Landroid/support/v7/widget/R;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/R;->dxl(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/q;->stop()V

    :cond_2
    :goto_1
    iget-boolean v1, p0, Landroid/support/v7/widget/q;->eai:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v2, p0, Landroid/support/v7/widget/q;->ean:Landroid/support/v7/widget/R;

    invoke-virtual {p0, p1, p2, v1, v2}, Landroid/support/v7/widget/q;->duC(IILandroid/support/v7/widget/e;Landroid/support/v7/widget/R;)V

    iget-object v1, p0, Landroid/support/v7/widget/q;->ean:Landroid/support/v7/widget/R;

    invoke-virtual {v1}, Landroid/support/v7/widget/R;->dxm()Z

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/q;->ean:Landroid/support/v7/widget/R;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/R;->dxl(Landroid/support/v7/widget/RecyclerView;)V

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Landroid/support/v7/widget/q;->eai:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v7/widget/q;->eal:Z

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYB:Landroid/support/v7/widget/g;

    invoke-virtual {v0}, Landroid/support/v7/widget/g;->dty()V

    :cond_3
    :goto_2
    return-void

    :cond_4
    if-nez v0, :cond_1

    goto :goto_0

    :cond_5
    const-string/jumbo v1, "RecyclerView"

    const-string/jumbo v2, "Passed over target position while smooth scrolling."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v3, p0, Landroid/support/v7/widget/q;->eaj:Landroid/view/View;

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Landroid/support/v7/widget/q;->stop()V

    goto :goto_2
.end method

.method static synthetic duH(Landroid/support/v7/widget/q;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/q;->duA(II)V

    return-void
.end method


# virtual methods
.method protected abstract duB(Landroid/view/View;Landroid/support/v7/widget/e;Landroid/support/v7/widget/R;)V
.end method

.method protected abstract duC(IILandroid/support/v7/widget/e;Landroid/support/v7/widget/R;)V
.end method

.method public duD()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/q;->eam:I

    return v0
.end method

.method public duE(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/q;->eam:I

    return-void
.end method

.method protected duF(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/q;->duG(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/q;->duD()I

    move-result v1

    if-ne v0, v1, :cond_0

    iput-object p1, p0, Landroid/support/v7/widget/q;->eaj:Landroid/view/View;

    :cond_0
    return-void
.end method

.method public duG(Landroid/view/View;)I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/q;->eao:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->dqy(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public duI()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/q;->eal:Z

    return v0
.end method

.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/q;->eai:Z

    return v0
.end method

.method protected abstract onStop()V
.end method

.method protected final stop()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Landroid/support/v7/widget/q;->eai:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/q;->onStop()V

    iget-object v0, p0, Landroid/support/v7/widget/q;->eao:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-static {v0, v2}, Landroid/support/v7/widget/e;->dto(Landroid/support/v7/widget/e;I)I

    iput-object v1, p0, Landroid/support/v7/widget/q;->eaj:Landroid/view/View;

    iput v2, p0, Landroid/support/v7/widget/q;->eam:I

    iput-boolean v3, p0, Landroid/support/v7/widget/q;->eal:Z

    iput-boolean v3, p0, Landroid/support/v7/widget/q;->eai:Z

    iget-object v0, p0, Landroid/support/v7/widget/q;->eak:Landroid/support/v7/widget/a;

    invoke-static {v0, p0}, Landroid/support/v7/widget/a;->dsv(Landroid/support/v7/widget/a;Landroid/support/v7/widget/q;)V

    iput-object v1, p0, Landroid/support/v7/widget/q;->eak:Landroid/support/v7/widget/a;

    iput-object v1, p0, Landroid/support/v7/widget/q;->eao:Landroid/support/v7/widget/RecyclerView;

    return-void
.end method
