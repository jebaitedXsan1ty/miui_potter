.class final Landroid/support/v7/widget/ag;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DefaultItemAnimator.java"


# instance fields
.field final synthetic eca:Landroid/support/v7/widget/Z;

.field final synthetic ecb:Landroid/support/v7/widget/p;

.field final synthetic ecc:Landroid/view/View;

.field final synthetic ecd:Landroid/view/ViewPropertyAnimator;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/Z;Landroid/support/v7/widget/p;Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/ag;->eca:Landroid/support/v7/widget/Z;

    iput-object p2, p0, Landroid/support/v7/widget/ag;->ecb:Landroid/support/v7/widget/p;

    iput-object p3, p0, Landroid/support/v7/widget/ag;->ecd:Landroid/view/ViewPropertyAnimator;

    iput-object p4, p0, Landroid/support/v7/widget/ag;->ecc:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ag;->ecd:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Landroid/support/v7/widget/ag;->ecc:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Landroid/support/v7/widget/ag;->eca:Landroid/support/v7/widget/Z;

    iget-object v1, p0, Landroid/support/v7/widget/ag;->ecb:Landroid/support/v7/widget/p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Z;->dxN(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/ag;->eca:Landroid/support/v7/widget/Z;

    iget-object v0, v0, Landroid/support/v7/widget/Z;->ebA:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/ag;->ecb:Landroid/support/v7/widget/p;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/ag;->eca:Landroid/support/v7/widget/Z;

    invoke-virtual {v0}, Landroid/support/v7/widget/Z;->dxw()V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ag;->eca:Landroid/support/v7/widget/Z;

    iget-object v1, p0, Landroid/support/v7/widget/ag;->ecb:Landroid/support/v7/widget/p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Z;->dxI(Landroid/support/v7/widget/p;)V

    return-void
.end method
