.class public Landroid/support/v7/widget/R;
.super Ljava/lang/Object;
.source "RecyclerView.java"


# instance fields
.field private ebl:Z

.field private ebm:I

.field private ebn:I

.field private ebo:Landroid/view/animation/Interpolator;

.field private ebp:I

.field private ebq:I

.field private ebr:I


# direct methods
.method public constructor <init>(II)V
    .locals 2

    const/high16 v0, -0x80000000

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/v7/widget/R;-><init>(IIILandroid/view/animation/Interpolator;)V

    return-void
.end method

.method public constructor <init>(IIILandroid/view/animation/Interpolator;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/R;->ebn:I

    iput-boolean v1, p0, Landroid/support/v7/widget/R;->ebl:Z

    iput v1, p0, Landroid/support/v7/widget/R;->ebp:I

    iput p1, p0, Landroid/support/v7/widget/R;->ebq:I

    iput p2, p0, Landroid/support/v7/widget/R;->ebr:I

    iput p3, p0, Landroid/support/v7/widget/R;->ebm:I

    iput-object p4, p0, Landroid/support/v7/widget/R;->ebo:Landroid/view/animation/Interpolator;

    return-void
.end method

.method private dxk()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/R;->ebo:Landroid/view/animation/Interpolator;

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/R;->ebm:I

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "If you provide an interpolator, you must set a positive duration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/R;->ebm:I

    if-ge v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Scroll duration must be a positive number"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method dxl(Landroid/support/v7/widget/RecyclerView;)V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Landroid/support/v7/widget/R;->ebn:I

    if-ltz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/R;->ebn:I

    const/4 v1, -0x1

    iput v1, p0, Landroid/support/v7/widget/R;->ebn:I

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->dpO(I)V

    iput-boolean v5, p0, Landroid/support/v7/widget/R;->ebl:Z

    return-void

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/R;->ebl:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Landroid/support/v7/widget/R;->dxk()V

    iget-object v0, p0, Landroid/support/v7/widget/R;->ebo:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_3

    iget v0, p0, Landroid/support/v7/widget/R;->ebm:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_2

    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->dYB:Landroid/support/v7/widget/g;

    iget v1, p0, Landroid/support/v7/widget/R;->ebq:I

    iget v2, p0, Landroid/support/v7/widget/R;->ebr:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/g;->dtv(II)V

    :goto_0
    iget v0, p0, Landroid/support/v7/widget/R;->ebp:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/R;->ebp:I

    iget v0, p0, Landroid/support/v7/widget/R;->ebp:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_1

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iput-boolean v5, p0, Landroid/support/v7/widget/R;->ebl:Z

    :goto_1
    return-void

    :cond_2
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->dYB:Landroid/support/v7/widget/g;

    iget v1, p0, Landroid/support/v7/widget/R;->ebq:I

    iget v2, p0, Landroid/support/v7/widget/R;->ebr:I

    iget v3, p0, Landroid/support/v7/widget/R;->ebm:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/g;->dtA(III)V

    goto :goto_0

    :cond_3
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->dYB:Landroid/support/v7/widget/g;

    iget v1, p0, Landroid/support/v7/widget/R;->ebq:I

    iget v2, p0, Landroid/support/v7/widget/R;->ebr:I

    iget v3, p0, Landroid/support/v7/widget/R;->ebm:I

    iget-object v4, p0, Landroid/support/v7/widget/R;->ebo:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/g;->dts(IIILandroid/view/animation/Interpolator;)V

    goto :goto_0

    :cond_4
    iput v5, p0, Landroid/support/v7/widget/R;->ebp:I

    goto :goto_1
.end method

.method dxm()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/widget/R;->ebn:I

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
