.class final Landroid/support/v7/widget/B;
.super Ljava/lang/Object;
.source "RecyclerView.java"

# interfaces
.implements Landroid/support/v7/widget/X;


# instance fields
.field final synthetic eaB:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dvA(II)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->dpU(II)V

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->dYS:Z

    return-void
.end method

.method public dvB(IILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->dqE(IILjava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->dYo:Z

    return-void
.end method

.method public dvC(I)Landroid/support/v7/widget/p;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/RecyclerView;->dpk(IZ)Landroid/support/v7/widget/p;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v3

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/G;->dvL(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-object v3

    :cond_1
    return-object v0
.end method

.method public dvD(II)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v7/widget/RecyclerView;->dqA(IIZ)V

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->dYS:Z

    return-void
.end method

.method public dvE(Landroid/support/v7/widget/Y;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/B;->dvw(Landroid/support/v7/widget/Y;)V

    return-void
.end method

.method dvw(Landroid/support/v7/widget/Y;)V
    .locals 5

    iget v0, p1, Landroid/support/v7/widget/Y;->ebs:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget v2, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v3, p1, Landroid/support/v7/widget/Y;->ebv:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/a;->dsy(Landroid/support/v7/widget/RecyclerView;II)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget v2, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v3, p1, Landroid/support/v7/widget/Y;->ebv:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/a;->dsN(Landroid/support/v7/widget/RecyclerView;II)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget v2, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v3, p1, Landroid/support/v7/widget/Y;->ebv:I

    iget-object v4, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/a;->dsY(Landroid/support/v7/widget/RecyclerView;IILjava/lang/Object;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget v2, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v3, p1, Landroid/support/v7/widget/Y;->ebv:I

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/a;->dru(Landroid/support/v7/widget/RecyclerView;III)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public dvx(II)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->dqp(II)V

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->dYS:Z

    return-void
.end method

.method public dvy(II)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v7/widget/RecyclerView;->dqA(IIZ)V

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->dYS:Z

    iget-object v0, p0, Landroid/support/v7/widget/B;->eaB:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget v1, v0, Landroid/support/v7/widget/e;->dZv:I

    add-int/2addr v1, p2

    iput v1, v0, Landroid/support/v7/widget/e;->dZv:I

    return-void
.end method

.method public dvz(Landroid/support/v7/widget/Y;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/B;->dvw(Landroid/support/v7/widget/Y;)V

    return-void
.end method
