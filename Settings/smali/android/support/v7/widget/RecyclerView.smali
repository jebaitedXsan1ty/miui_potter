.class public Landroid/support/v7/widget/RecyclerView;
.super Landroid/view/ViewGroup;
.source "RecyclerView.java"

# interfaces
.implements Landroid/support/v4/view/b;


# static fields
.field static final dXI:Z

.field private static final dXU:[I

.field private static final dYG:[Ljava/lang/Class;

.field private static final dYQ:[I

.field private static final dYU:Z

.field static final dYm:Z

.field static final dYn:Z

.field private static final dYu:Z

.field private static final dYw:Z

.field static final dZa:Landroid/view/animation/Interpolator;


# instance fields
.field dXA:Z

.field private dXB:F

.field private final dXC:Landroid/support/v7/widget/C;

.field private dXD:Landroid/support/v7/widget/D;

.field private dXE:I

.field dXF:Z

.field dXG:Z

.field private final dXH:Ljava/util/ArrayList;

.field private dXJ:I

.field private dXK:I

.field private dXL:F

.field private final dXM:[I

.field private dXN:Ljava/util/List;

.field private dXO:I

.field private dXP:Landroid/support/v7/widget/v;

.field private dXQ:Landroid/view/VelocityTracker;

.field private dXR:I

.field dXS:Z

.field private dXT:Landroid/widget/EdgeEffect;

.field private dXV:Landroid/widget/EdgeEffect;

.field private final dXW:[I

.field private final dXX:I

.field private dXY:Landroid/widget/EdgeEffect;

.field final dXZ:Ljava/lang/Runnable;

.field private dYA:I

.field final dYB:Landroid/support/v7/widget/g;

.field dYC:Landroid/support/v7/widget/G;

.field private dYD:I

.field private final dYE:[I

.field private dYF:I

.field final dYH:Landroid/support/v7/widget/j;

.field final dYI:Landroid/support/v7/widget/H;

.field dYJ:Landroid/support/v7/widget/I;

.field private dYK:Landroid/support/v7/widget/l;

.field dYL:Z

.field final dYM:Landroid/support/v7/widget/e;

.field private dYN:Z

.field private final dYO:Landroid/support/v7/widget/h;

.field private dYP:Landroid/support/v4/view/a;

.field dYR:Z

.field dYS:Z

.field dYT:Landroid/support/v7/widget/J;

.field private dYV:I

.field final dYW:Landroid/graphics/RectF;

.field final dYX:Landroid/graphics/Rect;

.field dYY:Landroid/support/v7/widget/u;

.field private final dYZ:Landroid/graphics/Rect;

.field private dYa:Ljava/lang/Runnable;

.field dYb:Z

.field private dYc:Landroid/widget/EdgeEffect;

.field private final dYd:I

.field private dYe:Ljava/util/List;

.field private dYf:I

.field final dYg:Ljava/util/ArrayList;

.field private dYh:Landroid/support/v7/widget/s;

.field private final dYi:[I

.field private dYj:I

.field dYk:Landroid/support/v7/widget/E;

.field dYl:Z

.field dYo:Z

.field dYp:Landroid/support/v7/widget/b;

.field private dYq:Z

.field dYr:Landroid/support/v7/widget/F;

.field private dYs:I

.field dYt:Landroid/support/v7/widget/n;

.field private final dYv:Landroid/view/accessibility/AccessibilityManager;

.field private dYx:Landroid/support/v7/widget/RecyclerView$SavedState;

.field private dYy:Z

.field dYz:Z

.field private dZb:I

.field mFirstLayoutComplete:Z

.field mLayout:Landroid/support/v7/widget/a;

.field final mPendingAccessibilityImportanceChange:Ljava/util/List;

.field private mScrollListener:Landroid/support/v7/widget/m;


# direct methods
.method static synthetic -get0()Z
    .locals 1

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYu:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0xf

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-array v0, v1, [I

    const v3, 0x1010436

    aput v3, v0, v2

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->dXU:[I

    new-array v0, v1, [I

    const v3, 0x10100eb

    aput v3, v0, v2

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->dYQ:[I

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-eq v0, v3, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-ne v0, v3, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYm:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v0, v3, :cond_3

    move v0, v1

    :goto_1
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->dXI:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_4

    move v0, v1

    :goto_2
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYn:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_5

    move v0, v1

    :goto_3
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYu:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v0, v4, :cond_6

    move v0, v1

    :goto_4
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYw:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v0, v4, :cond_7

    move v0, v1

    :goto_5
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYU:Z

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v0, v2

    const-class v2, Landroid/util/AttributeSet;

    aput-object v2, v0, v1

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->dYG:[Ljava/lang/Class;

    new-instance v0, Landroid/support/v7/widget/y;

    invoke-direct {v0}, Landroid/support/v7/widget/y;-><init>()V

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->dZa:Landroid/view/animation/Interpolator;

    return-void

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x14

    if-ne v0, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_5
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    const/4 v0, 0x0

    const/4 v7, -0x1

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->dYN:Z

    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    new-instance v1, Landroid/support/v7/widget/h;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/h;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYO:Landroid/support/v7/widget/h;

    new-instance v1, Landroid/support/v7/widget/j;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/j;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    new-instance v1, Landroid/support/v7/widget/H;

    invoke-direct {v1}, Landroid/support/v7/widget/H;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    new-instance v1, Landroid/support/v7/widget/w;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/w;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXZ:Ljava/lang/Runnable;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYW:Landroid/graphics/RectF;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXH:Ljava/util/ArrayList;

    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dYV:I

    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dZb:I

    new-instance v1, Landroid/support/v7/widget/Z;

    invoke-direct {v1}, Landroid/support/v7/widget/Z;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dYF:I

    iput v7, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    const/4 v1, 0x1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dXB:F

    const/4 v1, 0x1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dXL:F

    iput-boolean v6, p0, Landroid/support/v7/widget/RecyclerView;->dYy:Z

    new-instance v1, Landroid/support/v7/widget/g;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/g;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYB:Landroid/support/v7/widget/g;

    sget-boolean v1, Landroid/support/v7/widget/RecyclerView;->dYu:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/support/v7/widget/E;

    invoke-direct {v0}, Landroid/support/v7/widget/E;-><init>()V

    :cond_0
    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    new-instance v0, Landroid/support/v7/widget/e;

    invoke-direct {v0}, Landroid/support/v7/widget/e;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->dYS:Z

    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->dYo:Z

    new-instance v0, Landroid/support/v7/widget/t;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/t;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXD:Landroid/support/v7/widget/D;

    iput-boolean v5, p0, Landroid/support/v7/widget/RecyclerView;->dXS:Z

    new-array v0, v4, [I

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYE:[I

    new-array v0, v4, [I

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    new-array v0, v4, [I

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYi:[I

    new-array v0, v4, [I

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mPendingAccessibilityImportanceChange:Ljava/util/List;

    new-instance v0, Landroid/support/v7/widget/x;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/x;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYa:Ljava/lang/Runnable;

    new-instance v0, Landroid/support/v7/widget/z;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/z;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXC:Landroid/support/v7/widget/C;

    if-eqz p2, :cond_5

    sget-object v0, Landroid/support/v7/widget/RecyclerView;->dYQ:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :goto_0
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/RecyclerView;->setScrollContainer(Z)V

    invoke-virtual {p0, v6}, Landroid/support/v7/widget/RecyclerView;->setFocusableInTouchMode(Z)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    invoke-static {v0, p1}, Landroid/support/v4/view/ad;->dRy(Landroid/view/ViewConfiguration;Landroid/content/Context;)F

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dXB:F

    invoke-static {v0, p1}, Landroid/support/v4/view/ad;->dRz(Landroid/view/ViewConfiguration;Landroid/content/Context;)F

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dXL:F

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    int-to-double v2, v1

    const-wide v8, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v2, v8

    double-to-int v1, v2

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dXE:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dYd:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXX:I

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getOverScrollMode()I

    move-result v0

    if-ne v0, v4, :cond_6

    move v0, v6

    :goto_1
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXD:Landroid/support/v7/widget/D;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/u;->duQ(Landroid/support/v7/widget/D;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpl()V

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpa()V

    invoke-static {p0}, Landroid/support/v4/view/z;->dPI(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0, v6}, Landroid/support/v4/view/z;->dPL(Landroid/view/View;I)V

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYv:Landroid/view/accessibility/AccessibilityManager;

    new-instance v0, Landroid/support/v7/widget/F;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/F;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAccessibilityDelegateCompat(Landroid/support/v7/widget/F;)V

    if-eqz p2, :cond_7

    sget-object v0, Landroid/support/v7/a/c;->dGN:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    sget v0, Landroid/support/v7/a/c;->dGU:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v0, Landroid/support/v7/a/c;->dGO:I

    invoke-virtual {v3, v0, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    if-ne v0, v7, :cond_2

    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setDescendantFocusability(I)V

    :cond_2
    sget v0, Landroid/support/v7/a/c;->dGP:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXF:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXF:Z

    if-eqz v0, :cond_3

    sget v0, Landroid/support/v7/a/c;->dGS:I

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    sget v1, Landroid/support/v7/a/c;->dGT:I

    invoke-virtual {v3, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sget v1, Landroid/support/v7/a/c;->dGQ:I

    invoke-virtual {v3, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/StateListDrawable;

    sget v7, Landroid/support/v7/a/c;->dGR:I

    invoke-virtual {v3, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {p0, v0, v4, v1, v7}, Landroid/support/v7/widget/RecyclerView;->initFastScroller(Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;)V

    :cond_3
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/RecyclerView;->dqc(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;II)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_4

    sget-object v0, Landroid/support/v7/widget/RecyclerView;->dXU:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :cond_4
    :goto_2
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    return-void

    :cond_5
    iput-boolean v6, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    goto/16 :goto_0

    :cond_6
    move v0, v5

    goto/16 :goto_1

    :cond_7
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setDescendantFocusability(I)V

    goto :goto_2
.end method

.method private doE(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/16 v2, 0x2e

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "."

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p2

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static doH(Landroid/view/View;)Landroid/support/v7/widget/p;
    .locals 1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eaf:Landroid/support/v7/widget/p;

    return-object v0
.end method

.method private doI()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqP()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/e;->dtj(I)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwP()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v2}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/e;->dZH:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput v1, v0, Landroid/support/v7/widget/e;->dZv:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v1, v0, Landroid/support/v7/widget/e;->dZD:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/a;->dsi(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v1, v0, Landroid/support/v7/widget/e;->dZG:Z

    iput-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYx:Landroid/support/v7/widget/RecyclerView$SavedState;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v0, v0, Landroid/support/v7/widget/e;->dZF:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v2, Landroid/support/v7/widget/e;->dZF:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const/4 v2, 0x4

    iput v2, v0, Landroid/support/v7/widget/e;->dZI:I

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpS()V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic doS(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->detachViewFromParent(I)V

    return-void
.end method

.method private doy(II)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYE:[I

    invoke-direct {p0, v2}, Landroid/support/v7/widget/RecyclerView;->dpE([I)V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYE:[I

    aget v2, v2, v1

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYE:[I

    aget v2, v2, v0

    if-eq v2, p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic dpA(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    return-void
.end method

.method private dpC(FFFF)V
    .locals 6

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    cmpg-float v2, p2, v4

    if-gez v2, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doK()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    neg-float v2, p2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v3, p3, v3

    sub-float v3, v5, v3

    invoke-static {v1, v2, v3}, Landroid/support/v4/widget/W;->dXq(Landroid/widget/EdgeEffect;FF)V

    move v1, v0

    :cond_0
    :goto_0
    cmpg-float v2, p4, v4

    if-gez v2, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqR()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    neg-float v2, p4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v3, p1, v3

    invoke-static {v1, v2, v3}, Landroid/support/v4/widget/W;->dXq(Landroid/widget/EdgeEffect;FF)V

    :goto_1
    if-nez v0, :cond_1

    cmpl-float v0, p2, v4

    if-eqz v0, :cond_5

    :cond_1
    :goto_2
    invoke-static {p0}, Landroid/support/v4/view/z;->dPF(Landroid/view/View;)V

    :cond_2
    return-void

    :cond_3
    cmpl-float v2, p2, v4

    if-lez v2, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpc()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v2, p2, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v3, p3, v3

    invoke-static {v1, v2, v3}, Landroid/support/v4/widget/W;->dXq(Landroid/widget/EdgeEffect;FF)V

    move v1, v0

    goto :goto_0

    :cond_4
    cmpl-float v2, p4, v4

    if-lez v2, :cond_6

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqd()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v2, p4, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v3, p1, v3

    sub-float v3, v5, v3

    invoke-static {v1, v2, v3}, Landroid/support/v4/widget/W;->dXq(Landroid/widget/EdgeEffect;FF)V

    goto :goto_1

    :cond_5
    cmpl-float v0, p4, v4

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private dpE([I)V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, -0x1

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v5

    if-nez v5, :cond_0

    aput v1, p1, v4

    aput v1, p1, v7

    return-void

    :cond_0
    const v2, 0x7fffffff

    const/high16 v0, -0x80000000

    move v3, v4

    :goto_0
    if-ge v3, v5, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v6

    if-eqz v6, :cond_1

    move v1, v2

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v1

    if-ge v1, v2, :cond_2

    move v2, v1

    :cond_2
    if-le v1, v0, :cond_4

    move v0, v1

    move v1, v2

    goto :goto_1

    :cond_3
    aput v2, p1, v4

    aput v0, p1, v7

    return-void

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method private dpF()V
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->dYy:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_1

    move-object v2, v0

    :goto_1
    if-nez v2, :cond_2

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpT()V

    :goto_2
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->dpD(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_2
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Landroid/support/v7/widget/p;->getItemId()J

    move-result-wide v0

    :goto_3
    iput-wide v0, v3, Landroid/support/v7/widget/e;->dZw:J

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    if-eqz v0, :cond_4

    const/4 v0, -0x1

    :goto_4
    iput v0, v1, Landroid/support/v7/widget/e;->dZx:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v1, v2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-direct {p0, v1}, Landroid/support/v7/widget/RecyclerView;->dpZ(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/e;->dZz:I

    goto :goto_2

    :cond_3
    const-wide/16 v0, -0x1

    goto :goto_3

    :cond_4
    invoke-virtual {v2}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, v2, Landroid/support/v7/widget/p;->mOldPosition:I

    goto :goto_4

    :cond_5
    invoke-virtual {v2}, Landroid/support/v7/widget/p;->getAdapterPosition()I

    move-result v0

    goto :goto_4
.end method

.method private dpG()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpu()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setScrollState(I)V

    return-void
.end method

.method private dpL(Landroid/view/MotionEvent;)V
    .locals 5

    const/4 v0, 0x0

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    if-ne v2, v3, :cond_1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dXR:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    add-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYD:I

    :cond_1
    return-void
.end method

.method private dpN()Landroid/view/View;
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget v1, v1, Landroid/support/v7/widget/e;->dZx:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget v0, v0, Landroid/support/v7/widget/e;->dZx:I

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v1}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->dqv(I)Landroid/support/v7/widget/p;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_1
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_6

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->dqv(I)Landroid/support/v7/widget/p;

    move-result-object v1

    if-nez v1, :cond_4

    return-object v5

    :cond_2
    iget-object v4, v3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->hasFocusable()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v0, v3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    return-object v0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    iget-object v2, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->hasFocusable()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v0, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    return-object v0

    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_6
    return-object v5
.end method

.method private dpT()V
    .locals 4

    const/4 v1, -0x1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Landroid/support/v7/widget/e;->dZw:J

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput v1, v0, Landroid/support/v7/widget/e;->dZx:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput v1, v0, Landroid/support/v7/widget/e;->dZz:I

    return-void
.end method

.method private dpX()V
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYj:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dYj:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doA()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    const/16 v2, 0x800

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setEventType(I)V

    invoke-static {v1, v0}, Landroid/support/v4/view/a/i;->dNP(Landroid/view/accessibility/AccessibilityEvent;I)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    :cond_0
    return-void
.end method

.method private dpY()V
    .locals 6

    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYy:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->hasFocus()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getDescendantFocusability()I

    move-result v0

    const/high16 v2, 0x60000

    if-eq v0, v2, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getDescendantFocusability()I

    move-result v0

    const/high16 v2, 0x20000

    if-ne v0, v2, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    sget-boolean v2, Landroid/support/v7/widget/RecyclerView;->dYU:Z

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestFocus()Z

    return-void

    :cond_4
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvL(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_5

    return-void

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-wide v2, v0, Landroid/support/v7/widget/e;->dZw:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-wide v2, v0, Landroid/support/v7/widget/e;->dZw:J

    invoke-virtual {p0, v2, v3}, Landroid/support/v7/widget/RecyclerView;->doC(J)Landroid/support/v7/widget/p;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_6

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    iget-object v3, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/G;->dvL(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->hasFocusable()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_a

    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v0

    if-lez v0, :cond_7

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpN()Landroid/view/View;

    move-result-object v1

    :cond_7
    :goto_1
    if-eqz v1, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget v0, v0, Landroid/support/v7/widget/e;->dZz:I

    int-to-long v2, v0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget v0, v0, Landroid/support/v7/widget/e;->dZz:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_c

    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_8
    return-void

    :cond_9
    move-object v0, v1

    goto :goto_0

    :cond_a
    iget-object v1, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    goto :goto_1

    :cond_b
    move-object v0, v1

    goto :goto_2

    :cond_c
    move-object v0, v1

    goto :goto_2

    :cond_d
    move-object v0, v1

    goto :goto_0
.end method

.method private dpZ(Landroid/view/View;)I
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    move v1, v0

    move-object v0, p1

    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-nez v2, :cond_0

    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getFocusedChild()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    :goto_1
    move v1, v0

    move-object v0, p1

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private dpa()V
    .locals 2

    new-instance v0, Landroid/support/v7/widget/G;

    new-instance v1, Landroid/support/v7/widget/A;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/A;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v1}, Landroid/support/v7/widget/G;-><init>(Landroid/support/v7/widget/W;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    return-void
.end method

.method private dph(Landroid/view/View;Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x0

    if-eqz p2, :cond_3

    move-object v0, p2

    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eae:Z

    if-nez v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v3, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v0}, Landroid/support/v7/widget/RecyclerView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->mFirstLayoutComplete:Z

    xor-int/lit8 v4, v1, 0x1

    if-nez p2, :cond_2

    const/4 v5, 0x1

    :cond_2
    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/a;->drW(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;ZZ)Z

    return-void

    :cond_3
    move-object v0, p1

    goto :goto_0
.end method

.method private dpi(Landroid/support/v7/widget/p;)V
    .locals 5

    const/4 v1, 0x1

    iget-object v2, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/j;->dud(Landroid/support/v7/widget/p;)V

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isTmpDetached()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v0, v2, v4, v3, v1}, Landroid/support/v7/widget/G;->dvV(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/G;->dvN(Landroid/view/View;Z)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/G;->dwa(Landroid/view/View;)V

    goto :goto_1
.end method

.method private dpj(Landroid/view/View;Landroid/view/View;I)Z
    .locals 7

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v4, 0x0

    if-eqz p2, :cond_0

    if-ne p2, p0, :cond_1

    :cond_0
    return v4

    :cond_1
    if-nez p1, :cond_2

    return v2

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v0, v4, v4, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {v0, v4, v4, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v0}, Landroid/support/v7/widget/RecyclerView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->drR()I

    move-result v0

    if-ne v0, v2, :cond_6

    move v0, v1

    :goto_0
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-lt v3, v5, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-gt v3, v5, :cond_7

    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    if-ge v3, v5, :cond_7

    move v3, v2

    :goto_1
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    if-lt v5, v6, :cond_4

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    if-gt v5, v6, :cond_a

    :cond_4
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    if-ge v5, v6, :cond_a

    move v1, v2

    :cond_5
    :goto_2
    sparse-switch p3, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid direction: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v0, v2

    goto :goto_0

    :cond_7
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    if-gt v3, v5, :cond_8

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    if-lt v3, v5, :cond_9

    :cond_8
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-le v3, v5, :cond_9

    move v3, v1

    goto :goto_1

    :cond_9
    move v3, v4

    goto :goto_1

    :cond_a
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    if-gt v5, v6, :cond_b

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    if-lt v5, v6, :cond_c

    :cond_b
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYZ:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    if-gt v5, v6, :cond_5

    :cond_c
    move v1, v4

    goto :goto_2

    :sswitch_0
    if-gez v3, :cond_d

    :goto_3
    return v2

    :cond_d
    move v2, v4

    goto :goto_3

    :sswitch_1
    if-lez v3, :cond_e

    :goto_4
    return v2

    :cond_e
    move v2, v4

    goto :goto_4

    :sswitch_2
    if-gez v1, :cond_f

    :goto_5
    return v2

    :cond_f
    move v2, v4

    goto :goto_5

    :sswitch_3
    if-lez v1, :cond_10

    :goto_6
    return v2

    :cond_10
    move v2, v4

    goto :goto_6

    :sswitch_4
    if-gtz v1, :cond_11

    if-nez v1, :cond_12

    mul-int/2addr v0, v3

    if-ltz v0, :cond_12

    :cond_11
    :goto_7
    return v2

    :cond_12
    move v2, v4

    goto :goto_7

    :sswitch_5
    if-ltz v1, :cond_13

    if-nez v1, :cond_14

    mul-int/2addr v0, v3

    if-gtz v0, :cond_14

    :cond_13
    :goto_8
    return v2

    :cond_14
    move v2, v4

    goto :goto_8

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_4
        0x11 -> :sswitch_0
        0x21 -> :sswitch_2
        0x42 -> :sswitch_1
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method private dpn(JLandroid/support/v7/widget/p;Landroid/support/v7/widget/p;)V
    .locals 7

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v2

    if-ne v2, p3, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->dpH(Landroid/support/v7/widget/p;)J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Two different ViewHolders have the same stable ID. Stable IDs in your adapter MUST BE unique and SHOULD NOT change.\n ViewHolder 1:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " \n View Holder 2:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Two different ViewHolders have the same change ID. This might happen due to inconsistent Adapter update events or if the LayoutManager lays out the same View multiple times.\n ViewHolder 1:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " \n View Holder 2:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const-string/jumbo v0, "RecyclerView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Problem while matching changed view holders with the newones. The pre-layout information for the change holder "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " cannot be found but it is necessary for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private dpo(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v6, 0x3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eq v3, v6, :cond_0

    if-nez v3, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYK:Landroid/support/v7/widget/l;

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXH:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/l;

    invoke-interface {v0, p0, p1}, Landroid/support/v7/widget/l;->dop(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-eq v3, v6, :cond_2

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYK:Landroid/support/v7/widget/l;

    const/4 v0, 0x1

    return v0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return v2
.end method

.method private dpr(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYK:Landroid/support/v7/widget/l;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    iput-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYK:Landroid/support/v7/widget/l;

    :cond_0
    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXH:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/l;

    invoke-interface {v0, p0, p1}, Landroid/support/v7/widget/l;->dop(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_4

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYK:Landroid/support/v7/widget/l;

    return v5

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYK:Landroid/support/v7/widget/l;

    invoke-interface {v1, p0, p1}, Landroid/support/v7/widget/l;->dor(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)V

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    iput-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYK:Landroid/support/v7/widget/l;

    :cond_3
    return v5

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_5
    return v2
.end method

.method static synthetic dps(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    return v0
.end method

.method private dpu()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->dpy(I)V

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqe()V

    return-void
.end method

.method static dqC(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 6

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v3, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v3

    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iget v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v4

    iget v5, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v4, v5

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v5

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v5

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method private dqL()V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/e;->dtj(I)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->doQ(Landroid/support/v7/widget/e;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v1, v0, Landroid/support/v7/widget/e;->dZK:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v0}, Landroid/support/v7/widget/H;->clear()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqP()V

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqm()V

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpF()V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v0, v0, Landroid/support/v7/widget/e;->dZF:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYo:Z

    :goto_0
    iput-boolean v0, v2, Landroid/support/v7/widget/e;->dZy:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->dYo:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->dYS:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v2, v2, Landroid/support/v7/widget/e;->dZJ:Z

    iput-boolean v2, v0, Landroid/support/v7/widget/e;->dZD:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v2}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/e;->dZH:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYE:[I

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->dpE([I)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v0, v0, Landroid/support/v7/widget/e;->dZF:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v4}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-static {v3}, Landroid/support/v7/widget/u;->duP(Landroid/support/v7/widget/p;)I

    move-result v6

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->getUnmodifiedPayloads()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/support/v7/widget/u;->dva(Landroid/support/v7/widget/e;Landroid/support/v7/widget/p;ILjava/util/List;)Landroid/support/v7/widget/K;

    move-result-object v4

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v5, v3, v4}, Landroid/support/v7/widget/H;->dwh(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v4, v4, Landroid/support/v7/widget/e;->dZy:Z

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isUpdated()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {p0, v3}, Landroid/support/v7/widget/RecyclerView;->dpH(Landroid/support/v7/widget/p;)J

    move-result-wide v4

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v6, v4, v5, v3}, Landroid/support/v7/widget/H;->dwj(JLandroid/support/v7/widget/p;)V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v0, v0, Landroid/support/v7/widget/e;->dZJ:Z

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqb()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v0, v0, Landroid/support/v7/widget/e;->dZG:Z

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v1, v2, Landroid/support/v7/widget/e;->dZG:Z

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v2, v3, v4}, Landroid/support/v7/widget/a;->dsi(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;)V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v0, v2, Landroid/support/v7/widget/e;->dZG:Z

    move v0, v1

    :goto_3
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v2

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/H;->dwp(Landroid/support/v7/widget/p;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {v3}, Landroid/support/v7/widget/u;->duP(Landroid/support/v7/widget/p;)I

    move-result v2

    const/16 v4, 0x2000

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/p;->hasAnyOfTheFlags(I)Z

    move-result v4

    if-nez v4, :cond_6

    or-int/lit16 v2, v2, 0x1000

    :cond_6
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->getUnmodifiedPayloads()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v5, v6, v3, v2, v7}, Landroid/support/v7/widget/u;->dva(Landroid/support/v7/widget/e;Landroid/support/v7/widget/p;ILjava/util/List;)Landroid/support/v7/widget/K;

    move-result-object v2

    if-eqz v4, :cond_7

    invoke-virtual {p0, v3, v2}, Landroid/support/v7/widget/RecyclerView;->doN(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V

    goto :goto_4

    :cond_7
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v4, v3, v2}, Landroid/support/v7/widget/H;->dwm(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V

    goto :goto_4

    :cond_8
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doV()V

    :goto_5
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpS()V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const/4 v1, 0x2

    iput v1, v0, Landroid/support/v7/widget/e;->dZI:I

    return-void

    :cond_9
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doV()V

    goto :goto_5
.end method

.method private dqQ(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;ZZ)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/support/v7/widget/p;->setIsRecyclable(Z)V

    if-eqz p5, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpi(Landroid/support/v7/widget/p;)V

    :cond_0
    if-eq p1, p2, :cond_2

    if-eqz p6, :cond_1

    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView;->dpi(Landroid/support/v7/widget/p;)V

    :cond_1
    iput-object p2, p1, Landroid/support/v7/widget/p;->mShadowedHolder:Landroid/support/v7/widget/p;

    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpi(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/j;->dud(Landroid/support/v7/widget/p;)V

    invoke-virtual {p2, v1}, Landroid/support/v7/widget/p;->setIsRecyclable(Z)V

    iput-object p1, p2, Landroid/support/v7/widget/p;->mShadowingHolder:Landroid/support/v7/widget/p;

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v7/widget/u;->dvg(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqD()V

    :cond_3
    return-void
.end method

.method private dqc(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;II)V
    .locals 7

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->doE(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/support/v7/widget/a;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v4

    :try_start_1
    sget-object v0, Landroid/support/v7/widget/RecyclerView;->dYG:[Ljava/lang/Class;

    invoke-virtual {v4, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v1, v5

    const/4 v5, 0x1

    aput-object p3, v1, v5

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x2

    aput-object v5, v1, v6

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x3

    aput-object v5, v1, v6
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    const/4 v2, 0x1

    :try_start_2
    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/a;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/a;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_3
    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v4, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/InstantiationException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v0

    move-object v1, v2

    goto :goto_1

    :catch_1
    move-exception v1

    :try_start_4
    invoke-virtual {v1, v0}, Ljava/lang/NoSuchMethodException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ": Error creating LayoutManager "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/InstantiationException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ": Unable to find LayoutManager "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ": Class is not a LayoutManager "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_4
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ": Cannot access non-public constructor "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_5
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ": Could not instantiate the LayoutManager: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_6
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ": Could not instantiate the LayoutManager: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private dqe()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    :cond_3
    if-eqz v0, :cond_4

    invoke-static {p0}, Landroid/support/v4/view/z;->dPF(Landroid/view/View;)V

    :cond_4
    return-void
.end method

.method private dqg()V
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/e;->dtj(I)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqP()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput v11, v0, Landroid/support/v7/widget/e;->dZI:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v0, v0, Landroid/support/v7/widget/e;->dZF:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_0
    if-ltz v7, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0, v7}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->dpH(Landroid/support/v7/widget/p;)J

    move-result-wide v8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/u;->dvb(Landroid/support/v7/widget/e;Landroid/support/v7/widget/p;)Landroid/support/v7/widget/K;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v1, v8, v9}, Landroid/support/v7/widget/H;->dwd(J)Landroid/support/v7/widget/p;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/H;->dwi(Landroid/support/v7/widget/p;)Z

    move-result v5

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/H;->dwi(Landroid/support/v7/widget/p;)Z

    move-result v6

    if-eqz v5, :cond_1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/widget/H;->dwg(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/H;->dwr(Landroid/support/v7/widget/p;)Landroid/support/v7/widget/K;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v4, v2, v0}, Landroid/support/v7/widget/H;->dwg(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/H;->dwk(Landroid/support/v7/widget/p;)Landroid/support/v7/widget/K;

    move-result-object v4

    if-nez v3, :cond_2

    invoke-direct {p0, v8, v9, v2, v1}, Landroid/support/v7/widget/RecyclerView;->dpn(JLandroid/support/v7/widget/p;Landroid/support/v7/widget/p;)V

    goto :goto_1

    :cond_2
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/widget/RecyclerView;->dqQ(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;ZZ)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/widget/H;->dwg(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXC:Landroid/support/v7/widget/C;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/H;->dwe(Landroid/support/v7/widget/C;)V

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drx(Landroid/support/v7/widget/j;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget v1, v1, Landroid/support/v7/widget/e;->dZH:I

    iput v1, v0, Landroid/support/v7/widget/e;->dZC:I

    iput-boolean v10, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v10, v0, Landroid/support/v7/widget/e;->dZF:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v10, v0, Landroid/support/v7/widget/e;->dZJ:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iput-boolean v10, v0, Landroid/support/v7/widget/a;->dZs:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v0, v0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v0, v0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-boolean v0, v0, Landroid/support/v7/widget/a;->dZh:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iput v10, v0, Landroid/support/v7/widget/a;->dZq:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iput-boolean v10, v0, Landroid/support/v7/widget/a;->dZh:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->dua()V

    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->dqS(Landroid/support/v7/widget/e;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpS()V

    invoke-virtual {p0, v10}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v0}, Landroid/support/v7/widget/H;->clear()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYE:[I

    aget v0, v0, v10

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYE:[I

    aget v1, v1, v11

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->doy(II)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0, v10, v10}, Landroid/support/v7/widget/RecyclerView;->dpP(II)V

    :cond_8
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpY()V

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpT()V

    return-void
.end method

.method static synthetic dqj(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private dqk()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYB:Landroid/support/v7/widget/g;

    invoke-virtual {v0}, Landroid/support/v7/widget/g;->stop()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dra()V

    :cond_0
    return-void
.end method

.method private dqm()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwO()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/a;->drh(Landroid/support/v7/widget/RecyclerView;)V

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqs()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwC()V

    :goto_0
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYS:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYo:Z

    :goto_1
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v4, p0, Landroid/support/v7/widget/RecyclerView;->mFirstLayoutComplete:Z

    if-eqz v4, :cond_6

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v4, :cond_6

    iget-boolean v4, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    if-nez v4, :cond_1

    if-nez v0, :cond_1

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-boolean v4, v4, Landroid/support/v7/widget/a;->dZs:Z

    if-eqz v4, :cond_6

    :cond_1
    iget-boolean v4, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    if-eqz v4, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v1

    :cond_2
    :goto_2
    iput-boolean v1, v3, Landroid/support/v7/widget/e;->dZF:Z

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v3, v3, Landroid/support/v7/widget/e;->dZF:Z

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqs()Z

    move-result v2

    :cond_3
    iput-boolean v2, v1, Landroid/support/v7/widget/e;->dZJ:Z

    return-void

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwP()V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method static synthetic dqr(Landroid/support/v7/widget/RecyclerView;)[I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYi:[I

    return-object v0
.end method

.method private dqs()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dss()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static dqw(Landroid/support/v7/widget/p;)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/p;->mNestedRecyclerView:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/p;->mNestedRecyclerView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    if-ne v0, v2, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    iput-object v1, p0, Landroid/support/v7/widget/p;->mNestedRecyclerView:Ljava/lang/ref/WeakReference;

    :cond_3
    return-void
.end method

.method private dqx()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isUpdated()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_2
    return v1
.end method

.method static dqz(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView;
    .locals 4

    const/4 v3, 0x0

    instance-of v0, p0, Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    return-object v3

    :cond_0
    instance-of v0, p0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    check-cast p0, Landroid/support/v7/widget/RecyclerView;

    return-object p0

    :cond_1
    check-cast p0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->dqz(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v2

    if-eqz v2, :cond_2

    return-object v2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-object v3
.end method

.method private getScrollingChildHelper()Landroid/support/v4/view/a;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYP:Landroid/support/v4/view/a;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/view/a;

    invoke-direct {v0, p0}, Landroid/support/v4/view/a;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYP:Landroid/support/v4/view/a;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYP:Landroid/support/v4/view/a;

    return-object v0
.end method

.method private setAdapterInternal(Landroid/support/v7/widget/b;ZZ)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYO:Landroid/support/v7/widget/h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/b;->unregisterAdapterDataObserver(Landroid/support/v7/widget/c;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/b;->onDetachedFromRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    :cond_0
    if-eqz p2, :cond_1

    if-eqz p3, :cond_2

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpV()V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwO()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz p1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYO:Landroid/support/v7/widget/h;

    invoke-virtual {p1, v1}, Landroid/support/v7/widget/b;->registerAdapterDataObserver(Landroid/support/v7/widget/c;)V

    invoke-virtual {p1, p0}, Landroid/support/v7/widget/b;->onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/a;->dsb(Landroid/support/v7/widget/b;Landroid/support/v7/widget/b;)V

    :cond_4
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1, v0, v2, p2}, Landroid/support/v7/widget/j;->dtV(Landroid/support/v7/widget/b;Landroid/support/v7/widget/b;Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/e;->dZG:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->setDataSetChangedAfterLayout()V

    return-void
.end method


# virtual methods
.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/v7/widget/a;->dsU(Landroid/support/v7/widget/RecyclerView;Ljava/util/ArrayList;II)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addFocusables(Ljava/util/ArrayList;II)V

    :cond_1
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    check-cast p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a;->dtf(Landroid/support/v7/widget/RecyclerView$LayoutParams;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeHorizontalScrollExtent()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drr(Landroid/support/v7/widget/e;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public computeHorizontalScrollOffset()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drX(Landroid/support/v7/widget/e;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public computeHorizontalScrollRange()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->dsZ(Landroid/support/v7/widget/e;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public computeVerticalScrollExtent()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->dsD(Landroid/support/v7/widget/e;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public computeVerticalScrollOffset()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->dsO(Landroid/support/v7/widget/e;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public computeVerticalScrollRange()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->dsx(Landroid/support/v7/widget/e;)I

    move-result v0

    :cond_1
    return v0
.end method

.method public dispatchNestedFling(FFZ)Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/view/a;->dispatchNestedFling(FFZ)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedPreFling(FF)Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/a;->dispatchNestedPreFling(FF)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedPreScroll(II[I[I)Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/view/a;->dispatchNestedPreScroll(II[I[I)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedScroll(IIII[I)Z
    .locals 6

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/a;->dispatchNestedScroll(IIII[I)Z

    move-result v0

    return v0
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    return-void
.end method

.method doA()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYv:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYv:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doB(IILandroid/view/animation/Interpolator;)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v1, :cond_0

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    if-eqz v1, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v1

    if-nez v1, :cond_2

    move p1, v0

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v1

    if-nez v1, :cond_5

    :goto_0
    if-nez p1, :cond_3

    if-eqz v0, :cond_4

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYB:Landroid/support/v7/widget/g;

    invoke-virtual {v1, p1, v0, p3}, Landroid/support/v7/widget/g;->dtp(IILandroid/view/animation/Interpolator;)V

    :cond_4
    return-void

    :cond_5
    move v0, p2

    goto :goto_0
.end method

.method public doC(J)Landroid/support/v7/widget/p;
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getItemId()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-nez v4, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    iget-object v4, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/G;->dvL(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    return-object v0

    :cond_4
    return-object v1
.end method

.method doD(Landroid/view/View;)Z
    .locals 3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/G;->dvT(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/j;->dud(Landroid/support/v7/widget/p;)V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/j;->dur(Landroid/support/v7/widget/p;)V

    :cond_0
    xor-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    return v0
.end method

.method doF(II)V
    .locals 2

    if-gez p1, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doK()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    :cond_0
    :goto_0
    if-gez p2, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqR()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    neg-int v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    :cond_1
    :goto_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_3

    :cond_2
    invoke-static {p0}, Landroid/support/v4/view/z;->dPF(Landroid/view/View;)V

    :cond_3
    return-void

    :cond_4
    if-lez p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpc()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {v0, p1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    goto :goto_0

    :cond_5
    if-lez p2, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqd()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {v0, p2}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    goto :goto_1
.end method

.method doG()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-nez v0, :cond_0

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "No adapter attached; skipping layout"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_1

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "No layout manager attached; skipping layout"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/e;->dZK:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget v0, v0, Landroid/support/v7/widget/e;->dZI:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqL()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/a;->dsm(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->doI()V

    :goto_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqg()V

    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwz()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->drZ()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    if-eq v0, v1, :cond_4

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/a;->dsm(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->doI()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dre()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/a;->dsm(Landroid/support/v7/widget/RecyclerView;)V

    goto :goto_0
.end method

.method public doJ(Landroid/view/View;)Landroid/support/v7/widget/p;
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "View "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not a direct child of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    return-object v0
.end method

.method doK()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method doL()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/p;->addFlags(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqI()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->dtT()V

    return-void
.end method

.method public doM(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method doN(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x2000

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/widget/p;->setFlags(II)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v0, v0, Landroid/support/v7/widget/e;->dZy:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isUpdated()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpH(Landroid/support/v7/widget/p;)J

    move-result-wide v0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v2, v0, v1, p1}, Landroid/support/v7/widget/H;->dwj(JLandroid/support/v7/widget/p;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/H;->dwh(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V

    return-void
.end method

.method doO(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpi(Landroid/support/v7/widget/p;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/p;->setIsRecyclable(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/u;->duR(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqD()V

    :cond_0
    return-void
.end method

.method doP()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Landroid/view/ViewGroup;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", adapter:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", layout:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", context:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final doQ(Landroid/support/v7/widget/e;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYB:Landroid/support/v7/widget/g;

    invoke-static {v0}, Landroid/support/v7/widget/g;->dtt(Landroid/support/v7/widget/g;)Landroid/widget/OverScroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p1, Landroid/support/v7/widget/e;->dZB:I

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getFinalY()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p1, Landroid/support/v7/widget/e;->dZE:I

    :goto_0
    return-void

    :cond_0
    iput v2, p1, Landroid/support/v7/widget/e;->dZB:I

    iput v2, p1, Landroid/support/v7/widget/e;->dZE:I

    goto :goto_0
.end method

.method doR()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    return-void
.end method

.method public doT(Landroid/support/v7/widget/d;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    const-string/jumbo v1, "Cannot remove item decoration during a scroll  or layout"

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drm(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getOverScrollMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqI()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public doU(Landroid/support/v7/widget/l;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXH:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method doV()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/support/v7/widget/p;->clearOldPosition()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->duh()V

    return-void
.end method

.method public doW(I)Landroid/support/v7/widget/p;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->dpk(IZ)Landroid/support/v7/widget/p;

    move-result-object v0

    return-object v0
.end method

.method public doX(Landroid/support/v7/widget/m;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public doY(I)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method doZ(Ljava/lang/String;)V
    .locals 5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqu()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Cannot call this method while RecyclerView is computing a layout or scrolling"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dZb:I

    if-lez v0, :cond_2

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "Cannot call this method in a scroll callback. Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data. Any method call that might change the structureof the RecyclerView or the adapter contents should be postponed tothe next frame."

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    return-void
.end method

.method doz(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 8

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-boolean v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eae:Z

    if-nez v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    return-object v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v1}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->duy()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->dux()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    return-object v0

    :cond_2
    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eag:Landroid/graphics/Rect;

    invoke-virtual {v4, v3, v3, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/d;

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v1, v6, p1, p0, v7}, Landroid/support/v7/widget/d;->cdd(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V

    iget v1, v4, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v6

    iput v1, v4, Landroid/graphics/Rect;->left:I

    iget v1, v4, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v6

    iput v1, v4, Landroid/graphics/Rect;->top:I

    iget v1, v4, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v6

    iput v1, v4, Landroid/graphics/Rect;->right:I

    iget v1, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->dYX:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v6

    iput v1, v4, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_3
    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eae:Z

    return-object v4
.end method

.method dpB(Landroid/view/View;)V
    .locals 2

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpR(Landroid/view/View;)V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/b;->onViewDetachedFromWindow(Landroid/support/v7/widget/p;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/o;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/o;->dnu(Landroid/view/View;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dpD(Landroid/view/View;)Landroid/support/v7/widget/p;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dqK(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    goto :goto_0
.end method

.method dpH(Landroid/support/v7/widget/p;)J
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getItemId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget v0, p1, Landroid/support/v7/widget/p;->mPosition:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method dpI(Landroid/support/v7/widget/p;)Z
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getUnmodifiedPayloads()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/u;->duV(Landroid/support/v7/widget/p;Ljava/util/List;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method dpJ(Z)V
    .locals 2

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYV:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYV:I

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYV:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYV:I

    if-eqz p1, :cond_0

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpX()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqf()V

    :cond_0
    return-void
.end method

.method public dpK(IIII[II)Z
    .locals 7

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/support/v4/view/a;->dNS(IIII[II)Z

    move-result v0

    return v0
.end method

.method public dpM(Landroid/support/v7/widget/d;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->dqa(Landroid/support/v7/widget/d;I)V

    return-void
.end method

.method dpO(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a;->dsA(I)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    return-void
.end method

.method dpP(II)V
    .locals 2

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dZb:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dZb:I

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1, v0, v1}, Landroid/support/v7/widget/RecyclerView;->onScrollChanged(IIII)V

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->dqh(II)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mScrollListener:Landroid/support/v7/widget/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mScrollListener:Landroid/support/v7/widget/m;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v7/widget/m;->onScrolled(Landroid/support/v7/widget/RecyclerView;II)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/m;

    invoke-virtual {v0, p0, p1, p2}, Landroid/support/v7/widget/m;->onScrolled(Landroid/support/v7/widget/RecyclerView;II)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dZb:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dZb:I

    return-void
.end method

.method dpQ()V
    .locals 2

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYb:Z

    :cond_0
    return-void
.end method

.method public dpR(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method dpS()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->dpJ(Z)V

    return-void
.end method

.method dpU(II)V
    .locals 9

    const/4 v1, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v6

    if-ge p1, p2, :cond_1

    const/4 v0, -0x1

    move v2, p2

    move v3, p1

    :goto_0
    move v4, v5

    :goto_1
    if-ge v4, v6, :cond_4

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v7, v4}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v7

    invoke-static {v7}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v7

    if-eqz v7, :cond_0

    iget v8, v7, Landroid/support/v7/widget/p;->mPosition:I

    if-ge v8, v3, :cond_2

    :cond_0
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    move v2, p1

    move v3, p2

    goto :goto_0

    :cond_2
    iget v8, v7, Landroid/support/v7/widget/p;->mPosition:I

    if-gt v8, v2, :cond_0

    iget v8, v7, Landroid/support/v7/widget/p;->mPosition:I

    if-ne v8, p1, :cond_3

    sub-int v8, p2, p1

    invoke-virtual {v7, v8, v5}, Landroid/support/v7/widget/p;->offsetPosition(IZ)V

    :goto_3
    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v1, v7, Landroid/support/v7/widget/e;->dZG:Z

    goto :goto_2

    :cond_3
    invoke-virtual {v7, v0, v5}, Landroid/support/v7/widget/p;->offsetPosition(IZ)V

    goto :goto_3

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/j;->duf(II)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    return-void
.end method

.method dpV()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->dvh()V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->dqX(Landroid/support/v7/widget/j;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drx(Landroid/support/v7/widget/j;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->clear()V

    return-void
.end method

.method public dpW(Landroid/support/v7/widget/m;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public dpb(FF)Landroid/view/View;
    .locals 5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTranslationX()F

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v2, v4

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method dpc()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method public dpd(I)Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/a;->dNV(I)Z

    move-result v0

    return v0
.end method

.method dpe(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/p;->setIsRecyclable(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/u;->dvd(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqD()V

    :cond_0
    return-void
.end method

.method dpf(Landroid/support/v7/widget/p;)I
    .locals 2

    const/16 v0, 0x20c

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/p;->hasAnyOfTheFlags(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isBound()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    iget v1, p1, Landroid/support/v7/widget/p;->mPosition:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/I;->dwI(I)I

    move-result v0

    return v0
.end method

.method public dpg(II)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_0

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v3

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    if-eqz v0, :cond_1

    return v3

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v2

    if-eqz v4, :cond_2

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->dYd:I

    if-ge v0, v5, :cond_3

    :cond_2
    move p1, v3

    :cond_3
    if-eqz v2, :cond_4

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->dYd:I

    if-ge v0, v5, :cond_5

    :cond_4
    move p2, v3

    :cond_5
    if-nez p1, :cond_6

    if-nez p2, :cond_6

    return v3

    :cond_6
    int-to-float v0, p1

    int-to-float v5, p2

    invoke-virtual {p0, v0, v5}, Landroid/support/v7/widget/RecyclerView;->dispatchNestedPreFling(FF)Z

    move-result v0

    if-nez v0, :cond_a

    if-nez v4, :cond_7

    move v0, v2

    :goto_0
    int-to-float v5, p1

    int-to-float v6, p2

    invoke-virtual {p0, v5, v6, v0}, Landroid/support/v7/widget/RecyclerView;->dispatchNestedFling(FFZ)Z

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYh:Landroid/support/v7/widget/s;

    if-eqz v5, :cond_8

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dYh:Landroid/support/v7/widget/s;

    invoke-virtual {v5, p1, p2}, Landroid/support/v7/widget/s;->duK(II)Z

    move-result v5

    if-eqz v5, :cond_8

    return v1

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    if-eqz v0, :cond_a

    if-eqz v4, :cond_b

    move v0, v1

    :goto_1
    if-eqz v2, :cond_9

    or-int/lit8 v0, v0, 0x2

    :cond_9
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->dqq(II)Z

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dXX:I

    neg-int v0, v0

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->dXX:I

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->dXX:I

    neg-int v2, v2

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->dXX:I

    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYB:Landroid/support/v7/widget/g;

    invoke-virtual {v3, v0, v2}, Landroid/support/v7/widget/g;->dtw(II)V

    return v1

    :cond_a
    return v3

    :cond_b
    move v0, v3

    goto :goto_1
.end method

.method dpk(IZ)Landroid/support/v7/widget/p;
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_0
    if-ge v2, v3, :cond_4

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    if-eqz p2, :cond_1

    iget v4, v1, Landroid/support/v7/widget/p;->mPosition:I

    if-eq v4, p1, :cond_2

    :cond_0
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v4

    if-ne v4, p1, :cond_0

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    iget-object v4, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/G;->dvL(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    goto :goto_1

    :cond_3
    return-object v1

    :cond_4
    return-object v0
.end method

.method dpl()V
    .locals 2

    new-instance v0, Landroid/support/v7/widget/I;

    new-instance v1, Landroid/support/v7/widget/B;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/B;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v1}, Landroid/support/v7/widget/I;-><init>(Landroid/support/v7/widget/X;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    return-void
.end method

.method public dpm(II[I[II)Z
    .locals 6

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/a;->dNR(II[I[II)Z

    move-result v0

    return v0
.end method

.method public dpp()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->mFirstLayoutComplete:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwN()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dpq()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setScrollState(I)V

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqk()V

    return-void
.end method

.method dpt(II)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    if-lez p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    if-gez p1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    if-lez p2, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    if-gez p2, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    or-int/2addr v0, v1

    :cond_3
    if-eqz v0, :cond_4

    invoke-static {p0}, Landroid/support/v4/view/z;->dPF(Landroid/view/View;)V

    :cond_4
    return-void
.end method

.method dpv(II)V
    .locals 3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {p0}, Landroid/support/v4/view/z;->dPM(Landroid/view/View;)I

    move-result v1

    invoke-static {p1, v0, v1}, Landroid/support/v7/widget/a;->dry(III)I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {p0}, Landroid/support/v4/view/z;->dPS(Landroid/view/View;)I

    move-result v2

    invoke-static {p2, v1, v2}, Landroid/support/v7/widget/a;->dry(III)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    return-void
.end method

.method dpw(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqu()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_2

    invoke-static {p1}, Landroid/support/v4/view/a/i;->dNQ(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v1

    :goto_0
    if-nez v1, :cond_1

    :goto_1
    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->dYj:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYj:I

    const/4 v0, 0x1

    return v0

    :cond_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method dpx(Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    if-ge v0, v1, :cond_0

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    :cond_0
    if-nez p1, :cond_1

    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->dYb:Z

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    if-ne v0, v1, :cond_3

    if-eqz p1, :cond_2

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYb:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doG()V

    :cond_2
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    if-nez v0, :cond_3

    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->dYb:Z

    :cond_3
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    return-void
.end method

.method public dpy(I)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/a;->dNU(I)V

    return-void
.end method

.method public dpz(Landroid/view/View;)I
    .locals 1

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getAdapterPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method dqA(IIZ)V
    .locals 7

    const/4 v6, 0x1

    add-int v1, p1, p2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    iget v4, v3, Landroid/support/v7/widget/p;->mPosition:I

    if-lt v4, v1, :cond_1

    neg-int v4, p2

    invoke-virtual {v3, v4, p3}, Landroid/support/v7/widget/p;->offsetPosition(IZ)V

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v6, v3, Landroid/support/v7/widget/e;->dZG:Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v4, v3, Landroid/support/v7/widget/p;->mPosition:I

    if-lt v4, p1, :cond_0

    add-int/lit8 v4, p1, -0x1

    neg-int v5, p2

    invoke-virtual {v3, v4, v5, p3}, Landroid/support/v7/widget/p;->flagRemovedAndOffsetPosition(IIZ)V

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v6, v3, Landroid/support/v7/widget/e;->dZG:Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/j;->dtX(IIZ)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    return-void
.end method

.method dqB(I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a;->dtd(I)V

    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dqM(I)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mScrollListener:Landroid/support/v7/widget/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mScrollListener:Landroid/support/v7/widget/m;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/widget/m;->onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYe:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/m;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/widget/m;->onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method dqD()V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXS:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYl:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYa:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/z;->dPV(Landroid/view/View;Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXS:Z

    :cond_0
    return-void
.end method

.method dqE(IILjava/lang/Object;)V
    .locals 6

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v2

    add-int v3, p1, p2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v5, v4, Landroid/support/v7/widget/p;->mPosition:I

    if-lt v5, p1, :cond_0

    iget v5, v4, Landroid/support/v7/widget/p;->mPosition:I

    if-ge v5, v3, :cond_0

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/p;->addFlags(I)V

    invoke-virtual {v4, p3}, Landroid/support/v7/widget/p;->addChangePayload(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const/4 v4, 0x1

    iput-boolean v4, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eae:Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/j;->duc(II)V

    return-void
.end method

.method public dqF(I)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public dqG(Landroid/support/v7/widget/o;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public dqH(Landroid/support/v7/widget/o;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method dqI()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const/4 v3, 0x1

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eae:Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->duj()V

    return-void
.end method

.method public dqJ()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    const-string/jumbo v1, "Cannot invalidate item decorations during a scroll or layout"

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drm(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqI()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    return-void
.end method

.method public dqK(Landroid/view/View;)Landroid/view/View;
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v1, p1

    :goto_0
    if-eqz v0, :cond_0

    if-eq v0, p0, :cond_0

    instance-of v3, v0, Landroid/view/View;

    if-eqz v3, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    :cond_0
    if-ne v0, p0, :cond_1

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    move-object v0, v2

    goto :goto_1
.end method

.method public dqM(I)V
    .locals 0

    return-void
.end method

.method dqN()V
    .locals 2

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->mFirstLayoutComplete:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "RV FullInvalidate"

    invoke-static {v0}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doG()V

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwN()Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/I;->dws(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/I;->dws(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_6

    const-string/jumbo v0, "RV PartialInvalidate"

    invoke-static {v0}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqP()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwC()V

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYb:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqx()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doG()V

    :cond_3
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpS()V

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    :cond_4
    :goto_1
    return-void

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwH()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwN()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "RV FullInvalidate"

    invoke-static {v0}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doG()V

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    goto :goto_1
.end method

.method public dqO(II)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v7/widget/RecyclerView;->doB(IILandroid/view/animation/Interpolator;)V

    return-void
.end method

.method dqP()V
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYV:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYV:I

    return-void
.end method

.method dqR()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method public dqa(Landroid/support/v7/widget/d;I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    const-string/jumbo v1, "Cannot add item decoration during a scroll  or layout"

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drm(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    :cond_1
    if-gez p2, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqI()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method dqb()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/support/v7/widget/p;->saveOldPosition()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method dqd()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method dqf()V
    .locals 5

    const/4 v4, -0x1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mPendingAccessibilityImportanceChange:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mPendingAccessibilityImportanceChange:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v2, v0, Landroid/support/v7/widget/p;->mPendingAccessibilityState:I

    if-eq v2, v4, :cond_0

    iget-object v3, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-static {v3, v2}, Landroid/support/v4/view/z;->dPL(Landroid/view/View;I)V

    iput v4, v0, Landroid/support/v7/widget/p;->mPendingAccessibilityState:I

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mPendingAccessibilityImportanceChange:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public dqh(II)V
    .locals 0

    return-void
.end method

.method dqi(IILandroid/view/MotionEvent;)Z
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqN()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqP()V

    const-string/jumbo v0, "RV Scroll"

    invoke-static {v0}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->doQ(Landroid/support/v7/widget/e;)V

    if-eqz p1, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v7/widget/a;->dsM(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v2

    sub-int v3, p1, v2

    :goto_0
    if-eqz p2, :cond_a

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, p2, v1, v4}, Landroid/support/v7/widget/a;->dsJ(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v0

    sub-int v1, p2, v0

    :goto_1
    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dql()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpS()V

    invoke-virtual {p0, v6}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    move v4, v1

    move v1, v2

    move v2, v0

    :goto_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    :cond_0
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Landroid/support/v7/widget/RecyclerView;->dpK(IIII[II)Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v3, v3, v6

    sub-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v3, v3, v7

    sub-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    if-eqz p3, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v0, v0, v6

    int-to-float v0, v0

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v3, v3, v7

    int-to-float v3, v3

    invoke-virtual {p3, v0, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    aget v3, v0, v6

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v4, v4, v6

    add-int/2addr v3, v4

    aput v3, v0, v6

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    aget v3, v0, v7

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v4, v4, v7

    add-int/2addr v3, v4

    aput v3, v0, v7

    :cond_2
    :goto_3
    if-nez v1, :cond_3

    if-eqz v2, :cond_4

    :cond_3
    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/RecyclerView;->dpP(II)V

    :cond_4
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    :cond_5
    if-nez v1, :cond_6

    if-eqz v2, :cond_7

    :cond_6
    move v6, v7

    :cond_7
    return v6

    :cond_8
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getOverScrollMode()I

    move-result v0

    const/4 v5, 0x2

    if-eq v0, v5, :cond_2

    if-eqz p3, :cond_9

    const/16 v0, 0x2002

    invoke-static {p3, v0}, Landroid/support/v4/view/S;->dQU(Landroid/view/MotionEvent;I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_9

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    int-to-float v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    int-to-float v4, v4

    invoke-direct {p0, v0, v3, v5, v4}, Landroid/support/v7/widget/RecyclerView;->dpC(FFFF)V

    :cond_9
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->dpt(II)V

    goto :goto_3

    :cond_a
    move v0, v6

    move v1, v6

    goto/16 :goto_1

    :cond_b
    move v2, v6

    move v3, v6

    goto/16 :goto_0

    :cond_c
    move v2, v6

    move v1, v6

    move v4, v6

    move v3, v6

    goto/16 :goto_2
.end method

.method dql()V
    .locals 7

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvZ()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvQ(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v4, v3, Landroid/support/v7/widget/p;->mShadowingHolder:Landroid/support/v7/widget/p;

    if-eqz v4, :cond_1

    iget-object v3, v3, Landroid/support/v7/widget/p;->mShadowingHolder:Landroid/support/v7/widget/p;

    iget-object v3, v3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v5

    if-ne v4, v5, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v5

    if-eq v2, v5, :cond_1

    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v4

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/view/View;->layout(IIII)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public dqn(Landroid/support/v7/widget/l;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXH:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYK:Landroid/support/v7/widget/l;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYK:Landroid/support/v7/widget/l;

    :cond_0
    return-void
.end method

.method public dqo(I)V
    .locals 2

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpq()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_1

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "Cannot scroll to position a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a;->dsA(I)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    return-void
.end method

.method dqp(II)V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    iget v4, v3, Landroid/support/v7/widget/p;->mPosition:I

    if-lt v4, p1, :cond_0

    invoke-virtual {v3, p2, v1}, Landroid/support/v7/widget/p;->offsetPosition(IZ)V

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/support/v7/widget/e;->dZG:Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/j;->duu(II)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    return-void
.end method

.method public dqq(II)Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/a;->dNX(II)Z

    move-result v0

    return v0
.end method

.method dqt(Landroid/view/View;)V
    .locals 2

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->doM(Landroid/view/View;)V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/b;->onViewAttachedToWindow(Landroid/support/v7/widget/p;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXN:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/o;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/o;->dnt(Landroid/view/View;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dqu()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->dYV:I

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public dqv(I)Landroid/support/v7/widget/p;
    .locals 5

    const/4 v1, 0x0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvO()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/G;->dvK(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->dpf(Landroid/support/v7/widget/p;)I

    move-result v4

    if-ne v4, p1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    iget-object v4, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/G;->dvL(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    return-object v0

    :cond_3
    return-object v1
.end method

.method public dqy(Landroid/view/View;)I
    .locals 1

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/d;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, p1, p0, v4}, Landroid/support/v7/widget/d;->Cm(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    :goto_1
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v3

    neg-int v3, v3

    add-int/2addr v0, v3

    int-to-float v0, v0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYc:Landroid/widget/EdgeEffect;

    invoke-virtual {v0, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    :goto_2
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :goto_3
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    if-eqz v2, :cond_b

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXY:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    :goto_4
    or-int/2addr v0, v2

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_2
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    if-eqz v2, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    if-eqz v2, :cond_c

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    :goto_5
    const/high16 v5, 0x42b40000    # 90.0f

    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v2, v2

    int-to-float v2, v2

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    if-eqz v2, :cond_d

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXV:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    :goto_6
    or-int/2addr v0, v2

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_3
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    if-eqz v2, :cond_5

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {v2}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    const/high16 v3, 0x43340000    # 180.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    if-eqz v3, :cond_e

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v4

    neg-int v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_7
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    if-eqz v3, :cond_4

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXT:Landroid/widget/EdgeEffect;

    invoke-virtual {v1, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v1

    :cond_4
    or-int/2addr v0, v1

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_5
    if-nez v0, :cond_6

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v1, :cond_6

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_6

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v1}, Landroid/support/v7/widget/u;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x1

    :cond_6
    if-eqz v0, :cond_7

    invoke-static {p0}, Landroid/support/v4/view/z;->dPF(Landroid/view/View;)V

    :cond_7
    return-void

    :cond_8
    move v0, v1

    goto/16 :goto_3

    :cond_9
    move v0, v1

    goto/16 :goto_1

    :cond_a
    move v0, v1

    goto/16 :goto_2

    :cond_b
    move v2, v1

    goto/16 :goto_4

    :cond_c
    move v2, v1

    goto/16 :goto_5

    :cond_d
    move v2, v1

    goto :goto_6

    :cond_e
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_7

    :cond_f
    move v0, v1

    goto/16 :goto_3
.end method

.method public drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 8

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/a;->dsj(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqu()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v4

    if-eqz v0, :cond_d

    if-eq p2, v7, :cond_1

    if-ne p2, v3, :cond_d

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v0

    if-eqz v0, :cond_16

    if-ne p2, v7, :cond_4

    const/16 v0, 0x82

    :goto_1
    invoke-virtual {v4, p0, p1, v0}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_5

    move v2, v3

    :goto_2
    sget-boolean v5, Landroid/support/v7/widget/RecyclerView;->dYw:Z

    if-eqz v5, :cond_15

    move p2, v0

    move v0, v2

    :goto_3
    if-nez v0, :cond_6

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v2}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v2

    if-eqz v2, :cond_14

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->drR()I

    move-result v0

    if-ne v0, v3, :cond_7

    move v2, v3

    :goto_4
    if-ne p2, v7, :cond_8

    move v0, v3

    :goto_5
    xor-int/2addr v0, v2

    if-eqz v0, :cond_9

    const/16 v0, 0x42

    :goto_6
    invoke-virtual {v4, p0, p1, v0}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_a

    :goto_7
    sget-boolean v2, Landroid/support/v7/widget/RecyclerView;->dYw:Z

    if-eqz v2, :cond_2

    move p2, v0

    :cond_2
    :goto_8
    if-eqz v3, :cond_c

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqN()V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dqK(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_b

    return-object v6

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const/16 v0, 0x21

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_2

    :cond_6
    move v3, v0

    goto :goto_8

    :cond_7
    move v2, v1

    goto :goto_4

    :cond_8
    move v0, v1

    goto :goto_5

    :cond_9
    const/16 v0, 0x11

    goto :goto_6

    :cond_a
    move v3, v1

    goto :goto_7

    :cond_b
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, p1, p2, v2, v3}, Landroid/support/v7/widget/a;->dsq(Landroid/view/View;ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    :cond_c
    invoke-virtual {v4, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    :goto_9
    if-eqz v0, :cond_11

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_11

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_10

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_d
    invoke-virtual {v4, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_e

    if-eqz v0, :cond_13

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqN()V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dqK(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_f

    return-object v6

    :cond_e
    move-object v0, v2

    goto :goto_9

    :cond_f
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, p1, p2, v2, v3}, Landroid/support/v7/widget/a;->dsq(Landroid/view/View;ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    goto :goto_9

    :cond_10
    invoke-direct {p0, v0, v6}, Landroid/support/v7/widget/RecyclerView;->dph(Landroid/view/View;Landroid/view/View;)V

    return-object p1

    :cond_11
    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/RecyclerView;->dpj(Landroid/view/View;Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_12

    :goto_a
    return-object v0

    :cond_12
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_a

    :cond_13
    move-object v0, v2

    goto :goto_9

    :cond_14
    move v3, v0

    goto :goto_8

    :cond_15
    move v0, v2

    goto/16 :goto_3

    :cond_16
    move v0, v1

    goto/16 :goto_3
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "RecyclerView has no LayoutManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->generateDefaultLayoutParams()Landroid/support/v7/widget/RecyclerView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "RecyclerView has no LayoutManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/a;->drf(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/RecyclerView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "RecyclerView has no LayoutManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/RecyclerView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/support/v7/widget/b;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    return-object v0
.end method

.method public getBaseline()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->getBaseline()I

    move-result v0

    return v0

    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->getBaseline()I

    move-result v0

    return v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXP:Landroid/support/v7/widget/v;

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->getChildDrawingOrder(II)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXP:Landroid/support/v7/widget/v;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/widget/v;->dos(II)I

    move-result v0

    return v0
.end method

.method public getClipToPadding()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    return v0
.end method

.method public getCompatAccessibilityDelegate()Landroid/support/v7/widget/F;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYr:Landroid/support/v7/widget/F;

    return-object v0
.end method

.method public getItemAnimator()Landroid/support/v7/widget/u;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    return-object v0
.end method

.method public getItemDecorationCount()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getLayoutManager()Landroid/support/v7/widget/a;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    return-object v0
.end method

.method public getMaxFlingVelocity()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dXX:I

    return v0
.end method

.method public getMinFlingVelocity()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYd:I

    return v0
.end method

.method getNanoTime()J
    .locals 2

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYu:Z

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getOnFlingListener()Landroid/support/v7/widget/s;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYh:Landroid/support/v7/widget/s;

    return-object v0
.end method

.method public getPreserveFocusAfterLayout()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYy:Z

    return v0
.end method

.method public getRecycledViewPool()Landroid/support/v7/widget/i;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->getRecycledViewPool()Landroid/support/v7/widget/i;

    move-result-object v0

    return-object v0
.end method

.method public getScrollState()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYF:I

    return v0
.end method

.method public hasNestedScrollingParent()Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/a;->hasNestedScrollingParent()Z

    move-result v0

    return v0
.end method

.method initFastScroller(Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;)V
    .locals 9

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Trying to set fast scroller without both required drawables."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v0, Landroid/support/v7/widget/FastScroller;

    sget v2, Landroid/support/v7/a/a;->dFF:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sget v2, Landroid/support/v7/a/a;->dFH:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sget v2, Landroid/support/v7/a/a;->dFG:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v8}, Landroid/support/v7/widget/FastScroller;-><init>(Landroid/support/v7/widget/RecyclerView;Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/StateListDrawable;Landroid/graphics/drawable/Drawable;III)V

    return-void
.end method

.method public isAttachedToWindow()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYl:Z

    return v0
.end method

.method public isNestedScrollingEnabled()Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/a;->isNestedScrollingEnabled()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->dYV:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYl:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->mFirstLayoutComplete:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isLayoutRequested()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->mFirstLayoutComplete:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/a;->dsz(Landroid/support/v7/widget/RecyclerView;)V

    :cond_0
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->dXS:Z

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYu:Z

    if-eqz v0, :cond_2

    sget-object v0, Landroid/support/v7/widget/J;->eaW:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/J;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v7/widget/J;

    invoke-direct {v0}, Landroid/support/v7/widget/J;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    invoke-static {p0}, Landroid/support/v4/view/z;->dQb(Landroid/view/View;)Landroid/view/Display;

    move-result-object v0

    const/high16 v1, 0x42700000    # 60.0f

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->isInEditMode()Z

    move-result v2

    if-nez v2, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/Display;->getRefreshRate()F

    move-result v0

    const/high16 v2, 0x41f00000    # 30.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_5

    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    const v2, 0x4e6e6b28    # 1.0E9f

    div-float v0, v2, v0

    float-to-long v2, v0

    iput-wide v2, v1, Landroid/support/v7/widget/J;->eaU:J

    sget-object v0, Landroid/support/v7/widget/J;->eaW:Ljava/lang/ThreadLocal;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/J;->dxa(Landroid/support/v7/widget/RecyclerView;)V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->dvh()V

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpq()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYl:Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/a;->drz(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/j;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mPendingAccessibilityImportanceChange:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYa:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v0}, Landroid/support/v7/widget/H;->onDetach()V

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYu:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/J;->dwS(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    :cond_2
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/d;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, p1, p0, v3}, Landroid/support/v7/widget/d;->cde(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_0

    return v4

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    if-eqz v0, :cond_1

    return v4

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    neg-float v0, v0

    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v2}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    move v5, v2

    move v2, v0

    move v0, v5

    :goto_1
    cmpl-float v3, v2, v1

    if-nez v3, :cond_2

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_3

    :cond_2
    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->dXB:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->dXL:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1, p1}, Landroid/support/v7/widget/RecyclerView;->dqi(IILandroid/view/MotionEvent;)Z

    :cond_3
    return v4

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v0

    move v0, v1

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    const/high16 v2, 0x400000

    and-int/2addr v0, v2

    if-eqz v0, :cond_9

    const/16 v0, 0x1a

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v2}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v2

    if-eqz v2, :cond_7

    neg-float v0, v0

    move v2, v0

    move v0, v1

    goto :goto_1

    :cond_7
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v2}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v1

    goto :goto_1

    :cond_8
    move v0, v1

    move v2, v1

    goto :goto_1

    :cond_9
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    if-eqz v0, :cond_0

    return v2

    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpo(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpG()V

    return v1

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_2

    return v2

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    if-nez v0, :cond_3

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v3

    packed-switch v0, :pswitch_data_0

    :cond_4
    :goto_0
    :pswitch_0
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYF:I

    if-ne v0, v1, :cond_10

    :goto_1
    return v1

    :pswitch_1
    iput v2, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYq:Z

    if-eqz v0, :cond_5

    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->dYq:Z

    :cond_5
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXR:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYD:I

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYF:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->setScrollState(I)V

    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    aput v2, v3, v1

    aput v2, v0, v2

    if-eqz v4, :cond_12

    move v0, v1

    :goto_2
    if-eqz v5, :cond_7

    or-int/lit8 v0, v0, 0x2

    :cond_7
    invoke-virtual {p0, v0, v2}, Landroid/support/v7/widget/RecyclerView;->dqq(II)Z

    goto :goto_0

    :pswitch_2
    iput v2, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXR:I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYD:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_8

    const-string/jumbo v0, "RecyclerView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Error processing scroll; pointer index for id "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_8
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    add-float/2addr v3, v7

    float-to-int v6, v3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    add-float/2addr v0, v7

    float-to-int v7, v0

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYF:I

    if-eq v0, v1, :cond_4

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dXR:I

    sub-int v0, v6, v0

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->dYD:I

    sub-int v3, v7, v3

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    if-ne v8, v1, :cond_c

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->dXE:I

    if-le v0, v8, :cond_a

    move v0, v1

    :goto_3
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->dXE:I

    if-le v3, v8, :cond_b

    move v3, v1

    :goto_4
    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->dXE:I

    :goto_5
    if-eqz v4, :cond_f

    if-eqz v0, :cond_11

    iput v6, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    move v0, v1

    :goto_6
    if-eqz v5, :cond_9

    if-eqz v3, :cond_9

    iput v7, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    move v0, v1

    :cond_9
    if-eqz v0, :cond_4

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->setScrollState(I)V

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_3

    :cond_b
    move v3, v2

    goto :goto_4

    :cond_c
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    if-le v0, v8, :cond_d

    move v0, v1

    :goto_7
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    if-le v3, v8, :cond_e

    move v3, v1

    :goto_8
    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    goto :goto_5

    :cond_d
    move v0, v2

    goto :goto_7

    :cond_e
    move v3, v2

    goto :goto_8

    :cond_f
    move v0, v2

    goto :goto_6

    :pswitch_4
    iput v2, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpL(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    :pswitch_5
    iput v2, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->dpy(I)V

    goto/16 :goto_0

    :pswitch_6
    iput v2, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpG()V

    goto/16 :goto_0

    :cond_10
    move v1, v2

    goto/16 :goto_1

    :cond_11
    move v0, v2

    goto :goto_6

    :cond_12
    move v0, v2

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    const-string/jumbo v0, "RV OnLayout"

    invoke-static {v0}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doG()V

    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->mFirstLayoutComplete:Z

    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v2, :cond_0

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->dpv(II)V

    return-void

    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-boolean v2, v2, Landroid/support/v7/widget/a;->dZd:Z

    if-eqz v2, :cond_6

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    if-ne v2, v5, :cond_1

    if-ne v3, v5, :cond_1

    move v0, v1

    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v2, v3, v4, p1, p2}, Landroid/support/v7/widget/a;->dsh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;II)V

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-nez v0, :cond_3

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget v0, v0, Landroid/support/v7/widget/e;->dZI:I

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqL()V

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/a;->dsQ(II)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v1, v0, Landroid/support/v7/widget/e;->dZK:Z

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->doI()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/a;->dso(II)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dqY()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/a;->dsQ(II)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v1, v0, Landroid/support/v7/widget/e;->dZK:Z

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->doI()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/a;->dso(II)V

    :cond_5
    :goto_0
    return-void

    :cond_6
    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->dXG:Z

    if-eqz v2, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, v1, v2, p1, p2}, Landroid/support/v7/widget/a;->dsh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;II)V

    return-void

    :cond_7
    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->dYL:Z

    if-eqz v2, :cond_a

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqP()V

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqm()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpS()V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v2, v2, Landroid/support/v7/widget/e;->dZJ:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v1, v2, Landroid/support/v7/widget/e;->dZD:Z

    :goto_1
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYL:Z

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    :cond_8
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v1, :cond_b

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v2}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v2

    iput v2, v1, Landroid/support/v7/widget/e;->dZH:I

    :goto_2
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v1, v2, v3, p1, p2}, Landroid/support/v7/widget/a;->dsh(Landroid/support/v7/widget/j;Landroid/support/v7/widget/e;II)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v0, v1, Landroid/support/v7/widget/e;->dZD:Z

    goto :goto_0

    :cond_9
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v1}, Landroid/support/v7/widget/I;->dwP()V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput-boolean v0, v1, Landroid/support/v7/widget/e;->dZD:Z

    goto :goto_1

    :cond_a
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v1, v1, Landroid/support/v7/widget/e;->dZJ:Z

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    return-void

    :cond_b
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iput v0, v1, Landroid/support/v7/widget/e;->dZH:I

    goto :goto_2
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqu()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView$SavedState;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    :cond_0
    check-cast p1, Landroid/support/v7/widget/RecyclerView$SavedState;

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->dYx:Landroid/support/v7/widget/RecyclerView$SavedState;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYx:Landroid/support/v7/widget/RecyclerView$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$SavedState;->dNY()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYx:Landroid/support/v7/widget/RecyclerView$SavedState;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$SavedState;->eap:Landroid/os/Parcelable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYx:Landroid/support/v7/widget/RecyclerView$SavedState;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$SavedState;->eap:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/support/v7/widget/RecyclerView$SavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/RecyclerView$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYx:Landroid/support/v7/widget/RecyclerView$SavedState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYx:Landroid/support/v7/widget/RecyclerView$SavedState;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$SavedState;->duJ(Landroid/support/v7/widget/RecyclerView$SavedState;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$SavedState;->eap:Landroid/os/Parcelable;

    goto :goto_0

    :cond_1
    iput-object v2, v0, Landroid/support/v7/widget/RecyclerView$SavedState;->eap:Landroid/os/Parcelable;

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doR()V

    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    const/4 v1, 0x0

    const/high16 v11, 0x3f000000    # 0.5f

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYq:Z

    if-eqz v0, :cond_1

    :cond_0
    return v5

    :cond_1
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpr(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpG()V

    return v6

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v0, :cond_3

    return v5

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v7

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    if-nez v0, :cond_4

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    :cond_4
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v2

    if-nez v0, :cond_5

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    aput v5, v4, v6

    aput v5, v3, v5

    :cond_5
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    aget v3, v3, v5

    int-to-float v3, v3

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    aget v4, v4, v6

    int-to-float v4, v4

    invoke-virtual {v9, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    packed-switch v0, :pswitch_data_0

    :cond_6
    :goto_0
    :pswitch_0
    if-nez v5, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    invoke-virtual {v0, v9}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_7
    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    return v6

    :pswitch_1
    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXR:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYD:I

    if-eqz v7, :cond_20

    move v0, v6

    :goto_1
    if-eqz v8, :cond_8

    or-int/lit8 v0, v0, 0x2

    :cond_8
    invoke-virtual {p0, v0, v5}, Landroid/support/v7/widget/RecyclerView;->dqq(II)Z

    goto :goto_0

    :pswitch_2
    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXR:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYD:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_9

    const-string/jumbo v0, "RecyclerView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Error processing scroll; pointer index for id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v5

    :cond_9
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v1, v11

    float-to-int v10, v1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    add-float/2addr v0, v11

    float-to-int v11, v0

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    sub-int v1, v0, v10

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    sub-int v2, v0, v11

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dYi:[I

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/RecyclerView;->dpm(II[I[II)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYi:[I

    aget v0, v0, v5

    sub-int/2addr v1, v0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYi:[I

    aget v0, v0, v6

    sub-int/2addr v2, v0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v0, v0, v5

    int-to-float v0, v0

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v3, v3, v6

    int-to-float v3, v3

    invoke-virtual {v9, v0, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    aget v3, v0, v5

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v4, v4, v5

    add-int/2addr v3, v4

    aput v3, v0, v5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXM:[I

    aget v3, v0, v6

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v4, v4, v6

    add-int/2addr v3, v4

    aput v3, v0, v6

    :cond_a
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    if-ne v0, v6, :cond_10

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->dXE:I

    if-le v0, v3, :cond_e

    move v0, v6

    :goto_2
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->dXE:I

    if-le v3, v4, :cond_f

    move v3, v6

    :goto_3
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->dXE:I

    move v13, v4

    move v4, v0

    move v0, v13

    :goto_4
    iget v12, p0, Landroid/support/v7/widget/RecyclerView;->dYF:I

    if-eq v12, v6, :cond_b

    if-eqz v7, :cond_13

    if-eqz v4, :cond_1f

    if-lez v1, :cond_14

    sub-int/2addr v1, v0

    :goto_5
    move v4, v6

    :goto_6
    if-eqz v8, :cond_15

    if-eqz v3, :cond_1e

    if-lez v2, :cond_16

    sub-int v0, v2, v0

    :goto_7
    move v2, v0

    move v0, v6

    :goto_8
    if-eqz v0, :cond_b

    invoke-virtual {p0, v6}, Landroid/support/v7/widget/RecyclerView;->setScrollState(I)V

    :cond_b
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYF:I

    if-ne v0, v6, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v0, v0, v5

    sub-int v0, v10, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXK:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXW:[I

    aget v0, v0, v6

    sub-int v0, v11, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dYA:I

    if-eqz v7, :cond_17

    move v3, v1

    :goto_9
    if-eqz v8, :cond_18

    move v0, v2

    :goto_a
    invoke-virtual {p0, v3, v0, v9}, Landroid/support/v7/widget/RecyclerView;->dqi(IILandroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_c
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    if-eqz v0, :cond_6

    if-nez v1, :cond_d

    if-eqz v2, :cond_6

    :cond_d
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    invoke-virtual {v0, p0, v1, v2}, Landroid/support/v7/widget/J;->dwY(Landroid/support/v7/widget/RecyclerView;II)V

    goto/16 :goto_0

    :cond_e
    move v0, v5

    goto :goto_2

    :cond_f
    move v3, v5

    goto :goto_3

    :cond_10
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    if-le v0, v3, :cond_11

    move v0, v6

    :goto_b
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    if-le v3, v4, :cond_12

    move v3, v6

    :goto_c
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    move v13, v4

    move v4, v0

    move v0, v13

    goto :goto_4

    :cond_11
    move v0, v5

    goto :goto_b

    :cond_12
    move v3, v5

    goto :goto_c

    :cond_13
    move v4, v5

    goto :goto_6

    :cond_14
    add-int/2addr v1, v0

    goto :goto_5

    :cond_15
    move v0, v4

    goto :goto_8

    :cond_16
    add-int/2addr v0, v2

    goto :goto_7

    :cond_17
    move v3, v5

    goto :goto_9

    :cond_18
    move v0, v5

    goto :goto_a

    :pswitch_4
    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpL(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    :pswitch_5
    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    invoke-virtual {v0, v9}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->dXX:I

    int-to-float v2, v2

    const/16 v3, 0x3e8

    invoke-virtual {v0, v3, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    if-eqz v7, :cond_1b

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    neg-float v0, v0

    move v2, v0

    :goto_d
    if-eqz v8, :cond_1c

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXQ:Landroid/view/VelocityTracker;

    iget v3, p0, Landroid/support/v7/widget/RecyclerView;->dXJ:I

    invoke-virtual {v0, v3}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v0

    neg-float v0, v0

    :goto_e
    cmpl-float v3, v2, v1

    if-nez v3, :cond_19

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1d

    :cond_19
    float-to-int v1, v2

    float-to-int v0, v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/RecyclerView;->dpg(II)Z

    move-result v0

    :goto_f
    if-nez v0, :cond_1a

    invoke-virtual {p0, v5}, Landroid/support/v7/widget/RecyclerView;->setScrollState(I)V

    :cond_1a
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpu()V

    move v5, v6

    goto/16 :goto_0

    :cond_1b
    move v2, v1

    goto :goto_d

    :cond_1c
    move v0, v1

    goto :goto_e

    :cond_1d
    move v0, v5

    goto :goto_f

    :pswitch_6
    iput v5, p0, Landroid/support/v7/widget/RecyclerView;->dYs:I

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dpG()V

    goto/16 :goto_0

    :cond_1e
    move v0, v4

    goto/16 :goto_8

    :cond_1f
    move v4, v5

    goto/16 :goto_6

    :cond_20
    move v0, v5

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected removeDetachedView(Landroid/view/View;Z)V
    .locals 4

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isTmpDetached()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->clearTmpDetachFlag()V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpB(Landroid/view/View;)V

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->removeDetachedView(Landroid/view/View;Z)V

    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Called removeDetachedView with a view which is not flagged as tmp detached."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0, p0, v1, p1, p2}, Landroid/support/v7/widget/a;->drM(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->dph(Landroid/view/View;Landroid/view/View;)V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/v7/widget/a;->dsL(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;Z)Z

    move-result v0

    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXH:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/l;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/l;->doq(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    return-void
.end method

.method public requestLayout()V
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYf:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYb:Z

    goto :goto_0
.end method

.method public scrollBy(II)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v1, :cond_0

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    if-eqz v1, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v2}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v2

    if-nez v1, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    if-eqz v1, :cond_4

    :goto_0
    if-eqz v2, :cond_5

    :goto_1
    invoke-virtual {p0, p1, p2, v3}, Landroid/support/v7/widget/RecyclerView;->dqi(IILandroid/view/MotionEvent;)Z

    :cond_3
    return-void

    :cond_4
    move p1, v0

    goto :goto_0

    :cond_5
    move p2, v0

    goto :goto_1
.end method

.method public scrollTo(II)V
    .locals 2

    const-string/jumbo v0, "RecyclerView"

    const-string/jumbo v1, "RecyclerView does not support scrolling to an absolute position. Use scrollToPosition instead"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dpw(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method public setAccessibilityDelegateCompat(Landroid/support/v7/widget/F;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->dYr:Landroid/support/v7/widget/F;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYr:Landroid/support/v7/widget/F;

    invoke-static {p0, v0}, Landroid/support/v4/view/z;->dPu(Landroid/view/View;Landroid/support/v4/view/d;)V

    return-void
.end method

.method public setAdapter(Landroid/support/v7/widget/b;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutFrozen(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapterInternal(Landroid/support/v7/widget/b;ZZ)V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    return-void
.end method

.method public setChildDrawingOrderCallback(Landroid/support/v7/widget/v;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXP:Landroid/support/v7/widget/v;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->dXP:Landroid/support/v7/widget/v;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dXP:Landroid/support/v7/widget/v;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setChildrenDrawingOrderEnabled(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setChildImportantForAccessibilityInternal(Landroid/support/v7/widget/p;I)Z
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dqu()Z

    move-result v0

    if-eqz v0, :cond_0

    iput p2, p1, Landroid/support/v7/widget/p;->mPendingAccessibilityState:I

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mPendingAccessibilityImportanceChange:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-static {v0, p2}, Landroid/support/v4/view/z;->dPL(Landroid/view/View;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setClipToPadding(Z)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doR()V

    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->dYR:Z

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->mFirstLayoutComplete:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    :cond_1
    return-void
.end method

.method setDataSetChangedAfterLayout()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->doL()V

    return-void
.end method

.method public setHasFixedSize(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->dXG:Z

    return-void
.end method

.method public setItemAnimator(Landroid/support/v7/widget/u;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->dvh()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/u;->duQ(Landroid/support/v7/widget/D;)V

    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dXD:Landroid/support/v7/widget/D;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/u;->duQ(Landroid/support/v7/widget/D;)V

    :cond_1
    return-void
.end method

.method public setItemViewCacheSize(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/j;->dtY(I)V

    return-void
.end method

.method public setLayoutFrozen(Z)V
    .locals 9

    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    if-eq p1, v0, :cond_1

    const-string/jumbo v0, "Do not setLayoutFrozen in layout or scroll"

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->doZ(Ljava/lang/String;)V

    if-nez p1, :cond_2

    iput-boolean v7, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYb:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    :cond_0
    iput-boolean v7, p0, Landroid/support/v7/widget/RecyclerView;->dYb:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->dXA:Z

    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->dYq:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpq()V

    goto :goto_0
.end method

.method public setLayoutManager(Landroid/support/v7/widget/a;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->dpq()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->dvh()V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->dqX(Landroid/support/v7/widget/j;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drx(Landroid/support/v7/widget/j;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->clear()V

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYl:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/a;->drz(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/j;)V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/a;->drU(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v2, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0}, Landroid/support/v7/widget/G;->dvY()V

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz p1, :cond_5

    iget-object v0, p1, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "LayoutManager "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is already attached to a RecyclerView:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v7/widget/a;->dZg:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->clear()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/a;->drU(Landroid/support/v7/widget/RecyclerView;)V

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->dYl:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/a;->dsz(Landroid/support/v7/widget/RecyclerView;)V

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0}, Landroid/support/v7/widget/j;->dua()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    return-void
.end method

.method public setNestedScrollingEnabled(Z)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/a;->setNestedScrollingEnabled(Z)V

    return-void
.end method

.method public setOnFlingListener(Landroid/support/v7/widget/s;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->dYh:Landroid/support/v7/widget/s;

    return-void
.end method

.method public setOnScrollListener(Landroid/support/v7/widget/m;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->mScrollListener:Landroid/support/v7/widget/m;

    return-void
.end method

.method public setPreserveFocusAfterLayout(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->dYy:Z

    return-void
.end method

.method public setRecycledViewPool(Landroid/support/v7/widget/i;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/j;->setRecycledViewPool(Landroid/support/v7/widget/i;)V

    return-void
.end method

.method public setRecyclerListener(Landroid/support/v7/widget/n;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->dYt:Landroid/support/v7/widget/n;

    return-void
.end method

.method setScrollState(I)V
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dYF:I

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput p1, p0, Landroid/support/v7/widget/RecyclerView;->dYF:I

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->dqk()V

    :cond_1
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dqB(I)V

    return-void
.end method

.method public setScrollingTouchSlop(I)V
    .locals 6

    const-wide v4, 0x3fd3333333333333L    # 0.3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    const-string/jumbo v1, "RecyclerView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "setScrollingTouchSlop(): bad argument constant "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "; using default value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :pswitch_0
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXE:I

    :goto_0
    return-void

    :pswitch_1
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->dXO:I

    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->dXE:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setViewCacheExtension(Landroid/support/v7/widget/k;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/j;->setViewCacheExtension(Landroid/support/v7/widget/k;)V

    return-void
.end method

.method public startNestedScroll(I)Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/view/a;->startNestedScroll(I)Z

    move-result v0

    return v0
.end method

.method public stopNestedScroll()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->getScrollingChildHelper()Landroid/support/v4/view/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/a;->stopNestedScroll()V

    return-void
.end method
