.class final Landroid/support/v7/widget/ai;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DefaultItemAnimator.java"


# instance fields
.field final synthetic eci:I

.field final synthetic ecj:Landroid/support/v7/widget/Z;

.field final synthetic eck:I

.field final synthetic ecl:Landroid/support/v7/widget/p;

.field final synthetic ecm:Landroid/view/View;

.field final synthetic ecn:Landroid/view/ViewPropertyAnimator;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/Z;Landroid/support/v7/widget/p;ILandroid/view/View;ILandroid/view/ViewPropertyAnimator;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/ai;->ecj:Landroid/support/v7/widget/Z;

    iput-object p2, p0, Landroid/support/v7/widget/ai;->ecl:Landroid/support/v7/widget/p;

    iput p3, p0, Landroid/support/v7/widget/ai;->eck:I

    iput-object p4, p0, Landroid/support/v7/widget/ai;->ecm:Landroid/view/View;

    iput p5, p0, Landroid/support/v7/widget/ai;->eci:I

    iput-object p6, p0, Landroid/support/v7/widget/ai;->ecn:Landroid/view/ViewPropertyAnimator;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Landroid/support/v7/widget/ai;->eck:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ai;->ecm:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/ai;->eci:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/ai;->ecm:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    :cond_1
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ai;->ecn:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Landroid/support/v7/widget/ai;->ecj:Landroid/support/v7/widget/Z;

    iget-object v1, p0, Landroid/support/v7/widget/ai;->ecl:Landroid/support/v7/widget/p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Z;->dxQ(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/ai;->ecj:Landroid/support/v7/widget/Z;

    iget-object v0, v0, Landroid/support/v7/widget/Z;->ebD:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/ai;->ecl:Landroid/support/v7/widget/p;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/ai;->ecj:Landroid/support/v7/widget/Z;

    invoke-virtual {v0}, Landroid/support/v7/widget/Z;->dxw()V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ai;->ecj:Landroid/support/v7/widget/Z;

    iget-object v1, p0, Landroid/support/v7/widget/ai;->ecl:Landroid/support/v7/widget/p;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Z;->dxP(Landroid/support/v7/widget/p;)V

    return-void
.end method
