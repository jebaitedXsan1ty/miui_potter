.class public Landroid/support/v7/widget/ListPopupWindow;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"

# interfaces
.implements Landroid/support/v7/view/menu/m;


# static fields
.field private static efh:Ljava/lang/reflect/Method;

.field private static efi:Ljava/lang/reflect/Method;

.field private static efm:Ljava/lang/reflect/Method;


# instance fields
.field private eeU:Z

.field final eeV:Landroid/os/Handler;

.field private eeW:Z

.field private final eeX:Landroid/support/v7/widget/aX;

.field private eeY:I

.field private eeZ:Z

.field final efA:Landroid/support/v7/widget/bG;

.field efB:Landroid/widget/PopupWindow;

.field private efC:Landroid/widget/AdapterView$OnItemClickListener;

.field private final efa:Landroid/support/v7/widget/bd;

.field private final efb:Landroid/support/v7/widget/aS;

.field private efc:Z

.field private efd:I

.field private efe:I

.field private eff:Ljava/lang/Runnable;

.field private efg:I

.field efj:I

.field private efk:Landroid/database/DataSetObserver;

.field efl:Landroid/support/v7/widget/bV;

.field private efn:Z

.field private efo:Landroid/view/View;

.field private efp:Landroid/graphics/drawable/Drawable;

.field private efq:I

.field private efr:Landroid/graphics/Rect;

.field private efs:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private eft:Z

.field private efu:Landroid/widget/ListAdapter;

.field private efv:I

.field private efw:I

.field private efx:Landroid/view/View;

.field private final efy:Landroid/graphics/Rect;

.field private efz:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    :try_start_0
    const-class v0, Landroid/widget/PopupWindow;

    const-string/jumbo v1, "setClipToScreenEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/ListPopupWindow;->efh:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    const-class v0, Landroid/widget/PopupWindow;

    const-string/jumbo v1, "getMaxAvailableHeight"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Landroid/view/View;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/ListPopupWindow;->efi:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    const-class v0, Landroid/widget/PopupWindow;

    const-string/jumbo v1, "setEpicenterBounds"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/ListPopupWindow;->efm:Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "ListPopupWindow"

    const-string/jumbo v1, "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v0, "ListPopupWindow"

    const-string/jumbo v1, "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string/jumbo v0, "ListPopupWindow"

    const-string/jumbo v1, "Could not find method setEpicenterBounds(Rect) on PopupWindow. Oh well."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    sget v0, Landroid/support/v7/b/a;->dOz:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Landroid/support/v7/b/a;->dOz:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    const/4 v0, -0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efd:I

    iput v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    const/16 v0, 0x3ea

    iput v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efv:I

    iput-boolean v3, p0, Landroid/support/v7/widget/ListPopupWindow;->eeZ:Z

    iput v2, p0, Landroid/support/v7/widget/ListPopupWindow;->eeY:I

    iput-boolean v2, p0, Landroid/support/v7/widget/ListPopupWindow;->efc:Z

    iput-boolean v2, p0, Landroid/support/v7/widget/ListPopupWindow;->eeW:Z

    const v0, 0x7fffffff

    iput v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efj:I

    iput v2, p0, Landroid/support/v7/widget/ListPopupWindow;->efw:I

    new-instance v0, Landroid/support/v7/widget/bG;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bG;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efA:Landroid/support/v7/widget/bG;

    new-instance v0, Landroid/support/v7/widget/aS;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/aS;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efb:Landroid/support/v7/widget/aS;

    new-instance v0, Landroid/support/v7/widget/bd;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bd;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efa:Landroid/support/v7/widget/bd;

    new-instance v0, Landroid/support/v7/widget/aX;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/aX;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eeX:Landroid/support/v7/widget/aX;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iput-object p1, p0, Landroid/support/v7/widget/ListPopupWindow;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eeV:Landroid/os/Handler;

    sget-object v0, Landroid/support/v7/b/j;->dSR:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Landroid/support/v7/b/j;->dSS:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efq:I

    sget v1, Landroid/support/v7/b/j;->dST:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efg:I

    iget v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efg:I

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Landroid/support/v7/widget/ListPopupWindow;->eeU:Z

    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Landroid/support/v7/widget/AppCompatPopupWindow;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/support/v7/widget/AppCompatPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    return-void
.end method

.method private dAC(Z)V
    .locals 5

    sget-object v0, Landroid/support/v7/widget/ListPopupWindow;->efh:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Landroid/support/v7/widget/ListPopupWindow;->efh:Ljava/lang/reflect/Method;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "ListPopupWindow"

    const-string/jumbo v1, "Could not call setClipToScreenEnabled() on PopupWindow. Oh well."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private dAH()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efo:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efo:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private dAN(Landroid/view/View;IZ)I
    .locals 5

    sget-object v0, Landroid/support/v7/widget/ListPopupWindow;->efi:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Landroid/support/v7/widget/ListPopupWindow;->efi:Ljava/lang/reflect/Method;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "ListPopupWindow"

    const-string/jumbo v1, "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1, p2}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    move-result v0

    return v0
.end method

.method private dAO()I
    .locals 9

    const/high16 v4, -0x80000000

    const/4 v8, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    if-nez v0, :cond_4

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/support/v7/widget/bp;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bp;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eff:Ljava/lang/Runnable;

    iget-boolean v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eft:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/ListPopupWindow;->dAx(Landroid/content/Context;Z)Landroid/support/v7/widget/bV;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efp:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    iget-object v5, p0, Landroid/support/v7/widget/ListPopupWindow;->efp:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bV;->setSelector(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    iget-object v5, p0, Landroid/support/v7/widget/ListPopupWindow;->efu:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bV;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    iget-object v5, p0, Landroid/support/v7/widget/ListPopupWindow;->efC:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bV;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {v0, v8}, Landroid/support/v7/widget/bV;->setFocusable(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {v0, v8}, Landroid/support/v7/widget/bV;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    new-instance v5, Landroid/support/v7/widget/bN;

    invoke-direct {v5, p0}, Landroid/support/v7/widget/bN;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bV;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    iget-object v5, p0, Landroid/support/v7/widget/ListPopupWindow;->efa:Landroid/support/v7/widget/bd;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bV;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efs:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    iget-object v5, p0, Landroid/support/v7/widget/ListPopupWindow;->efs:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bV;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    iget-object v6, p0, Landroid/support/v7/widget/ListPopupWindow;->efo:Landroid/view/View;

    if-eqz v6, :cond_b

    new-instance v5, Landroid/widget/LinearLayout;

    invoke-direct {v5, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v1, v3, v2, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    iget v7, p0, Landroid/support/v7/widget/ListPopupWindow;->efw:I

    packed-switch v7, :pswitch_data_0

    const-string/jumbo v0, "ListPopupWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Invalid hint position "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v7, p0, Landroid/support/v7/widget/ListPopupWindow;->efw:I

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    if-ltz v0, :cond_3

    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    move v1, v4

    :goto_1
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v6, v0, v2}, Landroid/view/View;->measure(II)V

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v6, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v1, v6

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    move v1, v0

    move-object v0, v5

    :goto_2
    iget-object v5, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v5, v0}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    move v6, v1

    :goto_3
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iget-boolean v1, p0, Landroid/support/v7/widget/ListPopupWindow;->eeU:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efg:I

    move v7, v0

    :goto_4
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    move v0, v8

    :goto_5
    invoke-virtual {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAK()Landroid/view/View;

    move-result-object v1

    iget v5, p0, Landroid/support/v7/widget/ListPopupWindow;->efg:I

    invoke-direct {p0, v1, v5, v0}, Landroid/support/v7/widget/ListPopupWindow;->dAN(Landroid/view/View;IZ)I

    move-result v5

    iget-boolean v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efc:Z

    if-nez v0, :cond_2

    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efd:I

    if-ne v0, v3, :cond_7

    :cond_2
    add-int v0, v5, v7

    return v0

    :pswitch_0
    invoke-virtual {v5, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v5, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_3
    move v0, v2

    move v1, v2

    goto :goto_1

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efo:Landroid/view/View;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v5, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v1, v5

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v1, v0

    move v6, v1

    goto :goto_3

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    move v7, v2

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_5

    :cond_7
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    packed-switch v0, :pswitch_data_1

    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    :goto_6
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    sub-int v4, v5, v6

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/bV;->dCJ(IIIII)I

    move-result v0

    if-lez v0, :cond_8

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {v1}, Landroid/support/v7/widget/bV;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {v2}, Landroid/support/v7/widget/bV;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v7

    add-int/2addr v6, v1

    :cond_8
    add-int/2addr v0, v6

    return v0

    :pswitch_2
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v8

    sub-int/2addr v0, v1

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_6

    :pswitch_3
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v4

    sub-int/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_6

    :cond_9
    move v7, v0

    goto/16 :goto_4

    :cond_a
    move v6, v2

    goto/16 :goto_3

    :cond_b
    move v1, v2

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public dAA()Z
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dAB(Landroid/graphics/Rect;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/ListPopupWindow;->efr:Landroid/graphics/Rect;

    return-void
.end method

.method public dAD(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/ListPopupWindow;->efx:Landroid/view/View;

    return-void
.end method

.method public dAE(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    return-void
.end method

.method public dAF(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/ListPopupWindow;->efq:I

    return-void
.end method

.method public dAG(Z)V
    .locals 1

    iput-boolean p1, p0, Landroid/support/v7/widget/ListPopupWindow;->eft:Z

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    return-void
.end method

.method public dAI()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eft:Z

    return v0
.end method

.method public dAJ()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    return v0
.end method

.method public dAK()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efx:Landroid/view/View;

    return-object v0
.end method

.method public dAL()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public dAM(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/ListPopupWindow;->efC:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public dAo()I
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eeU:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efg:I

    return v0
.end method

.method public dAp(I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efy:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ListPopupWindow;->dAw(I)V

    goto :goto_0
.end method

.method public dAq(Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efz:Z

    iput-boolean p1, p0, Landroid/support/v7/widget/ListPopupWindow;->efn:Z

    return-void
.end method

.method public dAr(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/ListPopupWindow;->efw:I

    return-void
.end method

.method public dAs()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public dAt()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bV;->setListSelectionHidden(Z)V

    invoke-virtual {v0}, Landroid/support/v7/widget/bV;->requestLayout()V

    :cond_0
    return-void
.end method

.method public dAu(I)V
    .locals 1

    iput p1, p0, Landroid/support/v7/widget/ListPopupWindow;->efg:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eeU:Z

    return-void
.end method

.method public dAv()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efq:I

    return v0
.end method

.method public dAw(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    return-void
.end method

.method dAx(Landroid/content/Context;Z)Landroid/support/v7/widget/bV;
    .locals 1

    new-instance v0, Landroid/support/v7/widget/bV;

    invoke-direct {v0, p1, p2}, Landroid/support/v7/widget/bV;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method

.method public dAy(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    return-void
.end method

.method public dAz(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/ListPopupWindow;->eeY:I

    return-void
.end method

.method public dismiss()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    invoke-direct {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAH()V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iput-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eeV:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efA:Landroid/support/v7/widget/bG;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    return-object v0
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efk:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    new-instance v0, Landroid/support/v7/widget/cE;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cE;-><init>(Landroid/support/v7/widget/ListPopupWindow;)V

    iput-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efk:Landroid/database/DataSetObserver;

    :cond_0
    :goto_0
    iput-object p1, p0, Landroid/support/v7/widget/ListPopupWindow;->efu:Landroid/widget/ListAdapter;

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efu:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efk:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efu:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bV;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efu:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efu:Landroid/widget/ListAdapter;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efk:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    return-void
.end method

.method public setSelection(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAs()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/bV;->setListSelectionHidden(Z)V

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bV;->setSelection(I)V

    invoke-virtual {v0}, Landroid/support/v7/widget/bV;->getChoiceMode()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/bV;->setItemChecked(IZ)V

    :cond_0
    return-void
.end method

.method public show()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, -0x2

    const/4 v1, 0x0

    const/4 v5, -0x1

    invoke-direct {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAO()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAA()Z

    move-result v3

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    iget v4, p0, Landroid/support/v7/widget/ListPopupWindow;->efv:I

    invoke-static {v0, v4}, Landroid/support/v4/widget/aH;->dYC(Landroid/widget/PopupWindow;I)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAK()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/z;->dPO(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    if-ne v0, v5, :cond_4

    move v4, v5

    :goto_0
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efd:I

    if-ne v0, v5, :cond_a

    if-eqz v3, :cond_6

    :goto_1
    if-eqz v3, :cond_8

    iget-object v3, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    if-ne v0, v5, :cond_7

    move v0, v5

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v6, v2

    :goto_3
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    iget-boolean v2, p0, Landroid/support/v7/widget/ListPopupWindow;->eeW:Z

    if-nez v2, :cond_1

    iget-boolean v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efc:Z

    xor-int/lit8 v1, v1, 0x1

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAK()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/widget/ListPopupWindow;->efq:I

    iget v3, p0, Landroid/support/v7/widget/ListPopupWindow;->efg:I

    if-gez v4, :cond_2

    move v4, v5

    :cond_2
    if-gez v6, :cond_c

    :goto_4
    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    :cond_3
    :goto_5
    return-void

    :cond_4
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    if-ne v0, v6, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAK()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    move v4, v0

    goto :goto_0

    :cond_5
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    move v4, v0

    goto :goto_0

    :cond_6
    move v2, v5

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    iget-object v3, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    if-ne v0, v5, :cond_9

    move v0, v5

    :goto_6
    invoke-virtual {v3, v0}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v6, v2

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_6

    :cond_a
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efd:I

    if-ne v0, v6, :cond_b

    move v6, v2

    goto :goto_3

    :cond_b
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efd:I

    move v6, v0

    goto :goto_3

    :cond_c
    move v5, v6

    goto :goto_4

    :cond_d
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    if-ne v0, v5, :cond_14

    move v0, v5

    :goto_7
    iget v3, p0, Landroid/support/v7/widget/ListPopupWindow;->efd:I

    if-ne v3, v5, :cond_16

    move v2, v5

    :cond_e
    :goto_8
    iget-object v3, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v0}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setHeight(I)V

    invoke-direct {p0, v7}, Landroid/support/v7/widget/ListPopupWindow;->dAC(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    iget-boolean v2, p0, Landroid/support/v7/widget/ListPopupWindow;->eeW:Z

    if-nez v2, :cond_f

    iget-boolean v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efc:Z

    xor-int/lit8 v1, v1, 0x1

    :cond_f
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efb:Landroid/support/v7/widget/aS;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    iget-boolean v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efz:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    iget-boolean v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efn:Z

    invoke-static {v0, v1}, Landroid/support/v4/widget/aH;->dYD(Landroid/widget/PopupWindow;Z)V

    :cond_10
    sget-object v0, Landroid/support/v7/widget/ListPopupWindow;->efm:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_11

    :try_start_0
    sget-object v0, Landroid/support/v7/widget/ListPopupWindow;->efm:Ljava/lang/reflect/Method;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Landroid/support/v7/widget/ListPopupWindow;->efr:Landroid/graphics/Rect;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_11
    :goto_9
    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAK()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/widget/ListPopupWindow;->efq:I

    iget v3, p0, Landroid/support/v7/widget/ListPopupWindow;->efg:I

    iget v4, p0, Landroid/support/v7/widget/ListPopupWindow;->eeY:I

    invoke-static {v0, v1, v2, v3, v4}, Landroid/support/v4/widget/aH;->dYE(Landroid/widget/PopupWindow;Landroid/view/View;III)V

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/bV;->setSelection(I)V

    iget-boolean v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eft:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {v0}, Landroid/support/v7/widget/bV;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_12
    invoke-virtual {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAt()V

    :cond_13
    iget-boolean v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eft:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/ListPopupWindow;->eeV:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/widget/ListPopupWindow;->eeX:Landroid/support/v7/widget/aX;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_5

    :cond_14
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    if-ne v0, v6, :cond_15

    invoke-virtual {p0}, Landroid/support/v7/widget/ListPopupWindow;->dAK()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_7

    :cond_15
    iget v0, p0, Landroid/support/v7/widget/ListPopupWindow;->efe:I

    goto/16 :goto_7

    :cond_16
    iget v3, p0, Landroid/support/v7/widget/ListPopupWindow;->efd:I

    if-eq v3, v6, :cond_e

    iget v2, p0, Landroid/support/v7/widget/ListPopupWindow;->efd:I

    goto/16 :goto_8

    :catch_0
    move-exception v0

    const-string/jumbo v1, "ListPopupWindow"

    const-string/jumbo v2, "Could not invoke setEpicenterBounds on PopupWindow"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9
.end method
