.class Landroid/support/v7/widget/bq;
.super Landroid/support/v7/widget/bj;
.source "AppCompatSeekBarHelper.java"


# instance fields
.field private eih:Z

.field private eii:Landroid/content/res/ColorStateList;

.field private eij:Landroid/graphics/PorterDuff$Mode;

.field private eik:Z

.field private eil:Landroid/graphics/drawable/Drawable;

.field private final eim:Landroid/widget/SeekBar;


# direct methods
.method constructor <init>(Landroid/widget/SeekBar;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/bj;-><init>(Landroid/widget/ProgressBar;)V

    iput-object v1, p0, Landroid/support/v7/widget/bq;->eii:Landroid/content/res/ColorStateList;

    iput-object v1, p0, Landroid/support/v7/widget/bq;->eij:Landroid/graphics/PorterDuff$Mode;

    iput-boolean v0, p0, Landroid/support/v7/widget/bq;->eih:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/bq;->eik:Z

    iput-object p1, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    return-void
.end method

.method private dDe()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v7/widget/bq;->eih:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/bq;->eik:Z

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/c/a/g;->elO(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    iget-boolean v0, p0, Landroid/support/v7/widget/bq;->eih:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Landroid/support/v7/widget/bq;->eii:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, Landroid/support/v4/c/a/g;->elY(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/bq;->eik:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Landroid/support/v7/widget/bq;->eij:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v0, v1}, Landroid/support/v4/c/a/g;->ema(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_3
    return-void
.end method


# virtual methods
.method dCE(Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/bj;->dCE(Landroid/util/AttributeSet;I)V

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/b/j;->dRI:[I

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, p2, v2}, Landroid/support/v7/widget/bS;->dFw(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroid/support/v7/widget/bS;

    move-result-object v0

    sget v1, Landroid/support/v7/b/j;->dRJ:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bS;->dFz(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    sget v1, Landroid/support/v7/b/j;->dRK:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bS;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/bq;->dDd(Landroid/graphics/drawable/Drawable;)V

    sget v1, Landroid/support/v7/b/j;->dRM:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Landroid/support/v7/b/j;->dRM:I

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/bS;->dFr(II)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/bq;->eij:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v1, v2}, Landroid/support/v7/widget/cx;->dHt(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/widget/bq;->eij:Landroid/graphics/PorterDuff$Mode;

    iput-boolean v3, p0, Landroid/support/v7/widget/bq;->eik:Z

    :cond_1
    sget v1, Landroid/support/v7/b/j;->dRL:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bS;->dFx(I)Z

    move-result v1

    if-eqz v1, :cond_2

    sget v1, Landroid/support/v7/b/j;->dRL:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bS;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/widget/bq;->eii:Landroid/content/res/ColorStateList;

    iput-boolean v3, p0, Landroid/support/v7/widget/bq;->eih:Z

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/bS;->dFB()V

    invoke-direct {p0}, Landroid/support/v7/widget/bq;->dDe()V

    return-void
.end method

.method dDd(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/c/a/g;->elX(Landroid/graphics/drawable/Drawable;I)Z

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/bq;->dDe()V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->invalidate()V

    return-void
.end method

.method drawTickMarks(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMax()I

    move-result v3

    if-le v3, v0, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v4, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    if-ltz v2, :cond_1

    div-int/lit8 v2, v2, 0x2

    :goto_0
    if-ltz v4, :cond_0

    div-int/lit8 v0, v4, 0x2

    :cond_0
    iget-object v4, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    neg-int v5, v2

    neg-int v6, v0

    invoke-virtual {v4, v5, v6, v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getWidth()I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    int-to-float v2, v3

    div-float v2, v0, v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    iget-object v5, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    move v0, v1

    :goto_1
    if-gt v0, v3, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    const/4 v1, 0x0

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_3
    return-void
.end method

.method drawableStateChanged()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/bq;->eim:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method jumpDrawablesToCurrentState()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bq;->eil:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method
