.class public Landroid/support/v7/widget/a/b;
.super Landroid/support/v7/widget/d;
.source "ItemTouchHelper.java"

# interfaces
.implements Landroid/support/v7/widget/o;


# instance fields
.field dWA:I

.field private dWB:Landroid/support/v7/widget/v;

.field dWC:Landroid/view/VelocityTracker;

.field private dWD:J

.field final dWE:Ljava/util/List;

.field dWF:F

.field dWG:F

.field dWH:I

.field dWI:Ljava/util/List;

.field dWJ:F

.field final dWK:Ljava/lang/Runnable;

.field dWL:F

.field dWM:Landroid/support/v7/widget/a/c;

.field dWN:I

.field dWO:I

.field dWP:Landroid/support/v4/view/y;

.field private dWQ:Landroid/graphics/Rect;

.field private final dWR:Landroid/support/v7/widget/l;

.field dWS:F

.field dWT:F

.field dWU:F

.field dWs:Landroid/support/v7/widget/p;

.field dWt:F

.field dWu:Landroid/support/v7/widget/RecyclerView;

.field private final dWv:[F

.field private dWw:I

.field private dWx:Ljava/util/List;

.field dWy:Landroid/view/View;

.field private dWz:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/a/c;)V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v7/widget/d;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWE:Ljava/util/List;

    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    iput-object v1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iput v2, p0, Landroid/support/v7/widget/a/b;->dWH:I

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWO:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    new-instance v0, Landroid/support/v7/widget/a/g;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/a/g;-><init>(Landroid/support/v7/widget/a/b;)V

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWK:Ljava/lang/Runnable;

    iput-object v1, p0, Landroid/support/v7/widget/a/b;->dWB:Landroid/support/v7/widget/v;

    iput-object v1, p0, Landroid/support/v7/widget/a/b;->dWy:Landroid/view/View;

    iput v2, p0, Landroid/support/v7/widget/a/b;->dWA:I

    new-instance v0, Landroid/support/v7/widget/a/h;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/a/h;-><init>(Landroid/support/v7/widget/a/b;)V

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWR:Landroid/support/v7/widget/l;

    iput-object p1, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    return-void
.end method

.method private dnA()V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWP:Landroid/support/v4/view/y;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/support/v4/view/y;

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/support/v7/widget/a/e;

    invoke-direct {v2, p0}, Landroid/support/v7/widget/a/e;-><init>(Landroid/support/v7/widget/a/b;)V

    invoke-direct {v0, v1, v2}, Landroid/support/v4/view/y;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWP:Landroid/support/v4/view/y;

    return-void
.end method

.method private dnB(Landroid/support/v7/widget/p;)I
    .locals 5

    const v3, 0xff00

    const/4 v4, 0x0

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWO:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    return v4

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/a/c;->dnS(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/a/c;->dnX(II)I

    move-result v1

    and-int/2addr v1, v3

    shr-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_1

    return v4

    :cond_1
    and-int/2addr v0, v3

    shr-int/lit8 v0, v0, 0x8

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWS:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Landroid/support/v7/widget/a/b;->dWT:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    invoke-direct {p0, p1, v1}, Landroid/support/v7/widget/a/b;->dnF(Landroid/support/v7/widget/p;I)I

    move-result v2

    if-lez v2, :cond_3

    and-int/2addr v0, v2

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v0

    invoke-static {v2, v0}, Landroid/support/v7/widget/a/c;->dob(II)I

    move-result v0

    return v0

    :cond_2
    return v2

    :cond_3
    invoke-direct {p0, p1, v1}, Landroid/support/v7/widget/a/b;->dnl(Landroid/support/v7/widget/p;I)I

    move-result v0

    if-lez v0, :cond_7

    return v0

    :cond_4
    invoke-direct {p0, p1, v1}, Landroid/support/v7/widget/a/b;->dnl(Landroid/support/v7/widget/p;I)I

    move-result v2

    if-lez v2, :cond_5

    return v2

    :cond_5
    invoke-direct {p0, p1, v1}, Landroid/support/v7/widget/a/b;->dnF(Landroid/support/v7/widget/p;I)I

    move-result v1

    if-lez v1, :cond_7

    and-int/2addr v0, v1

    if-nez v0, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v0

    invoke-static {v1, v0}, Landroid/support/v7/widget/a/c;->dob(II)I

    move-result v0

    return v0

    :cond_6
    return v1

    :cond_7
    return v4
.end method

.method private dnF(Landroid/support/v7/widget/p;I)I
    .locals 8

    const/16 v1, 0x8

    const/4 v2, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    and-int/lit8 v0, p2, 0xc

    if-eqz v0, :cond_3

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWS:F

    cmpl-float v0, v0, v7

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_2

    iget v3, p0, Landroid/support/v7/widget/a/b;->dWH:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget v5, p0, Landroid/support/v7/widget/a/b;->dWU:F

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/a/c;->dnU(F)F

    move-result v4

    const/16 v5, 0x3e8

    invoke-virtual {v3, v5, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    iget v4, p0, Landroid/support/v7/widget/a/b;->dWH:I

    invoke-virtual {v3, v4}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    iget v5, p0, Landroid/support/v7/widget/a/b;->dWH:I

    invoke-virtual {v4, v5}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v4

    cmpl-float v5, v3, v7

    if-lez v5, :cond_1

    :goto_1
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    and-int v3, v1, p2

    if-eqz v3, :cond_2

    if-ne v0, v1, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget v5, p0, Landroid/support/v7/widget/a/b;->dWt:F

    invoke-virtual {v3, v5}, Landroid/support/v7/widget/a/c;->dnW(F)F

    move-result v3

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/a/c;->doe(Landroid/support/v7/widget/p;)F

    move-result v2

    mul-float/2addr v1, v2

    and-int v2, p2, v0

    if-eqz v2, :cond_3

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWS:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v2, v1

    if-lez v1, :cond_3

    return v0

    :cond_3
    return v6
.end method

.method private dnJ()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWw:I

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->dpM(Landroid/support/v7/widget/d;)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWR:Landroid/support/v7/widget/l;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->doU(Landroid/support/v7/widget/l;)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->dqG(Landroid/support/v7/widget/o;)V

    invoke-direct {p0}, Landroid/support/v7/widget/a/b;->dnA()V

    return-void
.end method

.method private dnL([F)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWN:I

    and-int/lit8 v0, v0, 0xc

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWJ:F

    iget v1, p0, Landroid/support/v7/widget/a/b;->dWS:F

    add-float/2addr v0, v1

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v1, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p1, v2

    :goto_0
    iget v0, p0, Landroid/support/v7/widget/a/b;->dWN:I

    and-int/lit8 v0, v0, 0x3

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWL:F

    iget v1, p0, Landroid/support/v7/widget/a/b;->dWT:F

    add-float/2addr v0, v1

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v1, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p1, v3

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    aput v0, p1, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    aput v0, p1, v3

    goto :goto_1
.end method

.method private dnl(Landroid/support/v7/widget/p;I)I
    .locals 8

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    and-int/lit8 v0, p2, 0x3

    if-eqz v0, :cond_3

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWT:F

    cmpl-float v0, v0, v7

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_2

    iget v3, p0, Landroid/support/v7/widget/a/b;->dWH:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget v5, p0, Landroid/support/v7/widget/a/b;->dWU:F

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/a/c;->dnU(F)F

    move-result v4

    const/16 v5, 0x3e8

    invoke-virtual {v3, v5, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    iget v4, p0, Landroid/support/v7/widget/a/b;->dWH:I

    invoke-virtual {v3, v4}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    iget v5, p0, Landroid/support/v7/widget/a/b;->dWH:I

    invoke-virtual {v4, v5}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v4

    cmpl-float v5, v4, v7

    if-lez v5, :cond_1

    :goto_1
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    and-int v4, v1, p2

    if-eqz v4, :cond_2

    if-ne v1, v0, :cond_2

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget v5, p0, Landroid/support/v7/widget/a/b;->dWt:F

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/a/c;->dnW(F)F

    move-result v4

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/a/c;->doe(Landroid/support/v7/widget/p;)F

    move-result v2

    mul-float/2addr v1, v2

    and-int v2, p2, v0

    if-eqz v2, :cond_3

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWT:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v2, v1

    if-lez v1, :cond_3

    return v0

    :cond_3
    return v6
.end method

.method private dno()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->doT(Landroid/support/v7/widget/d;)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWR:Landroid/support/v7/widget/l;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->dqn(Landroid/support/v7/widget/l;)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->dqH(Landroid/support/v7/widget/o;)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/a/f;

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    invoke-virtual {v2, v3, v0}, Landroid/support/v7/widget/a/c;->doi(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWy:Landroid/view/View;

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWA:I

    invoke-direct {p0}, Landroid/support/v7/widget/a/b;->dnz()V

    return-void
.end method

.method private dnp(Landroid/view/MotionEvent;)Landroid/support/v7/widget/p;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/a;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/a/b;->dWH:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    return-object v4

    :cond_0
    iget v1, p0, Landroid/support/v7/widget/a/b;->dWH:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget v3, p0, Landroid/support/v7/widget/a/b;->dWG:F

    sub-float/2addr v2, v3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget v3, p0, Landroid/support/v7/widget/a/b;->dWF:F

    sub-float/2addr v1, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Landroid/support/v7/widget/a/b;->dWw:I

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-gez v3, :cond_1

    iget v3, p0, Landroid/support/v7/widget/a/b;->dWw:I

    int-to-float v3, v3

    cmpg-float v3, v1, v3

    if-gez v3, :cond_1

    return-object v4

    :cond_1
    cmpl-float v3, v2, v1

    if-lez v3, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v3

    if-eqz v3, :cond_2

    return-object v4

    :cond_2
    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v0

    if-eqz v0, :cond_3

    return-object v4

    :cond_3
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a/b;->dnD(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_4

    return-object v4

    :cond_4
    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    return-object v0
.end method

.method private dnq()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWB:Landroid/support/v7/widget/v;

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v7/widget/a/k;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/a/k;-><init>(Landroid/support/v7/widget/a/b;)V

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWB:Landroid/support/v7/widget/v;

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWB:Landroid/support/v7/widget/v;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setChildDrawingOrderCallback(Landroid/support/v7/widget/v;)V

    return-void
.end method

.method private static dnv(Landroid/view/View;FFFF)Z
    .locals 2

    const/4 v0, 0x0

    cmpl-float v1, p1, p3

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, p3

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    cmpl-float v1, p2, p4

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, p4

    cmpg-float v1, p2, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private dnw(Landroid/support/v7/widget/p;)Ljava/util/List;
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWx:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/v7/widget/a/b;->dWx:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Landroid/support/v7/widget/a/b;->dWz:Ljava/util/List;

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    invoke-virtual {v1}, Landroid/support/v7/widget/a/c;->dok()I

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/a/b;->dWJ:F

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/a/b;->dWS:F

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    sub-int v5, v2, v1

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/a/b;->dWL:F

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v7/widget/a/b;->dWT:F

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    sub-int v6, v2, v1

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v2, v5

    mul-int/lit8 v3, v1, 0x2

    add-int v7, v2, v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v6

    mul-int/lit8 v1, v1, 0x2

    add-int v8, v2, v1

    add-int v1, v5, v7

    div-int/lit8 v9, v1, 0x2

    add-int v1, v6, v8

    div-int/lit8 v10, v1, 0x2

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/a;

    move-result-object v11

    invoke-virtual {v11}, Landroid/support/v7/widget/a;->drc()I

    move-result v12

    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v12, :cond_4

    invoke-virtual {v11, v4}, Landroid/support/v7/widget/a;->drB(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    if-ne v1, v2, :cond_2

    :cond_0
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWx:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWz:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    if-lt v2, v6, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-gt v2, v8, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v2

    if-lt v2, v5, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    if-gt v2, v7, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    invoke-virtual {v2, v3, v14, v13}, Landroid/support/v7/widget/a/c;->doc(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v9, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v10, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    mul-int/2addr v2, v2

    mul-int/2addr v1, v1

    add-int v14, v2, v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWx:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v15

    const/4 v1, 0x0

    move v3, v2

    move v2, v1

    :goto_3
    if-ge v2, v15, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWz:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v14, v1, :cond_3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWx:Ljava/util/List;

    invoke-interface {v1, v3, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWz:Ljava/util/List;

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v7/widget/a/b;->dWx:Ljava/util/List;

    return-object v1
.end method

.method private dnz()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v1, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method


# virtual methods
.method public Cm(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    invoke-direct {p0, v0}, Landroid/support/v7/widget/a/b;->dnL([F)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    const/4 v1, 0x0

    aget v6, v0, v1

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    const/4 v1, 0x1

    aget v7, v0, v1

    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    iget v5, p0, Landroid/support/v7/widget/a/b;->dWO:I

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Landroid/support/v7/widget/a/c;->dnQ(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Ljava/util/List;IFF)V

    return-void

    :cond_0
    move v6, v7

    goto :goto_0
.end method

.method public cdd(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V
    .locals 0

    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    return-void
.end method

.method public cde(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/e;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWA:I

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    invoke-direct {p0, v0}, Landroid/support/v7/widget/a/b;->dnL([F)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    const/4 v1, 0x0

    aget v6, v0, v1

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    const/4 v1, 0x1

    aget v7, v0, v1

    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    iget v5, p0, Landroid/support/v7/widget/a/b;->dWO:I

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Landroid/support/v7/widget/a/c;->dnY(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Ljava/util/List;IFF)V

    return-void

    :cond_0
    move v6, v7

    goto :goto_0
.end method

.method dnC(ILandroid/view/MotionEvent;I)Z
    .locals 10

    const/4 v1, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-nez v0, :cond_0

    if-eq p1, v1, :cond_1

    :cond_0
    return v7

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/a/b;->dWO:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    invoke-virtual {v0}, Landroid/support/v7/widget/a/c;->dog()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getScrollState()I

    move-result v0

    if-ne v0, v9, :cond_2

    return v7

    :cond_2
    invoke-direct {p0, p2}, Landroid/support/v7/widget/a/b;->dnp(Landroid/view/MotionEvent;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-nez v0, :cond_3

    return v7

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/widget/a/c;->dof(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)I

    move-result v1

    const v2, 0xff00

    and-int/2addr v1, v2

    shr-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_4

    return v7

    :cond_4
    invoke-virtual {p2, p3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p2, p3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget v4, p0, Landroid/support/v7/widget/a/b;->dWG:F

    sub-float/2addr v2, v4

    iget v4, p0, Landroid/support/v7/widget/a/b;->dWF:F

    sub-float/2addr v3, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v6, p0, Landroid/support/v7/widget/a/b;->dWw:I

    int-to-float v6, v6

    cmpg-float v6, v4, v6

    if-gez v6, :cond_5

    iget v6, p0, Landroid/support/v7/widget/a/b;->dWw:I

    int-to-float v6, v6

    cmpg-float v6, v5, v6

    if-gez v6, :cond_5

    return v7

    :cond_5
    cmpl-float v4, v4, v5

    if-lez v4, :cond_7

    cmpg-float v3, v2, v8

    if-gez v3, :cond_6

    and-int/lit8 v3, v1, 0x4

    if-nez v3, :cond_6

    return v7

    :cond_6
    cmpl-float v2, v2, v8

    if-lez v2, :cond_9

    and-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_9

    return v7

    :cond_7
    cmpg-float v2, v3, v8

    if-gez v2, :cond_8

    and-int/lit8 v2, v1, 0x1

    if-nez v2, :cond_8

    return v7

    :cond_8
    cmpl-float v2, v3, v8

    if-lez v2, :cond_9

    and-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_9

    return v7

    :cond_9
    iput v8, p0, Landroid/support/v7/widget/a/b;->dWT:F

    iput v8, p0, Landroid/support/v7/widget/a/b;->dWS:F

    invoke-virtual {p2, v7}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/a/b;->dWH:I

    invoke-virtual {p0, v0, v9}, Landroid/support/v7/widget/a/b;->dny(Landroid/support/v7/widget/p;I)V

    return v9
.end method

.method dnD(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    iget v1, p0, Landroid/support/v7/widget/a/b;->dWJ:F

    iget v4, p0, Landroid/support/v7/widget/a/b;->dWS:F

    add-float/2addr v1, v4

    iget v4, p0, Landroid/support/v7/widget/a/b;->dWL:F

    iget v5, p0, Landroid/support/v7/widget/a/b;->dWT:F

    add-float/2addr v4, v5

    invoke-static {v0, v2, v3, v1, v4}, Landroid/support/v7/widget/a/b;->dnv(Landroid/view/View;FFFF)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/a/f;

    iget-object v4, v0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iget-object v4, v4, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    iget v5, v0, Landroid/support/v7/widget/a/f;->dXf:F

    iget v0, v0, Landroid/support/v7/widget/a/f;->dXh:F

    invoke-static {v4, v2, v3, v5, v0}, Landroid/support/v7/widget/a/b;->dnv(Landroid/view/View;FFFF)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/RecyclerView;->dpb(FF)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method dnE()Z
    .locals 14

    const-wide/high16 v12, -0x8000000000000000L

    const/4 v5, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-nez v1, :cond_0

    iput-wide v12, p0, Landroid/support/v7/widget/a/b;->dWD:J

    return v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iget-wide v2, p0, Landroid/support/v7/widget/a/b;->dWD:J

    cmp-long v1, v2, v12

    if-nez v1, :cond_6

    const-wide/16 v6, 0x0

    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/a;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWQ:Landroid/graphics/Rect;

    if-nez v2, :cond_1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Landroid/support/v7/widget/a/b;->dWQ:Landroid/graphics/Rect;

    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v2, v2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWQ:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/a;->dqZ(Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v2

    if-eqz v2, :cond_8

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWJ:F

    iget v3, p0, Landroid/support/v7/widget/a/b;->dWS:F

    add-float/2addr v2, v3

    float-to-int v2, v2

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWQ:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v3, v2, v3

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v4

    sub-int v4, v3, v4

    iget v3, p0, Landroid/support/v7/widget/a/b;->dWS:F

    cmpg-float v3, v3, v5

    if-gez v3, :cond_7

    if-gez v4, :cond_7

    :cond_2
    :goto_1
    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v1

    if-eqz v1, :cond_a

    iget v1, p0, Landroid/support/v7/widget/a/b;->dWL:F

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWT:F

    add-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWQ:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v2, v1, v2

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int v8, v2, v3

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWT:F

    cmpg-float v2, v2, v5

    if-gez v2, :cond_9

    if-gez v8, :cond_9

    :cond_3
    :goto_2
    if-eqz v4, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v3, v3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    iget-object v5, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v5

    invoke-virtual/range {v1 .. v7}, Landroid/support/v7/widget/a/c;->dnV(Landroid/support/v7/widget/RecyclerView;IIIJ)I

    move-result v4

    move v9, v4

    :goto_3
    if-eqz v8, :cond_c

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v3, v3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    move v4, v8

    invoke-virtual/range {v1 .. v7}, Landroid/support/v7/widget/a/c;->dnV(Landroid/support/v7/widget/RecyclerView;IIIJ)I

    move-result v1

    :goto_4
    if-nez v9, :cond_4

    if-eqz v1, :cond_b

    :cond_4
    iget-wide v2, p0, Landroid/support/v7/widget/a/b;->dWD:J

    cmp-long v0, v2, v12

    if-nez v0, :cond_5

    iput-wide v10, p0, Landroid/support/v7/widget/a/b;->dWD:J

    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v9, v1}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    const/4 v0, 0x1

    return v0

    :cond_6
    iget-wide v2, p0, Landroid/support/v7/widget/a/b;->dWD:J

    sub-long v6, v10, v2

    goto/16 :goto_0

    :cond_7
    iget v3, p0, Landroid/support/v7/widget/a/b;->dWS:F

    cmpl-float v3, v3, v5

    if-lez v3, :cond_8

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v3, v3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWQ:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int v4, v2, v3

    if-gtz v4, :cond_2

    :cond_8
    move v4, v0

    goto/16 :goto_1

    :cond_9
    iget v2, p0, Landroid/support/v7/widget/a/b;->dWT:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_a

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v2, v2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWQ:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v8, v1, v2

    if-gtz v8, :cond_3

    :cond_a
    move v8, v0

    goto/16 :goto_2

    :cond_b
    iput-wide v12, p0, Landroid/support/v7/widget/a/b;->dWD:J

    return v0

    :cond_c
    move v1, v8

    goto :goto_4

    :cond_d
    move v9, v4

    goto/16 :goto_3
.end method

.method dnG()Z
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/a/f;

    iget-boolean v0, v0, Landroid/support/v7/widget/a/f;->dXj:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v2
.end method

.method dnH(Landroid/support/v7/widget/a/f;I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/a/j;

    invoke-direct {v1, p0, p1, p2}, Landroid/support/v7/widget/a/j;-><init>(Landroid/support/v7/widget/a/b;Landroid/support/v7/widget/a/f;I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public dnI(Landroid/support/v7/widget/p;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/a/c;->dnP(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "ItemTouchHelper"

    const-string/jumbo v1, "Start drag has been called but dragging is not enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    if-eq v0, v1, :cond_1

    const-string/jumbo v0, "ItemTouchHelper"

    const-string/jumbo v1, "Start drag has been called with a view holder which is not a child of the RecyclerView which is controlled by this ItemTouchHelper."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/a/b;->dnx()V

    iput v2, p0, Landroid/support/v7/widget/a/b;->dWT:F

    iput v2, p0, Landroid/support/v7/widget/a/b;->dWS:F

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/a/b;->dny(Landroid/support/v7/widget/p;I)V

    return-void
.end method

.method dnK(Landroid/view/MotionEvent;II)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1, p3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, p3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWG:F

    sub-float/2addr v0, v2

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWS:F

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWF:F

    sub-float v0, v1, v0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWT:F

    and-int/lit8 v0, p2, 0x4

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWS:F

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWS:F

    :cond_0
    and-int/lit8 v0, p2, 0x8

    if-nez v0, :cond_1

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWS:F

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWS:F

    :cond_1
    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_2

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWT:F

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWT:F

    :cond_2
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_3

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWT:F

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWT:F

    :cond_3
    return-void
.end method

.method dnM(Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWy:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iput-object v1, p0, Landroid/support/v7/widget/a/b;->dWy:Landroid/view/View;

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWB:Landroid/support/v7/widget/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setChildDrawingOrderCallback(Landroid/support/v7/widget/v;)V

    :cond_0
    return-void
.end method

.method public dnm(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Landroid/support/v7/widget/a/b;->dno()V

    :cond_1
    iput-object p1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v7/a/a;->dFK:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/a/b;->dWt:F

    sget v1, Landroid/support/v7/a/a;->dFJ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWU:F

    invoke-direct {p0}, Landroid/support/v7/widget/a/b;->dnJ()V

    :cond_2
    return-void
.end method

.method dnn(Landroid/support/v7/widget/p;)V
    .locals 8

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/a/b;->dWO:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a/c;->dnO(Landroid/support/v7/widget/p;)F

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/a/b;->dWJ:F

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWS:F

    add-float/2addr v1, v2

    float-to-int v6, v1

    iget v1, p0, Landroid/support/v7/widget/a/b;->dWL:F

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWT:F

    add-float/2addr v1, v2

    float-to-int v7, v1

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int v1, v7, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, v6, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    cmpg-float v0, v1, v0

    if-gez v0, :cond_2

    return-void

    :cond_2
    invoke-direct {p0, p1}, Landroid/support/v7/widget/a/b;->dnw(Landroid/support/v7/widget/p;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    return-void

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    invoke-virtual {v1, p1, v0, v6, v7}, Landroid/support/v7/widget/a/c;->dnZ(Landroid/support/v7/widget/p;Ljava/util/List;II)Landroid/support/v7/widget/p;

    move-result-object v4

    if-nez v4, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWx:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWz:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void

    :cond_4
    invoke-virtual {v4}, Landroid/support/v7/widget/p;->getAdapterPosition()I

    move-result v5

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getAdapterPosition()I

    move-result v3

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1, p1, v4}, Landroid/support/v7/widget/a/c;->Bs(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, Landroid/support/v7/widget/a/c;->dnN(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;ILandroid/support/v7/widget/p;III)V

    :cond_5
    return-void
.end method

.method dnr(Landroid/support/v7/widget/p;Z)I
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/a/f;

    iget-object v2, v0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    if-ne v2, p1, :cond_1

    iget-boolean v2, v0, Landroid/support/v7/widget/a/f;->dXm:Z

    or-int/2addr v2, p2

    iput-boolean v2, v0, Landroid/support/v7/widget/a/f;->dXm:Z

    iget-boolean v2, v0, Landroid/support/v7/widget/a/f;->dXj:Z

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/a/f;->cancel()V

    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget v0, v0, Landroid/support/v7/widget/a/f;->dXi:I

    return v0

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return v3
.end method

.method dns(Landroid/view/MotionEvent;)Landroid/support/v7/widget/a/f;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v4

    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a/b;->dnD(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/a/f;

    iget-object v3, v0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iget-object v3, v3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    if-ne v3, v2, :cond_1

    return-object v0

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-object v4
.end method

.method public dnt(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public dnu(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/a/b;->dnM(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0, v2, v3}, Landroid/support/v7/widget/a/b;->dny(Landroid/support/v7/widget/p;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0, v0, v3}, Landroid/support/v7/widget/a/b;->dnr(Landroid/support/v7/widget/p;Z)I

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWE:Ljava/util/List;

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/widget/a/c;->doi(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)V

    goto :goto_0
.end method

.method dnx()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    :cond_0
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWC:Landroid/view/VelocityTracker;

    return-void
.end method

.method dny(Landroid/support/v7/widget/p;I)V
    .locals 12

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-ne p1, v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWO:I

    if-ne p2, v0, :cond_0

    return-void

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Landroid/support/v7/widget/a/b;->dWD:J

    iget v4, p0, Landroid/support/v7/widget/a/b;->dWO:I

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/a/b;->dnr(Landroid/support/v7/widget/p;Z)I

    iput p2, p0, Landroid/support/v7/widget/a/b;->dWO:I

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    iput-object v0, p0, Landroid/support/v7/widget/a/b;->dWy:Landroid/view/View;

    invoke-direct {p0}, Landroid/support/v7/widget/a/b;->dnq()V

    :cond_1
    mul-int/lit8 v0, p2, 0x8

    add-int/lit8 v0, v0, 0x8

    const/4 v1, 0x1

    shl-int v0, v1, v0

    add-int/lit8 v11, v0, -0x1

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-eqz v1, :cond_a

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v1, v2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_8

    const/4 v0, 0x2

    if-ne v4, v0, :cond_5

    const/4 v9, 0x0

    :goto_0
    invoke-direct {p0}, Landroid/support/v7/widget/a/b;->dnz()V

    sparse-switch v9, :sswitch_data_0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_1
    const/4 v0, 0x2

    if-ne v4, v0, :cond_6

    const/16 v3, 0x8

    :goto_2
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    invoke-direct {p0, v0}, Landroid/support/v7/widget/a/b;->dnL([F)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    const/4 v1, 0x0

    aget v5, v0, v1

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWv:[F

    const/4 v1, 0x1

    aget v6, v0, v1

    new-instance v0, Landroid/support/v7/widget/a/i;

    move-object v1, p0

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, Landroid/support/v7/widget/a/i;-><init>(Landroid/support/v7/widget/a/b;Landroid/support/v7/widget/p;IIFFFFILandroid/support/v7/widget/p;)V

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    sub-float v4, v7, v5

    sub-float v5, v8, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/support/v7/widget/a/c;->doa(Landroid/support/v7/widget/RecyclerView;IFF)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/widget/a/f;->doo(J)V

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWI:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Landroid/support/v7/widget/a/f;->start()V

    const/4 v0, 0x1

    :goto_3
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    move v1, v0

    :goto_4
    if-eqz p1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v2, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2, p1}, Landroid/support/v7/widget/a/c;->dof(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)I

    move-result v0

    and-int/2addr v0, v11

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWO:I

    mul-int/lit8 v2, v2, 0x8

    shr-int/2addr v0, v2

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWN:I

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWJ:F

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Landroid/support/v7/widget/a/b;->dWL:F

    iput-object p1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->performHapticFeedback(I)Z

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_5
    invoke-interface {v2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_3
    if-nez v1, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsp()V

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWs:Landroid/support/v7/widget/p;

    iget v2, p0, Landroid/support/v7/widget/a/b;->dWO:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/a/c;->HW(Landroid/support/v7/widget/p;I)V

    iget-object v0, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    return-void

    :cond_5
    invoke-direct {p0, v2}, Landroid/support/v7/widget/a/b;->dnB(Landroid/support/v7/widget/p;)I

    move-result v9

    goto/16 :goto_0

    :sswitch_0
    const/4 v8, 0x0

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWS:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float v7, v0, v1

    goto/16 :goto_1

    :sswitch_1
    const/4 v7, 0x0

    iget v0, p0, Landroid/support/v7/widget/a/b;->dWT:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float v8, v0, v1

    goto/16 :goto_1

    :cond_6
    if-lez v9, :cond_7

    const/4 v3, 0x2

    goto/16 :goto_2

    :cond_7
    const/4 v3, 0x4

    goto/16 :goto_2

    :cond_8
    iget-object v1, v2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/a/b;->dnM(Landroid/view/View;)V

    iget-object v1, p0, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v3, p0, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3, v2}, Landroid/support/v7/widget/a/c;->doi(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)V

    goto/16 :goto_3

    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    :cond_a
    move v1, v0

    goto/16 :goto_4

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x4 -> :sswitch_0
        0x8 -> :sswitch_0
        0x10 -> :sswitch_0
        0x20 -> :sswitch_0
    .end sparse-switch
.end method
