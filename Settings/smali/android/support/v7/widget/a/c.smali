.class public abstract Landroid/support/v7/widget/a/c;
.super Ljava/lang/Object;
.source "ItemTouchHelper.java"


# static fields
.field private static final dWW:Landroid/view/animation/Interpolator;

.field private static final dWX:Landroid/support/v7/widget/a/n;

.field private static final dWY:Landroid/view/animation/Interpolator;


# instance fields
.field private dWV:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/support/v7/widget/a/l;

    invoke-direct {v0}, Landroid/support/v7/widget/a/l;-><init>()V

    sput-object v0, Landroid/support/v7/widget/a/c;->dWW:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/support/v7/widget/a/m;

    invoke-direct {v0}, Landroid/support/v7/widget/a/m;-><init>()V

    sput-object v0, Landroid/support/v7/widget/a/c;->dWY:Landroid/view/animation/Interpolator;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v7/widget/a/q;

    invoke-direct {v0}, Landroid/support/v7/widget/a/q;-><init>()V

    sput-object v0, Landroid/support/v7/widget/a/c;->dWX:Landroid/support/v7/widget/a/n;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/support/v7/widget/a/p;

    invoke-direct {v0}, Landroid/support/v7/widget/a/p;-><init>()V

    sput-object v0, Landroid/support/v7/widget/a/c;->dWX:Landroid/support/v7/widget/a/n;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/a/c;->dWV:I

    return-void
.end method

.method public static dnT(II)I
    .locals 2

    or-int v0, p1, p0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Landroid/support/v7/widget/a/c;->doj(II)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v1, p1}, Landroid/support/v7/widget/a/c;->doj(II)I

    move-result v1

    or-int/2addr v0, v1

    const/4 v1, 0x2

    invoke-static {v1, p0}, Landroid/support/v7/widget/a/c;->doj(II)I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public static dob(II)I
    .locals 5

    const v4, 0xc0c0c

    and-int v0, p0, v4

    if-nez v0, :cond_0

    return p0

    :cond_0
    not-int v1, v0

    and-int/2addr v1, p0

    if-nez p1, :cond_1

    shl-int/lit8 v0, v0, 0x2

    or-int/2addr v0, v1

    return v0

    :cond_1
    shl-int/lit8 v2, v0, 0x1

    const v3, -0xc0c0d

    and-int/2addr v2, v3

    or-int/2addr v1, v2

    shl-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v4

    shl-int/lit8 v0, v0, 0x2

    or-int/2addr v0, v1

    return v0
.end method

.method private dod(Landroid/support/v7/widget/RecyclerView;)I
    .locals 2

    iget v0, p0, Landroid/support/v7/widget/a/c;->dWV:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v7/a/a;->dFI:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/a/c;->dWV:I

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/a/c;->dWV:I

    return v0
.end method

.method public static doj(II)I
    .locals 1

    mul-int/lit8 v0, p0, 0x8

    shl-int v0, p1, v0

    return v0
.end method


# virtual methods
.method public abstract Bs(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;)Z
.end method

.method public abstract Bt(Landroid/support/v7/widget/p;I)V
.end method

.method public HV(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;FFIZ)V
    .locals 8

    sget-object v0, Landroid/support/v7/widget/a/c;->dWX:Landroid/support/v7/widget/a/n;

    iget-object v3, p3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Landroid/support/v7/widget/a/n;->dou(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FFIZ)V

    return-void
.end method

.method public HW(Landroid/support/v7/widget/p;I)V
    .locals 2

    if-eqz p1, :cond_0

    sget-object v0, Landroid/support/v7/widget/a/c;->dWX:Landroid/support/v7/widget/a/n;

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/a/n;->dow(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public dnN(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;ILandroid/support/v7/widget/p;III)V
    .locals 4

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/a;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/widget/a/a;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v7/widget/a/a;

    iget-object v1, p2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    iget-object v2, p4, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-interface {v0, v1, v2, p6, p7}, Landroid/support/v7/widget/a/a;->dnk(Landroid/view/View;Landroid/view/View;II)V

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p4, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drv(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    if-gt v1, v2, :cond_1

    invoke-virtual {p1, p5}, Landroid/support/v7/widget/RecyclerView;->dqo(I)V

    :cond_1
    iget-object v1, p4, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drg(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_2

    invoke-virtual {p1, p5}, Landroid/support/v7/widget/RecyclerView;->dqo(I)V

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p4, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->dsG(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    if-gt v1, v2, :cond_3

    invoke-virtual {p1, p5}, Landroid/support/v7/widget/RecyclerView;->dqo(I)V

    :cond_3
    iget-object v1, p4, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/a;->drk(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    if-lt v0, v1, :cond_4

    invoke-virtual {p1, p5}, Landroid/support/v7/widget/RecyclerView;->dqo(I)V

    :cond_4
    return-void
.end method

.method public dnO(Landroid/support/v7/widget/p;)F
    .locals 1

    const/high16 v0, 0x3f000000    # 0.5f

    return v0
.end method

.method dnP(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/a/c;->dof(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)I

    move-result v1

    const/high16 v2, 0xff0000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method dnQ(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Ljava/util/List;IFF)V
    .locals 11

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v9

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v9, :cond_0

    invoke-interface {p4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/a/f;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v10

    iget-object v3, v0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iget v4, v0, Landroid/support/v7/widget/a/f;->dXf:F

    iget v5, v0, Landroid/support/v7/widget/a/f;->dXh:F

    iget v6, v0, Landroid/support/v7/widget/a/f;->dXc:I

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Landroid/support/v7/widget/a/c;->doh(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;FFIZ)V

    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->restoreToCount(I)V

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v8

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v4, p6

    move/from16 v5, p7

    move/from16 v6, p5

    invoke-virtual/range {v0 .. v7}, Landroid/support/v7/widget/a/c;->doh(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;FFIZ)V

    invoke-virtual {p1, v8}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    const/4 v1, 0x0

    add-int/lit8 v0, v9, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_3

    invoke-interface {p4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/a/f;

    iget-boolean v3, v0, Landroid/support/v7/widget/a/f;->dXj:Z

    if-eqz v3, :cond_2

    iget-boolean v3, v0, Landroid/support/v7/widget/a/f;->dXl:Z

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    invoke-interface {p4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    :goto_2
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_2
    iget-boolean v0, v0, Landroid/support/v7/widget/a/f;->dXj:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public dnR()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract dnS(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)I
.end method

.method public dnU(F)F
    .locals 0

    return p1
.end method

.method public dnV(Landroid/support/v7/widget/RecyclerView;IIIJ)I
    .locals 5

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1}, Landroid/support/v7/widget/a/c;->dod(Landroid/support/v7/widget/RecyclerView;)I

    move-result v1

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-float v3, p3

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v2, v2

    mul-float/2addr v2, v0

    int-to-float v4, p2

    div-float/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    mul-int/2addr v1, v3

    int-to-float v1, v1

    sget-object v3, Landroid/support/v7/widget/a/c;->dWY:Landroid/view/animation/Interpolator;

    invoke-interface {v3, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    const-wide/16 v2, 0x7d0

    cmp-long v2, p5, v2

    if-lez v2, :cond_0

    :goto_0
    int-to-float v1, v1

    sget-object v2, Landroid/support/v7/widget/a/c;->dWW:Landroid/view/animation/Interpolator;

    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    if-nez v0, :cond_2

    if-lez p3, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    long-to-float v0, p5

    const/high16 v2, 0x44fa0000    # 2000.0f

    div-float/2addr v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1

    :cond_2
    return v0
.end method

.method public dnW(F)F
    .locals 0

    return p1
.end method

.method public dnX(II)I
    .locals 5

    const v4, 0x303030

    and-int v0, p1, v4

    if-nez v0, :cond_0

    return p1

    :cond_0
    not-int v1, v0

    and-int/2addr v1, p1

    if-nez p2, :cond_1

    shr-int/lit8 v0, v0, 0x2

    or-int/2addr v0, v1

    return v0

    :cond_1
    shr-int/lit8 v2, v0, 0x1

    const v3, -0x303031

    and-int/2addr v2, v3

    or-int/2addr v1, v2

    shr-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v4

    shr-int/lit8 v0, v0, 0x2

    or-int/2addr v0, v1

    return v0
.end method

.method dnY(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Ljava/util/List;IFF)V
    .locals 11

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v9

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v9, :cond_0

    invoke-interface {p4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/a/f;

    invoke-virtual {v0}, Landroid/support/v7/widget/a/f;->don()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v10

    iget-object v3, v0, Landroid/support/v7/widget/a/f;->dXd:Landroid/support/v7/widget/p;

    iget v4, v0, Landroid/support/v7/widget/a/f;->dXf:F

    iget v5, v0, Landroid/support/v7/widget/a/f;->dXh:F

    iget v6, v0, Landroid/support/v7/widget/a/f;->dXc:I

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Landroid/support/v7/widget/a/c;->HV(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;FFIZ)V

    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->restoreToCount(I)V

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v8

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v4, p6

    move/from16 v5, p7

    move/from16 v6, p5

    invoke-virtual/range {v0 .. v7}, Landroid/support/v7/widget/a/c;->HV(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;FFIZ)V

    invoke-virtual {p1, v8}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    return-void
.end method

.method public dnZ(Landroid/support/v7/widget/p;Ljava/util/List;II)Landroid/support/v7/widget/p;
    .locals 13

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int v5, p3, v0

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int v6, p4, v0

    const/4 v3, 0x0

    const/4 v1, -0x1

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v7, p3, v0

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v8, p4, v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v9

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v9, :cond_4

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    if-lez v7, :cond_8

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    sub-int/2addr v2, v5

    if-gez v2, :cond_0

    iget-object v10, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v10

    iget-object v11, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v11

    if-le v10, v11, :cond_0

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v2, v1, :cond_8

    move-object v3, v0

    :goto_1
    if-gez v7, :cond_7

    iget-object v1, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, v1, p3

    if-lez v1, :cond_1

    iget-object v10, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v10

    iget-object v11, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v11

    if-ge v10, v11, :cond_1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-le v1, v2, :cond_7

    move-object v3, v0

    :goto_2
    if-gez v8, :cond_6

    iget-object v2, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int v2, v2, p4

    if-lez v2, :cond_2

    iget-object v10, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v10

    iget-object v11, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v11

    if-ge v10, v11, :cond_2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v2, v1, :cond_6

    move-object v3, v0

    :goto_3
    if-lez v8, :cond_5

    iget-object v1, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    sub-int/2addr v1, v6

    if-gez v1, :cond_3

    iget-object v10, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v10

    iget-object v11, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v11

    if-le v10, v11, :cond_3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-le v1, v2, :cond_5

    move v12, v1

    move-object v1, v0

    move v0, v12

    :goto_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v1, v0

    goto/16 :goto_0

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_2

    :cond_2
    move v2, v1

    goto :goto_3

    :cond_3
    move v0, v2

    move-object v1, v3

    goto :goto_4

    :cond_4
    return-object v3

    :cond_5
    move v0, v2

    move-object v1, v3

    goto :goto_4

    :cond_6
    move v2, v1

    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_2

    :cond_8
    move v2, v1

    goto :goto_1
.end method

.method public doa(Landroid/support/v7/widget/RecyclerView;IFF)J
    .locals 2

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()Landroid/support/v7/widget/u;

    move-result-object v0

    if-nez v0, :cond_1

    if-ne p2, v1, :cond_0

    const/16 v0, 0xc8

    :goto_0
    int-to-long v0, v0

    return-wide v0

    :cond_0
    const/16 v0, 0xfa

    goto :goto_0

    :cond_1
    if-ne p2, v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->duX()J

    move-result-wide v0

    :goto_1
    return-wide v0

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/u;->dvc()J

    move-result-wide v0

    goto :goto_1
.end method

.method public doc(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public doe(Landroid/support/v7/widget/p;)F
    .locals 1

    const/high16 v0, 0x3f000000    # 0.5f

    return v0
.end method

.method final dof(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)I
    .locals 2

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/a/c;->dnS(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)I

    move-result v0

    invoke-static {p1}, Landroid/support/v4/view/z;->dPn(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/a/c;->dnX(II)I

    move-result v0

    return v0
.end method

.method public dog()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public doh(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;FFIZ)V
    .locals 8

    sget-object v0, Landroid/support/v7/widget/a/c;->dWX:Landroid/support/v7/widget/a/n;

    iget-object v3, p3, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Landroid/support/v7/widget/a/n;->dov(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;FFIZ)V

    return-void
.end method

.method public doi(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)V
    .locals 2

    sget-object v0, Landroid/support/v7/widget/a/c;->dWX:Landroid/support/v7/widget/a/n;

    iget-object v1, p2, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/support/v7/widget/a/n;->dot(Landroid/view/View;)V

    return-void
.end method

.method public dok()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
