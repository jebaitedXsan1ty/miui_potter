.class Landroid/support/v7/widget/a/e;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ItemTouchHelper.java"


# instance fields
.field final synthetic dXb:Landroid/support/v7/widget/a/b;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/a/b;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a/b;->dnD(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    iget-object v2, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iget-object v2, v2, Landroid/support/v7/widget/a/b;->dWu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/widget/a/c;->dnP(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/p;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iget v2, v2, Landroid/support/v7/widget/a/b;->dWH:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iget v1, v1, Landroid/support/v7/widget/a/b;->dWH:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iput v2, v3, Landroid/support/v7/widget/a/b;->dWG:F

    iget-object v2, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iput v1, v2, Landroid/support/v7/widget/a/b;->dWF:F

    iget-object v1, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iget-object v2, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iput v4, v2, Landroid/support/v7/widget/a/b;->dWT:F

    iput v4, v1, Landroid/support/v7/widget/a/b;->dWS:F

    iget-object v1, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    iget-object v1, v1, Landroid/support/v7/widget/a/b;->dWM:Landroid/support/v7/widget/a/c;

    invoke-virtual {v1}, Landroid/support/v7/widget/a/c;->dnR()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/a/e;->dXb:Landroid/support/v7/widget/a/b;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/a/b;->dny(Landroid/support/v7/widget/p;I)V

    :cond_1
    return-void
.end method
