.class Landroid/support/v7/widget/ck;
.super Ljava/lang/Object;
.source "RtlSpacingHelper.java"


# instance fields
.field private enT:Z

.field private enU:I

.field private enV:I

.field private enW:I

.field private enX:I

.field private enY:I

.field private enZ:I

.field private eoa:Z


# direct methods
.method constructor <init>()V
    .locals 2

    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Landroid/support/v7/widget/ck;->enX:I

    iput v0, p0, Landroid/support/v7/widget/ck;->enW:I

    iput v1, p0, Landroid/support/v7/widget/ck;->enV:I

    iput v1, p0, Landroid/support/v7/widget/ck;->enZ:I

    iput v0, p0, Landroid/support/v7/widget/ck;->enU:I

    iput v0, p0, Landroid/support/v7/widget/ck;->enY:I

    iput-boolean v0, p0, Landroid/support/v7/widget/ck;->eoa:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/ck;->enT:Z

    return-void
.end method


# virtual methods
.method public dGU()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/ck;->enX:I

    return v0
.end method

.method public dGV()I
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/ck;->enW:I

    return v0
.end method

.method public dGW(II)V
    .locals 2

    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/ck;->enT:Z

    if-eq p1, v1, :cond_0

    iput p1, p0, Landroid/support/v7/widget/ck;->enU:I

    iput p1, p0, Landroid/support/v7/widget/ck;->enX:I

    :cond_0
    if-eq p2, v1, :cond_1

    iput p2, p0, Landroid/support/v7/widget/ck;->enY:I

    iput p2, p0, Landroid/support/v7/widget/ck;->enW:I

    :cond_1
    return-void
.end method

.method public dGX(Z)V
    .locals 2

    const/high16 v1, -0x80000000

    iget-boolean v0, p0, Landroid/support/v7/widget/ck;->eoa:Z

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/ck;->eoa:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/ck;->enT:Z

    if-eqz v0, :cond_6

    if-eqz p1, :cond_3

    iget v0, p0, Landroid/support/v7/widget/ck;->enZ:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Landroid/support/v7/widget/ck;->enZ:I

    :goto_0
    iput v0, p0, Landroid/support/v7/widget/ck;->enX:I

    iget v0, p0, Landroid/support/v7/widget/ck;->enV:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Landroid/support/v7/widget/ck;->enV:I

    :goto_1
    iput v0, p0, Landroid/support/v7/widget/ck;->enW:I

    :goto_2
    return-void

    :cond_1
    iget v0, p0, Landroid/support/v7/widget/ck;->enU:I

    goto :goto_0

    :cond_2
    iget v0, p0, Landroid/support/v7/widget/ck;->enY:I

    goto :goto_1

    :cond_3
    iget v0, p0, Landroid/support/v7/widget/ck;->enV:I

    if-eq v0, v1, :cond_4

    iget v0, p0, Landroid/support/v7/widget/ck;->enV:I

    :goto_3
    iput v0, p0, Landroid/support/v7/widget/ck;->enX:I

    iget v0, p0, Landroid/support/v7/widget/ck;->enZ:I

    if-eq v0, v1, :cond_5

    iget v0, p0, Landroid/support/v7/widget/ck;->enZ:I

    :goto_4
    iput v0, p0, Landroid/support/v7/widget/ck;->enW:I

    goto :goto_2

    :cond_4
    iget v0, p0, Landroid/support/v7/widget/ck;->enU:I

    goto :goto_3

    :cond_5
    iget v0, p0, Landroid/support/v7/widget/ck;->enY:I

    goto :goto_4

    :cond_6
    iget v0, p0, Landroid/support/v7/widget/ck;->enU:I

    iput v0, p0, Landroid/support/v7/widget/ck;->enX:I

    iget v0, p0, Landroid/support/v7/widget/ck;->enY:I

    iput v0, p0, Landroid/support/v7/widget/ck;->enW:I

    goto :goto_2
.end method

.method public dGY(II)V
    .locals 2

    const/high16 v1, -0x80000000

    iput p1, p0, Landroid/support/v7/widget/ck;->enV:I

    iput p2, p0, Landroid/support/v7/widget/ck;->enZ:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ck;->enT:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/ck;->eoa:Z

    if-eqz v0, :cond_2

    if-eq p2, v1, :cond_0

    iput p2, p0, Landroid/support/v7/widget/ck;->enX:I

    :cond_0
    if-eq p1, v1, :cond_1

    iput p1, p0, Landroid/support/v7/widget/ck;->enW:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eq p1, v1, :cond_3

    iput p1, p0, Landroid/support/v7/widget/ck;->enX:I

    :cond_3
    if-eq p2, v1, :cond_1

    iput p2, p0, Landroid/support/v7/widget/ck;->enW:I

    goto :goto_0
.end method

.method public dGZ()I
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/ck;->eoa:Z

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/ck;->enW:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/ck;->enX:I

    goto :goto_0
.end method

.method public dHa()I
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/ck;->eoa:Z

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/ck;->enX:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/ck;->enW:I

    goto :goto_0
.end method
