.class Landroid/support/v7/widget/aR;
.super Ljava/lang/Object;
.source "Toolbar.java"

# interfaces
.implements Landroid/support/v7/view/menu/i;


# instance fields
.field final synthetic egr:Landroid/support/v7/widget/Toolbar;

.field egs:Landroid/support/v7/view/menu/z;

.field egt:Landroid/support/v7/view/menu/p;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/Toolbar;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dBV(Landroid/support/v7/view/menu/c;)V
    .locals 0

    return-void
.end method

.method public dBW(Landroid/content/Context;Landroid/support/v7/view/menu/p;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egt:Landroid/support/v7/view/menu/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egs:Landroid/support/v7/view/menu/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egt:Landroid/support/v7/view/menu/p;

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egs:Landroid/support/v7/view/menu/z;

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/p;->dJL(Landroid/support/v7/view/menu/z;)Z

    :cond_0
    iput-object p2, p0, Landroid/support/v7/widget/aR;->egt:Landroid/support/v7/view/menu/p;

    return-void
.end method

.method public dBX(Z)V
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egs:Landroid/support/v7/view/menu/z;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egt:Landroid/support/v7/view/menu/p;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egt:Landroid/support/v7/view/menu/p;

    invoke-virtual {v1}, Landroid/support/v7/view/menu/p;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Landroid/support/v7/widget/aR;->egt:Landroid/support/v7/view/menu/p;

    invoke-virtual {v3, v1}, Landroid/support/v7/view/menu/p;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v7/widget/aR;->egs:Landroid/support/v7/view/menu/z;

    if-ne v3, v4, :cond_2

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egt:Landroid/support/v7/view/menu/p;

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egs:Landroid/support/v7/view/menu/z;

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/aR;->dBY(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)Z

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public dBY(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    instance-of v0, v0, Landroid/support/v7/view/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    check-cast v0, Landroid/support/v7/view/c;

    invoke-interface {v0}, Landroid/support/v7/view/c;->dEV()V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->eds:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iput-object v2, v0, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->dzC()V

    iput-object v2, p0, Landroid/support/v7/widget/aR;->egs:Landroid/support/v7/view/menu/z;

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->requestLayout()V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/support/v7/view/menu/z;->dLg(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method public dBZ(Landroid/support/v7/view/menu/p;Landroid/support/v7/view/menu/z;)Z
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->dzG()V

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->eds:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->eds:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p2}, Landroid/support/v7/view/menu/z;->getActionView()Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    iput-object p2, p0, Landroid/support/v7/widget/aR;->egs:Landroid/support/v7/view/menu/z;

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->generateDefaultLayoutParams()Landroid/support/v7/widget/Toolbar$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget v1, v1, Landroid/support/v7/widget/Toolbar;->eef:I

    and-int/lit8 v1, v1, 0x70

    const v2, 0x800003

    or-int/2addr v1, v2

    iput v1, v0, Landroid/support/v7/widget/Toolbar$LayoutParams;->erL:I

    const/4 v1, 0x2

    iput v1, v0, Landroid/support/v7/widget/Toolbar$LayoutParams;->eoA:I

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->dzD()V

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->requestLayout()V

    invoke-virtual {p2, v3}, Landroid/support/v7/view/menu/z;->dLg(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    instance-of v0, v0, Landroid/support/v7/view/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/aR;->egr:Landroid/support/v7/widget/Toolbar;

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->edW:Landroid/view/View;

    check-cast v0, Landroid/support/v7/view/c;

    invoke-interface {v0}, Landroid/support/v7/view/c;->dET()V

    :cond_2
    return v3
.end method

.method public dCa(Landroid/support/v7/view/menu/h;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dCb()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dCc(Landroid/support/v7/view/menu/p;Z)V
    .locals 0

    return-void
.end method
