.class Landroid/support/v7/widget/h;
.super Landroid/support/v7/widget/c;
.source "RecyclerView.java"


# instance fields
.field final synthetic dZS:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Landroid/support/v7/widget/c;-><init>()V

    return-void
.end method


# virtual methods
.method public bZZ(IILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->doZ(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/I;->dwy(IILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/h;->dtB()V

    :cond_0
    return-void
.end method

.method public caa(II)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->doZ(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/I;->dwB(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/h;->dtB()V

    :cond_0
    return-void
.end method

.method public cab(III)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->doZ(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/I;->dwR(III)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/h;->dtB()V

    :cond_0
    return-void
.end method

.method public cac(II)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->doZ(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/I;->dwJ(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/h;->dtB()V

    :cond_0
    return-void
.end method

.method dtB()V
    .locals 2

    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->dYn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->dXG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->dYl:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dXZ:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/z;->dPV(Landroid/view/View;Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->dYL:Z

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_0
.end method

.method public onChanged()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->doZ(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/e;->dZG:Z

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->setDataSetChangedAfterLayout()V

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0}, Landroid/support/v7/widget/I;->dwN()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/h;->dZS:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    :cond_0
    return-void
.end method
