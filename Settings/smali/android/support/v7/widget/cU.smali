.class final Landroid/support/v7/widget/cU;
.super Landroid/support/v7/widget/ap;
.source "OrientationHelper.java"


# direct methods
.method constructor <init>(Landroid/support/v7/widget/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ap;-><init>(Landroid/support/v7/widget/a;Landroid/support/v7/widget/ap;)V

    return-void
.end method


# virtual methods
.method public dzc(Landroid/view/View;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/cU;->edj:Landroid/graphics/Rect;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Landroid/support/v7/widget/a;->dse(Landroid/view/View;ZLandroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edj:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public dze()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dro()I

    move-result v0

    return v0
.end method

.method public dzf(Landroid/view/View;)I
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/a;->dri(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public dzg(Landroid/view/View;)I
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/a;->drb(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public dzh()I
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dre()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsg()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsR()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public dzi(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/a;->dsG(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    sub-int v0, v1, v0

    return v0
.end method

.method public dzj()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsg()I

    move-result v0

    return v0
.end method

.method public dzk()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dre()I

    move-result v0

    return v0
.end method

.method public dzl(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/a;->drk(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public dzn()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dsR()I

    move-result v0

    return v0
.end method

.method public dzq(Landroid/view/View;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    iget-object v1, p0, Landroid/support/v7/widget/cU;->edj:Landroid/graphics/Rect;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Landroid/support/v7/widget/a;->dse(Landroid/view/View;ZLandroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edj:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0
.end method

.method public dzr()I
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v0}, Landroid/support/v7/widget/a;->dre()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v1}, Landroid/support/v7/widget/a;->dsR()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public dzs(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cU;->edi:Landroid/support/v7/widget/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/a;->dsT(I)V

    return-void
.end method
