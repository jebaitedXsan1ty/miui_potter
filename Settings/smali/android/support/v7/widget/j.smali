.class public final Landroid/support/v7/widget/j;
.super Ljava/lang/Object;
.source "RecyclerView.java"


# instance fields
.field dZV:I

.field dZW:Landroid/support/v7/widget/i;

.field private final dZX:Ljava/util/List;

.field final dZY:Ljava/util/ArrayList;

.field final dZZ:Ljava/util/ArrayList;

.field private eaa:I

.field private eab:Landroid/support/v7/widget/k;

.field eac:Ljava/util/ArrayList;

.field final synthetic ead:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    const/4 v1, 0x2

    iput-object p1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/j;->dZX:Ljava/util/List;

    iput v1, p0, Landroid/support/v7/widget/j;->eaa:I

    iput v1, p0, Landroid/support/v7/widget/j;->dZV:I

    return-void
.end method

.method private dtQ(Landroid/support/v7/widget/p;IIJ)Z
    .locals 6

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p1, Landroid/support/v7/widget/p;->mOwnerRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v1

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getNanoTime()J

    move-result-wide v2

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, p4, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/i;->dtH(IJJ)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/b;->bindViewHolder(Landroid/support/v7/widget/p;I)V

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getNanoTime()J

    move-result-wide v0

    iget-object v4, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v5

    sub-long/2addr v0, v2

    invoke-virtual {v4, v5, v0, v1}, Landroid/support/v7/widget/i;->dtC(IJ)V

    invoke-direct {p0, p1}, Landroid/support/v7/widget/j;->dus(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v0

    if-eqz v0, :cond_1

    iput p3, p1, Landroid/support/v7/widget/p;->mPreLayoutPosition:I

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private dtW(Landroid/support/v7/widget/p;)V
    .locals 2

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/j;->dut(Landroid/view/ViewGroup;Z)V

    :cond_0
    return-void
.end method

.method private dus(Landroid/support/v7/widget/p;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->doA()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPI(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v4/view/z;->dPL(Landroid/view/View;I)V

    :cond_0
    invoke-static {v0}, Landroid/support/v4/view/z;->dPG(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x4000

    invoke-virtual {p1, v1}, Landroid/support/v7/widget/p;->addFlags(I)V

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYr:Landroid/support/v7/widget/F;

    invoke-virtual {v1}, Landroid/support/v7/widget/F;->dmJ()Landroid/support/v4/view/d;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/view/z;->dPu(Landroid/view/View;Landroid/support/v4/view/d;)V

    :cond_1
    return-void
.end method

.method private dut(Landroid/view/ViewGroup;Z)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x4

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Landroid/support/v7/widget/j;->dut(Landroid/view/ViewGroup;Z)V

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_3

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_1
    return-void

    :cond_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Landroid/support/v7/widget/j;->duk()V

    return-void
.end method

.method dtN()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method dtO(Landroid/support/v7/widget/p;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v0

    return v0

    :cond_0
    iget v2, p1, Landroid/support/v7/widget/p;->mPosition:I

    if-ltz v2, :cond_1

    iget v2, p1, Landroid/support/v7/widget/p;->mPosition:I

    iget-object v3, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v3}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v3

    if-lt v2, v3, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Inconsistency detected. Invalid view holder adapter position"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v2}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    iget v3, p1, Landroid/support/v7/widget/p;->mPosition:I

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/b;->getItemViewType(I)I

    move-result v2

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v3

    if-eq v2, v3, :cond_3

    return v1

    :cond_3
    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v2}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->getItemId()J

    move-result-wide v2

    iget-object v4, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    iget v5, p1, Landroid/support/v7/widget/p;->mPosition:I

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/b;->getItemId(I)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    :goto_0
    return v0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    return v0
.end method

.method dtP(Landroid/view/View;)V
    .locals 3

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/p;->hasAnyOfTheFlags(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isUpdated()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->dpI(Landroid/support/v7/widget/p;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/p;->setScrapContainer(Landroid/support/v7/widget/j;Z)V

    iget-object v1, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    :cond_3
    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/p;->setScrapContainer(Landroid/support/v7/widget/j;Z)V

    iget-object v1, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method dtR(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    return-object v0
.end method

.method public dtS(I)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/j;->dui(IZ)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method dtT()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    if-eqz v0, :cond_0

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/p;->addFlags(I)V

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/p;->addChangePayload(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/j;->duk()V

    :cond_2
    return-void
.end method

.method dtU(IZ)Landroid/support/v7/widget/p;
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->wasReturnedFromScrap()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v4

    if-ne v4, p1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    iget-object v4, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v4, v4, Landroid/support/v7/widget/e;->dZD:Z

    if-nez v4, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    :cond_0
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/p;->addFlags(I)V

    return-object v0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    if-nez p2, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/G;->dvP(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dwc(Landroid/view/View;)V

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/G;->dvM(Landroid/view/View;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "layout index should not be -1 after unhiding a view:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->dYC:Landroid/support/v7/widget/G;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/G;->dvW(I)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/j;->dtP(Landroid/view/View;)V

    const/16 v0, 0x2020

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/p;->addFlags(I)V

    return-object v1

    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v3

    if-ne v3, p1, :cond_6

    if-nez p2, :cond_5

    iget-object v2, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_5
    return-object v0

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_7
    return-object v5
.end method

.method dtV(Landroid/support/v7/widget/b;Landroid/support/v7/widget/b;Z)V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/widget/j;->clear()V

    invoke-virtual {p0}, Landroid/support/v7/widget/j;->getRecycledViewPool()Landroid/support/v7/widget/i;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/i;->dtL(Landroid/support/v7/widget/b;Landroid/support/v7/widget/b;Z)V

    return-void
.end method

.method dtX(IIZ)V
    .locals 4

    add-int v2, p1, p2

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    if-eqz v0, :cond_0

    iget v3, v0, Landroid/support/v7/widget/p;->mPosition:I

    if-lt v3, v2, :cond_1

    neg-int v3, p2

    invoke-virtual {v0, v3, p3}, Landroid/support/v7/widget/p;->offsetPosition(IZ)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v3, v0, Landroid/support/v7/widget/p;->mPosition:I

    if-lt v3, p1, :cond_0

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/p;->addFlags(I)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/j;->dun(I)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public dtY(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/widget/j;->eaa:I

    invoke-virtual {p0}, Landroid/support/v7/widget/j;->dua()V

    return-void
.end method

.method public dtZ(Landroid/view/View;)V
    .locals 3

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isTmpDetached()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isScrap()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->unScrap()V

    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/j;->dur(Landroid/support/v7/widget/p;)V

    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/p;->wasReturnedFromScrap()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->clearReturnedFromScrapFlag()V

    goto :goto_0
.end method

.method dua()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget v0, v0, Landroid/support/v7/widget/a;->dZq:I

    :cond_0
    iget v1, p0, Landroid/support/v7/widget/j;->eaa:I

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/j;->dZV:I

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/j;->dZV:I

    if-le v1, v2, :cond_1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/j;->dun(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method dub(I)Landroid/support/v7/widget/p;
    .locals 10

    const/16 v9, 0x20

    const/4 v8, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    return-object v8

    :cond_1
    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->wasReturnedFromScrap()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v4

    if-ne v4, p1, :cond_2

    invoke-virtual {v0, v9}, Landroid/support/v7/widget/p;->addFlags(I)V

    return-object v0

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/I;->dwQ(I)I

    move-result v0

    if-lez v0, :cond_5

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v2}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v2

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/b;->getItemId(I)J

    move-result-wide v4

    :goto_1
    if-ge v1, v3, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->wasReturnedFromScrap()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getItemId()J

    move-result-wide v6

    cmp-long v2, v6, v4

    if-nez v2, :cond_4

    invoke-virtual {v0, v9}, Landroid/support/v7/widget/p;->addFlags(I)V

    return-object v0

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_5
    return-object v8
.end method

.method duc(II)V
    .locals 4

    add-int v2, p1, p2

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    if-nez v0, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v3, v0, Landroid/support/v7/widget/p;->mPosition:I

    if-lt v3, p1, :cond_0

    if-ge v3, v2, :cond_0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/p;->addFlags(I)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/j;->dun(I)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method dud(Landroid/support/v7/widget/p;)V
    .locals 1

    invoke-static {p1}, Landroid/support/v7/widget/p;->-get1(Landroid/support/v7/widget/p;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v7/widget/p;->-set1(Landroid/support/v7/widget/p;Landroid/support/v7/widget/j;)Landroid/support/v7/widget/j;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v7/widget/p;->-set0(Landroid/support/v7/widget/p;Z)Z

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->clearReturnedFromScrapFlag()V

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method due(Landroid/support/v7/widget/p;Z)V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x4000

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->dqw(Landroid/support/v7/widget/p;)V

    invoke-virtual {p1, v1}, Landroid/support/v7/widget/p;->hasAnyOfTheFlags(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/widget/p;->setFlags(II)V

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/z;->dPu(Landroid/view/View;Landroid/support/v4/view/d;)V

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/j;->dul(Landroid/support/v7/widget/p;)V

    :cond_1
    iput-object v2, p1, Landroid/support/v7/widget/p;->mOwnerRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0}, Landroid/support/v7/widget/j;->getRecycledViewPool()Landroid/support/v7/widget/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/i;->dtG(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method duf(II)V
    .locals 8

    const/4 v5, 0x0

    if-ge p1, p2, :cond_1

    const/4 v0, -0x1

    move v1, v0

    move v2, p2

    move v3, p1

    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v5

    :goto_1
    if-ge v4, v6, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    if-eqz v0, :cond_0

    iget v7, v0, Landroid/support/v7/widget/p;->mPosition:I

    if-ge v7, v3, :cond_2

    :cond_0
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    move v1, v0

    move v2, p1

    move v3, p2

    goto :goto_0

    :cond_2
    iget v7, v0, Landroid/support/v7/widget/p;->mPosition:I

    if-gt v7, v2, :cond_0

    iget v7, v0, Landroid/support/v7/widget/p;->mPosition:I

    if-ne v7, p1, :cond_3

    sub-int v7, p2, p1

    invoke-virtual {v0, v7, v5}, Landroid/support/v7/widget/p;->offsetPosition(IZ)V

    goto :goto_2

    :cond_3
    invoke-virtual {v0, v1, v5}, Landroid/support/v7/widget/p;->offsetPosition(IZ)V

    goto :goto_2

    :cond_4
    return-void
.end method

.method dug(IZJ)Landroid/support/v7/widget/p;
    .locals 11

    const/16 v9, 0x2000

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    if-ltz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid item position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "). Item count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v2}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/j;->dub(I)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_6

    move v2, v7

    :goto_0
    if-nez v0, :cond_4

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/j;->dtU(IZ)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/j;->dtO(Landroid/support/v7/widget/p;)Z

    move-result v1

    if-nez v1, :cond_8

    if-nez p2, :cond_3

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/p;->addFlags(I)V

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isScrap()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v1, v3, v6}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->unScrap()V

    :cond_2
    :goto_1
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/j;->dur(Landroid/support/v7/widget/p;)V

    :cond_3
    move-object v0, v8

    :cond_4
    :goto_2
    if-nez v0, :cond_18

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/I;->dwQ(I)I

    move-result v3

    if-ltz v3, :cond_5

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1}, Landroid/support/v7/widget/b;->getItemCount()I

    move-result v1

    if-lt v3, v1, :cond_9

    :cond_5
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Inconsistency detected. Invalid item position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "(offset:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v2}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v2, v6

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v0}, Landroid/support/v7/widget/p;->wasReturnedFromScrap()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->clearReturnedFromScrapFlag()V

    goto :goto_1

    :cond_8
    move v2, v7

    goto :goto_2

    :cond_9
    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/b;->getItemViewType(I)I

    move-result v1

    iget-object v4, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v4}, Landroid/support/v7/widget/b;->hasStableIds()Z

    move-result v4

    if-eqz v4, :cond_17

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/b;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, v1, p2}, Landroid/support/v7/widget/j;->duo(JIZ)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_17

    iput v3, v0, Landroid/support/v7/widget/p;->mPosition:I

    :goto_3
    if-nez v0, :cond_b

    iget-object v2, p0, Landroid/support/v7/widget/j;->eab:Landroid/support/v7/widget/k;

    if-eqz v2, :cond_b

    iget-object v2, p0, Landroid/support/v7/widget/j;->eab:Landroid/support/v7/widget/k;

    invoke-virtual {v2, p0, p1, v1}, Landroid/support/v7/widget/k;->duv(Landroid/support/v7/widget/j;II)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->doJ(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getViewForPositionAndType returned a view which does not have a ViewHolder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    invoke-virtual {v0}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v2

    if-eqz v2, :cond_b

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    if-nez v0, :cond_c

    invoke-virtual {p0}, Landroid/support/v7/widget/j;->getRecycledViewPool()Landroid/support/v7/widget/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/i;->dtK(I)Landroid/support/v7/widget/p;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->resetInternal()V

    sget-boolean v2, Landroid/support/v7/widget/RecyclerView;->dYm:Z

    if-eqz v2, :cond_c

    invoke-direct {p0, v0}, Landroid/support/v7/widget/j;->dtW(Landroid/support/v7/widget/p;)V

    :cond_c
    if-nez v0, :cond_16

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getNanoTime()J

    move-result-wide v2

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, p3, v4

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/i;->dtE(IJJ)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_d

    return-object v8

    :cond_d
    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    iget-object v4, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4, v1}, Landroid/support/v7/widget/b;->createViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;

    move-result-object v0

    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->-get0()Z

    move-result v4

    if-eqz v4, :cond_e

    iget-object v4, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->dqz(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v4

    if-eqz v4, :cond_e

    new-instance v5, Ljava/lang/ref/WeakReference;

    invoke-direct {v5, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v5, v0, Landroid/support/v7/widget/p;->mNestedRecyclerView:Ljava/lang/ref/WeakReference;

    :cond_e
    iget-object v4, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getNanoTime()J

    move-result-wide v4

    iget-object v8, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    sub-long v2, v4, v2

    invoke-virtual {v8, v1, v2, v3}, Landroid/support/v7/widget/i;->dtF(IJ)V

    move-object v1, v0

    :goto_4
    if-eqz v7, :cond_f

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_f

    invoke-virtual {v1, v9}, Landroid/support/v7/widget/p;->hasAnyOfTheFlags(I)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {v1, v6, v9}, Landroid/support/v7/widget/p;->setFlags(II)V

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    iget-boolean v0, v0, Landroid/support/v7/widget/e;->dZF:Z

    if-eqz v0, :cond_f

    invoke-static {v1}, Landroid/support/v7/widget/u;->duP(Landroid/support/v7/widget/p;)I

    move-result v0

    or-int/lit16 v0, v0, 0x1000

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    iget-object v3, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->getUnmodifiedPayloads()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/support/v7/widget/u;->dva(Landroid/support/v7/widget/e;Landroid/support/v7/widget/p;ILjava/util/List;)Landroid/support/v7/widget/K;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1, v0}, Landroid/support/v7/widget/RecyclerView;->doN(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;)V

    :cond_f
    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->isBound()Z

    move-result v0

    if-eqz v0, :cond_10

    iput p1, v1, Landroid/support/v7/widget/p;->mPreLayoutPosition:I

    move v2, v6

    :goto_5
    iget-object v0, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_12

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v3, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_6
    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eaf:Landroid/support/v7/widget/p;

    if-eqz v7, :cond_14

    :goto_7
    iput-boolean v2, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eah:Z

    return-object v1

    :cond_10
    invoke-virtual {v1}, Landroid/support/v7/widget/p;->isBound()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->needsUpdate()Z

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {v1}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_11
    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYJ:Landroid/support/v7/widget/I;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/I;->dwQ(I)I

    move-result v2

    move-object v0, p0

    move v3, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/j;->dtQ(Landroid/support/v7/widget/p;IIJ)Z

    move-result v0

    move v2, v0

    goto :goto_5

    :cond_12
    iget-object v3, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v3

    if-nez v3, :cond_13

    iget-object v3, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v3, v1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_6

    :cond_13
    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    goto :goto_6

    :cond_14
    move v2, v6

    goto :goto_7

    :cond_15
    move v2, v6

    goto :goto_5

    :cond_16
    move-object v1, v0

    goto/16 :goto_4

    :cond_17
    move v7, v2

    goto/16 :goto_3

    :cond_18
    move-object v1, v0

    move v7, v2

    goto/16 :goto_4

    :cond_19
    move-object v0, v8

    move v2, v6

    goto/16 :goto_0
.end method

.method duh()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->clearOldPosition()V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->clearOldPosition()V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_2
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->clearOldPosition()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    return-void
.end method

.method dui(IZ)Landroid/view/View;
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/support/v7/widget/j;->dug(IZJ)Landroid/support/v7/widget/p;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    return-object v0
.end method

.method duj()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    iput-boolean v3, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->eae:Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method duk()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/j;->dun(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->-get0()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    invoke-virtual {v0}, Landroid/support/v7/widget/E;->dvI()V

    :cond_1
    return-void
.end method

.method dul(Landroid/support/v7/widget/p;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYt:Landroid/support/v7/widget/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYt:Landroid/support/v7/widget/n;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/n;->onViewRecycled(Landroid/support/v7/widget/p;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/b;->onViewRecycled(Landroid/support/v7/widget/p;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/H;->dwq(Landroid/support/v7/widget/p;)V

    :cond_2
    return-void
.end method

.method dum()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->eac:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method dun(I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/j;->due(Landroid/support/v7/widget/p;Z)V

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method duo(JIZ)Landroid/support/v7/widget/p;
    .locals 7

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getItemId()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->wasReturnedFromScrap()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v2

    if-ne p3, v2, :cond_1

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/p;->addFlags(I)V

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v1}, Landroid/support/v7/widget/e;->dtn()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/p;->setFlags(II)V

    :cond_0
    return-object v0

    :cond_1
    if-nez p4, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/j;->dZY:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v2, v3, v4}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    iget-object v0, v0, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/j;->duq(Landroid/view/View;)V

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getItemId()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_6

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->getItemViewType()I

    move-result v2

    if-ne p3, v2, :cond_5

    if-nez p4, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_4
    return-object v0

    :cond_5
    if-nez p4, :cond_6

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/j;->dun(I)V

    return-object v5

    :cond_6
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_7
    return-object v5
.end method

.method public dup()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZX:Ljava/util/List;

    return-object v0
.end method

.method duq(Landroid/view/View;)V
    .locals 2

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->doH(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v7/widget/p;->-set1(Landroid/support/v7/widget/p;Landroid/support/v7/widget/j;)Landroid/support/v7/widget/j;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v7/widget/p;->-set0(Landroid/support/v7/widget/p;Z)Z

    invoke-virtual {v0}, Landroid/support/v7/widget/p;->clearReturnedFromScrapFlag()V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/j;->dur(Landroid/support/v7/widget/p;)V

    return-void
.end method

.method dur(Landroid/support/v7/widget/p;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isScrap()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Scrapped or attached views may not be recycled. isScrap:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isScrap()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, " isAttached:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isTmpDetached()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Tmp detached view should be removed from RecyclerView before it can be recycled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p1}, Landroid/support/v7/widget/p;->shouldIgnore()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->doP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-static {p1}, Landroid/support/v7/widget/p;->-wrap0(Landroid/support/v7/widget/p;)Z

    move-result v4

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v0, :cond_b

    if-eqz v4, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/b;->onFailedToRecycleView(Landroid/support/v7/widget/p;)Z

    move-result v0

    :goto_1
    if-nez v0, :cond_5

    invoke-virtual {p1}, Landroid/support/v7/widget/p;->isRecyclable()Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_5
    iget v0, p0, Landroid/support/v7/widget/j;->dZV:I

    if-lez v0, :cond_c

    const/16 v0, 0x20e

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/p;->hasAnyOfTheFlags(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v3, p0, Landroid/support/v7/widget/j;->dZV:I

    if-lt v0, v3, :cond_6

    if-lez v0, :cond_6

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/j;->dun(I)V

    add-int/lit8 v0, v0, -0x1

    :cond_6
    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->-get0()Z

    move-result v3

    if-eqz v3, :cond_8

    if-lez v0, :cond_8

    iget-object v3, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    iget v5, p1, Landroid/support/v7/widget/p;->mPosition:I

    invoke-virtual {v3, v5}, Landroid/support/v7/widget/E;->dvJ(I)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_8

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_2
    if-ltz v3, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    iget v0, v0, Landroid/support/v7/widget/p;->mPosition:I

    iget-object v5, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/E;->dvJ(I)Z

    move-result v0

    if-nez v0, :cond_d

    :cond_7
    add-int/lit8 v0, v3, 0x1

    :cond_8
    iget-object v3, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v3, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move v0, v1

    :goto_3
    if-nez v0, :cond_9

    invoke-virtual {p0, p1, v1}, Landroid/support/v7/widget/j;->due(Landroid/support/v7/widget/p;Z)V

    move v2, v1

    :cond_9
    :goto_4
    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->dYI:Landroid/support/v7/widget/H;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/H;->dwq(Landroid/support/v7/widget/p;)V

    if-nez v0, :cond_a

    xor-int/lit8 v0, v2, 0x1

    if-eqz v0, :cond_a

    if-eqz v4, :cond_a

    iput-object v6, p1, Landroid/support/v7/widget/p;->mOwnerRecyclerView:Landroid/support/v7/widget/RecyclerView;

    :cond_a
    return-void

    :cond_b
    move v0, v2

    goto :goto_1

    :cond_c
    move v0, v2

    goto :goto_3

    :cond_d
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_2

    :cond_e
    move v0, v2

    goto :goto_3

    :cond_f
    move v0, v2

    goto :goto_4
.end method

.method duu(II)V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/p;

    if-eqz v0, :cond_0

    iget v3, v0, Landroid/support/v7/widget/p;->mPosition:I

    if-lt v3, p1, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v0, p2, v3}, Landroid/support/v7/widget/p;->offsetPosition(IZ)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method getRecycledViewPool()Landroid/support/v7/widget/i;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/i;

    invoke-direct {v0}, Landroid/support/v7/widget/i;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    return-object v0
.end method

.method setRecycledViewPool(Landroid/support/v7/widget/i;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    invoke-virtual {v0}, Landroid/support/v7/widget/i;->dtI()V

    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/j;->dZW:Landroid/support/v7/widget/i;

    iget-object v1, p0, Landroid/support/v7/widget/j;->ead:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/i;->dtD(Landroid/support/v7/widget/b;)V

    :cond_1
    return-void
.end method

.method setViewCacheExtension(Landroid/support/v7/widget/k;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/j;->eab:Landroid/support/v7/widget/k;

    return-void
.end method
