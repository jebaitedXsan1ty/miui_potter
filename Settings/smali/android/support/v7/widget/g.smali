.class Landroid/support/v7/widget/g;
.super Ljava/lang/Object;
.source "RecyclerView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private dZL:Z

.field private dZM:I

.field private dZN:Z

.field private dZO:Landroid/widget/OverScroller;

.field private dZP:I

.field final synthetic dZQ:Landroid/support/v7/widget/RecyclerView;

.field dZR:Landroid/view/animation/Interpolator;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    const/4 v1, 0x0

    iput-object p1, p0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroid/support/v7/widget/RecyclerView;->dZa:Landroid/view/animation/Interpolator;

    iput-object v0, p0, Landroid/support/v7/widget/g;->dZR:Landroid/view/animation/Interpolator;

    iput-boolean v1, p0, Landroid/support/v7/widget/g;->dZL:Z

    iput-boolean v1, p0, Landroid/support/v7/widget/g;->dZN:Z

    new-instance v0, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/support/v7/widget/RecyclerView;->dZa:Landroid/view/animation/Interpolator;

    invoke-direct {v0, v1, v2}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Landroid/support/v7/widget/g;->dZO:Landroid/widget/OverScroller;

    return-void
.end method

.method private dtq(F)F
    .locals 2

    const/high16 v0, 0x3f000000    # 0.5f

    sub-float v0, p1, v0

    const v1, 0x3ef1463b

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private dtr()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/g;->dZN:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/g;->dZL:Z

    return-void
.end method

.method static synthetic dtt(Landroid/support/v7/widget/g;)Landroid/widget/OverScroller;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/g;->dZO:Landroid/widget/OverScroller;

    return-object v0
.end method

.method private dtu(IIII)I
    .locals 9

    const/4 v0, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-le v2, v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int v1, p3, p3

    mul-int v4, p4, p4

    add-int/2addr v1, v4

    int-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v4, v4

    mul-int v1, p1, p1

    mul-int v5, p2, p2

    add-int/2addr v1, v5

    int-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-int v5, v6

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v1

    :goto_0
    div-int/lit8 v6, v1, 0x2

    int-to-float v5, v5

    mul-float/2addr v5, v8

    int-to-float v7, v1

    div-float/2addr v5, v7

    invoke-static {v8, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    int-to-float v7, v6

    int-to-float v6, v6

    invoke-direct {p0, v5}, Landroid/support/v7/widget/g;->dtq(F)F

    move-result v5

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    if-lez v4, :cond_2

    int-to-float v0, v4

    div-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    :goto_1
    const/16 v1, 0x7d0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v1

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v8

    const/high16 v1, 0x43960000    # 300.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_2
.end method

.method private dtx()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/g;->dZL:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/g;->dZN:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/g;->dty()V

    :cond_0
    return-void
.end method


# virtual methods
.method public dtA(III)V
    .locals 1

    sget-object v0, Landroid/support/v7/widget/RecyclerView;->dZa:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/v7/widget/g;->dts(IIILandroid/view/animation/Interpolator;)V

    return-void
.end method

.method public dtp(IILandroid/view/animation/Interpolator;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v0}, Landroid/support/v7/widget/g;->dtu(IIII)I

    move-result v0

    if-nez p3, :cond_0

    sget-object p3, Landroid/support/v7/widget/RecyclerView;->dZa:Landroid/view/animation/Interpolator;

    :cond_0
    invoke-virtual {p0, p1, p2, v0, p3}, Landroid/support/v7/widget/g;->dts(IIILandroid/view/animation/Interpolator;)V

    return-void
.end method

.method public dts(IIILandroid/view/animation/Interpolator;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/g;->dZR:Landroid/view/animation/Interpolator;

    if-eq v0, p4, :cond_0

    iput-object p4, p0, Landroid/support/v7/widget/g;->dZR:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/widget/OverScroller;

    iget-object v2, p0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p4}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Landroid/support/v7/widget/g;->dZO:Landroid/widget/OverScroller;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setScrollState(I)V

    iput v1, p0, Landroid/support/v7/widget/g;->dZM:I

    iput v1, p0, Landroid/support/v7/widget/g;->dZP:I

    iget-object v0, p0, Landroid/support/v7/widget/g;->dZO:Landroid/widget/OverScroller;

    move v2, v1

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/g;->dZO:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/g;->dty()V

    return-void
.end method

.method public dtv(II)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/support/v7/widget/g;->dtz(IIII)V

    return-void
.end method

.method public dtw(II)V
    .locals 9

    const v6, 0x7fffffff

    const/high16 v5, -0x80000000

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setScrollState(I)V

    iput v1, p0, Landroid/support/v7/widget/g;->dZM:I

    iput v1, p0, Landroid/support/v7/widget/g;->dZP:I

    iget-object v0, p0, Landroid/support/v7/widget/g;->dZO:Landroid/widget/OverScroller;

    move v2, v1

    move v3, p1

    move v4, p2

    move v7, v5

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    invoke-virtual {p0}, Landroid/support/v7/widget/g;->dty()V

    return-void
.end method

.method dty()V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/g;->dZL:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/g;->dZN:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p0}, Landroid/support/v4/view/z;->dPV(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public dtz(IIII)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/g;->dtu(IIII)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v7/widget/g;->dtA(III)V

    return-void
.end method

.method public run()V
    .locals 19

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/g;->stop()V

    return-void

    :cond_0
    invoke-direct/range {p0 .. p0}, Landroid/support/v7/widget/g;->dtr()V

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->dqN()V

    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/support/v7/widget/g;->dZO:Landroid/widget/OverScroller;

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v15, v2, Landroid/support/v7/widget/a;->dZp:Landroid/support/v7/widget/q;

    invoke-virtual {v14}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->dqr(Landroid/support/v7/widget/RecyclerView;)[I

    move-result-object v5

    invoke-virtual {v14}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v16

    invoke-virtual {v14}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v17

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/g;->dZP:I

    sub-int v3, v16, v2

    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v7/widget/g;->dZM:I

    sub-int v4, v17, v2

    const/4 v13, 0x0

    const/4 v11, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v7/widget/g;->dZP:I

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v7/widget/g;->dZM:I

    const/4 v12, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Landroid/support/v7/widget/RecyclerView;->dpm(II[I[II)Z

    move-result v2

    if-eqz v2, :cond_26

    const/4 v2, 0x0

    aget v2, v5, v2

    sub-int/2addr v3, v2

    const/4 v2, 0x1

    aget v2, v5, v2

    sub-int/2addr v4, v2

    move v9, v4

    move v10, v3

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYp:Landroid/support/v7/widget/b;

    if-eqz v2, :cond_25

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->dpQ()V

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->dqP()V

    const-string/jumbo v2, "RV Scroll"

    invoke-static {v2}, Landroid/support/v4/os/c;->eiZ(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->doQ(Landroid/support/v7/widget/e;)V

    if-eqz v10, :cond_24

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v2, v10, v3, v4}, Landroid/support/v7/widget/a;->dsM(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v4

    sub-int v5, v10, v4

    :goto_1
    if-eqz v9, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v6, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v2, v9, v3, v6}, Landroid/support/v7/widget/a;->dsJ(ILandroid/support/v7/widget/j;Landroid/support/v7/widget/e;)I

    move-result v3

    sub-int v2, v9, v3

    :goto_2
    invoke-static {}, Landroid/support/v4/os/c;->eiY()V

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->dql()V

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->dpS()V

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/support/v7/widget/RecyclerView;->dpx(Z)V

    if-eqz v15, :cond_12

    invoke-virtual {v15}, Landroid/support/v7/widget/q;->duI()Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_12

    invoke-virtual {v15}, Landroid/support/v7/widget/q;->isRunning()Z

    move-result v6

    if-eqz v6, :cond_22

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v6, Landroid/support/v7/widget/RecyclerView;->dYM:Landroid/support/v7/widget/e;

    invoke-virtual {v6}, Landroid/support/v7/widget/e;->getItemCount()I

    move-result v6

    if-nez v6, :cond_13

    invoke-virtual {v15}, Landroid/support/v7/widget/q;->stop()V

    move v6, v2

    move/from16 v18, v3

    move v3, v4

    move/from16 v4, v18

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYg:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getOverScrollMode()I

    move-result v2

    const/4 v7, 0x2

    if-eq v2, v7, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v10, v9}, Landroid/support/v7/widget/RecyclerView;->dpt(II)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual/range {v2 .. v8}, Landroid/support/v7/widget/RecyclerView;->dpK(IIII[II)Z

    move-result v2

    if-nez v2, :cond_8

    if-nez v5, :cond_3

    if-eqz v6, :cond_8

    :cond_3
    invoke-virtual {v14}, Landroid/widget/OverScroller;->getCurrVelocity()F

    move-result v2

    float-to-int v2, v2

    const/4 v7, 0x0

    move/from16 v0, v16

    if-eq v5, v0, :cond_21

    if-gez v5, :cond_15

    neg-int v7, v2

    move v8, v7

    :goto_4
    const/4 v7, 0x0

    move/from16 v0, v17

    if-eq v6, v0, :cond_20

    if-gez v6, :cond_17

    neg-int v2, v2

    :cond_4
    :goto_5
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v7}, Landroid/support/v7/widget/RecyclerView;->getOverScrollMode()I

    move-result v7

    const/4 v11, 0x2

    if-eq v7, v11, :cond_5

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v7, v8, v2}, Landroid/support/v7/widget/RecyclerView;->doF(II)V

    :cond_5
    if-nez v8, :cond_6

    move/from16 v0, v16

    if-ne v5, v0, :cond_18

    :cond_6
    :goto_6
    if-nez v2, :cond_7

    move/from16 v0, v17

    if-ne v6, v0, :cond_19

    :cond_7
    :goto_7
    invoke-virtual {v14}, Landroid/widget/OverScroller;->abortAnimation()V

    :cond_8
    if-nez v3, :cond_9

    if-eqz v4, :cond_a

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v3, v4}, Landroid/support/v7/widget/RecyclerView;->dpP(II)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->dps(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v2

    if-nez v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    :cond_b
    if-eqz v9, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v2}, Landroid/support/v7/widget/a;->dsw()Z

    move-result v2

    if-eqz v2, :cond_1b

    if-ne v4, v9, :cond_1a

    const/4 v2, 0x1

    :goto_8
    if-eqz v10, :cond_1d

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    invoke-virtual {v4}, Landroid/support/v7/widget/a;->dsE()Z

    move-result v4

    if-eqz v4, :cond_1d

    if-ne v3, v10, :cond_1c

    const/4 v3, 0x1

    :goto_9
    if-nez v10, :cond_1e

    if-nez v9, :cond_1e

    :cond_c
    const/4 v2, 0x1

    :goto_a
    invoke-virtual {v14}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_d

    if-nez v2, :cond_1f

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->dpd(I)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1f

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setScrollState(I)V

    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->-get0()Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYk:Landroid/support/v7/widget/E;

    invoke-virtual {v2}, Landroid/support/v7/widget/E;->dvI()V

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->dpy(I)V

    :cond_f
    :goto_b
    if-eqz v15, :cond_11

    invoke-virtual {v15}, Landroid/support/v7/widget/q;->duI()Z

    move-result v2

    if-eqz v2, :cond_10

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v15, v2, v3}, Landroid/support/v7/widget/q;->duH(Landroid/support/v7/widget/q;II)V

    :cond_10
    move-object/from16 v0, p0

    iget-boolean v2, v0, Landroid/support/v7/widget/g;->dZN:Z

    if-nez v2, :cond_11

    invoke-virtual {v15}, Landroid/support/v7/widget/q;->stop()V

    :cond_11
    invoke-direct/range {p0 .. p0}, Landroid/support/v7/widget/g;->dtx()V

    return-void

    :cond_12
    move v6, v2

    move/from16 v18, v3

    move v3, v4

    move/from16 v4, v18

    goto/16 :goto_3

    :cond_13
    invoke-virtual {v15}, Landroid/support/v7/widget/q;->duD()I

    move-result v7

    if-lt v7, v6, :cond_14

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v15, v6}, Landroid/support/v7/widget/q;->duE(I)V

    sub-int v6, v10, v5

    sub-int v7, v9, v2

    invoke-static {v15, v6, v7}, Landroid/support/v7/widget/q;->duH(Landroid/support/v7/widget/q;II)V

    move v6, v2

    move/from16 v18, v3

    move v3, v4

    move/from16 v4, v18

    goto/16 :goto_3

    :cond_14
    sub-int v6, v10, v5

    sub-int v7, v9, v2

    invoke-static {v15, v6, v7}, Landroid/support/v7/widget/q;->duH(Landroid/support/v7/widget/q;II)V

    move v6, v2

    move/from16 v18, v3

    move v3, v4

    move/from16 v4, v18

    goto/16 :goto_3

    :cond_15
    if-lez v5, :cond_16

    move v8, v2

    goto/16 :goto_4

    :cond_16
    const/4 v7, 0x0

    move v8, v7

    goto/16 :goto_4

    :cond_17
    if-gtz v6, :cond_4

    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_18
    invoke-virtual {v14}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v5

    if-nez v5, :cond_8

    goto/16 :goto_6

    :cond_19
    invoke-virtual {v14}, Landroid/widget/OverScroller;->getFinalY()I

    move-result v2

    if-nez v2, :cond_8

    goto/16 :goto_7

    :cond_1a
    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_1b
    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_1c
    const/4 v3, 0x0

    goto/16 :goto_9

    :cond_1d
    const/4 v3, 0x0

    goto/16 :goto_9

    :cond_1e
    if-nez v3, :cond_c

    goto/16 :goto_a

    :cond_1f
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/g;->dty()V

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYT:Landroid/support/v7/widget/J;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v3, v10, v9}, Landroid/support/v7/widget/J;->dwY(Landroid/support/v7/widget/RecyclerView;II)V

    goto/16 :goto_b

    :cond_20
    move v2, v7

    goto/16 :goto_5

    :cond_21
    move v8, v7

    goto/16 :goto_4

    :cond_22
    move v6, v2

    move/from16 v18, v3

    move v3, v4

    move/from16 v4, v18

    goto/16 :goto_3

    :cond_23
    move v2, v8

    move v3, v11

    goto/16 :goto_2

    :cond_24
    move v5, v12

    move v4, v13

    goto/16 :goto_1

    :cond_25
    move v6, v8

    move v5, v12

    move v4, v11

    move v3, v13

    goto/16 :goto_3

    :cond_26
    move v9, v4

    move v10, v3

    goto/16 :goto_0
.end method

.method public stop()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/g;->dZQ:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Landroid/support/v7/widget/g;->dZO:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->abortAnimation()V

    return-void
.end method
