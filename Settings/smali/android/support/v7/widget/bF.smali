.class public final Landroid/support/v7/widget/bF;
.super Ljava/lang/Object;
.source "AppCompatDrawableManager.java"


# static fields
.field private static final eiT:[I

.field private static final eiU:Landroid/graphics/PorterDuff$Mode;

.field private static final eiV:[I

.field private static final eiW:[I

.field private static final eiZ:Landroid/support/v7/widget/aK;

.field private static final ejb:[I

.field private static final ejd:[I

.field private static eje:Landroid/support/v7/widget/bF;

.field private static final ejg:[I


# instance fields
.field private eiX:Z

.field private eiY:Landroid/support/v4/a/b;

.field private eja:Ljava/util/WeakHashMap;

.field private ejc:Landroid/support/v4/a/u;

.field private ejf:Landroid/util/TypedValue;

.field private final ejh:Ljava/util/WeakHashMap;

.field private final eji:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x6

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sput-object v0, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    new-instance v0, Landroid/support/v7/widget/aK;

    invoke-direct {v0, v7}, Landroid/support/v7/widget/aK;-><init>(I)V

    sput-object v0, Landroid/support/v7/widget/bF;->eiZ:Landroid/support/v7/widget/aK;

    new-array v0, v6, [I

    sget v1, Landroid/support/v7/b/e;->dPT:I

    aput v1, v0, v3

    sget v1, Landroid/support/v7/b/e;->dPR:I

    aput v1, v0, v4

    sget v1, Landroid/support/v7/b/e;->dPd:I

    aput v1, v0, v5

    sput-object v0, Landroid/support/v7/widget/bF;->eiW:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    sget v1, Landroid/support/v7/b/e;->dPp:I

    aput v1, v0, v3

    sget v1, Landroid/support/v7/b/e;->dPC:I

    aput v1, v0, v4

    sget v1, Landroid/support/v7/b/e;->dPu:I

    aput v1, v0, v5

    sget v1, Landroid/support/v7/b/e;->dPq:I

    aput v1, v0, v6

    sget v1, Landroid/support/v7/b/e;->dPr:I

    const/4 v2, 0x4

    aput v1, v0, v2

    sget v1, Landroid/support/v7/b/e;->dPt:I

    const/4 v2, 0x5

    aput v1, v0, v2

    sget v1, Landroid/support/v7/b/e;->dPs:I

    aput v1, v0, v7

    sput-object v0, Landroid/support/v7/widget/bF;->ejg:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    sget v1, Landroid/support/v7/b/e;->dPQ:I

    aput v1, v0, v3

    sget v1, Landroid/support/v7/b/e;->dPS:I

    aput v1, v0, v4

    sget v1, Landroid/support/v7/b/e;->dPl:I

    aput v1, v0, v5

    sget v1, Landroid/support/v7/b/e;->dPJ:I

    aput v1, v0, v6

    sget v1, Landroid/support/v7/b/e;->dPK:I

    const/4 v2, 0x4

    aput v1, v0, v2

    sget v1, Landroid/support/v7/b/e;->dPM:I

    const/4 v2, 0x5

    aput v1, v0, v2

    sget v1, Landroid/support/v7/b/e;->dPO:I

    aput v1, v0, v7

    sget v1, Landroid/support/v7/b/e;->dPL:I

    const/4 v2, 0x7

    aput v1, v0, v2

    sget v1, Landroid/support/v7/b/e;->dPN:I

    const/16 v2, 0x8

    aput v1, v0, v2

    sget v1, Landroid/support/v7/b/e;->dPP:I

    const/16 v2, 0x9

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/widget/bF;->eiV:[I

    new-array v0, v6, [I

    sget v1, Landroid/support/v7/b/e;->dPx:I

    aput v1, v0, v3

    sget v1, Landroid/support/v7/b/e;->dPj:I

    aput v1, v0, v4

    sget v1, Landroid/support/v7/b/e;->dPw:I

    aput v1, v0, v5

    sput-object v0, Landroid/support/v7/widget/bF;->ejb:[I

    new-array v0, v5, [I

    sget v1, Landroid/support/v7/b/e;->dPI:I

    aput v1, v0, v3

    sget v1, Landroid/support/v7/b/e;->dPU:I

    aput v1, v0, v4

    sput-object v0, Landroid/support/v7/widget/bF;->eiT:[I

    new-array v0, v5, [I

    sget v1, Landroid/support/v7/b/e;->dPf:I

    aput v1, v0, v3

    sget v1, Landroid/support/v7/b/e;->dPi:I

    aput v1, v0, v4

    sput-object v0, Landroid/support/v7/widget/bF;->ejd:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bF;->eji:Ljava/lang/Object;

    new-instance v0, Ljava/util/WeakHashMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/WeakHashMap;-><init>(I)V

    iput-object v0, p0, Landroid/support/v7/widget/bF;->ejh:Ljava/util/WeakHashMap;

    return-void
.end method

.method private dDA(Landroid/content/Context;ILandroid/content/res/ColorStateList;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/bF;->eja:Ljava/util/WeakHashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bF;->eja:Ljava/util/WeakHashMap;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bF;->eja:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/b;

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/a/b;

    invoke-direct {v0}, Landroid/support/v4/a/b;-><init>()V

    iget-object v1, p0, Landroid/support/v7/widget/bF;->eja:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0, p2, p3}, Landroid/support/v4/a/b;->dSn(ILjava/lang/Object;)V

    return-void
.end method

.method private dDB(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/bF;->ejf:Landroid/util/TypedValue;

    if-nez v0, :cond_0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bF;->ejf:Landroid/util/TypedValue;

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/bF;->ejf:Landroid/util/TypedValue;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2, v1, v7}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    invoke-static {v1}, Landroid/support/v7/widget/bF;->dDI(Landroid/util/TypedValue;)J

    move-result-wide v2

    invoke-direct {p0, p1, v2, v3}, Landroid/support/v7/widget/bF;->dDQ(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    sget v4, Landroid/support/v7/b/e;->dPk:I

    if-ne p2, v4, :cond_2

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    sget v5, Landroid/support/v7/b/e;->dPj:I

    invoke-virtual {p0, p1, v5}, Landroid/support/v7/widget/bF;->dDr(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    sget v5, Landroid/support/v7/b/e;->dPl:I

    invoke-virtual {p0, p1, v5}, Landroid/support/v7/widget/bF;->dDr(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-direct {v0, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    :cond_2
    if-eqz v0, :cond_3

    iget v1, v1, Landroid/util/TypedValue;->changingConfigurations:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    invoke-direct {p0, p1, v2, v3, v0}, Landroid/support/v7/widget/bF;->dDx(Landroid/content/Context;JLandroid/graphics/drawable/Drawable;)Z

    :cond_3
    return-object v0
.end method

.method private static dDC(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;[I)Landroid/graphics/PorterDuffColorFilter;
    .locals 1

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    invoke-static {v0, p1}, Landroid/support/v7/widget/bF;->dDH(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    return-object v0
.end method

.method private static dDD(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V
    .locals 1

    invoke-static {p0}, Landroid/support/v7/widget/cx;->dHw(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    :cond_0
    if-nez p2, :cond_1

    sget-object p2, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    :cond_1
    invoke-static {p1, p2}, Landroid/support/v7/widget/bF;->dDH(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    return-void
.end method

.method private dDE(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x3

    new-array v0, v1, [[I

    new-array v1, v1, [I

    sget v2, Landroid/support/v7/b/a;->dOu:I

    invoke-static {p1, v2}, Landroid/support/v7/widget/bH;->dDV(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Landroid/support/v7/widget/bH;->ejm:[I

    aput-object v3, v0, v4

    aget-object v3, v0, v4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    aput v3, v1, v4

    sget-object v3, Landroid/support/v7/widget/bH;->ejo:[I

    aput-object v3, v0, v5

    sget v3, Landroid/support/v7/b/a;->dOr:I

    invoke-static {p1, v3}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v3

    aput v3, v1, v5

    sget-object v3, Landroid/support/v7/widget/bH;->ejp:[I

    aput-object v3, v0, v6

    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    aput v2, v1, v6

    :goto_0
    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2

    :cond_0
    sget-object v2, Landroid/support/v7/widget/bH;->ejm:[I

    aput-object v2, v0, v4

    sget v2, Landroid/support/v7/b/a;->dOu:I

    invoke-static {p1, v2}, Landroid/support/v7/widget/bH;->dDU(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v4

    sget-object v2, Landroid/support/v7/widget/bH;->ejo:[I

    aput-object v2, v0, v5

    sget v2, Landroid/support/v7/b/a;->dOr:I

    invoke-static {p1, v2}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v5

    sget-object v2, Landroid/support/v7/widget/bH;->ejp:[I

    aput-object v2, v0, v6

    sget v2, Landroid/support/v7/b/a;->dOu:I

    invoke-static {p1, v2}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v2

    aput v2, v1, v6

    goto :goto_0
.end method

.method private dDF(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .locals 1

    sget v0, Landroid/support/v7/b/a;->dOp:I

    invoke-static {p1, v0}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/bF;->dDt(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private dDG(Landroid/content/Context;IZLandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 6

    const v5, 0x102000f

    const v4, 0x102000d

    const/high16 v2, 0x1020000

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/bF;->dDM(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {p4}, Landroid/support/v7/widget/cx;->dHw(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p4

    :cond_0
    invoke-static {p4}, Landroid/support/v4/c/a/g;->elO(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p4

    invoke-static {p4, v1}, Landroid/support/v4/c/a/g;->elY(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    invoke-static {p2}, Landroid/support/v7/widget/bF;->dDP(I)Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p4, v0}, Landroid/support/v4/c/a/g;->ema(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    :cond_1
    :goto_0
    return-object p4

    :cond_2
    sget v1, Landroid/support/v7/b/e;->dPD:I

    if-ne p2, v1, :cond_3

    move-object v0, p4

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget v2, Landroid/support/v7/b/a;->dOt:I

    invoke-static {p1, v2}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v1, v2, v3}, Landroid/support/v7/widget/bF;->dDD(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget v2, Landroid/support/v7/b/a;->dOt:I

    invoke-static {p1, v2}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v1, v2, v3}, Landroid/support/v7/widget/bF;->dDD(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget v1, Landroid/support/v7/b/a;->dOr:I

    invoke-static {p1, v1}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/bF;->dDD(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    :cond_3
    sget v1, Landroid/support/v7/b/e;->dPz:I

    if-eq p2, v1, :cond_4

    sget v1, Landroid/support/v7/b/e;->dPy:I

    if-ne p2, v1, :cond_5

    :cond_4
    move-object v0, p4

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget v2, Landroid/support/v7/b/a;->dOt:I

    invoke-static {p1, v2}, Landroid/support/v7/widget/bH;->dDU(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v1, v2, v3}, Landroid/support/v7/widget/bF;->dDD(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget v2, Landroid/support/v7/b/a;->dOr:I

    invoke-static {p1, v2}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v1, v2, v3}, Landroid/support/v7/widget/bF;->dDD(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget v1, Landroid/support/v7/b/a;->dOr:I

    invoke-static {p1, v1}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/bF;->dDD(Landroid/graphics/drawable/Drawable;ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    :cond_5
    sget v1, Landroid/support/v7/b/e;->dPA:I

    if-eq p2, v1, :cond_4

    invoke-static {p1, p2, p4}, Landroid/support/v7/widget/bF;->dDy(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p3, :cond_1

    move-object p4, v0

    goto :goto_0
.end method

.method public static dDH(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
    .locals 2

    sget-object v0, Landroid/support/v7/widget/bF;->eiZ:Landroid/support/v7/widget/aK;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/widget/aK;->dAR(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    invoke-direct {v0, p0, p1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    sget-object v1, Landroid/support/v7/widget/bF;->eiZ:Landroid/support/v7/widget/aK;

    invoke-virtual {v1, p0, p1, v0}, Landroid/support/v7/widget/aK;->dAQ(ILandroid/graphics/PorterDuff$Mode;Landroid/graphics/PorterDuffColorFilter;)Landroid/graphics/PorterDuffColorFilter;

    :cond_0
    return-object v0
.end method

.method private static dDI(Landroid/util/TypedValue;)J
    .locals 4

    iget v0, p0, Landroid/util/TypedValue;->assetCookie:I

    int-to-long v0, v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    iget v2, p0, Landroid/util/TypedValue;->data:I

    int-to-long v2, v2

    or-long/2addr v0, v2

    return-wide v0
.end method

.method static dDJ(Landroid/graphics/drawable/Drawable;Landroid/support/v7/widget/cw;[I)V
    .locals 2

    invoke-static {p0}, Landroid/support/v7/widget/cx;->dHw(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eq v0, p0, :cond_0

    const-string/jumbo v0, "AppCompatDrawableManag"

    const-string/jumbo v1, "Mutated drawable is not the same instance as the input."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-boolean v0, p1, Landroid/support/v7/widget/cw;->eoN:Z

    if-nez v0, :cond_1

    iget-boolean v0, p1, Landroid/support/v7/widget/cw;->eoP:Z

    if-eqz v0, :cond_5

    :cond_1
    iget-boolean v0, p1, Landroid/support/v7/widget/cw;->eoN:Z

    if-eqz v0, :cond_3

    iget-object v0, p1, Landroid/support/v7/widget/cw;->eoO:Landroid/content/res/ColorStateList;

    :goto_0
    iget-boolean v1, p1, Landroid/support/v7/widget/cw;->eoP:Z

    if-eqz v1, :cond_4

    iget-object v1, p1, Landroid/support/v7/widget/cw;->eoQ:Landroid/graphics/PorterDuff$Mode;

    :goto_1
    invoke-static {v0, v1, p2}, Landroid/support/v7/widget/bF;->dDC(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;[I)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-gt v0, v1, :cond_2

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    sget-object v1, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_2
.end method

.method private dDL(Landroid/content/Context;)V
    .locals 2

    iget-boolean v0, p0, Landroid/support/v7/widget/bF;->eiX:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/bF;->eiX:Z

    sget v0, Landroid/support/v7/b/e;->dPV:I

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/bF;->dDr(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/support/v7/widget/bF;->dDR(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/bF;->eiX:Z

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method

.method private dDO(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/bF;->eja:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bF;->eja:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/support/v4/a/b;->dSp(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/ColorStateList;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method static dDP(I)Landroid/graphics/PorterDuff$Mode;
    .locals 2

    const/4 v0, 0x0

    sget v1, Landroid/support/v7/b/e;->dPG:I

    if-ne p0, v1, :cond_0

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    :cond_0
    return-object v0
.end method

.method private dDQ(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;
    .locals 4

    const/4 v3, 0x0

    iget-object v2, p0, Landroid/support/v7/widget/bF;->eji:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Landroid/support/v7/widget/bF;->ejh:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit v2

    return-object v3

    :cond_0
    :try_start_1
    invoke-virtual {v0, p2, p3}, Landroid/support/v4/a/n;->dTl(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable$ConstantState;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit v2

    return-object v0

    :cond_1
    :try_start_2
    invoke-virtual {v0, p2, p3}, Landroid/support/v4/a/n;->dTg(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    monitor-exit v2

    return-object v3

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static dDR(Landroid/graphics/drawable/Drawable;)Z
    .locals 2

    instance-of v0, p0, Landroid/support/f/a/q;

    if-nez v0, :cond_0

    const-string/jumbo v0, "android.graphics.drawable.VectorDrawable"

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private dDS(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/bF;->ejc:Landroid/support/v4/a/u;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/bF;->ejc:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/bF;->eiY:Landroid/support/v4/a/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bF;->eiY:Landroid/support/v4/a/b;

    invoke-virtual {v0, p2}, Landroid/support/v4/a/b;->dSp(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "appcompat_skip_skip"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/bF;->ejc:Landroid/support/v4/a/u;

    invoke-virtual {v1, v0}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    return-object v2

    :cond_1
    new-instance v0, Landroid/support/v4/a/b;

    invoke-direct {v0}, Landroid/support/v4/a/b;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bF;->eiY:Landroid/support/v4/a/b;

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/bF;->ejf:Landroid/util/TypedValue;

    if-nez v0, :cond_3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bF;->ejf:Landroid/util/TypedValue;

    :cond_3
    iget-object v2, p0, Landroid/support/v7/widget/bF;->ejf:Landroid/util/TypedValue;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2, v2, v7}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    invoke-static {v2}, Landroid/support/v7/widget/bF;->dDI(Landroid/util/TypedValue;)J

    move-result-wide v4

    invoke-direct {p0, p1, v4, v5}, Landroid/support/v7/widget/bF;->dDQ(Landroid/content/Context;J)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_4

    return-object v1

    :cond_4
    iget-object v3, v2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    if-eqz v3, :cond_7

    iget-object v3, v2, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, ".xml"

    invoke-virtual {v3, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    :try_start_0
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v3

    invoke-static {v3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v6

    :cond_5
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    if-eq v0, v8, :cond_6

    if-ne v0, v7, :cond_5

    :cond_6
    if-eq v0, v8, :cond_9

    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string/jumbo v2, "No start tag found"

    invoke-direct {v0, v2}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "AppCompatDrawableManag"

    const-string/jumbo v3, "Exception while inflating drawable"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_7
    :goto_0
    if-nez v1, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/bF;->eiY:Landroid/support/v4/a/b;

    const-string/jumbo v2, "appcompat_skip_skip"

    invoke-virtual {v0, p2, v2}, Landroid/support/v4/a/b;->dSn(ILjava/lang/Object;)V

    :cond_8
    return-object v1

    :cond_9
    :try_start_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Landroid/support/v7/widget/bF;->eiY:Landroid/support/v4/a/b;

    invoke-virtual {v7, p2, v0}, Landroid/support/v4/a/b;->dSn(ILjava/lang/Object;)V

    iget-object v7, p0, Landroid/support/v7/widget/bF;->ejc:Landroid/support/v4/a/u;

    invoke-virtual {v7, v0}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ch;

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    invoke-interface {v0, p1, v3, v6, v7}, Landroid/support/v7/widget/ch;->dGT(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_a
    if-eqz v1, :cond_7

    iget v0, v2, Landroid/util/TypedValue;->changingConfigurations:I

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    invoke-direct {p0, p1, v4, v5, v1}, Landroid/support/v7/widget/bF;->dDx(Landroid/content/Context;JLandroid/graphics/drawable/Drawable;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_0

    :cond_b
    return-object v2
.end method

.method private dDs(Ljava/lang/String;Landroid/support/v7/widget/ch;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/bF;->ejc:Landroid/support/v4/a/u;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/a/u;

    invoke-direct {v0}, Landroid/support/v4/a/u;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bF;->ejc:Landroid/support/v4/a/u;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/bF;->ejc:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private dDt(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 6

    const/4 v1, 0x4

    new-array v0, v1, [[I

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Landroid/support/v7/b/a;->dOs:I

    invoke-static {p1, v3}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v3

    sget v4, Landroid/support/v7/b/a;->dOq:I

    invoke-static {p1, v4}, Landroid/support/v7/widget/bH;->dDU(Landroid/content/Context;I)I

    move-result v4

    sget-object v5, Landroid/support/v7/widget/bH;->ejm:[I

    aput-object v5, v0, v2

    aput v4, v1, v2

    const/4 v2, 0x1

    sget-object v4, Landroid/support/v7/widget/bH;->ejl:[I

    aput-object v4, v0, v2

    invoke-static {v3, p2}, Landroid/support/v4/c/c;->emm(II)I

    move-result v4

    aput v4, v1, v2

    const/4 v2, 0x2

    sget-object v4, Landroid/support/v7/widget/bH;->ejs:[I

    aput-object v4, v0, v2

    invoke-static {v3, p2}, Landroid/support/v4/c/c;->emm(II)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Landroid/support/v7/widget/bH;->ejp:[I

    aput-object v3, v0, v2

    aput p2, v1, v2

    new-instance v2, Landroid/content/res/ColorStateList;

    invoke-direct {v2, v0, v1}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v2
.end method

.method private static dDu(Landroid/support/v7/widget/bF;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_0

    const-string/jumbo v0, "vector"

    new-instance v1, Landroid/support/v7/widget/cs;

    invoke-direct {v1}, Landroid/support/v7/widget/cs;-><init>()V

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/bF;->dDs(Ljava/lang/String;Landroid/support/v7/widget/ch;)V

    const-string/jumbo v0, "animated-vector"

    new-instance v1, Landroid/support/v7/widget/cr;

    invoke-direct {v1}, Landroid/support/v7/widget/cr;-><init>()V

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/bF;->dDs(Ljava/lang/String;Landroid/support/v7/widget/ch;)V

    :cond_0
    return-void
.end method

.method private dDv(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .locals 1

    sget v0, Landroid/support/v7/b/a;->dOq:I

    invoke-static {p1, v0}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/bF;->dDt(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private static dDw([II)Z
    .locals 4

    const/4 v1, 0x0

    array-length v2, p0

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, p0, v0

    if-ne v3, p1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private dDx(Landroid/content/Context;JLandroid/graphics/drawable/Drawable;)Z
    .locals 4

    invoke-virtual {p4}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Landroid/support/v7/widget/bF;->eji:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Landroid/support/v7/widget/bF;->ejh:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/a/n;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/a/n;

    invoke-direct {v0}, Landroid/support/v4/a/n;-><init>()V

    iget-object v3, p0, Landroid/support/v7/widget/bF;->ejh:Ljava/util/WeakHashMap;

    invoke-virtual {v3, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p2, p3, v3}, Landroid/support/v4/a/n;->dTh(JLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method static dDy(Landroid/content/Context;ILandroid/graphics/drawable/Drawable;)Z
    .locals 7

    const v0, 0x1010031

    const/4 v1, -0x1

    const/4 v3, 0x0

    const/4 v5, 0x1

    sget-object v4, Landroid/support/v7/widget/bF;->eiU:Landroid/graphics/PorterDuff$Mode;

    sget-object v2, Landroid/support/v7/widget/bF;->eiW:[I

    invoke-static {v2, p1}, Landroid/support/v7/widget/bF;->dDw([II)Z

    move-result v2

    if-eqz v2, :cond_2

    sget v0, Landroid/support/v7/b/a;->dOt:I

    move v2, v0

    move-object v6, v4

    move v4, v5

    move v0, v1

    :goto_0
    if-eqz v4, :cond_6

    invoke-static {p2}, Landroid/support/v7/widget/cx;->dHw(Landroid/graphics/drawable/Drawable;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    :cond_0
    invoke-static {p0, v2}, Landroid/support/v7/widget/bH;->dDW(Landroid/content/Context;I)I

    move-result v2

    invoke-static {v2, v6}, Landroid/support/v7/widget/bF;->dDH(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    if-eq v0, v1, :cond_1

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_1
    return v5

    :cond_2
    sget-object v2, Landroid/support/v7/widget/bF;->eiV:[I

    invoke-static {v2, p1}, Landroid/support/v7/widget/bF;->dDw([II)Z

    move-result v2

    if-eqz v2, :cond_3

    sget v0, Landroid/support/v7/b/a;->dOr:I

    move v2, v0

    move-object v6, v4

    move v4, v5

    move v0, v1

    goto :goto_0

    :cond_3
    sget-object v2, Landroid/support/v7/widget/bF;->ejb:[I

    invoke-static {v2, p1}, Landroid/support/v7/widget/bF;->dDw([II)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    move v4, v5

    move-object v6, v2

    move v2, v0

    move v0, v1

    goto :goto_0

    :cond_4
    sget v2, Landroid/support/v7/b/e;->dPv:I

    if-ne p1, v2, :cond_5

    const v2, 0x1010030

    const v0, 0x42233333    # 40.8f

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move-object v6, v4

    move v4, v5

    goto :goto_0

    :cond_5
    sget v2, Landroid/support/v7/b/e;->dPm:I

    if-ne p1, v2, :cond_7

    move v2, v0

    move-object v6, v4

    move v4, v5

    move v0, v1

    goto :goto_0

    :cond_6
    return v3

    :cond_7
    move v0, v1

    move v2, v3

    move-object v6, v4

    move v4, v3

    goto :goto_0
.end method

.method private dDz(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/bF;->dDt(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public static get()Landroid/support/v7/widget/bF;
    .locals 1

    sget-object v0, Landroid/support/v7/widget/bF;->eje:Landroid/support/v7/widget/bF;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/bF;

    invoke-direct {v0}, Landroid/support/v7/widget/bF;-><init>()V

    sput-object v0, Landroid/support/v7/widget/bF;->eje:Landroid/support/v7/widget/bF;

    sget-object v0, Landroid/support/v7/widget/bF;->eje:Landroid/support/v7/widget/bF;

    invoke-static {v0}, Landroid/support/v7/widget/bF;->dDu(Landroid/support/v7/widget/bF;)V

    :cond_0
    sget-object v0, Landroid/support/v7/widget/bF;->eje:Landroid/support/v7/widget/bF;

    return-object v0
.end method


# virtual methods
.method dDK(Landroid/content/Context;Landroid/support/v7/widget/ct;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p3}, Landroid/support/v7/widget/bF;->dDS(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p2, p3}, Landroid/support/v7/widget/ct;->dHq(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p3, v1, v0}, Landroid/support/v7/widget/bF;->dDG(Landroid/content/Context;IZLandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_1
    return-object v1
.end method

.method dDM(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/bF;->dDO(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-nez v0, :cond_1

    sget v1, Landroid/support/v7/b/e;->dPn:I

    if-ne p2, v1, :cond_2

    sget v0, Landroid/support/v7/b/c;->dON:I

    invoke-static {p1, v0}, Landroid/support/v7/f/a/d;->dMG(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/bF;->dDA(Landroid/content/Context;ILandroid/content/res/ColorStateList;)V

    :cond_1
    return-object v0

    :cond_2
    sget v1, Landroid/support/v7/b/e;->dPH:I

    if-ne p2, v1, :cond_3

    sget v0, Landroid/support/v7/b/c;->dOQ:I

    invoke-static {p1, v0}, Landroid/support/v7/f/a/d;->dMG(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget v1, Landroid/support/v7/b/e;->dPG:I

    if-ne p2, v1, :cond_4

    invoke-direct {p0, p1}, Landroid/support/v7/widget/bF;->dDE(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_4
    sget v1, Landroid/support/v7/b/e;->dPh:I

    if-ne p2, v1, :cond_5

    invoke-direct {p0, p1}, Landroid/support/v7/widget/bF;->dDv(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_5
    sget v1, Landroid/support/v7/b/e;->dPe:I

    if-ne p2, v1, :cond_6

    invoke-direct {p0, p1}, Landroid/support/v7/widget/bF;->dDz(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_6
    sget v1, Landroid/support/v7/b/e;->dPg:I

    if-ne p2, v1, :cond_7

    invoke-direct {p0, p1}, Landroid/support/v7/widget/bF;->dDF(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_7
    sget v1, Landroid/support/v7/b/e;->dPE:I

    if-eq p2, v1, :cond_8

    sget v1, Landroid/support/v7/b/e;->dPF:I

    if-ne p2, v1, :cond_9

    :cond_8
    sget v0, Landroid/support/v7/b/c;->dOP:I

    invoke-static {p1, v0}, Landroid/support/v7/f/a/d;->dMG(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_9
    sget-object v1, Landroid/support/v7/widget/bF;->ejg:[I

    invoke-static {v1, p2}, Landroid/support/v7/widget/bF;->dDw([II)Z

    move-result v1

    if-eqz v1, :cond_a

    sget v0, Landroid/support/v7/b/a;->dOt:I

    invoke-static {p1, v0}, Landroid/support/v7/widget/bH;->dDV(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_a
    sget-object v1, Landroid/support/v7/widget/bF;->eiT:[I

    invoke-static {v1, p2}, Landroid/support/v7/widget/bF;->dDw([II)Z

    move-result v1

    if-eqz v1, :cond_b

    sget v0, Landroid/support/v7/b/c;->dOM:I

    invoke-static {p1, v0}, Landroid/support/v7/f/a/d;->dMG(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_b
    sget-object v1, Landroid/support/v7/widget/bF;->ejd:[I

    invoke-static {v1, p2}, Landroid/support/v7/widget/bF;->dDw([II)Z

    move-result v1

    if-eqz v1, :cond_c

    sget v0, Landroid/support/v7/b/c;->dOL:I

    invoke-static {p1, v0}, Landroid/support/v7/f/a/d;->dMG(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_c
    sget v1, Landroid/support/v7/b/e;->dPB:I

    if-ne p2, v1, :cond_0

    sget v0, Landroid/support/v7/b/c;->dOO:I

    invoke-static {p1, v0}, Landroid/support/v7/f/a/d;->dMG(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0
.end method

.method dDN(Landroid/content/Context;IZ)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/bF;->dDL(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/bF;->dDS(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/bF;->dDB(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-static {p1, p2}, Landroid/support/v4/content/a;->eab(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/widget/bF;->dDG(Landroid/content/Context;IZLandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_3

    invoke-static {v0}, Landroid/support/v7/widget/cx;->dHu(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    return-object v0
.end method

.method public dDr(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v7/widget/bF;->dDN(Landroid/content/Context;IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
