.class Landroid/support/v7/widget/Y;
.super Ljava/lang/Object;
.source "AdapterHelper.java"


# instance fields
.field ebs:I

.field ebt:Ljava/lang/Object;

.field ebu:I

.field ebv:I


# direct methods
.method constructor <init>(IIILjava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/support/v7/widget/Y;->ebs:I

    iput p2, p0, Landroid/support/v7/widget/Y;->ebu:I

    iput p3, p0, Landroid/support/v7/widget/Y;->ebv:I

    iput-object p4, p0, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method dxn()Ljava/lang/String;
    .locals 1

    iget v0, p0, Landroid/support/v7/widget/Y;->ebs:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string/jumbo v0, "??"

    return-object v0

    :pswitch_1
    const-string/jumbo v0, "add"

    return-object v0

    :pswitch_2
    const-string/jumbo v0, "rm"

    return-object v0

    :pswitch_3
    const-string/jumbo v0, "up"

    return-object v0

    :pswitch_4
    const-string/jumbo v0, "mv"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_0

    return v3

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/widget/Y;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    return v2

    :cond_2
    check-cast p1, Landroid/support/v7/widget/Y;

    iget v0, p0, Landroid/support/v7/widget/Y;->ebs:I

    iget v1, p1, Landroid/support/v7/widget/Y;->ebs:I

    if-eq v0, v1, :cond_3

    return v2

    :cond_3
    iget v0, p0, Landroid/support/v7/widget/Y;->ebs:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    iget v0, p0, Landroid/support/v7/widget/Y;->ebv:I

    iget v1, p0, Landroid/support/v7/widget/Y;->ebu:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ne v0, v3, :cond_4

    iget v0, p0, Landroid/support/v7/widget/Y;->ebv:I

    iget v1, p1, Landroid/support/v7/widget/Y;->ebu:I

    if-ne v0, v1, :cond_4

    iget v0, p0, Landroid/support/v7/widget/Y;->ebu:I

    iget v1, p1, Landroid/support/v7/widget/Y;->ebv:I

    if-ne v0, v1, :cond_4

    return v3

    :cond_4
    iget v0, p0, Landroid/support/v7/widget/Y;->ebv:I

    iget v1, p1, Landroid/support/v7/widget/Y;->ebv:I

    if-eq v0, v1, :cond_5

    return v2

    :cond_5
    iget v0, p0, Landroid/support/v7/widget/Y;->ebu:I

    iget v1, p1, Landroid/support/v7/widget/Y;->ebu:I

    if-eq v0, v1, :cond_6

    return v2

    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    return v2

    :cond_7
    iget-object v0, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    if-eqz v0, :cond_8

    return v2

    :cond_8
    return v3
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Landroid/support/v7/widget/Y;->ebs:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Landroid/support/v7/widget/Y;->ebu:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v7/widget/Y;->dxn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/Y;->ebu:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "c:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/Y;->ebv:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",p:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
