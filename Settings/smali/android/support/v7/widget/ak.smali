.class final Landroid/support/v7/widget/ak;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DefaultItemAnimator.java"


# instance fields
.field final synthetic ecs:Landroid/support/v7/widget/ac;

.field final synthetic ect:Landroid/support/v7/widget/Z;

.field final synthetic ecu:Landroid/view/ViewPropertyAnimator;

.field final synthetic ecv:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/Z;Landroid/support/v7/widget/ac;Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/ak;->ect:Landroid/support/v7/widget/Z;

    iput-object p2, p0, Landroid/support/v7/widget/ak;->ecs:Landroid/support/v7/widget/ac;

    iput-object p3, p0, Landroid/support/v7/widget/ak;->ecu:Landroid/view/ViewPropertyAnimator;

    iput-object p4, p0, Landroid/support/v7/widget/ak;->ecv:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/ak;->ecu:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Landroid/support/v7/widget/ak;->ecv:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->ecv:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->ecv:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->ect:Landroid/support/v7/widget/Z;

    iget-object v1, p0, Landroid/support/v7/widget/ak;->ecs:Landroid/support/v7/widget/ac;

    iget-object v1, v1, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/Z;->dxB(Landroid/support/v7/widget/p;Z)V

    iget-object v0, p0, Landroid/support/v7/widget/ak;->ect:Landroid/support/v7/widget/Z;

    iget-object v0, v0, Landroid/support/v7/widget/Z;->ebF:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/ak;->ecs:Landroid/support/v7/widget/ac;

    iget-object v1, v1, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/ak;->ect:Landroid/support/v7/widget/Z;

    invoke-virtual {v0}, Landroid/support/v7/widget/Z;->dxw()V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/ak;->ect:Landroid/support/v7/widget/Z;

    iget-object v1, p0, Landroid/support/v7/widget/ak;->ecs:Landroid/support/v7/widget/ac;

    iget-object v1, v1, Landroid/support/v7/widget/ac;->ebQ:Landroid/support/v7/widget/p;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/Z;->dxH(Landroid/support/v7/widget/p;Z)V

    return-void
.end method
