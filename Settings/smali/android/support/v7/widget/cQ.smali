.class public Landroid/support/v7/widget/cQ;
.super Landroid/support/v4/view/d;
.source "RecyclerViewAccessibilityDelegate.java"


# instance fields
.field final eqE:Landroid/support/v7/widget/F;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/F;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/view/d;-><init>()V

    iput-object p1, p0, Landroid/support/v7/widget/cQ;->eqE:Landroid/support/v7/widget/F;

    return-void
.end method


# virtual methods
.method public ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/support/v4/view/d;->ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V

    iget-object v0, p0, Landroid/support/v7/widget/cQ;->eqE:Landroid/support/v7/widget/F;

    invoke-virtual {v0}, Landroid/support/v7/widget/F;->shouldIgnore()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cQ;->eqE:Landroid/support/v7/widget/F;

    iget-object v0, v0, Landroid/support/v7/widget/F;->eaH:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cQ;->eqE:Landroid/support/v7/widget/F;

    iget-object v0, v0, Landroid/support/v7/widget/F;->eaH:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/a;->drP(Landroid/view/View;Landroid/support/v4/view/a/a;)V

    :cond_0
    return-void
.end method

.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/d;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/cQ;->eqE:Landroid/support/v7/widget/F;

    invoke-virtual {v0}, Landroid/support/v7/widget/F;->shouldIgnore()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/cQ;->eqE:Landroid/support/v7/widget/F;

    iget-object v0, v0, Landroid/support/v7/widget/F;->eaH:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/a;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/cQ;->eqE:Landroid/support/v7/widget/F;

    iget-object v0, v0, Landroid/support/v7/widget/F;->eaH:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/a;->drN(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
