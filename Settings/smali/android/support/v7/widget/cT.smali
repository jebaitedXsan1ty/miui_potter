.class Landroid/support/v7/widget/cT;
.super Ljava/lang/Object;
.source "ChildHelper.java"


# instance fields
.field eqK:J

.field eqL:Landroid/support/v7/widget/cT;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    return-void
.end method

.method private dIE()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/cT;

    invoke-direct {v0}, Landroid/support/v7/widget/cT;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    :cond_0
    return-void
.end method


# virtual methods
.method dIC(I)V
    .locals 4

    const/16 v0, 0x40

    if-lt p1, v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cT;->dIC(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    const-wide/16 v2, 0x1

    shl-long/2addr v2, p1

    not-long v2, v2

    and-long/2addr v0, v2

    iput-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    goto :goto_0
.end method

.method dID(IZ)V
    .locals 10

    const-wide/16 v8, 0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v0, 0x40

    if-lt p1, v0, :cond_1

    invoke-direct {p0}, Landroid/support/v7/widget/cT;->dIE()V

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1, p2}, Landroid/support/v7/widget/cT;->dID(IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v4, p0, Landroid/support/v7/widget/cT;->eqK:J

    const-wide/high16 v6, -0x8000000000000000L

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    shl-long v4, v8, p1

    sub-long/2addr v4, v8

    iget-wide v6, p0, Landroid/support/v7/widget/cT;->eqK:J

    and-long/2addr v6, v4

    iget-wide v8, p0, Landroid/support/v7/widget/cT;->eqK:J

    not-long v4, v4

    and-long/2addr v4, v8

    shl-long/2addr v4, v1

    or-long/2addr v4, v6

    iput-wide v4, p0, Landroid/support/v7/widget/cT;->eqK:J

    if-eqz p2, :cond_4

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/cT;->dII(I)V

    :goto_2
    if-nez v0, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    if-eqz v1, :cond_0

    :cond_2
    invoke-direct {p0}, Landroid/support/v7/widget/cT;->dIE()V

    iget-object v1, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/widget/cT;->dID(IZ)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/cT;->dIC(I)V

    goto :goto_2
.end method

.method dIF(I)Z
    .locals 4

    const/16 v0, 0x40

    if-lt p1, v0, :cond_0

    invoke-direct {p0}, Landroid/support/v7/widget/cT;->dIE()V

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cT;->dIF(I)Z

    move-result v0

    return v0

    :cond_0
    iget-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    const-wide/16 v2, 0x1

    shl-long/2addr v2, p1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method dIG(I)I
    .locals 6

    const/16 v1, 0x40

    const-wide/16 v4, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    if-nez v0, :cond_1

    if-lt p1, v1, :cond_0

    iget-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    return v0

    :cond_0
    iget-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    shl-long v2, v4, p1

    sub-long/2addr v2, v4

    and-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    return v0

    :cond_1
    if-ge p1, v1, :cond_2

    iget-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    shl-long v2, v4, p1

    sub-long/2addr v2, v4

    and-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    return v0

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cT;->dIG(I)I

    move-result v0

    iget-wide v2, p0, Landroid/support/v7/widget/cT;->eqK:J

    invoke-static {v2, v3}, Ljava/lang/Long;->bitCount(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method dIH()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    invoke-virtual {v0}, Landroid/support/v7/widget/cT;->dIH()V

    :cond_0
    return-void
.end method

.method dII(I)V
    .locals 4

    const/16 v0, 0x40

    if-lt p1, v0, :cond_0

    invoke-direct {p0}, Landroid/support/v7/widget/cT;->dIE()V

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cT;->dII(I)V

    :goto_0
    return-void

    :cond_0
    iget-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    const-wide/16 v2, 0x1

    shl-long/2addr v2, p1

    or-long/2addr v0, v2

    iput-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    goto :goto_0
.end method

.method remove(I)Z
    .locals 12

    const-wide/16 v10, 0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v0, 0x40

    if-lt p1, v0, :cond_0

    invoke-direct {p0}, Landroid/support/v7/widget/cT;->dIE()V

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    add-int/lit8 v1, p1, -0x40

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cT;->remove(I)Z

    move-result v0

    return v0

    :cond_0
    shl-long v4, v10, p1

    iget-wide v6, p0, Landroid/support/v7/widget/cT;->eqK:J

    and-long/2addr v6, v4

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iget-wide v6, p0, Landroid/support/v7/widget/cT;->eqK:J

    not-long v8, v4

    and-long/2addr v6, v8

    iput-wide v6, p0, Landroid/support/v7/widget/cT;->eqK:J

    sub-long/2addr v4, v10

    iget-wide v6, p0, Landroid/support/v7/widget/cT;->eqK:J

    and-long/2addr v6, v4

    iget-wide v8, p0, Landroid/support/v7/widget/cT;->eqK:J

    not-long v4, v4

    and-long/2addr v4, v8

    invoke-static {v4, v5, v1}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    or-long/2addr v4, v6

    iput-wide v4, p0, Landroid/support/v7/widget/cT;->eqK:J

    iget-object v1, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/cT;->dIF(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x3f

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/cT;->dII(I)V

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/cT;->remove(I)Z

    :cond_2
    return v0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    if-nez v0, :cond_0

    iget-wide v0, p0, Landroid/support/v7/widget/cT;->eqK:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid/support/v7/widget/cT;->eqL:Landroid/support/v7/widget/cT;

    invoke-virtual {v1}, Landroid/support/v7/widget/cT;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "xx"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Landroid/support/v7/widget/cT;->eqK:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
