.class Landroid/support/v7/widget/bG;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic ejj:Landroid/support/v7/widget/ListPopupWindow;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/ListPopupWindow;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/bG;->ejj:Landroid/support/v7/widget/ListPopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/bG;->ejj:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bG;->ejj:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPO(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bG;->ejj:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {v0}, Landroid/support/v7/widget/bV;->getCount()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/bG;->ejj:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v1, v1, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {v1}, Landroid/support/v7/widget/bV;->getChildCount()I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bG;->ejj:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->efl:Landroid/support/v7/widget/bV;

    invoke-virtual {v0}, Landroid/support/v7/widget/bV;->getChildCount()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/bG;->ejj:Landroid/support/v7/widget/ListPopupWindow;

    iget v1, v1, Landroid/support/v7/widget/ListPopupWindow;->efj:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/bG;->ejj:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v0, v0, Landroid/support/v7/widget/ListPopupWindow;->efB:Landroid/widget/PopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    iget-object v0, p0, Landroid/support/v7/widget/bG;->ejj:Landroid/support/v7/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/support/v7/widget/ListPopupWindow;->show()V

    :cond_0
    return-void
.end method
