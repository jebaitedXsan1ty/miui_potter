.class Landroid/support/v7/widget/aV;
.super Ljava/lang/Object;
.source "ActionMenuView.java"

# interfaces
.implements Landroid/support/v7/view/menu/H;


# instance fields
.field final synthetic egH:Landroid/support/v7/widget/ActionMenuView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/ActionMenuView;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/aV;->egH:Landroid/support/v7/widget/ActionMenuView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dCd(Landroid/support/v7/view/menu/p;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/aV;->egH:Landroid/support/v7/widget/ActionMenuView;

    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuView;->elV:Landroid/support/v7/view/menu/H;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aV;->egH:Landroid/support/v7/widget/ActionMenuView;

    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuView;->elV:Landroid/support/v7/view/menu/H;

    invoke-interface {v0, p1}, Landroid/support/v7/view/menu/H;->dCd(Landroid/support/v7/view/menu/p;)V

    :cond_0
    return-void
.end method

.method public dCe(Landroid/support/v7/view/menu/p;Landroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/aV;->egH:Landroid/support/v7/widget/ActionMenuView;

    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuView;->elT:Landroid/support/v7/widget/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/aV;->egH:Landroid/support/v7/widget/ActionMenuView;

    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuView;->elT:Landroid/support/v7/widget/bw;

    invoke-interface {v0, p2}, Landroid/support/v7/widget/bw;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
