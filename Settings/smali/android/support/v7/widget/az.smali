.class public Landroid/support/v7/widget/az;
.super Ljava/lang/Object;
.source "TooltipCompat.java"


# static fields
.field private static final een:Landroid/support/v7/widget/bT;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v7/widget/ci;

    invoke-direct {v0, v2}, Landroid/support/v7/widget/ci;-><init>(Landroid/support/v7/widget/ci;)V

    sput-object v0, Landroid/support/v7/widget/az;->een:Landroid/support/v7/widget/bT;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/support/v7/widget/aO;

    invoke-direct {v0, v2}, Landroid/support/v7/widget/aO;-><init>(Landroid/support/v7/widget/aO;)V

    sput-object v0, Landroid/support/v7/widget/az;->een:Landroid/support/v7/widget/bT;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dzY(Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 1

    sget-object v0, Landroid/support/v7/widget/az;->een:Landroid/support/v7/widget/bT;

    invoke-interface {v0, p0, p1}, Landroid/support/v7/widget/bT;->dBt(Landroid/view/View;Ljava/lang/CharSequence;)V

    return-void
.end method
