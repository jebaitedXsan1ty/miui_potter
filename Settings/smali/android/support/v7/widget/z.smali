.class final Landroid/support/v7/widget/z;
.super Ljava/lang/Object;
.source "RecyclerView.java"

# interfaces
.implements Landroid/support/v7/widget/C;


# instance fields
.field final synthetic eaz:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dvi(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/j;->dud(Landroid/support/v7/widget/p;)V

    iget-object v0, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->doO(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V

    return-void
.end method

.method public dvj(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->dpe(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V

    return-void
.end method

.method public dvk(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/p;->setIsRecyclable(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    iget-boolean v0, v0, Landroid/support/v7/widget/RecyclerView;->dYz:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/support/v7/widget/u;->dvg(Landroid/support/v7/widget/p;Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->dqD()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->dYY:Landroid/support/v7/widget/u;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/u;->duS(Landroid/support/v7/widget/p;Landroid/support/v7/widget/K;Landroid/support/v7/widget/K;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->dqD()V

    goto :goto_0
.end method

.method public dvl(Landroid/support/v7/widget/p;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->mLayout:Landroid/support/v7/widget/a;

    iget-object v1, p1, Landroid/support/v7/widget/p;->itemView:Landroid/view/View;

    iget-object v2, p0, Landroid/support/v7/widget/z;->eaz:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->dYH:Landroid/support/v7/widget/j;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/a;->dsC(Landroid/view/View;Landroid/support/v7/widget/j;)V

    return-void
.end method
