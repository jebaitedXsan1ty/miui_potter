.class Landroid/support/v7/widget/I;
.super Ljava/lang/Object;
.source "AdapterHelper.java"

# interfaces
.implements Landroid/support/v7/widget/cM;


# instance fields
.field final eaL:Ljava/util/ArrayList;

.field final eaM:Ljava/util/ArrayList;

.field final eaN:Landroid/support/v7/widget/X;

.field private eaO:I

.field final eaP:Z

.field private eaQ:Landroid/support/v4/a/j;

.field eaR:Ljava/lang/Runnable;

.field final eaS:Landroid/support/v7/widget/dc;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/X;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/I;-><init>(Landroid/support/v7/widget/X;Z)V

    return-void
.end method

.method constructor <init>(Landroid/support/v7/widget/X;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v4/a/d;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Landroid/support/v4/a/d;-><init>(I)V

    iput-object v0, p0, Landroid/support/v7/widget/I;->eaQ:Landroid/support/v4/a/j;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/I;->eaO:I

    iput-object p1, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iput-boolean p2, p0, Landroid/support/v7/widget/I;->eaP:Z

    new-instance v0, Landroid/support/v7/widget/dc;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/dc;-><init>(Landroid/support/v7/widget/cM;)V

    iput-object v0, p0, Landroid/support/v7/widget/I;->eaS:Landroid/support/v7/widget/dc;

    return-void
.end method

.method private dwA(Landroid/support/v7/widget/Y;)V
    .locals 10

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p1, Landroid/support/v7/widget/Y;->ebs:I

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/support/v7/widget/Y;->ebs:I

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "should not dispatch add or move for pre layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v3, p1, Landroid/support/v7/widget/Y;->ebs:I

    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/I;->dww(II)I

    move-result v4

    iget v3, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v0, p1, Landroid/support/v7/widget/Y;->ebs:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "op should be remove or update."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    move v0, v1

    :goto_0
    move v5, v1

    move v6, v4

    move v4, v3

    move v3, v1

    :goto_1
    iget v7, p1, Landroid/support/v7/widget/Y;->ebv:I

    if-ge v3, v7, :cond_6

    iget v7, p1, Landroid/support/v7/widget/Y;->ebu:I

    mul-int v8, v0, v3

    add-int/2addr v7, v8

    iget v8, p1, Landroid/support/v7/widget/Y;->ebs:I

    invoke-direct {p0, v7, v8}, Landroid/support/v7/widget/I;->dww(II)I

    move-result v8

    iget v7, p1, Landroid/support/v7/widget/Y;->ebs:I

    packed-switch v7, :pswitch_data_1

    :pswitch_2
    move v7, v2

    :goto_2
    if-eqz v7, :cond_4

    add-int/lit8 v5, v5, 0x1

    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :pswitch_3
    move v0, v2

    goto :goto_0

    :pswitch_4
    add-int/lit8 v7, v6, 0x1

    if-ne v8, v7, :cond_2

    move v7, v1

    goto :goto_2

    :cond_2
    move v7, v2

    goto :goto_2

    :pswitch_5
    if-ne v8, v6, :cond_3

    move v7, v1

    goto :goto_2

    :cond_3
    move v7, v2

    goto :goto_2

    :cond_4
    iget v7, p1, Landroid/support/v7/widget/Y;->ebs:I

    iget-object v9, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-virtual {p0, v7, v6, v5, v9}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v6

    invoke-virtual {p0, v6, v4}, Landroid/support/v7/widget/I;->dwD(Landroid/support/v7/widget/Y;I)V

    invoke-virtual {p0, v6}, Landroid/support/v7/widget/I;->dwv(Landroid/support/v7/widget/Y;)V

    iget v6, p1, Landroid/support/v7/widget/Y;->ebs:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_5

    add-int/2addr v4, v5

    :cond_5
    move v5, v1

    move v6, v8

    goto :goto_3

    :cond_6
    iget-object v0, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/I;->dwv(Landroid/support/v7/widget/Y;)V

    if-lez v5, :cond_7

    iget v1, p1, Landroid/support/v7/widget/Y;->ebs:I

    invoke-virtual {p0, v1, v6, v5, v0}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Landroid/support/v7/widget/I;->dwD(Landroid/support/v7/widget/Y;I)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/I;->dwv(Landroid/support/v7/widget/Y;)V

    :cond_7
    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_5
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private dwE(Landroid/support/v7/widget/Y;)V
    .locals 11

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v7, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v0, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v3, p1, Landroid/support/v7/widget/Y;->ebv:I

    add-int v4, v0, v3

    const/4 v5, -0x1

    iget v3, p1, Landroid/support/v7/widget/Y;->ebu:I

    move v6, v2

    :goto_0
    if-ge v3, v4, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    invoke-interface {v0, v3}, Landroid/support/v7/widget/X;->dvC(I)Landroid/support/v7/widget/p;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, v3}, Landroid/support/v7/widget/I;->dwx(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-nez v5, :cond_7

    invoke-virtual {p0, v9, v7, v6, v8}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/I;->dwA(Landroid/support/v7/widget/Y;)V

    move v0, v1

    :goto_1
    move v5, v1

    :goto_2
    if-eqz v0, :cond_2

    sub-int v0, v3, v6

    sub-int v3, v4, v6

    move v4, v1

    :goto_3
    add-int/lit8 v0, v0, 0x1

    move v6, v4

    move v4, v3

    move v3, v0

    goto :goto_0

    :cond_1
    if-ne v5, v1, :cond_6

    invoke-virtual {p0, v9, v7, v6, v8}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/I;->dwG(Landroid/support/v7/widget/Y;)V

    move v0, v1

    :goto_4
    move v5, v2

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v10, v3

    move v3, v4

    move v4, v0

    move v0, v10

    goto :goto_3

    :cond_3
    iget v0, p1, Landroid/support/v7/widget/Y;->ebv:I

    if-eq v6, v0, :cond_4

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/I;->dwv(Landroid/support/v7/widget/Y;)V

    invoke-virtual {p0, v9, v7, v6, v8}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object p1

    :cond_4
    if-nez v5, :cond_5

    invoke-direct {p0, p1}, Landroid/support/v7/widget/I;->dwA(Landroid/support/v7/widget/Y;)V

    :goto_5
    return-void

    :cond_5
    invoke-direct {p0, p1}, Landroid/support/v7/widget/I;->dwG(Landroid/support/v7/widget/Y;)V

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method private dwF(Landroid/support/v7/widget/Y;)V
    .locals 10

    const/4 v4, 0x1

    const/4 v8, 0x4

    const/4 v1, 0x0

    iget v2, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v0, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v3, p1, Landroid/support/v7/widget/Y;->ebv:I

    add-int v6, v0, v3

    const/4 v0, -0x1

    iget v3, p1, Landroid/support/v7/widget/Y;->ebu:I

    move v5, v0

    move v0, v1

    :goto_0
    if-ge v3, v6, :cond_4

    iget-object v7, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    invoke-interface {v7, v3}, Landroid/support/v7/widget/X;->dvC(I)Landroid/support/v7/widget/p;

    move-result-object v7

    if-nez v7, :cond_0

    invoke-direct {p0, v3}, Landroid/support/v7/widget/I;->dwx(I)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    if-nez v5, :cond_1

    iget-object v5, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-virtual {p0, v8, v2, v0, v5}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/I;->dwA(Landroid/support/v7/widget/Y;)V

    move v0, v1

    move v2, v3

    :cond_1
    move v5, v2

    move v2, v0

    move v0, v4

    :goto_1
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, 0x1

    move v9, v0

    move v0, v2

    move v2, v5

    move v5, v9

    goto :goto_0

    :cond_2
    if-ne v5, v4, :cond_3

    iget-object v5, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-virtual {p0, v8, v2, v0, v5}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/I;->dwG(Landroid/support/v7/widget/Y;)V

    move v0, v1

    move v2, v3

    :cond_3
    move v5, v2

    move v2, v0

    move v0, v1

    goto :goto_1

    :cond_4
    iget v1, p1, Landroid/support/v7/widget/Y;->ebv:I

    if-eq v0, v1, :cond_5

    iget-object v1, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/I;->dwv(Landroid/support/v7/widget/Y;)V

    invoke-virtual {p0, v8, v2, v0, v1}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object p1

    :cond_5
    if-nez v5, :cond_6

    invoke-direct {p0, p1}, Landroid/support/v7/widget/I;->dwA(Landroid/support/v7/widget/Y;)V

    :goto_2
    return-void

    :cond_6
    invoke-direct {p0, p1}, Landroid/support/v7/widget/I;->dwG(Landroid/support/v7/widget/Y;)V

    goto :goto_2
.end method

.method private dwG(Landroid/support/v7/widget/Y;)V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, p1, Landroid/support/v7/widget/Y;->ebs:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unknown update op type for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v1, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v2, p1, Landroid/support/v7/widget/Y;->ebv:I

    invoke-interface {v0, v1, v2}, Landroid/support/v7/widget/X;->dvx(II)V

    :goto_0
    return-void

    :pswitch_2
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v1, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v2, p1, Landroid/support/v7/widget/Y;->ebv:I

    invoke-interface {v0, v1, v2}, Landroid/support/v7/widget/X;->dvA(II)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v1, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v2, p1, Landroid/support/v7/widget/Y;->ebv:I

    invoke-interface {v0, v1, v2}, Landroid/support/v7/widget/X;->dvD(II)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v1, p1, Landroid/support/v7/widget/Y;->ebu:I

    iget v2, p1, Landroid/support/v7/widget/Y;->ebv:I

    iget-object v3, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-interface {v0, v1, v2, v3}, Landroid/support/v7/widget/X;->dvB(IILjava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private dwL(Landroid/support/v7/widget/Y;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/I;->dwG(Landroid/support/v7/widget/Y;)V

    return-void
.end method

.method private dwM(Landroid/support/v7/widget/Y;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/I;->dwG(Landroid/support/v7/widget/Y;)V

    return-void
.end method

.method private dww(II)I
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v5, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move v1, p1

    :goto_0
    if-ltz v4, :cond_c

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    iget v2, v0, Landroid/support/v7/widget/Y;->ebs:I

    if-ne v2, v7, :cond_8

    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v3, v0, Landroid/support/v7/widget/Y;->ebv:I

    if-ge v2, v3, :cond_1

    iget v3, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v2, v0, Landroid/support/v7/widget/Y;->ebv:I

    :goto_1
    if-lt v1, v3, :cond_6

    if-gt v1, v2, :cond_6

    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-ne v3, v2, :cond_3

    if-ne p2, v5, :cond_2

    iget v2, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebv:I

    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    :goto_3
    add-int/lit8 v1, v4, -0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v3, v0, Landroid/support/v7/widget/Y;->ebv:I

    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    goto :goto_1

    :cond_2
    if-ne p2, v6, :cond_0

    iget v2, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebv:I

    goto :goto_2

    :cond_3
    if-ne p2, v5, :cond_5

    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    :cond_4
    :goto_4
    add-int/lit8 v0, v1, -0x1

    goto :goto_3

    :cond_5
    if-ne p2, v6, :cond_4

    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    goto :goto_4

    :cond_6
    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-ge v1, v2, :cond_11

    if-ne p2, v5, :cond_7

    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v2, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebv:I

    move v0, v1

    goto :goto_3

    :cond_7
    if-ne p2, v6, :cond_11

    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v2, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebv:I

    move v0, v1

    goto :goto_3

    :cond_8
    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-gt v2, v1, :cond_a

    iget v2, v0, Landroid/support/v7/widget/Y;->ebs:I

    if-ne v2, v5, :cond_9

    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    sub-int v0, v1, v0

    goto :goto_3

    :cond_9
    iget v2, v0, Landroid/support/v7/widget/Y;->ebs:I

    if-ne v2, v6, :cond_11

    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/2addr v0, v1

    goto :goto_3

    :cond_a
    if-ne p2, v5, :cond_b

    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    move v0, v1

    goto :goto_3

    :cond_b
    if-ne p2, v6, :cond_11

    iget v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Landroid/support/v7/widget/Y;->ebu:I

    move v0, v1

    goto :goto_3

    :cond_c
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_5
    if-ltz v2, :cond_10

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    iget v3, v0, Landroid/support/v7/widget/Y;->ebs:I

    if-ne v3, v7, :cond_f

    iget v3, v0, Landroid/support/v7/widget/Y;->ebv:I

    iget v4, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-eq v3, v4, :cond_d

    iget v3, v0, Landroid/support/v7/widget/Y;->ebv:I

    if-gez v3, :cond_e

    :cond_d
    iget-object v3, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/I;->dwv(Landroid/support/v7/widget/Y;)V

    :cond_e
    :goto_6
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_5

    :cond_f
    iget v3, v0, Landroid/support/v7/widget/Y;->ebv:I

    if-gtz v3, :cond_e

    iget-object v3, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/I;->dwv(Landroid/support/v7/widget/Y;)V

    goto :goto_6

    :cond_10
    return v1

    :cond_11
    move v0, v1

    goto/16 :goto_3
.end method

.method private dwx(I)Z
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    iget v4, v0, Landroid/support/v7/widget/Y;->ebs:I

    const/16 v5, 0x8

    if-ne v4, v5, :cond_0

    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p0, v0, v4}, Landroid/support/v7/widget/I;->dwt(II)I

    move-result v0

    if-ne v0, p1, :cond_2

    return v6

    :cond_0
    iget v4, v0, Landroid/support/v7/widget/Y;->ebs:I

    if-ne v4, v6, :cond_2

    iget v4, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v5, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/2addr v4, v5

    iget v0, v0, Landroid/support/v7/widget/Y;->ebu:I

    :goto_1
    if-ge v0, v4, :cond_2

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v0, v5}, Landroid/support/v7/widget/I;->dwt(II)I

    move-result v5

    if-ne v5, p1, :cond_1

    return v6

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return v2
.end method


# virtual methods
.method dwB(II)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {p0, v0, p1, p2, v3}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v2, p0, Landroid/support/v7/widget/I;->eaO:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Landroid/support/v7/widget/I;->eaO:I

    iget-object v2, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method dwC()V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaS:Landroid/support/v7/widget/dc;

    iget-object v1, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/dc;->dIO(Ljava/util/List;)V

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    iget v3, v0, Landroid/support/v7/widget/Y;->ebs:I

    packed-switch v3, :pswitch_data_0

    :goto_1
    :pswitch_0
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaR:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaR:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v0}, Landroid/support/v7/widget/I;->dwL(Landroid/support/v7/widget/Y;)V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0, v0}, Landroid/support/v7/widget/I;->dwE(Landroid/support/v7/widget/Y;)V

    goto :goto_1

    :pswitch_3
    invoke-direct {p0, v0}, Landroid/support/v7/widget/I;->dwF(Landroid/support/v7/widget/Y;)V

    goto :goto_1

    :pswitch_4
    invoke-direct {p0, v0}, Landroid/support/v7/widget/I;->dwM(Landroid/support/v7/widget/Y;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method dwD(Landroid/support/v7/widget/Y;I)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/X;->dvE(Landroid/support/v7/widget/Y;)V

    iget v0, p1, Landroid/support/v7/widget/Y;->ebs:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "only remove and update ops can be dispatched in first pass"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v1, p1, Landroid/support/v7/widget/Y;->ebv:I

    invoke-interface {v0, p2, v1}, Landroid/support/v7/widget/X;->dvy(II)V

    :goto_0
    return-void

    :pswitch_2
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v1, p1, Landroid/support/v7/widget/Y;->ebv:I

    iget-object v2, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-interface {v0, p2, v1, v2}, Landroid/support/v7/widget/X;->dvB(IILjava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method dwH()V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    invoke-interface {v4, v0}, Landroid/support/v7/widget/X;->dvz(Landroid/support/v7/widget/Y;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/I;->dwK(Ljava/util/List;)V

    iput v2, p0, Landroid/support/v7/widget/I;->eaO:I

    return-void
.end method

.method public dwI(I)I
    .locals 6

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    move v1, p1

    :goto_0
    if-ge v2, v3, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    iget v4, v0, Landroid/support/v7/widget/Y;->ebs:I

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :sswitch_0
    iget v4, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-gt v4, v1, :cond_5

    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/2addr v0, v1

    :goto_2
    move v1, v0

    goto :goto_1

    :sswitch_1
    iget v4, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-gt v4, v1, :cond_0

    iget v4, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v5, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/2addr v4, v5

    if-le v4, v1, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_1
    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    sub-int/2addr v1, v0

    goto :goto_1

    :sswitch_2
    iget v4, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-ne v4, v1, :cond_2

    iget v1, v0, Landroid/support/v7/widget/Y;->ebv:I

    goto :goto_1

    :cond_2
    iget v4, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-ge v4, v1, :cond_3

    add-int/lit8 v1, v1, -0x1

    :cond_3
    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    if-gt v0, v1, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    return v1

    :cond_5
    move v0, v1

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method dwJ(II)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {p0, v3, p1, p2, v4}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v2, p0, Landroid/support/v7/widget/I;->eaO:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Landroid/support/v7/widget/I;->eaO:I

    iget-object v2, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method dwK(Ljava/util/List;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/I;->dwv(Landroid/support/v7/widget/Y;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method dwN()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method dwO()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/I;->dwK(Ljava/util/List;)V

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/I;->dwK(Ljava/util/List;)V

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/I;->eaO:I

    return-void
.end method

.method dwP()V
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/I;->dwH()V

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    iget v4, v0, Landroid/support/v7/widget/Y;->ebs:I

    packed-switch v4, :pswitch_data_0

    :goto_1
    :pswitch_0
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaR:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaR:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    invoke-interface {v4, v0}, Landroid/support/v7/widget/X;->dvz(Landroid/support/v7/widget/Y;)V

    iget-object v4, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v5, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    invoke-interface {v4, v5, v0}, Landroid/support/v7/widget/X;->dvx(II)V

    goto :goto_1

    :pswitch_2
    iget-object v4, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    invoke-interface {v4, v0}, Landroid/support/v7/widget/X;->dvz(Landroid/support/v7/widget/Y;)V

    iget-object v4, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v5, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    invoke-interface {v4, v5, v0}, Landroid/support/v7/widget/X;->dvy(II)V

    goto :goto_1

    :pswitch_3
    iget-object v4, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    invoke-interface {v4, v0}, Landroid/support/v7/widget/X;->dvz(Landroid/support/v7/widget/Y;)V

    iget-object v4, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v5, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v6, v0, Landroid/support/v7/widget/Y;->ebv:I

    iget-object v0, v0, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    invoke-interface {v4, v5, v6, v0}, Landroid/support/v7/widget/X;->dvB(IILjava/lang/Object;)V

    goto :goto_1

    :pswitch_4
    iget-object v4, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    invoke-interface {v4, v0}, Landroid/support/v7/widget/X;->dvz(Landroid/support/v7/widget/Y;)V

    iget-object v4, p0, Landroid/support/v7/widget/I;->eaN:Landroid/support/v7/widget/X;

    iget v5, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    invoke-interface {v4, v5, v0}, Landroid/support/v7/widget/X;->dvA(II)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/I;->dwK(Ljava/util/List;)V

    iput v2, p0, Landroid/support/v7/widget/I;->eaO:I

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method dwQ(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/I;->dwt(II)I

    move-result v0

    return v0
.end method

.method dwR(III)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-ne p1, p2, :cond_0

    return v1

    :cond_0
    if-eq p3, v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Moving more than 1 item is not supported yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    const/16 v3, 0x8

    const/4 v4, 0x0

    invoke-virtual {p0, v3, p1, p2, v4}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v2, p0, Landroid/support/v7/widget/I;->eaO:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Landroid/support/v7/widget/I;->eaO:I

    iget-object v2, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v0, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method dws(I)Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/widget/I;->eaO:I

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method dwt(II)I
    .locals 5

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, p1

    :goto_0
    if-ge p2, v2, :cond_6

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    iget v3, v0, Landroid/support/v7/widget/Y;->ebs:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    iget v3, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-ne v3, v1, :cond_1

    iget v1, v0, Landroid/support/v7/widget/Y;->ebv:I

    :cond_0
    :goto_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    iget v3, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-ge v3, v1, :cond_2

    add-int/lit8 v1, v1, -0x1

    :cond_2
    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    if-gt v0, v1, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget v3, v0, Landroid/support/v7/widget/Y;->ebu:I

    if-gt v3, v1, :cond_0

    iget v3, v0, Landroid/support/v7/widget/Y;->ebs:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    iget v3, v0, Landroid/support/v7/widget/Y;->ebu:I

    iget v4, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/2addr v3, v4

    if-ge v1, v3, :cond_4

    const/4 v0, -0x1

    return v0

    :cond_4
    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    sub-int/2addr v1, v0

    goto :goto_1

    :cond_5
    iget v3, v0, Landroid/support/v7/widget/Y;->ebs:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    iget v0, v0, Landroid/support/v7/widget/Y;->ebv:I

    add-int/2addr v1, v0

    goto :goto_1

    :cond_6
    return v1
.end method

.method public dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaQ:Landroid/support/v4/a/j;

    invoke-interface {v0}, Landroid/support/v4/a/j;->dSD()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Y;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/widget/Y;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/support/v7/widget/Y;-><init>(IIILjava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    iput p1, v0, Landroid/support/v7/widget/Y;->ebs:I

    iput p2, v0, Landroid/support/v7/widget/Y;->ebu:I

    iput p3, v0, Landroid/support/v7/widget/Y;->ebv:I

    iput-object p4, v0, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    goto :goto_0
.end method

.method public dwv(Landroid/support/v7/widget/Y;)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/widget/I;->eaP:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p1, Landroid/support/v7/widget/Y;->ebt:Ljava/lang/Object;

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaQ:Landroid/support/v4/a/j;

    invoke-interface {v0, p1}, Landroid/support/v4/a/j;->dSC(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method dwy(IILjava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    const/4 v3, 0x4

    invoke-virtual {p0, v3, p1, p2, p3}, Landroid/support/v7/widget/I;->dwu(IIILjava/lang/Object;)Landroid/support/v7/widget/Y;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v2, p0, Landroid/support/v7/widget/I;->eaO:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Landroid/support/v7/widget/I;->eaO:I

    iget-object v2, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method dwz()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/I;->eaL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
