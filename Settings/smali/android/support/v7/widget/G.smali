.class Landroid/support/v7/widget/G;
.super Ljava/lang/Object;
.source "ChildHelper.java"


# instance fields
.field final eaI:Ljava/util/List;

.field final eaJ:Landroid/support/v7/widget/W;

.field final eaK:Landroid/support/v7/widget/cT;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/W;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    new-instance v0, Landroid/support/v7/widget/cT;

    invoke-direct {v0}, Landroid/support/v7/widget/cT;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    return-void
.end method

.method private dvU(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/W;->dvo(Landroid/view/View;)V

    return-void
.end method

.method private dvX(Landroid/view/View;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/W;->dvu(Landroid/view/View;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private dwb(I)I
    .locals 4

    const/4 v3, -0x1

    if-gez p1, :cond_0

    return v3

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0}, Landroid/support/v7/widget/W;->dvr()I

    move-result v1

    move v0, p1

    :goto_0
    if-ge v0, v1, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/cT;->dIG(I)I

    move-result v2

    sub-int v2, v0, v2

    sub-int v2, p1, v2

    if-nez v2, :cond_2

    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->dIF(I)Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return v0

    :cond_2
    add-int/2addr v0, v2

    goto :goto_0

    :cond_3
    return v3
.end method


# virtual methods
.method dvK(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/W;->dvv(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method dvL(Landroid/view/View;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method dvM(Landroid/view/View;)I
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/W;->dvq(Landroid/view/View;)I

    move-result v0

    if-ne v0, v2, :cond_0

    return v2

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->dIF(I)Z

    move-result v1

    if-eqz v1, :cond_1

    return v2

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->dIG(I)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method dvN(Landroid/view/View;Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, p2}, Landroid/support/v7/widget/G;->dvR(Landroid/view/View;IZ)V

    return-void
.end method

.method dvO()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0}, Landroid/support/v7/widget/W;->dvr()I

    move-result v0

    return v0
.end method

.method dvP(I)Landroid/view/View;
    .locals 5

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v3, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v3, v0}, Landroid/support/v7/widget/W;->dvm(Landroid/view/View;)Landroid/support/v7/widget/p;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->getLayoutPosition()I

    move-result v4

    if-ne v4, p1, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isInvalid()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/p;->isRemoved()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method dvQ(I)Landroid/view/View;
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dwb(I)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/W;->dvv(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method dvR(Landroid/view/View;IZ)V
    .locals 2

    if-gez p2, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0}, Landroid/support/v7/widget/W;->dvr()I

    move-result v0

    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0, p3}, Landroid/support/v7/widget/cT;->dID(IZ)V

    if-eqz p3, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dvU(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v1, p1, v0}, Landroid/support/v7/widget/W;->addView(Landroid/view/View;I)V

    return-void

    :cond_1
    invoke-direct {p0, p2}, Landroid/support/v7/widget/G;->dwb(I)I

    move-result v0

    goto :goto_0
.end method

.method dvS(I)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dwb(I)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/W;->dvv(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/cT;->remove(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v1}, Landroid/support/v7/widget/G;->dvX(Landroid/view/View;)Z

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/W;->dvn(I)V

    return-void
.end method

.method dvT(Landroid/view/View;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/W;->dvq(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dvX(Landroid/view/View;)Z

    move-result v0

    return v2

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->dIF(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->remove(I)Z

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dvX(Landroid/view/View;)Z

    move-result v1

    iget-object v1, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/W;->dvn(I)V

    return v2

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method dvV(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V
    .locals 2

    if-gez p2, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0}, Landroid/support/v7/widget/W;->dvr()I

    move-result v0

    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0, p4}, Landroid/support/v7/widget/cT;->dID(IZ)V

    if-eqz p4, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dvU(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v1, p1, v0, p3}, Landroid/support/v7/widget/W;->dvs(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_1
    invoke-direct {p0, p2}, Landroid/support/v7/widget/G;->dwb(I)I

    move-result v0

    goto :goto_0
.end method

.method dvW(I)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dwb(I)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->remove(I)Z

    iget-object v1, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/W;->dvp(I)V

    return-void
.end method

.method dvY()V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v0}, Landroid/support/v7/widget/cT;->dIH()V

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v2, v0}, Landroid/support/v7/widget/W;->dvu(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0}, Landroid/support/v7/widget/W;->dvt()V

    return-void
.end method

.method dvZ()I
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0}, Landroid/support/v7/widget/W;->dvr()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method dwa(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/W;->dvq(Landroid/view/View;)I

    move-result v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "view is not a child, cannot hide "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->dII(I)V

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dvU(Landroid/view/View;)V

    return-void
.end method

.method dwc(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/W;->dvq(Landroid/view/View;)I

    move-result v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "view is not a child, cannot hide "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->dIF(I)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "trying to unhide a view that was not hidden"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->dIC(I)V

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dvX(Landroid/view/View;)Z

    return-void
.end method

.method removeView(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/W;->dvq(Landroid/view/View;)I

    move-result v0

    if-gez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/cT;->remove(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Landroid/support/v7/widget/G;->dvX(Landroid/view/View;)Z

    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/G;->eaJ:Landroid/support/v7/widget/W;

    invoke-interface {v1, v0}, Landroid/support/v7/widget/W;->dvn(I)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid/support/v7/widget/G;->eaK:Landroid/support/v7/widget/cT;

    invoke-virtual {v1}, Landroid/support/v7/widget/cT;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", hidden list:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/G;->eaI:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
