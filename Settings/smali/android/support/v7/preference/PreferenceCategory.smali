.class public Landroid/support/v7/preference/PreferenceCategory;
.super Landroid/support/v7/preference/PreferenceGroup;
.source "PreferenceCategory.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    sget v0, Landroid/support/v7/preference/a;->dHV:I

    const v1, 0x101008c

    invoke-static {p1, v0, v1}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/preference/PreferenceGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method


# virtual methods
.method public dkz(Landroid/support/v4/view/a/a;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/support/v7/preference/PreferenceGroup;->dkz(Landroid/support/v4/view/a/a;)V

    invoke-virtual {p1}, Landroid/support/v4/view/a/a;->dML()Landroid/support/v4/view/a/e;

    move-result-object v4

    if-nez v4, :cond_0

    return-void

    :cond_0
    invoke-virtual {v4}, Landroid/support/v4/view/a/e;->dNJ()I

    move-result v0

    invoke-virtual {v4}, Landroid/support/v4/view/a/e;->dNG()I

    move-result v1

    invoke-virtual {v4}, Landroid/support/v4/view/a/e;->dNL()I

    move-result v2

    invoke-virtual {v4}, Landroid/support/v4/view/a/e;->dNH()I

    move-result v3

    invoke-virtual {v4}, Landroid/support/v4/view/a/e;->dNK()Z

    move-result v5

    const/4 v4, 0x1

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/a/e;->dNI(IIIIZZ)Landroid/support/v4/view/a/e;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dMT(Ljava/lang/Object;)V

    return-void
.end method

.method protected dlj(Landroid/support/v7/preference/Preference;)Z
    .locals 2

    instance-of v0, p1, Landroid/support/v7/preference/PreferenceCategory;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Cannot add a PreferenceCategory directly to a PreferenceCategory"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/preference/PreferenceGroup;->dlj(Landroid/support/v7/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public shouldDisableDependents()Z
    .locals 1

    invoke-super {p0}, Landroid/support/v7/preference/PreferenceGroup;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
