.class public Landroid/support/v7/preference/y;
.super Landroid/support/v7/widget/b;
.source "PreferenceGroupAdapter.java"

# interfaces
.implements Landroid/support/v7/preference/h;


# instance fields
.field private dNk:Landroid/os/Handler;

.field private dNl:Ljava/util/List;

.field private dNm:Ljava/lang/Runnable;

.field private dNn:Ljava/util/List;

.field private dNo:Ljava/util/List;

.field private dNp:Landroid/support/v7/preference/PreferenceGroup;

.field private dNq:Landroid/support/v7/preference/z;


# direct methods
.method public constructor <init>(Landroid/support/v7/preference/PreferenceGroup;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/b;-><init>()V

    new-instance v0, Landroid/support/v7/preference/z;

    invoke-direct {v0}, Landroid/support/v7/preference/z;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/y;->dNq:Landroid/support/v7/preference/z;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/y;->dNk:Landroid/os/Handler;

    new-instance v0, Landroid/support/v7/preference/A;

    invoke-direct {v0, p0}, Landroid/support/v7/preference/A;-><init>(Landroid/support/v7/preference/y;)V

    iput-object v0, p0, Landroid/support/v7/preference/y;->dNm:Ljava/lang/Runnable;

    iput-object p1, p0, Landroid/support/v7/preference/y;->dNp:Landroid/support/v7/preference/PreferenceGroup;

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNp:Landroid/support/v7/preference/PreferenceGroup;

    invoke-virtual {v0, p0}, Landroid/support/v7/preference/PreferenceGroup;->dkT(Landroid/support/v7/preference/h;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/y;->dNo:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/y;->dNn:Ljava/util/List;

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNp:Landroid/support/v7/preference/PreferenceGroup;

    instance-of v0, v0, Landroid/support/v7/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNp:Landroid/support/v7/preference/PreferenceGroup;

    check-cast v0, Landroid/support/v7/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/support/v7/preference/PreferenceScreen;->dlb()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/y;->setHasStableIds(Z)V

    :goto_0
    invoke-direct {p0}, Landroid/support/v7/preference/y;->dmD()V

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/y;->setHasStableIds(Z)V

    goto :goto_0
.end method

.method private dmA(Landroid/support/v7/preference/Preference;Landroid/support/v7/preference/z;)Landroid/support/v7/preference/z;
    .locals 1

    if-eqz p2, :cond_0

    :goto_0
    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/support/v7/preference/z;->dmF(Landroid/support/v7/preference/z;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getLayoutResource()I

    move-result v0

    invoke-static {p2, v0}, Landroid/support/v7/preference/z;->dmE(Landroid/support/v7/preference/z;I)I

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->dla()I

    move-result v0

    invoke-static {p2, v0}, Landroid/support/v7/preference/z;->dmH(Landroid/support/v7/preference/z;I)I

    return-object p2

    :cond_0
    new-instance p2, Landroid/support/v7/preference/z;

    invoke-direct {p2}, Landroid/support/v7/preference/z;-><init>()V

    goto :goto_0
.end method

.method private dmB(Ljava/util/List;Landroid/support/v7/preference/PreferenceGroup;)V
    .locals 5

    invoke-virtual {p2}, Landroid/support/v7/preference/PreferenceGroup;->dle()V

    invoke-virtual {p2}, Landroid/support/v7/preference/PreferenceGroup;->dln()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p2, v2}, Landroid/support/v7/preference/PreferenceGroup;->dlf(I)Landroid/support/v7/preference/Preference;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v1}, Landroid/support/v7/preference/y;->dmz(Landroid/support/v7/preference/Preference;)V

    instance-of v0, v1, Landroid/support/v7/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/support/v7/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/support/v7/preference/PreferenceGroup;->dlc()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/preference/y;->dmB(Ljava/util/List;Landroid/support/v7/preference/PreferenceGroup;)V

    :cond_0
    invoke-virtual {v1, p0}, Landroid/support/v7/preference/Preference;->dkT(Landroid/support/v7/preference/h;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic dmC(Landroid/support/v7/preference/y;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/preference/y;->dmD()V

    return-void
.end method

.method private dmD()V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNo:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/support/v7/preference/Preference;->dkT(Landroid/support/v7/preference/h;)V

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNp:Landroid/support/v7/preference/PreferenceGroup;

    invoke-direct {p0, v1, v0}, Landroid/support/v7/preference/y;->dmB(Ljava/util/List;Landroid/support/v7/preference/PreferenceGroup;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    invoke-virtual {v0}, Landroid/support/v7/preference/Preference;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    iput-object v2, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    iput-object v1, p0, Landroid/support/v7/preference/y;->dNo:Ljava/util/List;

    iget-object v3, p0, Landroid/support/v7/preference/y;->dNp:Landroid/support/v7/preference/PreferenceGroup;

    invoke-virtual {v3}, Landroid/support/v7/preference/PreferenceGroup;->dky()Landroid/support/v7/preference/k;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/support/v7/preference/k;->dlK()Landroid/support/v7/preference/r;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Landroid/support/v7/preference/k;->dlK()Landroid/support/v7/preference/r;

    move-result-object v3

    new-instance v4, Landroid/support/v7/preference/B;

    invoke-direct {v4, p0, v0, v2, v3}, Landroid/support/v7/preference/B;-><init>(Landroid/support/v7/preference/y;Ljava/util/List;Ljava/util/List;Landroid/support/v7/preference/r;)V

    invoke-static {v4}, Landroid/support/v7/c/a;->dIT(Landroid/support/v7/c/b;)Landroid/support/v7/c/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v7/c/e;->dJc(Landroid/support/v7/widget/b;)V

    :goto_2
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    invoke-virtual {v0}, Landroid/support/v7/preference/Preference;->dkI()V

    goto :goto_3

    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/preference/y;->notifyDataSetChanged()V

    goto :goto_2

    :cond_4
    return-void
.end method

.method private dmz(Landroid/support/v7/preference/Preference;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/preference/y;->dmA(Landroid/support/v7/preference/Preference;Landroid/support/v7/preference/z;)Landroid/support/v7/preference/z;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/y;->dNn:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/preference/y;->dNn:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public bwL(Landroid/support/v7/preference/l;I)V
    .locals 1

    invoke-virtual {p0, p2}, Landroid/support/v7/preference/y;->getItem(I)Landroid/support/v7/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/preference/Preference;->al(Landroid/support/v7/preference/l;)V

    return-void
.end method

.method public dlC(Landroid/support/v7/preference/Preference;)V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    iget-object v1, p0, Landroid/support/v7/preference/y;->dNo:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/Preference;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v0, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/y;->notifyItemInserted(I)V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/preference/Preference;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_6

    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_4

    iget-object v2, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/support/v7/preference/Preference;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    iget-object v1, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/y;->notifyItemRemoved(I)V

    goto :goto_1

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public dlD(Landroid/support/v7/preference/Preference;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0, p1}, Landroid/support/v7/preference/y;->notifyItemChanged(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public dlE(Landroid/support/v7/preference/Preference;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNk:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/preference/y;->dNm:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNk:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/preference/y;->dNm:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public getItem(I)Landroid/support/v7/preference/Preference;
    .locals 1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/y;->getItemCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNl:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/preference/y;->hasStableIds()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    return-wide v0

    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/preference/y;->getItem(I)Landroid/support/v7/preference/Preference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/Preference;->getId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4

    invoke-virtual {p0, p1}, Landroid/support/v7/preference/y;->getItem(I)Landroid/support/v7/preference/Preference;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/y;->dNq:Landroid/support/v7/preference/z;

    invoke-direct {p0, v0, v1}, Landroid/support/v7/preference/y;->dmA(Landroid/support/v7/preference/Preference;Landroid/support/v7/preference/z;)Landroid/support/v7/preference/z;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/preference/y;->dNq:Landroid/support/v7/preference/z;

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNn:Ljava/util/List;

    iget-object v1, p0, Landroid/support/v7/preference/y;->dNq:Landroid/support/v7/preference/z;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/y;->dNn:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/preference/y;->dNn:Ljava/util/List;

    new-instance v2, Landroid/support/v7/preference/z;

    iget-object v3, p0, Landroid/support/v7/preference/y;->dNq:Landroid/support/v7/preference/z;

    invoke-direct {v2, v3}, Landroid/support/v7/preference/z;-><init>(Landroid/support/v7/preference/z;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/p;I)V
    .locals 0

    check-cast p1, Landroid/support/v7/preference/l;

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/preference/y;->bwL(Landroid/support/v7/preference/l;I)V

    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/preference/l;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/preference/y;->dNn:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/z;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v3, Landroid/support/v7/preference/d;->dIw:[I

    invoke-virtual {v1, v4, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    sget v1, Landroid/support/v7/preference/d;->dIx:I

    invoke-virtual {v3, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x1080062

    invoke-static {v1, v4}, Landroid/support/v4/content/a;->eab(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_0
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static {v0}, Landroid/support/v7/preference/z;->dmG(Landroid/support/v7/preference/z;)I

    move-result v3

    invoke-virtual {v2, v3, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-static {v3, v1}, Landroid/support/v4/view/z;->dPw(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    :cond_1
    const v1, 0x1020018

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    invoke-static {v0}, Landroid/support/v7/preference/z;->dmI(Landroid/support/v7/preference/z;)I

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v0}, Landroid/support/v7/preference/z;->dmI(Landroid/support/v7/preference/z;)I

    move-result v0

    invoke-virtual {v2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    :cond_2
    :goto_0
    new-instance v0, Landroid/support/v7/preference/l;

    invoke-direct {v0, v3}, Landroid/support/v7/preference/l;-><init>(Landroid/view/View;)V

    return-object v0

    :cond_3
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;
    .locals 1

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/preference/y;->onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/preference/l;

    move-result-object v0

    return-object v0
.end method
