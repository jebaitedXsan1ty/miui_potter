.class public Landroid/support/v7/preference/SeekBarPreference;
.super Landroid/support/v7/preference/Preference;
.source "SeekBarPreference.java"


# instance fields
.field private dND:Z

.field private dNE:I

.field private dNF:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private dNG:I

.field private dNH:Z

.field private dNI:I

.field private dNJ:Z

.field private dNK:Landroid/widget/TextView;

.field private dNL:Landroid/view/View$OnKeyListener;

.field private dNM:Landroid/widget/SeekBar;

.field private dNN:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Landroid/support/v7/preference/a;->dHQ:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/preference/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/preference/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Landroid/support/v7/preference/C;

    invoke-direct {v0, p0}, Landroid/support/v7/preference/C;-><init>(Landroid/support/v7/preference/SeekBarPreference;)V

    iput-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNF:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Landroid/support/v7/preference/G;

    invoke-direct {v0, p0}, Landroid/support/v7/preference/G;-><init>(Landroid/support/v7/preference/SeekBarPreference;)V

    iput-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNL:Landroid/view/View$OnKeyListener;

    sget-object v0, Landroid/support/v7/preference/d;->dKA:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Landroid/support/v7/preference/d;->dKD:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    sget v1, Landroid/support/v7/preference/d;->dKC:I

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/preference/SeekBarPreference;->dmN(I)V

    sget v1, Landroid/support/v7/preference/d;->dKE:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/preference/SeekBarPreference;->dmS(I)V

    sget v1, Landroid/support/v7/preference/d;->dKB:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dND:Z

    sget v1, Landroid/support/v7/preference/d;->dKF:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNH:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private dmK(IZ)V
    .locals 2

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    if-ge p1, v0, :cond_0

    iget p1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    :cond_0
    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNI:I

    if-le p1, v0, :cond_1

    iget p1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNI:I

    :cond_1
    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    if-eq p1, v0, :cond_3

    iput p1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNK:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNK:Landroid/widget/TextView;

    iget v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p0, p1}, Landroid/support/v7/preference/SeekBarPreference;->dkP(I)Z

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/preference/SeekBarPreference;->notifyChanged()V

    :cond_3
    return-void
.end method

.method static synthetic dmL(Landroid/support/v7/preference/SeekBarPreference;Landroid/widget/SeekBar;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/preference/SeekBarPreference;->dmT(Landroid/widget/SeekBar;)V

    return-void
.end method

.method static synthetic dmM(Landroid/support/v7/preference/SeekBarPreference;)I
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    return v0
.end method

.method static synthetic dmO(Landroid/support/v7/preference/SeekBarPreference;)Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dND:Z

    return v0
.end method

.method static synthetic dmP(Landroid/support/v7/preference/SeekBarPreference;)I
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    return v0
.end method

.method static synthetic dmQ(Landroid/support/v7/preference/SeekBarPreference;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNM:Landroid/widget/SeekBar;

    return-object v0
.end method

.method private dmT(Landroid/widget/SeekBar;)V
    .locals 2

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    if-eq v0, v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/preference/SeekBarPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/preference/SeekBarPreference;->dmK(IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    iget v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method static synthetic dmU(Landroid/support/v7/preference/SeekBarPreference;)Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNJ:Z

    return v0
.end method

.method static synthetic dmV(Landroid/support/v7/preference/SeekBarPreference;Z)Z
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNJ:Z

    return p1
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/support/v7/preference/Preference;->al(Landroid/support/v7/preference/l;)V

    iget-object v0, p1, Landroid/support/v7/preference/l;->itemView:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNL:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    sget v0, Landroid/support/v7/preference/b;->dIc:I

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNM:Landroid/widget/SeekBar;

    sget v0, Landroid/support/v7/preference/b;->dId:I

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNK:Landroid/widget/TextView;

    iget-boolean v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNH:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNK:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNM:Landroid/widget/SeekBar;

    if-nez v0, :cond_1

    const-string/jumbo v0, "SeekBarPreference"

    const-string/jumbo v1, "SeekBar view is null in onBindViewHolder."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNK:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iput-object v2, p0, Landroid/support/v7/preference/SeekBarPreference;->dNK:Landroid/widget/TextView;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNM:Landroid/widget/SeekBar;

    iget-object v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNF:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNM:Landroid/widget/SeekBar;

    iget v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNI:I

    iget v2, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNG:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNM:Landroid/widget/SeekBar;

    iget v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNG:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setKeyProgressIncrement(I)V

    :goto_1
    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNM:Landroid/widget/SeekBar;

    iget v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    iget v2, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNK:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNK:Landroid/widget/TextView;

    iget v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNM:Landroid/widget/SeekBar;

    invoke-virtual {p0}, Landroid/support/v7/preference/SeekBarPreference;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    return-void

    :cond_3
    iget-object v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNM:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getKeyProgressIncrement()I

    move-result v0

    iput v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNG:I

    goto :goto_1
.end method

.method public final dmN(I)V
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    if-ge p1, v0, :cond_0

    iget p1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    :cond_0
    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNI:I

    if-eq p1, v0, :cond_1

    iput p1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNI:I

    invoke-virtual {p0}, Landroid/support/v7/preference/SeekBarPreference;->notifyChanged()V

    :cond_1
    return-void
.end method

.method public dmR(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/support/v7/preference/SeekBarPreference;->dmK(IZ)V

    return-void
.end method

.method public final dmS(I)V
    .locals 2

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNG:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNI:I

    iget v1, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    sub-int/2addr v0, v1

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNG:I

    invoke-virtual {p0}, Landroid/support/v7/preference/SeekBarPreference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/support/v7/preference/SeekBarPreference$SavedState;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v7/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    :cond_0
    check-cast p1, Landroid/support/v7/preference/SeekBarPreference$SavedState;

    invoke-virtual {p1}, Landroid/support/v7/preference/SeekBarPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/support/v7/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Landroid/support/v7/preference/SeekBarPreference$SavedState;->dNT:I

    iput v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    iget v0, p1, Landroid/support/v7/preference/SeekBarPreference$SavedState;->dNV:I

    iput v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    iget v0, p1, Landroid/support/v7/preference/SeekBarPreference$SavedState;->dNU:I

    iput v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNI:I

    invoke-virtual {p0}, Landroid/support/v7/preference/SeekBarPreference;->notifyChanged()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/support/v7/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v7/preference/SeekBarPreference;->dkN()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v1, Landroid/support/v7/preference/SeekBarPreference$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v7/preference/SeekBarPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    iput v0, v1, Landroid/support/v7/preference/SeekBarPreference$SavedState;->dNT:I

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNN:I

    iput v0, v1, Landroid/support/v7/preference/SeekBarPreference$SavedState;->dNV:I

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNI:I

    iput v0, v1, Landroid/support/v7/preference/SeekBarPreference$SavedState;->dNU:I

    return-object v1
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Landroid/support/v7/preference/SeekBarPreference;->dNE:I

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/SeekBarPreference;->dkj(I)I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/preference/SeekBarPreference;->dmR(I)V

    return-void

    :cond_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
