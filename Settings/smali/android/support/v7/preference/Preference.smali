.class public Landroid/support/v7/preference/Preference;
.super Ljava/lang/Object;
.source "Preference.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private dLG:Z

.field private dLH:Z

.field private dLI:Ljava/lang/String;

.field private dLJ:Z

.field private dLK:J

.field private dLL:Ljava/util/List;

.field private dLM:Z

.field private dLN:Z

.field private dLO:Z

.field private dLP:Landroid/support/v7/preference/PreferenceGroup;

.field private dLQ:I

.field private dLR:I

.field private dLS:Z

.field private dLT:Z

.field private dLU:Z

.field private dLV:Z

.field private dLW:Landroid/support/v7/preference/g;

.field private dLX:Z

.field private dLY:Z

.field private dLZ:Landroid/support/v7/preference/h;

.field private dMa:Landroid/support/v7/preference/j;

.field private final dMb:Landroid/view/View$OnClickListener;

.field private dMc:Landroid/support/v7/preference/f;

.field private dMd:Z

.field private dMe:Z

.field private dMf:I

.field private dMg:I

.field private dMh:Landroid/support/v7/preference/k;

.field private dMi:Z

.field private dMj:Ljava/lang/Object;

.field private dMk:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mEnabled:Z

.field private mExtras:Landroid/os/Bundle;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIconResId:I

.field private mIntent:Landroid/content/Intent;

.field private mKey:Ljava/lang/String;

.field private mSummary:Ljava/lang/CharSequence;

.field private mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    sget v0, Landroid/support/v7/preference/a;->dHP:I

    const v1, 0x101008e

    invoke-static {p1, v0, v1}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 6

    const v3, 0x7fffffff

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Landroid/support/v7/preference/Preference;->dMf:I

    iput v5, p0, Landroid/support/v7/preference/Preference;->dLR:I

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->mEnabled:Z

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->dLY:Z

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->dLJ:Z

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->dLH:Z

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->dLO:Z

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->dLS:Z

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->dLU:Z

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->dLV:Z

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->dLM:Z

    iput-boolean v4, p0, Landroid/support/v7/preference/Preference;->dMd:Z

    sget v0, Landroid/support/v7/preference/c;->dIg:I

    iput v0, p0, Landroid/support/v7/preference/Preference;->dLQ:I

    new-instance v0, Landroid/support/v7/preference/i;

    invoke-direct {v0, p0}, Landroid/support/v7/preference/i;-><init>(Landroid/support/v7/preference/Preference;)V

    iput-object v0, p0, Landroid/support/v7/preference/Preference;->dMb:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Landroid/support/v7/preference/Preference;->mContext:Landroid/content/Context;

    sget-object v0, Landroid/support/v7/preference/d;->dJD:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Landroid/support/v7/preference/d;->dKi:I

    sget v2, Landroid/support/v7/preference/d;->dJS:I

    invoke-static {v0, v1, v2, v5}, Landroid/support/v4/content/a/a;->dZy(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Landroid/support/v7/preference/Preference;->mIconResId:I

    sget v1, Landroid/support/v7/preference/d;->dKk:I

    sget v2, Landroid/support/v7/preference/d;->dJU:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    sget v1, Landroid/support/v7/preference/d;->dKs:I

    sget v2, Landroid/support/v7/preference/d;->dKc:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZA(Landroid/content/res/TypedArray;II)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    sget v1, Landroid/support/v7/preference/d;->dKr:I

    sget v2, Landroid/support/v7/preference/d;->dKb:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZA(Landroid/content/res/TypedArray;II)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    sget v1, Landroid/support/v7/preference/d;->dKm:I

    sget v2, Landroid/support/v7/preference/d;->dJW:I

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/content/a/a;->dZu(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Landroid/support/v7/preference/Preference;->dMf:I

    sget v1, Landroid/support/v7/preference/d;->dKh:I

    sget v2, Landroid/support/v7/preference/d;->dJR:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/Preference;->dLI:Ljava/lang/String;

    sget v1, Landroid/support/v7/preference/d;->dKl:I

    sget v2, Landroid/support/v7/preference/d;->dJV:I

    sget v3, Landroid/support/v7/preference/c;->dIg:I

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/content/a/a;->dZy(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Landroid/support/v7/preference/Preference;->dLQ:I

    sget v1, Landroid/support/v7/preference/d;->dKt:I

    sget v2, Landroid/support/v7/preference/d;->dKd:I

    invoke-static {v0, v1, v2, v5}, Landroid/support/v4/content/a/a;->dZy(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Landroid/support/v7/preference/Preference;->dMg:I

    sget v1, Landroid/support/v7/preference/d;->dKg:I

    sget v2, Landroid/support/v7/preference/d;->dJQ:I

    invoke-static {v0, v1, v2, v4}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->mEnabled:Z

    sget v1, Landroid/support/v7/preference/d;->dKo:I

    sget v2, Landroid/support/v7/preference/d;->dJY:I

    invoke-static {v0, v1, v2, v4}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLY:Z

    sget v1, Landroid/support/v7/preference/d;->dKn:I

    sget v2, Landroid/support/v7/preference/d;->dJX:I

    invoke-static {v0, v1, v2, v4}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLJ:Z

    sget v1, Landroid/support/v7/preference/d;->dKf:I

    sget v2, Landroid/support/v7/preference/d;->dJP:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/Preference;->dMk:Ljava/lang/String;

    sget v1, Landroid/support/v7/preference/d;->dJM:I

    sget v2, Landroid/support/v7/preference/d;->dJM:I

    iget-boolean v3, p0, Landroid/support/v7/preference/Preference;->dLY:Z

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLU:Z

    sget v1, Landroid/support/v7/preference/d;->dJN:I

    sget v2, Landroid/support/v7/preference/d;->dJN:I

    iget-boolean v3, p0, Landroid/support/v7/preference/Preference;->dLY:Z

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLV:Z

    sget v1, Landroid/support/v7/preference/d;->dKe:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_2

    sget v1, Landroid/support/v7/preference/d;->dKe:I

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/preference/Preference;->onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/Preference;->dMj:Ljava/lang/Object;

    :cond_0
    :goto_0
    sget v1, Landroid/support/v7/preference/d;->dKp:I

    sget v2, Landroid/support/v7/preference/d;->dJZ:I

    invoke-static {v0, v1, v2, v4}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dMd:Z

    sget v1, Landroid/support/v7/preference/d;->dKq:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dMe:Z

    iget-boolean v1, p0, Landroid/support/v7/preference/Preference;->dMe:Z

    if-eqz v1, :cond_1

    sget v1, Landroid/support/v7/preference/d;->dKq:I

    sget v2, Landroid/support/v7/preference/d;->dKa:I

    invoke-static {v0, v1, v2, v4}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLM:Z

    :cond_1
    sget v1, Landroid/support/v7/preference/d;->dKj:I

    sget v2, Landroid/support/v7/preference/d;->dJT:I

    invoke-static {v0, v1, v2, v5}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLG:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_2
    sget v1, Landroid/support/v7/preference/d;->dJO:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Landroid/support/v7/preference/d;->dJO:I

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/preference/Preference;->onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/Preference;->dMj:Ljava/lang/Object;

    goto :goto_0
.end method

.method private dkC(Landroid/support/v7/preference/Preference;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLL:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/Preference;->dLL:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLL:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldDisableDependents()Z

    move-result v0

    invoke-virtual {p1, p0, v0}, Landroid/support/v7/preference/Preference;->dkY(Landroid/support/v7/preference/Preference;Z)V

    return-void
.end method

.method private dkL()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMk:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMk:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->dko(Ljava/lang/String;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {v0, p0}, Landroid/support/v7/preference/Preference;->dkk(Landroid/support/v7/preference/Preference;)V

    :cond_0
    return-void
.end method

.method private dkM(Landroid/content/SharedPreferences$Editor;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/support/v4/content/j;->getInstance()Landroid/support/v4/content/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/content/j;->eaF(Landroid/content/SharedPreferences$Editor;)V

    :cond_0
    return-void
.end method

.method private dkX()V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMk:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMk:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->dko(Ljava/lang/String;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {v0, p0}, Landroid/support/v7/preference/Preference;->dkC(Landroid/support/v7/preference/Preference;)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Dependency \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/preference/Preference;->dMk:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\" not found for preference \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\" (title: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private dki(Landroid/view/View;Z)V
    .locals 2

    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Landroid/support/v7/preference/Preference;->dki(Landroid/view/View;Z)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private dkk(Landroid/support/v7/preference/Preference;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLL:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLL:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private dkl()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMj:Ljava/lang/Object;

    invoke-virtual {p0, v3, v0}, Landroid/support/v7/preference/Preference;->onSetInitialValue(ZLjava/lang/Object;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldPersist()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkE()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMj:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMj:Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/support/v7/preference/Preference;->onSetInitialValue(ZLjava/lang/Object;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p0, v3, v2}, Landroid/support/v7/preference/Preference;->onSetInitialValue(ZLjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 6

    const/4 v2, 0x4

    const/16 v3, 0x8

    const/4 v5, 0x0

    iget-object v0, p1, Landroid/support/v7/preference/l;->itemView:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->dMb:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p1, Landroid/support/v7/preference/l;->itemView:Landroid/view/View;

    iget v1, p0, Landroid/support/v7/preference/Preference;->dLR:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-boolean v1, p0, Landroid/support/v7/preference/Preference;->dMe:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLM:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    :cond_0
    :goto_0
    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    :goto_1
    const v0, 0x1020006

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget v1, p0, Landroid/support/v7/preference/Preference;->mIconResId:I

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v4, p0, Landroid/support/v7/preference/Preference;->mIconResId:I

    invoke-static {v1, v4}, Landroid/support/v4/content/a;->eab(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    :cond_3
    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_a

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_5
    :goto_2
    sget v0, Landroid/support/v7/preference/b;->dIb:I

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_6

    const v0, 0x102003e

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    :cond_6
    if-eqz v0, :cond_7

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_c

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    :goto_3
    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dMd:Z

    if-eqz v0, :cond_e

    iget-object v0, p1, Landroid/support/v7/preference/l;->itemView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->isEnabled()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Landroid/support/v7/preference/Preference;->dki(Landroid/view/View;Z)V

    :goto_4
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkR()Z

    move-result v0

    iget-object v1, p1, Landroid/support/v7/preference/l;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setFocusable(Z)V

    iget-object v1, p1, Landroid/support/v7/preference/l;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLU:Z

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dmc(Z)V

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLV:Z

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dmb(Z)V

    return-void

    :cond_8
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_a
    iget-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLG:Z

    if-eqz v1, :cond_b

    move v1, v2

    :goto_5
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_b
    move v1, v3

    goto :goto_5

    :cond_c
    iget-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLG:Z

    if-eqz v1, :cond_d

    :goto_6
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_d
    move v2, v3

    goto :goto_6

    :cond_e
    iget-object v0, p1, Landroid/support/v7/preference/l;->itemView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Landroid/support/v7/preference/Preference;->dki(Landroid/view/View;Z)V

    goto :goto_4
.end method

.method public boV(Z)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLY:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Landroid/support/v7/preference/Preference;->dLY:Z

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public callChangeListener(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMc:Landroid/support/v7/preference/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMc:Landroid/support/v7/preference/f;

    invoke-interface {v0, p0, p1}, Landroid/support/v7/preference/f;->eH(Landroid/support/v7/preference/Preference;Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Landroid/support/v7/preference/Preference;

    invoke-virtual {p0, p1}, Landroid/support/v7/preference/Preference;->dkp(Landroid/support/v7/preference/Preference;)I

    move-result v0

    return v0
.end method

.method protected cpS(Landroid/support/v7/preference/k;)V
    .locals 2

    iput-object p1, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLX:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/preference/k;->dlY()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v7/preference/Preference;->dLK:J

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/preference/Preference;->dkl()V

    return-void
.end method

.method public dkA()Landroid/support/v7/preference/PreferenceGroup;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLP:Landroid/support/v7/preference/PreferenceGroup;

    return-object v0
.end method

.method public dkB(Landroid/os/Bundle;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/preference/Preference;->dkU(Landroid/os/Bundle;)V

    return-void
.end method

.method public dkD()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public dkE()Landroid/content/SharedPreferences;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlZ()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public dkF()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLI:Ljava/lang/String;

    return-object v0
.end method

.method public final dkG()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dMi:Z

    return v0
.end method

.method protected dkH()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLZ:Landroid/support/v7/preference/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLZ:Landroid/support/v7/preference/h;

    invoke-interface {v0, p0}, Landroid/support/v7/preference/h;->dlE(Landroid/support/v7/preference/Preference;)V

    :cond_0
    return-void
.end method

.method public final dkI()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/preference/Preference;->dMi:Z

    return-void
.end method

.method public dkJ(Landroid/support/v7/preference/f;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/Preference;->dMc:Landroid/support/v7/preference/f;

    return-void
.end method

.method public dkK(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/preference/Preference;->dMd:Z

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->notifyChanged()V

    return-void
.end method

.method public dkN()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLJ:Z

    return v0
.end method

.method dkO()Ljava/lang/StringBuilder;
    .locals 4

    const/16 v3, 0x20

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_2
    return-object v0
.end method

.method protected dkP(I)Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldPersist()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    not-int v0, p1

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->dkj(I)I

    move-result v0

    if-ne p1, v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/preference/j;->putInt(Ljava/lang/String;I)V

    :goto_0
    return v2

    :cond_2
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlT()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, v0}, Landroid/support/v7/preference/Preference;->dkM(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public dkQ(Z)V
    .locals 4

    iget-object v2, p0, Landroid/support/v7/preference/Preference;->dLL:Ljava/util/List;

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/Preference;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/preference/Preference;->dkY(Landroid/support/v7/preference/Preference;Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dkR()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLY:Z

    return v0
.end method

.method public dkS(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/preference/Preference;->dMg:I

    return-void
.end method

.method final dkT(Landroid/support/v7/preference/h;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/Preference;->dLZ:Landroid/support/v7/preference/h;

    return-void
.end method

.method dkU(Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkD()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLT:Z

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLT:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Derived class did not call super.onRestoreInstanceState()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method dkV(Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkD()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLT:Z

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLT:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Derived class did not call super.onSaveInstanceState()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-void
.end method

.method public dkW()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mExtras:Landroid/os/Bundle;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/support/v7/preference/Preference;->mExtras:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method public dkY(Landroid/support/v7/preference/Preference;Z)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLH:Z

    if-ne v0, p2, :cond_0

    xor-int/lit8 v0, p2, 0x1

    iput-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLH:Z

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldDisableDependents()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->dkQ(Z)V

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public dkZ(Ljava/util/Set;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldPersist()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0, v1}, Landroid/support/v7/preference/Preference;->dkh(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/preference/j;->putStringSet(Ljava/lang/String;Ljava/util/Set;)V

    :goto_0
    return v2

    :cond_2
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlT()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, v0}, Landroid/support/v7/preference/Preference;->dkM(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method protected dkg(Landroid/support/v7/preference/k;J)V
    .locals 2

    const/4 v1, 0x0

    iput-wide p2, p0, Landroid/support/v7/preference/Preference;->dLK:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLX:Z

    :try_start_0
    invoke-virtual {p0, p1}, Landroid/support/v7/preference/Preference;->cpS(Landroid/support/v7/preference/k;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLX:Z

    return-void

    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Landroid/support/v7/preference/Preference;->dLX:Z

    throw v0
.end method

.method public dkh(Ljava/util/Set;)Ljava/util/Set;
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldPersist()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/preference/j;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlZ()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected dkj(I)I
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldPersist()Z

    move-result v0

    if-nez v0, :cond_0

    return p1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/preference/j;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlZ()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public dkm(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/preference/Preference;->dkL()V

    iput-object p1, p0, Landroid/support/v7/preference/Preference;->dMk:Ljava/lang/String;

    invoke-direct {p0}, Landroid/support/v7/preference/Preference;->dkX()V

    return-void
.end method

.method dkn()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Preference does not have a key assigned."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLN:Z

    return-void
.end method

.method protected dko(Ljava/lang/String;)Landroid/support/v7/preference/Preference;
    .locals 2

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    if-nez v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0, p1}, Landroid/support/v7/preference/k;->dlS(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method public dkp(Landroid/support/v7/preference/Preference;)I
    .locals 2

    iget v0, p0, Landroid/support/v7/preference/Preference;->dMf:I

    iget v1, p1, Landroid/support/v7/preference/Preference;->dMf:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Landroid/support/v7/preference/Preference;->dMf:I

    iget v1, p1, Landroid/support/v7/preference/Preference;->dMf:I

    sub-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    iget-object v1, p1, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    return v0

    :cond_2
    iget-object v0, p1, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    if-nez v0, :cond_3

    const/4 v0, -0x1

    return v0

    :cond_3
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public dkq(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/preference/Preference;->dLJ:Z

    return-void
.end method

.method public dkr(Landroid/support/v7/preference/g;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/Preference;->dLW:Landroid/support/v7/preference/g;

    return-void
.end method

.method public dks(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public dkt(Landroid/os/Bundle;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/support/v7/preference/Preference;->dkV(Landroid/os/Bundle;)V

    return-void
.end method

.method dku(Landroid/support/v7/preference/PreferenceGroup;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/Preference;->dLP:Landroid/support/v7/preference/PreferenceGroup;

    return-void
.end method

.method public dkv()Landroid/support/v7/preference/j;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMa:Landroid/support/v7/preference/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMa:Landroid/support/v7/preference/j;

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlR()Landroid/support/v7/preference/j;

    move-result-object v0

    return-object v0

    :cond_1
    return-object v1
.end method

.method protected dkw(Z)Z
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldPersist()Z

    move-result v0

    if-nez v0, :cond_0

    return p1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/preference/j;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlZ()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public dkx(Landroid/support/v7/preference/Preference;Z)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLO:Z

    if-ne v0, p2, :cond_0

    xor-int/lit8 v0, p2, 0x1

    iput-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLO:Z

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldDisableDependents()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->dkQ(Z)V

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public dky()Landroid/support/v7/preference/k;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    return-object v0
.end method

.method public dkz(Landroid/support/v4/view/a/a;)V
    .locals 0

    return-void
.end method

.method public final dla()I
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/Preference;->dMg:I

    return v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v7/preference/Preference;->mIconResId:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mContext:Landroid/content/Context;

    iget v1, p0, Landroid/support/v7/preference/Preference;->mIconResId:I

    invoke-static {v0, v1}, Landroid/support/v4/content/a;->eab(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method getId()J
    .locals 2

    iget-wide v0, p0, Landroid/support/v7/preference/Preference;->dLK:J

    return-wide v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getLayoutResource()I
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/Preference;->dLQ:I

    return v0
.end method

.method public getOrder()I
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/Preference;->dMf:I

    return v0
.end method

.method protected getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldPersist()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/preference/j;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlZ()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public iM()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/preference/Preference;->dkX()V

    return-void
.end method

.method public iN()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/preference/Preference;->dkL()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/preference/Preference;->dMi:Z

    return-void
.end method

.method protected iY(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->performClick()V

    return-void
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->mEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLH:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLO:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isVisible()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLS:Z

    return v0
.end method

.method protected notifyChanged()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLZ:Landroid/support/v7/preference/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLZ:Landroid/support/v7/preference/h;

    invoke-interface {v0, p0}, Landroid/support/v7/preference/h;->dlD(Landroid/support/v7/preference/Preference;)V

    :cond_0
    return-void
.end method

.method protected onClick()V
    .locals 0

    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onPrepareForRemoval()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/preference/Preference;->dkL()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLT:Z

    sget-object v0, Landroid/support/v7/preference/Preference$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Wrong state class -- expecting Preference State"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLT:Z

    sget-object v0, Landroid/support/v7/preference/Preference$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    return-object v0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public performClick()V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->onClick()V

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLW:Landroid/support/v7/preference/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLW:Landroid/support/v7/preference/g;

    invoke-interface {v0, p0}, Landroid/support/v7/preference/g;->xb(Landroid/support/v7/preference/Preference;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dky()Landroid/support/v7/preference/k;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlV()Landroid/support/v7/preference/o;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0, p0}, Landroid/support/v7/preference/o;->EX(Landroid/support/v7/preference/Preference;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_3
    return-void
.end method

.method protected persistBoolean(Z)Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldPersist()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    xor-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->dkw(Z)Z

    move-result v0

    if-ne p1, v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/preference/j;->putBoolean(Ljava/lang/String;Z)V

    :goto_0
    return v2

    :cond_2
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlT()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, v0}, Landroid/support/v7/preference/Preference;->dkM(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method protected persistString(Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldPersist()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0, v1}, Landroid/support/v7/preference/Preference;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkv()Landroid/support/v7/preference/j;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/preference/j;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return v2

    :cond_2
    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlT()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, v0}, Landroid/support/v7/preference/Preference;->dkM(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->mEnabled:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Landroid/support/v7/preference/Preference;->mEnabled:Z

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->shouldDisableDependents()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->dkQ(Z)V

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public setIcon(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/support/v4/content/a;->eab(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    iput p1, p0, Landroid/support/v7/preference/Preference;->mIconResId:I

    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-nez p1, :cond_1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    :goto_0
    iput-object p1, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/preference/Preference;->mIconResId:I

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->notifyChanged()V

    :cond_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    goto :goto_0
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/Preference;->mIntent:Landroid/content/Intent;

    return-void
.end method

.method public setKey(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/preference/Preference;->mKey:Ljava/lang/String;

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLN:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkD()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkn()V

    :cond_0
    return-void
.end method

.method public setLayoutResource(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/preference/Preference;->dLQ:I

    return-void
.end method

.method public setOrder(I)V
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/Preference;->dMf:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Landroid/support/v7/preference/Preference;->dMf:I

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkH()V

    :cond_0
    return-void
.end method

.method public setSummary(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    :goto_0
    iput-object p1, p0, Landroid/support/v7/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->notifyChanged()V

    :cond_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mSummary:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    :goto_0
    iput-object p1, p0, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->notifyChanged()V

    :cond_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public final setVisible(Z)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/Preference;->dLS:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Landroid/support/v7/preference/Preference;->dLS:Z

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLZ:Landroid/support/v7/preference/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dLZ:Landroid/support/v7/preference/h;

    invoke-interface {v0, p0}, Landroid/support/v7/preference/h;->dlC(Landroid/support/v7/preference/Preference;)V

    :cond_0
    return-void
.end method

.method public shouldDisableDependents()Z
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected shouldPersist()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/Preference;->dMh:Landroid/support/v7/preference/k;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkN()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkD()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/preference/Preference;->dkO()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
