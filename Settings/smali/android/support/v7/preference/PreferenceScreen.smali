.class public final Landroid/support/v7/preference/PreferenceScreen;
.super Landroid/support/v7/preference/PreferenceGroup;
.source "PreferenceScreen.java"


# instance fields
.field private dMl:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    sget v0, Landroid/support/v7/preference/a;->dHX:I

    const v1, 0x101008b

    invoke-static {p1, v0, v1}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/preference/PreferenceGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/preference/PreferenceScreen;->dMl:Z

    return-void
.end method


# virtual methods
.method public dlb()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/preference/PreferenceScreen;->dMl:Z

    return v0
.end method

.method protected dlc()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dld(Z)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceScreen;->dlk()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot change the usage of generated IDs while attached to the preference hierarchy"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/preference/PreferenceScreen;->dMl:Z

    return-void
.end method

.method protected onClick()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceScreen;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceScreen;->dkF()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceScreen;->dln()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/PreferenceScreen;->dky()Landroid/support/v7/preference/k;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlQ()Landroid/support/v7/preference/q;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0, p0}, Landroid/support/v7/preference/q;->djn(Landroid/support/v7/preference/PreferenceScreen;)V

    :cond_2
    return-void
.end method
