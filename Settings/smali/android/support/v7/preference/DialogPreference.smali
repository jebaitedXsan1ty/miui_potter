.class public abstract Landroid/support/v7/preference/DialogPreference;
.super Landroid/support/v7/preference/Preference;
.source "DialogPreference.java"


# instance fields
.field private dMt:Ljava/lang/CharSequence;

.field private dMu:I

.field private dMv:Ljava/lang/CharSequence;

.field private dMw:Ljava/lang/CharSequence;

.field private dMx:Landroid/graphics/drawable/Drawable;

.field private dMy:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    sget v0, Landroid/support/v7/preference/a;->dHS:I

    const v1, 0x1010091

    invoke-static {p1, v0, v1}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    sget-object v0, Landroid/support/v7/preference/d;->dIO:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Landroid/support/v7/preference/d;->dIY:I

    sget v2, Landroid/support/v7/preference/d;->dIS:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/DialogPreference;->dMy:Ljava/lang/CharSequence;

    iget-object v1, p0, Landroid/support/v7/preference/DialogPreference;->dMy:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/preference/DialogPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/DialogPreference;->dMy:Ljava/lang/CharSequence;

    :cond_0
    sget v1, Landroid/support/v7/preference/d;->dIX:I

    sget v2, Landroid/support/v7/preference/d;->dIR:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/DialogPreference;->dMv:Ljava/lang/CharSequence;

    sget v1, Landroid/support/v7/preference/d;->dIV:I

    sget v2, Landroid/support/v7/preference/d;->dIP:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZE(Landroid/content/res/TypedArray;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/DialogPreference;->dMx:Landroid/graphics/drawable/Drawable;

    sget v1, Landroid/support/v7/preference/d;->dJa:I

    sget v2, Landroid/support/v7/preference/d;->dIU:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/DialogPreference;->dMw:Ljava/lang/CharSequence;

    sget v1, Landroid/support/v7/preference/d;->dIZ:I

    sget v2, Landroid/support/v7/preference/d;->dIT:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/preference/DialogPreference;->dMt:Ljava/lang/CharSequence;

    sget v1, Landroid/support/v7/preference/d;->dIW:I

    sget v2, Landroid/support/v7/preference/d;->dIQ:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/content/a/a;->dZy(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Landroid/support/v7/preference/DialogPreference;->dMu:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public dlA()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/DialogPreference;->dMt:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public dlB(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/DialogPreference;->dMw:Ljava/lang/CharSequence;

    return-void
.end method

.method public dlp(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/preference/DialogPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/DialogPreference;->dlu(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public dlq()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/DialogPreference;->dMy:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public dlr(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/DialogPreference;->dMx:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public dls()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/DialogPreference;->dMw:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public dlt()I
    .locals 1

    iget v0, p0, Landroid/support/v7/preference/DialogPreference;->dMu:I

    return v0
.end method

.method public dlu(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/DialogPreference;->dMt:Ljava/lang/CharSequence;

    return-void
.end method

.method public dlv(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/preference/DialogPreference;->dMy:Ljava/lang/CharSequence;

    return-void
.end method

.method public dlw()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/DialogPreference;->dMv:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public dlx()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/preference/DialogPreference;->dMx:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public dly(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/preference/DialogPreference;->dMu:I

    return-void
.end method

.method public dlz(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/preference/DialogPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/preference/DialogPreference;->dlB(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onClick()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/preference/DialogPreference;->dky()Landroid/support/v7/preference/k;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v7/preference/k;->dlF(Landroid/support/v7/preference/Preference;)V

    return-void
.end method
