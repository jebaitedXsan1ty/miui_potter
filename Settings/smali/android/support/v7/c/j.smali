.class public Landroid/support/v7/c/j;
.super Ljava/lang/Object;
.source "BatchingListUpdateCallback.java"

# interfaces
.implements Landroid/support/v7/c/i;


# instance fields
.field erB:I

.field final erC:Landroid/support/v7/c/i;

.field erD:I

.field erE:Ljava/lang/Object;

.field erF:I


# direct methods
.method public constructor <init>(Landroid/support/v7/c/i;)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/c/j;->erB:I

    iput v1, p0, Landroid/support/v7/c/j;->erD:I

    iput v1, p0, Landroid/support/v7/c/j;->erF:I

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/c/j;->erE:Ljava/lang/Object;

    iput-object p1, p0, Landroid/support/v7/c/j;->erC:Landroid/support/v7/c/i;

    return-void
.end method


# virtual methods
.method public dJh(II)V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/c/j;->dJl()V

    iget-object v0, p0, Landroid/support/v7/c/j;->erC:Landroid/support/v7/c/i;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/c/i;->dJh(II)V

    return-void
.end method

.method public dJi(II)V
    .locals 3

    const/4 v2, 0x2

    iget v0, p0, Landroid/support/v7/c/j;->erB:I

    if-ne v0, v2, :cond_0

    iget v0, p0, Landroid/support/v7/c/j;->erD:I

    if-lt v0, p1, :cond_0

    iget v0, p0, Landroid/support/v7/c/j;->erD:I

    add-int v1, p1, p2

    if-gt v0, v1, :cond_0

    iget v0, p0, Landroid/support/v7/c/j;->erF:I

    add-int/2addr v0, p2

    iput v0, p0, Landroid/support/v7/c/j;->erF:I

    iput p1, p0, Landroid/support/v7/c/j;->erD:I

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/c/j;->dJl()V

    iput p1, p0, Landroid/support/v7/c/j;->erD:I

    iput p2, p0, Landroid/support/v7/c/j;->erF:I

    iput v2, p0, Landroid/support/v7/c/j;->erB:I

    return-void
.end method

.method public dJj(IILjava/lang/Object;)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x1

    iget v1, p0, Landroid/support/v7/c/j;->erB:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Landroid/support/v7/c/j;->erD:I

    iget v2, p0, Landroid/support/v7/c/j;->erF:I

    add-int/2addr v1, v2

    if-gt p1, v1, :cond_0

    add-int v1, p1, p2

    iget v2, p0, Landroid/support/v7/c/j;->erD:I

    if-ge v1, v2, :cond_1

    :cond_0
    :goto_0
    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    iget v0, p0, Landroid/support/v7/c/j;->erD:I

    iget v1, p0, Landroid/support/v7/c/j;->erF:I

    add-int/2addr v0, v1

    iget v1, p0, Landroid/support/v7/c/j;->erD:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/c/j;->erD:I

    add-int v1, p1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Landroid/support/v7/c/j;->erD:I

    sub-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/c/j;->erF:I

    return-void

    :cond_1
    iget-object v1, p0, Landroid/support/v7/c/j;->erE:Ljava/lang/Object;

    if-ne v1, p3, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/c/j;->dJl()V

    iput p1, p0, Landroid/support/v7/c/j;->erD:I

    iput p2, p0, Landroid/support/v7/c/j;->erF:I

    iput-object p3, p0, Landroid/support/v7/c/j;->erE:Ljava/lang/Object;

    iput v3, p0, Landroid/support/v7/c/j;->erB:I

    return-void
.end method

.method public dJk(II)V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Landroid/support/v7/c/j;->erB:I

    if-ne v0, v2, :cond_0

    iget v0, p0, Landroid/support/v7/c/j;->erD:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Landroid/support/v7/c/j;->erD:I

    iget v1, p0, Landroid/support/v7/c/j;->erF:I

    add-int/2addr v0, v1

    if-gt p1, v0, :cond_0

    iget v0, p0, Landroid/support/v7/c/j;->erF:I

    add-int/2addr v0, p2

    iput v0, p0, Landroid/support/v7/c/j;->erF:I

    iget v0, p0, Landroid/support/v7/c/j;->erD:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/c/j;->erD:I

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/c/j;->dJl()V

    iput p1, p0, Landroid/support/v7/c/j;->erD:I

    iput p2, p0, Landroid/support/v7/c/j;->erF:I

    iput v2, p0, Landroid/support/v7/c/j;->erB:I

    return-void
.end method

.method public dJl()V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Landroid/support/v7/c/j;->erB:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Landroid/support/v7/c/j;->erB:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/c/j;->erE:Ljava/lang/Object;

    iput v4, p0, Landroid/support/v7/c/j;->erB:I

    return-void

    :pswitch_0
    iget-object v0, p0, Landroid/support/v7/c/j;->erC:Landroid/support/v7/c/i;

    iget v1, p0, Landroid/support/v7/c/j;->erD:I

    iget v2, p0, Landroid/support/v7/c/j;->erF:I

    invoke-interface {v0, v1, v2}, Landroid/support/v7/c/i;->dJk(II)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Landroid/support/v7/c/j;->erC:Landroid/support/v7/c/i;

    iget v1, p0, Landroid/support/v7/c/j;->erD:I

    iget v2, p0, Landroid/support/v7/c/j;->erF:I

    invoke-interface {v0, v1, v2}, Landroid/support/v7/c/i;->dJi(II)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Landroid/support/v7/c/j;->erC:Landroid/support/v7/c/i;

    iget v1, p0, Landroid/support/v7/c/j;->erD:I

    iget v2, p0, Landroid/support/v7/c/j;->erF:I

    iget-object v3, p0, Landroid/support/v7/c/j;->erE:Ljava/lang/Object;

    invoke-interface {v0, v1, v2, v3}, Landroid/support/v7/c/i;->dJj(IILjava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
