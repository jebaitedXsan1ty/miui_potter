.class public Landroid/support/v4/g/a;
.super Ljava/lang/Object;
.source "FontsContractCompat.java"


# instance fields
.field private final eSk:I

.field private final eSl:I

.field private final eSm:Landroid/net/Uri;

.field private final eSn:I

.field private final eSo:Z


# direct methods
.method public constructor <init>(Landroid/net/Uri;IIZI)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/support/v4/a/m;->dTe(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Landroid/support/v4/g/a;->eSm:Landroid/net/Uri;

    iput p2, p0, Landroid/support/v4/g/a;->eSn:I

    iput p3, p0, Landroid/support/v4/g/a;->eSk:I

    iput-boolean p4, p0, Landroid/support/v4/g/a;->eSo:Z

    iput p5, p0, Landroid/support/v4/g/a;->eSl:I

    return-void
.end method


# virtual methods
.method public eng()I
    .locals 1

    iget v0, p0, Landroid/support/v4/g/a;->eSn:I

    return v0
.end method

.method public enh()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/g/a;->eSo:Z

    return v0
.end method

.method public eni()I
    .locals 1

    iget v0, p0, Landroid/support/v4/g/a;->eSk:I

    return v0
.end method

.method public enj()I
    .locals 1

    iget v0, p0, Landroid/support/v4/g/a;->eSl:I

    return v0
.end method

.method public enk()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/g/a;->eSm:Landroid/net/Uri;

    return-object v0
.end method
