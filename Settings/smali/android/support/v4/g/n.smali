.class final Landroid/support/v4/g/n;
.super Ljava/lang/Object;
.source "FontsContractCompat.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic eSU:Ljava/lang/String;

.field final synthetic eSV:Landroid/support/v4/g/k;

.field final synthetic eSW:I

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/support/v4/g/k;ILjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/g/n;->val$context:Landroid/content/Context;

    iput-object p2, p0, Landroid/support/v4/g/n;->eSV:Landroid/support/v4/g/k;

    iput p3, p0, Landroid/support/v4/g/n;->eSW:I

    iput-object p4, p0, Landroid/support/v4/g/n;->eSU:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Landroid/graphics/Typeface;
    .locals 3

    iget-object v0, p0, Landroid/support/v4/g/n;->val$context:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/g/n;->eSV:Landroid/support/v4/g/k;

    iget v2, p0, Landroid/support/v4/g/n;->eSW:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/g/i;->enz(Landroid/content/Context;Landroid/support/v4/g/k;I)Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/support/v4/g/i;->enE()Landroid/support/v4/a/h;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/g/n;->eSU:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/a/h;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v4/g/n;->call()Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method
