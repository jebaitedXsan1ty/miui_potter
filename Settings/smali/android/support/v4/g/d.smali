.class public Landroid/support/v4/g/d;
.super Ljava/lang/Object;
.source "SelfDestructiveThread.java"


# instance fields
.field private final eSA:I

.field private final eSB:Ljava/lang/String;

.field private final eSC:I

.field private eSw:Landroid/os/Handler;

.field private eSx:Landroid/os/Handler$Callback;

.field private eSy:I

.field private eSz:Landroid/os/HandlerThread;

.field private final mLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/support/v4/g/d;->mLock:Ljava/lang/Object;

    new-instance v0, Landroid/support/v4/g/g;

    invoke-direct {v0, p0}, Landroid/support/v4/g/g;-><init>(Landroid/support/v4/g/d;)V

    iput-object v0, p0, Landroid/support/v4/g/d;->eSx:Landroid/os/Handler$Callback;

    iput-object p1, p0, Landroid/support/v4/g/d;->eSB:Ljava/lang/String;

    iput p2, p0, Landroid/support/v4/g/d;->eSC:I

    iput p3, p0, Landroid/support/v4/g/d;->eSA:I

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/g/d;->eSy:I

    return-void
.end method

.method static synthetic enn(Landroid/support/v4/g/d;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/g/d;->ent()V

    return-void
.end method

.method private enp(Ljava/lang/Runnable;)V
    .locals 4

    iget-object v1, p0, Landroid/support/v4/g/d;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/g/d;->eSz:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    iget-object v2, p0, Landroid/support/v4/g/d;->eSB:Ljava/lang/String;

    iget v3, p0, Landroid/support/v4/g/d;->eSC:I

    invoke-direct {v0, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Landroid/support/v4/g/d;->eSz:Landroid/os/HandlerThread;

    iget-object v0, p0, Landroid/support/v4/g/d;->eSz:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v4/g/d;->eSz:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v4/g/d;->eSx:Landroid/os/Handler$Callback;

    invoke-direct {v0, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Landroid/support/v4/g/d;->eSw:Landroid/os/Handler;

    iget v0, p0, Landroid/support/v4/g/d;->eSy:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/g/d;->eSy:I

    :cond_0
    iget-object v0, p0, Landroid/support/v4/g/d;->eSw:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Landroid/support/v4/g/d;->eSw:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v4/g/d;->eSw:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic enq(Landroid/support/v4/g/d;Ljava/lang/Runnable;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/g/d;->enr(Ljava/lang/Runnable;)V

    return-void
.end method

.method private enr(Ljava/lang/Runnable;)V
    .locals 6

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    iget-object v1, p0, Landroid/support/v4/g/d;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/g/d;->eSw:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Landroid/support/v4/g/d;->eSw:Landroid/os/Handler;

    iget-object v2, p0, Landroid/support/v4/g/d;->eSw:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    iget v3, p0, Landroid/support/v4/g/d;->eSA:I

    int-to-long v4, v3

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private ent()V
    .locals 3

    iget-object v1, p0, Landroid/support/v4/g/d;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/g/d;->eSw:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/g/d;->eSz:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/g/d;->eSz:Landroid/os/HandlerThread;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/g/d;->eSw:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public eno(Ljava/util/concurrent/Callable;Landroid/support/v4/g/e;)V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Landroid/support/v4/g/b;

    invoke-direct {v1, p0, p1, v0, p2}, Landroid/support/v4/g/b;-><init>(Landroid/support/v4/g/d;Ljava/util/concurrent/Callable;Landroid/os/Handler;Landroid/support/v4/g/e;)V

    invoke-direct {p0, v1}, Landroid/support/v4/g/d;->enp(Ljava/lang/Runnable;)V

    return-void
.end method

.method public ens(Ljava/util/concurrent/Callable;I)Ljava/lang/Object;
    .locals 10

    new-instance v4, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v4}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v6

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v5, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    new-instance v0, Landroid/support/v4/g/o;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/g/o;-><init>(Landroid/support/v4/g/d;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/Callable;Ljava/util/concurrent/locks/ReentrantLock;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/locks/Condition;)V

    invoke-direct {p0, v0}, Landroid/support/v4/g/d;->enp(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v8, p2

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    :cond_1
    :try_start_2
    invoke-interface {v6, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    :goto_0
    :try_start_3
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-object v0

    :catch_0
    move-exception v3

    goto :goto_0

    :cond_2
    const-wide/16 v8, 0x0

    cmp-long v3, v0, v8

    if-gtz v3, :cond_1

    :try_start_4
    new-instance v0, Ljava/lang/InterruptedException;

    const-string/jumbo v1, "timeout"

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public getGeneration()I
    .locals 2

    iget-object v0, p0, Landroid/support/v4/g/d;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget v1, p0, Landroid/support/v4/g/d;->eSy:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public isRunning()Z
    .locals 2

    iget-object v1, p0, Landroid/support/v4/g/d;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/g/d;->eSz:Landroid/os/HandlerThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
