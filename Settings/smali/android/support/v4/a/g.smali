.class final Landroid/support/v4/a/g;
.super Ljava/lang/Object;
.source "MapCollections.java"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field eBA:I

.field final eBB:I

.field eBC:I

.field final synthetic eBD:Landroid/support/v4/a/l;

.field eBE:Z


# direct methods
.method constructor <init>(Landroid/support/v4/a/l;I)V
    .locals 1

    iput-object p1, p0, Landroid/support/v4/a/g;->eBD:Landroid/support/v4/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/a/g;->eBE:Z

    iput p2, p0, Landroid/support/v4/a/g;->eBB:I

    invoke-virtual {p1}, Landroid/support/v4/a/l;->dSZ()I

    move-result v0

    iput v0, p0, Landroid/support/v4/a/g;->eBA:I

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    iget v0, p0, Landroid/support/v4/a/g;->eBC:I

    iget v1, p0, Landroid/support/v4/a/g;->eBA:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 3

    invoke-virtual {p0}, Landroid/support/v4/a/g;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/g;->eBD:Landroid/support/v4/a/l;

    iget v1, p0, Landroid/support/v4/a/g;->eBC:I

    iget v2, p0, Landroid/support/v4/a/g;->eBB:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/l;->dSO(II)Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Landroid/support/v4/a/g;->eBC:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Landroid/support/v4/a/g;->eBC:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v4/a/g;->eBE:Z

    return-object v0
.end method

.method public remove()V
    .locals 2

    iget-boolean v0, p0, Landroid/support/v4/a/g;->eBE:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Landroid/support/v4/a/g;->eBC:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v4/a/g;->eBC:I

    iget v0, p0, Landroid/support/v4/a/g;->eBA:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v4/a/g;->eBA:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/a/g;->eBE:Z

    iget-object v0, p0, Landroid/support/v4/a/g;->eBD:Landroid/support/v4/a/l;

    iget v1, p0, Landroid/support/v4/a/g;->eBC:I

    invoke-virtual {v0, v1}, Landroid/support/v4/a/l;->dTa(I)V

    return-void
.end method
