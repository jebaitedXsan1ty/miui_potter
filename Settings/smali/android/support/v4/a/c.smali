.class public final Landroid/support/v4/a/c;
.super Ljava/lang/Object;
.source "ArraySet.java"

# interfaces
.implements Ljava/util/Collection;
.implements Ljava/util/Set;


# static fields
.field static eBj:[Ljava/lang/Object;

.field static eBk:I

.field static eBl:I

.field private static final eBn:[Ljava/lang/Object;

.field private static final eBr:[I

.field static eBs:[Ljava/lang/Object;


# instance fields
.field eBi:I

.field eBm:Landroid/support/v4/a/l;

.field eBo:[I

.field eBp:[Ljava/lang/Object;

.field final eBq:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [I

    sput-object v0, Landroid/support/v4/a/c;->eBr:[I

    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, Landroid/support/v4/a/c;->eBn:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Landroid/support/v4/a/c;-><init>(IZ)V

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p2, p0, Landroid/support/v4/a/c;->eBq:Z

    if-nez p1, :cond_0

    sget-object v0, Landroid/support/v4/a/c;->eBr:[I

    iput-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    sget-object v0, Landroid/support/v4/a/c;->eBn:[Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    :goto_0
    iput v1, p0, Landroid/support/v4/a/c;->eBi:I

    return-void

    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v4/a/c;->dSs(I)V

    goto :goto_0
.end method

.method private dSs(I)V
    .locals 4

    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    const-class v1, Landroid/support/v4/a/c;

    monitor-enter v1

    :try_start_0
    sget-object v0, Landroid/support/v4/a/c;->eBs:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    sget-object v2, Landroid/support/v4/a/c;->eBs:[Ljava/lang/Object;

    iput-object v2, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    const/4 v0, 0x0

    aget-object v0, v2, v0

    check-cast v0, [Ljava/lang/Object;

    sput-object v0, Landroid/support/v4/a/c;->eBs:[Ljava/lang/Object;

    const/4 v0, 0x1

    aget-object v0, v2, v0

    check-cast v0, [I

    iput-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    const/4 v0, 0x0

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    sget v0, Landroid/support/v4/a/c;->eBl:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Landroid/support/v4/a/c;->eBl:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :cond_0
    monitor-exit v1

    :cond_1
    new-array v0, p1, [I

    iput-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    const-class v1, Landroid/support/v4/a/c;

    monitor-enter v1

    :try_start_1
    sget-object v0, Landroid/support/v4/a/c;->eBj:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    sget-object v2, Landroid/support/v4/a/c;->eBj:[Ljava/lang/Object;

    iput-object v2, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    const/4 v0, 0x0

    aget-object v0, v2, v0

    check-cast v0, [Ljava/lang/Object;

    sput-object v0, Landroid/support/v4/a/c;->eBj:[Ljava/lang/Object;

    const/4 v0, 0x1

    aget-object v0, v2, v0

    check-cast v0, [I

    iput-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    const/4 v0, 0x0

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    sget v0, Landroid/support/v4/a/c;->eBk:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Landroid/support/v4/a/c;->eBk:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v1

    return-void

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static dSt([I[Ljava/lang/Object;I)V
    .locals 4

    const/16 v2, 0xa

    const/4 v3, 0x2

    array-length v0, p0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    const-class v1, Landroid/support/v4/a/c;

    monitor-enter v1

    :try_start_0
    sget v0, Landroid/support/v4/a/c;->eBl:I

    if-ge v0, v2, :cond_1

    sget-object v0, Landroid/support/v4/a/c;->eBs:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, p1, v2

    const/4 v0, 0x1

    aput-object p0, p1, v0

    add-int/lit8 v0, p2, -0x1

    :goto_0
    if-lt v0, v3, :cond_0

    const/4 v2, 0x0

    aput-object v2, p1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    sput-object p1, Landroid/support/v4/a/c;->eBs:[Ljava/lang/Object;

    sget v0, Landroid/support/v4/a/c;->eBl:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Landroid/support/v4/a/c;->eBl:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_1
    monitor-exit v1

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    array-length v0, p0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const-class v1, Landroid/support/v4/a/c;

    monitor-enter v1

    :try_start_1
    sget v0, Landroid/support/v4/a/c;->eBk:I

    if-ge v0, v2, :cond_1

    sget-object v0, Landroid/support/v4/a/c;->eBj:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, p1, v2

    const/4 v0, 0x1

    aput-object p0, p1, v0

    add-int/lit8 v0, p2, -0x1

    :goto_2
    if-lt v0, v3, :cond_4

    const/4 v2, 0x0

    aput-object v2, p1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_4
    sput-object p1, Landroid/support/v4/a/c;->eBj:[Ljava/lang/Object;

    sget v0, Landroid/support/v4/a/c;->eBk:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Landroid/support/v4/a/c;->eBk:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private dSu(Ljava/lang/Object;I)I
    .locals 4

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    if-nez v2, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    invoke-static {v0, v2, p2}, Landroid/support/v4/a/t;->dTs([III)I

    move-result v3

    if-gez v3, :cond_1

    return v3

    :cond_1
    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v0, v0, v3

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v3

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    aget v0, v0, v1

    if-ne v0, p2, :cond_4

    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    return v1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v3, -0x1

    :goto_1
    if-ltz v0, :cond_6

    iget-object v2, p0, Landroid/support/v4/a/c;->eBo:[I

    aget v2, v2, v0

    if-ne v2, p2, :cond_6

    iget-object v2, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    return v0

    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_6
    not-int v0, v1

    return v0
.end method

.method private dSv()Landroid/support/v4/a/l;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/c;->eBm:Landroid/support/v4/a/l;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/a/s;

    invoke-direct {v0, p0}, Landroid/support/v4/a/s;-><init>(Landroid/support/v4/a/c;)V

    iput-object v0, p0, Landroid/support/v4/a/c;->eBm:Landroid/support/v4/a/l;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/c;->eBm:Landroid/support/v4/a/l;

    return-object v0
.end method

.method private dSx()I
    .locals 4

    const/4 v1, 0x0

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    if-nez v2, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    invoke-static {v0, v2, v1}, Landroid/support/v4/a/t;->dTs([III)I

    move-result v3

    if-gez v3, :cond_1

    return v3

    :cond_1
    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v0, v0, v3

    if-nez v0, :cond_2

    return v3

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    aget v0, v0, v1

    if-nez v0, :cond_4

    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v0, v0, v1

    if-nez v0, :cond_3

    return v1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v3, -0x1

    :goto_1
    if-ltz v0, :cond_6

    iget-object v2, p0, Landroid/support/v4/a/c;->eBo:[I

    aget v2, v2, v0

    if-nez v2, :cond_6

    iget-object v2, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    return v0

    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_6
    not-int v0, v1

    return v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 8

    const/16 v1, 0x8

    const/4 v2, 0x4

    const/4 v4, 0x0

    if-nez p1, :cond_0

    invoke-direct {p0}, Landroid/support/v4/a/c;->dSx()I

    move-result v0

    move v3, v4

    :goto_0
    if-ltz v0, :cond_2

    return v4

    :cond_0
    iget-boolean v0, p0, Landroid/support/v4/a/c;->eBq:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    :goto_1
    invoke-direct {p0, p1, v0}, Landroid/support/v4/a/c;->dSu(Ljava/lang/Object;I)I

    move-result v3

    move v7, v3

    move v3, v0

    move v0, v7

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    not-int v5, v0

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    iget-object v6, p0, Landroid/support/v4/a/c;->eBo:[I

    array-length v6, v6

    if-lt v0, v6, :cond_4

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    if-lt v0, v1, :cond_6

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    iget v1, p0, Landroid/support/v4/a/c;->eBi:I

    shr-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :goto_2
    iget-object v1, p0, Landroid/support/v4/a/c;->eBo:[I

    iget-object v2, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    invoke-direct {p0, v0}, Landroid/support/v4/a/c;->dSs(I)V

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    array-length v0, v0

    if-lez v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    array-length v6, v1

    invoke-static {v1, v4, v0, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    array-length v6, v2

    invoke-static {v2, v4, v0, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-static {v1, v2, v0}, Landroid/support/v4/a/c;->dSt([I[Ljava/lang/Object;I)V

    :cond_4
    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    if-ge v5, v0, :cond_5

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    iget-object v1, p0, Landroid/support/v4/a/c;->eBo:[I

    add-int/lit8 v2, v5, 0x1

    iget v4, p0, Landroid/support/v4/a/c;->eBi:I

    sub-int/2addr v4, v5

    invoke-static {v0, v5, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    add-int/lit8 v2, v5, 0x1

    iget v4, p0, Landroid/support/v4/a/c;->eBi:I

    sub-int/2addr v4, v5

    invoke-static {v0, v5, v1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    aput v3, v0, v5

    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aput-object p1, v0, v5

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/a/c;->eBi:I

    const/4 v0, 0x1

    return v0

    :cond_6
    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    if-lt v0, v2, :cond_7

    move v0, v1

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 3

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/a/c;->dSz(I)V

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/support/v4/a/c;->add(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v0, v2

    goto :goto_0

    :cond_0
    return v0
.end method

.method public clear()V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    iget-object v1, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/a/c;->dSt([I[Ljava/lang/Object;I)V

    sget-object v0, Landroid/support/v4/a/c;->eBr:[I

    iput-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    sget-object v0, Landroid/support/v4/a/c;->eBn:[Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iput v3, p0, Landroid/support/v4/a/c;->eBi:I

    :cond_0
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Landroid/support/v4/a/c;->dSA(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 2

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v4/a/c;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public dSA(Ljava/lang/Object;)I
    .locals 1

    if-nez p1, :cond_0

    invoke-direct {p0}, Landroid/support/v4/a/c;->dSx()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Landroid/support/v4/a/c;->eBq:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    :goto_1
    invoke-direct {p0, p1, v0}, Landroid/support/v4/a/c;->dSu(Ljava/lang/Object;I)I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public dSw(I)Ljava/lang/Object;
    .locals 6

    const/16 v0, 0x8

    const/4 v4, 0x0

    iget-object v1, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v1, v1, p1

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    iget-object v2, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iget v3, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-static {v0, v2, v3}, Landroid/support/v4/a/c;->dSt([I[Ljava/lang/Object;I)V

    sget-object v0, Landroid/support/v4/a/c;->eBr:[I

    iput-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    sget-object v0, Landroid/support/v4/a/c;->eBn:[Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iput v4, p0, Landroid/support/v4/a/c;->eBi:I

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget-object v2, p0, Landroid/support/v4/a/c;->eBo:[I

    array-length v2, v2

    if-le v2, v0, :cond_4

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    iget-object v3, p0, Landroid/support/v4/a/c;->eBo:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    if-ge v2, v3, :cond_4

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    if-le v2, v0, :cond_2

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Landroid/support/v4/a/c;->eBo:[I

    iget-object v3, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    invoke-direct {p0, v0}, Landroid/support/v4/a/c;->dSs(I)V

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v4/a/c;->eBi:I

    if-lez p1, :cond_3

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    invoke-static {v2, v4, v0, v4, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    invoke-static {v3, v4, v0, v4, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    if-ge p1, v0, :cond_0

    add-int/lit8 v0, p1, 0x1

    iget-object v4, p0, Landroid/support/v4/a/c;->eBo:[I

    iget v5, p0, Landroid/support/v4/a/c;->eBi:I

    sub-int/2addr v5, p1

    invoke-static {v2, v0, v4, p1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, p1, 0x1

    iget-object v2, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iget v4, p0, Landroid/support/v4/a/c;->eBi:I

    sub-int/2addr v4, p1

    invoke-static {v3, v0, v2, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_4
    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v4/a/c;->eBi:I

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    if-ge p1, v0, :cond_5

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Landroid/support/v4/a/c;->eBo:[I

    iget v4, p0, Landroid/support/v4/a/c;->eBi:I

    sub-int/2addr v4, p1

    invoke-static {v0, v2, v3, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iget v4, p0, Landroid/support/v4/a/c;->eBi:I

    sub-int/2addr v4, p1

    invoke-static {v0, v2, v3, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    const/4 v3, 0x0

    aput-object v3, v0, v2

    goto :goto_0
.end method

.method public dSy(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public dSz(I)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    array-length v0, v0

    if-ge v0, p1, :cond_1

    iget-object v0, p0, Landroid/support/v4/a/c;->eBo:[I

    iget-object v1, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    invoke-direct {p0, p1}, Landroid/support/v4/a/c;->dSs(I)V

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    if-lez v2, :cond_0

    iget-object v2, p0, Landroid/support/v4/a/c;->eBo:[I

    iget v3, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-static {v0, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iget v3, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/a/c;->dSt([I[Ljava/lang/Object;I)V

    :cond_1
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_0

    return v3

    :cond_0
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_4

    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0}, Landroid/support/v4/a/c;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    if-eq v0, v2, :cond_1

    return v1

    :cond_1
    move v0, v1

    :goto_0
    :try_start_0
    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    if-ge v0, v2, :cond_3

    invoke-virtual {p0, v0}, Landroid/support/v4/a/c;->dSy(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_2

    return v1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    return v1

    :catch_1
    move-exception v0

    return v1

    :cond_3
    return v3

    :cond_4
    return v1
.end method

.method public hashCode()I
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Landroid/support/v4/a/c;->eBo:[I

    iget v3, p0, Landroid/support/v4/a/c;->eBi:I

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    add-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public isEmpty()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v4/a/c;->eBi:I

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/a/c;->dSv()Landroid/support/v4/a/l;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/a/l;->dSS()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Landroid/support/v4/a/c;->dSA(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v4/a/c;->dSw(I)Ljava/lang/Object;

    const/4 v0, 0x1

    return v0

    :cond_0
    return v1
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/support/v4/a/c;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v0, v2

    goto :goto_0

    :cond_0
    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v0, v1

    move v1, v3

    :goto_0
    if-ltz v1, :cond_1

    iget-object v2, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    aget-object v2, v2, v1

    invoke-interface {p1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v1}, Landroid/support/v4/a/c;->dSw(I)Ljava/lang/Object;

    const/4 v0, 0x1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public size()I
    .locals 1

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Landroid/support/v4/a/c;->eBi:I

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4

    const/4 v3, 0x0

    array-length v0, p1

    iget v1, p0, Landroid/support/v4/a/c;->eBi:I

    if-ge v0, v1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    iget v1, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    :goto_0
    iget-object v1, p0, Landroid/support/v4/a/c;->eBp:[Ljava/lang/Object;

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v0

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    if-le v1, v2, :cond_0

    iget v1, p0, Landroid/support/v4/a/c;->eBi:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    :cond_0
    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v4/a/c;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "{}"

    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    mul-int/lit8 v2, v2, 0xe

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/16 v2, 0x7b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    iget v2, p0, Landroid/support/v4/a/c;->eBi:I

    if-ge v0, v2, :cond_3

    if-lez v0, :cond_1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p0, v0}, Landroid/support/v4/a/c;->dSy(I)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p0, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-string/jumbo v2, "(this Set)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
