.class final Landroid/support/v4/app/FragmentState;
.super Ljava/lang/Object;
.source "FragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final eNF:Z

.field eNG:Landroid/os/Bundle;

.field final eNH:Landroid/os/Bundle;

.field final eNI:I

.field final eNJ:Z

.field final eNK:I

.field final eNL:Z

.field final eNM:I

.field eNN:Landroid/support/v4/app/p;

.field final eNO:Z

.field final mClassName:Ljava/lang/String;

.field final mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/support/v4/app/O;

    invoke-direct {v0}, Landroid/support/v4/app/O;-><init>()V

    sput-object v0, Landroid/support/v4/app/FragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->mClassName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/FragmentState;->eNK:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNJ:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/FragmentState;->eNI:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/FragmentState;->eNM:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->mTag:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNL:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNF:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->eNH:Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Landroid/support/v4/app/FragmentState;->eNO:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method constructor <init>(Landroid/support/v4/app/p;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/support/v4/app/p;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->mClassName:Ljava/lang/String;

    iget v0, p1, Landroid/support/v4/app/p;->eKp:I

    iput v0, p0, Landroid/support/v4/app/FragmentState;->eNK:I

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKA:Z

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNJ:Z

    iget v0, p1, Landroid/support/v4/app/p;->eKv:I

    iput v0, p0, Landroid/support/v4/app/FragmentState;->eNI:I

    iget v0, p1, Landroid/support/v4/app/p;->eKq:I

    iput v0, p0, Landroid/support/v4/app/FragmentState;->eNM:I

    iget-object v0, p1, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->mTag:Ljava/lang/String;

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKP:Z

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNL:Z

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKR:Z

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNF:Z

    iget-object v0, p1, Landroid/support/v4/app/p;->eKH:Landroid/os/Bundle;

    iput-object v0, p0, Landroid/support/v4/app/FragmentState;->eNH:Landroid/os/Bundle;

    iget-boolean v0, p1, Landroid/support/v4/app/p;->eKZ:Z

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNO:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public efp(Landroid/support/v4/app/C;Landroid/support/v4/app/k;Landroid/support/v4/app/p;Landroid/support/v4/app/E;)Landroid/support/v4/app/p;
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->eNH:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->eNH:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    if-eqz p2, :cond_3

    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->mClassName:Ljava/lang/String;

    iget-object v2, p0, Landroid/support/v4/app/FragmentState;->eNH:Landroid/os/Bundle;

    invoke-virtual {p2, v0, v1, v2}, Landroid/support/v4/app/k;->ebg(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/p;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    :goto_0
    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    iput-object v1, v0, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget v1, p0, Landroid/support/v4/app/FragmentState;->eNK:I

    invoke-virtual {v0, v1, p3}, Landroid/support/v4/app/p;->ecv(ILandroid/support/v4/app/p;)V

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget-boolean v1, p0, Landroid/support/v4/app/FragmentState;->eNJ:Z

    iput-boolean v1, v0, Landroid/support/v4/app/p;->eKA:Z

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v4/app/p;->eKr:Z

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget v1, p0, Landroid/support/v4/app/FragmentState;->eNI:I

    iput v1, v0, Landroid/support/v4/app/p;->eKv:I

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget v1, p0, Landroid/support/v4/app/FragmentState;->eNM:I

    iput v1, v0, Landroid/support/v4/app/p;->eKq:I

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->mTag:Ljava/lang/String;

    iput-object v1, v0, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget-boolean v1, p0, Landroid/support/v4/app/FragmentState;->eNL:Z

    iput-boolean v1, v0, Landroid/support/v4/app/p;->eKP:Z

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget-boolean v1, p0, Landroid/support/v4/app/FragmentState;->eNF:Z

    iput-boolean v1, v0, Landroid/support/v4/app/p;->eKR:Z

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget-boolean v1, p0, Landroid/support/v4/app/FragmentState;->eNO:Z

    iput-boolean v1, v0, Landroid/support/v4/app/p;->eKZ:Z

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iget-object v1, p1, Landroid/support/v4/app/C;->eMf:Landroid/support/v4/app/an;

    iput-object v1, v0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Instantiated fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    iput-object p4, v0, Landroid/support/v4/app/p;->eKB:Landroid/support/v4/app/E;

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    return-object v0

    :cond_3
    iget-object v1, p0, Landroid/support/v4/app/FragmentState;->mClassName:Ljava/lang/String;

    iget-object v2, p0, Landroid/support/v4/app/FragmentState;->eNH:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Landroid/support/v4/app/p;->ebU(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/p;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/FragmentState;->eNN:Landroid/support/v4/app/p;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->mClassName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/FragmentState;->eNK:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNJ:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/support/v4/app/FragmentState;->eNI:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/support/v4/app/FragmentState;->eNM:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->mTag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNL:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNF:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNH:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Landroid/support/v4/app/FragmentState;->eNO:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/support/v4/app/FragmentState;->eNG:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method
