.class final Landroid/support/v4/app/q;
.super Ljava/lang/Object;
.source "FragmentTransition.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic eLa:Landroid/support/v4/app/p;

.field final synthetic eLb:Ljava/lang/Object;

.field final synthetic eLc:Ljava/lang/Object;

.field final synthetic eLd:Ljava/util/ArrayList;

.field final synthetic eLe:Ljava/util/ArrayList;

.field final synthetic eLf:Landroid/support/v4/app/am;

.field final synthetic eLg:Ljava/util/ArrayList;

.field final synthetic eLh:Landroid/view/View;


# direct methods
.method constructor <init>(Ljava/lang/Object;Landroid/support/v4/app/am;Landroid/view/View;Landroid/support/v4/app/p;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/app/q;->eLc:Ljava/lang/Object;

    iput-object p2, p0, Landroid/support/v4/app/q;->eLf:Landroid/support/v4/app/am;

    iput-object p3, p0, Landroid/support/v4/app/q;->eLh:Landroid/view/View;

    iput-object p4, p0, Landroid/support/v4/app/q;->eLa:Landroid/support/v4/app/p;

    iput-object p5, p0, Landroid/support/v4/app/q;->eLd:Ljava/util/ArrayList;

    iput-object p6, p0, Landroid/support/v4/app/q;->eLe:Ljava/util/ArrayList;

    iput-object p7, p0, Landroid/support/v4/app/q;->eLg:Ljava/util/ArrayList;

    iput-object p8, p0, Landroid/support/v4/app/q;->eLb:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Landroid/support/v4/app/q;->eLc:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/q;->eLf:Landroid/support/v4/app/am;

    iget-object v1, p0, Landroid/support/v4/app/q;->eLc:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/app/q;->eLh:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/am;->efu(Ljava/lang/Object;Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v4/app/q;->eLf:Landroid/support/v4/app/am;

    iget-object v1, p0, Landroid/support/v4/app/q;->eLc:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/app/q;->eLa:Landroid/support/v4/app/p;

    iget-object v3, p0, Landroid/support/v4/app/q;->eLd:Ljava/util/ArrayList;

    iget-object v4, p0, Landroid/support/v4/app/q;->eLh:Landroid/view/View;

    invoke-static {v0, v1, v2, v3, v4}, Landroid/support/v4/app/af;->efa(Landroid/support/v4/app/am;Ljava/lang/Object;Landroid/support/v4/app/p;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/q;->eLe:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/q;->eLg:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/q;->eLb:Ljava/lang/Object;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Landroid/support/v4/app/q;->eLh:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Landroid/support/v4/app/q;->eLf:Landroid/support/v4/app/am;

    iget-object v2, p0, Landroid/support/v4/app/q;->eLb:Ljava/lang/Object;

    iget-object v3, p0, Landroid/support/v4/app/q;->eLg:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3, v0}, Landroid/support/v4/app/am;->efy(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/q;->eLg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/support/v4/app/q;->eLg:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v4/app/q;->eLh:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method
