.class final Landroid/support/v4/app/A;
.super Landroid/support/v4/app/ak;
.source "FragmentManager.java"


# instance fields
.field final synthetic eLN:Landroid/support/v4/app/an;

.field final synthetic eLO:Landroid/view/View;

.field final synthetic eLP:Landroid/support/v4/app/p;

.field final synthetic eLQ:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/support/v4/app/an;Landroid/view/animation/Animation$AnimationListener;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/app/p;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v4/app/A;->eLN:Landroid/support/v4/app/an;

    iput-object p3, p0, Landroid/support/v4/app/A;->eLQ:Landroid/view/ViewGroup;

    iput-object p4, p0, Landroid/support/v4/app/A;->eLO:Landroid/view/View;

    iput-object p5, p0, Landroid/support/v4/app/A;->eLP:Landroid/support/v4/app/p;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Landroid/support/v4/app/ak;-><init>(Landroid/view/animation/Animation$AnimationListener;Landroid/support/v4/app/ak;)V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/ak;->onAnimationEnd(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Landroid/support/v4/app/A;->eLQ:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v4/app/A;->eLO:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->endViewTransition(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v4/app/A;->eLP:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebu()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/A;->eLP:Landroid/support/v4/app/p;

    invoke-virtual {v0, v2}, Landroid/support/v4/app/p;->eci(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v4/app/A;->eLN:Landroid/support/v4/app/an;

    iget-object v1, p0, Landroid/support/v4/app/A;->eLP:Landroid/support/v4/app/p;

    iget-object v2, p0, Landroid/support/v4/app/A;->eLP:Landroid/support/v4/app/p;

    invoke-virtual {v2}, Landroid/support/v4/app/p;->ecC()I

    move-result v2

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    :cond_0
    return-void
.end method
