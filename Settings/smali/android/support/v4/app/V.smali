.class final Landroid/support/v4/app/V;
.super Landroid/animation/AnimatorListenerAdapter;
.source "FragmentManager.java"


# instance fields
.field final synthetic eNi:Landroid/support/v4/app/an;

.field final synthetic eNj:Landroid/view/View;

.field final synthetic eNk:Landroid/support/v4/app/p;

.field final synthetic eNl:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/support/v4/app/an;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/app/p;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/app/V;->eNi:Landroid/support/v4/app/an;

    iput-object p2, p0, Landroid/support/v4/app/V;->eNl:Landroid/view/ViewGroup;

    iput-object p3, p0, Landroid/support/v4/app/V;->eNj:Landroid/view/View;

    iput-object p4, p0, Landroid/support/v4/app/V;->eNk:Landroid/support/v4/app/p;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/V;->eNl:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v4/app/V;->eNj:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->endViewTransition(Landroid/view/View;)V

    invoke-virtual {p1, p0}, Landroid/animation/Animator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Landroid/support/v4/app/V;->eNk:Landroid/support/v4/app/p;

    iget-object v0, v0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/V;->eNk:Landroid/support/v4/app/p;

    iget-object v0, v0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method
