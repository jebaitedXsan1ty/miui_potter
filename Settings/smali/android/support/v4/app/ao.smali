.class final Landroid/support/v4/app/ao;
.super Ljava/lang/Object;
.source "FragmentTransitionCompat21.java"

# interfaces
.implements Landroid/transition/Transition$TransitionListener;


# instance fields
.field final synthetic ePn:Ljava/util/ArrayList;

.field final synthetic ePo:Ljava/lang/Object;

.field final synthetic ePp:Ljava/lang/Object;

.field final synthetic ePq:Ljava/lang/Object;

.field final synthetic ePr:Ljava/util/ArrayList;

.field final synthetic ePs:Landroid/support/v4/app/ah;

.field final synthetic ePt:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Landroid/support/v4/app/ah;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/app/ao;->ePs:Landroid/support/v4/app/ah;

    iput-object p2, p0, Landroid/support/v4/app/ao;->ePp:Ljava/lang/Object;

    iput-object p3, p0, Landroid/support/v4/app/ao;->ePn:Ljava/util/ArrayList;

    iput-object p4, p0, Landroid/support/v4/app/ao;->ePo:Ljava/lang/Object;

    iput-object p5, p0, Landroid/support/v4/app/ao;->ePr:Ljava/util/ArrayList;

    iput-object p6, p0, Landroid/support/v4/app/ao;->ePq:Ljava/lang/Object;

    iput-object p7, p0, Landroid/support/v4/app/ao;->ePt:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionCancel(Landroid/transition/Transition;)V
    .locals 0

    return-void
.end method

.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 0

    return-void
.end method

.method public onTransitionPause(Landroid/transition/Transition;)V
    .locals 0

    return-void
.end method

.method public onTransitionResume(Landroid/transition/Transition;)V
    .locals 0

    return-void
.end method

.method public onTransitionStart(Landroid/transition/Transition;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v4/app/ao;->ePp:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/ao;->ePs:Landroid/support/v4/app/ah;

    iget-object v1, p0, Landroid/support/v4/app/ao;->ePp:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/app/ao;->ePn:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/ah;->efy(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/ao;->ePo:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/ao;->ePs:Landroid/support/v4/app/ah;

    iget-object v1, p0, Landroid/support/v4/app/ao;->ePo:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/app/ao;->ePr:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/ah;->efy(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/ao;->ePq:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/ao;->ePs:Landroid/support/v4/app/ah;

    iget-object v1, p0, Landroid/support/v4/app/ao;->ePq:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v4/app/ao;->ePt:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/ah;->efy(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_2
    return-void
.end method
