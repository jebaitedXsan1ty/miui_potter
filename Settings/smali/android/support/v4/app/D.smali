.class public Landroid/support/v4/app/D;
.super Landroid/app/Activity;
.source "SupportActivity.java"

# interfaces
.implements Landroid/arch/lifecycle/f;


# instance fields
.field private eMm:Landroid/support/v4/a/a;

.field private eMn:Landroid/arch/lifecycle/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Landroid/support/v4/a/a;

    invoke-direct {v0}, Landroid/support/v4/a/a;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/D;->eMm:Landroid/support/v4/a/a;

    new-instance v0, Landroid/arch/lifecycle/b;

    invoke-direct {v0, p0}, Landroid/arch/lifecycle/b;-><init>(Landroid/arch/lifecycle/f;)V

    iput-object v0, p0, Landroid/support/v4/app/D;->eMn:Landroid/arch/lifecycle/b;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/arch/lifecycle/d;->eqi(Landroid/app/Activity;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/D;->eMn:Landroid/arch/lifecycle/b;

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$State;->eWx:Landroid/arch/lifecycle/Lifecycle$State;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/b;->epS(Landroid/arch/lifecycle/Lifecycle$State;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
