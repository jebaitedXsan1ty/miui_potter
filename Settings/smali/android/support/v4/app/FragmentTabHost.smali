.class public Landroid/support/v4/app/FragmentTabHost;
.super Landroid/widget/TabHost;
.source "FragmentTabHost.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# instance fields
.field private eNA:Landroid/support/v4/app/au;

.field private eNB:Z

.field private eNC:I

.field private final eND:Ljava/util/ArrayList;

.field private eNx:Landroid/support/v4/app/ar;

.field private eNy:Landroid/widget/TabHost$OnTabChangeListener;

.field private eNz:Landroid/widget/FrameLayout;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Landroid/widget/TabHost;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eND:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v1}, Landroid/support/v4/app/FragmentTabHost;->efo(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/TabHost;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eND:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2}, Landroid/support/v4/app/FragmentTabHost;->efo(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private efk(Ljava/lang/String;Landroid/support/v4/app/N;)Landroid/support/v4/app/N;
    .locals 4

    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentTabHost;->efl(Ljava/lang/String;)Landroid/support/v4/app/au;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/FragmentTabHost;->eNA:Landroid/support/v4/app/au;

    if-eq v1, v0, :cond_3

    if-nez p2, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/FragmentTabHost;->eNx:Landroid/support/v4/app/ar;

    invoke-virtual {v1}, Landroid/support/v4/app/ar;->ehw()Landroid/support/v4/app/N;

    move-result-object p2

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/FragmentTabHost;->eNA:Landroid/support/v4/app/au;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/FragmentTabHost;->eNA:Landroid/support/v4/app/au;

    iget-object v1, v1, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/FragmentTabHost;->eNA:Landroid/support/v4/app/au;

    iget-object v1, v1, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    invoke-virtual {p2, v1}, Landroid/support/v4/app/N;->edA(Landroid/support/v4/app/p;)Landroid/support/v4/app/N;

    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    if-nez v1, :cond_4

    iget-object v1, p0, Landroid/support/v4/app/FragmentTabHost;->mContext:Landroid/content/Context;

    iget-object v2, v0, Landroid/support/v4/app/au;->ePF:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Landroid/support/v4/app/au;->ePE:Landroid/os/Bundle;

    invoke-static {v1, v2, v3}, Landroid/support/v4/app/p;->ebU(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/p;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    iget v1, p0, Landroid/support/v4/app/FragmentTabHost;->eNC:I

    iget-object v2, v0, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    iget-object v3, v0, Landroid/support/v4/app/au;->ePG:Ljava/lang/String;

    invoke-virtual {p2, v1, v2, v3}, Landroid/support/v4/app/N;->edC(ILandroid/support/v4/app/p;Ljava/lang/String;)Landroid/support/v4/app/N;

    :cond_2
    :goto_0
    iput-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNA:Landroid/support/v4/app/au;

    :cond_3
    return-object p2

    :cond_4
    iget-object v1, v0, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    invoke-virtual {p2, v1}, Landroid/support/v4/app/N;->edB(Landroid/support/v4/app/p;)Landroid/support/v4/app/N;

    goto :goto_0
.end method

.method private efl(Ljava/lang/String;)Landroid/support/v4/app/au;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/app/FragmentTabHost;->eND:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eND:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/au;

    iget-object v3, v0, Landroid/support/v4/app/au;->ePG:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private efm(Landroid/content/Context;)V
    .locals 7

    const v2, 0x1020013

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x1

    invoke-virtual {p0, v2}, Landroid/support/v4/app/FragmentTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/FragmentTabHost;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/TabWidget;

    invoke-direct {v1, p1}, Landroid/widget/TabWidget;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TabWidget;->setId(I)V

    invoke-virtual {v1, v5}, Landroid/widget/TabWidget;->setOrientation(I)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v4, v3, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const v2, 0x1020011

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setId(I)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v5, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v4/app/FragmentTabHost;->eNz:Landroid/widget/FrameLayout;

    iget-object v2, p0, Landroid/support/v4/app/FragmentTabHost;->eNz:Landroid/widget/FrameLayout;

    iget v3, p0, Landroid/support/v4/app/FragmentTabHost;->eNC:I

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setId(I)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v2, v4, v5, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method private efn()V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNz:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNC:I

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNz:Landroid/widget/FrameLayout;

    iget-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNz:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No tab content FrameLayout found for id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/v4/app/FragmentTabHost;->eNC:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private efo(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100f3

    aput v1, v0, v2

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v4/app/FragmentTabHost;->eNC:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-super {p0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 7

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/widget/TabHost;->onAttachedToWindow()V

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentTabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    iget-object v2, p0, Landroid/support/v4/app/FragmentTabHost;->eND:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eND:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/au;

    iget-object v5, p0, Landroid/support/v4/app/FragmentTabHost;->eNx:Landroid/support/v4/app/ar;

    iget-object v6, v0, Landroid/support/v4/app/au;->ePG:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/support/v4/app/ar;->eii(Ljava/lang/String;)Landroid/support/v4/app/p;

    move-result-object v5

    iput-object v5, v0, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    iget-object v5, v0, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    if-eqz v5, :cond_0

    iget-object v5, v0, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    invoke-virtual {v5}, Landroid/support/v4/app/p;->ecH()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    iget-object v5, v0, Landroid/support/v4/app/au;->ePG:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iput-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNA:Landroid/support/v4/app/au;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v4/app/FragmentTabHost;->eNx:Landroid/support/v4/app/ar;

    invoke-virtual {v1}, Landroid/support/v4/app/ar;->ehw()Landroid/support/v4/app/N;

    move-result-object v1

    :cond_2
    iget-object v0, v0, Landroid/support/v4/app/au;->ePD:Landroid/support/v4/app/p;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/N;->edA(Landroid/support/v4/app/p;)Landroid/support/v4/app/N;

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNB:Z

    invoke-direct {p0, v3, v1}, Landroid/support/v4/app/FragmentTabHost;->efk(Ljava/lang/String;Landroid/support/v4/app/N;)Landroid/support/v4/app/N;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/support/v4/app/N;->commit()I

    iget-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNx:Landroid/support/v4/app/ar;

    invoke-virtual {v0}, Landroid/support/v4/app/ar;->ehS()Z

    :cond_4
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/TabHost;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNB:Z

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    instance-of v0, p1, Landroid/support/v4/app/FragmentTabHost$SavedState;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/TabHost;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    :cond_0
    check-cast p1, Landroid/support/v4/app/FragmentTabHost$SavedState;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTabHost$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/TabHost;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v0, p1, Landroid/support/v4/app/FragmentTabHost$SavedState;->eMT:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentTabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/widget/TabHost;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/FragmentTabHost$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v4/app/FragmentTabHost$SavedState;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentTabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Landroid/support/v4/app/FragmentTabHost$SavedState;->eMT:Ljava/lang/String;

    return-object v1
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNB:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v1}, Landroid/support/v4/app/FragmentTabHost;->efk(Ljava/lang/String;Landroid/support/v4/app/N;)Landroid/support/v4/app/N;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/N;->commit()I

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNy:Landroid/widget/TabHost$OnTabChangeListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNy:Landroid/widget/TabHost$OnTabChangeListener;

    invoke-interface {v0, p1}, Landroid/widget/TabHost$OnTabChangeListener;->onTabChanged(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/app/FragmentTabHost;->eNy:Landroid/widget/TabHost$OnTabChangeListener;

    return-void
.end method

.method public setup()V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must call setup() that takes a Context and FragmentManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setup(Landroid/content/Context;Landroid/support/v4/app/ar;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentTabHost;->efm(Landroid/content/Context;)V

    invoke-super {p0}, Landroid/widget/TabHost;->setup()V

    iput-object p1, p0, Landroid/support/v4/app/FragmentTabHost;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/support/v4/app/FragmentTabHost;->eNx:Landroid/support/v4/app/ar;

    invoke-direct {p0}, Landroid/support/v4/app/FragmentTabHost;->efn()V

    return-void
.end method

.method public setup(Landroid/content/Context;Landroid/support/v4/app/ar;I)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentTabHost;->efm(Landroid/content/Context;)V

    invoke-super {p0}, Landroid/widget/TabHost;->setup()V

    iput-object p1, p0, Landroid/support/v4/app/FragmentTabHost;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/support/v4/app/FragmentTabHost;->eNx:Landroid/support/v4/app/ar;

    iput p3, p0, Landroid/support/v4/app/FragmentTabHost;->eNC:I

    invoke-direct {p0}, Landroid/support/v4/app/FragmentTabHost;->efn()V

    iget-object v0, p0, Landroid/support/v4/app/FragmentTabHost;->eNz:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p3}, Landroid/widget/FrameLayout;->setId(I)V

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentTabHost;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const v0, 0x1020012

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentTabHost;->setId(I)V

    :cond_0
    return-void
.end method
