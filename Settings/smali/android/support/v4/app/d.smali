.class public Landroid/support/v4/app/d;
.super Ljava/lang/Object;
.source "NotificationCompat.java"


# instance fields
.field eIN:I

.field public eIO:Ljava/util/ArrayList;

.field eIP:Ljava/lang/String;

.field public eIQ:Ljava/util/ArrayList;

.field eIR:Ljava/lang/String;

.field eIS:Ljava/lang/CharSequence;

.field eIT:Ljava/lang/CharSequence;

.field eIU:Landroid/widget/RemoteViews;

.field eIV:Z

.field eIW:I

.field eIX:I

.field eIY:Ljava/lang/CharSequence;

.field eIZ:I

.field eJa:Landroid/app/PendingIntent;

.field eJb:Landroid/graphics/Bitmap;

.field eJc:I

.field eJd:Z

.field eJe:Landroid/widget/RemoteViews;

.field eJf:Z

.field eJg:Z

.field eJh:Ljava/lang/String;

.field eJi:I

.field eJj:Ljava/lang/String;

.field eJk:Landroid/app/Notification;

.field eJl:I

.field eJm:Z

.field eJn:Landroid/widget/RemoteViews;

.field eJo:Z

.field eJp:Landroid/app/PendingIntent;

.field eJq:Landroid/app/Notification;

.field eJr:Landroid/widget/RemoteViews;

.field eJs:Ljava/lang/CharSequence;

.field eJt:J

.field eJu:Z

.field eJv:Landroid/support/v4/app/e;

.field eJw:Ljava/lang/String;

.field eJx:[Ljava/lang/CharSequence;

.field public mContext:Landroid/content/Context;

.field mExtras:Landroid/os/Bundle;

.field mProgress:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/app/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/d;->eIO:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/d;->eJo:Z

    iput-boolean v4, p0, Landroid/support/v4/app/d;->eIV:Z

    iput v4, p0, Landroid/support/v4/app/d;->eIN:I

    iput v4, p0, Landroid/support/v4/app/d;->eJi:I

    iput v4, p0, Landroid/support/v4/app/d;->eIZ:I

    iput v4, p0, Landroid/support/v4/app/d;->eIW:I

    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/d;->eJk:Landroid/app/Notification;

    iput-object p1, p0, Landroid/support/v4/app/d;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/support/v4/app/d;->eJj:Ljava/lang/String;

    iget-object v0, p0, Landroid/support/v4/app/d;->eJk:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    iget-object v0, p0, Landroid/support/v4/app/d;->eJk:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    iput v4, p0, Landroid/support/v4/app/d;->eJl:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/d;->eIQ:Ljava/util/ArrayList;

    return-void
.end method

.method protected static eaJ(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    const/16 v1, 0x1400

    if-nez p0, :cond_0

    return-object p0

    :cond_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    :cond_1
    return-object p0
.end method


# virtual methods
.method public build()Landroid/app/Notification;
    .locals 1

    new-instance v0, Landroid/support/v4/app/m;

    invoke-direct {v0, p0}, Landroid/support/v4/app/m;-><init>(Landroid/support/v4/app/d;)V

    invoke-virtual {v0}, Landroid/support/v4/app/m;->build()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public eaH(I)Landroid/support/v4/app/d;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/d;->eJk:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    return-object p0
.end method

.method public eaI(Ljava/lang/CharSequence;)Landroid/support/v4/app/d;
    .locals 1

    invoke-static {p1}, Landroid/support/v4/app/d;->eaJ(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/d;->eIT:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public eaK(Ljava/lang/CharSequence;)Landroid/support/v4/app/d;
    .locals 1

    invoke-static {p1}, Landroid/support/v4/app/d;->eaJ(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/d;->eIS:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public eaL(Landroid/app/PendingIntent;)Landroid/support/v4/app/d;
    .locals 0

    iput-object p1, p0, Landroid/support/v4/app/d;->eJp:Landroid/app/PendingIntent;

    return-object p0
.end method

.method public setColor(I)Landroid/support/v4/app/d;
    .locals 0

    iput p1, p0, Landroid/support/v4/app/d;->eIN:I

    return-object p0
.end method
