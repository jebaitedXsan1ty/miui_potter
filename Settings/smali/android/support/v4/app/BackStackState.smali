.class final Landroid/support/v4/app/BackStackState;
.super Ljava/lang/Object;
.source "BackStackRecord.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final ePH:Ljava/lang/CharSequence;

.field final ePI:[I

.field final ePJ:I

.field final ePK:Ljava/lang/String;

.field final ePL:I

.field final ePM:I

.field final ePN:I

.field final ePO:Ljava/lang/CharSequence;

.field final ePP:Z

.field final ePQ:Ljava/util/ArrayList;

.field final ePR:Ljava/util/ArrayList;

.field final ePS:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/support/v4/app/ad;

    invoke-direct {v0}, Landroid/support/v4/app/ad;-><init>()V

    sput-object v0, Landroid/support/v4/app/BackStackState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePL:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePJ:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePK:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePS:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePN:I

    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePO:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePM:I

    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePH:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePQ:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePR:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v4/app/BackStackState;->ePP:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/support/v4/app/al;)V
    .locals 7

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v1, p1, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    mul-int/lit8 v1, v3, 0x6

    new-array v1, v1, [I

    iput-object v1, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    iget-boolean v1, p1, Landroid/support/v4/app/al;->eOx:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Not on back stack"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p1, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    iget-object v4, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v5, v1, 0x1

    iget v6, v0, Landroid/support/v4/app/i;->eJJ:I

    aput v6, v4, v1

    iget-object v4, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v6, v5, 0x1

    iget-object v1, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-eqz v1, :cond_1

    iget-object v1, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    iget v1, v1, Landroid/support/v4/app/p;->eKp:I

    :goto_1
    aput v1, v4, v5

    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v4, v6, 0x1

    iget v5, v0, Landroid/support/v4/app/i;->eJO:I

    aput v5, v1, v6

    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v5, v4, 0x1

    iget v6, v0, Landroid/support/v4/app/i;->eJN:I

    aput v6, v1, v4

    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v4, v5, 0x1

    iget v6, v0, Landroid/support/v4/app/i;->eJL:I

    aput v6, v1, v5

    iget-object v5, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v1, v4, 0x1

    iget v0, v0, Landroid/support/v4/app/i;->eJM:I

    aput v0, v5, v4

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1

    :cond_2
    iget v0, p1, Landroid/support/v4/app/al;->eOA:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePL:I

    iget v0, p1, Landroid/support/v4/app/al;->eOz:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePJ:I

    iget-object v0, p1, Landroid/support/v4/app/al;->eOm:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePK:Ljava/lang/String;

    iget v0, p1, Landroid/support/v4/app/al;->eOp:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePS:I

    iget v0, p1, Landroid/support/v4/app/al;->eOB:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePN:I

    iget-object v0, p1, Landroid/support/v4/app/al;->eOw:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePO:Ljava/lang/CharSequence;

    iget v0, p1, Landroid/support/v4/app/al;->eOs:I

    iput v0, p0, Landroid/support/v4/app/BackStackState;->ePM:I

    iget-object v0, p1, Landroid/support/v4/app/al;->eOy:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePH:Ljava/lang/CharSequence;

    iget-object v0, p1, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePQ:Ljava/util/ArrayList;

    iget-object v0, p1, Landroid/support/v4/app/al;->eOu:Ljava/util/ArrayList;

    iput-object v0, p0, Landroid/support/v4/app/BackStackState;->ePR:Ljava/util/ArrayList;

    iget-boolean v0, p1, Landroid/support/v4/app/al;->eOn:Z

    iput-boolean v0, p0, Landroid/support/v4/app/BackStackState;->ePP:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public eiT(Landroid/support/v4/app/an;)Landroid/support/v4/app/al;
    .locals 8

    const/4 v7, 0x1

    const/4 v0, 0x0

    new-instance v3, Landroid/support/v4/app/al;

    invoke-direct {v3, p1}, Landroid/support/v4/app/al;-><init>(Landroid/support/v4/app/an;)V

    move v1, v0

    :goto_0
    iget-object v2, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    new-instance v4, Landroid/support/v4/app/i;

    invoke-direct {v4}, Landroid/support/v4/app/i;-><init>()V

    iget-object v2, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v5, v0, 0x1

    aget v0, v2, v0

    iput v0, v4, Landroid/support/v4/app/i;->eJJ:I

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Instantiate "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, " op #"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, " base fragment #"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v6, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    aget v6, v6, v5

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v2, v5, 0x1

    aget v0, v0, v5

    if-ltz v0, :cond_1

    iget-object v5, p1, Landroid/support/v4/app/an;->ePe:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    iput-object v0, v4, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    :goto_1
    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v5, v2, 0x1

    aget v0, v0, v2

    iput v0, v4, Landroid/support/v4/app/i;->eJO:I

    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v2, v5, 0x1

    aget v0, v0, v5

    iput v0, v4, Landroid/support/v4/app/i;->eJN:I

    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v5, v2, 0x1

    aget v0, v0, v2

    iput v0, v4, Landroid/support/v4/app/i;->eJL:I

    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    add-int/lit8 v2, v5, 0x1

    aget v0, v0, v5

    iput v0, v4, Landroid/support/v4/app/i;->eJM:I

    iget v0, v4, Landroid/support/v4/app/i;->eJO:I

    iput v0, v3, Landroid/support/v4/app/al;->eOC:I

    iget v0, v4, Landroid/support/v4/app/i;->eJN:I

    iput v0, v3, Landroid/support/v4/app/al;->eOk:I

    iget v0, v4, Landroid/support/v4/app/i;->eJL:I

    iput v0, v3, Landroid/support/v4/app/al;->eOl:I

    iget v0, v4, Landroid/support/v4/app/i;->eJM:I

    iput v0, v3, Landroid/support/v4/app/al;->eOv:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/al;->efY(Landroid/support/v4/app/i;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-object v0, v4, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    goto :goto_1

    :cond_2
    iget v0, p0, Landroid/support/v4/app/BackStackState;->ePL:I

    iput v0, v3, Landroid/support/v4/app/al;->eOA:I

    iget v0, p0, Landroid/support/v4/app/BackStackState;->ePJ:I

    iput v0, v3, Landroid/support/v4/app/al;->eOz:I

    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePK:Ljava/lang/String;

    iput-object v0, v3, Landroid/support/v4/app/al;->eOm:Ljava/lang/String;

    iget v0, p0, Landroid/support/v4/app/BackStackState;->ePS:I

    iput v0, v3, Landroid/support/v4/app/al;->eOp:I

    iput-boolean v7, v3, Landroid/support/v4/app/al;->eOx:Z

    iget v0, p0, Landroid/support/v4/app/BackStackState;->ePN:I

    iput v0, v3, Landroid/support/v4/app/al;->eOB:I

    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePO:Ljava/lang/CharSequence;

    iput-object v0, v3, Landroid/support/v4/app/al;->eOw:Ljava/lang/CharSequence;

    iget v0, p0, Landroid/support/v4/app/BackStackState;->ePM:I

    iput v0, v3, Landroid/support/v4/app/al;->eOs:I

    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePH:Ljava/lang/CharSequence;

    iput-object v0, v3, Landroid/support/v4/app/al;->eOy:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePQ:Ljava/util/ArrayList;

    iput-object v0, v3, Landroid/support/v4/app/al;->eOt:Ljava/util/ArrayList;

    iget-object v0, p0, Landroid/support/v4/app/BackStackState;->ePR:Ljava/util/ArrayList;

    iput-object v0, v3, Landroid/support/v4/app/al;->eOu:Ljava/util/ArrayList;

    iget-boolean v0, p0, Landroid/support/v4/app/BackStackState;->ePP:Z

    iput-boolean v0, v3, Landroid/support/v4/app/al;->eOn:Z

    invoke-virtual {v3, v7}, Landroid/support/v4/app/al;->efR(I)V

    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->ePI:[I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeIntArray([I)V

    iget v1, p0, Landroid/support/v4/app/BackStackState;->ePL:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Landroid/support/v4/app/BackStackState;->ePJ:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->ePK:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/BackStackState;->ePS:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Landroid/support/v4/app/BackStackState;->ePN:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->ePO:Ljava/lang/CharSequence;

    invoke-static {v1, p1, v0}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    iget v1, p0, Landroid/support/v4/app/BackStackState;->ePM:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->ePH:Ljava/lang/CharSequence;

    invoke-static {v1, p1, v0}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->ePQ:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-object v1, p0, Landroid/support/v4/app/BackStackState;->ePR:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-boolean v1, p0, Landroid/support/v4/app/BackStackState;->ePP:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
