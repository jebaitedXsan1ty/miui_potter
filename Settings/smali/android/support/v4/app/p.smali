.class public Landroid/support/v4/app/p;
.super Ljava/lang/Object;
.source "Fragment.java"

# interfaces
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/arch/lifecycle/f;


# static fields
.field private static final eKF:Landroid/support/v4/a/a;

.field static final eKm:Ljava/lang/Object;


# instance fields
.field eKA:Z

.field eKB:Landroid/support/v4/app/E;

.field eKC:Z

.field eKD:Z

.field eKE:Landroid/support/v4/app/an;

.field eKG:Landroid/os/Bundle;

.field eKH:Landroid/os/Bundle;

.field eKI:I

.field eKJ:Landroid/view/View;

.field eKK:I

.field eKL:Landroid/arch/lifecycle/b;

.field eKM:Z

.field eKN:Z

.field eKO:Z

.field eKP:Z

.field eKQ:I

.field eKR:Z

.field eKS:Landroid/support/v4/app/ap;

.field eKT:Landroid/support/v4/app/K;

.field eKU:Landroid/support/v4/app/p;

.field eKV:Landroid/support/v4/app/p;

.field eKW:Landroid/view/LayoutInflater;

.field eKX:F

.field eKY:Z

.field eKZ:Z

.field eKf:Landroid/view/ViewGroup;

.field eKg:Landroid/view/View;

.field eKh:Landroid/support/v4/app/C;

.field eKi:Ljava/lang/String;

.field eKj:Z

.field eKk:Z

.field eKl:Z

.field eKn:Z

.field eKo:Z

.field eKp:I

.field eKq:I

.field eKr:Z

.field eKs:Z

.field eKt:Z

.field eKu:Landroid/support/v4/app/an;

.field eKv:I

.field eKw:Z

.field eKx:Z

.field eKy:Landroid/util/SparseArray;

.field eKz:I

.field mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/support/v4/a/a;

    invoke-direct {v0}, Landroid/support/v4/a/a;-><init>()V

    sput-object v0, Landroid/support/v4/app/p;->eKF:Landroid/support/v4/a/a;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/app/p;->eKm:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/app/p;->eKI:I

    iput v1, p0, Landroid/support/v4/app/p;->eKp:I

    iput v1, p0, Landroid/support/v4/app/p;->eKK:I

    iput-boolean v2, p0, Landroid/support/v4/app/p;->eKY:Z

    iput-boolean v2, p0, Landroid/support/v4/app/p;->eKx:Z

    new-instance v0, Landroid/arch/lifecycle/b;

    invoke-direct {v0, p0}, Landroid/arch/lifecycle/b;-><init>(Landroid/arch/lifecycle/f;)V

    iput-object v0, p0, Landroid/support/v4/app/p;->eKL:Landroid/arch/lifecycle/b;

    return-void
.end method

.method static ebC(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    :try_start_0
    sget-object v0, Landroid/support/v4/app/p;->eKF:Landroid/support/v4/a/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Landroid/support/v4/app/p;->eKF:Landroid/support/v4/a/a;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/a/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-class v1, Landroid/support/v4/app/p;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return v0
.end method

.method private ebF()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v1, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/support/v4/app/y;->ebi()V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/support/v4/app/K;->eMI:Z

    iget-object v1, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-object v1, v1, Landroid/support/v4/app/K;->eMD:Landroid/support/v4/app/y;

    iget-object v2, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iput-object v0, v2, Landroid/support/v4/app/K;->eMD:Landroid/support/v4/app/y;

    move-object v0, v1

    goto :goto_0
.end method

.method public static ebU(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/p;
    .locals 4

    :try_start_0
    sget-object v0, Landroid/support/v4/app/p;->eKF:Landroid/support/v4/a/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Landroid/support/v4/app/p;->eKF:Landroid/support/v4/a/a;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/a/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    if-eqz p2, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/p;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    invoke-virtual {v0, p2}, Landroid/support/v4/app/p;->ecf(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Landroid/support/v4/app/Fragment$InstantiationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to instantiate fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": calling Fragment constructor caused an exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/Fragment$InstantiationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Landroid/support/v4/app/Fragment$InstantiationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to instantiate fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": could not find Fragment constructor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/Fragment$InstantiationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Landroid/support/v4/app/Fragment$InstantiationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to instantiate fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": make sure class name exists, is public, and has an"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/Fragment$InstantiationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Landroid/support/v4/app/Fragment$InstantiationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to instantiate fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": make sure class name exists, is public, and has an"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/Fragment$InstantiationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_4
    move-exception v0

    new-instance v1, Landroid/support/v4/app/Fragment$InstantiationException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to instantiate fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": make sure class name exists, is public, and has an"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v4/app/Fragment$InstantiationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method private ebZ()Landroid/support/v4/app/K;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/app/K;

    invoke-direct {v0}, Landroid/support/v4/app/K;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    return-object v0
.end method

.method static synthetic ecc(Landroid/support/v4/app/p;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/p;->ebF()V

    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mFragmentId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/p;->eKv:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, " mContainerId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/p;->eKq:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, " mTag="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/p;->eKI:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v0, " mIndex="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v0, " mWho="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, " mBackStackNesting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/p;->eKz:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mAdded="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKj:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, " mRemoving="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKN:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, " mFromLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKA:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, " mInLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKw:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mHidden="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKZ:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, " mDetached="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKR:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, " mMenuVisible="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKY:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, " mHasMenu="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKt:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mRetainInstance="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKP:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, " mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKo:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string/jumbo v0, " mUserVisibleHint="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKx:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mFragmentManager="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    if-eqz v0, :cond_1

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mHost="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKU:Landroid/support/v4/app/p;

    if-eqz v0, :cond_2

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mParentFragment="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKU:Landroid/support/v4/app/p;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/p;->eKH:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mArguments="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKH:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mSavedFragmentState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKG:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    if-eqz v0, :cond_5

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mSavedViewState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    if-eqz v0, :cond_6

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mTarget="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKV:Landroid/support/v4/app/p;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string/jumbo v0, " mTargetRequestCode="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v4/app/p;->eKQ:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    :cond_6
    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebS()I

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mNextAnim="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebS()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    :cond_7
    iget-object v0, p0, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    if-eqz v0, :cond_8

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKf:Landroid/view/ViewGroup;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_8
    iget-object v0, p0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v0, :cond_9

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_9
    iget-object v0, p0, Landroid/support/v4/app/p;->eKJ:Landroid/view/View;

    if-eqz v0, :cond_a

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mInnerView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_a
    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebu()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mAnimatingAway="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebu()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mStateAfterAnimating="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ecC()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    if-eqz v0, :cond_c

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "Loader Manager:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/ap;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_d

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Child "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/an;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_d
    return-void
.end method

.method ebA()Landroid/support/v4/app/av;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-object v0, v0, Landroid/support/v4/app/K;->eMy:Landroid/support/v4/app/av;

    return-object v0
.end method

.method public ebB(Landroid/support/v4/app/p;)V
    .locals 0

    return-void
.end method

.method public ebD(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->edm()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebI()Landroid/support/v4/app/ar;

    iget-object v1, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v1}, Landroid/support/v4/app/an;->egP()Landroid/view/LayoutInflater$Factory2;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->dRM(Landroid/view/LayoutInflater;Landroid/view/LayoutInflater$Factory2;)V

    return-object v0
.end method

.method public ebE()Ljava/lang/Object;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edx(Landroid/support/v4/app/K;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Landroid/support/v4/app/p;->eKm:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ecB()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edx(Landroid/support/v4/app/K;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method ebG()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-boolean v0, v0, Landroid/support/v4/app/K;->eMI:Z

    return v0
.end method

.method ebH(Landroid/view/Menu;)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKZ:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKt:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKY:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->eco(Landroid/view/Menu;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/an;->ehp(Landroid/view/Menu;)V

    :cond_1
    return-void
.end method

.method public final ebI()Landroid/support/v4/app/ar;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebr()V

    iget v0, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eiu()V

    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    return-object v0

    :cond_1
    iget v0, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->egq()V

    goto :goto_0

    :cond_2
    iget v0, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->ehJ()V

    goto :goto_0

    :cond_3
    iget v0, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eia()V

    goto :goto_0
.end method

.method ebJ()I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget v0, v0, Landroid/support/v4/app/K;->eMN:I

    return v0
.end method

.method ebK()V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eij()V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->ehn()Z

    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0}, Landroid/support/v4/app/p;->onResume()V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onResume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eiu()V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->ehn()Z

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/p;->eKL:Landroid/arch/lifecycle/b;

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWE:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/b;->epZ(Landroid/arch/lifecycle/Lifecycle$Event;)V

    return-void
.end method

.method public final ebL()Landroid/support/v4/app/S;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->edb()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/S;

    goto :goto_0
.end method

.method ebM(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->eby(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/p;->eKW:Landroid/view/LayoutInflater;

    iget-object v0, p0, Landroid/support/v4/app/p;->eKW:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method ebN()I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget v0, v0, Landroid/support/v4/app/K;->eMx:I

    return v0
.end method

.method ebO(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eij()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKD:Z

    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v4/app/p;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method ebP(Landroid/view/Menu;)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Landroid/support/v4/app/p;->eKZ:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/app/p;->eKt:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Landroid/support/v4/app/p;->eKY:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v1, p1}, Landroid/support/v4/app/an;->ehX(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method public ebQ()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edr(Landroid/support/v4/app/K;)Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edr(Landroid/support/v4/app/K;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public ebR()Ljava/lang/Object;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edy(Landroid/support/v4/app/K;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Landroid/support/v4/app/p;->eKm:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ecg()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edy(Landroid/support/v4/app/K;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method ebS()I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget v0, v0, Landroid/support/v4/app/K;->eMC:I

    return v0
.end method

.method ebT()V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0}, Landroid/support/v4/app/p;->onDetach()V

    iput-object v1, p0, Landroid/support/v4/app/p;->eKW:Landroid/view/LayoutInflater;

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onDetach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKo:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Child FragmentManager of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " was not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " destroyed and this fragment is not retaining instance"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eha()V

    iput-object v1, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    :cond_2
    return-void
.end method

.method ebV(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/an;->ehN(Landroid/content/res/Configuration;)V

    :cond_0
    return-void
.end method

.method final ebW()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v4/app/p;->eKz:I

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method ebX(Z)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onMultiWindowModeChanged(Z)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/an;->eiv(Z)V

    :cond_0
    return-void
.end method

.method ebY()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKL:Landroid/arch/lifecycle/b;

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWA:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/b;->epZ(Landroid/arch/lifecycle/Lifecycle$Event;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eha()V

    :cond_0
    iput v2, p0, Landroid/support/v4/app/p;->eKI:I

    iput-boolean v2, p0, Landroid/support/v4/app/p;->eKO:Z

    iput-boolean v2, p0, Landroid/support/v4/app/p;->eKl:Z

    invoke-virtual {p0}, Landroid/support/v4/app/p;->onDestroy()V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object v3, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    return-void
.end method

.method ebr()V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Fragment has not been attached yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Landroid/support/v4/app/an;

    invoke-direct {v0}, Landroid/support/v4/app/an;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    iget-object v1, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    new-instance v2, Landroid/support/v4/app/at;

    invoke-direct {v2, p0}, Landroid/support/v4/app/at;-><init>(Landroid/support/v4/app/p;)V

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/an;->ehG(Landroid/support/v4/app/C;Landroid/support/v4/app/k;Landroid/support/v4/app/p;)V

    return-void
.end method

.method ebs()Landroid/animation/Animator;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-object v0, v0, Landroid/support/v4/app/K;->eMK:Landroid/animation/Animator;

    return-object v0
.end method

.method ebt()Landroid/support/v4/app/av;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-object v0, v0, Landroid/support/v4/app/K;->eMz:Landroid/support/v4/app/av;

    return-object v0
.end method

.method ebu()Landroid/view/View;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-object v0, v0, Landroid/support/v4/app/K;->eMA:Landroid/view/View;

    return-object v0
.end method

.method ebv(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eij()V

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onActivityCreated(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onActivityCreated()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->ehJ()V

    :cond_2
    return-void
.end method

.method ebw()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v4/app/p;->onLowMemory()V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->ehi()V

    :cond_0
    return-void
.end method

.method ebx()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-boolean v0, v0, Landroid/support/v4/app/K;->eML:Z

    return v0
.end method

.method public eby(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->ebD(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method ebz()V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKL:Landroid/arch/lifecycle/b;

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWC:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/b;->epZ(Landroid/arch/lifecycle/Lifecycle$Event;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->ehz()V

    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0}, Landroid/support/v4/app/p;->onPause()V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onPause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method final ecA(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKJ:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    iput-object v2, p0, Landroid/support/v4/app/p;->eKy:Landroid/util/SparseArray;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onViewStateRestored(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onViewStateRestored()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method public ecB()Ljava/lang/Object;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edw(Landroid/support/v4/app/K;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method ecC()I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget v0, v0, Landroid/support/v4/app/K;->eME:I

    return v0
.end method

.method public final ecD()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eig()Z

    move-result v0

    return v0
.end method

.method ecE()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/p;->eKp:I

    iput-object v2, p0, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKj:Z

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKN:Z

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKA:Z

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKw:Z

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKr:Z

    iput v1, p0, Landroid/support/v4/app/p;->eKz:I

    iput-object v2, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    iput-object v2, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    iput-object v2, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    iput v1, p0, Landroid/support/v4/app/p;->eKv:I

    iput v1, p0, Landroid/support/v4/app/p;->eKq:I

    iput-object v2, p0, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKZ:Z

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKR:Z

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKo:Z

    iput-object v2, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKn:Z

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKs:Z

    return-void
.end method

.method ecF(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKZ:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/an;->egR(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method ecG()V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eij()V

    :cond_0
    return-void
.end method

.method public final ecH()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKR:Z

    return v0
.end method

.method ecI(II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Landroid/support/v4/app/p;->ebZ()Landroid/support/v4/app/K;

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iput p1, v0, Landroid/support/v4/app/K;->eMN:I

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iput p2, v0, Landroid/support/v4/app/K;->eMx:I

    return-void
.end method

.method public ecJ()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    iget-object v0, v0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Landroid/support/v4/app/p;->ebZ()Landroid/support/v4/app/K;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/app/K;->eMI:Z

    :goto_0
    return-void

    :cond_1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    iget-object v1, v1, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v1}, Landroid/support/v4/app/C;->ecX()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    iget-object v0, v0, Landroid/support/v4/app/an;->eOH:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->ecX()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/ag;

    invoke-direct {v1, p0}, Landroid/support/v4/app/ag;-><init>(Landroid/support/v4/app/p;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Landroid/support/v4/app/p;->ebF()V

    goto :goto_0
.end method

.method ecK(Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->egv()Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public ecL(Z)V
    .locals 0

    return-void
.end method

.method public ecM()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edv(Landroid/support/v4/app/K;)Ljava/lang/Boolean;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edv(Landroid/support/v4/app/K;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method ecN()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->egV()V

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v4/app/p;->eKI:I

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0}, Landroid/support/v4/app/p;->onDestroyView()V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onDestroyView()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->eiA()V

    :cond_2
    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKD:Z

    return-void
.end method

.method public ecO(IZI)Landroid/view/animation/Animation;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method ecP(Landroid/support/v4/app/y;)V
    .locals 3

    invoke-direct {p0}, Landroid/support/v4/app/p;->ebZ()Landroid/support/v4/app/K;

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-object v0, v0, Landroid/support/v4/app/K;->eMD:Landroid/support/v4/app/y;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-object v0, v0, Landroid/support/v4/app/K;->eMD:Landroid/support/v4/app/y;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Trying to set a replacement startPostponedEnterTransition on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iget-boolean v0, v0, Landroid/support/v4/app/K;->eMI:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    iput-object p1, v0, Landroid/support/v4/app/K;->eMD:Landroid/support/v4/app/y;

    :cond_2
    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/support/v4/app/y;->ebm()V

    :cond_3
    return-void
.end method

.method public final ecQ()Landroid/support/v4/app/ar;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    return-object v0
.end method

.method public eca(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method ecb(Landroid/view/MenuItem;)Z
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKZ:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKt:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKY:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/an;->eid(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method ecd(Landroid/animation/Animator;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/p;->ebZ()Landroid/support/v4/app/K;

    move-result-object v0

    iput-object p1, v0, Landroid/support/v4/app/K;->eMK:Landroid/animation/Animator;

    return-void
.end method

.method ece(I)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/p;->ebZ()Landroid/support/v4/app/K;

    move-result-object v0

    iput p1, v0, Landroid/support/v4/app/K;->eME:I

    return-void
.end method

.method public ecf(Landroid/os/Bundle;)V
    .locals 2

    iget v0, p0, Landroid/support/v4/app/p;->eKp:I

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ecD()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Fragment already active and state has been saved"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Landroid/support/v4/app/p;->eKH:Landroid/os/Bundle;

    return-void
.end method

.method public ecg()Ljava/lang/Object;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edu(Landroid/support/v4/app/K;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public ech()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    return-object v0
.end method

.method eci(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/p;->ebZ()Landroid/support/v4/app/K;

    move-result-object v0

    iput-object p1, v0, Landroid/support/v4/app/K;->eMA:Landroid/view/View;

    return-void
.end method

.method ecj()V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKL:Landroid/arch/lifecycle/b;

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWG:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/b;->epZ(Landroid/arch/lifecycle/Lifecycle$Event;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->egU()V

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0}, Landroid/support/v4/app/p;->onStop()V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onStop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method eck()V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eij()V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->ehn()Z

    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0}, Landroid/support/v4/app/p;->onStart()V

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onStart()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->egq()V

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->eiD()V

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/p;->eKL:Landroid/arch/lifecycle/b;

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWF:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/b;->epZ(Landroid/arch/lifecycle/Lifecycle$Event;)V

    return-void
.end method

.method ecl(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    const-string/jumbo v0, "android:support:fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebr()V

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    iget-object v2, p0, Landroid/support/v4/app/p;->eKB:Landroid/support/v4/app/E;

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/an;->ehk(Landroid/os/Parcelable;Landroid/support/v4/app/E;)V

    iput-object v3, p0, Landroid/support/v4/app/p;->eKB:Landroid/support/v4/app/E;

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eia()V

    :cond_1
    return-void
.end method

.method ecm(Z)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onPictureInPictureModeChanged(Z)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/an;->ehR(Z)V

    :cond_0
    return-void
.end method

.method public ecn(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKO:Z

    iget-object v1, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    if-nez v1, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0, v0, p2, p3}, Landroid/support/v4/app/p;->eca(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->edb()Landroid/app/Activity;

    move-result-object v0

    goto :goto_0
.end method

.method public eco(Landroid/view/Menu;)V
    .locals 0

    return-void
.end method

.method ecp(Landroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eij()V

    :cond_0
    iput v1, p0, Landroid/support/v4/app/p;->eKI:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->onCreate(Landroid/os/Bundle;)V

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKl:Z

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v4/app/SuperNotCalledException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " did not call through to super.onCreate()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKL:Landroid/arch/lifecycle/b;

    sget-object v1, Landroid/arch/lifecycle/Lifecycle$Event;->eWB:Landroid/arch/lifecycle/Lifecycle$Event;

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/b;->epZ(Landroid/arch/lifecycle/Lifecycle$Event;)V

    return-void
.end method

.method ecq(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Landroid/support/v4/app/p;->ebZ()Landroid/support/v4/app/K;

    move-result-object v0

    iput p1, v0, Landroid/support/v4/app/K;->eMC:I

    return-void
.end method

.method public ecr()Ljava/lang/Object;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->edt(Landroid/support/v4/app/K;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method ecs()Landroid/support/v4/app/ar;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    return-object v0
.end method

.method ect(Z)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/p;->ebZ()Landroid/support/v4/app/K;

    move-result-object v0

    iput-boolean p1, v0, Landroid/support/v4/app/K;->eML:Z

    return-void
.end method

.method ecu(Ljava/lang/String;)Landroid/support/v4/app/p;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/an;->ehU(Ljava/lang/String;)Landroid/support/v4/app/p;

    move-result-object v0

    return-object v0

    :cond_1
    return-object v1
.end method

.method final ecv(ILandroid/support/v4/app/p;)V
    .locals 2

    iput p1, p0, Landroid/support/v4/app/p;->eKp:I

    if-eqz p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "android:fragment:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    goto :goto_0
.end method

.method public ecw()Ljava/lang/Object;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->eds(Landroid/support/v4/app/K;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Landroid/support/v4/app/p;->eKm:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ecr()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKT:Landroid/support/v4/app/K;

    invoke-static {v0}, Landroid/support/v4/app/K;->eds(Landroid/support/v4/app/K;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method ecx(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Landroid/support/v4/app/p;->eKZ:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/app/p;->eKt:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Landroid/support/v4/app/p;->eKY:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2}, Landroid/support/v4/app/p;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v1, p1, p2}, Landroid/support/v4/app/an;->egs(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    or-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method public ecy(IZI)Landroid/animation/Animator;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method ecz()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->ehe()V

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v4/app/p;->eKI:I

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKn:Z

    if-eqz v0, :cond_2

    iput-boolean v3, p0, Landroid/support/v4/app/p;->eKn:Z

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKs:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKs:Z

    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    iget-object v1, p0, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/p;->eKn:Z

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/C;->ecZ(Ljava/lang/String;ZZ)Landroid/support/v4/app/ap;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->edf()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->eiy()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->eiC()V

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKO:Z

    iget-object v1, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    if-nez v1, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0, v0}, Landroid/support/v4/app/p;->onAttach(Landroid/app/Activity;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    invoke-virtual {v0}, Landroid/support/v4/app/C;->edb()Landroid/app/Activity;

    move-result-object v0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKO:Z

    invoke-virtual {p0, p1}, Landroid/support/v4/app/p;->ecl(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/an;->eio(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKE:Landroid/support/v4/app/an;

    invoke-virtual {v0}, Landroid/support/v4/app/an;->eia()V

    :cond_0
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v4/app/p;->ebL()Landroid/support/v4/app/S;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/S;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 4

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKO:Z

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKs:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKs:Z

    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    iget-object v1, p0, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/p;->eKn:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/C;->ecZ(Ljava/lang/String;ZZ)Landroid/support/v4/app/ap;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->eiw()V

    :cond_1
    return-void
.end method

.method public onDestroyOptionsMenu()V
    .locals 0

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public onDetach()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public onPictureInPictureModeChanged(Z)V
    .locals 0

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onStart()V
    .locals 4

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKO:Z

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKn:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKn:Z

    iget-boolean v0, p0, Landroid/support/v4/app/p;->eKs:Z

    if-nez v0, :cond_1

    iput-boolean v1, p0, Landroid/support/v4/app/p;->eKs:Z

    iget-object v0, p0, Landroid/support/v4/app/p;->eKh:Landroid/support/v4/app/C;

    iget-object v1, p0, Landroid/support/v4/app/p;->eKi:Ljava/lang/String;

    iget-boolean v2, p0, Landroid/support/v4/app/p;->eKn:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/C;->ecZ(Ljava/lang/String;ZZ)Landroid/support/v4/app/ap;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/p;->eKS:Landroid/support/v4/app/ap;

    invoke-virtual {v0}, Landroid/support/v4/app/ap;->eiB()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/p;->eKO:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-static {p0, v0}, Landroid/support/v4/a/k;->dSL(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    iget v1, p0, Landroid/support/v4/app/p;->eKp:I

    if-ltz v1, :cond_0

    const-string/jumbo v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Landroid/support/v4/app/p;->eKp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget v1, p0, Landroid/support/v4/app/p;->eKv:I

    if-eqz v1, :cond_1

    const-string/jumbo v1, " id=0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Landroid/support/v4/app/p;->eKv:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
