.class Landroid/support/v4/app/P;
.super Ljava/lang/Object;
.source "FragmentManager.java"


# instance fields
.field public final eMR:Landroid/animation/Animator;

.field public final eMS:Landroid/view/animation/Animation;


# direct methods
.method private constructor <init>(Landroid/animation/Animator;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    iput-object p1, p0, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Animator cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/animation/Animator;Landroid/support/v4/app/P;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/app/P;-><init>(Landroid/animation/Animator;)V

    return-void
.end method

.method private constructor <init>(Landroid/view/animation/Animation;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v4/app/P;->eMS:Landroid/view/animation/Animation;

    iput-object v0, p0, Landroid/support/v4/app/P;->eMR:Landroid/animation/Animator;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Animation cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/animation/Animation;Landroid/support/v4/app/P;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/app/P;-><init>(Landroid/view/animation/Animation;)V

    return-void
.end method
