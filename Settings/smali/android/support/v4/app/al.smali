.class final Landroid/support/v4/app/al;
.super Landroid/support/v4/app/N;
.source "BackStackRecord.java"

# interfaces
.implements Landroid/support/v4/app/L;


# instance fields
.field eOA:I

.field eOB:I

.field eOC:I

.field eOD:Ljava/util/ArrayList;

.field eOk:I

.field eOl:I

.field eOm:Ljava/lang/String;

.field eOn:Z

.field eOo:Z

.field eOp:I

.field final eOq:Landroid/support/v4/app/an;

.field eOr:Z

.field eOs:I

.field eOt:Ljava/util/ArrayList;

.field eOu:Ljava/util/ArrayList;

.field eOv:I

.field eOw:Ljava/lang/CharSequence;

.field eOx:Z

.field eOy:Ljava/lang/CharSequence;

.field eOz:I

.field mOps:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/an;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/N;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/al;->eOo:Z

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/al;->eOp:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/al;->eOn:Z

    iput-object p1, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    return-void
.end method

.method private static efX(Landroid/support/v4/app/i;)Z
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Landroid/support/v4/app/p;->eKj:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/support/v4/app/p;->eKg:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Landroid/support/v4/app/p;->eKR:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Landroid/support/v4/app/p;->eKZ:Z

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebG()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private efZ(ILandroid/support/v4/app/p;Ljava/lang/String;I)V
    .locals 4

    invoke-virtual {p2}, Landroid/support/v4/app/p;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/Class;->isAnonymousClass()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " must be a public static class to be  properly recreated from"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " instance state."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iput-object v0, p2, Landroid/support/v4/app/p;->eKu:Landroid/support/v4/app/an;

    if-eqz p3, :cond_3

    iget-object v0, p2, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p2, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can\'t change tag of fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p3, p2, Landroid/support/v4/app/p;->mTag:Ljava/lang/String;

    :cond_3
    if-eqz p1, :cond_6

    const/4 v0, -0x1

    if-ne p1, v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can\'t add fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " with tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to container view with no id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget v0, p2, Landroid/support/v4/app/p;->eKv:I

    if-eqz v0, :cond_5

    iget v0, p2, Landroid/support/v4/app/p;->eKv:I

    if-eq v0, p1, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Can\'t change container ID of fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/support/v4/app/p;->eKv:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iput p1, p2, Landroid/support/v4/app/p;->eKv:I

    iput p1, p2, Landroid/support/v4/app/p;->eKq:I

    :cond_6
    new-instance v0, Landroid/support/v4/app/i;

    invoke-direct {v0, p4, p2}, Landroid/support/v4/app/i;-><init>(ILandroid/support/v4/app/p;)V

    invoke-virtual {p0, v0}, Landroid/support/v4/app/al;->efY(Landroid/support/v4/app/i;)V

    return-void
.end method


# virtual methods
.method public commit()I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/al;->efT(Z)I

    move-result v0

    return v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p3, v0}, Landroid/support/v4/app/al;->efV(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    return-void
.end method

.method public edA(Landroid/support/v4/app/p;)Landroid/support/v4/app/N;
    .locals 2

    new-instance v0, Landroid/support/v4/app/i;

    const/4 v1, 0x6

    invoke-direct {v0, v1, p1}, Landroid/support/v4/app/i;-><init>(ILandroid/support/v4/app/p;)V

    invoke-virtual {p0, v0}, Landroid/support/v4/app/al;->efY(Landroid/support/v4/app/i;)V

    return-object p0
.end method

.method public edB(Landroid/support/v4/app/p;)Landroid/support/v4/app/N;
    .locals 2

    new-instance v0, Landroid/support/v4/app/i;

    const/4 v1, 0x7

    invoke-direct {v0, v1, p1}, Landroid/support/v4/app/i;-><init>(ILandroid/support/v4/app/p;)V

    invoke-virtual {p0, v0}, Landroid/support/v4/app/al;->efY(Landroid/support/v4/app/i;)V

    return-object p0
.end method

.method public edC(ILandroid/support/v4/app/p;Ljava/lang/String;)Landroid/support/v4/app/N;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/app/al;->efZ(ILandroid/support/v4/app/p;Ljava/lang/String;I)V

    return-object p0
.end method

.method public edz(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 3

    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Run: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Landroid/support/v4/app/al;->eOx:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v0, p0}, Landroid/support/v4/app/an;->egn(Landroid/support/v4/app/al;)V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method efM(Landroid/support/v4/app/y;)V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    invoke-static {v0}, Landroid/support/v4/app/al;->efX(Landroid/support/v4/app/i;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/p;->ecP(Landroid/support/v4/app/y;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method efN(I)Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    iget-object v4, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-eqz v4, :cond_0

    iget-object v0, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    iget v0, v0, Landroid/support/v4/app/p;->eKq:I

    :goto_1
    if-eqz v0, :cond_1

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    return v1
.end method

.method efO(Ljava/util/ArrayList;Landroid/support/v4/app/p;)Landroid/support/v4/app/p;
    .locals 10

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    iget v2, v0, Landroid/support/v4/app/i;->eJJ:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_1
    iget-object v0, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    iget-object v2, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v2, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-ne v2, p2, :cond_0

    iget-object v2, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    new-instance v3, Landroid/support/v4/app/i;

    iget-object v0, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    const/16 v4, 0x9

    invoke-direct {v3, v4, v0}, Landroid/support/v4/app/i;-><init>(ILandroid/support/v4/app/p;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    const/4 p2, 0x0

    goto :goto_1

    :pswitch_3
    iget-object v6, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    iget v7, v6, Landroid/support/v4/app/p;->eKq:I

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v5, v2

    move-object v3, p2

    move v2, v1

    :goto_2
    if-ltz v5, :cond_3

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/p;

    iget v8, v1, Landroid/support/v4/app/p;->eKq:I

    if-ne v8, v7, :cond_6

    if-ne v1, v6, :cond_1

    const/4 v1, 0x1

    :goto_3
    add-int/lit8 v4, v5, -0x1

    move v5, v4

    move v4, v1

    goto :goto_2

    :cond_1
    if-ne v1, v3, :cond_2

    iget-object v3, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    new-instance v8, Landroid/support/v4/app/i;

    const/16 v9, 0x9

    invoke-direct {v8, v9, v1}, Landroid/support/v4/app/i;-><init>(ILandroid/support/v4/app/p;)V

    invoke-virtual {v3, v2, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x0

    :cond_2
    new-instance v8, Landroid/support/v4/app/i;

    const/4 v9, 0x3

    invoke-direct {v8, v9, v1}, Landroid/support/v4/app/i;-><init>(ILandroid/support/v4/app/p;)V

    iget v9, v0, Landroid/support/v4/app/i;->eJO:I

    iput v9, v8, Landroid/support/v4/app/i;->eJO:I

    iget v9, v0, Landroid/support/v4/app/i;->eJL:I

    iput v9, v8, Landroid/support/v4/app/i;->eJL:I

    iget v9, v0, Landroid/support/v4/app/i;->eJN:I

    iput v9, v8, Landroid/support/v4/app/i;->eJN:I

    iget v9, v0, Landroid/support/v4/app/i;->eJM:I

    iput v9, v8, Landroid/support/v4/app/i;->eJM:I

    iget-object v9, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v9, v2, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    move v1, v4

    goto :goto_3

    :cond_3
    if-eqz v4, :cond_4

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v2, v2, -0x1

    :goto_4
    move v1, v2

    move-object p2, v3

    goto/16 :goto_1

    :cond_4
    const/4 v1, 0x1

    iput v1, v0, Landroid/support/v4/app/i;->eJJ:I

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :pswitch_4
    iget-object v2, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    new-instance v3, Landroid/support/v4/app/i;

    const/16 v4, 0x9

    invoke-direct {v3, v4, p2}, Landroid/support/v4/app/i;-><init>(ILandroid/support/v4/app/p;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    iget-object p2, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    goto/16 :goto_1

    :cond_5
    return-object p2

    :cond_6
    move v1, v4

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method efP(Ljava/util/ArrayList;Landroid/support/v4/app/p;)Landroid/support/v4/app/p;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    iget v2, v0, Landroid/support/v4/app/i;->eJJ:I

    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_1
    iget-object v0, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    iget-object v0, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_3
    iget-object p2, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    goto :goto_1

    :pswitch_4
    const/4 p2, 0x0

    goto :goto_1

    :cond_0
    return-object p2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public efQ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/al;->eOm:Ljava/lang/String;

    return-object v0
.end method

.method efR(I)V
    .locals 6

    iget-boolean v0, p0, Landroid/support/v4/app/al;->eOx:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Bump nesting in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    iget-object v3, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-eqz v3, :cond_2

    iget-object v3, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    iget v4, v3, Landroid/support/v4/app/p;->eKz:I

    add-int/2addr v4, p1

    iput v4, v3, Landroid/support/v4/app/p;->eKz:I

    sget-boolean v3, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v3, :cond_2

    const-string/jumbo v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Bump nesting of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    iget v0, v0, Landroid/support/v4/app/p;->eKz:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return-void
.end method

.method efS(Ljava/util/ArrayList;II)Z
    .locals 10

    const/4 v3, 0x0

    if-ne p3, p2, :cond_0

    return v3

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v1, -0x1

    move v6, v3

    :goto_0
    if-ge v6, v7, :cond_6

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    iget-object v2, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-eqz v2, :cond_1

    iget-object v0, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    iget v2, v0, Landroid/support/v4/app/p;->eKq:I

    :goto_1
    if-eqz v2, :cond_2

    if-eq v2, v1, :cond_2

    move v5, p2

    :goto_2
    if-ge v5, p3, :cond_7

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    iget-object v1, v0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v4, v3

    :goto_3
    if-ge v4, v8, :cond_5

    iget-object v1, v0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/i;

    iget-object v9, v1, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-eqz v9, :cond_3

    iget-object v1, v1, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    iget v1, v1, Landroid/support/v4/app/p;->eKq:I

    :goto_4
    if-ne v1, v2, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_1
    move v2, v3

    goto :goto_1

    :cond_2
    move v0, v1

    :goto_5
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v1, v0

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_4

    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_6
    return v3

    :cond_7
    move v0, v2

    goto :goto_5
.end method

.method efT(Z)I
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Landroid/support/v4/app/al;->eOr:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "commit already called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-boolean v0, Landroid/support/v4/app/an;->ePd:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Commit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/support/v4/a/q;

    const-string/jumbo v1, "FragmentManager"

    invoke-direct {v0, v1}, Landroid/support/v4/a/q;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    const-string/jumbo v0, "  "

    invoke-virtual {p0, v0, v3, v1, v3}, Landroid/support/v4/app/al;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/al;->eOr:Z

    iget-boolean v0, p0, Landroid/support/v4/app/al;->eOx:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v0, p0}, Landroid/support/v4/app/an;->egC(Landroid/support/v4/app/al;)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/al;->eOp:I

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/an;->ehc(Landroid/support/v4/app/L;Z)V

    iget v0, p0, Landroid/support/v4/app/al;->eOp:I

    return v0

    :cond_2
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/al;->eOp:I

    goto :goto_0
.end method

.method public efU()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v4/app/al;->eOD:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/app/al;->eOD:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/al;->eOD:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iput-object v3, p0, Landroid/support/v4/app/al;->eOD:Ljava/util/ArrayList;

    :cond_1
    return-void
.end method

.method public efV(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .locals 5

    const/4 v0, 0x0

    if-eqz p3, :cond_8

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mName="

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v4/app/al;->eOm:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, " mIndex="

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/al;->eOp:I

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v1, " mCommitted="

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v1, p0, Landroid/support/v4/app/al;->eOr:Z

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Z)V

    iget v1, p0, Landroid/support/v4/app/al;->eOA:I

    if-eqz v1, :cond_0

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mTransition=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/al;->eOA:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, " mTransitionStyle=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/al;->eOz:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    iget v1, p0, Landroid/support/v4/app/al;->eOC:I

    if-nez v1, :cond_1

    iget v1, p0, Landroid/support/v4/app/al;->eOk:I

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mEnterAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/al;->eOC:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, " mExitAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/al;->eOk:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_2
    iget v1, p0, Landroid/support/v4/app/al;->eOl:I

    if-nez v1, :cond_3

    iget v1, p0, Landroid/support/v4/app/al;->eOv:I

    if-eqz v1, :cond_4

    :cond_3
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mPopEnterAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/al;->eOl:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, " mPopExitAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/al;->eOv:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_4
    iget v1, p0, Landroid/support/v4/app/al;->eOB:I

    if-nez v1, :cond_5

    iget-object v1, p0, Landroid/support/v4/app/al;->eOw:Ljava/lang/CharSequence;

    if-eqz v1, :cond_6

    :cond_5
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mBreadCrumbTitleRes=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/al;->eOB:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, " mBreadCrumbTitleText="

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v4/app/al;->eOw:Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_6
    iget v1, p0, Landroid/support/v4/app/al;->eOs:I

    if-nez v1, :cond_7

    iget-object v1, p0, Landroid/support/v4/app/al;->eOy:Ljava/lang/CharSequence;

    if-eqz v1, :cond_8

    :cond_7
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mBreadCrumbShortTitleRes=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/v4/app/al;->eOs:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, " mBreadCrumbShortTitleText="

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v4/app/al;->eOy:Ljava/lang/CharSequence;

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_8
    iget-object v1, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "Operations:"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "    "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v1, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_d

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    iget v1, v0, Landroid/support/v4/app/i;->eJJ:I

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "cmd="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, v0, Landroid/support/v4/app/i;->eJJ:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v4, "  Op #"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string/jumbo v4, ": "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, " "

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    if-eqz p3, :cond_c

    iget v1, v0, Landroid/support/v4/app/i;->eJO:I

    if-nez v1, :cond_9

    iget v1, v0, Landroid/support/v4/app/i;->eJN:I

    if-eqz v1, :cond_a

    :cond_9
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "enterAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, v0, Landroid/support/v4/app/i;->eJO:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, " exitAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, v0, Landroid/support/v4/app/i;->eJN:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_a
    iget v1, v0, Landroid/support/v4/app/i;->eJL:I

    if-nez v1, :cond_b

    iget v1, v0, Landroid/support/v4/app/i;->eJM:I

    if-eqz v1, :cond_c

    :cond_b
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "popEnterAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, v0, Landroid/support/v4/app/i;->eJL:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, " popExitAnim=#"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, v0, Landroid/support/v4/app/i;->eJM:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_c
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :pswitch_0
    const-string/jumbo v1, "NULL"

    goto :goto_1

    :pswitch_1
    const-string/jumbo v1, "ADD"

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "REPLACE"

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v1, "REMOVE"

    goto/16 :goto_1

    :pswitch_4
    const-string/jumbo v1, "HIDE"

    goto/16 :goto_1

    :pswitch_5
    const-string/jumbo v1, "SHOW"

    goto/16 :goto_1

    :pswitch_6
    const-string/jumbo v1, "DETACH"

    goto/16 :goto_1

    :pswitch_7
    const-string/jumbo v1, "ATTACH"

    goto/16 :goto_1

    :pswitch_8
    const-string/jumbo v1, "SET_PRIMARY_NAV"

    goto/16 :goto_1

    :pswitch_9
    const-string/jumbo v1, "UNSET_PRIMARY_NAV"

    goto/16 :goto_1

    :cond_d
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method efW()V
    .locals 9

    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    iget-object v4, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-eqz v4, :cond_0

    iget v5, p0, Landroid/support/v4/app/al;->eOA:I

    iget v6, p0, Landroid/support/v4/app/al;->eOz:I

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/p;->ecI(II)V

    :cond_0
    iget v5, v0, Landroid/support/v4/app/i;->eJJ:I

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unknown cmd: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Landroid/support/v4/app/i;->eJJ:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    iget v5, v0, Landroid/support/v4/app/i;->eJO:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v5, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v5, v4, v2}, Landroid/support/v4/app/an;->ehE(Landroid/support/v4/app/p;Z)V

    :goto_1
    iget-boolean v5, p0, Landroid/support/v4/app/al;->eOn:Z

    if-nez v5, :cond_1

    iget v0, v0, Landroid/support/v4/app/i;->eJJ:I

    if-eq v0, v8, :cond_1

    if-eqz v4, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v0, v4}, Landroid/support/v4/app/an;->egx(Landroid/support/v4/app/p;)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_2
    iget v5, v0, Landroid/support/v4/app/i;->eJN:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v5, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v5, v4}, Landroid/support/v4/app/an;->ehr(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_3
    iget v5, v0, Landroid/support/v4/app/i;->eJN:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v5, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v5, v4}, Landroid/support/v4/app/an;->ehV(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_4
    iget v5, v0, Landroid/support/v4/app/i;->eJO:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v5, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v5, v4}, Landroid/support/v4/app/an;->egO(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_5
    iget v5, v0, Landroid/support/v4/app/i;->eJN:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v5, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v5, v4}, Landroid/support/v4/app/an;->ehv(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_6
    iget v5, v0, Landroid/support/v4/app/i;->eJO:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v5, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v5, v4}, Landroid/support/v4/app/an;->egt(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_7
    iget-object v5, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v5, v4}, Landroid/support/v4/app/an;->ehZ(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_8
    iget-object v5, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v5, v7}, Landroid/support/v4/app/an;->ehZ(Landroid/support/v4/app/p;)V

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Landroid/support/v4/app/al;->eOn:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iget-object v1, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iget v1, v1, Landroid/support/v4/app/an;->ePg:I

    invoke-virtual {v0, v1, v8}, Landroid/support/v4/app/an;->egp(IZ)V

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method efY(Landroid/support/v4/app/i;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, p0, Landroid/support/v4/app/al;->eOC:I

    iput v0, p1, Landroid/support/v4/app/i;->eJO:I

    iget v0, p0, Landroid/support/v4/app/al;->eOk:I

    iput v0, p1, Landroid/support/v4/app/i;->eJN:I

    iget v0, p0, Landroid/support/v4/app/al;->eOl:I

    iput v0, p1, Landroid/support/v4/app/i;->eJL:I

    iget v0, p0, Landroid/support/v4/app/al;->eOv:I

    iput v0, p1, Landroid/support/v4/app/i;->eJM:I

    return-void
.end method

.method ega(Z)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    iget-object v2, v0, Landroid/support/v4/app/i;->eJK:Landroid/support/v4/app/p;

    if-eqz v2, :cond_0

    iget v3, p0, Landroid/support/v4/app/al;->eOA:I

    invoke-static {v3}, Landroid/support/v4/app/an;->ehY(I)I

    move-result v3

    iget v4, p0, Landroid/support/v4/app/al;->eOz:I

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/p;->ecI(II)V

    :cond_0
    iget v3, v0, Landroid/support/v4/app/i;->eJJ:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unknown cmd: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Landroid/support/v4/app/i;->eJJ:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    iget v3, v0, Landroid/support/v4/app/i;->eJM:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v3, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v3, v2}, Landroid/support/v4/app/an;->ehr(Landroid/support/v4/app/p;)V

    :goto_1
    iget-boolean v3, p0, Landroid/support/v4/app/al;->eOn:Z

    if-nez v3, :cond_1

    iget v0, v0, Landroid/support/v4/app/i;->eJJ:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    if-eqz v2, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v0, v2}, Landroid/support/v4/app/an;->egx(Landroid/support/v4/app/p;)V

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :pswitch_2
    iget v3, v0, Landroid/support/v4/app/i;->eJL:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v3, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v3, v2, v6}, Landroid/support/v4/app/an;->ehE(Landroid/support/v4/app/p;Z)V

    goto :goto_1

    :pswitch_3
    iget v3, v0, Landroid/support/v4/app/i;->eJL:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v3, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v3, v2}, Landroid/support/v4/app/an;->egO(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_4
    iget v3, v0, Landroid/support/v4/app/i;->eJM:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v3, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v3, v2}, Landroid/support/v4/app/an;->ehV(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_5
    iget v3, v0, Landroid/support/v4/app/i;->eJL:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v3, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v3, v2}, Landroid/support/v4/app/an;->egt(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_6
    iget v3, v0, Landroid/support/v4/app/i;->eJM:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/p;->ecq(I)V

    iget-object v3, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v3, v2}, Landroid/support/v4/app/an;->ehv(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_7
    iget-object v3, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v3, v5}, Landroid/support/v4/app/an;->ehZ(Landroid/support/v4/app/p;)V

    goto :goto_1

    :pswitch_8
    iget-object v3, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-virtual {v3, v2}, Landroid/support/v4/app/an;->ehZ(Landroid/support/v4/app/p;)V

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Landroid/support/v4/app/al;->eOn:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    iget-object v0, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iget-object v1, p0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iget v1, v1, Landroid/support/v4/app/an;->ePg:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/an;->egp(IZ)V

    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method egb()Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/al;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/i;

    invoke-static {v0}, Landroid/support/v4/app/al;->efX(Landroid/support/v4/app/i;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string/jumbo v1, "BackStackEntry{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Landroid/support/v4/app/al;->eOp:I

    if-ltz v1, :cond_0

    const-string/jumbo v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Landroid/support/v4/app/al;->eOp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/al;->eOm:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/support/v4/app/al;->eOm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
