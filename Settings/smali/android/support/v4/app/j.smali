.class final Landroid/support/v4/app/j;
.super Landroid/animation/AnimatorListenerAdapter;
.source "FragmentManager.java"


# instance fields
.field final synthetic eJP:Landroid/support/v4/app/an;

.field final synthetic eJQ:Landroid/view/View;

.field final synthetic eJR:Landroid/support/v4/app/p;

.field final synthetic eJS:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/support/v4/app/an;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/v4/app/p;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/app/j;->eJP:Landroid/support/v4/app/an;

    iput-object p2, p0, Landroid/support/v4/app/j;->eJS:Landroid/view/ViewGroup;

    iput-object p3, p0, Landroid/support/v4/app/j;->eJQ:Landroid/view/View;

    iput-object p4, p0, Landroid/support/v4/app/j;->eJR:Landroid/support/v4/app/p;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v4/app/j;->eJS:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v4/app/j;->eJQ:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->endViewTransition(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v4/app/j;->eJR:Landroid/support/v4/app/p;

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebs()Landroid/animation/Animator;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/j;->eJR:Landroid/support/v4/app/p;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/p;->ecd(Landroid/animation/Animator;)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/j;->eJS:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v4/app/j;->eJQ:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/j;->eJP:Landroid/support/v4/app/an;

    iget-object v1, p0, Landroid/support/v4/app/j;->eJR:Landroid/support/v4/app/p;

    iget-object v2, p0, Landroid/support/v4/app/j;->eJR:Landroid/support/v4/app/p;

    invoke-virtual {v2}, Landroid/support/v4/app/p;->ecC()I

    move-result v2

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/app/an;->egK(Landroid/support/v4/app/p;IIIZ)V

    :cond_0
    return-void
.end method
