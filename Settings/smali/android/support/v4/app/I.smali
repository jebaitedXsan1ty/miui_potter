.class Landroid/support/v4/app/I;
.super Landroid/support/v4/app/C;
.source "FragmentActivity.java"


# instance fields
.field final synthetic eMt:Landroid/support/v4/app/S;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/S;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-direct {p0, p1}, Landroid/support/v4/app/C;-><init>(Landroid/support/v4/app/S;)V

    return-void
.end method


# virtual methods
.method public ebe(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/S;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public ebf()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v1}, Landroid/support/v4/app/S;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public eda(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/app/S;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method public edd()V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v0}, Landroid/support/v4/app/S;->edF()V

    return-void
.end method

.method public edh()I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v0}, Landroid/support/v4/app/S;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    goto :goto_0
.end method

.method public edi(Landroid/support/v4/app/p;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v0}, Landroid/support/v4/app/S;->isFinishing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public edj(Landroid/support/v4/app/p;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/S;->edJ(Landroid/support/v4/app/p;)V

    return-void
.end method

.method public edk()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v0}, Landroid/support/v4/app/S;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public edm()Landroid/view/LayoutInflater;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v0}, Landroid/support/v4/app/S;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/I;->eMt:Landroid/support/v4/app/S;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method
