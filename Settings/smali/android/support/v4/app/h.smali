.class public final Landroid/support/v4/app/h;
.super Ljava/lang/Object;
.source "RemoteInput.java"


# instance fields
.field private final eJE:Ljava/util/Set;

.field private final eJF:Z

.field private final eJG:[Ljava/lang/CharSequence;

.field private final eJH:Ljava/lang/CharSequence;

.field private final eJI:Ljava/lang/String;

.field private final mExtras:Landroid/os/Bundle;


# direct methods
.method static eaX([Landroid/support/v4/app/h;)[Landroid/app/RemoteInput;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    array-length v0, p0

    new-array v1, v0, [Landroid/app/RemoteInput;

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    aget-object v2, p0, v0

    invoke-static {v2}, Landroid/support/v4/app/h;->ebb(Landroid/support/v4/app/h;)Landroid/app/RemoteInput;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method static ebb(Landroid/support/v4/app/h;)Landroid/app/RemoteInput;
    .locals 2

    new-instance v0, Landroid/app/RemoteInput$Builder;

    invoke-virtual {p0}, Landroid/support/v4/app/h;->eaZ()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/RemoteInput$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v4/app/h;->ebc()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/RemoteInput$Builder;->setLabel(Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/h;->ebd()[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/RemoteInput$Builder;->setChoices([Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/h;->eba()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/RemoteInput$Builder;->setAllowFreeFormInput(Z)Landroid/app/RemoteInput$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/h;->eaY()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/RemoteInput$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/RemoteInput$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/RemoteInput$Builder;->build()Landroid/app/RemoteInput;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public eaW()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/h;->eJE:Ljava/util/Set;

    return-object v0
.end method

.method public eaY()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/h;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method public eaZ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/h;->eJI:Ljava/lang/String;

    return-object v0
.end method

.method public eba()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/app/h;->eJF:Z

    return v0
.end method

.method public ebc()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/h;->eJH:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public ebd()[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/h;->eJG:[Ljava/lang/CharSequence;

    return-object v0
.end method
