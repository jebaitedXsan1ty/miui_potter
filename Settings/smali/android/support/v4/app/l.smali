.class Landroid/support/v4/app/l;
.super Ljava/lang/Object;
.source "FragmentManager.java"

# interfaces
.implements Landroid/support/v4/app/y;


# instance fields
.field private final eJT:Z

.field private eJU:I

.field private final eJV:Landroid/support/v4/app/al;


# direct methods
.method constructor <init>(Landroid/support/v4/app/al;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p2, p0, Landroid/support/v4/app/l;->eJT:Z

    iput-object p1, p0, Landroid/support/v4/app/l;->eJV:Landroid/support/v4/app/al;

    return-void
.end method

.method static synthetic ebj(Landroid/support/v4/app/l;)Landroid/support/v4/app/al;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/l;->eJV:Landroid/support/v4/app/al;

    return-object v0
.end method

.method static synthetic ebn(Landroid/support/v4/app/l;)Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/app/l;->eJT:Z

    return v0
.end method


# virtual methods
.method public ebh()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v4/app/l;->eJU:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public ebi()V
    .locals 1

    iget v0, p0, Landroid/support/v4/app/l;->eJU:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v4/app/l;->eJU:I

    iget v0, p0, Landroid/support/v4/app/l;->eJU:I

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/l;->eJV:Landroid/support/v4/app/al;

    iget-object v0, v0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    invoke-static {v0}, Landroid/support/v4/app/an;->ego(Landroid/support/v4/app/an;)V

    return-void
.end method

.method public ebk()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/support/v4/app/l;->eJV:Landroid/support/v4/app/al;

    iget-object v0, v0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iget-object v1, p0, Landroid/support/v4/app/l;->eJV:Landroid/support/v4/app/al;

    iget-boolean v2, p0, Landroid/support/v4/app/l;->eJT:Z

    invoke-static {v0, v1, v2, v3, v3}, Landroid/support/v4/app/an;->eht(Landroid/support/v4/app/an;Landroid/support/v4/app/al;ZZZ)V

    return-void
.end method

.method public ebl()V
    .locals 7

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v4/app/l;->eJU:I

    if-lez v1, :cond_1

    move v1, v2

    :goto_0
    iget-object v3, p0, Landroid/support/v4/app/l;->eJV:Landroid/support/v4/app/al;

    iget-object v4, v3, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iget-object v3, v4, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_2

    iget-object v0, v4, Landroid/support/v4/app/an;->eOM:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/p;

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/support/v4/app/p;->ecP(Landroid/support/v4/app/y;)V

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ebG()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/p;->ecJ()V

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/l;->eJV:Landroid/support/v4/app/al;

    iget-object v0, v0, Landroid/support/v4/app/al;->eOq:Landroid/support/v4/app/an;

    iget-object v3, p0, Landroid/support/v4/app/l;->eJV:Landroid/support/v4/app/al;

    iget-boolean v4, p0, Landroid/support/v4/app/l;->eJT:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v3, v4, v1, v2}, Landroid/support/v4/app/an;->eht(Landroid/support/v4/app/an;Landroid/support/v4/app/al;ZZZ)V

    return-void
.end method

.method public ebm()V
    .locals 1

    iget v0, p0, Landroid/support/v4/app/l;->eJU:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/app/l;->eJU:I

    return-void
.end method
