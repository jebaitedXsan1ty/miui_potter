.class public interface abstract Landroid/support/v4/media/session/i;
.super Ljava/lang/Object;
.source "IMediaSession.java"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract ejK(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract ejL()Ljava/lang/String;
.end method

.method public abstract ejM()Z
.end method

.method public abstract ejN(Z)V
.end method

.method public abstract ejO(Landroid/support/v4/media/MediaDescriptionCompat;)V
.end method

.method public abstract ejP(IILjava/lang/String;)V
.end method

.method public abstract ejQ()Landroid/support/v4/media/MediaMetadataCompat;
.end method

.method public abstract ejR(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract ejS(Landroid/support/v4/media/MediaDescriptionCompat;)V
.end method

.method public abstract ejT()Ljava/lang/String;
.end method

.method public abstract ejU()V
.end method

.method public abstract ejV(Z)V
.end method

.method public abstract ejW()V
.end method

.method public abstract ejX(I)V
.end method

.method public abstract ejY()Landroid/app/PendingIntent;
.end method

.method public abstract ejZ(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract ekA()I
.end method

.method public abstract ekB(Landroid/net/Uri;Landroid/os/Bundle;)V
.end method

.method public abstract ekC()Z
.end method

.method public abstract ekD(Landroid/support/v4/media/session/y;)V
.end method

.method public abstract eka(Landroid/net/Uri;Landroid/os/Bundle;)V
.end method

.method public abstract ekb(I)V
.end method

.method public abstract ekc(Landroid/support/v4/media/RatingCompat;)V
.end method

.method public abstract ekd(J)V
.end method

.method public abstract eke(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract ekf()Landroid/support/v4/media/session/PlaybackStateCompat;
.end method

.method public abstract ekg()Ljava/util/List;
.end method

.method public abstract ekh()Landroid/support/v4/media/session/ParcelableVolumeInfo;
.end method

.method public abstract eki()J
.end method

.method public abstract ekj(IILjava/lang/String;)V
.end method

.method public abstract ekk(Landroid/support/v4/media/session/y;)V
.end method

.method public abstract ekl()V
.end method

.method public abstract ekm(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract ekn()Ljava/lang/CharSequence;
.end method

.method public abstract eko(Landroid/support/v4/media/MediaDescriptionCompat;I)V
.end method

.method public abstract ekp()V
.end method

.method public abstract ekq(Landroid/view/KeyEvent;)Z
.end method

.method public abstract ekr()Z
.end method

.method public abstract eks(Landroid/support/v4/media/RatingCompat;Landroid/os/Bundle;)V
.end method

.method public abstract ekt(I)V
.end method

.method public abstract eku(Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;)V
.end method

.method public abstract ekv()Landroid/os/Bundle;
.end method

.method public abstract ekw()I
.end method

.method public abstract ekx()I
.end method

.method public abstract eky()V
.end method

.method public abstract ekz(J)V
.end method

.method public abstract next()V
.end method

.method public abstract pause()V
.end method

.method public abstract stop()V
.end method
