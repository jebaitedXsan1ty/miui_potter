.class public interface abstract Landroid/support/v4/media/session/y;
.super Ljava/lang/Object;
.source "IMediaControllerCallback.java"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract ejh(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract eji(Z)V
.end method

.method public abstract ejj(Z)V
.end method

.method public abstract ejk(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
.end method

.method public abstract ejl(I)V
.end method

.method public abstract ejm()V
.end method

.method public abstract ejn(Landroid/support/v4/media/session/PlaybackStateCompat;)V
.end method

.method public abstract ejo(Landroid/support/v4/media/MediaMetadataCompat;)V
.end method

.method public abstract ejp(I)V
.end method

.method public abstract onExtrasChanged(Landroid/os/Bundle;)V
.end method

.method public abstract onQueueChanged(Ljava/util/List;)V
.end method

.method public abstract onQueueTitleChanged(Ljava/lang/CharSequence;)V
.end method

.method public abstract onSessionDestroyed()V
.end method
