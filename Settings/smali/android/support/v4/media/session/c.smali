.class public abstract Landroid/support/v4/media/session/c;
.super Ljava/lang/Object;
.source "MediaControllerCompat.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field eQg:Landroid/support/v4/media/session/j;

.field private final eQh:Ljava/lang/Object;

.field eQi:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/media/session/q;

    invoke-direct {v0, p0}, Landroid/support/v4/media/session/q;-><init>(Landroid/support/v4/media/session/c;)V

    invoke-static {v0}, Landroid/support/v4/media/session/t;->ekP(Landroid/support/v4/media/session/x;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/session/c;->eQh:Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/support/v4/media/session/b;

    invoke-direct {v0, p0}, Landroid/support/v4/media/session/b;-><init>(Landroid/support/v4/media/session/c;)V

    iput-object v0, p0, Landroid/support/v4/media/session/c;->eQh:Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public binderDied()V
    .locals 0

    invoke-virtual {p0}, Landroid/support/v4/media/session/c;->onSessionDestroyed()V

    return-void
.end method

.method public ejq(Landroid/support/v4/media/session/z;)V
    .locals 0

    return-void
.end method

.method public ejr(Z)V
    .locals 0

    return-void
.end method

.method public ejs()V
    .locals 0

    return-void
.end method

.method public ejt(I)V
    .locals 0

    return-void
.end method

.method public eju(I)V
    .locals 0

    return-void
.end method

.method public ejv(Z)V
    .locals 0

    return-void
.end method

.method public ejw(Landroid/support/v4/media/session/PlaybackStateCompat;)V
    .locals 0

    return-void
.end method

.method public ejx(Landroid/support/v4/media/MediaMetadataCompat;)V
    .locals 0

    return-void
.end method

.method ejy(ILjava/lang/Object;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/session/c;->eQg:Landroid/support/v4/media/session/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/media/session/c;->eQg:Landroid/support/v4/media/session/j;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/media/session/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public onExtrasChanged(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onQueueChanged(Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public onQueueTitleChanged(Ljava/lang/CharSequence;)V
    .locals 0

    return-void
.end method

.method public onSessionDestroyed()V
    .locals 0

    return-void
.end method

.method public onSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method
