.class Landroid/support/v4/media/l;
.super Landroid/media/browse/MediaBrowser$ItemCallback;
.source "MediaBrowserCompatApi23.java"


# instance fields
.field protected final eQZ:Landroid/support/v4/media/t;


# direct methods
.method public constructor <init>(Landroid/support/v4/media/t;)V
    .locals 0

    invoke-direct {p0}, Landroid/media/browse/MediaBrowser$ItemCallback;-><init>()V

    iput-object p1, p0, Landroid/support/v4/media/l;->eQZ:Landroid/support/v4/media/t;

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/media/l;->eQZ:Landroid/support/v4/media/t;

    invoke-interface {v0, p1}, Landroid/support/v4/media/t;->onError(Ljava/lang/String;)V

    return-void
.end method

.method public onItemLoaded(Landroid/media/browse/MediaBrowser$MediaItem;)V
    .locals 2

    const/4 v1, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/media/l;->eQZ:Landroid/support/v4/media/t;

    invoke-interface {v0, v1}, Landroid/support/v4/media/t;->elB(Landroid/os/Parcel;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/media/browse/MediaBrowser$MediaItem;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v1, p0, Landroid/support/v4/media/l;->eQZ:Landroid/support/v4/media/t;

    invoke-interface {v1, v0}, Landroid/support/v4/media/t;->elB(Landroid/os/Parcel;)V

    goto :goto_0
.end method
