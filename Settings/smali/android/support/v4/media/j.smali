.class public abstract Landroid/support/v4/media/j;
.super Ljava/lang/Object;
.source "MediaBrowserCompat.java"


# instance fields
.field final eQY:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/media/p;

    invoke-direct {v0, p0}, Landroid/support/v4/media/p;-><init>(Landroid/support/v4/media/j;)V

    invoke-static {v0}, Landroid/support/v4/media/a;->ekR(Landroid/support/v4/media/t;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/j;->eQY:Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/media/j;->eQY:Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public elh(Landroid/support/v4/media/MediaBrowserCompat$MediaItem;)V
    .locals 0

    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
