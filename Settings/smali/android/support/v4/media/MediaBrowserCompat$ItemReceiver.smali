.class Landroid/support/v4/media/MediaBrowserCompat$ItemReceiver;
.super Landroid/support/v4/os/ResultReceiver;
.source "MediaBrowserCompat.java"


# instance fields
.field private final eRx:Ljava/lang/String;

.field private final eRy:Landroid/support/v4/media/j;


# virtual methods
.method protected onReceiveResult(ILandroid/os/Bundle;)V
    .locals 2

    if-eqz p2, :cond_0

    const-class v0, Landroid/support/v4/media/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    iget-object v0, p0, Landroid/support/v4/media/MediaBrowserCompat$ItemReceiver;->eRy:Landroid/support/v4/media/j;

    iget-object v1, p0, Landroid/support/v4/media/MediaBrowserCompat$ItemReceiver;->eRx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/media/j;->onError(Ljava/lang/String;)V

    return-void

    :cond_2
    const-string/jumbo v0, "media_item"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    const-string/jumbo v0, "media_item"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_3

    instance-of v1, v0, Landroid/support/v4/media/MediaBrowserCompat$MediaItem;

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Landroid/support/v4/media/MediaBrowserCompat$ItemReceiver;->eRy:Landroid/support/v4/media/j;

    check-cast v0, Landroid/support/v4/media/MediaBrowserCompat$MediaItem;

    invoke-virtual {v1, v0}, Landroid/support/v4/media/j;->elh(Landroid/support/v4/media/MediaBrowserCompat$MediaItem;)V

    :goto_0
    return-void

    :cond_4
    iget-object v0, p0, Landroid/support/v4/media/MediaBrowserCompat$ItemReceiver;->eRy:Landroid/support/v4/media/j;

    iget-object v1, p0, Landroid/support/v4/media/MediaBrowserCompat$ItemReceiver;->eRx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/media/j;->onError(Ljava/lang/String;)V

    goto :goto_0
.end method
