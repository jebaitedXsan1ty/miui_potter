.class public final Landroid/support/v4/h/k;
.super Ljava/lang/Object;
.source "TextDirectionHeuristicsCompat.java"


# static fields
.field public static final eTu:Landroid/support/v4/h/c;

.field public static final eTv:Landroid/support/v4/h/c;

.field public static final eTw:Landroid/support/v4/h/c;

.field public static final eTx:Landroid/support/v4/h/c;

.field public static final eTy:Landroid/support/v4/h/c;

.field public static final eTz:Landroid/support/v4/h/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/support/v4/h/e;

    invoke-direct {v0, v1, v2}, Landroid/support/v4/h/e;-><init>(Landroid/support/v4/h/f;Z)V

    sput-object v0, Landroid/support/v4/h/k;->eTv:Landroid/support/v4/h/c;

    new-instance v0, Landroid/support/v4/h/e;

    invoke-direct {v0, v1, v3}, Landroid/support/v4/h/e;-><init>(Landroid/support/v4/h/f;Z)V

    sput-object v0, Landroid/support/v4/h/k;->eTz:Landroid/support/v4/h/c;

    new-instance v0, Landroid/support/v4/h/e;

    sget-object v1, Landroid/support/v4/h/m;->eTH:Landroid/support/v4/h/m;

    invoke-direct {v0, v1, v2}, Landroid/support/v4/h/e;-><init>(Landroid/support/v4/h/f;Z)V

    sput-object v0, Landroid/support/v4/h/k;->eTu:Landroid/support/v4/h/c;

    new-instance v0, Landroid/support/v4/h/e;

    sget-object v1, Landroid/support/v4/h/m;->eTH:Landroid/support/v4/h/m;

    invoke-direct {v0, v1, v3}, Landroid/support/v4/h/e;-><init>(Landroid/support/v4/h/f;Z)V

    sput-object v0, Landroid/support/v4/h/k;->eTx:Landroid/support/v4/h/c;

    new-instance v0, Landroid/support/v4/h/e;

    sget-object v1, Landroid/support/v4/h/i;->eTs:Landroid/support/v4/h/i;

    invoke-direct {v0, v1, v2}, Landroid/support/v4/h/e;-><init>(Landroid/support/v4/h/f;Z)V

    sput-object v0, Landroid/support/v4/h/k;->eTw:Landroid/support/v4/h/c;

    sget-object v0, Landroid/support/v4/h/j;->eTt:Landroid/support/v4/h/j;

    sput-object v0, Landroid/support/v4/h/k;->eTy:Landroid/support/v4/h/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static eoh(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x2

    return v0

    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    const/4 v0, 0x0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static eoi(I)I
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x2

    return v0

    :sswitch_0
    const/4 v0, 0x1

    return v0

    :sswitch_1
    const/4 v0, 0x0

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0xe -> :sswitch_0
        0xf -> :sswitch_0
        0x10 -> :sswitch_1
        0x11 -> :sswitch_1
    .end sparse-switch
.end method
