.class public Landroid/support/v4/c/l;
.super Ljava/lang/Object;
.source "TypefaceCompat.java"


# static fields
.field private static final eSe:Landroid/support/v4/c/d;

.field private static final eSf:Landroid/support/v4/a/h;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/c/i;

    invoke-direct {v0}, Landroid/support/v4/c/i;-><init>()V

    sput-object v0, Landroid/support/v4/c/l;->eSe:Landroid/support/v4/c/d;

    :goto_0
    new-instance v0, Landroid/support/v4/a/h;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Landroid/support/v4/a/h;-><init>(I)V

    sput-object v0, Landroid/support/v4/c/l;->eSf:Landroid/support/v4/a/h;

    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_1

    invoke-static {}, Landroid/support/v4/c/m;->emV()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/support/v4/c/m;

    invoke-direct {v0}, Landroid/support/v4/c/m;-><init>()V

    sput-object v0, Landroid/support/v4/c/l;->eSe:Landroid/support/v4/c/d;

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2

    new-instance v0, Landroid/support/v4/c/a;

    invoke-direct {v0}, Landroid/support/v4/c/a;-><init>()V

    sput-object v0, Landroid/support/v4/c/l;->eSe:Landroid/support/v4/c/d;

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/support/v4/c/k;

    invoke-direct {v0}, Landroid/support/v4/c/k;-><init>()V

    sput-object v0, Landroid/support/v4/c/l;->eSe:Landroid/support/v4/c/d;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static emQ(Landroid/content/Context;Landroid/support/v4/content/a/e;Landroid/content/res/Resources;IILandroid/widget/TextView;)Landroid/graphics/Typeface;
    .locals 6

    instance-of v0, p1, Landroid/support/v4/content/a/f;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/support/v4/content/a/f;

    invoke-virtual {p1}, Landroid/support/v4/content/a/f;->dZR()Landroid/support/v4/g/k;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/content/a/f;->dZS()I

    move-result v3

    invoke-virtual {p1}, Landroid/support/v4/content/a/f;->dZQ()I

    move-result v4

    move-object v0, p0

    move-object v2, p5

    move v5, p4

    invoke-static/range {v0 .. v5}, Landroid/support/v4/g/i;->enF(Landroid/content/Context;Landroid/support/v4/g/k;Landroid/widget/TextView;III)Landroid/graphics/Typeface;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    sget-object v1, Landroid/support/v4/c/l;->eSf:Landroid/support/v4/a/h;

    invoke-static {p2, p3, p4}, Landroid/support/v4/c/l;->emT(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/a/h;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0

    :cond_1
    sget-object v0, Landroid/support/v4/c/l;->eSe:Landroid/support/v4/c/d;

    check-cast p1, Landroid/support/v4/content/a/b;

    invoke-interface {v0, p0, p1, p2, p4}, Landroid/support/v4/c/d;->emn(Landroid/content/Context;Landroid/support/v4/content/a/b;Landroid/content/res/Resources;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0
.end method

.method public static emR(Landroid/content/Context;Landroid/content/res/Resources;ILjava/lang/String;I)Landroid/graphics/Typeface;
    .locals 6

    sget-object v0, Landroid/support/v4/c/l;->eSe:Landroid/support/v4/c/d;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/c/d;->emo(Landroid/content/Context;Landroid/content/res/Resources;ILjava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Landroid/support/v4/c/l;->eSf:Landroid/support/v4/a/h;

    invoke-static {p1, p2, p4}, Landroid/support/v4/c/l;->emT(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/a/h;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public static emS(Landroid/content/Context;Landroid/os/CancellationSignal;[Landroid/support/v4/g/a;I)Landroid/graphics/Typeface;
    .locals 1

    sget-object v0, Landroid/support/v4/c/l;->eSe:Landroid/support/v4/c/d;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/c/d;->emd(Landroid/content/Context;Landroid/os/CancellationSignal;[Landroid/support/v4/g/a;I)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method private static emT(Landroid/content/res/Resources;II)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static emU(Landroid/content/res/Resources;II)Landroid/graphics/Typeface;
    .locals 2

    sget-object v0, Landroid/support/v4/c/l;->eSf:Landroid/support/v4/a/h;

    invoke-static {p0, p1, p2}, Landroid/support/v4/c/l;->emT(Landroid/content/res/Resources;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/a/h;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    return-object v0
.end method
