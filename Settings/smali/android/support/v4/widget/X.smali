.class public Landroid/support/v4/widget/X;
.super Landroid/support/v4/widget/ag;
.source "ListViewAutoScrollHelper.java"


# instance fields
.field private final eFv:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/widget/ag;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Landroid/support/v4/widget/X;->eFv:Landroid/widget/ListView;

    return-void
.end method


# virtual methods
.method public dXr(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dXs(I)Z
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v4/widget/X;->eFv:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    return v5

    :cond_0
    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v3

    add-int v4, v3, v2

    if-lez p1, :cond_1

    if-lt v4, v1, :cond_3

    add-int/lit8 v1, v2, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeight()I

    move-result v0

    if-gt v1, v0, :cond_3

    return v5

    :cond_1
    if-gez p1, :cond_2

    if-gtz v3, :cond_3

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-ltz v0, :cond_3

    return v5

    :cond_2
    return v5

    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method public dXt(II)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/X;->eFv:Landroid/widget/ListView;

    invoke-static {v0, p2}, Landroid/support/v4/widget/K;->dVL(Landroid/widget/ListView;I)V

    return-void
.end method
