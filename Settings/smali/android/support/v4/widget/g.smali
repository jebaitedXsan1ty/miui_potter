.class public Landroid/support/v4/widget/g;
.super Ljava/lang/Object;
.source "ImageViewCompat.java"


# static fields
.field static final eCu:Landroid/support/v4/widget/Q;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/widget/aE;

    invoke-direct {v0}, Landroid/support/v4/widget/aE;-><init>()V

    sput-object v0, Landroid/support/v4/widget/g;->eCu:Landroid/support/v4/widget/Q;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/support/v4/widget/aa;

    invoke-direct {v0}, Landroid/support/v4/widget/aa;-><init>()V

    sput-object v0, Landroid/support/v4/widget/g;->eCu:Landroid/support/v4/widget/Q;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dTP(Landroid/widget/ImageView;)Landroid/content/res/ColorStateList;
    .locals 1

    sget-object v0, Landroid/support/v4/widget/g;->eCu:Landroid/support/v4/widget/Q;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/Q;->dVQ(Landroid/widget/ImageView;)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public static dTQ(Landroid/widget/ImageView;Landroid/content/res/ColorStateList;)V
    .locals 1

    sget-object v0, Landroid/support/v4/widget/g;->eCu:Landroid/support/v4/widget/Q;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/Q;->dVR(Landroid/widget/ImageView;Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public static dTR(Landroid/widget/ImageView;Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    sget-object v0, Landroid/support/v4/widget/g;->eCu:Landroid/support/v4/widget/Q;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/Q;->dVS(Landroid/widget/ImageView;Landroid/graphics/PorterDuff$Mode;)V

    return-void
.end method

.method public static dTS(Landroid/widget/ImageView;)Landroid/graphics/PorterDuff$Mode;
    .locals 1

    sget-object v0, Landroid/support/v4/widget/g;->eCu:Landroid/support/v4/widget/Q;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/Q;->dVT(Landroid/widget/ImageView;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method
