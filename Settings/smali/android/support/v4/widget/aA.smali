.class Landroid/support/v4/widget/aA;
.super Landroid/support/v4/view/d;
.source "SlidingPaneLayout.java"


# instance fields
.field private final eHk:Landroid/graphics/Rect;

.field final synthetic eHl:Landroid/support/v4/widget/SlidingPaneLayout;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v4/widget/aA;->eHl:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-direct {p0}, Landroid/support/v4/view/d;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/aA;->eHk:Landroid/graphics/Rect;

    return-void
.end method

.method private dYA(Landroid/support/v4/view/a/a;Landroid/support/v4/view/a/a;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/aA;->eHk:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dMU(Landroid/graphics/Rect;)V

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNp(Landroid/graphics/Rect;)V

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dNb(Landroid/graphics/Rect;)V

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNe(Landroid/graphics/Rect;)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dMO()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNq(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNc()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNi(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNu()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNk(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->setEnabled(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNo()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNh(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNn()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dMZ(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->isFocused()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNv(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dMS()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNx(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dMW()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->setSelected(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNy()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dMK(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNC()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNf(I)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNt()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dMJ(I)V

    return-void
.end method


# virtual methods
.method public ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V
    .locals 4

    const/4 v1, 0x0

    invoke-static {p2}, Landroid/support/v4/view/a/a;->dNz(Landroid/support/v4/view/a/a;)Landroid/support/v4/view/a/a;

    move-result-object v0

    invoke-super {p0, p1, v0}, Landroid/support/v4/view/d;->ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V

    invoke-direct {p0, p2, v0}, Landroid/support/v4/widget/aA;->dYA(Landroid/support/v4/view/a/a;Landroid/support/v4/view/a/a;)V

    invoke-virtual {v0}, Landroid/support/v4/view/a/a;->dNl()V

    const-class v0, Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dNk(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, p1}, Landroid/support/v4/view/a/a;->dNs(Landroid/view/View;)V

    invoke-static {p1}, Landroid/support/v4/view/z;->dQa(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dNa(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/aA;->eHl:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildCount()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_2

    iget-object v1, p0, Landroid/support/v4/widget/aA;->eHl:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/aA;->dYB(Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    invoke-static {v1, v3}, Landroid/support/v4/view/z;->dPL(Landroid/view/View;I)V

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/a;->dNw(Landroid/view/View;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public dYB(Landroid/view/View;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/aA;->eHl:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SlidingPaneLayout;->dWb(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/support/v4/view/d;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v0, Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    invoke-virtual {p0, p2}, Landroid/support/v4/widget/aA;->dYB(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/d;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
