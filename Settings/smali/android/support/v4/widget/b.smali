.class Landroid/support/v4/widget/b;
.super Landroid/support/v4/view/a/b;
.source "ExploreByTouchHelper.java"


# instance fields
.field final synthetic eCt:Landroid/support/v4/widget/a;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/a;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/widget/b;->eCt:Landroid/support/v4/widget/a;

    invoke-direct {p0}, Landroid/support/v4/view/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method public createAccessibilityNodeInfo(I)Landroid/support/v4/view/a/a;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/b;->eCt:Landroid/support/v4/widget/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/a;->dTz(I)Landroid/support/v4/view/a/a;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/a/a;->dNz(Landroid/support/v4/view/a/a;)Landroid/support/v4/view/a/a;

    move-result-object v0

    return-object v0
.end method

.method public findFocus(I)Landroid/support/v4/view/a/a;
    .locals 2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/b;->eCt:Landroid/support/v4/widget/a;

    invoke-static {v0}, Landroid/support/v4/widget/a;->dTD(Landroid/support/v4/widget/a;)I

    move-result v0

    :goto_0
    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/b;->eCt:Landroid/support/v4/widget/a;

    invoke-static {v0}, Landroid/support/v4/widget/a;->dTA(Landroid/support/v4/widget/a;)I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/b;->createAccessibilityNodeInfo(I)Landroid/support/v4/view/a/a;

    move-result-object v0

    return-object v0
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/b;->eCt:Landroid/support/v4/widget/a;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/widget/a;->performAction(IILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method
