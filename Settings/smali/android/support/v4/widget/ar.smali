.class Landroid/support/v4/widget/ar;
.super Landroid/support/v4/view/d;
.source "DrawerLayout.java"


# instance fields
.field private final eHe:Landroid/graphics/Rect;

.field final synthetic eHf:Landroid/support/v4/widget/DrawerLayout;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/DrawerLayout;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v4/widget/ar;->eHf:Landroid/support/v4/widget/DrawerLayout;

    invoke-direct {p0}, Landroid/support/v4/view/d;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/ar;->eHe:Landroid/graphics/Rect;

    return-void
.end method

.method private dYv(Landroid/support/v4/view/a/a;Landroid/view/ViewGroup;)V
    .locals 4

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/widget/DrawerLayout;->dYT(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1, v2}, Landroid/support/v4/view/a/a;->dNw(Landroid/view/View;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private dYw(Landroid/support/v4/view/a/a;Landroid/support/v4/view/a/a;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/ar;->eHe:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dMU(Landroid/graphics/Rect;)V

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNp(Landroid/graphics/Rect;)V

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dNb(Landroid/graphics/Rect;)V

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNe(Landroid/graphics/Rect;)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dMO()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNq(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNc()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNi(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNu()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNk(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->setEnabled(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNo()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNh(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNn()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dMZ(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->isFocused()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNv(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dMS()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNx(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dMW()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->setSelected(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNy()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dMK(Z)V

    invoke-virtual {p2}, Landroid/support/v4/view/a/a;->dNC()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/a;->dNf(I)V

    return-void
.end method


# virtual methods
.method public ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Landroid/support/v4/widget/DrawerLayout;->eHN:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/support/v4/view/d;->ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V

    :goto_0
    const-class v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dNk(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v3}, Landroid/support/v4/view/a/a;->dMZ(Z)V

    invoke-virtual {p2, v3}, Landroid/support/v4/view/a/a;->dNv(Z)V

    sget-object v0, Landroid/support/v4/view/a/c;->ewO:Landroid/support/v4/view/a/c;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dMY(Landroid/support/v4/view/a/c;)Z

    sget-object v0, Landroid/support/v4/view/a/c;->ewQ:Landroid/support/v4/view/a/c;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dMY(Landroid/support/v4/view/a/c;)Z

    return-void

    :cond_0
    invoke-static {p2}, Landroid/support/v4/view/a/a;->dNz(Landroid/support/v4/view/a/a;)Landroid/support/v4/view/a/a;

    move-result-object v1

    invoke-super {p0, p1, v1}, Landroid/support/v4/view/d;->ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V

    invoke-virtual {p2, p1}, Landroid/support/v4/view/a/a;->dNs(Landroid/view/View;)V

    invoke-static {p1}, Landroid/support/v4/view/z;->dQa(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/a;->dNa(Landroid/view/View;)V

    :cond_1
    invoke-direct {p0, p2, v1}, Landroid/support/v4/widget/ar;->dYw(Landroid/support/v4/view/a/a;Landroid/support/v4/view/a/a;)V

    invoke-virtual {v1}, Landroid/support/v4/view/a/a;->dNl()V

    check-cast p1, Landroid/view/ViewGroup;

    invoke-direct {p0, p2, p1}, Landroid/support/v4/widget/ar;->dYv(Landroid/support/v4/view/a/a;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/widget/ar;->eHf:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/DrawerLayout;->dZb()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Landroid/support/v4/widget/ar;->eHf:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/DrawerLayout;->dYU(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/v4/widget/ar;->eHf:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/DrawerLayout;->dZp(I)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/d;->dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/support/v4/view/d;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    sget-boolean v0, Landroid/support/v4/widget/DrawerLayout;->eHN:Z

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/support/v4/widget/DrawerLayout;->dYT(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/d;->onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
