.class public abstract Landroid/support/v4/widget/a;
.super Landroid/support/v4/view/d;
.source "ExploreByTouchHelper.java"


# static fields
.field private static final eCg:Landroid/graphics/Rect;

.field private static final eCh:Landroid/support/v4/widget/e;

.field private static final eCp:Landroid/support/v4/widget/f;


# instance fields
.field private eCi:I

.field private final eCj:[I

.field private final eCk:Landroid/graphics/Rect;

.field private final eCl:Landroid/graphics/Rect;

.field private eCm:I

.field private eCn:Landroid/support/v4/widget/b;

.field private final eCo:Landroid/view/View;

.field private eCq:I

.field private final eCr:Landroid/view/accessibility/AccessibilityManager;

.field private final eCs:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const v2, 0x7fffffff

    const/high16 v1, -0x80000000

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v2, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v0, Landroid/support/v4/widget/a;->eCg:Landroid/graphics/Rect;

    new-instance v0, Landroid/support/v4/widget/c;

    invoke-direct {v0}, Landroid/support/v4/widget/c;-><init>()V

    sput-object v0, Landroid/support/v4/widget/a;->eCp:Landroid/support/v4/widget/f;

    new-instance v0, Landroid/support/v4/widget/d;

    invoke-direct {v0}, Landroid/support/v4/widget/d;-><init>()V

    sput-object v0, Landroid/support/v4/widget/a;->eCh:Landroid/support/v4/widget/e;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x1

    const/high16 v1, -0x80000000

    invoke-direct {p0}, Landroid/support/v4/view/d;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/a;->eCl:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/a;->eCk:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/a;->eCs:Landroid/graphics/Rect;

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/v4/widget/a;->eCj:[I

    iput v1, p0, Landroid/support/v4/widget/a;->eCm:I

    iput v1, p0, Landroid/support/v4/widget/a;->eCi:I

    iput v1, p0, Landroid/support/v4/widget/a;->eCq:I

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "View may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Landroid/support/v4/widget/a;->eCr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusable(Z)V

    invoke-static {p1}, Landroid/support/v4/view/z;->dPI(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1, v2}, Landroid/support/v4/view/z;->dPL(Landroid/view/View;I)V

    :cond_1
    return-void
.end method

.method static synthetic dTA(Landroid/support/v4/widget/a;)I
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/a;->eCi:I

    return v0
.end method

.method private dTB(I)Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return v1

    :cond_1
    iget v0, p0, Landroid/support/v4/widget/a;->eCm:I

    if-eq v0, p1, :cond_3

    iget v0, p0, Landroid/support/v4/widget/a;->eCm:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    iget v0, p0, Landroid/support/v4/widget/a;->eCm:I

    invoke-direct {p0, v0}, Landroid/support/v4/widget/a;->dTK(I)Z

    :cond_2
    iput p1, p0, Landroid/support/v4/widget/a;->eCm:I

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    const v0, 0x8000

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/a;->dTJ(II)Z

    const/4 v0, 0x1

    return v0

    :cond_3
    return v1
.end method

.method private dTC(I)Landroid/view/accessibility/AccessibilityEvent;
    .locals 2

    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    return-object v0
.end method

.method static synthetic dTD(Landroid/support/v4/widget/a;)I
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/a;->eCm:I

    return v0
.end method

.method private dTE()Landroid/support/v4/view/a/a;
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-static {v1}, Landroid/support/v4/view/a/a;->dNr(Landroid/view/View;)Landroid/support/v4/view/a/a;

    move-result-object v2

    iget-object v1, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-static {v1, v2}, Landroid/support/v4/view/z;->dPU(Landroid/view/View;Landroid/support/v4/view/a/a;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v3}, Landroid/support/v4/widget/a;->azV(Ljava/util/List;)V

    invoke-virtual {v2}, Landroid/support/v4/view/a/a;->dMI()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Views cannot have both real and virtual children"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    iget-object v5, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v5, v0}, Landroid/support/v4/view/a/a;->dMP(Landroid/view/View;I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private dTG(IILandroid/os/Bundle;)Z
    .locals 1

    sparse-switch p2, :sswitch_data_0

    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v4/widget/a;->azW(IILandroid/os/Bundle;)Z

    move-result v0

    return v0

    :sswitch_0
    invoke-direct {p0, p1}, Landroid/support/v4/widget/a;->dTB(I)Z

    move-result v0

    return v0

    :sswitch_1
    invoke-direct {p0, p1}, Landroid/support/v4/widget/a;->dTK(I)Z

    move-result v0

    return v0

    :sswitch_2
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/a;->dTv(I)Z

    move-result v0

    return v0

    :sswitch_3
    invoke-virtual {p0, p1}, Landroid/support/v4/widget/a;->dTH(I)Z

    move-result v0

    return v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
    .end sparse-switch
.end method

.method private dTI(I)Landroid/support/v4/view/a/a;
    .locals 8

    const/4 v7, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {}, Landroid/support/v4/view/a/a;->dMQ()Landroid/support/v4/view/a/a;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v4/view/a/a;->setEnabled(Z)V

    invoke-virtual {v3, v1}, Landroid/support/v4/view/a/a;->dMZ(Z)V

    const-string/jumbo v0, "android.view.View"

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNk(Ljava/lang/CharSequence;)V

    sget-object v0, Landroid/support/v4/widget/a;->eCg:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNp(Landroid/graphics/Rect;)V

    sget-object v0, Landroid/support/v4/widget/a;->eCg:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNe(Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNa(Landroid/view/View;)V

    invoke-virtual {p0, p1, v3}, Landroid/support/v4/widget/a;->aAa(ILandroid/support/v4/view/a/a;)V

    invoke-virtual {v3}, Landroid/support/v4/view/a/a;->dNm()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v3}, Landroid/support/v4/view/a/a;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Callbacks must add text or a content description in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/a;->eCk:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dMU(Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCk:Landroid/graphics/Rect;

    sget-object v4, Landroid/support/v4/widget/a;->eCg:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Callbacks must set parent bounds in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v3}, Landroid/support/v4/view/a/a;->dNC()I

    move-result v0

    and-int/lit8 v4, v0, 0x40

    if-eqz v4, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNi(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v3, v0, p1}, Landroid/support/v4/view/a/a;->dNj(Landroid/view/View;I)V

    iget v0, p0, Landroid/support/v4/widget/a;->eCm:I

    if-ne v0, p1, :cond_5

    invoke-virtual {v3, v1}, Landroid/support/v4/view/a/a;->dNx(Z)V

    const/16 v0, 0x80

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNf(I)V

    :goto_0
    iget v0, p0, Landroid/support/v4/widget/a;->eCi:I

    if-ne v0, p1, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_7

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/support/v4/view/a/a;->dNf(I)V

    :cond_4
    :goto_2
    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNv(Z)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    iget-object v4, p0, Landroid/support/v4/widget/a;->eCj:[I

    invoke-virtual {v0, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCl:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNb(Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCl:Landroid/graphics/Rect;

    sget-object v4, Landroid/support/v4/widget/a;->eCg:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCl:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dMU(Landroid/graphics/Rect;)V

    iget v0, v3, Landroid/support/v4/view/a/a;->ewF:I

    if-eq v0, v7, :cond_9

    invoke-static {}, Landroid/support/v4/view/a/a;->dMQ()Landroid/support/v4/view/a/a;

    move-result-object v4

    iget v0, v3, Landroid/support/v4/view/a/a;->ewF:I

    :goto_3
    if-eq v0, v7, :cond_8

    iget-object v5, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v4, v5, v7}, Landroid/support/v4/view/a/a;->dNA(Landroid/view/View;I)V

    sget-object v5, Landroid/support/v4/widget/a;->eCg:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/support/v4/view/a/a;->dNp(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v4}, Landroid/support/v4/widget/a;->aAa(ILandroid/support/v4/view/a/a;)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCk:Landroid/graphics/Rect;

    invoke-virtual {v4, v0}, Landroid/support/v4/view/a/a;->dMU(Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCl:Landroid/graphics/Rect;

    iget-object v5, p0, Landroid/support/v4/widget/a;->eCk:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Landroid/support/v4/widget/a;->eCk:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    iget v0, v4, Landroid/support/v4/view/a/a;->ewF:I

    goto :goto_3

    :cond_5
    invoke-virtual {v3, v2}, Landroid/support/v4/view/a/a;->dNx(Z)V

    const/16 v0, 0x40

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNf(I)V

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    invoke-virtual {v3}, Landroid/support/v4/view/a/a;->dNn()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v1}, Landroid/support/v4/view/a/a;->dNf(I)V

    goto :goto_2

    :cond_8
    invoke-virtual {v4}, Landroid/support/v4/view/a/a;->dNl()V

    :cond_9
    iget-object v0, p0, Landroid/support/v4/widget/a;->eCl:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/v4/widget/a;->eCj:[I

    aget v4, v4, v2

    iget-object v5, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getScrollX()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Landroid/support/v4/widget/a;->eCj:[I

    aget v5, v5, v1

    iget-object v6, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getScrollY()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    :cond_a
    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    iget-object v4, p0, Landroid/support/v4/widget/a;->eCs:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCs:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/v4/widget/a;->eCj:[I

    aget v2, v4, v2

    iget-object v4, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getScrollX()I

    move-result v4

    sub-int/2addr v2, v4

    iget-object v4, p0, Landroid/support/v4/widget/a;->eCj:[I

    aget v4, v4, v1

    iget-object v5, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getScrollY()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCl:Landroid/graphics/Rect;

    iget-object v2, p0, Landroid/support/v4/widget/a;->eCs:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCl:Landroid/graphics/Rect;

    invoke-virtual {v3, v0}, Landroid/support/v4/view/a/a;->dNe(Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCl:Landroid/graphics/Rect;

    invoke-direct {p0, v0}, Landroid/support/v4/widget/a;->dTw(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v3, v1}, Landroid/support/v4/view/a/a;->dNq(Z)V

    :cond_b
    return-object v3
.end method

.method private dTK(I)Z
    .locals 1

    iget v0, p0, Landroid/support/v4/widget/a;->eCm:I

    if-ne v0, p1, :cond_0

    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v4/widget/a;->eCm:I

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    const/high16 v0, 0x10000

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/a;->dTJ(II)Z

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private dTL(I)V
    .locals 2

    iget v0, p0, Landroid/support/v4/widget/a;->eCq:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget v0, p0, Landroid/support/v4/widget/a;->eCq:I

    iput p1, p0, Landroid/support/v4/widget/a;->eCq:I

    const/16 v1, 0x80

    invoke-virtual {p0, p1, v1}, Landroid/support/v4/widget/a;->dTJ(II)Z

    const/16 v1, 0x100

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/a;->dTJ(II)Z

    return-void
.end method

.method private dTM(II)Landroid/view/accessibility/AccessibilityEvent;
    .locals 4

    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    invoke-virtual {p0, p1}, Landroid/support/v4/widget/a;->dTz(I)Landroid/support/v4/view/a/a;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/view/a/a;->dNm()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Landroid/support/v4/view/a/a;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/support/v4/view/a/a;->dNg()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    invoke-virtual {v1}, Landroid/support/v4/view/a/a;->dNB()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setPassword(Z)V

    invoke-virtual {v1}, Landroid/support/v4/view/a/a;->isEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    invoke-virtual {v1}, Landroid/support/v4/view/a/a;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/a;->azY(ILandroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Callbacks must add text or a content description in populateEventForVirtualViewId()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v1}, Landroid/support/v4/view/a/a;->dNu()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-static {v0, v1, p1}, Landroid/support/v4/view/a/h;->dNN(Landroid/view/accessibility/AccessibilityRecord;Landroid/view/View;I)V

    iget-object v1, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private dTN(ILandroid/os/Bundle;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-static {v0, p1, p2}, Landroid/support/v4/view/z;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method private dTw(Landroid/graphics/Rect;)Z
    .locals 4

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    return v1

    :cond_2
    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_0
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_5

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-lez v2, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    return v1

    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    :cond_5
    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method private dTx(II)Landroid/view/accessibility/AccessibilityEvent;
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/a;->dTM(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    return-object v0

    :pswitch_0
    invoke-direct {p0, p2}, Landroid/support/v4/widget/a;->dTC(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected abstract aAa(ILandroid/support/v4/view/a/a;)V
.end method

.method protected abstract azT(FF)I
.end method

.method protected abstract azV(Ljava/util/List;)V
.end method

.method protected abstract azW(IILandroid/os/Bundle;)Z
.end method

.method protected azX(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0

    return-void
.end method

.method protected azY(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 0

    return-void
.end method

.method protected azZ(Landroid/support/v4/view/a/a;)V
    .locals 0

    return-void
.end method

.method public ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/support/v4/view/d;->ccc(Landroid/view/View;Landroid/support/v4/view/a/a;)V

    invoke-virtual {p0, p2}, Landroid/support/v4/widget/a;->azZ(Landroid/support/v4/view/a/a;)V

    return-void
.end method

.method public final dTF(II)V
    .locals 3

    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x800

    invoke-direct {p0, p1, v1}, Landroid/support/v4/widget/a;->dTx(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/support/v4/view/a/i;->dNP(Landroid/view/accessibility/AccessibilityEvent;I)V

    iget-object v2, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-static {v0, v2, v1}, Landroid/support/v4/view/au;->dRU(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    :cond_0
    return-void
.end method

.method public final dTH(I)Z
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Landroid/support/v4/widget/a;->eCi:I

    if-eq v0, p1, :cond_0

    return v1

    :cond_0
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v4/widget/a;->eCi:I

    invoke-virtual {p0, p1, v1}, Landroid/support/v4/widget/a;->dTy(IZ)V

    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/a;->dTJ(II)Z

    const/4 v0, 0x1

    return v0
.end method

.method public final dTJ(II)Z
    .locals 3

    const/4 v1, 0x0

    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/a;->dTx(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-static {v0, v2, v1}, Landroid/support/v4/view/au;->dRU(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public final dTO()V
    .locals 2

    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/a;->dTF(II)V

    return-void
.end method

.method public final dTv(I)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget v0, p0, Landroid/support/v4/widget/a;->eCi:I

    if-ne v0, p1, :cond_1

    return v1

    :cond_1
    iget v0, p0, Landroid/support/v4/widget/a;->eCi:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    iget v0, p0, Landroid/support/v4/widget/a;->eCi:I

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/a;->dTH(I)Z

    :cond_2
    iput p1, p0, Landroid/support/v4/widget/a;->eCi:I

    invoke-virtual {p0, p1, v2}, Landroid/support/v4/widget/a;->dTy(IZ)V

    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/a;->dTJ(II)Z

    return v2
.end method

.method protected dTy(IZ)V
    .locals 0

    return-void
.end method

.method dTz(I)Landroid/support/v4/view/a/a;
    .locals 1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Landroid/support/v4/widget/a;->dTE()Landroid/support/v4/view/a/a;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v4/widget/a;->dTI(I)Landroid/support/v4/view/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x1

    const/high16 v4, -0x80000000

    const/4 v1, 0x0

    iget-object v2, p0, Landroid/support/v4/widget/a;->eCr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/v4/widget/a;->eCr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    :cond_0
    return v1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    return v1

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/widget/a;->azT(FF)I

    move-result v2

    invoke-direct {p0, v2}, Landroid/support/v4/widget/a;->dTL(I)V

    if-eq v2, v4, :cond_2

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_2
    iget v2, p0, Landroid/support/v4/widget/a;->eCm:I

    if-eq v2, v4, :cond_3

    invoke-direct {p0, v4}, Landroid/support/v4/widget/a;->dTL(I)V

    return v0

    :cond_3
    return v1

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getAccessibilityNodeProvider(Landroid/view/View;)Landroid/support/v4/view/a/b;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/a;->eCn:Landroid/support/v4/widget/b;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/widget/b;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/b;-><init>(Landroid/support/v4/widget/a;)V

    iput-object v0, p0, Landroid/support/v4/widget/a;->eCn:Landroid/support/v4/widget/b;

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/a;->eCn:Landroid/support/v4/widget/b;

    return-object v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/support/v4/view/d;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {p0, p2}, Landroid/support/v4/widget/a;->azX(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method performAction(IILandroid/os/Bundle;)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/a;->dTG(IILandroid/os/Bundle;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-direct {p0, p2, p3}, Landroid/support/v4/widget/a;->dTN(ILandroid/os/Bundle;)Z

    move-result v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
