.class public abstract Landroid/support/v4/widget/ag;
.super Ljava/lang/Object;
.source "AutoScrollHelper.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final eFX:I


# instance fields
.field private eFJ:Z

.field final eFK:Landroid/support/v4/widget/am;

.field eFL:Z

.field private eFM:[F

.field private final eFN:Landroid/view/animation/Interpolator;

.field eFO:Z

.field private eFP:I

.field private eFQ:[F

.field private eFR:[F

.field private eFS:Ljava/lang/Runnable;

.field eFT:Z

.field private eFU:Z

.field private eFV:I

.field final eFW:Landroid/view/View;

.field private eFY:[F

.field private eFZ:[F

.field private mEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Landroid/support/v4/widget/ag;->eFX:I

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 7

    const v6, 0x7f7fffff    # Float.MAX_VALUE

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f000000    # 0.5f

    const v3, 0x3e4ccccd    # 0.2f

    const/4 v1, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v4/widget/am;

    invoke-direct {v0}, Landroid/support/v4/widget/am;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/ag;->eFK:Landroid/support/v4/widget/am;

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/ag;->eFN:Landroid/view/animation/Interpolator;

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Landroid/support/v4/widget/ag;->eFZ:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Landroid/support/v4/widget/ag;->eFQ:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, Landroid/support/v4/widget/ag;->eFR:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_3

    iput-object v0, p0, Landroid/support/v4/widget/ag;->eFM:[F

    new-array v0, v1, [F

    fill-array-data v0, :array_4

    iput-object v0, p0, Landroid/support/v4/widget/ag;->eFY:[F

    iput-object p1, p0, Landroid/support/v4/widget/ag;->eFW:Landroid/view/View;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    const v2, 0x44c4e000    # 1575.0f

    mul-float/2addr v1, v2

    add-float/2addr v1, v4

    float-to-int v1, v1

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const v2, 0x439d8000    # 315.0f

    mul-float/2addr v0, v2

    add-float/2addr v0, v4

    float-to-int v0, v0

    int-to-float v2, v1

    int-to-float v1, v1

    invoke-virtual {p0, v2, v1}, Landroid/support/v4/widget/ag;->dXz(FF)Landroid/support/v4/widget/ag;

    int-to-float v1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/widget/ag;->dXE(FF)Landroid/support/v4/widget/ag;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/ag;->dXM(I)Landroid/support/v4/widget/ag;

    invoke-virtual {p0, v6, v6}, Landroid/support/v4/widget/ag;->dXO(FF)Landroid/support/v4/widget/ag;

    invoke-virtual {p0, v3, v3}, Landroid/support/v4/widget/ag;->dXG(FF)Landroid/support/v4/widget/ag;

    invoke-virtual {p0, v5, v5}, Landroid/support/v4/widget/ag;->dXH(FF)Landroid/support/v4/widget/ag;

    sget v0, Landroid/support/v4/widget/ag;->eFX:I

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/ag;->dXy(I)Landroid/support/v4/widget/ag;

    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/ag;->dXK(I)Landroid/support/v4/widget/ag;

    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/ag;->dXP(I)Landroid/support/v4/widget/ag;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x7f7fffff    # Float.MAX_VALUE
        0x7f7fffff    # Float.MAX_VALUE
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_4
    .array-data 4
        0x7f7fffff    # Float.MAX_VALUE
        0x7f7fffff    # Float.MAX_VALUE
    .end array-data
.end method

.method private dXA()V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFS:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/widget/h;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/h;-><init>(Landroid/support/v4/widget/ag;)V

    iput-object v0, p0, Landroid/support/v4/widget/ag;->eFS:Ljava/lang/Runnable;

    :cond_0
    iput-boolean v4, p0, Landroid/support/v4/widget/ag;->eFT:Z

    iput-boolean v4, p0, Landroid/support/v4/widget/ag;->eFO:Z

    iget-boolean v0, p0, Landroid/support/v4/widget/ag;->eFJ:Z

    if-nez v0, :cond_1

    iget v0, p0, Landroid/support/v4/widget/ag;->eFP:I

    if-lez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFW:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v4/widget/ag;->eFS:Ljava/lang/Runnable;

    iget v2, p0, Landroid/support/v4/widget/ag;->eFP:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/view/z;->dPA(Landroid/view/View;Ljava/lang/Runnable;J)V

    :goto_0
    iput-boolean v4, p0, Landroid/support/v4/widget/ag;->eFJ:Z

    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFS:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private dXC(IFFF)F
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFZ:[F

    aget v0, v0, p1

    iget-object v1, p0, Landroid/support/v4/widget/ag;->eFQ:[F

    aget v1, v1, p1

    invoke-direct {p0, v0, p3, v1, p2}, Landroid/support/v4/widget/ag;->dXL(FFFF)F

    move-result v0

    cmpl-float v1, v0, v4

    if-nez v1, :cond_0

    return v4

    :cond_0
    iget-object v1, p0, Landroid/support/v4/widget/ag;->eFR:[F

    aget v1, v1, p1

    iget-object v2, p0, Landroid/support/v4/widget/ag;->eFM:[F

    aget v2, v2, p1

    iget-object v3, p0, Landroid/support/v4/widget/ag;->eFY:[F

    aget v3, v3, p1

    mul-float/2addr v1, p4

    cmpl-float v4, v0, v4

    if-lez v4, :cond_1

    mul-float/2addr v0, v1

    invoke-static {v0, v2, v3}, Landroid/support/v4/widget/ag;->dXN(FFF)F

    move-result v0

    return v0

    :cond_1
    neg-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0, v2, v3}, Landroid/support/v4/widget/ag;->dXN(FFF)F

    move-result v0

    neg-float v0, v0

    return v0
.end method

.method static dXD(III)I
    .locals 0

    if-le p0, p2, :cond_0

    return p2

    :cond_0
    if-ge p0, p1, :cond_1

    return p1

    :cond_1
    return p0
.end method

.method private dXF(FF)F
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    cmpl-float v0, p2, v2

    if-nez v0, :cond_0

    return v2

    :cond_0
    iget v0, p0, Landroid/support/v4/widget/ag;->eFV:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    return v2

    :pswitch_0
    cmpg-float v0, p1, p2

    if-gez v0, :cond_1

    cmpl-float v0, p1, v2

    if-ltz v0, :cond_2

    div-float v0, p1, p2

    sub-float v0, v3, v0

    return v0

    :cond_2
    iget-boolean v0, p0, Landroid/support/v4/widget/ag;->eFT:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v4/widget/ag;->eFV:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    return v3

    :pswitch_1
    cmpg-float v0, p1, v2

    if-gez v0, :cond_1

    neg-float v0, p2

    div-float v0, p1, v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private dXJ()V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/widget/ag;->eFO:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/ag;->eFT:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFK:Landroid/support/v4/widget/am;

    invoke-virtual {v0}, Landroid/support/v4/widget/am;->dYu()V

    goto :goto_0
.end method

.method private dXL(FFFF)F
    .locals 4

    const/4 v3, 0x0

    mul-float v0, p1, p2

    invoke-static {v0, v3, p3}, Landroid/support/v4/widget/ag;->dXN(FFF)F

    move-result v0

    invoke-direct {p0, p4, v0}, Landroid/support/v4/widget/ag;->dXF(FF)F

    move-result v1

    sub-float v2, p2, p4

    invoke-direct {p0, v2, v0}, Landroid/support/v4/widget/ag;->dXF(FF)F

    move-result v0

    sub-float/2addr v0, v1

    cmpg-float v1, v0, v3

    if-gez v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/widget/ag;->eFN:Landroid/view/animation/Interpolator;

    neg-float v0, v0

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    neg-float v0, v0

    :goto_0
    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, Landroid/support/v4/widget/ag;->dXN(FFF)F

    move-result v0

    return v0

    :cond_0
    cmpl-float v1, v0, v3

    if-lez v1, :cond_1

    iget-object v1, p0, Landroid/support/v4/widget/ag;->eFN:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_0

    :cond_1
    return v3
.end method

.method static dXN(FFF)F
    .locals 1

    cmpl-float v0, p0, p2

    if-lez v0, :cond_0

    return p2

    :cond_0
    cmpg-float v0, p0, p1

    if-gez v0, :cond_1

    return p1

    :cond_1
    return p0
.end method


# virtual methods
.method dXB()V
    .locals 8

    const/4 v5, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const/4 v4, 0x3

    const/4 v7, 0x0

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/widget/ag;->eFW:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    return-void
.end method

.method public dXE(FF)Landroid/support/v4/widget/ag;
    .locals 4

    const/high16 v3, 0x447a0000    # 1000.0f

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFM:[F

    div-float v1, p1, v3

    const/4 v2, 0x0

    aput v1, v0, v2

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFM:[F

    div-float v1, p2, v3

    const/4 v2, 0x1

    aput v1, v0, v2

    return-object p0
.end method

.method public dXG(FF)Landroid/support/v4/widget/ag;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFZ:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFZ:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    return-object p0
.end method

.method public dXH(FF)Landroid/support/v4/widget/ag;
    .locals 4

    const/high16 v3, 0x447a0000    # 1000.0f

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFR:[F

    div-float v1, p1, v3

    const/4 v2, 0x0

    aput v1, v0, v2

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFR:[F

    div-float v1, p2, v3

    const/4 v2, 0x1

    aput v1, v0, v2

    return-object p0
.end method

.method dXI()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v4/widget/ag;->eFK:Landroid/support/v4/widget/am;

    invoke-virtual {v1}, Landroid/support/v4/widget/am;->dYs()I

    move-result v2

    invoke-virtual {v1}, Landroid/support/v4/widget/am;->dYk()I

    move-result v1

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Landroid/support/v4/widget/ag;->dXs(I)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/ag;->dXr(I)Z

    move-result v0

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dXK(I)Landroid/support/v4/widget/ag;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFK:Landroid/support/v4/widget/am;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/am;->dYr(I)V

    return-object p0
.end method

.method public dXM(I)Landroid/support/v4/widget/ag;
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/ag;->eFV:I

    return-object p0
.end method

.method public dXO(FF)Landroid/support/v4/widget/ag;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFQ:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFQ:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    return-object p0
.end method

.method public dXP(I)Landroid/support/v4/widget/ag;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFK:Landroid/support/v4/widget/am;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/am;->dYq(I)V

    return-object p0
.end method

.method public abstract dXr(I)Z
.end method

.method public abstract dXs(I)Z
.end method

.method public abstract dXt(II)V
.end method

.method public dXy(I)Landroid/support/v4/widget/ag;
    .locals 0

    iput p1, p0, Landroid/support/v4/widget/ag;->eFP:I

    return-object p0
.end method

.method public dXz(FF)Landroid/support/v4/widget/ag;
    .locals 4

    const/high16 v3, 0x447a0000    # 1000.0f

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFY:[F

    div-float v1, p1, v3

    const/4 v2, 0x0

    aput v1, v0, v2

    iget-object v0, p0, Landroid/support/v4/widget/ag;->eFY:[F

    div-float v1, p2, v3

    const/4 v2, 0x1

    aput v1, v0, v2

    return-object p0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-boolean v1, p0, Landroid/support/v4/widget/ag;->mEnabled:Z

    if-nez v1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_0
    iget-boolean v1, p0, Landroid/support/v4/widget/ag;->eFU:Z

    if-eqz v1, :cond_2

    iget-boolean v0, p0, Landroid/support/v4/widget/ag;->eFT:Z

    :cond_2
    return v0

    :pswitch_0
    iput-boolean v5, p0, Landroid/support/v4/widget/ag;->eFL:Z

    iput-boolean v0, p0, Landroid/support/v4/widget/ag;->eFJ:Z

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Landroid/support/v4/widget/ag;->eFW:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {p0, v0, v1, v2, v3}, Landroid/support/v4/widget/ag;->dXC(IFFF)F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Landroid/support/v4/widget/ag;->eFW:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {p0, v5, v2, v3, v4}, Landroid/support/v4/widget/ag;->dXC(IFFF)F

    move-result v2

    iget-object v3, p0, Landroid/support/v4/widget/ag;->eFK:Landroid/support/v4/widget/am;

    invoke-virtual {v3, v1, v2}, Landroid/support/v4/widget/am;->dYn(FF)V

    iget-boolean v1, p0, Landroid/support/v4/widget/ag;->eFT:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/widget/ag;->dXI()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Landroid/support/v4/widget/ag;->dXA()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Landroid/support/v4/widget/ag;->dXJ()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setEnabled(Z)Landroid/support/v4/widget/ag;
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/widget/ag;->mEnabled:Z

    if-eqz v0, :cond_0

    xor-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v4/widget/ag;->dXJ()V

    :cond_0
    iput-boolean p1, p0, Landroid/support/v4/widget/ag;->mEnabled:Z

    return-object p0
.end method
