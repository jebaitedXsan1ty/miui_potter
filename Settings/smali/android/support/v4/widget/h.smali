.class Landroid/support/v4/widget/h;
.super Ljava/lang/Object;
.source "AutoScrollHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic eCv:Landroid/support/v4/widget/ag;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/ag;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    iget-boolean v0, v0, Landroid/support/v4/widget/ag;->eFT:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    iget-boolean v0, v0, Landroid/support/v4/widget/ag;->eFO:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    iput-boolean v2, v0, Landroid/support/v4/widget/ag;->eFO:Z

    iget-object v0, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    iget-object v0, v0, Landroid/support/v4/widget/ag;->eFK:Landroid/support/v4/widget/am;

    invoke-virtual {v0}, Landroid/support/v4/widget/am;->start()V

    :cond_1
    iget-object v0, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    iget-object v0, v0, Landroid/support/v4/widget/ag;->eFK:Landroid/support/v4/widget/am;

    invoke-virtual {v0}, Landroid/support/v4/widget/am;->dYp()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    invoke-virtual {v1}, Landroid/support/v4/widget/ag;->dXI()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v0, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    iput-boolean v2, v0, Landroid/support/v4/widget/ag;->eFT:Z

    return-void

    :cond_3
    iget-object v1, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    iget-boolean v1, v1, Landroid/support/v4/widget/ag;->eFL:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    iput-boolean v2, v1, Landroid/support/v4/widget/ag;->eFL:Z

    iget-object v1, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    invoke-virtual {v1}, Landroid/support/v4/widget/ag;->dXB()V

    :cond_4
    invoke-virtual {v0}, Landroid/support/v4/widget/am;->dYl()V

    invoke-virtual {v0}, Landroid/support/v4/widget/am;->dYj()I

    move-result v1

    invoke-virtual {v0}, Landroid/support/v4/widget/am;->dYm()I

    move-result v0

    iget-object v2, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    invoke-virtual {v2, v1, v0}, Landroid/support/v4/widget/ag;->dXt(II)V

    iget-object v0, p0, Landroid/support/v4/widget/h;->eCv:Landroid/support/v4/widget/ag;

    iget-object v0, v0, Landroid/support/v4/widget/ag;->eFW:Landroid/view/View;

    invoke-static {v0, p0}, Landroid/support/v4/view/z;->dPV(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method
