.class public Landroid/support/v4/view/a/c;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoCompat.java"


# static fields
.field public static final ewH:Landroid/support/v4/view/a/c;

.field public static final ewI:Landroid/support/v4/view/a/c;

.field public static final ewJ:Landroid/support/v4/view/a/c;

.field public static final ewK:Landroid/support/v4/view/a/c;

.field public static final ewL:Landroid/support/v4/view/a/c;

.field public static final ewM:Landroid/support/v4/view/a/c;

.field public static final ewN:Landroid/support/v4/view/a/c;

.field public static final ewO:Landroid/support/v4/view/a/c;

.field public static final ewP:Landroid/support/v4/view/a/c;

.field public static final ewQ:Landroid/support/v4/view/a/c;

.field public static final ewR:Landroid/support/v4/view/a/c;

.field public static final ewS:Landroid/support/v4/view/a/c;

.field public static final ewU:Landroid/support/v4/view/a/c;

.field public static final ewV:Landroid/support/v4/view/a/c;

.field public static final ewW:Landroid/support/v4/view/a/c;

.field public static final ewX:Landroid/support/v4/view/a/c;

.field public static final ewY:Landroid/support/v4/view/a/c;

.field public static final ewZ:Landroid/support/v4/view/a/c;

.field public static final exa:Landroid/support/v4/view/a/c;

.field public static final exb:Landroid/support/v4/view/a/c;

.field public static final exc:Landroid/support/v4/view/a/c;

.field public static final exd:Landroid/support/v4/view/a/c;

.field public static final exe:Landroid/support/v4/view/a/c;

.field public static final exf:Landroid/support/v4/view/a/c;

.field public static final exg:Landroid/support/v4/view/a/c;

.field public static final exh:Landroid/support/v4/view/a/c;

.field public static final exi:Landroid/support/v4/view/a/c;

.field public static final exj:Landroid/support/v4/view/a/c;

.field public static final exk:Landroid/support/v4/view/a/c;

.field public static final exl:Landroid/support/v4/view/a/c;


# instance fields
.field final ewT:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x17

    const/4 v1, 0x0

    new-instance v0, Landroid/support/v4/view/a/c;

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewO:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/4 v2, 0x2

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewQ:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/4 v2, 0x4

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewP:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x8

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exd:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x10

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exe:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x20

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewI:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x40

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewN:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x80

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewH:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x100

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exb:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x200

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewR:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x400

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewX:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x800

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewJ:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x1000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exc:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x2000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exg:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/16 v2, 0x4000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exj:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const v2, 0x8000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewY:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/high16 v2, 0x10000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exl:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/high16 v2, 0x20000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewW:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/high16 v2, 0x40000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exf:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/high16 v2, 0x80000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exi:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/high16 v2, 0x100000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->exa:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    const/high16 v2, 0x200000

    invoke-direct {v0, v2, v1}, Landroid/support/v4/view/a/c;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewK:Landroid/support/v4/view/a/c;

    new-instance v2, Landroid/support/v4/view/a/c;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_1

    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SHOW_ON_SCREEN:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    :goto_0
    invoke-direct {v2, v0}, Landroid/support/v4/view/a/c;-><init>(Ljava/lang/Object;)V

    sput-object v2, Landroid/support/v4/view/a/c;->ewL:Landroid/support/v4/view/a/c;

    new-instance v2, Landroid/support/v4/view/a/c;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_2

    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_TO_POSITION:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    :goto_1
    invoke-direct {v2, v0}, Landroid/support/v4/view/a/c;-><init>(Ljava/lang/Object;)V

    sput-object v2, Landroid/support/v4/view/a/c;->ewM:Landroid/support/v4/view/a/c;

    new-instance v2, Landroid/support/v4/view/a/c;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_3

    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_UP:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    :goto_2
    invoke-direct {v2, v0}, Landroid/support/v4/view/a/c;-><init>(Ljava/lang/Object;)V

    sput-object v2, Landroid/support/v4/view/a/c;->ewS:Landroid/support/v4/view/a/c;

    new-instance v2, Landroid/support/v4/view/a/c;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_4

    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_LEFT:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    :goto_3
    invoke-direct {v2, v0}, Landroid/support/v4/view/a/c;-><init>(Ljava/lang/Object;)V

    sput-object v2, Landroid/support/v4/view/a/c;->exk:Landroid/support/v4/view/a/c;

    new-instance v2, Landroid/support/v4/view/a/c;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_5

    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_DOWN:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    :goto_4
    invoke-direct {v2, v0}, Landroid/support/v4/view/a/c;-><init>(Ljava/lang/Object;)V

    sput-object v2, Landroid/support/v4/view/a/c;->exh:Landroid/support/v4/view/a/c;

    new-instance v2, Landroid/support/v4/view/a/c;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_6

    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_RIGHT:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    :goto_5
    invoke-direct {v2, v0}, Landroid/support/v4/view/a/c;-><init>(Ljava/lang/Object;)V

    sput-object v2, Landroid/support/v4/view/a/c;->ewU:Landroid/support/v4/view/a/c;

    new-instance v2, Landroid/support/v4/view/a/c;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_7

    sget-object v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_CONTEXT_CLICK:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    :goto_6
    invoke-direct {v2, v0}, Landroid/support/v4/view/a/c;-><init>(Ljava/lang/Object;)V

    sput-object v2, Landroid/support/v4/view/a/c;->ewV:Landroid/support/v4/view/a/c;

    new-instance v0, Landroid/support/v4/view/a/c;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x18

    if-lt v2, v3, :cond_0

    sget-object v1, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SET_PROGRESS:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    :cond_0
    invoke-direct {v0, v1}, Landroid/support/v4/view/a/c;-><init>(Ljava/lang/Object;)V

    sput-object v0, Landroid/support/v4/view/a/c;->ewZ:Landroid/support/v4/view/a/c;

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_4

    :cond_6
    move-object v0, v1

    goto :goto_5

    :cond_7
    move-object v0, v1

    goto :goto_6
.end method

.method public constructor <init>(ILjava/lang/CharSequence;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    invoke-direct {v0, p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;-><init>(ILjava/lang/CharSequence;)V

    :goto_0
    invoke-direct {p0, v0}, Landroid/support/v4/view/a/c;-><init>(Ljava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v4/view/a/c;->ewT:Ljava/lang/Object;

    return-void
.end method
