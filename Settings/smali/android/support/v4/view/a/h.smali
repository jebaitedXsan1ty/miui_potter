.class public Landroid/support/v4/view/a/h;
.super Ljava/lang/Object;
.source "AccessibilityRecordCompat.java"


# instance fields
.field private final exp:Landroid/view/accessibility/AccessibilityRecord;


# direct methods
.method public static dNM(Landroid/view/accessibility/AccessibilityRecord;I)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setMaxScrollY(I)V

    :cond_0
    return-void
.end method

.method public static dNN(Landroid/view/accessibility/AccessibilityRecord;Landroid/view/View;I)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, p1, p2}, Landroid/view/accessibility/AccessibilityRecord;->setSource(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public static dNO(Landroid/view/accessibility/AccessibilityRecord;I)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/accessibility/AccessibilityRecord;->setMaxScrollX(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_0

    return v3

    :cond_0
    if-nez p1, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/view/a/h;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    return v2

    :cond_2
    check-cast p1, Landroid/support/v4/view/a/h;

    iget-object v0, p0, Landroid/support/v4/view/a/h;->exp:Landroid/view/accessibility/AccessibilityRecord;

    if-nez v0, :cond_3

    iget-object v0, p1, Landroid/support/v4/view/a/h;->exp:Landroid/view/accessibility/AccessibilityRecord;

    if-eqz v0, :cond_4

    return v2

    :cond_3
    iget-object v0, p0, Landroid/support/v4/view/a/h;->exp:Landroid/view/accessibility/AccessibilityRecord;

    iget-object v1, p1, Landroid/support/v4/view/a/h;->exp:Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityRecord;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    return v2

    :cond_4
    return v3
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Landroid/support/v4/view/a/h;->exp:Landroid/view/accessibility/AccessibilityRecord;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/a/h;->exp:Landroid/view/accessibility/AccessibilityRecord;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityRecord;->hashCode()I

    move-result v0

    goto :goto_0
.end method
