.class public Landroid/support/v4/view/a;
.super Ljava/lang/Object;
.source "NestedScrollingChildHelper.java"


# instance fields
.field private exv:Landroid/view/ViewParent;

.field private exw:Z

.field private exx:Landroid/view/ViewParent;

.field private exy:[I

.field private final exz:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    return-void
.end method

.method private dNT(I)Landroid/view/ViewParent;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    iget-object v0, p0, Landroid/support/v4/view/a;->exv:Landroid/view/ViewParent;

    return-object v0

    :pswitch_1
    iget-object v0, p0, Landroid/support/v4/view/a;->exx:Landroid/view/ViewParent;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private dNW(ILandroid/view/ViewParent;)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-object p2, p0, Landroid/support/v4/view/a;->exv:Landroid/view/ViewParent;

    goto :goto_0

    :pswitch_1
    iput-object p2, p0, Landroid/support/v4/view/a;->exx:Landroid/view/ViewParent;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public dNR(II[I[II)Z
    .locals 10

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/support/v4/view/a;->isNestedScrollingEnabled()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, p5}, Landroid/support/v4/view/a;->dNT(I)Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    return v7

    :cond_0
    if-nez p1, :cond_1

    if-eqz p2, :cond_6

    :cond_1
    if-eqz p4, :cond_9

    iget-object v1, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-virtual {v1, p4}, Landroid/view/View;->getLocationInWindow([I)V

    aget v2, p4, v7

    aget v1, p4, v6

    move v8, v1

    move v9, v2

    :goto_0
    if-nez p3, :cond_8

    iget-object v1, p0, Landroid/support/v4/view/a;->exy:[I

    if-nez v1, :cond_2

    const/4 v1, 0x2

    new-array v1, v1, [I

    iput-object v1, p0, Landroid/support/v4/view/a;->exy:[I

    :cond_2
    iget-object v4, p0, Landroid/support/v4/view/a;->exy:[I

    :goto_1
    aput v7, v4, v7

    aput v7, v4, v6

    iget-object v1, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    move v2, p1

    move v3, p2

    move v5, p5

    invoke-static/range {v0 .. v5}, Landroid/support/v4/view/au;->dRQ(Landroid/view/ViewParent;Landroid/view/View;II[II)V

    if-eqz p4, :cond_3

    iget-object v0, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-virtual {v0, p4}, Landroid/view/View;->getLocationInWindow([I)V

    aget v0, p4, v7

    sub-int/2addr v0, v9

    aput v0, p4, v7

    aget v0, p4, v6

    sub-int/2addr v0, v8

    aput v0, p4, v6

    :cond_3
    aget v0, v4, v7

    if-nez v0, :cond_4

    aget v0, v4, v6

    if-eqz v0, :cond_5

    :cond_4
    move v0, v6

    :goto_2
    return v0

    :cond_5
    move v0, v7

    goto :goto_2

    :cond_6
    if-eqz p4, :cond_7

    aput v7, p4, v7

    aput v7, p4, v6

    :cond_7
    return v7

    :cond_8
    move-object v4, p3

    goto :goto_1

    :cond_9
    move v8, v7

    move v9, v7

    goto :goto_0
.end method

.method public dNS(IIII[II)Z
    .locals 9

    invoke-virtual {p0}, Landroid/support/v4/view/a;->isNestedScrollingEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p6}, Landroid/support/v4/view/a;->dNT(I)Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    if-nez p1, :cond_1

    if-eqz p2, :cond_3

    :cond_1
    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p5, :cond_5

    iget-object v1, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-virtual {v1, p5}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v1, 0x0

    aget v2, p5, v1

    const/4 v1, 0x1

    aget v1, p5, v1

    move v7, v1

    move v8, v2

    :goto_0
    iget-object v1, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p6

    invoke-static/range {v0 .. v6}, Landroid/support/v4/view/au;->dRR(Landroid/view/ViewParent;Landroid/view/View;IIIII)V

    if-eqz p5, :cond_2

    iget-object v0, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-virtual {v0, p5}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v0, 0x0

    aget v1, p5, v0

    sub-int/2addr v1, v8

    aput v1, p5, v0

    const/4 v0, 0x1

    aget v1, p5, v0

    sub-int/2addr v1, v7

    aput v1, p5, v0

    :cond_2
    const/4 v0, 0x1

    return v0

    :cond_3
    if-nez p3, :cond_1

    if-nez p4, :cond_1

    if-eqz p5, :cond_4

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v0, p5, v1

    const/4 v0, 0x0

    const/4 v1, 0x1

    aput v0, p5, v1

    :cond_4
    const/4 v0, 0x0

    return v0

    :cond_5
    move v7, v1

    move v8, v2

    goto :goto_0
.end method

.method public dNU(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/support/v4/view/a;->dNT(I)Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-static {v0, v1, p1}, Landroid/support/v4/view/au;->dRS(Landroid/view/ViewParent;Landroid/view/View;I)V

    invoke-direct {p0, p1, v2}, Landroid/support/v4/view/a;->dNW(ILandroid/view/ViewParent;)V

    :cond_0
    return-void
.end method

.method public dNV(I)Z
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v4/view/a;->dNT(I)Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dNX(II)Z
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0, p2}, Landroid/support/v4/view/a;->dNV(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return v3

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/view/a;->isNestedScrollingEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v0, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    :goto_0
    if-eqz v1, :cond_3

    iget-object v2, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-static {v1, v0, v2, p1, p2}, Landroid/support/v4/view/au;->dRV(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;II)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p2, v1}, Landroid/support/v4/view/a;->dNW(ILandroid/view/ViewParent;)V

    iget-object v2, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-static {v1, v0, v2, p1, p2}, Landroid/support/v4/view/au;->dRP(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;II)V

    return v3

    :cond_1
    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_2

    move-object v0, v1

    check-cast v0, Landroid/view/View;

    :cond_2
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method public dispatchNestedFling(FFZ)Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v4/view/a;->isNestedScrollingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Landroid/support/v4/view/a;->dNT(I)Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-static {v0, v1, p1, p2, p3}, Landroid/support/v4/view/au;->dRT(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z

    move-result v0

    return v0

    :cond_0
    return v1
.end method

.method public dispatchNestedPreFling(FF)Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v4/view/a;->isNestedScrollingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Landroid/support/v4/view/a;->dNT(I)Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-static {v0, v1, p1, p2}, Landroid/support/v4/view/au;->dRO(Landroid/view/ViewParent;Landroid/view/View;FF)Z

    move-result v0

    return v0

    :cond_0
    return v1
.end method

.method public dispatchNestedPreScroll(II[I[I)Z
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/a;->dNR(II[I[II)Z

    move-result v0

    return v0
.end method

.method public dispatchNestedScroll(IIII[I)Z
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Landroid/support/v4/view/a;->dNS(IIII[II)Z

    move-result v0

    return v0
.end method

.method public hasNestedScrollingParent()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/a;->dNV(I)Z

    move-result v0

    return v0
.end method

.method public isNestedScrollingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/view/a;->exw:Z

    return v0
.end method

.method public setNestedScrollingEnabled(Z)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/view/a;->exw:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/a;->exz:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/z;->dPr(Landroid/view/View;)V

    :cond_0
    iput-boolean p1, p0, Landroid/support/v4/view/a;->exw:Z

    return-void
.end method

.method public startNestedScroll(I)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/view/a;->dNX(II)Z

    move-result v0

    return v0
.end method

.method public stopNestedScroll()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/a;->dNU(I)V

    return-void
.end method
