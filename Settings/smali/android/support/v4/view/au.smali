.class public final Landroid/support/v4/view/au;
.super Ljava/lang/Object;
.source "ViewParentCompat.java"


# static fields
.field static final eAV:Landroid/support/v4/view/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/view/U;

    invoke-direct {v0}, Landroid/support/v4/view/U;-><init>()V

    sput-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    new-instance v0, Landroid/support/v4/view/V;

    invoke-direct {v0}, Landroid/support/v4/view/V;-><init>()V

    sput-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/support/v4/view/ac;

    invoke-direct {v0}, Landroid/support/v4/view/ac;-><init>()V

    sput-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dRO(Landroid/view/ViewParent;Landroid/view/View;FF)Z
    .locals 1

    sget-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/v4/view/ac;->dRe(Landroid/view/ViewParent;Landroid/view/View;FF)Z

    move-result v0

    return v0
.end method

.method public static dRP(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;II)V
    .locals 1

    instance-of v0, p0, Landroid/support/v4/view/X;

    if-eqz v0, :cond_1

    check-cast p0, Landroid/support/v4/view/X;

    invoke-interface {p0, p1, p2, p3, p4}, Landroid/support/v4/view/X;->dRo(Landroid/view/View;Landroid/view/View;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p4, :cond_0

    sget-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/v4/view/ac;->dRc(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0
.end method

.method public static dRQ(Landroid/view/ViewParent;Landroid/view/View;II[II)V
    .locals 6

    instance-of v0, p0, Landroid/support/v4/view/X;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Landroid/support/v4/view/X;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/view/X;->dRn(Landroid/view/View;II[II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p5, :cond_0

    sget-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/ac;->dRg(Landroid/view/ViewParent;Landroid/view/View;II[I)V

    goto :goto_0
.end method

.method public static dRR(Landroid/view/ViewParent;Landroid/view/View;IIIII)V
    .locals 7

    instance-of v0, p0, Landroid/support/v4/view/X;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Landroid/support/v4/view/X;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/view/X;->dRm(Landroid/view/View;IIIII)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p6, :cond_0

    sget-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Landroid/support/v4/view/ac;->dRf(Landroid/view/ViewParent;Landroid/view/View;IIII)V

    goto :goto_0
.end method

.method public static dRS(Landroid/view/ViewParent;Landroid/view/View;I)V
    .locals 1

    instance-of v0, p0, Landroid/support/v4/view/X;

    if-eqz v0, :cond_1

    check-cast p0, Landroid/support/v4/view/X;

    invoke-interface {p0, p1, p2}, Landroid/support/v4/view/X;->dRk(Landroid/view/View;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    sget-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/view/ac;->dRi(Landroid/view/ViewParent;Landroid/view/View;)V

    goto :goto_0
.end method

.method public static dRT(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
    .locals 6

    sget-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/ac;->dRd(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z

    move-result v0

    return v0
.end method

.method public static dRU(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    invoke-interface {p0, p1, p2}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public static dRV(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;II)Z
    .locals 2

    const/4 v1, 0x0

    instance-of v0, p0, Landroid/support/v4/view/X;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/support/v4/view/X;

    invoke-interface {p0, p1, p2, p3, p4}, Landroid/support/v4/view/X;->dRl(Landroid/view/View;Landroid/view/View;II)Z

    move-result v0

    return v0

    :cond_0
    if-nez p4, :cond_1

    sget-object v0, Landroid/support/v4/view/au;->eAV:Landroid/support/v4/view/ac;

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/v4/view/ac;->dRh(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z

    move-result v0

    return v0

    :cond_1
    return v1
.end method
