.class public final Landroid/support/v4/view/am;
.super Ljava/lang/Object;
.source "ViewGroupCompat.java"


# static fields
.field static final eAs:Landroid/support/v4/view/an;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v4/view/ar;

    invoke-direct {v0}, Landroid/support/v4/view/ar;-><init>()V

    sput-object v0, Landroid/support/v4/view/am;->eAs:Landroid/support/v4/view/an;

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    new-instance v0, Landroid/support/v4/view/Y;

    invoke-direct {v0}, Landroid/support/v4/view/Y;-><init>()V

    sput-object v0, Landroid/support/v4/view/am;->eAs:Landroid/support/v4/view/an;

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/support/v4/view/an;

    invoke-direct {v0}, Landroid/support/v4/view/an;-><init>()V

    sput-object v0, Landroid/support/v4/view/am;->eAs:Landroid/support/v4/view/an;

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dRF(Landroid/view/ViewGroup;)Z
    .locals 1

    sget-object v0, Landroid/support/v4/view/am;->eAs:Landroid/support/v4/view/an;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/an;->dRH(Landroid/view/ViewGroup;)Z

    move-result v0

    return v0
.end method

.method public static dRG(Landroid/view/ViewGroup;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->setMotionEventSplittingEnabled(Z)V

    return-void
.end method
