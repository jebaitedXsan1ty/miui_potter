.class final Landroid/support/v4/view/t;
.super Ljava/lang/Object;
.source "ViewPager.java"

# interfaces
.implements Landroid/support/v4/view/w;


# instance fields
.field final synthetic ezh:Landroid/support/v4/view/ViewPager;

.field private final ezi:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Landroid/support/v4/view/ViewPager;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v4/view/t;->ezh:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/t;->ezi:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public dOW(Landroid/view/View;Landroid/support/v4/view/x;)Landroid/support/v4/view/x;
    .locals 7

    invoke-static {p1, p2}, Landroid/support/v4/view/z;->dPE(Landroid/view/View;Landroid/support/v4/view/x;)Landroid/support/v4/view/x;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/x;->dPf()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    iget-object v2, p0, Landroid/support/v4/view/t;->ezi:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/support/v4/view/x;->dPm()I

    move-result v0

    iput v0, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/support/v4/view/x;->dPj()I

    move-result v0

    iput v0, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/support/v4/view/x;->dPh()I

    move-result v0

    iput v0, v2, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/support/v4/view/x;->dPg()I

    move-result v0

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    const/4 v0, 0x0

    iget-object v3, p0, Landroid/support/v4/view/t;->ezh:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v3

    :goto_0
    if-ge v0, v3, :cond_1

    iget-object v4, p0, Landroid/support/v4/view/t;->ezh:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4, v1}, Landroid/support/v4/view/z;->dPX(Landroid/view/View;Landroid/support/v4/view/x;)Landroid/support/v4/view/x;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/x;->dPm()I

    move-result v5

    iget v6, v2, Landroid/graphics/Rect;->left:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4}, Landroid/support/v4/view/x;->dPj()I

    move-result v5

    iget v6, v2, Landroid/graphics/Rect;->top:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/support/v4/view/x;->dPh()I

    move-result v5

    iget v6, v2, Landroid/graphics/Rect;->right:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v2, Landroid/graphics/Rect;->right:I

    invoke-virtual {v4}, Landroid/support/v4/view/x;->dPg()I

    move-result v4

    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, v2, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, v2, Landroid/graphics/Rect;->left:I

    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget v4, v2, Landroid/graphics/Rect;->right:I

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v0, v3, v4, v2}, Landroid/support/v4/view/x;->dPl(IIII)Landroid/support/v4/view/x;

    move-result-object v0

    return-object v0
.end method
