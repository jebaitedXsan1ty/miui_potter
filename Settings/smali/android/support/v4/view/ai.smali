.class public abstract Landroid/support/v4/view/ai;
.super Ljava/lang/Object;
.source "ActionProvider.java"


# instance fields
.field private eAq:Landroid/support/v4/view/al;

.field private eAr:Landroid/support/v4/view/aq;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v4/view/ai;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public dJT(Landroid/support/v4/view/al;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/view/ai;->eAq:Landroid/support/v4/view/al;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string/jumbo v0, "ActionProvider(support)"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/view/ai;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " instance while it is still in use somewhere else?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Landroid/support/v4/view/ai;->eAq:Landroid/support/v4/view/al;

    return-void
.end method

.method public dJU(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v4/view/ai;->dLC()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public dJV()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dLB()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract dLC()Landroid/view/View;
.end method

.method public dLD(Landroid/view/SubMenu;)V
    .locals 0

    return-void
.end method

.method public dRB(Landroid/support/v4/view/aq;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v4/view/ai;->eAr:Landroid/support/v4/view/aq;

    return-void
.end method

.method public dRC(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v4/view/ai;->eAr:Landroid/support/v4/view/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/view/ai;->eAr:Landroid/support/v4/view/aq;

    invoke-interface {v0, p1}, Landroid/support/v4/view/aq;->dGj(Z)V

    :cond_0
    return-void
.end method

.method public dRD()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/ai;->eAq:Landroid/support/v4/view/al;

    iput-object v0, p0, Landroid/support/v4/view/ai;->eAr:Landroid/support/v4/view/aq;

    return-void
.end method

.method public hasSubMenu()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
