.class public Landroid/support/v4/view/Z;
.super Ljava/lang/Object;
.source "NestedScrollingParentHelper.java"


# instance fields
.field private final eAl:Landroid/view/ViewGroup;

.field private eAm:I


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v4/view/Z;->eAl:Landroid/view/ViewGroup;

    return-void
.end method


# virtual methods
.method public dRp(Landroid/view/View;I)V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/view/Z;->eAm:I

    return-void
.end method

.method public dRq(Landroid/view/View;Landroid/view/View;II)V
    .locals 0

    iput p3, p0, Landroid/support/v4/view/Z;->eAm:I

    return-void
.end method

.method public getNestedScrollAxes()I
    .locals 1

    iget v0, p0, Landroid/support/v4/view/Z;->eAm:I

    return v0
.end method

.method public onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/support/v4/view/Z;->dRq(Landroid/view/View;Landroid/view/View;II)V

    return-void
.end method

.method public onStopNestedScroll(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/view/Z;->dRp(Landroid/view/View;I)V

    return-void
.end method
