.class public Landroid/support/f/a/q;
.super Landroid/support/f/a/s;
.source "VectorDrawableCompat.java"


# static fields
.field static final eWb:Landroid/graphics/PorterDuff$Mode;


# instance fields
.field private eVY:Landroid/graphics/drawable/Drawable$ConstantState;

.field private eVZ:Z

.field private eWa:Landroid/graphics/PorterDuffColorFilter;

.field private final eWc:[F

.field private eWd:Landroid/graphics/ColorFilter;

.field private final eWe:Landroid/graphics/Rect;

.field private eWf:Z

.field private final eWg:Landroid/graphics/Matrix;

.field private eWh:Landroid/support/f/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sput-object v0, Landroid/support/f/a/q;->eWb:Landroid/graphics/PorterDuff$Mode;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/f/a/s;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/f/a/q;->eVZ:Z

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Landroid/support/f/a/q;->eWc:[F

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/q;->eWg:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    new-instance v0, Landroid/support/f/a/b;

    invoke-direct {v0}, Landroid/support/f/a/b;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    return-void
.end method

.method constructor <init>(Landroid/support/f/a/b;)V
    .locals 3

    invoke-direct {p0}, Landroid/support/f/a/s;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/f/a/q;->eVZ:Z

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Landroid/support/f/a/q;->eWc:[F

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/q;->eWg:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    iput-object p1, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v0, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    iget-object v1, p1, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    iget-object v2, p1, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v0, v1, v2}, Landroid/support/f/a/q;->updateTintFilter(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    return-void
.end method

.method static epI(IF)I
    .locals 2

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    const v1, 0xffffff

    and-int/2addr v1, p0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public static epJ(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/support/f/a/q;
    .locals 1

    new-instance v0, Landroid/support/f/a/q;

    invoke-direct {v0}, Landroid/support/f/a/q;-><init>()V

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/f/a/q;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    return-object v0
.end method

.method private epL(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v1, v0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    const-string/jumbo v2, "tintMode"

    const/4 v3, 0x6

    const/4 v4, -0x1

    invoke-static {p1, p2, v2, v3, v4}, Landroid/support/v4/content/a/a;->dZH(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;II)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v2, v3}, Landroid/support/f/a/q;->epP(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    iput-object v2, v0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    if-eqz v2, :cond_0

    iput-object v2, v0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    :cond_0
    const-string/jumbo v2, "autoMirrored"

    iget-boolean v3, v0, Landroid/support/f/a/b;->eUO:Z

    const/4 v4, 0x5

    invoke-static {p1, p2, v2, v4, v3}, Landroid/support/v4/content/a/a;->dZv(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IZ)Z

    move-result v2

    iput-boolean v2, v0, Landroid/support/f/a/b;->eUO:Z

    const-string/jumbo v0, "viewportWidth"

    iget v2, v1, Landroid/support/f/a/n;->eVP:F

    const/4 v3, 0x7

    invoke-static {p1, p2, v0, v3, v2}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, v1, Landroid/support/f/a/n;->eVP:F

    const-string/jumbo v0, "viewportHeight"

    iget v2, v1, Landroid/support/f/a/n;->eVO:F

    const/16 v3, 0x8

    invoke-static {p1, p2, v0, v3, v2}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    iput v0, v1, Landroid/support/f/a/n;->eVO:F

    iget v0, v1, Landroid/support/f/a/n;->eVP:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_1

    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<vector> tag requires viewportWidth > 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, v1, Landroid/support/f/a/n;->eVO:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_2

    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<vector> tag requires viewportHeight > 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget v0, v1, Landroid/support/f/a/n;->eVG:F

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, v1, Landroid/support/f/a/n;->eVG:F

    iget v0, v1, Landroid/support/f/a/n;->eVM:F

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, v1, Landroid/support/f/a/n;->eVM:F

    iget v0, v1, Landroid/support/f/a/n;->eVG:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_3

    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<vector> tag requires width > 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget v0, v1, Landroid/support/f/a/n;->eVM:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_4

    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "<vector> tag requires height > 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const-string/jumbo v0, "alpha"

    invoke-virtual {v1}, Landroid/support/f/a/n;->getAlpha()F

    move-result v2

    const/4 v3, 0x4

    invoke-static {p1, p2, v0, v3, v2}, Landroid/support/v4/content/a/a;->dZC(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;IF)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/f/a/n;->epw(F)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    iput-object v0, v1, Landroid/support/f/a/n;->eVQ:Ljava/lang/String;

    iget-object v2, v1, Landroid/support/f/a/n;->eVV:Landroid/support/v4/a/u;

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    return-void
.end method

.method private epM()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_2

    invoke-virtual {p0}, Landroid/support/f/a/q;->isAutoMirrored()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Landroid/support/v4/c/a/g;->elW(Landroid/graphics/drawable/Drawable;)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    return v1
.end method

.method public static epN(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/support/f/a/q;
    .locals 5

    const/4 v4, 0x2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/f/a/q;

    invoke-direct {v0}, Landroid/support/f/a/q;-><init>()V

    invoke-static {p0, p1, p2}, Landroid/support/v4/content/a/g;->dZU(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    new-instance v1, Landroid/support/f/a/i;

    iget-object v2, v0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/f/a/i;-><init>(Landroid/graphics/drawable/Drawable$ConstantState;)V

    iput-object v1, v0, Landroid/support/f/a/q;->eVY:Landroid/graphics/drawable/Drawable$ConstantState;

    return-object v0

    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    invoke-static {v0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v1

    :cond_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    if-eq v2, v4, :cond_2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    :cond_2
    if-eq v2, v4, :cond_3

    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string/jumbo v1, "No start tag found"

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    const-string/jumbo v1, "VectorDrawableCompat"

    const-string/jumbo v2, "parser error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_3
    :try_start_1
    invoke-static {p0, v0, v1, p2}, Landroid/support/f/a/q;->epJ(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/support/f/a/q;
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    return-object v0

    :catch_1
    move-exception v0

    const-string/jumbo v1, "VectorDrawableCompat"

    const-string/jumbo v2, "parser error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private epO(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    .locals 9

    iget-object v2, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v3, v2, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    const/4 v1, 0x1

    new-instance v4, Ljava/util/Stack;

    invoke-direct {v4}, Ljava/util/Stack;-><init>()V

    iget-object v0, v3, Landroid/support/f/a/n;->eVL:Landroid/support/f/a/a;

    invoke-virtual {v4, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    :goto_0
    const/4 v6, 0x1

    if-eq v0, v6, :cond_7

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v6

    if-ge v6, v5, :cond_0

    const/4 v6, 0x3

    if-eq v0, v6, :cond_7

    :cond_0
    const/4 v6, 0x2

    if-ne v0, v6, :cond_6

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/f/a/a;

    const-string/jumbo v7, "path"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    new-instance v1, Landroid/support/f/a/e;

    invoke-direct {v1}, Landroid/support/f/a/e;-><init>()V

    invoke-virtual {v1, p1, p3, p4, p2}, Landroid/support/f/a/e;->epg(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V

    iget-object v0, v0, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Landroid/support/f/a/e;->epm()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, v3, Landroid/support/f/a/n;->eVV:Landroid/support/v4/a/u;

    invoke-virtual {v1}, Landroid/support/f/a/e;->epm()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v1}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const/4 v0, 0x0

    iget v6, v2, Landroid/support/f/a/b;->eUN:I

    iget v1, v1, Landroid/support/f/a/e;->eVC:I

    or-int/2addr v1, v6

    iput v1, v2, Landroid/support/f/a/b;->eUN:I

    :goto_1
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_0

    :cond_2
    const-string/jumbo v7, "clip-path"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    new-instance v6, Landroid/support/f/a/p;

    invoke-direct {v6}, Landroid/support/f/a/p;-><init>()V

    invoke-virtual {v6, p1, p3, p4, p2}, Landroid/support/f/a/p;->epG(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V

    iget-object v0, v0, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Landroid/support/f/a/p;->epm()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v3, Landroid/support/f/a/n;->eVV:Landroid/support/v4/a/u;

    invoke-virtual {v6}, Landroid/support/f/a/p;->epm()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7, v6}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget v0, v2, Landroid/support/f/a/b;->eUN:I

    iget v6, v6, Landroid/support/f/a/p;->eVC:I

    or-int/2addr v0, v6

    iput v0, v2, Landroid/support/f/a/b;->eUN:I

    move v0, v1

    goto :goto_1

    :cond_4
    const-string/jumbo v7, "group"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    new-instance v6, Landroid/support/f/a/a;

    invoke-direct {v6}, Landroid/support/f/a/a;-><init>()V

    invoke-virtual {v6, p1, p3, p4, p2}, Landroid/support/f/a/a;->eox(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V

    iget-object v0, v0, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6}, Landroid/support/f/a/a;->eow()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, v3, Landroid/support/f/a/n;->eVV:Landroid/support/v4/a/u;

    invoke-virtual {v6}, Landroid/support/f/a/a;->eow()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7, v6}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    iget v0, v2, Landroid/support/f/a/b;->eUN:I

    iget v6, v6, Landroid/support/f/a/a;->eUA:I

    or-int/2addr v0, v6

    iput v0, v2, Landroid/support/f/a/b;->eUN:I

    move v0, v1

    goto :goto_1

    :cond_6
    const/4 v6, 0x3

    if-ne v0, v6, :cond_a

    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v6, "group"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move v0, v1

    goto :goto_1

    :cond_7
    if-eqz v1, :cond_9

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-lez v1, :cond_8

    const-string/jumbo v1, " or "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_8
    const-string/jumbo v1, "path"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v1, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "no "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " defined"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    return-void

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method private static epP(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;
    .locals 2

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    return-object p1

    :pswitch_1
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    return-object v0

    :pswitch_2
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    return-object v0

    :pswitch_3
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    return-object v0

    :pswitch_4
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    return-object v0

    :pswitch_5
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    return-object v0

    :pswitch_6
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->ADD:Landroid/graphics/PorterDuff$Mode;

    return-object v0

    :cond_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic applyTheme(Landroid/content/res/Resources$Theme;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/f/a/s;->applyTheme(Landroid/content/res/Resources$Theme;)V

    return-void
.end method

.method public canApplyTheme()Z
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Landroid/support/v4/c/a/g;->elT(Landroid/graphics/drawable/Drawable;)Z

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic clearColorFilter()V
    .locals 0

    invoke-super {p0}, Landroid/support/f/a/s;->clearColorFilter()V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 10

    const/16 v9, 0x800

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Landroid/support/f/a/q;->copyBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/f/a/q;->eWd:Landroid/graphics/ColorFilter;

    if-nez v0, :cond_6

    iget-object v0, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    :goto_0
    iget-object v1, p0, Landroid/support/f/a/q;->eWg:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->getMatrix(Landroid/graphics/Matrix;)V

    iget-object v1, p0, Landroid/support/f/a/q;->eWg:Landroid/graphics/Matrix;

    iget-object v3, p0, Landroid/support/f/a/q;->eWc:[F

    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v1, p0, Landroid/support/f/a/q;->eWc:[F

    aget v1, v1, v7

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget-object v1, p0, Landroid/support/f/a/q;->eWc:[F

    const/4 v4, 0x4

    aget v1, v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v4, p0, Landroid/support/f/a/q;->eWc:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget-object v5, p0, Landroid/support/f/a/q;->eWc:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v4, v4, v8

    if-nez v4, :cond_3

    cmpl-float v4, v5, v8

    if-eqz v4, :cond_4

    :cond_3
    move v1, v2

    move v3, v2

    :cond_4
    iget-object v4, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget-object v4, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-static {v9, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v9, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lez v3, :cond_5

    if-gtz v1, :cond_7

    :cond_5
    return-void

    :cond_6
    iget-object v0, p0, Landroid/support/f/a/q;->eWd:Landroid/graphics/ColorFilter;

    goto :goto_0

    :cond_7
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    iget-object v5, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget-object v6, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-direct {p0}, Landroid/support/f/a/q;->epM()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v5, v8}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v5, -0x40800000    # -1.0f

    invoke-virtual {p1, v5, v2}, Landroid/graphics/Canvas;->scale(FF)V

    :cond_8
    iget-object v2, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    invoke-virtual {v2, v7, v7}, Landroid/graphics/Rect;->offsetTo(II)V

    iget-object v2, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    invoke-virtual {v2, v3, v1}, Landroid/support/f/a/b;->eoF(II)V

    iget-boolean v2, p0, Landroid/support/f/a/q;->eVZ:Z

    if-nez v2, :cond_a

    iget-object v2, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    invoke-virtual {v2, v3, v1}, Landroid/support/f/a/b;->eoD(II)V

    :cond_9
    :goto_1
    iget-object v1, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v2, p0, Landroid/support/f/a/q;->eWe:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, v0, v2}, Landroid/support/f/a/b;->eoA(Landroid/graphics/Canvas;Landroid/graphics/ColorFilter;Landroid/graphics/Rect;)V

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void

    :cond_a
    iget-object v2, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    invoke-virtual {v2}, Landroid/support/f/a/b;->eoE()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    invoke-virtual {v2, v3, v1}, Landroid/support/f/a/b;->eoD(II)V

    iget-object v1, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    invoke-virtual {v1}, Landroid/support/f/a/b;->eoH()V

    goto :goto_1
.end method

.method epK(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/support/f/a/q;->eVZ:Z

    return-void
.end method

.method epQ(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v0, v0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    iget-object v0, v0, Landroid/support/f/a/n;->eVV:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getAlpha()I
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Landroid/support/v4/c/a/g;->elQ(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v0, v0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-virtual {v0}, Landroid/support/f/a/n;->eps()I

    move-result v0

    return v0
.end method

.method public getChangingConfigurations()I
    .locals 2

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    return v0

    :cond_0
    invoke-super {p0}, Landroid/support/f/a/s;->getChangingConfigurations()I

    move-result v0

    iget-object v1, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    invoke-virtual {v1}, Landroid/support/f/a/b;->getChangingConfigurations()I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic getColorFilter()Landroid/graphics/ColorFilter;
    .locals 1

    invoke-super {p0}, Landroid/support/f/a/s;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    return-object v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 2

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/f/a/i;

    iget-object v1, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/f/a/i;-><init>(Landroid/graphics/drawable/Drawable$ConstantState;)V

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    invoke-virtual {p0}, Landroid/support/f/a/q;->getChangingConfigurations()I

    move-result v1

    iput v1, v0, Landroid/support/f/a/b;->eUN:I

    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    return-object v0
.end method

.method public bridge synthetic getCurrent()Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-super {p0}, Landroid/support/f/a/s;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v0, v0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    iget v0, v0, Landroid/support/f/a/n;->eVM:F

    float-to-int v0, v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v0, v0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    iget v0, v0, Landroid/support/f/a/n;->eVG:F

    float-to-int v0, v0

    return v0
.end method

.method public bridge synthetic getMinimumHeight()I
    .locals 1

    invoke-super {p0}, Landroid/support/f/a/s;->getMinimumHeight()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getMinimumWidth()I
    .locals 1

    invoke-super {p0}, Landroid/support/f/a/s;->getMinimumWidth()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x3

    return v0
.end method

.method public bridge synthetic getPadding(Landroid/graphics/Rect;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/support/f/a/s;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getState()[I
    .locals 1

    invoke-super {p0}, Landroid/support/f/a/s;->getState()[I

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getTransparentRegion()Landroid/graphics/Region;
    .locals 1

    invoke-super {p0}, Landroid/support/f/a/s;->getTransparentRegion()Landroid/graphics/Region;

    move-result-object v0

    return-object v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2, p3}, Landroid/graphics/drawable/Drawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    return-void

    :cond_0
    invoke-virtual {p0, p1, p2, p3, v1}, Landroid/support/f/a/q;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    return-void
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    .locals 3

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1, p2, p3, p4}, Landroid/support/v4/c/a/g;->elV(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    new-instance v1, Landroid/support/f/a/n;

    invoke-direct {v1}, Landroid/support/f/a/n;-><init>()V

    iput-object v1, v0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    sget-object v1, Landroid/support/f/a/j;->eVA:[I

    invoke-static {p1, p4, p3, v1}, Landroid/support/v4/content/a/a;->dZt(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Landroid/support/f/a/q;->epL(Landroid/content/res/TypedArray;Lorg/xmlpull/v1/XmlPullParser;)V

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Landroid/support/f/a/q;->getChangingConfigurations()I

    move-result v1

    iput v1, v0, Landroid/support/f/a/b;->eUN:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/f/a/b;->eUE:Z

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/f/a/q;->epO(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    iget-object v1, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    iget-object v2, v0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    iget-object v0, v0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v1, v2, v0}, Landroid/support/f/a/q;->updateTintFilter(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    return-void
.end method

.method public invalidateSelf()V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void

    :cond_0
    invoke-super {p0}, Landroid/support/f/a/s;->invalidateSelf()V

    return-void
.end method

.method public isAutoMirrored()Z
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Landroid/support/v4/c/a/g;->emb(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-boolean v0, v0, Landroid/support/f/a/b;->eUO:Z

    return v0
.end method

.method public isStateful()Z
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    return v0

    :cond_0
    invoke-super {p0}, Landroid/support/f/a/s;->isStateful()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v0, v0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v0, v0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic jumpToCurrentState()V
    .locals 0

    invoke-super {p0}, Landroid/support/f/a/s;->jumpToCurrentState()V

    return-void
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    return-object p0

    :cond_0
    iget-boolean v0, p0, Landroid/support/f/a/q;->eWf:Z

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/support/f/a/s;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p0, :cond_1

    new-instance v0, Landroid/support/f/a/b;

    iget-object v1, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    invoke-direct {v0, v1}, Landroid/support/f/a/b;-><init>(Landroid/support/f/a/b;)V

    iput-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/f/a/q;->eWf:Z

    :cond_1
    return-object p0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method protected onStateChange([I)Z
    .locals 3

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v1, v0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_1

    iget-object v1, v0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    iget-object v2, v0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    iget-object v0, v0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v1, v2, v0}, Landroid/support/f/a/q;->updateTintFilter(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, Landroid/support/f/a/q;->invalidateSelf()V

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public scheduleSelf(Ljava/lang/Runnable;J)V
    .locals 2

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2, p3}, Landroid/graphics/drawable/Drawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/f/a/s;->scheduleSelf(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v0, v0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-virtual {v0}, Landroid/support/f/a/n;->eps()I

    move-result v0

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v0, v0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-virtual {v0, p1}, Landroid/support/f/a/n;->epu(I)V

    invoke-virtual {p0}, Landroid/support/f/a/q;->invalidateSelf()V

    :cond_1
    return-void
.end method

.method public setAutoMirrored(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/c/a/g;->elR(Landroid/graphics/drawable/Drawable;Z)V

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iput-boolean p1, v0, Landroid/support/f/a/b;->eUO:Z

    return-void
.end method

.method public bridge synthetic setChangingConfigurations(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/f/a/s;->setChangingConfigurations(I)V

    return-void
.end method

.method public bridge synthetic setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/support/f/a/s;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    return-void

    :cond_0
    iput-object p1, p0, Landroid/support/f/a/q;->eWd:Landroid/graphics/ColorFilter;

    invoke-virtual {p0}, Landroid/support/f/a/q;->invalidateSelf()V

    return-void
.end method

.method public bridge synthetic setFilterBitmap(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/f/a/s;->setFilterBitmap(Z)V

    return-void
.end method

.method public bridge synthetic setHotspot(FF)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/support/f/a/s;->setHotspot(FF)V

    return-void
.end method

.method public bridge synthetic setHotspotBounds(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/f/a/s;->setHotspotBounds(IIII)V

    return-void
.end method

.method public bridge synthetic setState([I)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/support/f/a/s;->setState([I)Z

    move-result v0

    return v0
.end method

.method public setTint(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/c/a/g;->elN(Landroid/graphics/drawable/Drawable;I)V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/f/a/q;->setTintList(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setTintList(Landroid/content/res/ColorStateList;)V
    .locals 2

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/c/a/g;->elY(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v1, v0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    if-eq v1, p1, :cond_1

    iput-object p1, v0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    iget-object v1, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    iget-object v0, v0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v1, p1, v0}, Landroid/support/f/a/q;->updateTintFilter(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, Landroid/support/f/a/q;->invalidateSelf()V

    :cond_1
    return-void
.end method

.method public setTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Landroid/support/v4/c/a/g;->ema(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/q;->eWh:Landroid/support/f/a/b;

    iget-object v1, v0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    if-eq v1, p1, :cond_1

    iput-object p1, v0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    iget-object v1, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    iget-object v0, v0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v1, v0, p1}, Landroid/support/f/a/q;->updateTintFilter(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/q;->eWa:Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, Landroid/support/f/a/q;->invalidateSelf()V

    :cond_1
    return-void
.end method

.method public setVisible(ZZ)Z
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/f/a/s;->setVisible(ZZ)Z

    move-result v0

    return v0
.end method

.method public unscheduleSelf(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/q;->eWj:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/support/f/a/s;->unscheduleSelf(Ljava/lang/Runnable;)V

    return-void
.end method

.method updateTintFilter(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/f/a/q;->getState()[I

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    invoke-direct {v1, v0, p3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    return-object v1
.end method
