.class Landroid/support/f/a/k;
.super Ljava/lang/Object;
.source "VectorDrawableCompat.java"


# instance fields
.field eVC:I

.field eVD:Ljava/lang/String;

.field protected eVE:[Landroid/support/v4/c/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/f/a/k;->eVE:[Landroid/support/v4/c/h;

    return-void
.end method

.method public constructor <init>(Landroid/support/f/a/k;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/f/a/k;->eVE:[Landroid/support/v4/c/h;

    iget-object v0, p1, Landroid/support/f/a/k;->eVD:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/f/a/k;->eVD:Ljava/lang/String;

    iget v0, p1, Landroid/support/f/a/k;->eVC:I

    iput v0, p0, Landroid/support/f/a/k;->eVC:I

    iget-object v0, p1, Landroid/support/f/a/k;->eVE:[Landroid/support/v4/c/h;

    invoke-static {v0}, Landroid/support/v4/c/j;->emI([Landroid/support/v4/c/h;)[Landroid/support/v4/c/h;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/k;->eVE:[Landroid/support/v4/c/h;

    return-void
.end method


# virtual methods
.method public epl(Landroid/graphics/Path;)V
    .locals 1

    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Landroid/support/f/a/k;->eVE:[Landroid/support/v4/c/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/k;->eVE:[Landroid/support/v4/c/h;

    invoke-static {v0, p1}, Landroid/support/v4/c/h;->emt([Landroid/support/v4/c/h;Landroid/graphics/Path;)V

    :cond_0
    return-void
.end method

.method public epm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/k;->eVD:Ljava/lang/String;

    return-object v0
.end method

.method public epn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
