.class Landroid/support/f/a/p;
.super Landroid/support/f/a/k;
.source "VectorDrawableCompat.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/f/a/k;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/support/f/a/p;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/f/a/k;-><init>(Landroid/support/f/a/k;)V

    return-void
.end method

.method private epH(Landroid/content/res/TypedArray;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Landroid/support/f/a/p;->eVD:Ljava/lang/String;

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/support/v4/c/j;->emD(Ljava/lang/String;)[Landroid/support/v4/c/h;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/p;->eVE:[Landroid/support/v4/c/h;

    :cond_1
    return-void
.end method


# virtual methods
.method public epG(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 1

    const-string/jumbo v0, "pathData"

    invoke-static {p4, v0}, Landroid/support/v4/content/a/a;->dZD(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Landroid/support/f/a/j;->eVz:[I

    invoke-static {p1, p3, p2, v0}, Landroid/support/v4/content/a/a;->dZt(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/f/a/p;->epH(Landroid/content/res/TypedArray;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public epn()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
