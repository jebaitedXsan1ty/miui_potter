.class Landroid/support/f/a/r;
.super Ljava/lang/Object;
.source "AnimatorInflaterCompat.java"

# interfaces
.implements Landroid/animation/TypeEvaluator;


# instance fields
.field private eWi:[Landroid/support/v4/c/h;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/f/a/r;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/f/a/r;-><init>()V

    return-void
.end method


# virtual methods
.method public epR(F[Landroid/support/v4/c/h;[Landroid/support/v4/c/h;)[Landroid/support/v4/c/h;
    .locals 4

    invoke-static {p2, p3}, Landroid/support/v4/c/j;->emL([Landroid/support/v4/c/h;[Landroid/support/v4/c/h;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Can\'t interpolate between two incompatible pathData"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/r;->eWi:[Landroid/support/v4/c/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/f/a/r;->eWi:[Landroid/support/v4/c/h;

    invoke-static {v0, p2}, Landroid/support/v4/c/j;->emL([Landroid/support/v4/c/h;[Landroid/support/v4/c/h;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {p2}, Landroid/support/v4/c/j;->emI([Landroid/support/v4/c/h;)[Landroid/support/v4/c/h;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/r;->eWi:[Landroid/support/v4/c/h;

    :cond_2
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Landroid/support/f/a/r;->eWi:[Landroid/support/v4/c/h;

    aget-object v1, v1, v0

    aget-object v2, p2, v0

    aget-object v3, p3, v0

    invoke-virtual {v1, v2, v3, p1}, Landroid/support/v4/c/h;->ems(Landroid/support/v4/c/h;Landroid/support/v4/c/h;F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Landroid/support/f/a/r;->eWi:[Landroid/support/v4/c/h;

    return-object v0
.end method

.method public bridge synthetic evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p2, [Landroid/support/v4/c/h;

    check-cast p3, [Landroid/support/v4/c/h;

    invoke-virtual {p0, p1, p2, p3}, Landroid/support/f/a/r;->epR(F[Landroid/support/v4/c/h;[Landroid/support/v4/c/h;)[Landroid/support/v4/c/h;

    move-result-object v0

    return-object v0
.end method
