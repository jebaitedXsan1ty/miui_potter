.class Landroid/support/f/a/b;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "VectorDrawableCompat.java"


# instance fields
.field eUE:Z

.field eUF:Z

.field eUG:I

.field eUH:Landroid/content/res/ColorStateList;

.field eUI:Landroid/graphics/PorterDuff$Mode;

.field eUJ:Landroid/graphics/PorterDuff$Mode;

.field eUK:Landroid/support/f/a/n;

.field eUL:Landroid/content/res/ColorStateList;

.field eUM:Landroid/graphics/Bitmap;

.field eUN:I

.field eUO:Z

.field eUP:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    sget-object v0, Landroid/support/f/a/q;->eWb:Landroid/graphics/PorterDuff$Mode;

    iput-object v0, p0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    new-instance v0, Landroid/support/f/a/n;

    invoke-direct {v0}, Landroid/support/f/a/n;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    return-void
.end method

.method public constructor <init>(Landroid/support/f/a/b;)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    sget-object v0, Landroid/support/f/a/q;->eWb:Landroid/graphics/PorterDuff$Mode;

    iput-object v0, p0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    if-eqz p1, :cond_2

    iget v0, p1, Landroid/support/f/a/b;->eUN:I

    iput v0, p0, Landroid/support/f/a/b;->eUN:I

    new-instance v0, Landroid/support/f/a/n;

    iget-object v1, p1, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-direct {v0, v1}, Landroid/support/f/a/n;-><init>(Landroid/support/f/a/n;)V

    iput-object v0, p0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    iget-object v0, p1, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-static {v0}, Landroid/support/f/a/n;->ept(Landroid/support/f/a/n;)Landroid/graphics/Paint;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p1, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-static {v2}, Landroid/support/f/a/n;->ept(Landroid/support/f/a/n;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    invoke-static {v0, v1}, Landroid/support/f/a/n;->epq(Landroid/support/f/a/n;Landroid/graphics/Paint;)Landroid/graphics/Paint;

    :cond_0
    iget-object v0, p1, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-static {v0}, Landroid/support/f/a/n;->epx(Landroid/support/f/a/n;)Landroid/graphics/Paint;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p1, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-static {v2}, Landroid/support/f/a/n;->epx(Landroid/support/f/a/n;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    invoke-static {v0, v1}, Landroid/support/f/a/n;->epB(Landroid/support/f/a/n;Landroid/graphics/Paint;)Landroid/graphics/Paint;

    :cond_1
    iget-object v0, p1, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    iput-object v0, p0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    iget-object v0, p1, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    iput-object v0, p0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    iget-boolean v0, p1, Landroid/support/f/a/b;->eUO:Z

    iput-boolean v0, p0, Landroid/support/f/a/b;->eUO:Z

    :cond_2
    return-void
.end method


# virtual methods
.method public eoA(Landroid/graphics/Canvas;Landroid/graphics/ColorFilter;Landroid/graphics/Rect;)V
    .locals 3

    invoke-virtual {p0, p2}, Landroid/support/f/a/b;->eoB(Landroid/graphics/ColorFilter;)Landroid/graphics/Paint;

    move-result-object v0

    iget-object v1, p0, Landroid/support/f/a/b;->eUM:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, p3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void
.end method

.method public eoB(Landroid/graphics/ColorFilter;)Landroid/graphics/Paint;
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/f/a/b;->eoC()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/b;->eUP:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/b;->eUP:Landroid/graphics/Paint;

    iget-object v0, p0, Landroid/support/f/a/b;->eUP:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    :cond_1
    iget-object v0, p0, Landroid/support/f/a/b;->eUP:Landroid/graphics/Paint;

    iget-object v1, p0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-virtual {v1}, Landroid/support/f/a/n;->eps()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Landroid/support/f/a/b;->eUP:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    iget-object v0, p0, Landroid/support/f/a/b;->eUP:Landroid/graphics/Paint;

    return-object v0
.end method

.method public eoC()Z
    .locals 2

    iget-object v0, p0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-virtual {v0}, Landroid/support/f/a/n;->eps()I

    move-result v0

    const/16 v1, 0xff

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public eoD(II)V
    .locals 3

    iget-object v0, p0, Landroid/support/f/a/b;->eUM:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Landroid/support/f/a/b;->eUM:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, p2, v2}, Landroid/support/f/a/n;->epz(Landroid/graphics/Canvas;IILandroid/graphics/ColorFilter;)V

    return-void
.end method

.method public eoE()Z
    .locals 2

    iget-boolean v0, p0, Landroid/support/f/a/b;->eUE:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/b;->eUH:Landroid/content/res/ColorStateList;

    iget-object v1, p0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Landroid/support/f/a/b;->eUI:Landroid/graphics/PorterDuff$Mode;

    iget-object v1, p0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Landroid/support/f/a/b;->eUF:Z

    iget-boolean v1, p0, Landroid/support/f/a/b;->eUO:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/support/f/a/b;->eUG:I

    iget-object v1, p0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-virtual {v1}, Landroid/support/f/a/n;->eps()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public eoF(II)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/b;->eUM:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Landroid/support/f/a/b;->eoG(II)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Landroid/support/f/a/b;->eUM:Landroid/graphics/Bitmap;

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/f/a/b;->eUE:Z

    :cond_1
    return-void
.end method

.method public eoG(II)Z
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/b;->eUM:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/b;->eUM:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public eoH()V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/b;->eUL:Landroid/content/res/ColorStateList;

    iput-object v0, p0, Landroid/support/f/a/b;->eUH:Landroid/content/res/ColorStateList;

    iget-object v0, p0, Landroid/support/f/a/b;->eUJ:Landroid/graphics/PorterDuff$Mode;

    iput-object v0, p0, Landroid/support/f/a/b;->eUI:Landroid/graphics/PorterDuff$Mode;

    iget-object v0, p0, Landroid/support/f/a/b;->eUK:Landroid/support/f/a/n;

    invoke-virtual {v0}, Landroid/support/f/a/n;->eps()I

    move-result v0

    iput v0, p0, Landroid/support/f/a/b;->eUG:I

    iget-boolean v0, p0, Landroid/support/f/a/b;->eUO:Z

    iput-boolean v0, p0, Landroid/support/f/a/b;->eUF:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/f/a/b;->eUE:Z

    return-void
.end method

.method public getChangingConfigurations()I
    .locals 1

    iget v0, p0, Landroid/support/f/a/b;->eUN:I

    return v0
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    new-instance v0, Landroid/support/f/a/q;

    invoke-direct {v0, p0}, Landroid/support/f/a/q;-><init>(Landroid/support/f/a/b;)V

    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1

    new-instance v0, Landroid/support/f/a/q;

    invoke-direct {v0, p0}, Landroid/support/f/a/q;-><init>(Landroid/support/f/a/b;)V

    return-object v0
.end method
