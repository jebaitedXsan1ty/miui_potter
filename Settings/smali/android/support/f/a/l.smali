.class final Landroid/support/f/a/l;
.super Ljava/lang/Object;
.source "AnimatedVectorDrawableCompat.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field final synthetic eVF:Landroid/support/f/a/d;


# direct methods
.method constructor <init>(Landroid/support/f/a/d;)V
    .locals 0

    iput-object p1, p0, Landroid/support/f/a/l;->eVF:Landroid/support/f/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/l;->eVF:Landroid/support/f/a/d;

    invoke-virtual {v0}, Landroid/support/f/a/d;->invalidateSelf()V

    return-void
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/l;->eVF:Landroid/support/f/a/d;

    invoke-virtual {v0, p2, p3, p4}, Landroid/support/f/a/d;->scheduleSelf(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/l;->eVF:Landroid/support/f/a/d;

    invoke-virtual {v0, p2}, Landroid/support/f/a/d;->unscheduleSelf(Ljava/lang/Runnable;)V

    return-void
.end method
