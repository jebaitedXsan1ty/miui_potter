.class Landroid/support/f/a/n;
.super Ljava/lang/Object;
.source "VectorDrawableCompat.java"


# static fields
.field private static final eVH:Landroid/graphics/Matrix;


# instance fields
.field eVG:F

.field private eVI:Landroid/graphics/PathMeasure;

.field eVJ:I

.field private eVK:Landroid/graphics/Paint;

.field final eVL:Landroid/support/f/a/a;

.field eVM:F

.field private final eVN:Landroid/graphics/Path;

.field eVO:F

.field eVP:F

.field eVQ:Ljava/lang/String;

.field private eVR:I

.field private final eVS:Landroid/graphics/Matrix;

.field private final eVT:Landroid/graphics/Path;

.field private eVU:Landroid/graphics/Paint;

.field final eVV:Landroid/support/v4/a/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Landroid/support/f/a/n;->eVH:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/n;->eVS:Landroid/graphics/Matrix;

    iput v1, p0, Landroid/support/f/a/n;->eVG:F

    iput v1, p0, Landroid/support/f/a/n;->eVM:F

    iput v1, p0, Landroid/support/f/a/n;->eVP:F

    iput v1, p0, Landroid/support/f/a/n;->eVO:F

    const/16 v0, 0xff

    iput v0, p0, Landroid/support/f/a/n;->eVJ:I

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/f/a/n;->eVQ:Ljava/lang/String;

    new-instance v0, Landroid/support/v4/a/u;

    invoke-direct {v0}, Landroid/support/v4/a/u;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/n;->eVV:Landroid/support/v4/a/u;

    new-instance v0, Landroid/support/f/a/a;

    invoke-direct {v0}, Landroid/support/f/a/a;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/n;->eVL:Landroid/support/f/a/a;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/n;->eVN:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    return-void
.end method

.method public constructor <init>(Landroid/support/f/a/n;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/n;->eVS:Landroid/graphics/Matrix;

    iput v1, p0, Landroid/support/f/a/n;->eVG:F

    iput v1, p0, Landroid/support/f/a/n;->eVM:F

    iput v1, p0, Landroid/support/f/a/n;->eVP:F

    iput v1, p0, Landroid/support/f/a/n;->eVO:F

    const/16 v0, 0xff

    iput v0, p0, Landroid/support/f/a/n;->eVJ:I

    iput-object v2, p0, Landroid/support/f/a/n;->eVQ:Ljava/lang/String;

    new-instance v0, Landroid/support/v4/a/u;

    invoke-direct {v0}, Landroid/support/v4/a/u;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/n;->eVV:Landroid/support/v4/a/u;

    new-instance v0, Landroid/support/f/a/a;

    iget-object v1, p1, Landroid/support/f/a/n;->eVL:Landroid/support/f/a/a;

    iget-object v2, p0, Landroid/support/f/a/n;->eVV:Landroid/support/v4/a/u;

    invoke-direct {v0, v1, v2}, Landroid/support/f/a/a;-><init>(Landroid/support/f/a/a;Landroid/support/v4/a/u;)V

    iput-object v0, p0, Landroid/support/f/a/n;->eVL:Landroid/support/f/a/a;

    new-instance v0, Landroid/graphics/Path;

    iget-object v1, p1, Landroid/support/f/a/n;->eVN:Landroid/graphics/Path;

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    iput-object v0, p0, Landroid/support/f/a/n;->eVN:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    iget-object v1, p1, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    iput-object v0, p0, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    iget v0, p1, Landroid/support/f/a/n;->eVG:F

    iput v0, p0, Landroid/support/f/a/n;->eVG:F

    iget v0, p1, Landroid/support/f/a/n;->eVM:F

    iput v0, p0, Landroid/support/f/a/n;->eVM:F

    iget v0, p1, Landroid/support/f/a/n;->eVP:F

    iput v0, p0, Landroid/support/f/a/n;->eVP:F

    iget v0, p1, Landroid/support/f/a/n;->eVO:F

    iput v0, p0, Landroid/support/f/a/n;->eVO:F

    iget v0, p1, Landroid/support/f/a/n;->eVR:I

    iput v0, p0, Landroid/support/f/a/n;->eVR:I

    iget v0, p1, Landroid/support/f/a/n;->eVJ:I

    iput v0, p0, Landroid/support/f/a/n;->eVJ:I

    iget-object v0, p1, Landroid/support/f/a/n;->eVQ:Ljava/lang/String;

    iput-object v0, p0, Landroid/support/f/a/n;->eVQ:Ljava/lang/String;

    iget-object v0, p1, Landroid/support/f/a/n;->eVQ:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/f/a/n;->eVV:Landroid/support/v4/a/u;

    iget-object v1, p1, Landroid/support/f/a/n;->eVQ:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Landroid/support/v4/a/u;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private epA(Landroid/support/f/a/a;Landroid/support/f/a/k;Landroid/graphics/Canvas;IILandroid/graphics/ColorFilter;)V
    .locals 8

    int-to-float v0, p4

    iget v1, p0, Landroid/support/f/a/n;->eVP:F

    div-float/2addr v0, v1

    int-to-float v1, p5

    iget v2, p0, Landroid/support/f/a/n;->eVO:F

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {p1}, Landroid/support/f/a/a;->eou(Landroid/support/f/a/a;)Landroid/graphics/Matrix;

    move-result-object v3

    iget-object v4, p0, Landroid/support/f/a/n;->eVS:Landroid/graphics/Matrix;

    invoke-virtual {v4, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v4, p0, Landroid/support/f/a/n;->eVS:Landroid/graphics/Matrix;

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    invoke-direct {p0, v3}, Landroid/support/f/a/n;->epy(Landroid/graphics/Matrix;)F

    move-result v1

    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/f/a/n;->eVN:Landroid/graphics/Path;

    invoke-virtual {p2, v0}, Landroid/support/f/a/k;->epl(Landroid/graphics/Path;)V

    iget-object v0, p0, Landroid/support/f/a/n;->eVN:Landroid/graphics/Path;

    iget-object v3, p0, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    invoke-virtual {p2}, Landroid/support/f/a/k;->epn()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v1, p0, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    iget-object v2, p0, Landroid/support/f/a/n;->eVS:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    iget-object v0, p0, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    invoke-virtual {p3, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    check-cast p2, Landroid/support/f/a/e;

    iget v3, p2, Landroid/support/f/a/e;->eVg:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    iget v3, p2, Landroid/support/f/a/e;->eVf:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_5

    :cond_3
    iget v3, p2, Landroid/support/f/a/e;->eVg:F

    iget v4, p2, Landroid/support/f/a/e;->eUZ:F

    add-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    rem-float/2addr v3, v4

    iget v4, p2, Landroid/support/f/a/e;->eVf:F

    iget v5, p2, Landroid/support/f/a/e;->eUZ:F

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    rem-float/2addr v4, v5

    iget-object v5, p0, Landroid/support/f/a/n;->eVI:Landroid/graphics/PathMeasure;

    if-nez v5, :cond_4

    new-instance v5, Landroid/graphics/PathMeasure;

    invoke-direct {v5}, Landroid/graphics/PathMeasure;-><init>()V

    iput-object v5, p0, Landroid/support/f/a/n;->eVI:Landroid/graphics/PathMeasure;

    :cond_4
    iget-object v5, p0, Landroid/support/f/a/n;->eVI:Landroid/graphics/PathMeasure;

    iget-object v6, p0, Landroid/support/f/a/n;->eVN:Landroid/graphics/Path;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PathMeasure;->setPath(Landroid/graphics/Path;Z)V

    iget-object v5, p0, Landroid/support/f/a/n;->eVI:Landroid/graphics/PathMeasure;

    invoke-virtual {v5}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v5

    mul-float/2addr v3, v5

    mul-float/2addr v4, v5

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    cmpl-float v6, v3, v4

    if-lez v6, :cond_b

    iget-object v6, p0, Landroid/support/f/a/n;->eVI:Landroid/graphics/PathMeasure;

    const/4 v7, 0x1

    invoke-virtual {v6, v3, v5, v0, v7}, Landroid/graphics/PathMeasure;->getSegment(FFLandroid/graphics/Path;Z)Z

    iget-object v3, p0, Landroid/support/f/a/n;->eVI:Landroid/graphics/PathMeasure;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v4, v0, v6}, Landroid/graphics/PathMeasure;->getSegment(FFLandroid/graphics/Path;Z)Z

    :goto_1
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    :cond_5
    iget-object v3, p0, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    iget-object v4, p0, Landroid/support/f/a/n;->eVS:Landroid/graphics/Matrix;

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    iget v0, p2, Landroid/support/f/a/e;->eVb:I

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/f/a/n;->eVU:Landroid/graphics/Paint;

    if-nez v0, :cond_6

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/n;->eVU:Landroid/graphics/Paint;

    iget-object v0, p0, Landroid/support/f/a/n;->eVU:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Landroid/support/f/a/n;->eVU:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_6
    iget-object v3, p0, Landroid/support/f/a/n;->eVU:Landroid/graphics/Paint;

    iget v0, p2, Landroid/support/f/a/e;->eVb:I

    iget v4, p2, Landroid/support/f/a/e;->eVa:F

    invoke-static {v0, v4}, Landroid/support/f/a/q;->epI(IF)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v3, p6}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    iget-object v4, p0, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    iget v0, p2, Landroid/support/f/a/e;->eVh:I

    if-nez v0, :cond_c

    sget-object v0, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    :goto_2
    invoke-virtual {v4, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    iget-object v0, p0, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    invoke-virtual {p3, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_7
    iget v0, p2, Landroid/support/f/a/e;->eUV:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/f/a/n;->eVK:Landroid/graphics/Paint;

    if-nez v0, :cond_8

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/f/a/n;->eVK:Landroid/graphics/Paint;

    iget-object v0, p0, Landroid/support/f/a/n;->eVK:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Landroid/support/f/a/n;->eVK:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_8
    iget-object v0, p0, Landroid/support/f/a/n;->eVK:Landroid/graphics/Paint;

    iget-object v3, p2, Landroid/support/f/a/e;->eVc:Landroid/graphics/Paint$Join;

    if-eqz v3, :cond_9

    iget-object v3, p2, Landroid/support/f/a/e;->eVc:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    :cond_9
    iget-object v3, p2, Landroid/support/f/a/e;->eUW:Landroid/graphics/Paint$Cap;

    if-eqz v3, :cond_a

    iget-object v3, p2, Landroid/support/f/a/e;->eUW:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    :cond_a
    iget v3, p2, Landroid/support/f/a/e;->eUX:F

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeMiter(F)V

    iget v3, p2, Landroid/support/f/a/e;->eUV:I

    iget v4, p2, Landroid/support/f/a/e;->eVe:F

    invoke-static {v3, v4}, Landroid/support/f/a/q;->epI(IF)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    mul-float/2addr v1, v2

    iget v2, p2, Landroid/support/f/a/e;->eVd:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Landroid/support/f/a/n;->eVT:Landroid/graphics/Path;

    invoke-virtual {p3, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_b
    iget-object v5, p0, Landroid/support/f/a/n;->eVI:Landroid/graphics/PathMeasure;

    const/4 v6, 0x1

    invoke-virtual {v5, v3, v4, v0, v6}, Landroid/graphics/PathMeasure;->getSegment(FFLandroid/graphics/Path;Z)Z

    goto/16 :goto_1

    :cond_c
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    goto :goto_2
.end method

.method static synthetic epB(Landroid/support/f/a/n;Landroid/graphics/Paint;)Landroid/graphics/Paint;
    .locals 0

    iput-object p1, p0, Landroid/support/f/a/n;->eVK:Landroid/graphics/Paint;

    return-object p1
.end method

.method static synthetic epq(Landroid/support/f/a/n;Landroid/graphics/Paint;)Landroid/graphics/Paint;
    .locals 0

    iput-object p1, p0, Landroid/support/f/a/n;->eVU:Landroid/graphics/Paint;

    return-object p1
.end method

.method private epr(Landroid/support/f/a/a;Landroid/graphics/Matrix;Landroid/graphics/Canvas;IILandroid/graphics/ColorFilter;)V
    .locals 8

    invoke-static {p1}, Landroid/support/f/a/a;->eou(Landroid/support/f/a/a;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    invoke-static {p1}, Landroid/support/f/a/a;->eou(Landroid/support/f/a/a;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-static {p1}, Landroid/support/f/a/a;->eoz(Landroid/support/f/a/a;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    invoke-virtual {p3}, Landroid/graphics/Canvas;->save()I

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    iget-object v0, p1, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v7, v0, :cond_2

    iget-object v0, p1, Landroid/support/f/a/a;->eUB:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v0, v1, Landroid/support/f/a/a;

    if-eqz v0, :cond_1

    check-cast v1, Landroid/support/f/a/a;

    invoke-static {p1}, Landroid/support/f/a/a;->eou(Landroid/support/f/a/a;)Landroid/graphics/Matrix;

    move-result-object v2

    move-object v0, p0

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Landroid/support/f/a/n;->epr(Landroid/support/f/a/a;Landroid/graphics/Matrix;Landroid/graphics/Canvas;IILandroid/graphics/ColorFilter;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_1
    instance-of v0, v1, Landroid/support/f/a/k;

    if-eqz v0, :cond_0

    move-object v2, v1

    check-cast v2, Landroid/support/f/a/k;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Landroid/support/f/a/n;->epA(Landroid/support/f/a/a;Landroid/support/f/a/k;Landroid/graphics/Canvas;IILandroid/graphics/ColorFilter;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p3}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method static synthetic ept(Landroid/support/f/a/n;)Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/n;->eVU:Landroid/graphics/Paint;

    return-object v0
.end method

.method private static epv(FFFF)F
    .locals 2

    mul-float v0, p0, p3

    mul-float v1, p1, p2

    sub-float/2addr v0, v1

    return v0
.end method

.method static synthetic epx(Landroid/support/f/a/n;)Landroid/graphics/Paint;
    .locals 1

    iget-object v0, p0, Landroid/support/f/a/n;->eVK:Landroid/graphics/Paint;

    return-object v0
.end method

.method private epy(Landroid/graphics/Matrix;)F
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x4

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->mapVectors([F)V

    aget v2, v1, v8

    float-to-double v2, v2

    aget v4, v1, v9

    float-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v2, v2

    aget v3, v1, v10

    float-to-double v4, v3

    aget v3, v1, v11

    float-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v4

    double-to-float v3, v4

    aget v4, v1, v8

    aget v5, v1, v9

    aget v6, v1, v10

    aget v1, v1, v11

    invoke-static {v4, v5, v6, v1}, Landroid/support/f/a/n;->epv(FFFF)F

    move-result v1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    cmpl-float v3, v2, v0

    if-lez v3, :cond_0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    div-float/2addr v0, v2

    :cond_0
    return v0

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public eps()I
    .locals 1

    iget v0, p0, Landroid/support/f/a/n;->eVJ:I

    return v0
.end method

.method public epu(I)V
    .locals 0

    iput p1, p0, Landroid/support/f/a/n;->eVJ:I

    return-void
.end method

.method public epw(F)V
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Landroid/support/f/a/n;->epu(I)V

    return-void
.end method

.method public epz(Landroid/graphics/Canvas;IILandroid/graphics/ColorFilter;)V
    .locals 7

    iget-object v1, p0, Landroid/support/f/a/n;->eVL:Landroid/support/f/a/a;

    sget-object v2, Landroid/support/f/a/n;->eVH:Landroid/graphics/Matrix;

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Landroid/support/f/a/n;->epr(Landroid/support/f/a/a;Landroid/graphics/Matrix;Landroid/graphics/Canvas;IILandroid/graphics/ColorFilter;)V

    return-void
.end method

.method public getAlpha()F
    .locals 2

    invoke-virtual {p0}, Landroid/support/f/a/n;->eps()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    return v0
.end method
