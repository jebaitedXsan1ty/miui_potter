.class public abstract Landroid/support/v14/preference/b;
.super Landroid/app/Fragment;
.source "PreferenceFragment.java"

# interfaces
.implements Landroid/support/v7/preference/o;
.implements Landroid/support/v7/preference/p;
.implements Landroid/support/v7/preference/q;
.implements Landroid/support/v7/preference/e;


# instance fields
.field private final dEO:Ljava/lang/Runnable;

.field private dEP:Ljava/lang/Runnable;

.field private final dEQ:Landroid/support/v14/preference/f;

.field private dER:Landroid/os/Handler;

.field private dES:I

.field private dET:Landroid/content/Context;

.field private dEU:Landroid/support/v7/preference/k;

.field private dEV:Z

.field private dEW:Z

.field private dEX:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    sget v0, Landroid/support/v7/preference/c;->dIh:I

    iput v0, p0, Landroid/support/v14/preference/b;->dES:I

    new-instance v0, Landroid/support/v14/preference/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v14/preference/f;-><init>(Landroid/support/v14/preference/b;Landroid/support/v14/preference/f;)V

    iput-object v0, p0, Landroid/support/v14/preference/b;->dEQ:Landroid/support/v14/preference/f;

    new-instance v0, Landroid/support/v14/preference/g;

    invoke-direct {v0, p0}, Landroid/support/v14/preference/g;-><init>(Landroid/support/v14/preference/b;)V

    iput-object v0, p0, Landroid/support/v14/preference/b;->dER:Landroid/os/Handler;

    new-instance v0, Landroid/support/v14/preference/h;

    invoke-direct {v0, p0}, Landroid/support/v14/preference/h;-><init>(Landroid/support/v14/preference/b;)V

    iput-object v0, p0, Landroid/support/v14/preference/b;->dEO:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic djs(Landroid/support/v14/preference/b;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v14/preference/b;->djx()V

    return-void
.end method

.method private djv()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/preference/PreferenceScreen;->iN()V

    :cond_0
    invoke-virtual {p0}, Landroid/support/v14/preference/b;->onUnbindPreferences()V

    return-void
.end method

.method static synthetic djw(Landroid/support/v14/preference/b;)Landroid/support/v7/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEX:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method private djx()V
    .locals 3

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getListView()Landroid/support/v7/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {p0, v0}, Landroid/support/v14/preference/b;->bwt(Landroid/support/v7/preference/PreferenceScreen;)Landroid/support/v7/widget/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/b;)V

    invoke-virtual {v0}, Landroid/support/v7/preference/PreferenceScreen;->iM()V

    :cond_0
    invoke-virtual {p0}, Landroid/support/v14/preference/b;->onBindPreferences()V

    return-void
.end method

.method private djy()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Landroid/support/v14/preference/b;->dER:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v14/preference/b;->dER:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method private djz()V
    .locals 2

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "This should be called after super.onCreate."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public abstract EW(Landroid/os/Bundle;Ljava/lang/String;)V
.end method

.method public EX(Landroid/support/v7/preference/Preference;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->dkF()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djq()Landroid/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Landroid/support/v14/preference/c;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djq()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v14/preference/c;

    invoke-interface {v0, p0, p1}, Landroid/support/v14/preference/c;->djB(Landroid/support/v14/preference/b;Landroid/support/v7/preference/Preference;)Z

    move-result v0

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Landroid/support/v14/preference/c;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/support/v14/preference/c;

    invoke-interface {v0, p0, p1}, Landroid/support/v14/preference/c;->djB(Landroid/support/v14/preference/b;Landroid/support/v7/preference/Preference;)Z

    move-result v0

    :cond_1
    return v0

    :cond_2
    return v0
.end method

.method public ajw(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    invoke-virtual {v0, p1}, Landroid/support/v7/preference/k;->dlG(Landroid/support/v7/preference/PreferenceScreen;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->onUnbindPreferences()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v14/preference/b;->dEW:Z

    iget-boolean v0, p0, Landroid/support/v14/preference/b;->dEV:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v14/preference/b;->djy()V

    :cond_0
    return-void
.end method

.method public bwf(I)V
    .locals 3

    invoke-direct {p0}, Landroid/support/v14/preference/b;->djz()V

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    iget-object v1, p0, Landroid/support/v14/preference/b;->dET:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v7/preference/k;->dlM(Landroid/content/Context;ILandroid/support/v7/preference/PreferenceScreen;)Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v14/preference/b;->ajw(Landroid/support/v7/preference/PreferenceScreen;)V

    return-void
.end method

.method protected bwt(Landroid/support/v7/preference/PreferenceScreen;)Landroid/support/v7/widget/b;
    .locals 1

    new-instance v0, Landroid/support/v7/preference/y;

    invoke-direct {v0, p1}, Landroid/support/v7/preference/y;-><init>(Landroid/support/v7/preference/PreferenceGroup;)V

    return-object v0
.end method

.method public bwu()Landroid/support/v7/widget/a;
    .locals 2

    new-instance v0, Landroid/support/v7/widget/al;

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/al;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public bwx(Landroid/support/v7/preference/Preference;)V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djq()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v14/preference/e;

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djq()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v14/preference/e;

    invoke-interface {v0, p0, p1}, Landroid/support/v14/preference/e;->djD(Landroid/support/v14/preference/b;Landroid/support/v7/preference/Preference;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v2

    instance-of v2, v2, Landroid/support/v14/preference/e;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/support/v14/preference/e;

    invoke-interface {v0, p0, p1}, Landroid/support/v14/preference/e;->djD(Landroid/support/v14/preference/b;Landroid/support/v7/preference/Preference;)Z

    move-result v0

    :cond_0
    if-eqz v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "android.support.v14.preference.PreferenceFragment.DIALOG"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    instance-of v0, p1, Landroid/support/v7/preference/EditTextPreference;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v14/preference/m;->djT(Ljava/lang/String;)Landroid/support/v14/preference/m;

    move-result-object v0

    :goto_1
    invoke-virtual {v0, p0, v1}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "android.support.v14.preference.PreferenceFragment.DIALOG"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_3
    instance-of v0, p1, Landroid/support/v7/preference/ListPreference;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v14/preference/j;->djO(Ljava/lang/String;)Landroid/support/v14/preference/j;

    move-result-object v0

    goto :goto_1

    :cond_4
    instance-of v0, p1, Landroid/support/v14/preference/MultiSelectListPreference;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v14/preference/p;->dke(Ljava/lang/String;)Landroid/support/v14/preference/p;

    move-result-object v0

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Tried to display dialog for unknown preference type. Did you forget to override onDisplayPreferenceDialog()?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public djA(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEQ:Landroid/support/v14/preference/f;

    invoke-virtual {v0, p1}, Landroid/support/v14/preference/f;->djF(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public djn(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djq()Landroid/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Landroid/support/v14/preference/d;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djq()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v14/preference/d;

    invoke-interface {v0, p0, p1}, Landroid/support/v14/preference/d;->djC(Landroid/support/v14/preference/b;Landroid/support/v7/preference/PreferenceScreen;)Z

    move-result v0

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v14/preference/d;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/support/v14/preference/d;

    invoke-interface {v0, p0, p1}, Landroid/support/v14/preference/d;->djC(Landroid/support/v14/preference/b;Landroid/support/v7/preference/PreferenceScreen;)Z

    :cond_1
    return-void
.end method

.method public djo(I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEQ:Landroid/support/v14/preference/f;

    invoke-virtual {v0, p1}, Landroid/support/v14/preference/f;->djG(I)V

    return-void
.end method

.method public djp(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/support/v7/widget/RecyclerView;
    .locals 2

    sget v0, Landroid/support/v7/preference/c;->dIi:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->bwu()Landroid/support/v7/widget/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/a;)V

    new-instance v1, Landroid/support/v7/preference/E;

    invoke-direct {v1, v0}, Landroid/support/v7/preference/E;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAccessibilityDelegateCompat(Landroid/support/v7/widget/F;)V

    return-object v0
.end method

.method public djq()Landroid/app/Fragment;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public djr()Landroid/support/v7/preference/PreferenceScreen;
    .locals 1

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    invoke-virtual {v0}, Landroid/support/v7/preference/k;->dlO()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    return-object v0
.end method

.method public djt(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    invoke-virtual {v0, p1}, Landroid/support/v7/preference/k;->dlS(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    return-object v0
.end method

.method public dju()Landroid/support/v7/preference/k;
    .locals 1

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    return-object v0
.end method

.method public final getListView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEX:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "android:preferences"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dkB(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method protected onBindPreferences()V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v0, 0x0

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget v3, Landroid/support/v7/preference/a;->dHY:I

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must specify preferenceTheme in theme"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v2, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Landroid/support/v14/preference/b;->dET:Landroid/content/Context;

    new-instance v1, Landroid/support/v7/preference/k;

    iget-object v2, p0, Landroid/support/v14/preference/b;->dET:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v7/preference/k;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    iget-object v1, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    invoke-virtual {v1, p0}, Landroid/support/v7/preference/k;->dlJ(Landroid/support/v7/preference/q;)V

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "android.support.v7.preference.PreferenceFragmentCompat.PREFERENCE_ROOT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, p1, v0}, Landroid/support/v14/preference/b;->EW(Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v7, -0x1

    iget-object v0, p0, Landroid/support/v14/preference/b;->dET:Landroid/content/Context;

    sget-object v1, Landroid/support/v14/preference/a;->dDy:[I

    iget-object v2, p0, Landroid/support/v14/preference/b;->dET:Landroid/content/Context;

    sget v3, Landroid/support/v7/preference/a;->dHW:I

    const v4, 0x1010506

    invoke-static {v2, v3, v4}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v2

    invoke-virtual {v0, v5, v1, v2, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Landroid/support/v14/preference/a;->dDD:I

    iget v2, p0, Landroid/support/v14/preference/b;->dES:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v14/preference/b;->dES:I

    sget v1, Landroid/support/v14/preference/a;->dDB:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget v2, Landroid/support/v14/preference/a;->dDC:I

    invoke-virtual {v0, v2, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    sget v3, Landroid/support/v14/preference/a;->dDA:I

    invoke-virtual {v0, v3, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    sget v5, Landroid/support/v7/preference/a;->dHY:I

    invoke-virtual {v4, v5, v0, v8}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    new-instance v4, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v4}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    iget v0, p0, Landroid/support/v14/preference/b;->dES:I

    invoke-virtual {v4, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    const v0, 0x102003f

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v6, v0, Landroid/view/ViewGroup;

    if-nez v6, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Content has view with id attribute \'android.R.id.list_container\' that is not a ViewGroup class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v4, v0, p3}, Landroid/support/v14/preference/b;->djp(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v4

    if-nez v4, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Could not create RecyclerView"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object v4, p0, Landroid/support/v14/preference/b;->dEX:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, p0, Landroid/support/v14/preference/b;->dEQ:Landroid/support/v14/preference/f;

    invoke-virtual {v4, v6}, Landroid/support/v7/widget/RecyclerView;->dpM(Landroid/support/v7/widget/d;)V

    invoke-virtual {p0, v1}, Landroid/support/v14/preference/b;->djA(Landroid/graphics/drawable/Drawable;)V

    if-eq v2, v7, :cond_2

    invoke-virtual {p0, v2}, Landroid/support/v14/preference/b;->djo(I)V

    :cond_2
    iget-object v1, p0, Landroid/support/v14/preference/b;->dEQ:Landroid/support/v14/preference/f;

    invoke-virtual {v1, v3}, Landroid/support/v14/preference/f;->djE(Z)V

    iget-object v1, p0, Landroid/support/v14/preference/b;->dEX:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Landroid/support/v14/preference/b;->dER:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v14/preference/b;->dEO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-object v5
.end method

.method public onDestroyView()V
    .locals 2

    iget-object v0, p0, Landroid/support/v14/preference/b;->dER:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v14/preference/b;->dEO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Landroid/support/v14/preference/b;->dER:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-boolean v0, p0, Landroid/support/v14/preference/b;->dEW:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v14/preference/b;->djv()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v14/preference/b;->dEX:Landroid/support/v7/widget/RecyclerView;

    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/support/v14/preference/b;->djr()Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceScreen;->dkt(Landroid/os/Bundle;)V

    const-string/jumbo v0, "android:preferences"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    invoke-virtual {v0, p0}, Landroid/support/v7/preference/k;->dlH(Landroid/support/v7/preference/o;)V

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    invoke-virtual {v0, p0}, Landroid/support/v7/preference/k;->dlU(Landroid/support/v7/preference/p;)V

    return-void
.end method

.method public onStop()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/k;->dlH(Landroid/support/v7/preference/o;)V

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEU:Landroid/support/v7/preference/k;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/k;->dlU(Landroid/support/v7/preference/p;)V

    return-void
.end method

.method protected onUnbindPreferences()V
    .locals 0

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-boolean v0, p0, Landroid/support/v14/preference/b;->dEW:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v14/preference/b;->djx()V

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEP:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v14/preference/b;->dEP:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iput-object v1, p0, Landroid/support/v14/preference/b;->dEP:Ljava/lang/Runnable;

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v14/preference/b;->dEV:Z

    return-void
.end method
