.class public Landroid/support/v14/preference/SwitchPreference;
.super Landroid/support/v7/preference/TwoStatePreference;
.source "SwitchPreference.java"


# instance fields
.field private final dFe:Landroid/support/v14/preference/i;

.field private dFf:Ljava/lang/CharSequence;

.field private dFg:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v14/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    sget v0, Landroid/support/v7/preference/a;->dIa:I

    const v1, 0x101036d

    invoke-static {p1, v0, v1}, Landroid/support/v4/content/a/a;->dZs(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v14/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v14/preference/SwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/preference/TwoStatePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Landroid/support/v14/preference/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v14/preference/i;-><init>(Landroid/support/v14/preference/SwitchPreference;Landroid/support/v14/preference/i;)V

    iput-object v0, p0, Landroid/support/v14/preference/SwitchPreference;->dFe:Landroid/support/v14/preference/i;

    sget-object v0, Landroid/support/v14/preference/a;->dEq:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Landroid/support/v14/preference/a;->dEz:I

    sget v2, Landroid/support/v14/preference/a;->dEu:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v14/preference/SwitchPreference;->dmm(Ljava/lang/CharSequence;)V

    sget v1, Landroid/support/v14/preference/a;->dEy:I

    sget v2, Landroid/support/v14/preference/a;->dEt:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v14/preference/SwitchPreference;->dmj(Ljava/lang/CharSequence;)V

    sget v1, Landroid/support/v14/preference/a;->dEB:I

    sget v2, Landroid/support/v14/preference/a;->dEw:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v14/preference/SwitchPreference;->djI(Ljava/lang/CharSequence;)V

    sget v1, Landroid/support/v14/preference/a;->dEA:I

    sget v2, Landroid/support/v14/preference/a;->dEv:I

    invoke-static {v0, v1, v2}, Landroid/support/v4/content/a/a;->dZz(Landroid/content/res/TypedArray;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v14/preference/SwitchPreference;->djK(Ljava/lang/CharSequence;)V

    sget v1, Landroid/support/v14/preference/a;->dEx:I

    sget v2, Landroid/support/v14/preference/a;->dEs:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/content/a/a;->dZw(Landroid/content/res/TypedArray;IIZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v14/preference/SwitchPreference;->dmk(Z)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private djJ(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v14/preference/SwitchPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x1020040

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v14/preference/SwitchPreference;->djL(Landroid/view/View;)V

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v14/preference/SwitchPreference;->dml(Landroid/view/View;)V

    return-void
.end method

.method private djL(Landroid/view/View;)V
    .locals 2

    instance-of v0, p1, Landroid/widget/Switch;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_0
    instance-of v0, p1, Landroid/widget/Checkable;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/widget/Checkable;

    iget-boolean v1, p0, Landroid/support/v14/preference/SwitchPreference;->dMU:Z

    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_1
    instance-of v0, p1, Landroid/widget/Switch;

    if-eqz v0, :cond_2

    check-cast p1, Landroid/widget/Switch;

    iget-object v0, p0, Landroid/support/v14/preference/SwitchPreference;->dFf:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/Switch;->setTextOn(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Landroid/support/v14/preference/SwitchPreference;->dFg:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/Switch;->setTextOff(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Landroid/support/v14/preference/SwitchPreference;->dFe:Landroid/support/v14/preference/i;

    invoke-virtual {p1, v0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public al(Landroid/support/v7/preference/l;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/preference/TwoStatePreference;->al(Landroid/support/v7/preference/l;)V

    const v0, 0x1020040

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/l;->dma(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v14/preference/SwitchPreference;->djL(Landroid/view/View;)V

    invoke-virtual {p0, p1}, Landroid/support/v14/preference/SwitchPreference;->dmn(Landroid/support/v7/preference/l;)V

    return-void
.end method

.method public djI(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v14/preference/SwitchPreference;->dFf:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/support/v14/preference/SwitchPreference;->notifyChanged()V

    return-void
.end method

.method public djK(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v14/preference/SwitchPreference;->dFg:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/support/v14/preference/SwitchPreference;->notifyChanged()V

    return-void
.end method

.method protected iY(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v7/preference/TwoStatePreference;->iY(Landroid/view/View;)V

    invoke-direct {p0, p1}, Landroid/support/v14/preference/SwitchPreference;->djJ(Landroid/view/View;)V

    return-void
.end method
