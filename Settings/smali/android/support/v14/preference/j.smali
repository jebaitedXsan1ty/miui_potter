.class public Landroid/support/v14/preference/j;
.super Landroid/support/v14/preference/k;
.source "ListPreferenceDialogFragment.java"


# instance fields
.field private dFi:[Ljava/lang/CharSequence;

.field private dFj:[Ljava/lang/CharSequence;

.field private dFk:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v14/preference/k;-><init>()V

    return-void
.end method

.method static synthetic djM(Landroid/support/v14/preference/j;I)I
    .locals 0

    iput p1, p0, Landroid/support/v14/preference/j;->dFk:I

    return p1
.end method

.method private djN()Landroid/support/v7/preference/ListPreference;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v14/preference/j;->djS()Landroid/support/v7/preference/DialogPreference;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/ListPreference;

    return-object v0
.end method

.method public static djO(Ljava/lang/String;)Landroid/support/v14/preference/j;
    .locals 3

    new-instance v0, Landroid/support/v14/preference/j;

    invoke-direct {v0}, Landroid/support/v14/preference/j;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "key"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v14/preference/j;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v14/preference/k;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_2

    invoke-direct {p0}, Landroid/support/v14/preference/j;->djN()Landroid/support/v7/preference/ListPreference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/ListPreference;->dmx()[Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/preference/ListPreference;->dmv()[Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "ListPreference requires an entries array and an entryValues array."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/ListPreference;->dmu(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Landroid/support/v14/preference/j;->dFk:I

    invoke-virtual {v0}, Landroid/support/v7/preference/ListPreference;->dmx()[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v14/preference/j;->dFi:[Ljava/lang/CharSequence;

    invoke-virtual {v0}, Landroid/support/v7/preference/ListPreference;->dmv()[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v14/preference/j;->dFj:[Ljava/lang/CharSequence;

    :goto_0
    return-void

    :cond_2
    const-string/jumbo v0, "ListPreferenceDialogFragment.index"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v14/preference/j;->dFk:I

    const-string/jumbo v0, "ListPreferenceDialogFragment.entries"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v14/preference/j;->dFi:[Ljava/lang/CharSequence;

    const-string/jumbo v0, "ListPreferenceDialogFragment.entryValues"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v14/preference/j;->dFj:[Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public onDialogClosed(Z)V
    .locals 3

    invoke-direct {p0}, Landroid/support/v14/preference/j;->djN()Landroid/support/v7/preference/ListPreference;

    move-result-object v0

    if-eqz p1, :cond_0

    iget v1, p0, Landroid/support/v14/preference/j;->dFk:I

    if-ltz v1, :cond_0

    iget-object v1, p0, Landroid/support/v14/preference/j;->dFj:[Ljava/lang/CharSequence;

    iget v2, p0, Landroid/support/v14/preference/j;->dFk:I

    aget-object v1, v1, v2

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/ListPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/ListPreference;->setValue(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/support/v14/preference/k;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    iget-object v0, p0, Landroid/support/v14/preference/j;->dFi:[Ljava/lang/CharSequence;

    iget v1, p0, Landroid/support/v14/preference/j;->dFk:I

    new-instance v2, Landroid/support/v14/preference/l;

    invoke-direct {v2, p0}, Landroid/support/v14/preference/l;-><init>(Landroid/support/v14/preference/j;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v14/preference/k;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "ListPreferenceDialogFragment.index"

    iget v1, p0, Landroid/support/v14/preference/j;->dFk:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "ListPreferenceDialogFragment.entries"

    iget-object v1, p0, Landroid/support/v14/preference/j;->dFi:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "ListPreferenceDialogFragment.entryValues"

    iget-object v1, p0, Landroid/support/v14/preference/j;->dFj:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V

    return-void
.end method
