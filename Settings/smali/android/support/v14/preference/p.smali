.class public Landroid/support/v14/preference/p;
.super Landroid/support/v14/preference/k;
.source "MultiSelectListPreferenceDialogFragment.java"


# instance fields
.field private dFB:Ljava/util/Set;

.field private dFC:[Ljava/lang/CharSequence;

.field private dFD:[Ljava/lang/CharSequence;

.field private dFE:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v14/preference/k;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Landroid/support/v14/preference/p;->dFB:Ljava/util/Set;

    return-void
.end method

.method static synthetic dka(Landroid/support/v14/preference/p;)Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v14/preference/p;->dFE:Z

    return v0
.end method

.method private dkb()Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v14/preference/p;->djS()Landroid/support/v7/preference/DialogPreference;

    move-result-object v0

    check-cast v0, Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;

    return-object v0
.end method

.method static synthetic dkc(Landroid/support/v14/preference/p;)[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/support/v14/preference/p;->dFC:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic dkd(Landroid/support/v14/preference/p;Z)Z
    .locals 0

    iput-boolean p1, p0, Landroid/support/v14/preference/p;->dFE:Z

    return p1
.end method

.method public static dke(Ljava/lang/String;)Landroid/support/v14/preference/p;
    .locals 3

    new-instance v0, Landroid/support/v14/preference/p;

    invoke-direct {v0}, Landroid/support/v14/preference/p;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "key"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v14/preference/p;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic dkf(Landroid/support/v14/preference/p;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Landroid/support/v14/preference/p;->dFB:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/support/v14/preference/k;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_2

    invoke-direct {p0}, Landroid/support/v14/preference/p;->dkb()Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;->djZ()[Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;->djW()[Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "MultiSelectListPreference requires an entries array and an entryValues array."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p0, Landroid/support/v14/preference/p;->dFB:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    iget-object v1, p0, Landroid/support/v14/preference/p;->dFB:Ljava/util/Set;

    invoke-virtual {v0}, Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;->djY()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iput-boolean v3, p0, Landroid/support/v14/preference/p;->dFE:Z

    invoke-virtual {v0}, Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;->djZ()[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v14/preference/p;->dFD:[Ljava/lang/CharSequence;

    invoke-virtual {v0}, Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;->djW()[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v14/preference/p;->dFC:[Ljava/lang/CharSequence;

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v14/preference/p;->dFB:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Landroid/support/v14/preference/p;->dFB:Ljava/util/Set;

    const-string/jumbo v1, "MultiSelectListPreferenceDialogFragment.values"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    const-string/jumbo v0, "MultiSelectListPreferenceDialogFragment.changed"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v14/preference/p;->dFE:Z

    const-string/jumbo v0, "MultiSelectListPreferenceDialogFragment.entries"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v14/preference/p;->dFD:[Ljava/lang/CharSequence;

    const-string/jumbo v0, "MultiSelectListPreferenceDialogFragment.entryValues"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequenceArray(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v14/preference/p;->dFC:[Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public onDialogClosed(Z)V
    .locals 3

    invoke-direct {p0}, Landroid/support/v14/preference/p;->dkb()Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Landroid/support/v14/preference/p;->dFE:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v14/preference/p;->dFB:Ljava/util/Set;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/internal/AbstractMultiSelectListPreference;->djX(Ljava/util/Set;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v14/preference/p;->dFE:Z

    return-void
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/support/v14/preference/k;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    iget-object v0, p0, Landroid/support/v14/preference/p;->dFC:[Ljava/lang/CharSequence;

    array-length v1, v0

    new-array v2, v1, [Z

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Landroid/support/v14/preference/p;->dFB:Ljava/util/Set;

    iget-object v4, p0, Landroid/support/v14/preference/p;->dFC:[Ljava/lang/CharSequence;

    aget-object v4, v4, v0

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    aput-boolean v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v14/preference/p;->dFD:[Ljava/lang/CharSequence;

    new-instance v1, Landroid/support/v14/preference/o;

    invoke-direct {v1, p0}, Landroid/support/v14/preference/o;-><init>(Landroid/support/v14/preference/p;)V

    invoke-virtual {p1, v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v14/preference/k;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "MultiSelectListPreferenceDialogFragment.values"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v14/preference/p;->dFB:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string/jumbo v0, "MultiSelectListPreferenceDialogFragment.changed"

    iget-boolean v1, p0, Landroid/support/v14/preference/p;->dFE:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "MultiSelectListPreferenceDialogFragment.entries"

    iget-object v1, p0, Landroid/support/v14/preference/p;->dFD:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "MultiSelectListPreferenceDialogFragment.entryValues"

    iget-object v1, p0, Landroid/support/v14/preference/p;->dFC:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V

    return-void
.end method
