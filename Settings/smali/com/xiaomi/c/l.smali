.class public abstract Lcom/xiaomi/c/l;
.super Ljava/lang/Object;
.source "HttpUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static cVr(III)I
    .locals 1

    add-int/lit16 v0, p1, 0xc8

    div-int/lit16 v0, v0, 0x5a8

    mul-int/lit16 v0, v0, 0x84

    add-int/lit16 v0, v0, 0x3f3

    add-int/2addr v0, p1

    add-int/2addr v0, p0

    add-int/2addr v0, p2

    return v0
.end method

.method public static cVs(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Lcom/xiaomi/c/d;Z)Ljava/lang/String;
    .locals 12

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_0
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    if-nez p4, :cond_3

    :cond_1
    :goto_1
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_2
    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_2
    :goto_4
    return-object v3

    :cond_3
    invoke-static {}, Lcom/xiaomi/c/g;->getInstance()Lcom/xiaomi/c/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/xiaomi/c/g;->cVj(Ljava/lang/String;)Lcom/xiaomi/c/e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/xiaomi/c/e;->cUt(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0

    :cond_5
    :try_start_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez p2, :cond_6

    const/4 v2, 0x0

    move-object v7, v2

    :goto_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v10

    :try_start_2
    invoke-virtual {p3, p0, v1, v7}, Lcom/xiaomi/c/d;->cUk(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p3, p0, v1, v7}, Lcom/xiaomi/c/d;->cUj(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v8

    :try_start_3
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0

    move-result v2

    if-eqz v2, :cond_7

    if-nez v0, :cond_9

    :goto_6
    move-object v3, v8

    goto :goto_3

    :cond_6
    :try_start_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_0

    move-object v7, v2

    goto :goto_5

    :cond_7
    if-nez v0, :cond_8

    :goto_7
    move-object v3, v8

    goto :goto_4

    :cond_8
    :try_start_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-static {p3, v1, v7, v8}, Lcom/xiaomi/c/l;->cVw(Lcom/xiaomi/c/d;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/c/e;->cUu(Ljava/lang/String;JJ)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_7

    :catch_1
    move-exception v6

    :goto_8
    if-nez v0, :cond_a

    :goto_9
    :try_start_6
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catch Ljava/net/MalformedURLException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_6

    :cond_9
    :try_start_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-static {p3, v1, v7, v8}, Lcom/xiaomi/c/l;->cVw(Lcom/xiaomi/c/d;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/xiaomi/c/e;->cUr(Ljava/lang/String;JJLjava/lang/Exception;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_6

    :cond_a
    :try_start_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-static {p3, v1, v7, v8}, Lcom/xiaomi/c/l;->cVw(Lcom/xiaomi/c/d;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v6}, Lcom/xiaomi/c/e;->cUr(Ljava/lang/String;JJLjava/lang/Exception;)V
    :try_end_8
    .catch Ljava/net/MalformedURLException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_9

    :catch_2
    move-exception v6

    move-object v8, v3

    goto :goto_8
.end method

.method public static cVt(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/xiaomi/c/i;

    invoke-direct {v0}, Lcom/xiaomi/c/i;-><init>()V

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/xiaomi/c/l;->cVs(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Lcom/xiaomi/c/d;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static cVu(Ljava/util/List;)I
    .locals 4

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    mul-int/lit8 v0, v1, 0x2

    return v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/channel/commonutils/g/a;

    invoke-interface {v0}, Lcom/xiaomi/channel/commonutils/g/a;->cAw()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-interface {v0}, Lcom/xiaomi/channel/commonutils/g/a;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lcom/xiaomi/channel/commonutils/g/a;->cAw()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_1

    :cond_2
    invoke-interface {v0}, Lcom/xiaomi/channel/commonutils/g/a;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_2
.end method

.method static cVv(II)I
    .locals 1

    add-int/lit16 v0, p1, 0xf3

    div-int/lit16 v0, v0, 0x5a8

    mul-int/lit16 v0, v0, 0x84

    add-int/lit16 v0, v0, 0x438

    add-int/2addr v0, p0

    add-int/2addr v0, p1

    return v0
.end method

.method private static cVw(Lcom/xiaomi/c/d;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)I
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/c/d;->cUl()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/c/d;->cUl()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {p3}, Lcom/xiaomi/c/l;->cVx(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/xiaomi/c/l;->cVv(II)I

    move-result v0

    return v0

    :cond_1
    invoke-static {p2}, Lcom/xiaomi/c/l;->cVu(Ljava/util/List;)I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p3}, Lcom/xiaomi/c/l;->cVx(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/xiaomi/c/l;->cVr(III)I

    move-result v0

    return v0
.end method

.method static cVx(Ljava/lang/String;)I
    .locals 2

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    const-string/jumbo v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :cond_0
    return v1

    :catch_0
    move-exception v0

    return v1
.end method
