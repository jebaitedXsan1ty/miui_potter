.class public Lcom/xiaomi/c/j;
.super Ljava/lang/Object;
.source "AccessHistory.java"


# instance fields
.field private dlE:J

.field private dlF:J

.field private dlG:J

.field private dlH:I

.field private dlI:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 7

    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x0

    move-object v0, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/xiaomi/c/j;-><init>(IJJLjava/lang/Exception;)V

    return-void
.end method

.method public constructor <init>(IJJLjava/lang/Exception;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/xiaomi/c/j;->dlH:I

    iput-wide p2, p0, Lcom/xiaomi/c/j;->dlF:J

    iput-wide p4, p0, Lcom/xiaomi/c/j;->dlG:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/c/j;->dlE:J

    if-nez p6, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/c/j;->dlI:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public cVk()Lorg/json/JSONObject;
    .locals 4

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iget-wide v2, p0, Lcom/xiaomi/c/j;->dlF:J

    const-string/jumbo v1, "cost"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    iget-wide v2, p0, Lcom/xiaomi/c/j;->dlG:J

    const-string/jumbo v1, "size"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    iget-wide v2, p0, Lcom/xiaomi/c/j;->dlE:J

    const-string/jumbo v1, "ts"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    iget v1, p0, Lcom/xiaomi/c/j;->dlH:I

    const-string/jumbo v2, "wt"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/xiaomi/c/j;->dlI:Ljava/lang/String;

    const-string/jumbo v2, "expt"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0
.end method

.method public cVl(Lorg/json/JSONObject;)Lcom/xiaomi/c/j;
    .locals 2

    const-string/jumbo v0, "cost"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/c/j;->dlF:J

    const-string/jumbo v0, "size"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/c/j;->dlG:J

    const-string/jumbo v0, "ts"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/c/j;->dlE:J

    const-string/jumbo v0, "wt"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/c/j;->dlH:I

    const-string/jumbo v0, "expt"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/c/j;->dlI:Ljava/lang/String;

    return-object p0
.end method

.method public cVm()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/c/j;->dlH:I

    return v0
.end method
