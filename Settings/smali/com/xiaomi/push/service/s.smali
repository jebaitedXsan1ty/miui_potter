.class public Lcom/xiaomi/push/service/s;
.super Lcom/xiaomi/channel/commonutils/h/i;
.source "GeoFenceDBCleaner.java"


# instance fields
.field private deY:Lcom/xiaomi/push/service/XMPushService;


# direct methods
.method public constructor <init>(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/channel/commonutils/h/i;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/push/service/s;->deY:Lcom/xiaomi/push/service/XMPushService;

    return-void
.end method


# virtual methods
.method public cBi()I
    .locals 1

    const/16 v0, 0xf

    return v0
.end method

.method public run()V
    .locals 10

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/push/service/s;->deY:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/push/service/O;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/O;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/service/O;->cOr()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/service/module/a;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMo()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-ltz v1, :cond_2

    move v1, v2

    :goto_1
    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/push/service/s;->deY:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v1}, Lcom/xiaomi/push/service/O;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/O;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMr()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/xiaomi/push/service/O;->cOo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_3

    :goto_2
    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMk()[B

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/service/T;->cOw([B)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/s;->deY:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v1, v0, v3, v3, v2}, Lcom/xiaomi/push/service/T;->cOT(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;ZZZ)V

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "GeofenceDbCleaner delete a geofence message failed message_id:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/push/service/module/a;->cMr()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_2
.end method
