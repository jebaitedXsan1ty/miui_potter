.class public Lcom/xiaomi/push/service/aP;
.super Ljava/lang/Object;
.source "PacketSync.java"


# instance fields
.field private div:Lcom/xiaomi/push/service/XMPushService;


# direct methods
.method constructor <init>(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    return-void
.end method

.method private cSB(Lcom/xiaomi/smack/packet/d;)V
    .locals 7

    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/d;->cvO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/d;->cvN()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->pkgName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/d;->cvl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/smack/c/d;->cwB(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const/4 v4, 0x1

    invoke-static/range {v0 .. v6}, Lcom/xiaomi/smack/c/d;->cwA(Landroid/content/Context;Ljava/lang/String;JZJ)V

    goto :goto_0
.end method

.method private cSC(Lcom/xiaomi/smack/packet/g;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/g;->cvZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/c/g;->getInstance()Lcom/xiaomi/c/g;

    move-result-object v1

    invoke-static {}, Lcom/xiaomi/smack/h;->cyc()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/c/g;->cUO(Ljava/lang/String;Z)Lcom/xiaomi/c/e;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/xiaomi/c/e;->cUB([Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    const/16 v1, 0x14

    invoke-virtual {v0, v1, v4}, Lcom/xiaomi/push/service/XMPushService;->cPK(ILjava/lang/Exception;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cQf(Z)V

    goto :goto_0
.end method

.method private cSD(Lcom/xiaomi/a/e;)V
    .locals 7

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->pkgName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->getSerializedSize()I

    move-result v2

    int-to-long v2, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const/4 v4, 0x1

    invoke-static/range {v0 .. v6}, Lcom/xiaomi/smack/c/d;->cwA(Landroid/content/Context;Ljava/lang/String;JZJ)V

    goto :goto_0
.end method


# virtual methods
.method public cSA(Lcom/xiaomi/a/e;)V
    .locals 3

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v0

    const/4 v1, 0x5

    if-ne v1, v0, :cond_0

    :goto_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/xiaomi/push/service/aP;->cSz(Lcom/xiaomi/a/e;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/aP;->cSD(Lcom/xiaomi/a/e;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "handle Blob chid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " cmd = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " packetid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " failure "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/xiaomi/channel/commonutils/e/a;->czv(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public cSy(Lcom/xiaomi/smack/packet/d;)V
    .locals 12

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/d;->cvN()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "5"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/d;->cvN()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    const-string/jumbo v0, "0"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :goto_2
    instance-of v0, p1, Lcom/xiaomi/smack/packet/e;

    if-nez v0, :cond_4

    instance-of v0, p1, Lcom/xiaomi/smack/packet/c;

    if-nez v0, :cond_7

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/XMPushService;->cPD()Lcom/xiaomi/push/service/r;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v1, v7, p1}, Lcom/xiaomi/push/service/r;->cNz(Lcom/xiaomi/push/service/XMPushService;Ljava/lang/String;Lcom/xiaomi/smack/packet/d;)V

    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/aP;->cSB(Lcom/xiaomi/smack/packet/d;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v7, "1"

    invoke-virtual {p1, v7}, Lcom/xiaomi/smack/packet/d;->cvJ(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Received wrong packet with chid = 0 : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/d;->cvl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    const-string/jumbo v0, "kick"

    invoke-virtual {p1, v0}, Lcom/xiaomi/smack/packet/d;->cvy(Ljava/lang/String;)Lcom/xiaomi/smack/packet/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/smack/packet/d;->cvO()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/packet/g;->cwe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v1, "reason"

    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/packet/g;->cwe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "kicked by server, chid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v8}, Lcom/xiaomi/push/service/as;->cRm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    const-string/jumbo v0, "wait"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v6, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    move v9, v2

    move-object v10, v4

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/xiaomi/push/service/XMPushService;->cPx(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Lcom/xiaomi/push/service/aR;->cSM(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_3
    return-void

    :cond_6
    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/XMPushService;->cQd(Lcom/xiaomi/push/service/as;)V

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/as;->cRg(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    move-object v0, p1

    check-cast v0, Lcom/xiaomi/smack/packet/c;

    invoke-virtual {v0}, Lcom/xiaomi/smack/packet/c;->getType()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "redir"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "hosts"

    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/packet/c;->cvy(Ljava/lang/String;)Lcom/xiaomi/smack/packet/g;

    move-result-object v0

    if-nez v0, :cond_8

    :goto_4
    return-void

    :cond_8
    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/aP;->cSC(Lcom/xiaomi/smack/packet/g;)V

    goto :goto_4
.end method

.method public cSz(Lcom/xiaomi/a/e;)V
    .locals 11

    const/4 v3, 0x7

    const/4 v7, 0x3

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v5, "SECMSG"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string/jumbo v1, "BIND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string/jumbo v1, "KICK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string/jumbo v1, "PING"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "SYNC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "NOTIFY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCd()[B

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/a/b;->cIt([B)Lcom/xiaomi/push/a/b;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "notify by server err = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/push/a/b;->cIx()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " desc = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/push/a/b;->cIv()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCd()[B

    move-result-object v0

    if-nez v0, :cond_4

    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/xiaomi/e/d;->dgd()V

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/XMPushService;->cPy()V

    goto :goto_0

    :cond_4
    array-length v1, v0

    if-lez v1, :cond_2

    invoke-static {v0}, Lcom/xiaomi/push/a/l;->cLA([B)Lcom/xiaomi/push/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/a/l;->cLB()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/xiaomi/push/service/an;->getInstance()Lcom/xiaomi/push/service/an;

    move-result-object v1

    invoke-virtual {v0}, Lcom/xiaomi/push/a/l;->cLF()Lcom/xiaomi/push/a/g;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/an;->cQI(Lcom/xiaomi/push/a/g;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCa()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "CONF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCa()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "U"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCa()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "P"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCd()[B

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/a/h;->cKq([B)Lcom/xiaomi/push/a/h;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/a/e;

    invoke-direct {v1}, Lcom/xiaomi/a/e;-><init>()V

    invoke-virtual {v1, v10}, Lcom/xiaomi/a/e;->cBJ(I)V

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "PCA"

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/a/e;->cBM(Ljava/lang/String;)V

    new-instance v2, Lcom/xiaomi/push/a/h;

    invoke-direct {v2}, Lcom/xiaomi/push/a/h;-><init>()V

    invoke-virtual {v0}, Lcom/xiaomi/push/a/h;->cKo()Z

    move-result v3

    if-nez v3, :cond_8

    :goto_2
    invoke-virtual {v2}, Lcom/xiaomi/push/a/h;->diz()[B

    move-result-object v0

    invoke-virtual {v1, v0, v4}, Lcom/xiaomi/a/e;->cBN([BLjava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    new-instance v2, Lcom/xiaomi/push/service/g;

    iget-object v3, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-direct {v2, v3, v1}, Lcom/xiaomi/push/service/g;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/a/e;)V

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "ACK msgP: id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCd()[B

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/a/g;->cKk([B)Lcom/xiaomi/push/a/g;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/push/service/an;->getInstance()Lcom/xiaomi/push/service/an;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/an;->cQI(Lcom/xiaomi/push/a/g;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCd()[B

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/a/k;->cLr([B)Lcom/xiaomi/push/a/k;

    move-result-object v6

    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/push/b/f;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/b/f;

    move-result-object v0

    invoke-virtual {v6}, Lcom/xiaomi/push/a/k;->cLj()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/xiaomi/push/a/k;->cLn()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-virtual {v6}, Lcom/xiaomi/push/a/k;->cLu()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    new-instance v4, Ljava/util/Date;

    invoke-virtual {v6}, Lcom/xiaomi/push/a/k;->cLi()J

    move-result-wide v8

    invoke-direct {v4, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6}, Lcom/xiaomi/push/a/k;->cLx()I

    move-result v5

    mul-int/lit16 v5, v5, 0x400

    invoke-virtual {v6}, Lcom/xiaomi/push/a/k;->cLv()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/xiaomi/push/b/f;->cLW(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;IZ)V

    new-instance v0, Lcom/xiaomi/a/e;

    invoke-direct {v0}, Lcom/xiaomi/a/e;-><init>()V

    invoke-virtual {v0, v10}, Lcom/xiaomi/a/e;->cBJ(I)V

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "UCA"

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/a/e;->cBM(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    new-instance v2, Lcom/xiaomi/push/service/g;

    iget-object v3, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-direct {v2, v3, v0}, Lcom/xiaomi/push/service/g;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/a/e;)V

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v0}, Lcom/xiaomi/push/a/h;->cKp()Lcom/google/protobuf/micro/d;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/h;->cKn(Lcom/google/protobuf/micro/d;)Lcom/xiaomi/push/a/h;

    goto/16 :goto_2

    :cond_9
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBP()Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Recv SECMSG errCode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBX()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " errStr = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/XMPushService;->cPD()Lcom/xiaomi/push/service/r;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v1, v6, p1}, Lcom/xiaomi/push/service/r;->cNA(Lcom/xiaomi/push/service/XMPushService;Ljava/lang/String;Lcom/xiaomi/a/e;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCd()[B

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/a/i;->cKu([B)Lcom/xiaomi/push/a/i;

    move-result-object v7

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBO()Ljava/lang/String;

    move-result-object v8

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v6, v8}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v7}, Lcom/xiaomi/push/a/i;->cKC()Z

    move-result v1

    if-nez v1, :cond_d

    invoke-virtual {v7}, Lcom/xiaomi/push/a/i;->cKt()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v1, "auth"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string/jumbo v1, "cancel"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string/jumbo v1, "wait"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "SMACK: channel bind failed, chid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/xiaomi/push/a/i;->cKs()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    return-void

    :cond_d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "SMACK: channel bind succeeded, chid="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCe()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfT:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    move v3, v10

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/as;->cRg(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;IILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {v7}, Lcom/xiaomi/push/a/i;->cKs()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "invalid-sig"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    :goto_4
    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    const/4 v3, 0x5

    invoke-virtual {v7}, Lcom/xiaomi/push/a/i;->cKs()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/as;->cRg(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;IILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v6, v8}, Lcom/xiaomi/push/service/aR;->cSM(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_f
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "SMACK: bind error invalid-sig token = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v0, Lcom/xiaomi/push/service/as;->token:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " sec = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v0, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkC:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v1}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v1

    invoke-static {v10, v1, v2, v4, v10}, Lcom/xiaomi/e/d;->dgi(IIILjava/lang/String;I)V

    goto :goto_4

    :cond_10
    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    invoke-virtual {v7}, Lcom/xiaomi/push/a/i;->cKs()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/as;->cRg(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;IILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v6, v8}, Lcom/xiaomi/push/service/aR;->cSM(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_11
    iget-object v1, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/XMPushService;->cQd(Lcom/xiaomi/push/service/as;)V

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    invoke-virtual {v7}, Lcom/xiaomi/push/a/i;->cKs()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/as;->cRg(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;IILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_12
    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cCd()[B

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/a/a;->cIs([B)Lcom/xiaomi/push/a/a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/a/e;->cBO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/push/a/a;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/xiaomi/push/a/a;->cIk()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "kicked by server, chid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " res= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Lcom/xiaomi/push/service/as;->cRm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    const-string/jumbo v0, "wait"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    move-object v1, v6

    move v3, v7

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/XMPushService;->cPx(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v6, v2}, Lcom/xiaomi/push/service/aR;->cSM(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v0

    invoke-virtual {v0, v6, v2}, Lcom/xiaomi/push/service/aR;->cSG(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/xiaomi/push/service/aP;->div:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/XMPushService;->cQd(Lcom/xiaomi/push/service/as;)V

    sget-object v1, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    move v2, v7

    move v3, v10

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/as;->cRg(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;IILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
