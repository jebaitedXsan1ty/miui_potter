.class public Lcom/xiaomi/push/service/R;
.super Ljava/lang/Object;
.source "PushProvision.java"


# static fields
.field private static dfO:Lcom/xiaomi/push/service/R;


# instance fields
.field private dfN:I

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/push/service/R;->dfN:I

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/service/R;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/R;
    .locals 1

    sget-object v0, Lcom/xiaomi/push/service/R;->dfO:Lcom/xiaomi/push/service/R;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/push/service/R;->dfO:Lcom/xiaomi/push/service/R;

    return-object v0

    :cond_0
    new-instance v0, Lcom/xiaomi/push/service/R;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/R;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/push/service/R;->dfO:Lcom/xiaomi/push/service/R;

    goto :goto_0
.end method


# virtual methods
.method public cOt()I
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/xiaomi/push/service/R;->dfN:I

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/push/service/R;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "device_provisioned"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/push/service/R;->dfN:I

    iget v0, p0, Lcom/xiaomi/push/service/R;->dfN:I

    return v0

    :cond_0
    iget v0, p0, Lcom/xiaomi/push/service/R;->dfN:I

    return v0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/push/service/R;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "device_provisioned"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/push/service/R;->dfN:I

    iget v0, p0, Lcom/xiaomi/push/service/R;->dfN:I

    return v0
.end method

.method public cOu()Landroid/net/Uri;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    const-string/jumbo v0, "device_provisioned"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "device_provisioned"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public cOv()Z
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v2, "xmsf"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v2, "xiaomi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v2, "miui"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0
.end method
