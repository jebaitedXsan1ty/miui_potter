.class final Lcom/xiaomi/push/service/aF;
.super Ljava/lang/Object;
.source "MIPushHelper.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cSb(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->packageName:Ljava/lang/String;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->metaInfo:Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    const-string/jumbo v1, "ext_traffic_source_pkg"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    return-object v0
.end method

.method static cSc(Lcom/xiaomi/push/service/XMPushService;[B)Lcom/xiaomi/a/e;
    .locals 2

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;-><init>()V

    :try_start_0
    invoke-static {v0, p1}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    invoke-static {p0}, Lcom/xiaomi/push/service/ac;->cPc(Landroid/content/Context;)Lcom/xiaomi/push/service/ay;

    move-result-object v1

    invoke-static {v1, p0, v0}, Lcom/xiaomi/push/service/aF;->cSg(Lcom/xiaomi/push/service/ay;Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Lcom/xiaomi/a/e;
    :try_end_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method static cSd(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cQs()Lcom/xiaomi/smack/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "try send msg while connection is null."

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/xiaomi/smack/e;->cxv()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "Don\'t support XMPP connection."

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/push/service/ac;->cPc(Landroid/content/Context;)Lcom/xiaomi/push/service/ay;

    move-result-object v1

    invoke-static {v1, p0, p1}, Lcom/xiaomi/push/service/aF;->cSg(Lcom/xiaomi/push/service/ay;Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Lcom/xiaomi/a/e;

    move-result-object v1

    if-nez v1, :cond_2

    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/e;->cxz(Lcom/xiaomi/a/e;)V

    goto :goto_0
.end method

.method static cSe(Ljava/lang/String;Ljava/lang/String;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;
    .locals 6

    invoke-static {p2}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;-><init>()V

    new-instance v2, Lcom/xiaomi/xmpush/thrift/Target;

    invoke-direct {v2}, Lcom/xiaomi/xmpush/thrift/Target;-><init>()V

    const-wide/16 v4, 0x5

    iput-wide v4, v2, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    const-string/jumbo v3, "fakeid"

    iput-object v3, v2, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->dec(Lcom/xiaomi/xmpush/thrift/Target;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddR(Ljava/nio/ByteBuffer;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-virtual {v1, p3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddD(Lcom/xiaomi/xmpush/thrift/ActionType;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddU(Z)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-virtual {v1, p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddL(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddT(Z)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-virtual {v1, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->dea(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    return-object v1
.end method

.method static cSf(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/service/ac;->cPc(Landroid/content/Context;)Lcom/xiaomi/push/service/ay;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/push/service/ac;->cPc(Landroid/content/Context;)Lcom/xiaomi/push/service/ay;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/service/ay;->cRL(Lcom/xiaomi/push/service/XMPushService;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/xiaomi/push/service/aF;->cSi(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/aR;->cSN(Lcom/xiaomi/push/service/as;)V

    goto :goto_0
.end method

.method static cSg(Lcom/xiaomi/push/service/ay;Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Lcom/xiaomi/a/e;
    .locals 5

    :try_start_0
    new-instance v0, Lcom/xiaomi/a/e;

    invoke-direct {v0}, Lcom/xiaomi/a/e;-><init>()V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/xiaomi/a/e;->cBJ(I)V

    iget-object v1, p0, Lcom/xiaomi/push/service/ay;->dhP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/a/e;->cBW(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/xiaomi/push/service/aF;->cSb(Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/a/e;->cCb(Ljava/lang/String;)V

    const-string/jumbo v1, "SECMSG"

    const-string/jumbo v2, "message"

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/ay;->dhP:Ljava/lang/String;

    iget-object v2, p2, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->target:Lcom/xiaomi/xmpush/thrift/Target;

    const-string/jumbo v3, "@"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    iget-object v2, p2, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->target:Lcom/xiaomi/xmpush/thrift/Target;

    const-string/jumbo v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    invoke-static {p2}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/push/service/ay;->dhS:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/a/e;->cBN([BLjava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/a/e;->cBT(S)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "try send mi push message. packagename:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " action:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->action:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method static cSh(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;
    .locals 2

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>()V

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    const-string/jumbo v1, "package uninstalled"

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-static {}, Lcom/xiaomi/smack/packet/d;->cvL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dah(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daD(Z)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-static {p0, p1, v0, v1}, Lcom/xiaomi/push/service/aF;->cSe(Ljava/lang/String;Ljava/lang/String;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v0

    return-object v0
.end method

.method static cSi(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/xiaomi/push/service/as;->cRs(Landroid/os/Messenger;)V

    new-instance v0, Lcom/xiaomi/push/service/aa;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/aa;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-virtual {p1, v0}, Lcom/xiaomi/push/service/as;->cRq(Lcom/xiaomi/push/service/ae;)V

    return-void
.end method

.method static cSj(Lcom/xiaomi/push/service/XMPushService;Ljava/lang/String;[B)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/service/XMPushService;->cQs()Lcom/xiaomi/smack/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "try send msg while connection is null."

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/xiaomi/smack/e;->cxv()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Lcom/xiaomi/smack/XMPPException;

    const-string/jumbo v1, "Don\'t support XMPP connection."

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/XMPPException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p0, p2}, Lcom/xiaomi/push/service/aF;->cSc(Lcom/xiaomi/push/service/XMPushService;[B)Lcom/xiaomi/a/e;

    move-result-object v1

    if-nez v1, :cond_2

    const-string/jumbo v0, "not a valid message"

    const v1, 0x42c1d83

    invoke-static {p0, p1, p2, v1, v0}, Lcom/xiaomi/push/service/av;->cRE(Landroid/content/Context;Ljava/lang/String;[BILjava/lang/String;)V

    return-void

    :cond_2
    invoke-virtual {v0, v1}, Lcom/xiaomi/smack/e;->cxz(Lcom/xiaomi/a/e;)V

    return-void
.end method
