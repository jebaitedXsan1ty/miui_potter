.class public Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NetworkStatusReceiver.java"


# static fields
.field private static ddD:I

.field private static ddE:I

.field private static ddG:Ljava/util/concurrent/BlockingQueue;

.field private static ddH:Z

.field private static ddI:Ljava/util/concurrent/ThreadPoolExecutor;

.field private static ddJ:I


# instance fields
.field private ddF:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x1

    sput v0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddE:I

    sput v0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddD:I

    const/4 v0, 0x2

    sput v0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddJ:I

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddG:Ljava/util/concurrent/BlockingQueue;

    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v2, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddE:I

    sget v3, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddD:I

    sget v0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddJ:I

    int-to-long v4, v0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v7, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddG:Ljava/util/concurrent/BlockingQueue;

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    sput-object v1, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddI:Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddH:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddF:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddF:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddF:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddH:Z

    return-void
.end method

.method private cMs(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/z;->cFh()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    :goto_1
    invoke-static {p1}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_2
    return-void

    :cond_2
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDL()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDN()Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string/jumbo v2, "com.xiaomi.push.service.XMPushService"

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string/jumbo v1, "com.xiaomi.push.network_status_changed"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/z;->cFI()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/z;->cFB()V

    goto :goto_1

    :cond_4
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZw:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "syncing"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_3
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZx:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "syncing"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    :goto_4
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZB:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "syncing"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    :goto_5
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZy:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "syncing"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    :goto_6
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/m;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/m;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/mipush/sdk/RetryType;->cZA:Lcom/xiaomi/mipush/sdk/RetryType;

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/m;->cEb(Lcom/xiaomi/mipush/sdk/RetryType;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "syncing"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    :goto_7
    invoke-static {}, Lcom/xiaomi/mipush/sdk/I;->cHj()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_5
    :goto_8
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/N;->cHn(Landroid/content/Context;)V

    goto/16 :goto_2

    :cond_6
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/C;->cGN(Landroid/content/Context;)V

    goto :goto_3

    :cond_7
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/C;->cGu(Landroid/content/Context;)V

    goto :goto_4

    :cond_8
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/C;->cGv(Landroid/content/Context;)V

    goto :goto_5

    :cond_9
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/C;->cGs(Landroid/content/Context;)V

    goto :goto_6

    :cond_a
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/C;->cGw(Landroid/content/Context;)V

    goto :goto_7

    :cond_b
    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/I;->cHi(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/I;->cHg(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/I;->cHh(Landroid/content/Context;)V

    goto :goto_8
.end method

.method static synthetic cMt(Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->cMs(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    iget-boolean v0, p0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddF:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;->ddI:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/xiaomi/push/service/receivers/a;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/push/service/receivers/a;-><init>(Lcom/xiaomi/push/service/receivers/NetworkStatusReceiver;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    return-void
.end method
