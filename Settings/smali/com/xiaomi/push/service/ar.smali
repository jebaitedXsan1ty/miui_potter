.class final Lcom/xiaomi/push/service/ar;
.super Ljava/lang/Object;
.source "JobScheduler.java"


# instance fields
.field private dhk:I

.field private dhl:[Lcom/xiaomi/push/service/ag;

.field private dhm:I

.field private dhn:I


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x100

    iput v0, p0, Lcom/xiaomi/push/service/ar;->dhn:I

    iget v0, p0, Lcom/xiaomi/push/service/ar;->dhn:I

    new-array v0, v0, [Lcom/xiaomi/push/service/ag;

    iput-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    iput v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    iput v1, p0, Lcom/xiaomi/push/service/ar;->dhm:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/push/service/u;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/push/service/ar;-><init>()V

    return-void
.end method

.method private cQT()V
    .locals 7

    iget v0, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    add-int/lit8 v1, v0, -0x1

    add-int/lit8 v0, v1, -0x1

    div-int/lit8 v0, v0, 0x2

    :goto_0
    iget-object v2, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v2, v2, v1

    iget-wide v2, v2, Lcom/xiaomi/push/service/ag;->dgx:J

    iget-object v4, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v4, v4, v0

    iget-wide v4, v4, Lcom/xiaomi/push/service/ag;->dgx:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    iget-object v4, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v4, v4, v0

    aput-object v4, v3, v1

    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aput-object v2, v1, v0

    add-int/lit8 v1, v0, -0x1

    div-int/lit8 v1, v1, 0x2

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method

.method private cQY(Lcom/xiaomi/push/service/ag;)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v1, v1, v0

    if-eq v1, p1, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method static synthetic cRa(Lcom/xiaomi/push/service/ar;Lcom/xiaomi/push/service/ag;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/ar;->cQY(Lcom/xiaomi/push/service/ag;)I

    move-result v0

    return v0
.end method

.method private cRd(I)V
    .locals 8

    const/4 v2, 0x1

    const/4 v3, 0x0

    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    :goto_0
    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-lt v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-lez v1, :cond_0

    add-int/lit8 v1, v0, 0x1

    iget v4, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-lt v1, v4, :cond_3

    :cond_2
    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v0, v0, p1

    iget-wide v4, v0, Lcom/xiaomi/push/service/ag;->dgx:J

    iget-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v0, v0, v1

    iget-wide v6, v0, Lcom/xiaomi/push/service/ag;->dgx:J

    cmp-long v0, v4, v6

    if-ltz v0, :cond_5

    move v0, v2

    :goto_2
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v0, v0, p1

    iget-object v4, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    iget-object v5, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v5, v5, v1

    aput-object v5, v4, p1

    iget-object v4, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aput-object v0, v4, v1

    mul-int/lit8 v0, v1, 0x2

    add-int/lit8 v0, v0, 0x1

    move p1, v1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    add-int/lit8 v4, v0, 0x1

    aget-object v1, v1, v4

    iget-wide v4, v1, Lcom/xiaomi/push/service/ag;->dgx:J

    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v1, v1, v0

    iget-wide v6, v1, Lcom/xiaomi/push/service/ag;->dgx:J

    cmp-long v1, v4, v6

    if-ltz v1, :cond_4

    move v1, v2

    :goto_3
    if-nez v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_2
.end method


# virtual methods
.method public cQU(I)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/push/service/ar;->cQW()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/xiaomi/push/service/ag;->type:I

    if-eq v1, p1, :cond_1

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/xiaomi/push/service/ag;->cancel()Z

    goto :goto_1
.end method

.method public cQV(I)V
    .locals 3

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    iget v2, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    aget-object v1, v1, v2

    aput-object v1, v0, p1

    iget-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/ar;->cRd(I)V

    goto :goto_0
.end method

.method public cQW()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lcom/xiaomi/push/service/ag;->dgv:Z

    if-nez v1, :cond_1

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhm:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/xiaomi/push/service/ar;->dhm:I

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/ar;->cQV(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public cQX(ILcom/xiaomi/push/service/C;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/push/service/ar;->cQW()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/xiaomi/push/service/ag;->dgt:Lcom/xiaomi/push/service/C;

    if-eq v1, p2, :cond_1

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/xiaomi/push/service/ag;->cancel()Z

    goto :goto_1
.end method

.method public cQZ(I)Z
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-lt v0, v2, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/xiaomi/push/service/ag;->type:I

    if-eq v2, p1, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public cRb()Lcom/xiaomi/push/service/ag;
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public cRc(Lcom/xiaomi/push/service/ag;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    array-length v0, v0

    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-eq v0, v1, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    aput-object p1, v0, v1

    invoke-direct {p0}, Lcom/xiaomi/push/service/ar;->cQT()V

    return-void

    :cond_0
    iget v0, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Lcom/xiaomi/push/service/ag;

    iget-object v1, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    iget v2, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    goto :goto_0
.end method

.method public cRe()V
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/service/ar;->dhn:I

    new-array v0, v0, [Lcom/xiaomi/push/service/ag;

    iput-object v0, p0, Lcom/xiaomi/push/service/ar;->dhl:[Lcom/xiaomi/push/service/ag;

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    return-void
.end method

.method public isEmpty()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/xiaomi/push/service/ar;->dhk:I

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
