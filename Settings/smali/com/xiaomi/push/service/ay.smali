.class public Lcom/xiaomi/push/service/ay;
.super Ljava/lang/Object;
.source "MIPushAccount.java"


# instance fields
.field public final appId:Ljava/lang/String;

.field public final dhP:Ljava/lang/String;

.field public final dhQ:Ljava/lang/String;

.field public final dhR:I

.field public final dhS:Ljava/lang/String;

.field public final packageName:Ljava/lang/String;

.field public final token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/push/service/ay;->dhP:Ljava/lang/String;

    iput-object p2, p0, Lcom/xiaomi/push/service/ay;->token:Ljava/lang/String;

    iput-object p3, p0, Lcom/xiaomi/push/service/ay;->dhS:Ljava/lang/String;

    iput-object p4, p0, Lcom/xiaomi/push/service/ay;->appId:Ljava/lang/String;

    iput-object p5, p0, Lcom/xiaomi/push/service/ay;->dhQ:Ljava/lang/String;

    iput-object p6, p0, Lcom/xiaomi/push/service/ay;->packageName:Ljava/lang/String;

    iput p7, p0, Lcom/xiaomi/push/service/ay;->dhR:I

    return-void
.end method

.method public static cRM()Z
    .locals 2

    :try_start_0
    const-string/jumbo v0, "miui.os.Build"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "IS_ALPHA_BUILD"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    return v0
.end method

.method public static cRN(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.xiaomi.xmsf"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/xiaomi/push/service/ay;->cRM()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static cRO(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi.xmsf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public cRK(Lcom/xiaomi/push/service/as;Landroid/content/Context;Lcom/xiaomi/push/service/r;Ljava/lang/String;)Lcom/xiaomi/push/service/as;
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/xiaomi/push/service/as;->pkgName:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/push/service/ay;->dhP:Ljava/lang/String;

    iput-object v0, p1, Lcom/xiaomi/push/service/as;->userId:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/push/service/ay;->dhS:Ljava/lang/String;

    iput-object v0, p1, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    iget-object v0, p0, Lcom/xiaomi/push/service/ay;->token:Ljava/lang/String;

    iput-object v0, p1, Lcom/xiaomi/push/service/as;->token:Ljava/lang/String;

    const-string/jumbo v0, "5"

    iput-object v0, p1, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    const-string/jumbo v0, "XMPUSH-PASS"

    iput-object v0, p1, Lcom/xiaomi/push/service/as;->dhw:Ljava/lang/String;

    iput-boolean v5, p1, Lcom/xiaomi/push/service/as;->dhF:Z

    const-string/jumbo v0, ""

    invoke-static {p2}, Lcom/xiaomi/push/service/ay;->cRO(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "sdk_ver"

    aput-object v2, v1, v5

    const-string/jumbo v2, "%1$s:%2$s,%3$s:%4$s,%5$s:%6$s:%7$s:%8$s"

    const/16 v3, 0x24

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v6

    const-string/jumbo v3, "cpvn"

    aput-object v3, v1, v7

    const-string/jumbo v3, "3_6_3"

    aput-object v3, v1, v8

    const-string/jumbo v3, "cpvc"

    aput-object v3, v1, v9

    const/4 v3, 0x5

    const/16 v4, 0x778b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    const-string/jumbo v3, "aapn"

    const/4 v4, 0x6

    aput-object v3, v1, v4

    const/4 v3, 0x7

    aput-object v0, v1, v3

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/xiaomi/push/service/as;->dht:Ljava/lang/String;

    invoke-static {p2}, Lcom/xiaomi/push/service/ay;->cRO(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/push/service/ay;->appId:Ljava/lang/String;

    :goto_1
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "appid"

    aput-object v2, v1, v5

    aput-object v0, v1, v6

    const-string/jumbo v0, "locale"

    aput-object v0, v1, v7

    const-string/jumbo v0, "%1$s:%2$s,%3$s:%4$s,%5$s:%6$s,sync:1"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const-string/jumbo v2, "miid"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    invoke-static {p2}, Lcom/xiaomi/push/service/X;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/X;

    move-result-object v3

    invoke-virtual {v3}, Lcom/xiaomi/push/service/X;->cPa()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/xiaomi/push/service/as;->dhx:Ljava/lang/String;

    invoke-static {p2}, Lcom/xiaomi/push/service/ay;->cRN(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    iput-object p3, p1, Lcom/xiaomi/push/service/as;->dhD:Lcom/xiaomi/push/service/r;

    return-object p1

    :cond_0
    invoke-static {p2}, Lcom/xiaomi/channel/commonutils/android/b;->czM(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    const-string/jumbo v0, "1000271"

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/xiaomi/push/service/as;->dhx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    const-string/jumbo v2, "ab"

    aput-object v2, v1, v5

    aput-object p4, v1, v6

    const-string/jumbo v2, ",%1$s:%2$s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/xiaomi/push/service/as;->dhx:Ljava/lang/String;

    goto :goto_2
.end method

.method public cRL(Lcom/xiaomi/push/service/XMPushService;)Lcom/xiaomi/push/service/as;
    .locals 3

    new-instance v0, Lcom/xiaomi/push/service/as;

    invoke-direct {v0, p1}, Lcom/xiaomi/push/service/as;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-virtual {p1}, Lcom/xiaomi/push/service/XMPushService;->cPD()Lcom/xiaomi/push/service/r;

    move-result-object v1

    const-string/jumbo v2, "c"

    invoke-virtual {p0, v0, p1, v1, v2}, Lcom/xiaomi/push/service/ay;->cRK(Lcom/xiaomi/push/service/as;Landroid/content/Context;Lcom/xiaomi/push/service/r;Ljava/lang/String;)Lcom/xiaomi/push/service/as;

    return-object v0
.end method
