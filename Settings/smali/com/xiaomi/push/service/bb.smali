.class public Lcom/xiaomi/push/service/bb;
.super Lcom/xiaomi/push/service/f;
.source "PushHostManagerFactory.java"

# interfaces
.implements Lcom/xiaomi/c/h;


# instance fields
.field private djb:Lcom/xiaomi/push/service/XMPushService;

.field private djc:J


# direct methods
.method constructor <init>(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/push/service/f;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/push/service/bb;->djb:Lcom/xiaomi/push/service/XMPushService;

    return-void
.end method

.method public static cTl(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 7

    new-instance v0, Lcom/xiaomi/push/service/bb;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/bb;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    invoke-static {}, Lcom/xiaomi/push/service/an;->getInstance()Lcom/xiaomi/push/service/an;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/an;->cQE(Lcom/xiaomi/push/service/f;)V

    const-class v6, Lcom/xiaomi/c/g;

    const-class v1, Lcom/xiaomi/c/g;

    monitor-enter v1

    :try_start_0
    invoke-static {v0}, Lcom/xiaomi/c/g;->cVd(Lcom/xiaomi/c/h;)V

    new-instance v2, Lcom/xiaomi/push/service/ba;

    invoke-direct {v2}, Lcom/xiaomi/push/service/ba;-><init>()V

    const/4 v1, 0x0

    const-string/jumbo v3, "0"

    const-string/jumbo v4, "push"

    const-string/jumbo v5, "2.2"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/c/g;->cUW(Landroid/content/Context;Lcom/xiaomi/c/n;Lcom/xiaomi/c/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public cMJ(Lcom/xiaomi/push/a/f;)V
    .locals 0

    return-void
.end method

.method public cMK(Lcom/xiaomi/push/a/g;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/push/a/g;->cKd()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/xiaomi/push/a/g;->cKi()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/xiaomi/push/service/bb;->djc:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x36ee80

    cmp-long v0, v4, v6

    if-gtz v0, :cond_3

    move v0, v1

    :goto_1
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "fetch bucket :"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/a/g;->cKi()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/xiaomi/push/service/bb;->djc:J

    invoke-static {}, Lcom/xiaomi/c/g;->getInstance()Lcom/xiaomi/c/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/c/g;->clear()V

    invoke-virtual {v0}, Lcom/xiaomi/c/g;->cVg()V

    iget-object v3, p0, Lcom/xiaomi/push/service/bb;->djb:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v3}, Lcom/xiaomi/push/service/XMPushService;->cQs()Lcom/xiaomi/smack/e;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/xiaomi/smack/e;->getConfiguration()Lcom/xiaomi/smack/h;

    move-result-object v4

    invoke-virtual {v4}, Lcom/xiaomi/smack/h;->cxV()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/xiaomi/c/g;->cUV(Ljava/lang/String;)Lcom/xiaomi/c/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/c/e;->cUm()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "bucket changed, force reconnect"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/bb;->djb:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v2, v8}, Lcom/xiaomi/push/service/XMPushService;->cPK(ILjava/lang/Exception;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/bb;->djb:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/service/XMPushService;->cQf(Z)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3}, Lcom/xiaomi/smack/e;->cxk()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_2
.end method

.method public cTk(Landroid/content/Context;Lcom/xiaomi/c/n;Lcom/xiaomi/c/b;Ljava/lang/String;)Lcom/xiaomi/c/g;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/service/M;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/xiaomi/push/service/M;-><init>(Landroid/content/Context;Lcom/xiaomi/c/n;Lcom/xiaomi/c/b;Ljava/lang/String;)V

    return-object v0
.end method
