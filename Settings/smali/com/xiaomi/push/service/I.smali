.class Lcom/xiaomi/push/service/I;
.super Lcom/xiaomi/push/service/n;
.source "MIPushEventProcessor.java"


# instance fields
.field final synthetic dfA:Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

.field final synthetic dfz:Lcom/xiaomi/push/service/XMPushService;


# direct methods
.method constructor <init>(ILcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V
    .locals 0

    iput-object p2, p0, Lcom/xiaomi/push/service/I;->dfz:Lcom/xiaomi/push/service/XMPushService;

    iput-object p3, p0, Lcom/xiaomi/push/service/I;->dfA:Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/n;-><init>(I)V

    return-void
.end method


# virtual methods
.method public cxd()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "send ack message for unrecognized new miui message."

    return-object v0
.end method

.method public cxe()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/I;->dfz:Lcom/xiaomi/push/service/XMPushService;

    iget-object v1, p0, Lcom/xiaomi/push/service/I;->dfA:Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-static {v0, v1}, Lcom/xiaomi/push/service/T;->cOW(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v1

    const-string/jumbo v2, "miui_message_unrecognized"

    const-string/jumbo v3, "1"

    invoke-virtual {v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZv(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/I;->dfz:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v1, v0}, Lcom/xiaomi/push/service/aF;->cSd(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)V
    :try_end_0
    .catch Lcom/xiaomi/smack/XMPPException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/I;->dfz:Lcom/xiaomi/push/service/XMPushService;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, Lcom/xiaomi/push/service/XMPushService;->cPK(ILjava/lang/Exception;)V

    goto :goto_0
.end method
