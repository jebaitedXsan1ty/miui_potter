.class Lcom/xiaomi/push/service/af;
.super Ljava/lang/Object;
.source "PushClientsManager.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic dgq:Lcom/xiaomi/push/service/as;

.field final dgr:Lcom/xiaomi/push/service/as;

.field final dgs:Landroid/os/Messenger;


# direct methods
.method constructor <init>(Lcom/xiaomi/push/service/as;Lcom/xiaomi/push/service/as;Landroid/os/Messenger;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/service/af;->dgq:Lcom/xiaomi/push/service/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/xiaomi/push/service/af;->dgr:Lcom/xiaomi/push/service/as;

    iput-object p3, p0, Lcom/xiaomi/push/service/af;->dgs:Landroid/os/Messenger;

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "peer died, chid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/af;->dgr:Lcom/xiaomi/push/service/as;

    iget-object v1, v1, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czD(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/af;->dgq:Lcom/xiaomi/push/service/as;

    invoke-static {v0}, Lcom/xiaomi/push/service/as;->cRv(Lcom/xiaomi/push/service/as;)Lcom/xiaomi/push/service/XMPushService;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/push/service/aH;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/xiaomi/push/service/aH;-><init>(Lcom/xiaomi/push/service/af;I)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/xiaomi/push/service/XMPushService;->cPu(Lcom/xiaomi/push/service/n;J)V

    return-void
.end method
