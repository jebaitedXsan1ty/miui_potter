.class public Lcom/xiaomi/push/service/ah;
.super Lcom/xiaomi/push/service/n;
.source "MIPushAppRegisterJob.java"


# instance fields
.field private appId:Ljava/lang/String;

.field private dgy:Lcom/xiaomi/push/service/XMPushService;

.field private dgz:Ljava/lang/String;

.field private packageName:Ljava/lang/String;

.field private payload:[B


# direct methods
.method public constructor <init>(Lcom/xiaomi/push/service/XMPushService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 1

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/xiaomi/push/service/n;-><init>(I)V

    iput-object p1, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    iput-object p2, p0, Lcom/xiaomi/push/service/ah;->packageName:Ljava/lang/String;

    iput-object p5, p0, Lcom/xiaomi/push/service/ah;->payload:[B

    iput-object p3, p0, Lcom/xiaomi/push/service/ah;->appId:Ljava/lang/String;

    iput-object p4, p0, Lcom/xiaomi/push/service/ah;->dgz:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public cxd()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "register app"

    return-object v0
.end method

.method public cxe()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/push/service/ac;->cPc(Landroid/content/Context;)Lcom/xiaomi/push/service/ay;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v1

    const-string/jumbo v2, "5"

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/service/aR;->cSE(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/service/as;

    :goto_1
    iget-object v1, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v1}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cQf(Z)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    iget-object v2, p0, Lcom/xiaomi/push/service/ah;->packageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/push/service/ah;->appId:Ljava/lang/String;

    iget-object v4, p0, Lcom/xiaomi/push/service/ah;->dgz:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4}, Lcom/xiaomi/push/service/ac;->cPd(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/ay;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "no account for mipush"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    const-string/jumbo v1, "no account."

    const v2, 0x42c1d82

    invoke-static {v0, v2, v1}, Lcom/xiaomi/push/service/av;->cRC(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/ay;->cRL(Lcom/xiaomi/push/service/XMPushService;)Lcom/xiaomi/push/service/as;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v1, v0}, Lcom/xiaomi/push/service/aF;->cSi(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V

    invoke-static {}, Lcom/xiaomi/push/service/aR;->getInstance()Lcom/xiaomi/push/service/aR;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/aR;->cSN(Lcom/xiaomi/push/service/as;)V

    goto :goto_1

    :cond_4
    :try_start_1
    iget-object v1, v0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v2, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfT:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-eq v1, v2, :cond_5

    iget-object v1, v0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v2, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    new-instance v2, Lcom/xiaomi/push/service/t;

    iget-object v3, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v2, v3, v0}, Lcom/xiaomi/push/service/t;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/service/XMPushService;->cPA(Lcom/xiaomi/push/service/n;)V
    :try_end_1
    .catch Lcom/xiaomi/smack/XMPPException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, Lcom/xiaomi/push/service/XMPushService;->cPK(ILjava/lang/Exception;)V

    goto :goto_2

    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/xiaomi/push/service/ah;->dgy:Lcom/xiaomi/push/service/XMPushService;

    iget-object v1, p0, Lcom/xiaomi/push/service/ah;->packageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/push/service/ah;->payload:[B

    invoke-static {v0, v1, v2}, Lcom/xiaomi/push/service/aF;->cSj(Lcom/xiaomi/push/service/XMPushService;Ljava/lang/String;[B)V
    :try_end_2
    .catch Lcom/xiaomi/smack/XMPPException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2
.end method
