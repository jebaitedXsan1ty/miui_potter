.class final Lcom/xiaomi/push/service/aV;
.super Ljava/lang/Thread;
.source "JobScheduler.java"


# instance fields
.field private diM:Lcom/xiaomi/push/service/ar;

.field private volatile diN:J

.field private diO:J

.field private diP:Z

.field private volatile diQ:Z

.field private diR:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/push/service/aV;->diN:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/push/service/aV;->diQ:Z

    const-wide/16 v0, 0x32

    iput-wide v0, p0, Lcom/xiaomi/push/service/aV;->diO:J

    new-instance v0, Lcom/xiaomi/push/service/ar;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/xiaomi/push/service/ar;-><init>(Lcom/xiaomi/push/service/u;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/service/aV;->setName(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/xiaomi/push/service/aV;->setDaemon(Z)V

    invoke-virtual {p0}, Lcom/xiaomi/push/service/aV;->start()V

    return-void
.end method

.method private cTd(Lcom/xiaomi/push/service/ag;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/service/ar;->cRc(Lcom/xiaomi/push/service/ag;)V

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    return-void
.end method

.method static synthetic cTf(Lcom/xiaomi/push/service/aV;)Lcom/xiaomi/push/service/ar;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    return-object v0
.end method

.method static synthetic cTg(Lcom/xiaomi/push/service/aV;Lcom/xiaomi/push/service/ag;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/push/service/aV;->cTd(Lcom/xiaomi/push/service/ag;)V

    return-void
.end method

.method static synthetic cTh(Lcom/xiaomi/push/service/aV;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/push/service/aV;->diP:Z

    return p1
.end method

.method static synthetic cTi(Lcom/xiaomi/push/service/aV;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/service/aV;->diR:Z

    return v0
.end method


# virtual methods
.method public cTe()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/xiaomi/push/service/aV;->diQ:Z

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/push/service/aV;->diN:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_0

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public declared-synchronized cancel()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/xiaomi/push/service/aV;->diR:Z

    iget-object v0, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/ar;->cRe()V

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 14

    const-wide/16 v12, 0x32

    const-wide/16 v10, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    :goto_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/xiaomi/push/service/aV;->diR:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/ar;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/xiaomi/push/service/x;->cNM()J

    move-result-wide v0

    iget-object v3, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    invoke-virtual {v3}, Lcom/xiaomi/push/service/ar;->cRb()Lcom/xiaomi/push/service/ag;

    move-result-object v5

    iget-object v3, v5, Lcom/xiaomi/push/service/ag;->dgw:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v6, v5, Lcom/xiaomi/push/service/ag;->dgv:Z

    if-nez v6, :cond_5

    iget-wide v6, v5, Lcom/xiaomi/push/service/ag;->dgx:J

    sub-long v0, v6, v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    cmp-long v3, v0, v10

    if-gtz v3, :cond_6

    move v3, v4

    :goto_1
    if-nez v3, :cond_9

    :try_start_2
    iget-wide v6, p0, Lcom/xiaomi/push/service/aV;->diO:J

    cmp-long v3, v0, v6

    if-gtz v3, :cond_7

    move v3, v4

    :goto_2
    if-nez v3, :cond_0

    iget-wide v0, p0, Lcom/xiaomi/push/service/aV;->diO:J

    :cond_0
    iget-wide v6, p0, Lcom/xiaomi/push/service/aV;->diO:J

    add-long/2addr v6, v12

    iput-wide v6, p0, Lcom/xiaomi/push/service/aV;->diO:J

    iget-wide v6, p0, Lcom/xiaomi/push/service/aV;->diO:J

    const-wide/16 v8, 0x1f4

    cmp-long v3, v6, v8

    if-gtz v3, :cond_8

    move v3, v4

    :goto_3
    if-nez v3, :cond_1

    const-wide/16 v6, 0x1f4

    iput-wide v6, p0, Lcom/xiaomi/push/service/aV;->diO:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :try_start_3
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_4
    :try_start_4
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :cond_2
    :try_start_5
    monitor-exit p0

    return-void

    :cond_3
    iget-boolean v0, p0, Lcom/xiaomi/push/service/aV;->diP:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v0, :cond_4

    :try_start_6
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_5
    :try_start_7
    monitor-exit p0

    goto :goto_0

    :cond_4
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    return-void

    :catch_0
    move-exception v0

    goto :goto_5

    :cond_5
    :try_start_8
    iget-object v0, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/ar;->cQV(I)V

    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v0

    :cond_6
    move v3, v2

    goto :goto_1

    :cond_7
    move v3, v2

    goto :goto_2

    :cond_8
    move v3, v2

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_4

    :cond_9
    const-wide/16 v0, 0x32

    iput-wide v0, p0, Lcom/xiaomi/push/service/aV;->diO:J

    iget-object v1, v5, Lcom/xiaomi/push/service/ag;->dgw:Ljava/lang/Object;

    monitor-enter v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :try_start_c
    iget-object v0, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/ar;->cRb()Lcom/xiaomi/push/service/ag;

    move-result-object v0

    iget-wide v6, v0, Lcom/xiaomi/push/service/ag;->dgx:J

    iget-wide v8, v5, Lcom/xiaomi/push/service/ag;->dgx:J

    cmp-long v0, v6, v8

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    invoke-static {v0, v5}, Lcom/xiaomi/push/service/ar;->cRa(Lcom/xiaomi/push/service/ar;Lcom/xiaomi/push/service/ag;)I

    move-result v0

    :goto_6
    iget-boolean v3, v5, Lcom/xiaomi/push/service/ag;->dgv:Z

    if-nez v3, :cond_b

    iget-wide v6, v5, Lcom/xiaomi/push/service/ag;->dgx:J

    invoke-virtual {v5, v6, v7}, Lcom/xiaomi/push/service/ag;->cPo(J)V

    iget-object v3, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    invoke-virtual {v3, v0}, Lcom/xiaomi/push/service/ar;->cQV(I)V

    const-wide/16 v6, 0x0

    iput-wide v6, v5, Lcom/xiaomi/push/service/ag;->dgx:J

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :try_start_e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/push/service/aV;->diN:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/service/aV;->diQ:Z

    iget-object v0, v5, Lcom/xiaomi/push/service/ag;->dgt:Lcom/xiaomi/push/service/C;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/C;->run()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/push/service/aV;->diQ:Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-enter p0

    const/4 v1, 0x1

    :try_start_f
    iput-boolean v1, p0, Lcom/xiaomi/push/service/aV;->diR:Z

    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    throw v0

    :cond_a
    move v0, v2

    goto :goto_6

    :cond_b
    :try_start_10
    iget-object v0, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    iget-object v3, p0, Lcom/xiaomi/push/service/aV;->diM:Lcom/xiaomi/push/service/ar;

    invoke-static {v3, v5}, Lcom/xiaomi/push/service/ar;->cRa(Lcom/xiaomi/push/service/ar;Lcom/xiaomi/push/service/ag;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/xiaomi/push/service/ar;->cQV(I)V

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :try_start_11
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    :try_start_12
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :try_start_13
    throw v0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :catchall_4
    move-exception v0

    :try_start_14
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    throw v0
.end method
