.class Lcom/xiaomi/push/service/a/b;
.super Ljava/lang/Object;
.source "AlarmManagerTimer.java"

# interfaces
.implements Lcom/xiaomi/push/service/a/c;


# instance fields
.field private ddr:Landroid/app/PendingIntent;

.field private volatile dds:J

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/a/b;->ddr:Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/xiaomi/push/service/a/b;->mContext:Landroid/content/Context;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    iput-object p1, p0, Lcom/xiaomi/push/service/a/b;->mContext:Landroid/content/Context;

    return-void
.end method

.method private cMe(Landroid/app/AlarmManager;JLandroid/app/PendingIntent;)V
    .locals 4

    const-class v0, Landroid/app/AlarmManager;

    const/4 v1, 0x3

    :try_start_0
    new-array v1, v1, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-class v2, Landroid/app/PendingIntent;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string/jumbo v2, "setExact"

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x2

    aput-object p4, v1, v2

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public cMa()Z
    .locals 4

    iget-wide v0, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cMc(Z)V
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/xiaomi/smack/f;->cxQ()I

    move-result v1

    int-to-long v2, v1

    if-eqz p1, :cond_3

    :cond_0
    if-nez p1, :cond_4

    :goto_0
    if-eqz p1, :cond_5

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    rem-long/2addr v0, v2

    sub-long v0, v2, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    :cond_2
    :goto_1
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/xiaomi/push/service/b;->dej:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/push/service/a/b;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-wide v2, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/xiaomi/push/service/a/b;->cMd(Landroid/content/Intent;J)V

    return-void

    :cond_3
    iget-wide v4, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/push/service/a/b;->stop()V

    goto :goto_0

    :cond_5
    iget-wide v4, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    iget-wide v4, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    iget-wide v4, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-ltz v1, :cond_6

    const/4 v0, 0x1

    :cond_6
    if-nez v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    goto :goto_1
.end method

.method public cMd(Landroid/content/Intent;J)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/push/service/a/b;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/xiaomi/push/service/a/b;->mContext:Landroid/content/Context;

    invoke-static {v1, v3, p1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/xiaomi/push/service/a/b;->ddr:Landroid/app/PendingIntent;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-ge v1, v2, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v1, v2, :cond_1

    iget-object v1, p0, Lcom/xiaomi/push/service/a/b;->ddr:Landroid/app/PendingIntent;

    invoke-virtual {v0, v3, p2, p3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "register timer "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/xiaomi/push/service/a/b;->ddr:Landroid/app/PendingIntent;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-string/jumbo v2, "setExactAndAllowWhileIdle"

    invoke-static {v0, v2, v1}, Lcom/xiaomi/channel/commonutils/a/b;->cyz(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/push/service/a/b;->ddr:Landroid/app/PendingIntent;

    invoke-direct {p0, v0, p2, p3, v1}, Lcom/xiaomi/push/service/a/b;->cMe(Landroid/app/AlarmManager;JLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/xiaomi/push/service/a/b;->ddr:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    :goto_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/push/service/a/b;->dds:J

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/a/b;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/xiaomi/push/service/a/b;->ddr:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    iput-object v2, p0, Lcom/xiaomi/push/service/a/b;->ddr:Landroid/app/PendingIntent;

    const-string/jumbo v0, "unregister timer"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto :goto_0
.end method
