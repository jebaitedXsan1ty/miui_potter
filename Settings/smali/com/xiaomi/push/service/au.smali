.class public Lcom/xiaomi/push/service/au;
.super Ljava/lang/Object;
.source "OnlineConfigHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cRA(Ljava/util/List;Z)Ljava/util/List;
    .locals 8

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/h/c;->cAT(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    return-object v3

    :cond_1
    return-object v1

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->getKey()I

    move-result v5

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->getType()I

    move-result v2

    invoke-static {v2}, Lcom/xiaomi/xmpush/thrift/ConfigType;->dcg(I)Lcom/xiaomi/xmpush/thrift/ConfigType;

    move-result-object v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_4

    :cond_3
    sget-object v6, Lcom/xiaomi/push/service/l;->deO:[I

    invoke-virtual {v2}, Lcom/xiaomi/xmpush/thrift/ConfigType;->ordinal()I

    move-result v2

    aget v2, v6, v2

    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    :goto_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-boolean v6, v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->clear:Z

    if-eqz v6, :cond_3

    new-instance v0, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_0
    new-instance v2, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXI()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_1

    :pswitch_1
    new-instance v2, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXz()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v2, v5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_1

    :pswitch_2
    new-instance v2, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXG()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_1

    :pswitch_3
    new-instance v2, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXJ()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {v2, v5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static cRB(Lcom/xiaomi/xmpush/thrift/ConfigListType;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "oc_version_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/ConfigListType;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static cRw(Lcom/xiaomi/push/service/aU;Lcom/xiaomi/xmpush/thrift/ConfigListType;)I
    .locals 4

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/xiaomi/push/service/au;->cRB(Lcom/xiaomi/xmpush/thrift/ConfigListType;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/xiaomi/push/service/l;->deP:[I

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/ConfigListType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/xiaomi/push/service/aU;->diL:Landroid/content/SharedPreferences;

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static cRx(Lcom/xiaomi/push/service/aU;Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionNormalConfig;->dbu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/NormalConfig;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/NormalConfig;->cXn()I

    move-result v2

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/NormalConfig;->getType()Lcom/xiaomi/xmpush/thrift/ConfigListType;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/xiaomi/push/service/au;->cRw(Lcom/xiaomi/push/service/aU;Lcom/xiaomi/xmpush/thrift/ConfigListType;)I

    move-result v3

    if-le v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/NormalConfig;->getType()Lcom/xiaomi/xmpush/thrift/ConfigListType;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/NormalConfig;->cXn()I

    move-result v3

    invoke-static {p0, v2, v3}, Lcom/xiaomi/push/service/au;->cRz(Lcom/xiaomi/push/service/aU;Lcom/xiaomi/xmpush/thrift/ConfigListType;I)V

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/NormalConfig;->configItems:Ljava/util/List;

    invoke-static {v0, v4}, Lcom/xiaomi/push/service/au;->cRA(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/aU;->cTa(Ljava/util/List;)V

    goto :goto_0
.end method

.method public static cRy(Lcom/xiaomi/push/service/aU;Lcom/xiaomi/xmpush/thrift/XmPushActionCustomConfig;)V
    .locals 2

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCustomConfig;->dfb()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/xiaomi/push/service/au;->cRA(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/aU;->cSZ(Ljava/util/List;)V

    return-void
.end method

.method public static cRz(Lcom/xiaomi/push/service/aU;Lcom/xiaomi/xmpush/thrift/ConfigListType;I)V
    .locals 2

    invoke-static {p1}, Lcom/xiaomi/push/service/au;->cRB(Lcom/xiaomi/xmpush/thrift/ConfigListType;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/aU;->diL:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method
