.class public Lcom/xiaomi/push/service/c;
.super Ljava/lang/Object;
.source "RC4Cryption.java"


# static fields
.field private static dew:I


# instance fields
.field private deu:I

.field private dev:I

.field private dex:I

.field private dey:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    sput v0, Lcom/xiaomi/push/service/c;->dew:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, -0x29a

    iput v0, p0, Lcom/xiaomi/push/service/c;->dev:I

    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iput v1, p0, Lcom/xiaomi/push/service/c;->dex:I

    iput v1, p0, Lcom/xiaomi/push/service/c;->deu:I

    return-void
.end method

.method public static cMA([B[BZII)[B
    .locals 7

    const/4 v2, 0x0

    if-gez p3, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " len = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    array-length v0, p1

    if-gt p3, v0, :cond_0

    add-int v0, p3, p4

    array-length v1, p1

    if-gt v0, v1, :cond_0

    if-eqz p2, :cond_2

    move-object v0, p1

    move v1, p3

    :goto_0
    new-instance v3, Lcom/xiaomi/push/service/c;

    invoke-direct {v3}, Lcom/xiaomi/push/service/c;-><init>()V

    invoke-direct {v3, p0}, Lcom/xiaomi/push/service/c;->cMC([B)V

    invoke-direct {v3}, Lcom/xiaomi/push/service/c;->cMx()V

    :goto_1
    if-lt v2, p4, :cond_3

    return-object v0

    :cond_2
    new-array v0, p4, [B

    move v1, v2

    goto :goto_0

    :cond_3
    add-int v4, v1, v2

    add-int v5, p3, v2

    aget-byte v5, p1, v5

    invoke-virtual {v3}, Lcom/xiaomi/push/service/c;->cMw()B

    move-result v6

    xor-int/2addr v5, v6

    int-to-byte v5, v5

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static cMB([BLjava/lang/String;)[B
    .locals 1

    invoke-static {p1}, Lcom/xiaomi/channel/commonutils/c/a;->cyV(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {p0, v0}, Lcom/xiaomi/push/service/c;->cMF([B[B)[B

    move-result-object v0

    return-object v0
.end method

.method private cMC([B)V
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x100

    invoke-direct {p0, v1, p1, v0}, Lcom/xiaomi/push/service/c;->cMz(I[BZ)V

    return-void
.end method

.method public static cMD(B)I
    .locals 1

    if-gez p0, :cond_0

    add-int/lit16 v0, p0, 0x100

    return v0

    :cond_0
    return p0
.end method

.method private static cME([BII)V
    .locals 2

    aget-byte v0, p0, p1

    aget-byte v1, p0, p2

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    int-to-byte v0, v0

    aput-byte v0, p0, p2

    return-void
.end method

.method public static cMF([B[B)[B
    .locals 5

    array-length v0, p1

    new-array v1, v0, [B

    new-instance v2, Lcom/xiaomi/push/service/c;

    invoke-direct {v2}, Lcom/xiaomi/push/service/c;-><init>()V

    invoke-direct {v2, p0}, Lcom/xiaomi/push/service/c;->cMC([B)V

    invoke-direct {v2}, Lcom/xiaomi/push/service/c;->cMx()V

    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-lt v0, v3, :cond_0

    return-object v1

    :cond_0
    aget-byte v3, p1, v0

    invoke-virtual {v2}, Lcom/xiaomi/push/service/c;->cMw()B

    move-result v4

    xor-int/2addr v3, v4

    int-to-byte v3, v3

    int-to-byte v3, v3

    aput-byte v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private cMx()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/push/service/c;->dex:I

    iput v0, p0, Lcom/xiaomi/push/service/c;->deu:I

    return-void
.end method

.method public static cMy(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 6

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/c/a;->cyV(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v0, v2

    add-int/lit8 v0, v0, 0x1

    array-length v4, v3

    add-int/2addr v0, v4

    new-array v4, v0, [B

    move v0, v1

    :goto_0
    array-length v5, v2

    if-lt v0, v5, :cond_0

    array-length v0, v2

    const/16 v5, 0x5f

    aput-byte v5, v4, v0

    :goto_1
    array-length v0, v3

    if-lt v1, v0, :cond_1

    return-object v4

    :cond_0
    aget-byte v5, v2, v0

    int-to-byte v5, v5

    aput-byte v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    array-length v0, v2

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    aget-byte v5, v3, v1

    int-to-byte v5, v5

    aput-byte v5, v4, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private cMz(I[BZ)V
    .locals 6

    const/16 v5, 0x100

    const/4 v0, 0x0

    array-length v2, p2

    move v1, v0

    :goto_0
    if-lt v1, v5, :cond_0

    iput v0, p0, Lcom/xiaomi/push/service/c;->dex:I

    iput v0, p0, Lcom/xiaomi/push/service/c;->deu:I

    :goto_1
    iget v1, p0, Lcom/xiaomi/push/service/c;->deu:I

    if-lt v1, p1, :cond_1

    if-ne p1, v5, :cond_2

    :goto_2
    if-nez p3, :cond_3

    :goto_3
    return-void

    :cond_0
    iget-object v3, p0, Lcom/xiaomi/push/service/c;->dey:[B

    int-to-byte v4, v1

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/xiaomi/push/service/c;->dex:I

    iget-object v3, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iget v4, p0, Lcom/xiaomi/push/service/c;->deu:I

    aget-byte v3, v3, v4

    invoke-static {v3}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v3

    add-int/2addr v1, v3

    iget v3, p0, Lcom/xiaomi/push/service/c;->deu:I

    rem-int/2addr v3, v2

    aget-byte v3, p2, v3

    invoke-static {v3}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v3

    add-int/2addr v1, v3

    rem-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/xiaomi/push/service/c;->dex:I

    iget-object v1, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iget v3, p0, Lcom/xiaomi/push/service/c;->deu:I

    iget v4, p0, Lcom/xiaomi/push/service/c;->dex:I

    invoke-static {v1, v3, v4}, Lcom/xiaomi/push/service/c;->cME([BII)V

    iget v1, p0, Lcom/xiaomi/push/service/c;->deu:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/xiaomi/push/service/c;->deu:I

    goto :goto_1

    :cond_2
    iget v1, p0, Lcom/xiaomi/push/service/c;->dex:I

    iget-object v3, p0, Lcom/xiaomi/push/service/c;->dey:[B

    aget-byte v3, v3, p1

    invoke-static {v3}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v3

    add-int/2addr v1, v3

    rem-int v2, p1, v2

    aget-byte v2, p2, v2

    invoke-static {v2}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v2

    add-int/2addr v1, v2

    rem-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/xiaomi/push/service/c;->dev:I

    goto :goto_2

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "S_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    if-le v0, p1, :cond_4

    const-string/jumbo v0, "   j_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/xiaomi/push/service/c;->dex:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "   j_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/xiaomi/push/service/c;->dev:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "   S_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "[j_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "]="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iget v3, p0, Lcom/xiaomi/push/service/c;->dex:I

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "   S_"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "[j_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "]="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iget v3, p0, Lcom/xiaomi/push/service/c;->dev:I

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/push/service/c;->dey:[B

    const/4 v2, 0x1

    aget-byte v0, v0, v2

    if-nez v0, :cond_5

    :goto_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_4
    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/push/service/c;->dey:[B

    aget-byte v3, v3, v0

    invoke-static {v3}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_4

    :cond_5
    const-string/jumbo v0, "   S[1]!=0"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method


# virtual methods
.method cMw()B
    .locals 4

    iget v0, p0, Lcom/xiaomi/push/service/c;->deu:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/xiaomi/push/service/c;->deu:I

    iget v0, p0, Lcom/xiaomi/push/service/c;->dex:I

    iget-object v1, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iget v2, p0, Lcom/xiaomi/push/service/c;->deu:I

    aget-byte v1, v1, v2

    invoke-static {v1}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v1

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/xiaomi/push/service/c;->dex:I

    iget-object v0, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iget v1, p0, Lcom/xiaomi/push/service/c;->deu:I

    iget v2, p0, Lcom/xiaomi/push/service/c;->dex:I

    invoke-static {v0, v1, v2}, Lcom/xiaomi/push/service/c;->cME([BII)V

    iget-object v0, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iget-object v1, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iget v2, p0, Lcom/xiaomi/push/service/c;->deu:I

    aget-byte v1, v1, v2

    invoke-static {v1}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/push/service/c;->dey:[B

    iget v3, p0, Lcom/xiaomi/push/service/c;->dex:I

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/xiaomi/push/service/c;->cMD(B)I

    move-result v2

    add-int/2addr v1, v2

    rem-int/lit16 v1, v1, 0x100

    aget-byte v0, v0, v1

    return v0
.end method
