.class public Lcom/xiaomi/push/service/X;
.super Ljava/lang/Object;
.source "MIIDManager.java"


# static fields
.field private static volatile dgd:Lcom/xiaomi/push/service/X;


# instance fields
.field private dfZ:Ljava/lang/Object;

.field private dga:Landroid/accounts/OnAccountsUpdateListener;

.field private dgb:Landroid/accounts/AccountManager;

.field private dgc:Ljava/util/ArrayList;

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/X;->dfZ:Ljava/lang/Object;

    iput-object p1, p0, Lcom/xiaomi/push/service/X;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/push/service/X;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/android/f;->cAn(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/X;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/service/X;->dgb:Landroid/accounts/AccountManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/X;->dgc:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private cOZ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/X;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/android/f;->cAo(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/X;
    .locals 2

    sget-object v0, Lcom/xiaomi/push/service/X;->dgd:Lcom/xiaomi/push/service/X;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/push/service/X;->dgd:Lcom/xiaomi/push/service/X;

    return-object v0

    :cond_0
    const-class v1, Lcom/xiaomi/push/service/X;

    const-class v0, Lcom/xiaomi/push/service/X;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/push/service/X;->dgd:Lcom/xiaomi/push/service/X;

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/xiaomi/push/service/X;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/X;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/push/service/X;->dgd:Lcom/xiaomi/push/service/X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public cOY()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/service/X;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/android/f;->cAn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/service/X;->dga:Landroid/accounts/OnAccountsUpdateListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/push/service/X;->dgb:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/xiaomi/push/service/X;->dga:Landroid/accounts/OnAccountsUpdateListener;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    return-void

    :cond_0
    return-void

    :cond_1
    return-void
.end method

.method public cPa()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/push/service/X;->cOZ()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/service/X;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/push/service/aw;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aw;

    move-result-object v0

    const-string/jumbo v1, "0"

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/aw;->cRI(Ljava/lang/String;)V

    const-string/jumbo v0, "0"

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/push/service/X;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/push/service/aw;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aw;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/service/aw;->cRI(Ljava/lang/String;)V

    return-object v0
.end method

.method public cPb(Lcom/xiaomi/push/service/be;)V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/X;->dfZ:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/X;->dgc:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    if-nez p1, :cond_2

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    monitor-exit v1

    return-void

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/push/service/X;->dgc:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/push/service/X;->dgc:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/push/service/X;->cOY()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
