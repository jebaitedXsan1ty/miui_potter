.class Lcom/xiaomi/push/service/aS;
.super Lcom/xiaomi/push/service/n;
.source "LongConnUploader.java"


# instance fields
.field final synthetic diF:Lcom/xiaomi/push/service/aT;

.field final synthetic diG:Ljava/lang/String;

.field final synthetic diH:Ljava/util/List;

.field final synthetic diI:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/xiaomi/push/service/aT;ILjava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/service/aS;->diF:Lcom/xiaomi/push/service/aT;

    iput-object p3, p0, Lcom/xiaomi/push/service/aS;->diG:Ljava/lang/String;

    iput-object p4, p0, Lcom/xiaomi/push/service/aS;->diH:Ljava/util/List;

    iput-object p5, p0, Lcom/xiaomi/push/service/aS;->diI:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/xiaomi/push/service/n;-><init>(I)V

    return-void
.end method


# virtual methods
.method public cxd()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "Send tiny data."

    return-object v0
.end method

.method public cxe()V
    .locals 6

    iget-object v0, p0, Lcom/xiaomi/push/service/aS;->diF:Lcom/xiaomi/push/service/aT;

    iget-object v1, p0, Lcom/xiaomi/push/service/aS;->diG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/xiaomi/push/service/aT;->cSU(Lcom/xiaomi/push/service/aT;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/xiaomi/push/service/aS;->diH:Ljava/util/List;

    iget-object v2, p0, Lcom/xiaomi/push/service/aS;->diG:Ljava/lang/String;

    const v3, 0x8000

    invoke-static {v0, v2, v1, v3}, Lcom/xiaomi/push/service/y;->cNO(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "Get a null XmPushActionNotification list when TinyDataHelper.pack() in XMPushService."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/xiaomi/push/service/aS;->diH:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "TinyData uploaded by TinyDataUploader."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    const-string/jumbo v3, "uploadWay"

    const-string/jumbo v4, "longXMPushService"

    invoke-virtual {v0, v3, v4}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dao(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/xiaomi/push/service/aS;->diG:Ljava/lang/String;

    sget-object v4, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-static {v3, v1, v0, v4}, Lcom/xiaomi/push/service/aF;->cSe(Ljava/lang/String;Ljava/lang/String;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/push/service/aS;->diI:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    :goto_2
    invoke-static {v0}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/push/service/aS;->diF:Lcom/xiaomi/push/service/aT;

    invoke-static {v3}, Lcom/xiaomi/push/service/aT;->cSV(Lcom/xiaomi/push/service/aT;)Lcom/xiaomi/push/service/XMPushService;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/push/service/aS;->diG:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v0, v5}, Lcom/xiaomi/push/service/XMPushService;->cQk(Ljava/lang/String;[BZ)V

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/xiaomi/push/service/aS;->diG:Ljava/lang/String;

    iget-object v4, p0, Lcom/xiaomi/push/service/aS;->diI:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v3

    if-eqz v3, :cond_5

    :goto_3
    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddH()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/push/service/aS;->diI:Ljava/lang/String;

    const-string/jumbo v5, "ext_traffic_source_pkg"

    invoke-virtual {v3, v5, v4}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZC(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    new-instance v3, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    invoke-direct {v3}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;-><init>()V

    const-string/jumbo v4, "-1"

    invoke-virtual {v3, v4}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZp(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    invoke-virtual {v0, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->deb(Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    goto :goto_3
.end method
