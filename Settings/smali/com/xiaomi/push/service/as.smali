.class public Lcom/xiaomi/push/service/as;
.super Ljava/lang/Object;
.source "PushClientsManager.java"


# instance fields
.field private dhA:Landroid/os/Messenger;

.field private dhB:Lcom/xiaomi/push/service/D;

.field dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

.field public dhD:Lcom/xiaomi/push/service/r;

.field private dhE:I

.field public dhF:Z

.field dhG:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

.field private dho:Z

.field public dhp:Landroid/content/Context;

.field final dhq:Lcom/xiaomi/push/service/aO;

.field dhr:Landroid/os/IBinder$DeathRecipient;

.field private dhs:Ljava/util/List;

.field public dht:Ljava/lang/String;

.field public dhu:Ljava/lang/String;

.field public dhv:Ljava/lang/String;

.field public dhw:Ljava/lang/String;

.field public dhx:Ljava/lang/String;

.field public dhy:Ljava/lang/String;

.field private dhz:Lcom/xiaomi/push/service/XMPushService;

.field public pkgName:Ljava/lang/String;

.field public token:Ljava/lang/String;

.field public userId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput-object v0, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput v1, p0, Lcom/xiaomi/push/service/as;->dhE:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/as;->dhs:Ljava/util/List;

    iput-object v2, p0, Lcom/xiaomi/push/service/as;->dhG:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput-boolean v1, p0, Lcom/xiaomi/push/service/as;->dho:Z

    new-instance v0, Lcom/xiaomi/push/service/D;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/D;-><init>(Lcom/xiaomi/push/service/as;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/as;->dhB:Lcom/xiaomi/push/service/D;

    iput-object v2, p0, Lcom/xiaomi/push/service/as;->dhr:Landroid/os/IBinder$DeathRecipient;

    new-instance v0, Lcom/xiaomi/push/service/aO;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/aO;-><init>(Lcom/xiaomi/push/service/as;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/as;->dhq:Lcom/xiaomi/push/service/aO;

    return-void
.end method

.method public constructor <init>(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfS:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput-object v0, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput v1, p0, Lcom/xiaomi/push/service/as;->dhE:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/service/as;->dhs:Ljava/util/List;

    iput-object v2, p0, Lcom/xiaomi/push/service/as;->dhG:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput-boolean v1, p0, Lcom/xiaomi/push/service/as;->dho:Z

    new-instance v0, Lcom/xiaomi/push/service/D;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/D;-><init>(Lcom/xiaomi/push/service/as;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/as;->dhB:Lcom/xiaomi/push/service/D;

    iput-object v2, p0, Lcom/xiaomi/push/service/as;->dhr:Landroid/os/IBinder$DeathRecipient;

    new-instance v0, Lcom/xiaomi/push/service/aO;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/aO;-><init>(Lcom/xiaomi/push/service/as;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/as;->dhq:Lcom/xiaomi/push/service/aO;

    iput-object p1, p0, Lcom/xiaomi/push/service/as;->dhz:Lcom/xiaomi/push/service/XMPushService;

    new-instance v0, Lcom/xiaomi/push/service/V;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/service/V;-><init>(Lcom/xiaomi/push/service/as;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/service/as;->cRq(Lcom/xiaomi/push/service/ae;)V

    return-void
.end method

.method static synthetic cRf(Lcom/xiaomi/push/service/as;)Landroid/os/Messenger;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhA:Landroid/os/Messenger;

    return-object v0
.end method

.method private cRh(IILjava/lang/String;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhz:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v0

    return v0

    :pswitch_1
    const-string/jumbo v2, "wait"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v3, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfT:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/xiaomi/push/service/as;->dhz:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v2}, Lcom/xiaomi/push/service/XMPushService;->cPz()Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x15

    if-ne p2, v2, :cond_4

    :cond_1
    return v0

    :cond_2
    return v0

    :cond_3
    return v0

    :cond_4
    const/4 v2, 0x7

    if-eq p2, v2, :cond_5

    :goto_1
    return v1

    :cond_5
    const-string/jumbo v2, "wait"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private cRi(IILjava/lang/String;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhG:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-nez v0, :cond_1

    :cond_0
    return v3

    :cond_1
    iget-boolean v0, p0, Lcom/xiaomi/push/service/as;->dho:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhG:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhA:Landroid/os/Messenger;

    if-nez v0, :cond_4

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "peer died, ignore notify "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czD(Ljava/lang/String;)V

    return v2

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " status recovered, don\'t notify client:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czD(Ljava/lang/String;)V

    return v2

    :cond_4
    iget-boolean v0, p0, Lcom/xiaomi/push/service/as;->dho:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Peer alive notify status to client:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czD(Ljava/lang/String;)V

    return v3
.end method

.method public static cRm(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string/jumbo v0, ""

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :goto_0
    return-object v0

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic cRn(Lcom/xiaomi/push/service/as;)Lcom/xiaomi/push/service/D;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhB:Lcom/xiaomi/push/service/D;

    return-object v0
.end method

.method static synthetic cRo(Lcom/xiaomi/push/service/as;IILjava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/push/service/as;->cRi(IILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private cRp(IILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    iput-object v1, p0, Lcom/xiaomi/push/service/as;->dhG:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    if-eq p1, v3, :cond_2

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhD:Lcom/xiaomi/push/service/r;

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhp:Landroid/content/Context;

    invoke-virtual {v0, v1, p0, p2}, Lcom/xiaomi/push/service/r;->cNC(Landroid/content/Context;Lcom/xiaomi/push/service/as;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhD:Lcom/xiaomi/push/service/r;

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhp:Landroid/content/Context;

    invoke-virtual {v0, v1, p0, p4, p3}, Lcom/xiaomi/push/service/r;->cNx(Landroid/content/Context;Lcom/xiaomi/push/service/as;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    sget-object v2, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfT:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-eq v1, v2, :cond_3

    move v3, v0

    :cond_3
    if-eqz v3, :cond_5

    :cond_4
    if-nez v3, :cond_6

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhD:Lcom/xiaomi/push/service/r;

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhp:Landroid/content/Context;

    move-object v2, p0

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/xiaomi/push/service/r;->cNB(Landroid/content/Context;Lcom/xiaomi/push/service/as;ZILjava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string/jumbo v1, "wait"

    invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v0, p0, Lcom/xiaomi/push/service/as;->dhE:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/push/service/as;->dhE:I

    goto :goto_1

    :cond_6
    iput v0, p0, Lcom/xiaomi/push/service/as;->dhE:I

    goto :goto_1
.end method

.method static synthetic cRr(Lcom/xiaomi/push/service/as;IILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/xiaomi/push/service/as;->cRp(IILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic cRu(Lcom/xiaomi/push/service/as;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/service/as;->dhA:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic cRv(Lcom/xiaomi/push/service/as;)Lcom/xiaomi/push/service/XMPushService;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhz:Lcom/xiaomi/push/service/XMPushService;

    return-object v0
.end method


# virtual methods
.method public cRg(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;IILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/xiaomi/push/service/as;->dhs:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-ne v0, p1, :cond_3

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhD:Lcom/xiaomi/push/service/r;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;->dfV:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-eq p1, v0, :cond_5

    const/16 v0, 0x2774

    iget-object v2, p0, Lcom/xiaomi/push/service/as;->dhG:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    if-nez v2, :cond_6

    :cond_0
    move v0, v1

    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhz:Lcom/xiaomi/push/service/XMPushService;

    iget-object v2, p0, Lcom/xiaomi/push/service/as;->dhq:Lcom/xiaomi/push/service/aO;

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/service/XMPushService;->cQm(Lcom/xiaomi/push/service/n;)V

    invoke-direct {p0, p2, p3, p5}, Lcom/xiaomi/push/service/as;->cRh(IILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhz:Lcom/xiaomi/push/service/XMPushService;

    iget-object v2, p0, Lcom/xiaomi/push/service/as;->dhq:Lcom/xiaomi/push/service/aO;

    invoke-virtual {v2, p2, p3, p4, p5}, Lcom/xiaomi/push/service/aO;->cSx(IILjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/push/service/n;

    move-result-object v2

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Lcom/xiaomi/push/service/XMPushService;->cPu(Lcom/xiaomi/push/service/n;J)V

    :goto_3
    return-void

    :cond_2
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/service/ae;

    iget-object v4, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    invoke-interface {v0, v4, p1, p3}, Lcom/xiaomi/push/service/ae;->cOX(Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    aput-object v2, v0, v1

    const/4 v2, 0x1

    aput-object p1, v0, v2

    const-string/jumbo v2, "update the client %7$s status. %1$s->%2$s %3$s %4$s %5$s %6$s"

    const/4 v3, 0x2

    invoke-virtual {p0, p2}, Lcom/xiaomi/push/service/as;->cRt(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3

    invoke-static {p3}, Lcom/xiaomi/push/service/b;->cMv(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v0, v4

    const/4 v3, 0x4

    aput-object p4, v0, v3

    const/4 v3, 0x5

    aput-object p5, v0, v3

    iget-object v3, p0, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    const/4 v4, 0x6

    aput-object v3, v0, v4

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/xiaomi/push/service/as;->dhC:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "status changed while the client dispatcher is missing"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    return-void

    :cond_5
    return-void

    :cond_6
    iget-boolean v2, p0, Lcom/xiaomi/push/service/as;->dho:Z

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhA:Landroid/os/Messenger;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/xiaomi/push/service/as;->dho:Z

    if-eqz v1, :cond_1

    const/16 v0, 0x3e8

    goto :goto_2

    :cond_7
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/xiaomi/push/service/as;->cRp(IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method cRj()V
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhA:Landroid/os/Messenger;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/xiaomi/push/service/as;->dhG:Lcom/xiaomi/push/service/PushClientsManager$ClientStatus;

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhr:Landroid/os/IBinder$DeathRecipient;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhr:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public cRk(Lcom/xiaomi/push/service/ae;)V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhs:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cRl()J
    .locals 4

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    sub-double/2addr v0, v2

    double-to-long v0, v0

    iget v2, p0, Lcom/xiaomi/push/service/as;->dhE:I

    add-int/lit8 v2, v2, 0x1

    mul-int/lit8 v2, v2, 0xf

    int-to-long v2, v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public cRq(Lcom/xiaomi/push/service/ae;)V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhs:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method cRs(Landroid/os/Messenger;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/service/as;->cRj()V

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "peer linked with old sdk chid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czD(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/xiaomi/push/service/as;->dhA:Landroid/os/Messenger;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/service/as;->dho:Z

    new-instance v0, Lcom/xiaomi/push/service/af;

    invoke-direct {v0, p0, p0, p1}, Lcom/xiaomi/push/service/af;-><init>(Lcom/xiaomi/push/service/as;Lcom/xiaomi/push/service/as;Landroid/os/Messenger;)V

    iput-object v0, p0, Lcom/xiaomi/push/service/as;->dhr:Landroid/os/IBinder$DeathRecipient;

    invoke-virtual {p1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/push/service/as;->dhr:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "peer linkToDeath err: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czD(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/xiaomi/push/service/as;->dhA:Landroid/os/Messenger;

    iput-boolean v3, p0, Lcom/xiaomi/push/service/as;->dho:Z

    goto :goto_0
.end method

.method public cRt(I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const-string/jumbo v0, "unknown"

    return-object v0

    :pswitch_0
    const-string/jumbo v0, "OPEN"

    return-object v0

    :pswitch_1
    const-string/jumbo v0, "CLOSE"

    return-object v0

    :pswitch_2
    const-string/jumbo v0, "KICK"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
