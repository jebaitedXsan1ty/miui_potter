.class Lcom/xiaomi/push/service/aC;
.super Ljava/lang/Object;
.source "MIPushNotificationHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private dhY:Z

.field private dhZ:Ljava/lang/String;

.field private dia:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/xiaomi/push/service/aC;->dia:Landroid/content/Context;

    iput-object p1, p0, Lcom/xiaomi/push/service/aC;->dhZ:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/xiaomi/push/service/aC;->dhY:Z

    return-void
.end method


# virtual methods
.method public call()Landroid/graphics/Bitmap;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/push/service/aC;->dhZ:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "Failed get online picture/icon resource cause picUrl is empty"

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/push/service/aC;->dhZ:Ljava/lang/String;

    const-string/jumbo v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/xiaomi/push/service/aC;->dia:Landroid/content/Context;

    iget-object v1, p0, Lcom/xiaomi/push/service/aC;->dhZ:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/xiaomi/push/service/j;->cNq(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v1, "Failed get online picture/icon resource"

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/push/service/aC;->dia:Landroid/content/Context;

    iget-object v2, p0, Lcom/xiaomi/push/service/aC;->dhZ:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/xiaomi/push/service/aC;->dhY:Z

    invoke-static {v1, v2, v3}, Lcom/xiaomi/push/service/j;->cNr(Landroid/content/Context;Ljava/lang/String;Z)Lcom/xiaomi/push/service/at;

    move-result-object v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "Failed get online picture/icon resource"

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, v1, Lcom/xiaomi/push/service/at;->dhI:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/push/service/aC;->call()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
