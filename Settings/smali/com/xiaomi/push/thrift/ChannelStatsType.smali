.class public final enum Lcom/xiaomi/push/thrift/ChannelStatsType;
.super Ljava/lang/Enum;
.source "ChannelStatsType.java"


# static fields
.field public static final enum djG:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djH:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djI:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djK:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djL:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djM:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djN:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djO:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djP:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djQ:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djR:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djS:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djT:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djU:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djV:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djW:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djX:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djY:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum djZ:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkA:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkB:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkC:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkD:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkE:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkF:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkG:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkH:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkI:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dka:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkb:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field private static final synthetic dkc:[Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkd:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dke:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkf:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkg:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkh:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dki:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkj:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkk:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkl:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkm:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkn:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dko:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkp:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkq:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkr:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dks:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkt:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dku:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkv:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkw:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkx:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dky:Lcom/xiaomi/push/thrift/ChannelStatsType;

.field public static final enum dkz:Lcom/xiaomi/push/thrift/ChannelStatsType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "TCP_CONN_FAIL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkq:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "TCP_CONN_TIME"

    invoke-direct {v0, v1, v4, v5}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djR:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "PING_RTT"

    invoke-direct {v0, v1, v5, v6}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djQ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_CON_FAIL"

    invoke-direct {v0, v1, v6, v7}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkw:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_CON_OK"

    invoke-direct {v0, v1, v7, v8}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dke:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "ICMP_PING_FAIL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkE:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "ICMP_PING_OK"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkD:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_ONLINE_RATE"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djS:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_REQUEST_SUCCESS"

    const/16 v2, 0x8

    const/16 v3, 0x2710

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkg:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_TCP_NOACCESS"

    const/16 v2, 0x9

    const/16 v3, 0x2775

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djP:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_TCP_NETUNREACH"

    const/16 v2, 0xa

    const/16 v3, 0x2776

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djG:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_TCP_CONNREFUSED"

    const/16 v2, 0xb

    const/16 v3, 0x2777

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkF:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_TCP_NOROUTETOHOST"

    const/16 v2, 0xc

    const/16 v3, 0x2778

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkl:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_TCP_TIMEOUT"

    const/16 v2, 0xd

    const/16 v3, 0x2779

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djW:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_TCP_INVALARG"

    const/16 v2, 0xe

    const/16 v3, 0x277a

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkp:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_TCP_UKNOWNHOST"

    const/16 v2, 0xf

    const/16 v3, 0x277b

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkH:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_TCP_ERR_OTHER"

    const/16 v2, 0x10

    const/16 v3, 0x27d7

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dko:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "GSLB_ERR"

    const/16 v2, 0x11

    const/16 v3, 0x2af7

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djX:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_SUCCESS"

    const/16 v2, 0x12

    const/16 v3, 0x4e20

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djN:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_TCP_NOACCESS"

    const/16 v2, 0x13

    const/16 v3, 0x4e85

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkr:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_TCP_NETUNREACH"

    const/16 v2, 0x14

    const/16 v3, 0x4e86

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkB:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_TCP_CONNREFUSED"

    const/16 v2, 0x15

    const/16 v3, 0x4e87

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkm:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_TCP_NOROUTETOHOST"

    const/16 v2, 0x16

    const/16 v3, 0x4e88

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkz:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_TCP_TIMEOUT"

    const/16 v2, 0x17

    const/16 v3, 0x4e89

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkk:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_TCP_INVALARG"

    const/16 v2, 0x18

    const/16 v3, 0x4e8a

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dka:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_TCP_UKNOWNHOST"

    const/16 v2, 0x19

    const/16 v3, 0x4e8b

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djT:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_TCP_ERR_OTHER"

    const/16 v2, 0x1a

    const/16 v3, 0x4ee7

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djH:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_XMPP_ERR"

    const/16 v2, 0x1b

    const/16 v3, 0x4faf

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_BOSH_UNKNOWNHOST"

    const/16 v2, 0x1c

    const/16 v3, 0x4fb7

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CONN_BOSH_ERR"

    const/16 v2, 0x1d

    const/16 v3, 0x5013

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkx:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_SUCCESS"

    const/16 v2, 0x1e

    const/16 v3, 0x7530

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djM:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_TCP_READ_TIMEOUT_DEPRECTED"

    const/16 v2, 0x1f

    const/16 v3, 0x7595

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djU:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_TCP_CONNRESET_DEPRECTED"

    const/16 v2, 0x20

    const/16 v3, 0x7596

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkt:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_TCP_BROKEN_PIPE_DEPRECTED"

    const/16 v2, 0x21

    const/16 v3, 0x7597

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkv:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_TCP_READ_TIMEOUT"

    const/16 v2, 0x22

    const/16 v3, 0x759c

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dki:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_TCP_CONNRESET"

    const/16 v2, 0x23

    const/16 v3, 0x759d

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkG:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_TCP_BROKEN_PIPE"

    const/16 v2, 0x24

    const/16 v3, 0x759e

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkh:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_TCP_ERR"

    const/16 v2, 0x25

    const/16 v3, 0x75f7

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_XMPP_ERR"

    const/16 v2, 0x26

    const/16 v3, 0x76bf

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_BOSH_ITEM_NOT_FOUND"

    const/16 v2, 0x27

    const/16 v3, 0x76c1

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dku:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_BOSH_ERR"

    const/16 v2, 0x28

    const/16 v3, 0x7723

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dky:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_TIMEOUT"

    const/16 v2, 0x29

    const/16 v3, 0x7725

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djL:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "BIND_INVALID_SIG"

    const/16 v2, 0x2a

    const/16 v3, 0x7726

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkC:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_TCP_READTIMEOUT_DEPRECTED"

    const/16 v2, 0x2b

    const v3, 0x9ca5

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djO:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_TCP_CONNRESET_DEPRECTED"

    const/16 v2, 0x2c

    const v3, 0x9ca6

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djK:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_TCP_BROKEN_PIPE_DEPRECTED"

    const/16 v2, 0x2d

    const v3, 0x9ca7

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkn:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_TCP_READTIMEOUT"

    const/16 v2, 0x2e

    const v3, 0x9cac

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkb:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_TCP_CONNRESET"

    const/16 v2, 0x2f

    const v3, 0x9cad

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkd:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_TCP_BROKEN_PIPE"

    const/16 v2, 0x30

    const v3, 0x9cae

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dks:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_TCP_ERR"

    const/16 v2, 0x31

    const v3, 0x9d07

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_XMPPEXCEPTION"

    const/16 v2, 0x32

    const v3, 0x9dcf

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djZ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_BOSH_ITEMNOTFIND"

    const/16 v2, 0x33

    const v3, 0x9dd1

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djY:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_BOSH_EXCEPTION"

    const/16 v2, 0x34

    const v3, 0x9e33

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkA:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_TIMER_DELAYED"

    const/16 v2, 0x35

    const v3, 0xc351

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkj:Lcom/xiaomi/push/thrift/ChannelStatsType;

    new-instance v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    const-string/jumbo v1, "CHANNEL_STATS_COUNTER"

    const/16 v2, 0x36

    const/16 v3, 0x1f40

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/push/thrift/ChannelStatsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djV:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v0, 0x37

    new-array v0, v0, [Lcom/xiaomi/push/thrift/ChannelStatsType;

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkq:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djR:Lcom/xiaomi/push/thrift/ChannelStatsType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djQ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkw:Lcom/xiaomi/push/thrift/ChannelStatsType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dke:Lcom/xiaomi/push/thrift/ChannelStatsType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkE:Lcom/xiaomi/push/thrift/ChannelStatsType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkD:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djS:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkg:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djP:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djG:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkF:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkl:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djW:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkp:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkH:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dko:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djX:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djN:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkr:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkB:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkm:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkz:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkk:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dka:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djT:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djH:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkx:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djM:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djU:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkt:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkv:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dki:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkG:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkh:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dku:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dky:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djL:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkC:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djO:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djK:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkn:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkb:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkd:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dks:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djZ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djY:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkA:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkj:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djV:Lcom/xiaomi/push/thrift/ChannelStatsType;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkc:[Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/xiaomi/push/thrift/ChannelStatsType;->value:I

    return-void
.end method

.method public static cTZ(I)Lcom/xiaomi/push/thrift/ChannelStatsType;
    .locals 1

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :sswitch_0
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkq:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_1
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djR:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_2
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djQ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_3
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkw:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_4
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dke:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_5
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkE:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_6
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkD:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_7
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djS:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_8
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkg:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_9
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djP:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_a
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djG:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_b
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkF:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_c
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkl:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_d
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djW:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_e
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkp:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_f
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkH:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_10
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dko:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_11
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djX:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_12
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djN:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_13
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkr:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_14
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkB:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_15
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkm:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_16
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkz:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_17
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkk:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_18
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dka:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_19
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djT:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_1a
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djH:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_1b
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_1c
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_1d
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkx:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_1e
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djM:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_1f
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djU:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_20
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkt:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_21
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkv:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_22
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dki:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_23
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkG:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_24
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkh:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_25
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkI:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_26
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_27
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dku:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_28
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dky:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_29
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djL:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_2a
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkC:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_2b
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djO:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_2c
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djK:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_2d
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkn:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_2e
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkb:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_2f
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkd:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_30
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dks:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_31
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkJ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_32
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djZ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_33
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djY:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_34
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkA:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_35
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkj:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_36
    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djV:Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x1f40 -> :sswitch_36
        0x2710 -> :sswitch_8
        0x2775 -> :sswitch_9
        0x2776 -> :sswitch_a
        0x2777 -> :sswitch_b
        0x2778 -> :sswitch_c
        0x2779 -> :sswitch_d
        0x277a -> :sswitch_e
        0x277b -> :sswitch_f
        0x27d7 -> :sswitch_10
        0x2af7 -> :sswitch_11
        0x4e20 -> :sswitch_12
        0x4e85 -> :sswitch_13
        0x4e86 -> :sswitch_14
        0x4e87 -> :sswitch_15
        0x4e88 -> :sswitch_16
        0x4e89 -> :sswitch_17
        0x4e8a -> :sswitch_18
        0x4e8b -> :sswitch_19
        0x4ee7 -> :sswitch_1a
        0x4faf -> :sswitch_1b
        0x4fb7 -> :sswitch_1c
        0x5013 -> :sswitch_1d
        0x7530 -> :sswitch_1e
        0x7595 -> :sswitch_1f
        0x7596 -> :sswitch_20
        0x7597 -> :sswitch_21
        0x759c -> :sswitch_22
        0x759d -> :sswitch_23
        0x759e -> :sswitch_24
        0x75f7 -> :sswitch_25
        0x76bf -> :sswitch_26
        0x76c1 -> :sswitch_27
        0x7723 -> :sswitch_28
        0x7725 -> :sswitch_29
        0x7726 -> :sswitch_2a
        0x9ca5 -> :sswitch_2b
        0x9ca6 -> :sswitch_2c
        0x9ca7 -> :sswitch_2d
        0x9cac -> :sswitch_2e
        0x9cad -> :sswitch_2f
        0x9cae -> :sswitch_30
        0x9d07 -> :sswitch_31
        0x9dcf -> :sswitch_32
        0x9dd1 -> :sswitch_33
        0x9e33 -> :sswitch_34
        0xc351 -> :sswitch_35
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/push/thrift/ChannelStatsType;
    .locals 1

    const-class v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/push/thrift/ChannelStatsType;
    .locals 1

    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkc:[Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/push/thrift/ChannelStatsType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/thrift/ChannelStatsType;->value:I

    return v0
.end method
