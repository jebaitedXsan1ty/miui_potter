.class public Lcom/xiaomi/push/thrift/StatsEvent;
.super Ljava/lang/Object;
.source "StatsEvent.java"

# interfaces
.implements Lorg/apache/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final djA:Lorg/apache/thrift/protocol/i;

.field private static final djp:Lorg/apache/thrift/protocol/c;

.field private static final djq:Lorg/apache/thrift/protocol/i;

.field private static final djr:Lorg/apache/thrift/protocol/i;

.field private static final djs:Lorg/apache/thrift/protocol/i;

.field private static final djt:Lorg/apache/thrift/protocol/i;

.field private static final dju:Lorg/apache/thrift/protocol/i;

.field private static final djv:Lorg/apache/thrift/protocol/i;

.field private static final djw:Lorg/apache/thrift/protocol/i;

.field public static final djx:Ljava/util/Map;

.field private static final djy:Lorg/apache/thrift/protocol/i;

.field private static final djz:Lorg/apache/thrift/protocol/i;


# instance fields
.field private __isset_bit_vector:Ljava/util/BitSet;

.field public annotation:Ljava/lang/String;

.field public chid:B

.field public clientIp:I

.field public connpt:Ljava/lang/String;

.field public host:Ljava/lang/String;

.field public subvalue:I

.field public time:I

.field public type:I

.field public user:Ljava/lang/String;

.field public value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v4, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x2

    const/16 v6, 0xb

    const/16 v5, 0x8

    new-instance v0, Lorg/apache/thrift/protocol/c;

    const-string/jumbo v1, "StatsEvent"

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djp:Lorg/apache/thrift/protocol/c;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "chid"

    invoke-direct {v0, v1, v4, v8}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djr:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "type"

    invoke-direct {v0, v1, v5, v7}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djA:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "value"

    invoke-direct {v0, v1, v5, v4}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djs:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "connpt"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djv:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "host"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djy:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "subvalue"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djq:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "annotation"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djz:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "user"

    invoke-direct {v0, v1, v6, v5}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djw:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "time"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djt:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "clientIp"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->dju:Lorg/apache/thrift/protocol/i;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkM:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v4}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "chid"

    invoke-direct {v2, v4, v8, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkT:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "type"

    invoke-direct {v2, v4, v8, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkK:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "value"

    invoke-direct {v2, v4, v8, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkN:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "connpt"

    invoke-direct {v2, v4, v8, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkS:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "host"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkP:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "subvalue"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkL:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "annotation"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkO:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "user"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkR:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "time"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/push/thrift/StatsEvent$_Fields;->dkV:Lcom/xiaomi/push/thrift/StatsEvent$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "clientIp"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djx:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djx:Ljava/util/Map;

    const-class v1, Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-static {v1, v0}, Lorg/apache/thrift/meta_data/FieldMetaData;->esm(Ljava/lang/Class;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 1

    iput p1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTN(Z)V

    return-object p0
.end method

.method public cTB(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cTC()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cTD(I)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 1

    iput p1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->subvalue:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTO(Z)V

    return-object p0
.end method

.method public cTE()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cTF(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cTG(Lcom/xiaomi/push/thrift/StatsEvent;)I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTR()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTR()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTR()Z

    move-result v0

    if-nez v0, :cond_c

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTM()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTM()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTM()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTy()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTy()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTy()Z

    move-result v0

    if-nez v0, :cond_10

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTC()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTC()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTC()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTV()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTV()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_13

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTV()Z

    move-result v0

    if-nez v0, :cond_14

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTT()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTT()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTT()Z

    move-result v0

    if-nez v0, :cond_16

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTE()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTE()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_17

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTE()Z

    move-result v0

    if-nez v0, :cond_18

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTH()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTH()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_19

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTH()Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_7
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTK()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTK()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1b

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTK()Z

    move-result v0

    if-nez v0, :cond_1c

    :cond_8
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTJ()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTJ()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1d

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTJ()Z

    move-result v0

    if-nez v0, :cond_1e

    :cond_9
    return v2

    :cond_a
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_b
    return v0

    :cond_c
    iget-byte v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    iget-byte v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esB(BB)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_d
    return v0

    :cond_e
    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_f
    return v0

    :cond_10
    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    :cond_11
    return v0

    :cond_12
    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    :cond_13
    return v0

    :cond_14
    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    :cond_15
    return v0

    :cond_16
    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->subvalue:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->subvalue:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    :cond_17
    return v0

    :cond_18
    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    :cond_19
    return v0

    :cond_1a
    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    :cond_1b
    return v0

    :cond_1c
    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->time:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->time:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    :cond_1d
    return v0

    :cond_1e
    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->clientIp:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->clientIp:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_9

    return v0
.end method

.method public cTH()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cTI(I)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 1

    iput p1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->clientIp:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTF(Z)V

    return-object p0
.end method

.method public cTJ()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cTK()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cTL(I)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 1

    iput p1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->time:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTB(Z)V

    return-object p0
.end method

.method public cTM()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cTN(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cTO(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cTP(B)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 1

    int-to-byte v0, p1

    iput-byte v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTz(Z)V

    return-object p0
.end method

.method public cTQ(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    return-object p0
.end method

.method public cTR()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cTS()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'connpt\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cTT()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cTU(I)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 1

    iput p1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTx(Z)V

    return-object p0
.end method

.method public cTV()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cTW(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    return-object p0
.end method

.method public cTX(Lcom/xiaomi/push/thrift/StatsEvent;)Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    iget-byte v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    iget-byte v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    if-ne v0, v1, :cond_4

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTC()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTC()Z

    move-result v1

    if-eqz v0, :cond_6

    :cond_0
    if-nez v0, :cond_a

    :cond_1
    return v2

    :cond_2
    return v2

    :cond_3
    return v2

    :cond_4
    return v2

    :cond_5
    return v2

    :cond_6
    if-nez v1, :cond_0

    :cond_7
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTV()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTV()Z

    move-result v1

    if-eqz v0, :cond_b

    :cond_8
    if-nez v0, :cond_f

    :cond_9
    return v2

    :cond_a
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    return v2

    :cond_b
    if-nez v1, :cond_8

    :cond_c
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTT()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTT()Z

    move-result v1

    if-eqz v0, :cond_10

    :cond_d
    if-nez v0, :cond_14

    :cond_e
    return v2

    :cond_f
    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    return v2

    :cond_10
    if-nez v1, :cond_d

    :cond_11
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTE()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTE()Z

    move-result v1

    if-eqz v0, :cond_15

    :cond_12
    if-nez v0, :cond_19

    :cond_13
    return v2

    :cond_14
    if-eqz v1, :cond_e

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->subvalue:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->subvalue:I

    if-eq v0, v1, :cond_11

    return v2

    :cond_15
    if-nez v1, :cond_12

    :cond_16
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTH()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTH()Z

    move-result v1

    if-eqz v0, :cond_1a

    :cond_17
    if-nez v0, :cond_1e

    :cond_18
    return v2

    :cond_19
    if-eqz v1, :cond_13

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    return v2

    :cond_1a
    if-nez v1, :cond_17

    :cond_1b
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTK()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTK()Z

    move-result v1

    if-eqz v0, :cond_1f

    :cond_1c
    if-nez v0, :cond_23

    :cond_1d
    return v2

    :cond_1e
    if-eqz v1, :cond_18

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    return v2

    :cond_1f
    if-nez v1, :cond_1c

    :cond_20
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTJ()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTJ()Z

    move-result v1

    if-eqz v0, :cond_24

    :cond_21
    if-nez v0, :cond_26

    :cond_22
    return v2

    :cond_23
    if-eqz v1, :cond_1d

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->time:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->time:I

    if-eq v0, v1, :cond_20

    return v2

    :cond_24
    if-nez v1, :cond_21

    :cond_25
    return v3

    :cond_26
    if-eqz v1, :cond_22

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->clientIp:I

    iget v1, p1, Lcom/xiaomi/push/thrift/StatsEvent;->clientIp:I

    if-eq v0, v1, :cond_25

    return v2
.end method

.method public cTq(Lorg/apache/thrift/protocol/a;)V
    .locals 6

    const/16 v5, 0xb

    const/16 v4, 0x8

    const/4 v3, 0x1

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erS()Lorg/apache/thrift/protocol/c;

    :goto_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esa()Lorg/apache/thrift/protocol/i;

    move-result-object v0

    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eqz v1, :cond_a

    iget-short v1, v0, Lorg/apache/thrift/protocol/i;->eXX:S

    packed-switch v1, :pswitch_data_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    :goto_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erA()V

    goto :goto_0

    :pswitch_0
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esb()B

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    invoke-virtual {p0, v3}, Lcom/xiaomi/push/thrift/StatsEvent;->cTz(Z)V

    goto :goto_1

    :pswitch_1
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v4, :cond_1

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    invoke-virtual {p0, v3}, Lcom/xiaomi/push/thrift/StatsEvent;->cTN(Z)V

    goto :goto_1

    :pswitch_2
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v4, :cond_2

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    invoke-virtual {p0, v3}, Lcom/xiaomi/push/thrift/StatsEvent;->cTx(Z)V

    goto :goto_1

    :pswitch_3
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v5, :cond_3

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v5, :cond_4

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    goto :goto_1

    :pswitch_5
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v4, :cond_5

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->subvalue:I

    invoke-virtual {p0, v3}, Lcom/xiaomi/push/thrift/StatsEvent;->cTO(Z)V

    goto :goto_1

    :pswitch_6
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v5, :cond_6

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_7
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v5, :cond_7

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_8
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v4, :cond_8

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->time:I

    invoke-virtual {p0, v3}, Lcom/xiaomi/push/thrift/StatsEvent;->cTB(Z)V

    goto/16 :goto_1

    :pswitch_9
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v4, :cond_9

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->clientIp:I

    invoke-virtual {p0, v3}, Lcom/xiaomi/push/thrift/StatsEvent;->cTF(Z)V

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erU()V

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTR()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTM()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTy()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTS()V

    return-void

    :cond_b
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'chid\' was not found in serialized data! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'type\' was not found in serialized data! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'value\' was not found in serialized data! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public cTu(Lorg/apache/thrift/protocol/a;)V
    .locals 1

    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTS()V

    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djp:Lorg/apache/thrift/protocol/c;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erx(Lorg/apache/thrift/protocol/c;)V

    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djr:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-byte v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erO(B)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djA:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djs:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    if-nez v0, :cond_3

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    if-nez v0, :cond_4

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTT()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_2
    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    if-nez v0, :cond_6

    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    if-nez v0, :cond_7

    :cond_2
    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTK()Z

    move-result v0

    if-nez v0, :cond_8

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTJ()Z

    move-result v0

    if-nez v0, :cond_9

    :goto_6
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erQ()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erL()V

    return-void

    :cond_3
    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djv:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTV()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djy:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djq:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->subvalue:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTE()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djz:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTH()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djw:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_4

    :cond_8
    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->djt:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->time:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_5

    :cond_9
    sget-object v0, Lcom/xiaomi/push/thrift/StatsEvent;->dju:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->clientIp:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_6
.end method

.method public cTv(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    return-object p0
.end method

.method public cTw(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    return-object p0
.end method

.method public cTx(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cTy()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cTz(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/push/thrift/StatsEvent;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTG(Lcom/xiaomi/push/thrift/StatsEvent;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/xiaomi/push/thrift/StatsEvent;

    if-nez v0, :cond_1

    return v1

    :cond_0
    return v1

    :cond_1
    check-cast p1, Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTX(Lcom/xiaomi/push/thrift/StatsEvent;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "StatsEvent("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "chid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-byte v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "value:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "connpt:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->connpt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTV()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTT()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTE()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTH()Z

    move-result v1

    if-nez v1, :cond_6

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTK()Z

    move-result v1

    if-nez v1, :cond_8

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTJ()Z

    move-result v1

    if-nez v1, :cond_9

    :goto_6
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "host:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->host:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "subvalue:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->subvalue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "annotation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->annotation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_5
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_6
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "user:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->user:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_7
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    :cond_8
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "time:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->time:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    :cond_9
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "clientIp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/push/thrift/StatsEvent;->clientIp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_6
.end method
