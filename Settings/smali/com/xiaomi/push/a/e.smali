.class public final Lcom/xiaomi/push/a/e;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private dbj:Ljava/lang/String;

.field private dbk:Z

.field private dbl:Z

.field private dbm:Ljava/lang/String;

.field private dbn:I

.field private dbo:Z

.field private dbp:Ljava/lang/String;

.field private dbq:Z

.field private dbr:Z

.field private dbs:Ljava/lang/String;

.field private dbt:Ljava/lang/String;

.field private dbu:Ljava/lang/String;

.field private dbv:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/e;->dbt:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/e;->dbu:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/e;->dbp:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/e;->dbs:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/e;->dbm:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/e;->dbj:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/e;->dbn:I

    return-void
.end method


# virtual methods
.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/e;->dbn:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/e;->dbn:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/e;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/e;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/e;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/e;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/e;->cJB(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/e;->cJD(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/e;->cJF(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/e;->cJI(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/e;->cJH(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/e;->cJs(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJu()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJw()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJC()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJr()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJx()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJA()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_5
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJv()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJE()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJt()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJG()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJz()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJy()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_5
.end method

.method public cJA()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbq:Z

    return v0
.end method

.method public cJB(Ljava/lang/String;)Lcom/xiaomi/push/a/e;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbv:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/e;->dbt:Ljava/lang/String;

    return-object p0
.end method

.method public cJC()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbk:Z

    return v0
.end method

.method public cJD(Ljava/lang/String;)Lcom/xiaomi/push/a/e;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbo:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/e;->dbu:Ljava/lang/String;

    return-object p0
.end method

.method public cJE()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/e;->dbu:Ljava/lang/String;

    return-object v0
.end method

.method public cJF(Ljava/lang/String;)Lcom/xiaomi/push/a/e;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbk:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/e;->dbp:Ljava/lang/String;

    return-object p0
.end method

.method public cJG()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/e;->dbs:Ljava/lang/String;

    return-object v0
.end method

.method public cJH(Ljava/lang/String;)Lcom/xiaomi/push/a/e;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbl:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/e;->dbm:Ljava/lang/String;

    return-object p0
.end method

.method public cJI(Ljava/lang/String;)Lcom/xiaomi/push/a/e;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbr:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/e;->dbs:Ljava/lang/String;

    return-object p0
.end method

.method public cJr()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbr:Z

    return v0
.end method

.method public cJs(Ljava/lang/String;)Lcom/xiaomi/push/a/e;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbq:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/e;->dbj:Ljava/lang/String;

    return-object p0
.end method

.method public cJt()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/e;->dbp:Ljava/lang/String;

    return-object v0
.end method

.method public cJu()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbv:Z

    return v0
.end method

.method public cJv()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/e;->dbt:Ljava/lang/String;

    return-object v0
.end method

.method public cJw()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbo:Z

    return v0
.end method

.method public cJx()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/e;->dbl:Z

    return v0
.end method

.method public cJy()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/e;->dbj:Ljava/lang/String;

    return-object v0
.end method

.method public cJz()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/e;->dbm:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJu()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJw()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJC()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJr()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJx()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJA()Z

    move-result v1

    if-nez v1, :cond_5

    :goto_5
    iput v0, p0, Lcom/xiaomi/push/a/e;->dbn:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJv()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJE()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJt()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJG()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJz()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_4

    :cond_5
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/xiaomi/push/a/e;->cJy()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_5
.end method
