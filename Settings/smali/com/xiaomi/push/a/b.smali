.class public final Lcom/xiaomi/push/a/b;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private daA:I

.field private daB:Z

.field private daC:Z

.field private daD:I

.field private daE:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/push/a/b;->daD:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/b;->daE:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/b;->daA:I

    return-void
.end method

.method public static cIt([B)Lcom/xiaomi/push/a/b;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/b;

    invoke-direct {v0}, Lcom/xiaomi/push/a/b;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/b;->diC([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/a/b;

    check-cast v0, Lcom/xiaomi/push/a/b;

    return-object v0
.end method


# virtual methods
.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/b;->daA:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/b;->daA:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/b;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/b;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/b;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/b;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/b;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/b;->cIw(I)Lcom/xiaomi/push/a/b;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/b;->cIz(Ljava/lang/String;)Lcom/xiaomi/push/a/b;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/b;->cIu()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/b;->cIy()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/b;->cIx()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/b;->cIv()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public cIu()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/b;->daC:Z

    return v0
.end method

.method public cIv()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/b;->daE:Ljava/lang/String;

    return-object v0
.end method

.method public cIw(I)Lcom/xiaomi/push/a/b;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/b;->daC:Z

    iput p1, p0, Lcom/xiaomi/push/a/b;->daD:I

    return-object p0
.end method

.method public cIx()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/b;->daD:I

    return v0
.end method

.method public cIy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/b;->daB:Z

    return v0
.end method

.method public cIz(Ljava/lang/String;)Lcom/xiaomi/push/a/b;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/b;->daB:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/b;->daE:Ljava/lang/String;

    return-object p0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/b;->cIu()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/b;->cIy()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    iput v0, p0, Lcom/xiaomi/push/a/b;->daA:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/b;->cIx()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/b;->cIv()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1
.end method
