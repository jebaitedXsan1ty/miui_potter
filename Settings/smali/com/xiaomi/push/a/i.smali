.class public final Lcom/xiaomi/push/a/i;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private dbS:Ljava/lang/String;

.field private dbT:Z

.field private dbU:Z

.field private dbV:Ljava/lang/String;

.field private dbW:Ljava/lang/String;

.field private dbX:I

.field private dbY:Z

.field private dbZ:Z

.field private dca:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/push/a/i;->dbU:Z

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/i;->dbW:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/i;->dbS:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/i;->dbV:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/i;->dbX:I

    return-void
.end method

.method public static cKu([B)Lcom/xiaomi/push/a/i;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/i;

    invoke-direct {v0}, Lcom/xiaomi/push/a/i;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/i;->diC([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/a/i;

    check-cast v0, Lcom/xiaomi/push/a/i;

    return-object v0
.end method


# virtual methods
.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/i;->dbX:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/i;->dbX:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/i;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/i;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/i;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/i;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhp()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/i;->cKr(Z)Lcom/xiaomi/push/a/i;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/i;->cKy(Ljava/lang/String;)Lcom/xiaomi/push/a/i;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/i;->cKD(Ljava/lang/String;)Lcom/xiaomi/push/a/i;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/i;->cKv(Ljava/lang/String;)Lcom/xiaomi/push/a/i;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKw()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKz()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKB()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKx()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKC()Z

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhZ(IZ)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKt()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKs()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKA()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_3
.end method

.method public cKA()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/i;->dbV:Ljava/lang/String;

    return-object v0
.end method

.method public cKB()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/i;->dca:Z

    return v0
.end method

.method public cKC()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/i;->dbU:Z

    return v0
.end method

.method public cKD(Ljava/lang/String;)Lcom/xiaomi/push/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/i;->dca:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/i;->dbS:Ljava/lang/String;

    return-object p0
.end method

.method public cKr(Z)Lcom/xiaomi/push/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/i;->dbT:Z

    iput-boolean p1, p0, Lcom/xiaomi/push/a/i;->dbU:Z

    return-object p0
.end method

.method public cKs()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/i;->dbS:Ljava/lang/String;

    return-object v0
.end method

.method public cKt()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/i;->dbW:Ljava/lang/String;

    return-object v0
.end method

.method public cKv(Ljava/lang/String;)Lcom/xiaomi/push/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/i;->dbZ:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/i;->dbV:Ljava/lang/String;

    return-object p0
.end method

.method public cKw()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/i;->dbT:Z

    return v0
.end method

.method public cKx()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/i;->dbZ:Z

    return v0
.end method

.method public cKy(Ljava/lang/String;)Lcom/xiaomi/push/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/i;->dbY:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/i;->dbW:Ljava/lang/String;

    return-object p0
.end method

.method public cKz()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/i;->dbY:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKw()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKz()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKB()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKx()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_3
    iput v0, p0, Lcom/xiaomi/push/a/i;->dbX:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKC()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->dih(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKt()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKs()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/xiaomi/push/a/i;->cKA()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_3
.end method
