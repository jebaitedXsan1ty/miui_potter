.class public final Lcom/xiaomi/push/a/f;
.super Lcom/google/protobuf/micro/c;
.source "ChannelConfig.java"


# instance fields
.field private dbA:Z

.field private dbB:Z

.field private dbC:Z

.field private dbD:I

.field private dbE:Z

.field private dbF:I

.field private dbw:Z

.field private dbx:Ljava/util/List;

.field private dby:Z

.field private dbz:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    iput v0, p0, Lcom/xiaomi/push/a/f;->dbD:I

    iput-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbB:Z

    iput v0, p0, Lcom/xiaomi/push/a/f;->dbF:I

    iput-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbC:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/a/f;->dbx:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/f;->dbz:I

    return-void
.end method

.method public static cJO([B)Lcom/xiaomi/push/a/f;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/f;

    invoke-direct {v0}, Lcom/xiaomi/push/a/f;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/f;->diC([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/a/f;

    check-cast v0, Lcom/xiaomi/push/a/f;

    return-object v0
.end method

.method public static cJP(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/f;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/f;

    invoke-direct {v0}, Lcom/xiaomi/push/a/f;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/f;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/f;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/f;->dbz:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/f;->dbz:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/f;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/f;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/f;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/f;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhe()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/f;->cJW(I)Lcom/xiaomi/push/a/f;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhp()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/f;->cJU(Z)Lcom/xiaomi/push/a/f;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/f;->cJV(I)Lcom/xiaomi/push/a/f;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhp()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/f;->cJX(Z)Lcom/xiaomi/push/a/f;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/f;->cJN(Ljava/lang/String;)Lcom/xiaomi/push/a/f;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJR()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJJ()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJM()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJK()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJQ()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJL()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->diy(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJZ()Z

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhZ(IZ)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJT()I

    move-result v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJY()Z

    move-result v0

    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhZ(IZ)V

    goto :goto_3

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_4
.end method

.method public cJJ()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/f;->dby:Z

    return v0
.end method

.method public cJK()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbw:Z

    return v0
.end method

.method public cJL()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/f;->dbD:I

    return v0
.end method

.method public cJM()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbE:Z

    return v0
.end method

.method public cJN(Ljava/lang/String;)Lcom/xiaomi/push/a/f;
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/a/f;->dbx:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/a/f;->dbx:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/a/f;->dbx:Ljava/util/List;

    goto :goto_0
.end method

.method public cJQ()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/f;->dbx:Ljava/util/List;

    return-object v0
.end method

.method public cJR()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbA:Z

    return v0
.end method

.method public cJS()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/f;->dbx:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public cJT()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/f;->dbF:I

    return v0
.end method

.method public cJU(Z)Lcom/xiaomi/push/a/f;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/f;->dby:Z

    iput-boolean p1, p0, Lcom/xiaomi/push/a/f;->dbB:Z

    return-object p0
.end method

.method public cJV(I)Lcom/xiaomi/push/a/f;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbE:Z

    iput p1, p0, Lcom/xiaomi/push/a/f;->dbF:I

    return-object p0
.end method

.method public cJW(I)Lcom/xiaomi/push/a/f;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbA:Z

    iput p1, p0, Lcom/xiaomi/push/a/f;->dbD:I

    return-object p0
.end method

.method public cJX(Z)Lcom/xiaomi/push/a/f;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbw:Z

    iput-boolean p1, p0, Lcom/xiaomi/push/a/f;->dbC:Z

    return-object p0
.end method

.method public cJY()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbC:Z

    return v0
.end method

.method public cJZ()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/f;->dbB:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJR()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJJ()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJM()Z

    move-result v2

    if-nez v2, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJK()Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJQ()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    add-int v0, v2, v1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJQ()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/xiaomi/push/a/f;->dbz:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJL()I

    move-result v0

    invoke-static {v2, v0}, Lcom/google/protobuf/micro/b;->dia(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJZ()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/b;->dih(IZ)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJT()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/xiaomi/push/a/f;->cJY()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/micro/b;->dih(IZ)I

    move-result v2

    add-int/2addr v0, v2

    move v2, v0

    goto :goto_3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/micro/b;->dhN(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_4
.end method
