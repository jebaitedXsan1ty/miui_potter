.class public final Lcom/xiaomi/push/a/k;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private dcA:I

.field private dcB:Z

.field private dcC:Ljava/lang/String;

.field private dcD:J

.field private dcE:Z

.field private dcF:Ljava/lang/String;

.field private dcG:Z

.field private dcH:Z

.field private dcI:Z

.field private dcw:Z

.field private dcx:Z

.field private dcy:I

.field private dcz:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/k;->dcF:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/k;->dcC:Ljava/lang/String;

    iput-wide v2, p0, Lcom/xiaomi/push/a/k;->dcz:J

    iput-wide v2, p0, Lcom/xiaomi/push/a/k;->dcD:J

    iput-boolean v1, p0, Lcom/xiaomi/push/a/k;->dcE:Z

    iput v1, p0, Lcom/xiaomi/push/a/k;->dcy:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/k;->dcA:I

    return-void
.end method

.method public static cLr([B)Lcom/xiaomi/push/a/k;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/k;

    invoke-direct {v0}, Lcom/xiaomi/push/a/k;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/k;->diC([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/a/k;

    check-cast v0, Lcom/xiaomi/push/a/k;

    return-object v0
.end method


# virtual methods
.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/k;->dcA:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/k;->dcA:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/k;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/k;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/k;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/k;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/k;->cLw(Ljava/lang/String;)Lcom/xiaomi/push/a/k;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/k;->cLt(Ljava/lang/String;)Lcom/xiaomi/push/a/k;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhn()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/push/a/k;->cLl(J)Lcom/xiaomi/push/a/k;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhn()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/push/a/k;->cLk(J)Lcom/xiaomi/push/a/k;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhp()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/k;->cLz(Z)Lcom/xiaomi/push/a/k;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/k;->cLo(I)Lcom/xiaomi/push/a/k;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLs()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLm()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLh()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLy()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLq()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLp()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_5
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLj()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLn()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLu()J

    move-result-wide v0

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/micro/b;->dhM(IJ)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLi()J

    move-result-wide v0

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/micro/b;->dhM(IJ)V

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLv()Z

    move-result v0

    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhZ(IZ)V

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLx()I

    move-result v0

    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_5
.end method

.method public cLh()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcG:Z

    return v0
.end method

.method public cLi()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/push/a/k;->dcD:J

    return-wide v0
.end method

.method public cLj()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/k;->dcF:Ljava/lang/String;

    return-object v0
.end method

.method public cLk(J)Lcom/xiaomi/push/a/k;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcH:Z

    iput-wide p1, p0, Lcom/xiaomi/push/a/k;->dcD:J

    return-object p0
.end method

.method public cLl(J)Lcom/xiaomi/push/a/k;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcG:Z

    iput-wide p1, p0, Lcom/xiaomi/push/a/k;->dcz:J

    return-object p0
.end method

.method public cLm()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcI:Z

    return v0
.end method

.method public cLn()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/k;->dcC:Ljava/lang/String;

    return-object v0
.end method

.method public cLo(I)Lcom/xiaomi/push/a/k;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcx:Z

    iput p1, p0, Lcom/xiaomi/push/a/k;->dcy:I

    return-object p0
.end method

.method public cLp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcx:Z

    return v0
.end method

.method public cLq()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcw:Z

    return v0
.end method

.method public cLs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcB:Z

    return v0
.end method

.method public cLt(Ljava/lang/String;)Lcom/xiaomi/push/a/k;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcI:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/k;->dcC:Ljava/lang/String;

    return-object p0
.end method

.method public cLu()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/push/a/k;->dcz:J

    return-wide v0
.end method

.method public cLv()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcE:Z

    return v0
.end method

.method public cLw(Ljava/lang/String;)Lcom/xiaomi/push/a/k;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcB:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/k;->dcF:Ljava/lang/String;

    return-object p0
.end method

.method public cLx()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/k;->dcy:I

    return v0
.end method

.method public cLy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcH:Z

    return v0
.end method

.method public cLz(Z)Lcom/xiaomi/push/a/k;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/k;->dcw:Z

    iput-boolean p1, p0, Lcom/xiaomi/push/a/k;->dcE:Z

    return-object p0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLs()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLm()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLh()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLy()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLq()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLp()Z

    move-result v1

    if-nez v1, :cond_5

    :goto_5
    iput v0, p0, Lcom/xiaomi/push/a/k;->dcA:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLj()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLn()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLu()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/b;->dhY(IJ)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLi()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/b;->dhY(IJ)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLv()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dih(IZ)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_4

    :cond_5
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/xiaomi/push/a/k;->cLx()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_5
.end method
