.class public final Lcom/xiaomi/push/a/g;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private dbG:Z

.field private dbH:Z

.field private dbI:I

.field private dbJ:I

.field private dbK:Z

.field private dbL:Z

.field private dbM:Z

.field private dbN:I

.field private dbO:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    iput-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbG:Z

    iput v0, p0, Lcom/xiaomi/push/a/g;->dbO:I

    iput v0, p0, Lcom/xiaomi/push/a/g;->dbJ:I

    iput v0, p0, Lcom/xiaomi/push/a/g;->dbI:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/g;->dbN:I

    return-void
.end method

.method public static cKk([B)Lcom/xiaomi/push/a/g;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/g;

    invoke-direct {v0}, Lcom/xiaomi/push/a/g;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/g;->diC([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/a/g;

    check-cast v0, Lcom/xiaomi/push/a/g;

    return-object v0
.end method


# virtual methods
.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/g;->dbN:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/g;->dbN:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/g;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/g;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/g;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/g;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhp()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/g;->cKg(Z)Lcom/xiaomi/push/a/g;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/g;->cKc(I)Lcom/xiaomi/push/a/g;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/g;->cKm(I)Lcom/xiaomi/push/a/g;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/g;->cKf(I)Lcom/xiaomi/push/a/g;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKd()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKb()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKe()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKa()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKi()Z

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhZ(IZ)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKl()I

    move-result v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKh()I

    move-result v0

    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKj()I

    move-result v0

    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_3
.end method

.method public cKa()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbK:Z

    return v0
.end method

.method public cKb()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbM:Z

    return v0
.end method

.method public cKc(I)Lcom/xiaomi/push/a/g;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbM:Z

    iput p1, p0, Lcom/xiaomi/push/a/g;->dbO:I

    return-object p0
.end method

.method public cKd()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbL:Z

    return v0
.end method

.method public cKe()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbH:Z

    return v0
.end method

.method public cKf(I)Lcom/xiaomi/push/a/g;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbK:Z

    iput p1, p0, Lcom/xiaomi/push/a/g;->dbI:I

    return-object p0
.end method

.method public cKg(Z)Lcom/xiaomi/push/a/g;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbL:Z

    iput-boolean p1, p0, Lcom/xiaomi/push/a/g;->dbG:Z

    return-object p0
.end method

.method public cKh()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/g;->dbJ:I

    return v0
.end method

.method public cKi()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbG:Z

    return v0
.end method

.method public cKj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/g;->dbI:I

    return v0
.end method

.method public cKl()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/g;->dbO:I

    return v0
.end method

.method public cKm(I)Lcom/xiaomi/push/a/g;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/g;->dbH:Z

    iput p1, p0, Lcom/xiaomi/push/a/g;->dbJ:I

    return-object p0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKd()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKb()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKe()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKa()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_3
    iput v0, p0, Lcom/xiaomi/push/a/g;->dbN:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKi()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->dih(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKl()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKh()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/xiaomi/push/a/g;->cKj()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_3
.end method
