.class public final Lcom/xiaomi/push/a/a;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private dat:Z

.field private dau:Z

.field private dav:Z

.field private daw:I

.field private dax:Ljava/lang/String;

.field private day:Ljava/lang/String;

.field private daz:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/a;->day:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/a;->dax:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/a;->daz:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/a;->daw:I

    return-void
.end method

.method public static cIs([B)Lcom/xiaomi/push/a/a;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/a;

    invoke-direct {v0}, Lcom/xiaomi/push/a/a;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/a;->diC([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/a/a;

    check-cast v0, Lcom/xiaomi/push/a/a;

    return-object v0
.end method


# virtual methods
.method public cIh()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/a;->dav:Z

    return v0
.end method

.method public cIi()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/a;->dau:Z

    return v0
.end method

.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/a;->daw:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/a;->daw:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->getSerializedSize()I

    goto :goto_0
.end method

.method public cIk()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/a;->dax:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/a;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/a;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/a;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/a;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/a;->cIn(Ljava/lang/String;)Lcom/xiaomi/push/a/a;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/a;->cIr(Ljava/lang/String;)Lcom/xiaomi/push/a/a;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/a;->cIo(Ljava/lang/String;)Lcom/xiaomi/push/a/a;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIp()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIh()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIi()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->getType()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIk()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIq()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_2
.end method

.method public cIn(Ljava/lang/String;)Lcom/xiaomi/push/a/a;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/a;->dat:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/a;->day:Ljava/lang/String;

    return-object p0
.end method

.method public cIo(Ljava/lang/String;)Lcom/xiaomi/push/a/a;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/a;->dau:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/a;->daz:Ljava/lang/String;

    return-object p0
.end method

.method public cIp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/a;->dat:Z

    return v0
.end method

.method public cIq()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/a;->daz:Ljava/lang/String;

    return-object v0
.end method

.method public cIr(Ljava/lang/String;)Lcom/xiaomi/push/a/a;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/a;->dav:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/a;->dax:Ljava/lang/String;

    return-object p0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIp()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIh()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIi()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    iput v0, p0, Lcom/xiaomi/push/a/a;->daw:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIk()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/a;->cIq()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/a;->day:Ljava/lang/String;

    return-object v0
.end method
