.class public final Lcom/xiaomi/push/a/j;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private dcb:Z

.field private dcc:Z

.field private dcd:Z

.field private dce:Z

.field private dcf:Ljava/lang/String;

.field private dcg:Ljava/lang/String;

.field private dch:I

.field private dci:I

.field private dcj:Ljava/lang/String;

.field private dck:Z

.field private dcl:Ljava/lang/String;

.field private dcm:I

.field private dcn:I

.field private dco:Ljava/lang/String;

.field private dcp:Z

.field private dcq:Z

.field private dcr:Ljava/lang/String;

.field private dcs:Z

.field private dct:Z

.field private dcu:Z

.field private dcv:Lcom/xiaomi/push/a/g;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    iput v1, p0, Lcom/xiaomi/push/a/j;->dci:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/j;->dcg:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/j;->dco:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/j;->dcl:Ljava/lang/String;

    iput v1, p0, Lcom/xiaomi/push/a/j;->dcm:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/j;->dcj:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/j;->dcf:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/push/a/j;->dcr:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/push/a/j;->dcv:Lcom/xiaomi/push/a/g;

    iput v1, p0, Lcom/xiaomi/push/a/j;->dcn:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/j;->dch:I

    return-void
.end method


# virtual methods
.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/j;->dch:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/j;->dch:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/j;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/j;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/j;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/j;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhe()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->cKJ(I)Lcom/xiaomi/push/a/j;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->cKX(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->cKG(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->cLc(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->cLd(I)Lcom/xiaomi/push/a/j;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->cLb(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->cKS(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhs()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->setLocale(Ljava/lang/String;)Lcom/xiaomi/push/a/j;

    goto :goto_0

    :sswitch_9
    new-instance v0, Lcom/xiaomi/push/a/g;

    invoke-direct {v0}, Lcom/xiaomi/push/a/g;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/a;->dhh(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->cKQ(Lcom/xiaomi/push/a/g;)Lcom/xiaomi/push/a/j;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhv()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/j;->cLf(I)Lcom/xiaomi/push/a/j;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cLe()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKF()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKO()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKU()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKP()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKL()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKI()Z

    move-result v0

    if-nez v0, :cond_6

    :goto_6
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKN()Z

    move-result v0

    if-nez v0, :cond_7

    :goto_7
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKW()Z

    move-result v0

    if-nez v0, :cond_8

    :goto_8
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKM()Z

    move-result v0

    if-nez v0, :cond_9

    :goto_9
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKK()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->diy(II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKE()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKY()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKT()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKV()I

    move-result v0

    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cLa()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_5

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKH()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_6

    :cond_7
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKR()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dim(ILjava/lang/String;)V

    goto :goto_7

    :cond_8
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cLg()Lcom/xiaomi/push/a/g;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhP(ILcom/google/protobuf/micro/c;)V

    goto :goto_8

    :cond_9
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKZ()I

    move-result v0

    const/16 v1, 0xa

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dhR(II)V

    goto :goto_9
.end method

.method public cKE()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/j;->dcg:Ljava/lang/String;

    return-object v0
.end method

.method public cKF()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcs:Z

    return v0
.end method

.method public cKG(Ljava/lang/String;)Lcom/xiaomi/push/a/j;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcd:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/j;->dco:Ljava/lang/String;

    return-object p0
.end method

.method public cKH()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/j;->dcf:Ljava/lang/String;

    return-object v0
.end method

.method public cKI()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcq:Z

    return v0
.end method

.method public cKJ(I)Lcom/xiaomi/push/a/j;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dck:Z

    iput p1, p0, Lcom/xiaomi/push/a/j;->dci:I

    return-object p0
.end method

.method public cKK()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/j;->dci:I

    return v0
.end method

.method public cKL()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dce:Z

    return v0
.end method

.method public cKM()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcp:Z

    return v0
.end method

.method public cKN()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcb:Z

    return v0
.end method

.method public cKO()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcd:Z

    return v0
.end method

.method public cKP()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dct:Z

    return v0
.end method

.method public cKQ(Lcom/xiaomi/push/a/g;)Lcom/xiaomi/push/a/j;
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcc:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/j;->dcv:Lcom/xiaomi/push/a/g;

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
.end method

.method public cKR()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/j;->dcr:Ljava/lang/String;

    return-object v0
.end method

.method public cKS(Ljava/lang/String;)Lcom/xiaomi/push/a/j;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcq:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/j;->dcf:Ljava/lang/String;

    return-object p0
.end method

.method public cKT()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/j;->dcl:Ljava/lang/String;

    return-object v0
.end method

.method public cKU()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcu:Z

    return v0
.end method

.method public cKV()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/j;->dcm:I

    return v0
.end method

.method public cKW()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcc:Z

    return v0
.end method

.method public cKX(Ljava/lang/String;)Lcom/xiaomi/push/a/j;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcs:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/j;->dcg:Ljava/lang/String;

    return-object p0
.end method

.method public cKY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/j;->dco:Ljava/lang/String;

    return-object v0
.end method

.method public cKZ()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/j;->dcn:I

    return v0
.end method

.method public cLa()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/j;->dcj:Ljava/lang/String;

    return-object v0
.end method

.method public cLb(Ljava/lang/String;)Lcom/xiaomi/push/a/j;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dce:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/j;->dcj:Ljava/lang/String;

    return-object p0
.end method

.method public cLc(Ljava/lang/String;)Lcom/xiaomi/push/a/j;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcu:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/j;->dcl:Ljava/lang/String;

    return-object p0
.end method

.method public cLd(I)Lcom/xiaomi/push/a/j;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dct:Z

    iput p1, p0, Lcom/xiaomi/push/a/j;->dcm:I

    return-object p0
.end method

.method public cLe()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/j;->dck:Z

    return v0
.end method

.method public cLf(I)Lcom/xiaomi/push/a/j;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcp:Z

    iput p1, p0, Lcom/xiaomi/push/a/j;->dcn:I

    return-object p0
.end method

.method public cLg()Lcom/xiaomi/push/a/g;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/j;->dcv:Lcom/xiaomi/push/a/g;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cLe()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKF()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKO()Z

    move-result v1

    if-nez v1, :cond_2

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKU()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKP()Z

    move-result v1

    if-nez v1, :cond_4

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKL()Z

    move-result v1

    if-nez v1, :cond_5

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKI()Z

    move-result v1

    if-nez v1, :cond_6

    :goto_6
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKN()Z

    move-result v1

    if-nez v1, :cond_7

    :goto_7
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKW()Z

    move-result v1

    if-nez v1, :cond_8

    :goto_8
    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKM()Z

    move-result v1

    if-nez v1, :cond_9

    :goto_9
    iput v0, p0, Lcom/xiaomi/push/a/j;->dch:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKK()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->dia(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKE()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKY()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKT()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKV()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_4

    :cond_5
    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cLa()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_5

    :cond_6
    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKH()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_6

    :cond_7
    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKR()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->diq(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_7

    :cond_8
    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cLg()Lcom/xiaomi/push/a/g;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhT(ILcom/google/protobuf/micro/c;)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_8

    :cond_9
    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/xiaomi/push/a/j;->cKZ()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/b;->dhV(II)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_9
.end method

.method public setLocale(Ljava/lang/String;)Lcom/xiaomi/push/a/j;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/j;->dcb:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/j;->dcr:Ljava/lang/String;

    return-object p0
.end method
