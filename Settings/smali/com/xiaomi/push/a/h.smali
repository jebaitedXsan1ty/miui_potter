.class public final Lcom/xiaomi/push/a/h;
.super Lcom/google/protobuf/micro/c;
.source "ChannelMessage.java"


# instance fields
.field private dbP:Z

.field private dbQ:Lcom/google/protobuf/micro/d;

.field private dbR:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    sget-object v0, Lcom/google/protobuf/micro/d;->dCc:Lcom/google/protobuf/micro/d;

    iput-object v0, p0, Lcom/xiaomi/push/a/h;->dbQ:Lcom/google/protobuf/micro/d;

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/push/a/h;->dbR:I

    return-void
.end method

.method public static cKq([B)Lcom/xiaomi/push/a/h;
    .locals 1

    new-instance v0, Lcom/xiaomi/push/a/h;

    invoke-direct {v0}, Lcom/xiaomi/push/a/h;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/a/h;->diC([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/a/h;

    check-cast v0, Lcom/xiaomi/push/a/h;

    return-object v0
.end method


# virtual methods
.method public cIj()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/push/a/h;->dbR:I

    if-ltz v0, :cond_0

    :goto_0
    iget v0, p0, Lcom/xiaomi/push/a/h;->dbR:I

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/h;->getSerializedSize()I

    goto :goto_0
.end method

.method public bridge synthetic cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/xiaomi/push/a/h;->cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/h;

    move-result-object v0

    return-object v0
.end method

.method public cIl(Lcom/google/protobuf/micro/a;)Lcom/xiaomi/push/a/h;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/push/a/h;->diA(Lcom/google/protobuf/micro/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/a;->dhq()Lcom/google/protobuf/micro/d;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/xiaomi/push/a/h;->cKn(Lcom/google/protobuf/micro/d;)Lcom/xiaomi/push/a/h;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public cIm(Lcom/google/protobuf/micro/b;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/push/a/h;->cKo()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/push/a/h;->cKp()Lcom/google/protobuf/micro/d;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/micro/b;->dif(ILcom/google/protobuf/micro/d;)V

    goto :goto_0
.end method

.method public cKn(Lcom/google/protobuf/micro/d;)Lcom/xiaomi/push/a/h;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/a/h;->dbP:Z

    iput-object p1, p0, Lcom/xiaomi/push/a/h;->dbQ:Lcom/google/protobuf/micro/d;

    return-object p0
.end method

.method public cKo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/push/a/h;->dbP:Z

    return v0
.end method

.method public cKp()Lcom/google/protobuf/micro/d;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/a/h;->dbQ:Lcom/google/protobuf/micro/d;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/xiaomi/push/a/h;->cKo()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    iput v0, p0, Lcom/xiaomi/push/a/h;->dbR:I

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/push/a/h;->cKp()Lcom/google/protobuf/micro/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/b;->dhE(ILcom/google/protobuf/micro/d;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_0
.end method
