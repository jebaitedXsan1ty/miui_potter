.class Lcom/xiaomi/push/b/d;
.super Lcom/xiaomi/push/b/c;
.source "LogUploader.java"


# instance fields
.field final synthetic dcY:Z

.field final synthetic dcZ:Lcom/xiaomi/push/b/f;

.field final synthetic dda:Ljava/util/Date;

.field final synthetic ddb:Ljava/util/Date;

.field final synthetic ddc:Ljava/lang/String;

.field final synthetic ddd:Ljava/lang/String;

.field final synthetic dde:I

.field ddf:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/xiaomi/push/b/f;ILjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/b/d;->dcZ:Lcom/xiaomi/push/b/f;

    iput p2, p0, Lcom/xiaomi/push/b/d;->dde:I

    iput-object p3, p0, Lcom/xiaomi/push/b/d;->dda:Ljava/util/Date;

    iput-object p4, p0, Lcom/xiaomi/push/b/d;->ddb:Ljava/util/Date;

    iput-object p5, p0, Lcom/xiaomi/push/b/d;->ddd:Ljava/lang/String;

    iput-object p6, p0, Lcom/xiaomi/push/b/d;->ddc:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/xiaomi/push/b/d;->dcY:Z

    invoke-direct {p0, p1}, Lcom/xiaomi/push/b/c;-><init>(Lcom/xiaomi/push/b/f;)V

    return-void
.end method


# virtual methods
.method public cAS()V
    .locals 7

    iget-object v0, p0, Lcom/xiaomi/push/b/d;->ddf:Ljava/io/File;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/b/d;->dcZ:Lcom/xiaomi/push/b/f;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lcom/xiaomi/push/b/f;->cLY(Lcom/xiaomi/push/b/f;J)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/push/b/d;->ddf:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/b/d;->dcZ:Lcom/xiaomi/push/b/f;

    invoke-static {v0}, Lcom/xiaomi/push/b/f;->cLR(Lcom/xiaomi/push/b/f;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v6

    new-instance v0, Lcom/xiaomi/push/b/e;

    iget-object v1, p0, Lcom/xiaomi/push/b/d;->dcZ:Lcom/xiaomi/push/b/f;

    iget-object v2, p0, Lcom/xiaomi/push/b/d;->ddd:Ljava/lang/String;

    iget-object v3, p0, Lcom/xiaomi/push/b/d;->ddc:Ljava/lang/String;

    iget-object v4, p0, Lcom/xiaomi/push/b/d;->ddf:Ljava/io/File;

    iget-boolean v5, p0, Lcom/xiaomi/push/b/d;->dcY:Z

    invoke-direct/range {v0 .. v5}, Lcom/xiaomi/push/b/e;-><init>(Lcom/xiaomi/push/b/f;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Z)V

    invoke-virtual {v6, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public cwo()V
    .locals 5

    invoke-static {}, Lcom/xiaomi/channel/commonutils/b/c;->cyR()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/xiaomi/push/b/d;->dcZ:Lcom/xiaomi/push/b/f;

    invoke-static {v2}, Lcom/xiaomi/push/b/f;->cLV(Lcom/xiaomi/push/b/f;)Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "/.logcache"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    return-void

    :cond_0
    return-void

    :cond_1
    new-instance v1, Lcom/xiaomi/push/b/a;

    invoke-direct {v1}, Lcom/xiaomi/push/b/a;-><init>()V

    iget v2, p0, Lcom/xiaomi/push/b/d;->dde:I

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/b/a;->cLJ(I)V

    iget-object v2, p0, Lcom/xiaomi/push/b/d;->dcZ:Lcom/xiaomi/push/b/f;

    invoke-static {v2}, Lcom/xiaomi/push/b/f;->cLV(Lcom/xiaomi/push/b/f;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/xiaomi/push/b/d;->dda:Ljava/util/Date;

    iget-object v4, p0, Lcom/xiaomi/push/b/d;->ddb:Ljava/util/Date;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/xiaomi/push/b/a;->cLH(Landroid/content/Context;Ljava/util/Date;Ljava/util/Date;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/push/b/d;->ddf:Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
