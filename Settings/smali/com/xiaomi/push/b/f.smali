.class public Lcom/xiaomi/push/b/f;
.super Ljava/lang/Object;
.source "LogUploader.java"


# static fields
.field private static volatile ddl:Lcom/xiaomi/push/b/f;


# instance fields
.field private final ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/push/b/f;->ddl:Lcom/xiaomi/push/b/f;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iput-object p1, p0, Lcom/xiaomi/push/b/f;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lcom/xiaomi/push/b/b;

    invoke-direct {v1, p0}, Lcom/xiaomi/push/b/b;-><init>(Lcom/xiaomi/push/b/f;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/push/b/f;->cLT(J)V

    return-void
.end method

.method private cLQ(J)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/b/c;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/xiaomi/push/b/c;->cLN()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/push/b/f;->cLT(J)V

    goto :goto_0
.end method

.method static synthetic cLR(Lcom/xiaomi/push/b/f;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic cLS(Lcom/xiaomi/push/b/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/push/b/f;->cLZ()V

    return-void
.end method

.method private cLT(J)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/xiaomi/push/b/g;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/b/g;-><init>(Lcom/xiaomi/push/b/f;)V

    invoke-static {v0, p1, p2}, Lcom/xiaomi/smack/c/b;->cwr(Lcom/xiaomi/channel/commonutils/h/b;J)V

    goto :goto_0
.end method

.method static synthetic cLV(Lcom/xiaomi/push/b/f;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/push/b/f;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private cLX()V
    .locals 3

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/push/b/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/xiaomi/push/b/c;->cLO()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const-string/jumbo v1, "remove Expired task"

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    const/4 v2, 0x6

    if-gt v1, v2, :cond_2

    goto :goto_1
.end method

.method static synthetic cLY(Lcom/xiaomi/push/b/f;J)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/push/b/f;->cLQ(J)V

    return-void
.end method

.method private cLZ()V
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/xiaomi/channel/commonutils/b/c;->cyU()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Lcom/xiaomi/channel/commonutils/b/c;->cyT()Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/xiaomi/push/b/f;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/.logcache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/push/b/f;
    .locals 2

    sget-object v0, Lcom/xiaomi/push/b/f;->ddl:Lcom/xiaomi/push/b/f;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/push/b/f;->ddl:Lcom/xiaomi/push/b/f;

    iput-object p0, v0, Lcom/xiaomi/push/b/f;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/xiaomi/push/b/f;->ddl:Lcom/xiaomi/push/b/f;

    return-object v0

    :cond_0
    const-class v1, Lcom/xiaomi/push/b/f;

    const-class v0, Lcom/xiaomi/push/b/f;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/push/b/f;->ddl:Lcom/xiaomi/push/b/f;

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/xiaomi/push/b/f;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/b/f;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/push/b/f;->ddl:Lcom/xiaomi/push/b/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public cLU()V
    .locals 2

    invoke-direct {p0}, Lcom/xiaomi/push/b/f;->cLX()V

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/push/b/f;->cLQ(J)V

    return-void
.end method

.method public cLW(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;IZ)V
    .locals 9

    iget-object v8, p0, Lcom/xiaomi/push/b/f;->ddm:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lcom/xiaomi/push/b/d;

    move-object v1, p0

    move v2, p5

    move-object v3, p3

    move-object v4, p4

    move-object v5, p1

    move-object v6, p2

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/xiaomi/push/b/d;-><init>(Lcom/xiaomi/push/b/f;ILjava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v8, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/push/b/f;->cLT(J)V

    return-void
.end method
