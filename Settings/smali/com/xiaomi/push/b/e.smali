.class Lcom/xiaomi/push/b/e;
.super Lcom/xiaomi/push/b/c;
.source "LogUploader.java"


# instance fields
.field ddg:Z

.field final synthetic ddh:Lcom/xiaomi/push/b/f;

.field ddi:I

.field ddj:Ljava/io/File;

.field ddk:Z

.field token:Ljava/lang/String;

.field url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/xiaomi/push/b/f;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Z)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/push/b/e;->ddh:Lcom/xiaomi/push/b/f;

    invoke-direct {p0, p1}, Lcom/xiaomi/push/b/c;-><init>(Lcom/xiaomi/push/b/f;)V

    iput-object p2, p0, Lcom/xiaomi/push/b/e;->url:Ljava/lang/String;

    iput-object p3, p0, Lcom/xiaomi/push/b/e;->token:Ljava/lang/String;

    iput-object p4, p0, Lcom/xiaomi/push/b/e;->ddj:Ljava/io/File;

    iput-boolean p5, p0, Lcom/xiaomi/push/b/e;->ddg:Z

    return-void
.end method

.method private cLP()Z
    .locals 12

    const/4 v6, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/push/b/e;->ddh:Lcom/xiaomi/push/b/f;

    invoke-static {v0}, Lcom/xiaomi/push/b/f;->cLV(Lcom/xiaomi/push/b/f;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "log.timestamp"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string/jumbo v0, "log.requst"

    const-string/jumbo v1, ""

    invoke-interface {v7, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "time"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    const-string/jumbo v2, "times"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    move-wide v4, v0

    move v0, v2

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    const-wide/32 v10, 0x5265c00

    cmp-long v1, v8, v10

    if-ltz v1, :cond_0

    move v1, v6

    :goto_1
    if-nez v1, :cond_2

    const/16 v1, 0xa

    if-gt v0, v1, :cond_1

    :goto_2
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    :try_start_1
    const-string/jumbo v2, "time"

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    add-int/lit8 v0, v0, 0x1

    const-string/jumbo v2, "times"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "log.requst"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    return v6

    :catch_0
    move-exception v2

    move-wide v4, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_1

    :cond_1
    return v3

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move v0, v3

    goto :goto_2

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "JSONException on put "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto :goto_3
.end method


# virtual methods
.method public cAS()V
    .locals 4

    const/4 v1, 0x3

    iget-boolean v0, p0, Lcom/xiaomi/push/b/e;->ddk:Z

    if-eqz v0, :cond_2

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/xiaomi/push/b/e;->ddk:Z

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/push/b/e;->ddj:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/push/b/e;->ddh:Lcom/xiaomi/push/b/f;

    iget v1, p0, Lcom/xiaomi/push/b/e;->ddi:I

    const/4 v2, 0x1

    shl-int v1, v2, v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lcom/xiaomi/push/b/f;->cLY(Lcom/xiaomi/push/b/f;J)V

    return-void

    :cond_2
    iget v0, p0, Lcom/xiaomi/push/b/e;->ddi:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/push/b/e;->ddi:I

    iget v0, p0, Lcom/xiaomi/push/b/e;->ddi:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/b/e;->ddh:Lcom/xiaomi/push/b/f;

    invoke-static {v0}, Lcom/xiaomi/push/b/f;->cLR(Lcom/xiaomi/push/b/f;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/xiaomi/push/b/e;->ddi:I

    if-ge v0, v1, :cond_1

    goto :goto_1
.end method

.method public cLN()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/push/b/e;->ddh:Lcom/xiaomi/push/b/f;

    invoke-static {v1}, Lcom/xiaomi/push/b/f;->cLV(Lcom/xiaomi/push/b/f;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/g/b;->cAE(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-boolean v1, p0, Lcom/xiaomi/push/b/e;->ddg:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/xiaomi/push/b/e;->ddh:Lcom/xiaomi/push/b/f;

    invoke-static {v1}, Lcom/xiaomi/push/b/f;->cLV(Lcom/xiaomi/push/b/f;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method public cwo()V
    .locals 4

    :try_start_0
    invoke-direct {p0}, Lcom/xiaomi/push/b/e;->cLP()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/push/b/e;->ddk:Z

    :goto_1
    return-void

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lcom/xiaomi/push/service/an;->cQA()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "uid"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/push/b/e;->token:Ljava/lang/String;

    const-string/jumbo v2, "token"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/push/b/e;->ddh:Lcom/xiaomi/push/b/f;

    invoke-static {v1}, Lcom/xiaomi/push/b/f;->cLV(Lcom/xiaomi/push/b/f;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "net"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/xiaomi/push/b/e;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/push/b/e;->ddj:Ljava/io/File;

    const-string/jumbo v3, "file"

    invoke-static {v1, v0, v2, v3}, Lcom/xiaomi/channel/commonutils/g/b;->cAA(Ljava/lang/String;Ljava/util/Map;Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
