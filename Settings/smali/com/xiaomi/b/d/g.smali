.class Lcom/xiaomi/b/d/g;
.super Landroid/os/Handler;
.source "GeoFencingServiceWrapper.java"


# instance fields
.field final synthetic das:Lcom/xiaomi/b/d/e;


# direct methods
.method public constructor <init>(Lcom/xiaomi/b/d/e;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "unknown message type "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cHU(Lcom/xiaomi/b/d/e;)I

    iget-object v0, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    iget-object v1, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v1}, Lcom/xiaomi/b/d/e;->cId(Lcom/xiaomi/b/d/e;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/b/d/e;->cHV(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Try bindService count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v1}, Lcom/xiaomi/b/d/e;->cIf(Lcom/xiaomi/b/d/e;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ",mBinded="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v1}, Lcom/xiaomi/b/d/e;->cHX(Lcom/xiaomi/b/d/e;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "GeoFencingServiceWrapper"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cHX(Lcom/xiaomi/b/d/e;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cIe(Lcom/xiaomi/b/d/e;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cIf(Lcom/xiaomi/b/d/e;)I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cIe(Lcom/xiaomi/b/d/e;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cHY(Lcom/xiaomi/b/d/e;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/xiaomi/b/d/g;->das:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cIg(Lcom/xiaomi/b/d/e;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
