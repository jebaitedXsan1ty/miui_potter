.class Lcom/xiaomi/b/d/d;
.super Ljava/lang/Object;
.source "GeoFencingServiceWrapper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic dad:Lcom/xiaomi/b/d/e;


# direct methods
.method constructor <init>(Lcom/xiaomi/b/d/e;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/b/d/d;->dad:Lcom/xiaomi/b/d/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "*** GeoFencingService connected ***"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/b/d/d;->dad:Lcom/xiaomi/b/d/e;

    invoke-static {p2}, Lcom/xiaomi/b/d/b;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/b/d/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/b/d/e;->cHW(Lcom/xiaomi/b/d/e;Lcom/xiaomi/b/d/a;)Lcom/xiaomi/b/d/a;

    iget-object v0, p0, Lcom/xiaomi/b/d/d;->dad:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cIe(Lcom/xiaomi/b/d/e;)Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/b/d/d;->dad:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cIe(Lcom/xiaomi/b/d/e;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/xiaomi/b/d/d;->dad:Lcom/xiaomi/b/d/e;

    invoke-static {v0}, Lcom/xiaomi/b/d/e;->cIe(Lcom/xiaomi/b/d/e;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    const-string/jumbo v0, "GeoFencingServiceWrapper"

    const-string/jumbo v1, "*** GeoFencingService disconnected ***"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/xiaomi/b/d/d;->dad:Lcom/xiaomi/b/d/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/xiaomi/b/d/e;->cHW(Lcom/xiaomi/b/d/e;Lcom/xiaomi/b/d/a;)Lcom/xiaomi/b/d/a;

    return-void
.end method
