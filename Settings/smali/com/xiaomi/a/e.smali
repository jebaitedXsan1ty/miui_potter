.class public Lcom/xiaomi/a/e;
.super Ljava/lang/Object;
.source "Blob.java"


# static fields
.field private static final cXO:[B

.field private static cXQ:J

.field private static prefix:Ljava/lang/String;


# instance fields
.field private cXM:[B

.field private cXN:Lcom/xiaomi/push/a/d;

.field private cXP:S

.field mPackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/xiaomi/smack/c/e;->cwF(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/a/e;->prefix:Ljava/lang/String;

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/xiaomi/a/e;->cXQ:J

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/xiaomi/a/e;->cXO:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput-short v0, p0, Lcom/xiaomi/a/e;->cXP:S

    sget-object v0, Lcom/xiaomi/a/e;->cXO:[B

    iput-object v0, p0, Lcom/xiaomi/a/e;->cXM:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/a/e;->mPackageName:Ljava/lang/String;

    new-instance v0, Lcom/xiaomi/push/a/d;

    invoke-direct {v0}, Lcom/xiaomi/push/a/d;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    return-void
.end method

.method constructor <init>(Lcom/xiaomi/push/a/d;S[B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput-short v0, p0, Lcom/xiaomi/a/e;->cXP:S

    sget-object v0, Lcom/xiaomi/a/e;->cXO:[B

    iput-object v0, p0, Lcom/xiaomi/a/e;->cXM:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/a/e;->mPackageName:Ljava/lang/String;

    iput-object p1, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    int-to-short v0, p2

    iput-short v0, p0, Lcom/xiaomi/a/e;->cXP:S

    iput-object p3, p0, Lcom/xiaomi/a/e;->cXM:[B

    return-void
.end method

.method static cBQ(Ljava/nio/ByteBuffer;)Lcom/xiaomi/a/e;
    .locals 7

    :try_start_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v2

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v3

    new-instance v4, Lcom/xiaomi/push/a/d;

    invoke-direct {v4}, Lcom/xiaomi/push/a/d;-><init>()V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v6

    add-int/lit8 v6, v6, 0x8

    invoke-virtual {v4, v5, v6, v2}, Lcom/xiaomi/push/a/d;->diD([BII)Lcom/google/protobuf/micro/c;

    new-array v5, v3, [B

    add-int/lit8 v2, v2, 0x8

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    const/4 v2, 0x0

    invoke-virtual {v0, v5, v2, v3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    new-instance v0, Lcom/xiaomi/a/e;

    invoke-direct {v0, v4, v1, v5}, Lcom/xiaomi/a/e;-><init>(Lcom/xiaomi/push/a/d;S[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "read Blob err :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Malformed Input"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static declared-synchronized cBR()Ljava/lang/String;
    .locals 6

    const-class v1, Lcom/xiaomi/a/e;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/xiaomi/a/e;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-wide v2, Lcom/xiaomi/a/e;->cXQ:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    sput-wide v4, Lcom/xiaomi/a/e;->cXQ:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static cBY(Lcom/xiaomi/smack/packet/d;Ljava/lang/String;)Lcom/xiaomi/a/e;
    .locals 6

    const/4 v5, 0x0

    new-instance v2, Lcom/xiaomi/a/e;

    invoke-direct {v2}, Lcom/xiaomi/a/e;-><init>()V

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/d;->cvN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/xiaomi/a/e;->cBJ(I)V

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/d;->cvB()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/xiaomi/a/e;->cBM(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/d;->cvx()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/xiaomi/a/e;->cBW(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/d;->cvw()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/xiaomi/a/e;->cCb(Ljava/lang/String;)V

    const-string/jumbo v0, "XMLMSG"

    invoke-virtual {v2, v0, v5}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/d;->cvl()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "utf8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v2, v0, p1}, Lcom/xiaomi/a/e;->cBN([BLjava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Lcom/xiaomi/a/e;->cBT(S)V

    const-string/jumbo v0, "SECMSG"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-object v2

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Blob parse chid err "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    :try_start_2
    invoke-virtual {v2, v0}, Lcom/xiaomi/a/e;->cBT(S)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Blob setPayload err\uff1a "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method cBE(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 5

    if-eqz p1, :cond_0

    :goto_0
    iget-short v0, p0, Lcom/xiaomi/a/e;->cXP:S

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cIj()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXM:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v4}, Lcom/xiaomi/push/a/d;->cIj()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/xiaomi/push/a/d;->diB([BII)V

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v1}, Lcom/xiaomi/push/a/d;->cIj()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXM:[B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    return-object p1

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/a/e;->getSerializedSize()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object p1

    goto :goto_0
.end method

.method public cBI()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cIW()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cBJ(I)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/a/d;->cJb(I)Lcom/xiaomi/push/a/d;

    return-void
.end method

.method public cBK()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public cBL(Ljava/lang/String;)[B
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cJj()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cJj()I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unknow cipher = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v1}, Lcom/xiaomi/push/a/d;->cJj()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXM:[B

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/xiaomi/push/service/c;->cMy(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXM:[B

    invoke-static {v0, v1}, Lcom/xiaomi/push/service/c;->cMF([B[B)[B

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/a/e;->cXM:[B

    return-object v0
.end method

.method public cBM(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/a/d;->cJe(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    return-void
.end method

.method public cBN([BLjava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/d;->cJf(I)Lcom/xiaomi/push/a/d;

    iput-object p1, p0, Lcom/xiaomi/a/e;->cXM:[B

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/d;->cJf(I)Lcom/xiaomi/push/a/d;

    invoke-virtual {p0}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/xiaomi/push/service/c;->cMy(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0, p1}, Lcom/xiaomi/push/service/c;->cMF([B[B)[B

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/a/e;->cXM:[B

    goto :goto_0
.end method

.method public cBO()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cIS()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v1}, Lcom/xiaomi/push/a/d;->cJp()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v1}, Lcom/xiaomi/push/a/d;->cIT()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v1}, Lcom/xiaomi/push/a/d;->cIZ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cBP()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cJg()Z

    move-result v0

    return v0
.end method

.method public cBS()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->getId()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "ID_NOT_AVAILABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v1}, Lcom/xiaomi/push/a/d;->cIX()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    invoke-static {}, Lcom/xiaomi/a/e;->cBR()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/a/d;->cJe(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_0
.end method

.method public cBT(S)V
    .locals 1

    int-to-short v0, p1

    iput-short v0, p0, Lcom/xiaomi/a/e;->cXP:S

    return-void
.end method

.method public cBU()S
    .locals 1

    iget-short v0, p0, Lcom/xiaomi/a/e;->cXP:S

    return v0
.end method

.method public cBV()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cJl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cBW(Ljava/lang/String;)V
    .locals 5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string/jumbo v1, "/"

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v4, v2, v3}, Lcom/xiaomi/push/a/d;->cJk(J)Lcom/xiaomi/push/a/d;

    iget-object v2, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/d;->cIP(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/a/d;->cIO(Ljava/lang/String;)Lcom/xiaomi/push/a/d;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Blob parse user err "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cBX()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cJn()I

    move-result v0

    return v0
.end method

.method public cBZ(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/a/d;->cIL(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cJa()Lcom/xiaomi/push/a/d;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "command should not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0, p2}, Lcom/xiaomi/push/a/d;->cIM(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_0
.end method

.method public cCa()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cIQ()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cCb(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/a/e;->mPackageName:Ljava/lang/String;

    return-void
.end method

.method public cCc(JLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/push/a/d;->cJk(J)Lcom/xiaomi/push/a/d;

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0, p3}, Lcom/xiaomi/push/a/d;->cIP(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0, p4}, Lcom/xiaomi/push/a/d;->cIO(Ljava/lang/String;)Lcom/xiaomi/push/a/d;

    goto :goto_1
.end method

.method public cCd()[B
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXM:[B

    return-object v0
.end method

.method public cCe()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->cJi()I

    move-result v0

    return v0
.end method

.method public getSerializedSize()I
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/a/e;->cXN:Lcom/xiaomi/push/a/d;

    invoke-virtual {v0}, Lcom/xiaomi/push/a/d;->getSerializedSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lcom/xiaomi/a/e;->cXM:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Blob [chid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/a/e;->cCe()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; Id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; cmd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/a/e;->cBI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/a/e;->cBU()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "; from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/xiaomi/a/e;->cBO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
