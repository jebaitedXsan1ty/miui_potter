.class Lcom/xiaomi/a/d;
.super Ljava/lang/Object;
.source "Binder.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cBG(Lcom/xiaomi/push/service/as;Ljava/lang/String;Lcom/xiaomi/smack/e;)V
    .locals 6

    const/4 v1, 0x0

    new-instance v2, Lcom/xiaomi/push/a/e;

    invoke-direct {v2}, Lcom/xiaomi/push/a/e;-><init>()V

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->token:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dht:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhx:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_2
    iget-boolean v0, p0, Lcom/xiaomi/push/service/as;->dhF:Z

    if-nez v0, :cond_4

    const-string/jumbo v0, "0"

    :goto_3
    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/e;->cJD(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhw:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v0, "XIAOMI-SASL"

    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/e;->cJF(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    :goto_4
    new-instance v3, Lcom/xiaomi/a/e;

    invoke-direct {v3}, Lcom/xiaomi/a/e;-><init>()V

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->userId:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/xiaomi/a/e;->cBW(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/xiaomi/a/e;->cBJ(I)V

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->pkgName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/xiaomi/a/e;->cCb(Ljava/lang/String;)V

    const-string/jumbo v0, "BIND"

    invoke-virtual {v3, v0, v1}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/xiaomi/a/e;->cBM(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "[Slim]: bind id="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v4, "challenge"

    invoke-interface {v0, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/xiaomi/push/service/as;->token:Ljava/lang/String;

    const-string/jumbo v5, "token"

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/xiaomi/push/service/as;->dhy:Ljava/lang/String;

    const-string/jumbo v5, "chid"

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/xiaomi/push/service/as;->userId:Ljava/lang/String;

    const-string/jumbo v5, "from"

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/xiaomi/a/e;->cBS()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "id"

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v4, "to"

    const-string/jumbo v5, "xiaomi.com"

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v4, p0, Lcom/xiaomi/push/service/as;->dhF:Z

    if-nez v4, :cond_6

    const-string/jumbo v4, "kick"

    const-string/jumbo v5, "0"

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_5
    iget-object v4, p0, Lcom/xiaomi/push/service/as;->dht:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string/jumbo v4, "client_attrs"

    const-string/jumbo v5, ""

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_6
    iget-object v4, p0, Lcom/xiaomi/push/service/as;->dhx:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string/jumbo v4, "cloud_attrs"

    const-string/jumbo v5, ""

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_7
    iget-object v4, p0, Lcom/xiaomi/push/service/as;->dhw:Ljava/lang/String;

    const-string/jumbo v5, "XIAOMI-PASS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_0
    iget-object v4, p0, Lcom/xiaomi/push/service/as;->dhw:Ljava/lang/String;

    iget-object v5, p0, Lcom/xiaomi/push/service/as;->dhu:Ljava/lang/String;

    invoke-static {v4, v1, v0, v5}, Lcom/xiaomi/channel/commonutils/c/d;->czo(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/e;->cJs(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    invoke-virtual {v2}, Lcom/xiaomi/push/a/e;->diz()[B

    move-result-object v0

    invoke-virtual {v3, v0, v1}, Lcom/xiaomi/a/e;->cBN([BLjava/lang/String;)V

    invoke-virtual {p2, v3}, Lcom/xiaomi/smack/e;->cxz(Lcom/xiaomi/a/e;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->token:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/e;->cJB(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dht:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/e;->cJI(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhx:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/e;->cJH(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto/16 :goto_2

    :cond_4
    const-string/jumbo v0, "1"

    goto/16 :goto_3

    :cond_5
    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhw:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/xiaomi/push/a/e;->cJF(Ljava/lang/String;)Lcom/xiaomi/push/a/e;

    goto/16 :goto_4

    :cond_6
    const-string/jumbo v4, "kick"

    const-string/jumbo v5, "1"

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_7
    iget-object v4, p0, Lcom/xiaomi/push/service/as;->dht:Ljava/lang/String;

    const-string/jumbo v5, "client_attrs"

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    :cond_8
    iget-object v4, p0, Lcom/xiaomi/push/service/as;->dhx:Ljava/lang/String;

    const-string/jumbo v5, "cloud_attrs"

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_9
    iget-object v4, p0, Lcom/xiaomi/push/service/as;->dhw:Ljava/lang/String;

    const-string/jumbo v5, "XMPUSH-PASS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/xiaomi/push/service/as;->dhw:Ljava/lang/String;

    const-string/jumbo v4, "XIAOMI-SASL"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    move-object v0, v1

    goto :goto_8

    :cond_a
    move-object v0, v1

    goto :goto_8
.end method

.method public static cBH(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/smack/e;)V
    .locals 3

    new-instance v0, Lcom/xiaomi/a/e;

    invoke-direct {v0}, Lcom/xiaomi/a/e;-><init>()V

    invoke-virtual {v0, p1}, Lcom/xiaomi/a/e;->cBW(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/a/e;->cBJ(I)V

    const-string/jumbo v1, "UBND"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/a/e;->cBZ(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/xiaomi/smack/e;->cxz(Lcom/xiaomi/a/e;)V

    return-void
.end method
