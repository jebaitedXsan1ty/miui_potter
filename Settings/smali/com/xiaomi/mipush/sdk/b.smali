.class public Lcom/xiaomi/mipush/sdk/b;
.super Ljava/lang/Object;
.source "GeoFenceRegister.java"


# instance fields
.field private cYf:Lcom/xiaomi/b/d/e;

.field private final cYg:I

.field private final cYh:D

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/xiaomi/mipush/sdk/b;->cYg:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/mipush/sdk/b;->cYh:D

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/b;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/b;->cCr()V

    return-void
.end method

.method private cCr()V
    .locals 2

    new-instance v0, Lcom/xiaomi/b/d/e;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/b;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/xiaomi/b/d/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/b;->cYf:Lcom/xiaomi/b/d/e;

    return-void
.end method


# virtual methods
.method public cCp(Lcom/xiaomi/xmpush/thrift/GeoFencing;)Z
    .locals 12

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcD()Lcom/xiaomi/xmpush/thrift/Location;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0

    :cond_2
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcC()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcD()Lcom/xiaomi/xmpush/thrift/Location;

    move-result-object v4

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/b;->cYf:Lcom/xiaomi/b/d/e;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/b;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/xiaomi/xmpush/thrift/Location;->deR()D

    move-result-wide v2

    invoke-virtual {v4}, Lcom/xiaomi/xmpush/thrift/Location;->deO()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcC()D

    move-result-wide v6

    double-to-float v6, v6

    const-wide/16 v7, -0x1

    const-string/jumbo v9, "com.xiaomi.xmsf"

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/GeoFencing;->dcv()Lcom/xiaomi/xmpush/thrift/CoordinateProvider;

    move-result-object v11

    invoke-virtual {v11}, Lcom/xiaomi/xmpush/thrift/CoordinateProvider;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v0 .. v11}, Lcom/xiaomi/b/d/e;->cIa(Landroid/content/Context;DDFJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cCq(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/b;->cYf:Lcom/xiaomi/b/d/e;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/b;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.xiaomi.xmsf"

    invoke-virtual {v0, v1, v2, p1}, Lcom/xiaomi/b/d/e;->cIc(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
