.class public Lcom/xiaomi/mipush/sdk/Q;
.super Ljava/lang/Object;
.source "MiTinyDataClient.java"


# instance fields
.field private cZS:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field public final cZT:Ljava/util/ArrayList;

.field final synthetic cZU:Lcom/xiaomi/mipush/sdk/w;

.field private final cZV:Ljava/lang/Runnable;

.field private cZW:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public constructor <init>(Lcom/xiaomi/mipush/sdk/w;)V
    .locals 2

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/Q;->cZU:Lcom/xiaomi/mipush/sdk/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/Q;->cZS:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/Q;->cZT:Ljava/util/ArrayList;

    new-instance v0, Lcom/xiaomi/mipush/sdk/a;

    invoke-direct {v0, p0}, Lcom/xiaomi/mipush/sdk/a;-><init>(Lcom/xiaomi/mipush/sdk/Q;)V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/Q;->cZV:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic cHq(Lcom/xiaomi/mipush/sdk/Q;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/Q;->cHw()V

    return-void
.end method

.method static synthetic cHr(Lcom/xiaomi/mipush/sdk/Q;Ljava/util/concurrent/ScheduledFuture;)Ljava/util/concurrent/ScheduledFuture;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/Q;->cZW:Ljava/util/concurrent/ScheduledFuture;

    return-object p1
.end method

.method static synthetic cHs(Lcom/xiaomi/mipush/sdk/Q;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/Q;->cHt()V

    return-void
.end method

.method private cHt()V
    .locals 7

    const/4 v6, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/Q;->cZT:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/Q;->cZU:Lcom/xiaomi/mipush/sdk/w;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/w;->cEM(Lcom/xiaomi/mipush/sdk/w;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/Q;->cZU:Lcom/xiaomi/mipush/sdk/w;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/w;->cEM(Lcom/xiaomi/mipush/sdk/w;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;

    aput-object v0, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const/16 v4, 0x7800

    invoke-static {v3, v1, v2, v4}, Lcom/xiaomi/push/service/y;->cNO(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "MiTinyDataClient Send item by PushServiceClient.sendMessage(XmActionNotification)."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/Q;->cZU:Lcom/xiaomi/mipush/sdk/w;

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/w;->cEM(Lcom/xiaomi/mipush/sdk/w;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v3

    sget-object v4, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v5, 0x0

    invoke-virtual {v3, v1, v4, v6, v5}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    goto :goto_0
.end method

.method static synthetic cHv(Lcom/xiaomi/mipush/sdk/Q;)Ljava/util/concurrent/ScheduledFuture;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/Q;->cZW:Ljava/util/concurrent/ScheduledFuture;

    return-object v0
.end method

.method private cHw()V
    .locals 7

    const-wide/16 v2, 0x3e8

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/Q;->cZW:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/Q;->cZS:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/Q;->cZV:Ljava/lang/Runnable;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide v4, v2

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/Q;->cZW:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method


# virtual methods
.method public cHu(Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/Q;->cZS:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lcom/xiaomi/mipush/sdk/M;

    invoke-direct {v1, p0, p1}, Lcom/xiaomi/mipush/sdk/M;-><init>(Lcom/xiaomi/mipush/sdk/Q;Lcom/xiaomi/xmpush/thrift/ClientUploadDataItem;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
