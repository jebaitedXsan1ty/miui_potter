.class public Lcom/xiaomi/mipush/sdk/R;
.super Ljava/lang/Object;
.source "PushContainerHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cHA(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Lorg/apache/thrift/TBase;
    .locals 3

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddC()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddM()[B

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddQ()Lcom/xiaomi/xmpush/thrift/ActionType;

    move-result-object v1

    iget-boolean v2, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->isRequest:Z

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/R;->cHy(Lcom/xiaomi/xmpush/thrift/ActionType;Z)Lorg/apache/thrift/TBase;

    move-result-object v1

    if-nez v1, :cond_1

    :goto_1
    return-object v1

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDS()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/c/a;->cyV(Ljava/lang/String;)[B

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddM()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/channel/commonutils/android/a;->czI([B[B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/xiaomi/mipush/sdk/DecryptException;

    const-string/jumbo v2, "the aes decrypt failed."

    invoke-direct {v1, v2, v0}, Lcom/xiaomi/mipush/sdk/DecryptException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    invoke-static {v1, v0}, Lcom/xiaomi/xmpush/thrift/a;->daH(Lorg/apache/thrift/TBase;[B)V

    goto :goto_1
.end method

.method protected static cHx(Landroid/content/Context;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;
    .locals 6

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-direct {v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;-><init>()V

    if-nez p3, :cond_1

    :goto_0
    new-instance v2, Lcom/xiaomi/xmpush/thrift/Target;

    invoke-direct {v2}, Lcom/xiaomi/xmpush/thrift/Target;-><init>()V

    const-wide/16 v4, 0x5

    iput-wide v4, v2, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    const-string/jumbo v3, "fakeid"

    iput-object v3, v2, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->dec(Lcom/xiaomi/xmpush/thrift/Target;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddR(Ljava/nio/ByteBuffer;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-virtual {v1, p2}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddD(Lcom/xiaomi/xmpush/thrift/ActionType;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddU(Z)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-virtual {v1, p4}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddL(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-virtual {v1, p3}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->ddT(Z)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    invoke-virtual {v1, p5}, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->dea(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    return-object v1

    :cond_0
    const-string/jumbo v0, "invoke convertThriftObjectToBytes method, return null."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v4

    :cond_1
    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDS()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/c/a;->cyV(Ljava/lang/String;)[B

    move-result-object v2

    :try_start_0
    invoke-static {v2, v0}, Lcom/xiaomi/channel/commonutils/android/a;->czJ([B[B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "regSecret is empty, return null"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return-object v4

    :catch_0
    move-exception v2

    const-string/jumbo v2, "encryption error. "

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static cHy(Lcom/xiaomi/xmpush/thrift/ActionType;Z)Lorg/apache/thrift/TBase;
    .locals 2

    sget-object v0, Lcom/xiaomi/mipush/sdk/x;->cYW:[I

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/ActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistrationResult;-><init>()V

    return-object v0

    :pswitch_1
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnRegistrationResult;-><init>()V

    return-object v0

    :pswitch_2
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSubscriptionResult;-><init>()V

    return-object v0

    :pswitch_3
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionUnSubscriptionResult;-><init>()V

    return-object v0

    :pswitch_4
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendMessage;-><init>()V

    return-object v0

    :pswitch_5
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage;-><init>()V

    return-object v0

    :pswitch_6
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;-><init>()V

    return-object v0

    :pswitch_7
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionSendFeedbackResult;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionSendFeedbackResult;-><init>()V

    return-object v0

    :pswitch_8
    if-nez p1, :cond_0

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckNotification;->dew(Z)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>()V

    return-object v0

    :pswitch_9
    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommandResult;-><init>()V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method protected static cHz(Landroid/content/Context;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;
    .locals 6

    const/4 v3, 0x0

    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAQ:Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-virtual {p2, v0}, Lcom/xiaomi/xmpush/thrift/ActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/xiaomi/mipush/sdk/R;->cHx(Landroid/content/Context;Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLjava/lang/String;Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v3, 0x1

    goto :goto_0
.end method
