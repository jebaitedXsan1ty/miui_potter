.class Lcom/xiaomi/mipush/sdk/G;
.super Ljava/lang/Object;
.source "SyncInfoHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic cZv:Z

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/xiaomi/mipush/sdk/G;->cZv:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x0

    const-string/jumbo v0, "do sync info"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    new-instance v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-static {}, Lcom/xiaomi/mipush/sdk/C;->cGf()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v6}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;-><init>(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;

    move-result-object v2

    sget-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzw:Lcom/xiaomi/xmpush/thrift/NotificationType;

    iget-object v0, v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dam(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDQ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->daf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->dau(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    const-string/jumbo v5, "app_version"

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/xiaomi/channel/commonutils/android/b;->czK(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v5, v3}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    const-string/jumbo v5, "app_version_code"

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/xiaomi/channel/commonutils/android/b;->czL(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v5, v3}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    const-string/jumbo v3, "push_sdk_vn"

    const-string/jumbo v4, "3_6_3"

    invoke-static {v0, v3, v4}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    const-string/jumbo v3, "push_sdk_vc"

    const/16 v4, 0x778b

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    const-string/jumbo v3, "token"

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDV()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/e;->cAf()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    const-string/jumbo v3, "reg_id"

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDK()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    const-string/jumbo v3, "reg_secret"

    invoke-virtual {v2}, Lcom/xiaomi/mipush/sdk/l;->cDS()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v2}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/C;->cGt(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, ","

    const-string/jumbo v3, "-"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    const-string/jumbo v3, "accept_time"

    invoke-static {v2, v3, v0}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/xiaomi/mipush/sdk/G;->cZv:Z

    if-nez v0, :cond_2

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    const-string/jumbo v3, "aliases"

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/C;->cGg(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/d;->cCz(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v2}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    const-string/jumbo v3, "topics"

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/C;->cGp(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/d;->cCz(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v2}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    const-string/jumbo v3, "user_accounts"

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/C;->cGM(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/d;->cCz(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v2}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/z;

    move-result-object v0

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v6, v3}, Lcom/xiaomi/mipush/sdk/z;->cFk(Lorg/apache/thrift/TBase;Lcom/xiaomi/xmpush/thrift/ActionType;ZLcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/android/c;->czW(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/c/b;->czj(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/xiaomi/channel/commonutils/android/c;->czT(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_2
    iget-object v3, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    const-string/jumbo v4, "imei_md5"

    invoke-static {v3, v4, v0}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    const-string/jumbo v3, "aliases_md5"

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/C;->cGg(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/d;->cCy(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v2}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    const-string/jumbo v3, "topics_md5"

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/C;->cGp(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/d;->cCy(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v2}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/xiaomi/xmpush/thrift/XmPushActionNotification;->extra:Ljava/util/Map;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/G;->val$context:Landroid/content/Context;

    const-string/jumbo v3, "accounts_md5"

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/C;->cGM(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/d;->cCy(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v2}, Lcom/xiaomi/channel/commonutils/android/d;->cAe(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
