.class Lcom/xiaomi/mipush/sdk/P;
.super Ljava/lang/Object;
.source "OperatePushHelper.java"


# instance fields
.field cZR:I

.field messageId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/mipush/sdk/P;->cZR:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/P;->messageId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    return v2

    :cond_1
    instance-of v0, p1, Lcom/xiaomi/mipush/sdk/P;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/xiaomi/mipush/sdk/P;

    iget-object v0, p1, Lcom/xiaomi/mipush/sdk/P;->messageId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/xiaomi/mipush/sdk/P;->messageId:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/P;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0
.end method
