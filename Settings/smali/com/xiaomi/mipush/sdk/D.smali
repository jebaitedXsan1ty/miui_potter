.class public Lcom/xiaomi/mipush/sdk/D;
.super Ljava/lang/Object;
.source "AssemblePushInfoHelper.java"


# static fields
.field private static cZt:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/mipush/sdk/D;->cZt:Ljava/util/HashMap;

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    new-instance v1, Lcom/xiaomi/mipush/sdk/u;

    const-string/jumbo v2, "com.xiaomi.assemble.control.HmsPushManager"

    const-string/jumbo v3, "newInstance"

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/mipush/sdk/u;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/D;->cGQ(Lcom/xiaomi/mipush/sdk/AssemblePush;Lcom/xiaomi/mipush/sdk/u;)V

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    new-instance v1, Lcom/xiaomi/mipush/sdk/u;

    const-string/jumbo v2, "com.xiaomi.assemble.control.FCMPushManager"

    const-string/jumbo v3, "newInstance"

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/mipush/sdk/u;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/D;->cGQ(Lcom/xiaomi/mipush/sdk/AssemblePush;Lcom/xiaomi/mipush/sdk/u;)V

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    new-instance v1, Lcom/xiaomi/mipush/sdk/u;

    const-string/jumbo v2, "com.xiaomi.assemble.control.COSPushManager"

    const-string/jumbo v3, "newInstance"

    invoke-direct {v1, v2, v3}, Lcom/xiaomi/mipush/sdk/u;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/D;->cGQ(Lcom/xiaomi/mipush/sdk/AssemblePush;Lcom/xiaomi/mipush/sdk/u;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cGO(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/RetryType;
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/xiaomi/mipush/sdk/B;->cZj:[I

    invoke-virtual {p0}, Lcom/xiaomi/mipush/sdk/AssemblePush;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/RetryType;->cZB:Lcom/xiaomi/mipush/sdk/RetryType;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/xiaomi/mipush/sdk/RetryType;->cZy:Lcom/xiaomi/mipush/sdk/RetryType;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/xiaomi/mipush/sdk/RetryType;->cZA:Lcom/xiaomi/mipush/sdk/RetryType;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static cGP(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/u;
    .locals 1

    sget-object v0, Lcom/xiaomi/mipush/sdk/D;->cZt:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/u;

    return-object v0
.end method

.method private static cGQ(Lcom/xiaomi/mipush/sdk/AssemblePush;Lcom/xiaomi/mipush/sdk/u;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/D;->cZt:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static cGR(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/xmpush/thrift/ConfigKey;
    .locals 1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/ConfigKey;->dqs:Lcom/xiaomi/xmpush/thrift/ConfigKey;

    return-object v0
.end method
