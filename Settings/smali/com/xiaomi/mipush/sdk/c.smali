.class public Lcom/xiaomi/mipush/sdk/c;
.super Ljava/lang/Object;
.source "PushManagerFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cCs(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;
    .locals 1

    invoke-static {p0, p1}, Lcom/xiaomi/mipush/sdk/c;->cCt(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;

    move-result-object v0

    return-object v0
.end method

.method private static cCt(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/xiaomi/mipush/sdk/D;->cGP(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/u;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-object v2

    :cond_1
    iget-object v1, v0, Lcom/xiaomi/mipush/sdk/u;->className:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/xiaomi/mipush/sdk/u;->cYM:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/xiaomi/mipush/sdk/u;->className:Ljava/lang/String;

    iget-object v0, v0, Lcom/xiaomi/mipush/sdk/u;->cYM:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v1, v0, v2}, Lcom/xiaomi/channel/commonutils/a/b;->cyC(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/t;

    return-object v0
.end method
