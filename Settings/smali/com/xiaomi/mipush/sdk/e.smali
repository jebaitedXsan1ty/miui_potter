.class public Lcom/xiaomi/mipush/sdk/e;
.super Ljava/lang/Object;
.source "AssemblePushCollectionsManager.java"

# interfaces
.implements Lcom/xiaomi/mipush/sdk/t;


# static fields
.field private static volatile cYi:Lcom/xiaomi/mipush/sdk/e;


# instance fields
.field private cYj:Lcom/xiaomi/mipush/sdk/p;

.field private cYk:Ljava/util/Map;

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    return-void
.end method

.method private cCB()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/p;->cEn()Z

    move-result v0

    if-nez v0, :cond_7

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/p;->cEn()Z

    move-result v0

    if-nez v0, :cond_8

    :cond_2
    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v0}, Lcom/xiaomi/mipush/sdk/e;->cCC(Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v0

    if-nez v0, :cond_a

    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/p;->cEo()Z

    move-result v0

    if-nez v0, :cond_b

    :goto_3
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/p;->cEo()Z

    move-result v0

    if-nez v0, :cond_c

    :cond_4
    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v0}, Lcom/xiaomi/mipush/sdk/e;->cCC(Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v0

    if-nez v0, :cond_e

    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/p;->cEp()Z

    move-result v0

    if-nez v0, :cond_f

    :goto_5
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/p;->cEp()Z

    move-result v0

    if-nez v0, :cond_10

    :cond_6
    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v0}, Lcom/xiaomi/mipush/sdk/e;->cCC(Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v0}, Lcom/xiaomi/mipush/sdk/e;->cCG(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v1}, Lcom/xiaomi/mipush/sdk/e;->cCA(Lcom/xiaomi/mipush/sdk/AssemblePush;)V

    invoke-interface {v0}, Lcom/xiaomi/mipush/sdk/t;->cCE()V

    goto :goto_0

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " HW user switch : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/p;->cEn()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " HW online switch : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/h;->cDn(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " HW isSupport : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZo:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/A;->cFS(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/PhoneBrand;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/mipush/sdk/PhoneBrand;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/h;->cDn(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/xiaomi/mipush/sdk/PhoneBrand;->cZo:Lcom/xiaomi/mipush/sdk/PhoneBrand;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/A;->cFS(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/PhoneBrand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/mipush/sdk/PhoneBrand;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v0}, Lcom/xiaomi/mipush/sdk/e;->cCC(Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_6
    const-string/jumbo v0, "hw manager add to list"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/c;->cCs(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/mipush/sdk/e;->cCF(Lcom/xiaomi/mipush/sdk/AssemblePush;Lcom/xiaomi/mipush/sdk/t;)V

    goto :goto_6

    :cond_a
    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v0}, Lcom/xiaomi/mipush/sdk/e;->cCG(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;

    move-result-object v0

    if-eqz v0, :cond_3

    sget-object v1, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZN:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v1}, Lcom/xiaomi/mipush/sdk/e;->cCA(Lcom/xiaomi/mipush/sdk/AssemblePush;)V

    invoke-interface {v0}, Lcom/xiaomi/mipush/sdk/t;->cCE()V

    goto/16 :goto_2

    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " FCM user switch : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/p;->cEo()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " FCM online switch : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/h;->cDn(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " FCM isSupport : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/A;->cFT(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_c
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/h;->cDn(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/A;->cFT(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v0}, Lcom/xiaomi/mipush/sdk/e;->cCC(Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_7
    const-string/jumbo v0, "fcm manager add to list"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_d
    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/c;->cCs(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/mipush/sdk/e;->cCF(Lcom/xiaomi/mipush/sdk/AssemblePush;Lcom/xiaomi/mipush/sdk/t;)V

    goto :goto_7

    :cond_e
    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v0}, Lcom/xiaomi/mipush/sdk/e;->cCG(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;

    move-result-object v0

    if-eqz v0, :cond_5

    sget-object v1, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZP:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-virtual {p0, v1}, Lcom/xiaomi/mipush/sdk/e;->cCA(Lcom/xiaomi/mipush/sdk/AssemblePush;)V

    invoke-interface {v0}, Lcom/xiaomi/mipush/sdk/t;->cCE()V

    goto/16 :goto_4

    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, " COS user switch : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->cYj:Lcom/xiaomi/mipush/sdk/p;

    invoke-virtual {v1}, Lcom/xiaomi/mipush/sdk/p;->cEp()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " COS online switch : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/h;->cDn(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string/jumbo v1, " COS isSupport : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/A;->cFU(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_10
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/h;->cDn(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/A;->cFU(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/e;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/xiaomi/mipush/sdk/AssemblePush;->cZM:Lcom/xiaomi/mipush/sdk/AssemblePush;

    invoke-static {v1, v2}, Lcom/xiaomi/mipush/sdk/c;->cCs(Landroid/content/Context;Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/xiaomi/mipush/sdk/e;->cCF(Lcom/xiaomi/mipush/sdk/AssemblePush;Lcom/xiaomi/mipush/sdk/t;)V

    goto/16 :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/e;
    .locals 2

    sget-object v0, Lcom/xiaomi/mipush/sdk/e;->cYi:Lcom/xiaomi/mipush/sdk/e;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/e;->cYi:Lcom/xiaomi/mipush/sdk/e;

    return-object v0

    :cond_0
    const-class v1, Lcom/xiaomi/mipush/sdk/e;

    const-class v0, Lcom/xiaomi/mipush/sdk/e;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/e;->cYi:Lcom/xiaomi/mipush/sdk/e;

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/xiaomi/mipush/sdk/e;

    invoke-direct {v0, p0}, Lcom/xiaomi/mipush/sdk/e;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/e;->cYi:Lcom/xiaomi/mipush/sdk/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public cCA(Lcom/xiaomi/mipush/sdk/AssemblePush;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public cCC(Lcom/xiaomi/mipush/sdk/AssemblePush;)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public cCD()V
    .locals 2

    const-string/jumbo v0, "assemble push register"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/e;->cCB()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/t;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/xiaomi/mipush/sdk/t;->cCD()V

    goto :goto_1
.end method

.method public cCE()V
    .locals 2

    const-string/jumbo v0, "assemble push unregister"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/t;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/xiaomi/mipush/sdk/t;->cCE()V

    goto :goto_0
.end method

.method public cCF(Lcom/xiaomi/mipush/sdk/AssemblePush;Lcom/xiaomi/mipush/sdk/t;)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public cCG(Lcom/xiaomi/mipush/sdk/AssemblePush;)Lcom/xiaomi/mipush/sdk/t;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/e;->cYk:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mipush/sdk/t;

    return-object v0
.end method
