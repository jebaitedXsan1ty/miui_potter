.class Lcom/xiaomi/mipush/sdk/K;
.super Landroid/database/ContentObserver;
.source "PushServiceClient.java"


# instance fields
.field final synthetic cZG:Lcom/xiaomi/mipush/sdk/z;


# direct methods
.method constructor <init>(Lcom/xiaomi/mipush/sdk/z;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/K;->cZG:Lcom/xiaomi/mipush/sdk/z;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/K;->cZG:Lcom/xiaomi/mipush/sdk/z;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/K;->cZG:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v1}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/push/service/R;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/R;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/push/service/R;->cOt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/xiaomi/mipush/sdk/z;->cFO(Lcom/xiaomi/mipush/sdk/z;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/K;->cZG:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->cFb(Lcom/xiaomi/mipush/sdk/z;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/K;->cZG:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/K;->cZG:Lcom/xiaomi/mipush/sdk/z;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/z;->cFw(Lcom/xiaomi/mipush/sdk/z;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/K;->cZG:Lcom/xiaomi/mipush/sdk/z;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/z;->cFB()V

    goto :goto_0
.end method
