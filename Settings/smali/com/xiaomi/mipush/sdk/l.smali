.class public Lcom/xiaomi/mipush/sdk/l;
.super Ljava/lang/Object;
.source "AppInfoHolder.java"


# static fields
.field private static cYs:Lcom/xiaomi/mipush/sdk/l;


# instance fields
.field private cYp:Ljava/util/Map;

.field private cYq:Lcom/xiaomi/mipush/sdk/o;

.field cYr:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/mipush/sdk/l;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/xiaomi/mipush/sdk/l;->cDH()V

    return-void
.end method

.method public static cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    const-string/jumbo v0, "mipush"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private cDH()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    new-instance v0, Lcom/xiaomi/mipush/sdk/o;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/xiaomi/mipush/sdk/o;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYp:Ljava/util/Map;

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "appId"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/mipush/sdk/o;->cYE:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "appToken"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/mipush/sdk/o;->cYz:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "regId"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/mipush/sdk/o;->cYw:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "regSec"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/mipush/sdk/o;->regSecret:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "devId"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-object v1, v1, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "vName"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/mipush/sdk/o;->cYC:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "valid"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/xiaomi/mipush/sdk/o;->cYy:Z

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "paused"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/xiaomi/mipush/sdk/o;->cYA:Z

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "envType"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/xiaomi/mipush/sdk/o;->cYB:I

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    const-string/jumbo v2, "regResource"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/xiaomi/mipush/sdk/o;->cYx:Ljava/lang/String;

    return-void

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-object v1, v1, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    const-string/jumbo v2, "a-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/l;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/channel/commonutils/android/c;->czU(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-object v2, v2, Lcom/xiaomi/mipush/sdk/o;->deviceId:Ljava/lang/String;

    const-string/jumbo v3, "devId"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/mipush/sdk/l;
    .locals 1

    sget-object v0, Lcom/xiaomi/mipush/sdk/l;->cYs:Lcom/xiaomi/mipush/sdk/l;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/mipush/sdk/l;->cYs:Lcom/xiaomi/mipush/sdk/l;

    return-object v0

    :cond_0
    new-instance v0, Lcom/xiaomi/mipush/sdk/l;

    invoke-direct {v0, p0}, Lcom/xiaomi/mipush/sdk/l;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/mipush/sdk/l;->cYs:Lcom/xiaomi/mipush/sdk/l;

    goto :goto_0
.end method


# virtual methods
.method public cDG(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    invoke-virtual {v0, p1, p2, p3}, Lcom/xiaomi/mipush/sdk/o;->cEe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public cDI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    invoke-virtual {v0, p1, p2, p3}, Lcom/xiaomi/mipush/sdk/o;->cEj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public cDJ()Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/o;->cEh()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string/jumbo v0, "Don\'t send message before initialization succeeded!"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return v1
.end method

.method public cDK()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-object v0, v0, Lcom/xiaomi/mipush/sdk/o;->cYw:Ljava/lang/String;

    return-object v0
.end method

.method public cDL()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/o;->cEh()Z

    move-result v0

    return v0
.end method

.method public cDM()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/o;->cEm()V

    return-void
.end method

.method public cDN()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-boolean v1, v1, Lcom/xiaomi/mipush/sdk/o;->cYy:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cDO()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-boolean v0, v0, Lcom/xiaomi/mipush/sdk/o;->cYA:Z

    return v0
.end method

.method public cDP(Ljava/lang/String;Lcom/xiaomi/mipush/sdk/o;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYp:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/xiaomi/mipush/sdk/o;->cEi(Lcom/xiaomi/mipush/sdk/o;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "hybrid_app_info_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/mipush/sdk/l;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/xiaomi/mipush/sdk/l;->cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public cDQ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-object v0, v0, Lcom/xiaomi/mipush/sdk/o;->cYE:Ljava/lang/String;

    return-object v0
.end method

.method public cDR(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    invoke-virtual {v0, p1}, Lcom/xiaomi/mipush/sdk/o;->cEk(Z)V

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "paused"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public cDS()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-object v0, v0, Lcom/xiaomi/mipush/sdk/o;->regSecret:Ljava/lang/String;

    return-object v0
.end method

.method public cDT(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/mipush/sdk/l;->cDF(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "vName"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iput-object p1, v0, Lcom/xiaomi/mipush/sdk/o;->cYC:Ljava/lang/String;

    return-void
.end method

.method public cDU()I
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget v0, v0, Lcom/xiaomi/mipush/sdk/o;->cYB:I

    return v0
.end method

.method public cDV()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    iget-object v0, v0, Lcom/xiaomi/mipush/sdk/o;->cYz:Ljava/lang/String;

    return-object v0
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/mipush/sdk/l;->cYq:Lcom/xiaomi/mipush/sdk/o;

    invoke-virtual {v0}, Lcom/xiaomi/mipush/sdk/o;->clear()V

    return-void
.end method
