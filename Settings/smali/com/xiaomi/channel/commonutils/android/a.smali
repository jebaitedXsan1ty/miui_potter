.class public Lcom/xiaomi/channel/commonutils/android/a;
.super Ljava/lang/Object;
.source "DataCryptUtils.java"


# static fields
.field private static final cWv:[B


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v7, 0x61

    const/16 v6, 0x54

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x10

    new-array v0, v0, [B

    const/16 v1, 0x64

    aput-byte v1, v0, v3

    const/16 v1, 0x17

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    aput-byte v6, v0, v4

    const/16 v1, 0x72

    const/4 v2, 0x3

    aput-byte v1, v0, v2

    const/16 v1, 0x48

    aput-byte v1, v0, v5

    const/4 v1, 0x5

    aput-byte v3, v0, v1

    const/4 v1, 0x6

    aput-byte v5, v0, v1

    const/4 v1, 0x7

    aput-byte v7, v0, v1

    const/16 v1, 0x49

    const/16 v2, 0x8

    aput-byte v1, v0, v2

    const/16 v1, 0x9

    aput-byte v7, v0, v1

    const/16 v1, 0xa

    aput-byte v4, v0, v1

    const/16 v1, 0x34

    const/16 v2, 0xb

    aput-byte v1, v0, v2

    const/16 v1, 0xc

    aput-byte v6, v0, v1

    const/16 v1, 0x66

    const/16 v2, 0xd

    aput-byte v1, v0, v2

    const/16 v1, 0x12

    const/16 v2, 0xe

    aput-byte v1, v0, v2

    const/16 v1, 0x20

    const/16 v2, 0xf

    aput-byte v1, v0, v2

    sput-object v0, Lcom/xiaomi/channel/commonutils/android/a;->cWv:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static czH([BI)Ljavax/crypto/Cipher;
    .locals 3

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string/jumbo v1, "AES"

    invoke-direct {v0, p0, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    sget-object v2, Lcom/xiaomi/channel/commonutils/android/a;->cWv:[B

    invoke-direct {v1, v2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    const-string/jumbo v2, "AES/CBC/PKCS5Padding"

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    invoke-virtual {v2, p1, v0, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    return-object v2
.end method

.method public static czI([B[B)[B
    .locals 1

    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/xiaomi/channel/commonutils/android/a;->czH([BI)Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    return-object v0
.end method

.method public static czJ([B[B)[B
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/xiaomi/channel/commonutils/android/a;->czH([BI)Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    return-object v0
.end method
