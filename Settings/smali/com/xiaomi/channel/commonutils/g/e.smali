.class public Lcom/xiaomi/channel/commonutils/g/e;
.super Ljava/lang/Object;
.source "BasicNameValuePair.java"

# interfaces
.implements Lcom/xiaomi/channel/commonutils/g/a;


# instance fields
.field private final name:Ljava/lang/String;

.field private final value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/xiaomi/channel/commonutils/g/e;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/xiaomi/channel/commonutils/g/e;->value:Ljava/lang/String;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Name may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public cAw()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/g/e;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/g/e;->value:Ljava/lang/String;

    return-object v0
.end method
