.class public Lcom/xiaomi/channel/commonutils/h/k;
.super Ljava/lang/Object;
.source "BuildSettings.java"


# static fields
.field public static final cXi:Z

.field public static final cXj:Z

.field public static final cXk:Z

.field public static final cXl:Z

.field public static final cXm:Z

.field public static cXn:Z

.field public static final cXo:Ljava/lang/String;

.field private static cXp:I

.field public static final cXq:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-boolean v0, Lcom/xiaomi/channel/commonutils/h/o;->cXz:Z

    if-nez v0, :cond_2

    const-string/jumbo v0, "@SHIP.TO.2A2FE0D7@"

    :goto_0
    sput-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v3, "2A2FE0D7"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/channel/commonutils/h/k;->cXq:Z

    sget-boolean v0, Lcom/xiaomi/channel/commonutils/h/k;->cXq:Z

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/xiaomi/channel/commonutils/h/k;->cXm:Z

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v3, "LOGABLE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/channel/commonutils/h/k;->cXi:Z

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v3, "YY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/channel/commonutils/h/k;->cXj:Z

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v3, "TEST"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/channel/commonutils/h/k;->cXn:Z

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v3, "BETA"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/channel/commonutils/h/k;->cXk:Z

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    if-nez v0, :cond_4

    :cond_1
    :goto_2
    sput-boolean v1, Lcom/xiaomi/channel/commonutils/h/k;->cXl:Z

    sput v2, Lcom/xiaomi/channel/commonutils/h/k;->cXp:I

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v1, "SANDBOX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v1, "ONEBOX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sput v2, Lcom/xiaomi/channel/commonutils/h/k;->cXp:I

    :goto_3
    return-void

    :cond_2
    const-string/jumbo v0, "ONEBOX"

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v3, "DEBUG"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/xiaomi/channel/commonutils/h/k;->cXo:Ljava/lang/String;

    const-string/jumbo v3, "RC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    goto :goto_2

    :cond_5
    const/4 v0, 0x2

    sput v0, Lcom/xiaomi/channel/commonutils/h/k;->cXp:I

    goto :goto_3

    :cond_6
    const/4 v0, 0x3

    sput v0, Lcom/xiaomi/channel/commonutils/h/k;->cXp:I

    goto :goto_3
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cBl()Z
    .locals 2

    sget v0, Lcom/xiaomi/channel/commonutils/h/k;->cXp:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static cBm()Z
    .locals 2

    sget v0, Lcom/xiaomi/channel/commonutils/h/k;->cXp:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static cBn()I
    .locals 1

    sget v0, Lcom/xiaomi/channel/commonutils/h/k;->cXp:I

    return v0
.end method

.method public static cBo(I)V
    .locals 0

    sput p0, Lcom/xiaomi/channel/commonutils/h/k;->cXp:I

    return-void
.end method
