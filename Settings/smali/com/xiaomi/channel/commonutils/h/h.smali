.class public Lcom/xiaomi/channel/commonutils/h/h;
.super Ljava/lang/Object;
.source "ScheduledJobManager.java"


# static fields
.field private static volatile cXg:Lcom/xiaomi/channel/commonutils/h/h;


# instance fields
.field private cXd:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private cXe:Landroid/content/SharedPreferences;

.field private cXf:Ljava/lang/Object;

.field private cXh:Landroid/util/SparseArray;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXd:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXh:Landroid/util/SparseArray;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXf:Ljava/lang/Object;

    const-string/jumbo v0, "mipush_extra"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXe:Landroid/content/SharedPreferences;

    return-void
.end method

.method private static cAY(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "last_job_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic cBb(Lcom/xiaomi/channel/commonutils/h/h;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXe:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic cBc(Lcom/xiaomi/channel/commonutils/h/h;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXh:Landroid/util/SparseArray;

    return-object v0
.end method

.method private cBf(Lcom/xiaomi/channel/commonutils/h/i;)Ljava/util/concurrent/ScheduledFuture;
    .locals 3

    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXf:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXh:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/xiaomi/channel/commonutils/h/i;->cBi()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledFuture;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic cBg(Lcom/xiaomi/channel/commonutils/h/h;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXf:Ljava/lang/Object;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/xiaomi/channel/commonutils/h/h;
    .locals 2

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/h;->cXg:Lcom/xiaomi/channel/commonutils/h/h;

    if-eqz v0, :cond_0

    :goto_0
    sget-object v0, Lcom/xiaomi/channel/commonutils/h/h;->cXg:Lcom/xiaomi/channel/commonutils/h/h;

    return-object v0

    :cond_0
    const-class v1, Lcom/xiaomi/channel/commonutils/h/h;

    const-class v0, Lcom/xiaomi/channel/commonutils/h/h;

    monitor-enter v0

    :try_start_0
    sget-object v0, Lcom/xiaomi/channel/commonutils/h/h;->cXg:Lcom/xiaomi/channel/commonutils/h/h;

    if-eqz v0, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/xiaomi/channel/commonutils/h/h;

    invoke-direct {v0, p0}, Lcom/xiaomi/channel/commonutils/h/h;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/channel/commonutils/h/h;->cXg:Lcom/xiaomi/channel/commonutils/h/h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public cAX(Lcom/xiaomi/channel/commonutils/h/i;II)Z
    .locals 8

    const/4 v7, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/xiaomi/channel/commonutils/h/h;->cBf(Lcom/xiaomi/channel/commonutils/h/i;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/xiaomi/channel/commonutils/h/i;->cBi()I

    move-result v1

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/h/h;->cAY(I)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Lcom/xiaomi/channel/commonutils/h/a;

    invoke-direct {v1, p0, p1, v2}, Lcom/xiaomi/channel/commonutils/h/a;-><init>(Lcom/xiaomi/channel/commonutils/h/h;Lcom/xiaomi/channel/commonutils/h/i;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXe:Landroid/content/SharedPreferences;

    const-wide/16 v4, 0x0

    invoke-interface {v3, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    sub-int v4, p2, p3

    int-to-long v4, v4

    cmp-long v4, v2, v4

    if-ltz v4, :cond_2

    move v0, v7

    :cond_2
    if-nez v0, :cond_3

    int-to-long v4, p2

    sub-long v2, v4, v2

    long-to-int p3, v2

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXd:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    int-to-long v2, p3

    int-to-long v4, p2

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXf:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXh:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/xiaomi/channel/commonutils/h/i;->cBi()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    monitor-exit v1

    return v7

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cAZ(Lcom/xiaomi/channel/commonutils/h/i;I)Z
    .locals 5

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/xiaomi/channel/commonutils/h/h;->cBf(Lcom/xiaomi/channel/commonutils/h/i;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/channel/commonutils/h/d;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/channel/commonutils/h/d;-><init>(Lcom/xiaomi/channel/commonutils/h/h;Lcom/xiaomi/channel/commonutils/h/i;)V

    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXd:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    int-to-long v2, p2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXf:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXh:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/xiaomi/channel/commonutils/h/i;->cBi()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    monitor-exit v1

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cBa(I)Z
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXf:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXh:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXh:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    move-result v0

    return v0

    :cond_0
    :try_start_1
    monitor-exit v1

    return v3

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public cBd(Ljava/lang/Runnable;I)V
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/h;->cXd:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    int-to-long v2, p2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, v2, v3, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method

.method public cBe(Ljava/lang/Runnable;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/channel/commonutils/h/h;->cBd(Ljava/lang/Runnable;I)V

    return-void
.end method

.method public cBh(Lcom/xiaomi/channel/commonutils/h/i;I)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/channel/commonutils/h/h;->cAX(Lcom/xiaomi/channel/commonutils/h/i;II)Z

    move-result v0

    return v0
.end method
