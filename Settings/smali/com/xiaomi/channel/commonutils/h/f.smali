.class Lcom/xiaomi/channel/commonutils/h/f;
.super Ljava/lang/Thread;
.source "SerializedAsyncTaskProcessor.java"


# instance fields
.field private final cXa:Ljava/util/concurrent/LinkedBlockingQueue;

.field final synthetic cXb:Lcom/xiaomi/channel/commonutils/h/n;


# direct methods
.method public constructor <init>(Lcom/xiaomi/channel/commonutils/h/n;)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    const-string/jumbo v0, "PackageProcessor"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXa:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method


# virtual methods
.method public cAW(Lcom/xiaomi/channel/commonutils/h/b;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXa:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBu(Lcom/xiaomi/channel/commonutils/h/n;)I

    move-result v0

    if-gtz v0, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    move-wide v2, v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBs(Lcom/xiaomi/channel/commonutils/h/n;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBu(Lcom/xiaomi/channel/commonutils/h/n;)I

    move-result v0

    int-to-long v0, v0

    move-wide v2, v0

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXa:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/channel/commonutils/h/b;

    invoke-static {v1, v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBy(Lcom/xiaomi/channel/commonutils/h/n;Lcom/xiaomi/channel/commonutils/h/b;)Lcom/xiaomi/channel/commonutils/h/b;

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBz(Lcom/xiaomi/channel/commonutils/h/n;)Lcom/xiaomi/channel/commonutils/h/b;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBu(Lcom/xiaomi/channel/commonutils/h/n;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBw(Lcom/xiaomi/channel/commonutils/h/n;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBx(Lcom/xiaomi/channel/commonutils/h/n;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/h/n;->cBz(Lcom/xiaomi/channel/commonutils/h/n;)Lcom/xiaomi/channel/commonutils/h/b;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/h/n;->cBx(Lcom/xiaomi/channel/commonutils/h/n;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBz(Lcom/xiaomi/channel/commonutils/h/n;)Lcom/xiaomi/channel/commonutils/h/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/channel/commonutils/h/b;->cwo()V

    iget-object v0, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/h/n;->cBx(Lcom/xiaomi/channel/commonutils/h/n;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/h/n;->cBz(Lcom/xiaomi/channel/commonutils/h/n;)Lcom/xiaomi/channel/commonutils/h/b;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/channel/commonutils/h/f;->cXb:Lcom/xiaomi/channel/commonutils/h/n;

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/h/n;->cBx(Lcom/xiaomi/channel/commonutils/h/n;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
