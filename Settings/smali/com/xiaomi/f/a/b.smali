.class public Lcom/xiaomi/f/a/b;
.super Ljava/lang/Object;
.source "AndroidDebugger.java"

# interfaces
.implements Lcom/xiaomi/smack/a/a;


# static fields
.field public static dBy:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field private dBA:Lcom/xiaomi/f/a/a;

.field private dBB:Lcom/xiaomi/smack/a;

.field private dBC:Ljava/text/SimpleDateFormat;

.field private dBD:Lcom/xiaomi/smack/e;

.field private dBz:Lcom/xiaomi/f/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x1

    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/g;->cAt()I

    move-result v1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Lcom/xiaomi/f/a/b;->dBy:Z

    return-void
.end method

.method public constructor <init>(Lcom/xiaomi/smack/e;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "hh:mm:ss aaa"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/xiaomi/f/a/b;->dBC:Ljava/text/SimpleDateFormat;

    iput-object v2, p0, Lcom/xiaomi/f/a/b;->dBD:Lcom/xiaomi/smack/e;

    iput-object v2, p0, Lcom/xiaomi/f/a/b;->dBz:Lcom/xiaomi/f/a/a;

    iput-object v2, p0, Lcom/xiaomi/f/a/b;->dBA:Lcom/xiaomi/f/a/a;

    iput-object v2, p0, Lcom/xiaomi/f/a/b;->dBB:Lcom/xiaomi/smack/a;

    const-string/jumbo v0, "[Slim] "

    iput-object v0, p0, Lcom/xiaomi/f/a/b;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/xiaomi/f/a/b;->dBD:Lcom/xiaomi/smack/e;

    invoke-direct {p0}, Lcom/xiaomi/f/a/b;->dgI()V

    return-void
.end method

.method static synthetic dgH(Lcom/xiaomi/f/a/b;)Ljava/text/SimpleDateFormat;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/f/a/b;->dBC:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method private dgI()V
    .locals 3

    new-instance v0, Lcom/xiaomi/f/a/a;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/f/a/a;-><init>(Lcom/xiaomi/f/a/b;Z)V

    iput-object v0, p0, Lcom/xiaomi/f/a/b;->dBz:Lcom/xiaomi/f/a/a;

    new-instance v0, Lcom/xiaomi/f/a/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/xiaomi/f/a/a;-><init>(Lcom/xiaomi/f/a/b;Z)V

    iput-object v0, p0, Lcom/xiaomi/f/a/b;->dBA:Lcom/xiaomi/f/a/a;

    iget-object v0, p0, Lcom/xiaomi/f/a/b;->dBD:Lcom/xiaomi/smack/e;

    iget-object v1, p0, Lcom/xiaomi/f/a/b;->dBz:Lcom/xiaomi/f/a/a;

    iget-object v2, p0, Lcom/xiaomi/f/a/b;->dBz:Lcom/xiaomi/f/a/a;

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/smack/e;->cxi(Lcom/xiaomi/smack/k;Lcom/xiaomi/smack/b/a;)V

    iget-object v0, p0, Lcom/xiaomi/f/a/b;->dBD:Lcom/xiaomi/smack/e;

    iget-object v1, p0, Lcom/xiaomi/f/a/b;->dBA:Lcom/xiaomi/f/a/a;

    iget-object v2, p0, Lcom/xiaomi/f/a/b;->dBA:Lcom/xiaomi/f/a/a;

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/smack/e;->cxE(Lcom/xiaomi/smack/k;Lcom/xiaomi/smack/b/a;)V

    new-instance v0, Lcom/xiaomi/f/a/c;

    invoke-direct {v0, p0}, Lcom/xiaomi/f/a/c;-><init>(Lcom/xiaomi/f/a/b;)V

    iput-object v0, p0, Lcom/xiaomi/f/a/b;->dBB:Lcom/xiaomi/smack/a;

    return-void
.end method

.method static synthetic dgJ(Lcom/xiaomi/f/a/b;)Lcom/xiaomi/smack/e;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/f/a/b;->dBD:Lcom/xiaomi/smack/e;

    return-object v0
.end method
