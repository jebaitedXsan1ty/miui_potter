.class public Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;
.super Ljava/lang/Object;
.source "OnlineConfigItem.java"

# interfaces
.implements Lorg/apache/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final dof:Lorg/apache/thrift/protocol/c;

.field private static final dog:Lorg/apache/thrift/protocol/i;

.field private static final doh:Lorg/apache/thrift/protocol/i;

.field private static final doi:Lorg/apache/thrift/protocol/i;

.field public static final doj:Ljava/util/Map;

.field private static final dok:Lorg/apache/thrift/protocol/i;

.field private static final dol:Lorg/apache/thrift/protocol/i;

.field private static final dom:Lorg/apache/thrift/protocol/i;

.field private static final don:Lorg/apache/thrift/protocol/i;


# instance fields
.field private __isset_bit_vector:Ljava/util/BitSet;

.field public boolValue:Z

.field public clear:Z

.field public intValue:I

.field public key:I

.field public longValue:J

.field public stringValue:Ljava/lang/String;

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/16 v8, 0xb

    const/16 v7, 0xa

    const/16 v6, 0x8

    const/4 v5, 0x2

    new-instance v0, Lorg/apache/thrift/protocol/c;

    const-string/jumbo v1, "OnlineConfigItem"

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dof:Lorg/apache/thrift/protocol/c;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "key"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->doh:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "type"

    invoke-direct {v0, v1, v6, v5}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dom:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "clear"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->doi:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "intValue"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dok:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "longValue"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v7, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->don:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "stringValue"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dog:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "boolValue"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dol:Lorg/apache/thrift/protocol/i;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;->dlZ:Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "key"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;->dmb:Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "type"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;->dlW:Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "clear"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;->dmc:Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "intValue"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;->dma:Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v7}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "longValue"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;->dlX:Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v8}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "stringValue"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;->dme:Lcom/xiaomi/xmpush/thrift/OnlineConfigItem$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "boolValue"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->doj:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->doj:Ljava/util/Map;

    const-class v1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;

    invoke-static {v1, v0}, Lorg/apache/thrift/meta_data/FieldMetaData;->esm(Ljava/lang/Class;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public cTq(Lorg/apache/thrift/protocol/a;)V
    .locals 6

    const/4 v5, 0x2

    const/16 v4, 0x8

    const/4 v3, 0x1

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erS()Lorg/apache/thrift/protocol/c;

    :goto_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esa()Lorg/apache/thrift/protocol/i;

    move-result-object v0

    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eqz v1, :cond_7

    iget-short v1, v0, Lorg/apache/thrift/protocol/i;->eXX:S

    packed-switch v1, :pswitch_data_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    :goto_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erA()V

    goto :goto_0

    :pswitch_0
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v4, :cond_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->key:I

    invoke-virtual {p0, v3}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXK(Z)V

    goto :goto_1

    :pswitch_1
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v4, :cond_1

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->type:I

    invoke-virtual {p0, v3}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXx(Z)V

    goto :goto_1

    :pswitch_2
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v5, :cond_2

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erM()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->clear:Z

    invoke-virtual {p0, v3}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXF(Z)V

    goto :goto_1

    :pswitch_3
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v4, :cond_3

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->intValue:I

    invoke-virtual {p0, v3}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXL(Z)V

    goto :goto_1

    :pswitch_4
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v2, 0xa

    if-eq v1, v2, :cond_4

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erw()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->longValue:J

    invoke-virtual {p0, v3}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXD(Z)V

    goto :goto_1

    :pswitch_5
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v2, 0xb

    if-eq v1, v2, :cond_5

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    goto :goto_1

    :pswitch_6
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v5, :cond_6

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erM()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->boolValue:Z

    invoke-virtual {p0, v3}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXM(Z)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erU()V

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXC()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public cTu(Lorg/apache/thrift/protocol/a;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXC()V

    sget-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dof:Lorg/apache/thrift/protocol/c;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erx(Lorg/apache/thrift/protocol/c;)V

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXy()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXw()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXE()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXH()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXu()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_4
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    if-nez v0, :cond_6

    :cond_0
    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXv()Z

    move-result v0

    if-nez v0, :cond_7

    :goto_6
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erQ()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erL()V

    return-void

    :cond_1
    sget-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->doh:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->key:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dom:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->type:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->doi:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->clear:Z

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->eru(Z)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dok:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->intValue:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_3

    :cond_5
    sget-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->don:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->longValue:J

    invoke-virtual {p1, v0, v1}, Lorg/apache/thrift/protocol/a;->erF(J)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_4

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXA()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dog:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_5

    :cond_7
    sget-object v0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->dol:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->boolValue:Z

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->eru(Z)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_6
.end method

.method public cXA()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cXB(Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;)I
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXy()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXy()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXy()Z

    move-result v0

    if-nez v0, :cond_9

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXw()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXw()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXw()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXE()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXE()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXE()Z

    move-result v0

    if-nez v0, :cond_d

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXH()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXH()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXH()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXu()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXu()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_10

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXu()Z

    move-result v0

    if-nez v0, :cond_11

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXA()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXA()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_12

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXA()Z

    move-result v0

    if-nez v0, :cond_13

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXv()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXv()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_14

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXv()Z

    move-result v0

    if-nez v0, :cond_15

    :cond_6
    return v4

    :cond_7
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_8
    return v0

    :cond_9
    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->key:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->key:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_a
    return v0

    :cond_b
    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->type:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->type:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_c
    return v0

    :cond_d
    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->clear:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->clear:Z

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esy(ZZ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    :cond_e
    return v0

    :cond_f
    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->intValue:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->intValue:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    :cond_10
    return v0

    :cond_11
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->longValue:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->longValue:J

    invoke-static {v0, v1, v2, v3}, Lorg/apache/thrift/b;->esF(JJ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    :cond_12
    return v0

    :cond_13
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    :cond_14
    return v0

    :cond_15
    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->boolValue:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->boolValue:Z

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esy(ZZ)I

    move-result v0

    if-eqz v0, :cond_6

    return v0
.end method

.method public cXC()V
    .locals 0

    return-void
.end method

.method public cXD(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cXE()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cXF(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cXG()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    return-object v0
.end method

.method public cXH()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cXI()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->intValue:I

    return v0
.end method

.method public cXJ()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->boolValue:Z

    return v0
.end method

.method public cXK(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cXL(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cXM(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cXN(Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;)Z
    .locals 5

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXy()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXy()Z

    move-result v1

    if-eqz v0, :cond_3

    :cond_0
    if-nez v0, :cond_7

    :cond_1
    return v4

    :cond_2
    return v4

    :cond_3
    if-nez v1, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXw()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXw()Z

    move-result v1

    if-eqz v0, :cond_8

    :cond_5
    if-nez v0, :cond_c

    :cond_6
    return v4

    :cond_7
    if-eqz v1, :cond_1

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->key:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->key:I

    if-eq v0, v1, :cond_4

    return v4

    :cond_8
    if-nez v1, :cond_5

    :cond_9
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXE()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXE()Z

    move-result v1

    if-eqz v0, :cond_d

    :cond_a
    if-nez v0, :cond_11

    :cond_b
    return v4

    :cond_c
    if-eqz v1, :cond_6

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->type:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->type:I

    if-eq v0, v1, :cond_9

    return v4

    :cond_d
    if-nez v1, :cond_a

    :cond_e
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXH()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXH()Z

    move-result v1

    if-eqz v0, :cond_12

    :cond_f
    if-nez v0, :cond_16

    :cond_10
    return v4

    :cond_11
    if-eqz v1, :cond_b

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->clear:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->clear:Z

    if-eq v0, v1, :cond_e

    return v4

    :cond_12
    if-nez v1, :cond_f

    :cond_13
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXu()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXu()Z

    move-result v1

    if-eqz v0, :cond_17

    :cond_14
    if-nez v0, :cond_1b

    :cond_15
    return v4

    :cond_16
    if-eqz v1, :cond_10

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->intValue:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->intValue:I

    if-eq v0, v1, :cond_13

    return v4

    :cond_17
    if-nez v1, :cond_14

    :cond_18
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXA()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXA()Z

    move-result v1

    if-eqz v0, :cond_1c

    :cond_19
    if-nez v0, :cond_20

    :cond_1a
    return v4

    :cond_1b
    if-eqz v1, :cond_15

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->longValue:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->longValue:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_18

    return v4

    :cond_1c
    if-nez v1, :cond_19

    :cond_1d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXv()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXv()Z

    move-result v1

    if-eqz v0, :cond_21

    :cond_1e
    if-nez v0, :cond_23

    :cond_1f
    return v4

    :cond_20
    if-eqz v1, :cond_1a

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d

    return v4

    :cond_21
    if-nez v1, :cond_1e

    :cond_22
    const/4 v0, 0x1

    return v0

    :cond_23
    if-eqz v1, :cond_1f

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->boolValue:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->boolValue:Z

    if-eq v0, v1, :cond_22

    return v4
.end method

.method public cXu()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cXv()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cXw()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cXx(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cXy()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cXz()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->longValue:J

    return-wide v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXB(Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;

    if-nez v0, :cond_1

    return v1

    :cond_0
    return v1

    :cond_1
    check-cast p1, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXN(Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;)Z

    move-result v0

    return v0
.end method

.method public getKey()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->key:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "OnlineConfigItem("

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXy()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXw()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXE()Z

    move-result v3

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXH()Z

    move-result v3

    if-nez v3, :cond_5

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXu()Z

    move-result v3

    if-nez v3, :cond_7

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXA()Z

    move-result v3

    if-nez v3, :cond_9

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->cXv()Z

    move-result v1

    if-nez v1, :cond_c

    :goto_6
    const-string/jumbo v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "key:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->key:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    :goto_7
    const-string/jumbo v0, "type:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->type:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_1

    :cond_2
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_3
    if-eqz v0, :cond_4

    :goto_8
    const-string/jumbo v0, "clear:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->clear:Z

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_2

    :cond_4
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    :cond_5
    if-eqz v0, :cond_6

    :goto_9
    const-string/jumbo v0, "intValue:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->intValue:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_3

    :cond_6
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    :cond_7
    if-eqz v0, :cond_8

    :goto_a
    const-string/jumbo v0, "longValue:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->longValue:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_4

    :cond_8
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_a

    :cond_9
    if-eqz v0, :cond_a

    :goto_b
    const-string/jumbo v0, "stringValue:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->stringValue:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_c
    move v0, v1

    goto/16 :goto_5

    :cond_a
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_b

    :cond_b
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_c

    :cond_c
    if-eqz v0, :cond_d

    :goto_d
    const-string/jumbo v0, "boolValue:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/OnlineConfigItem;->boolValue:Z

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_d
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_d
.end method
