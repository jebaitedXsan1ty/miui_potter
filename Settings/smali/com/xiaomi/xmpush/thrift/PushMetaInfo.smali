.class public Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
.super Ljava/lang/Object;
.source "PushMetaInfo.java"

# interfaces
.implements Lorg/apache/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final drb:Lorg/apache/thrift/protocol/i;

.field private static final drc:Lorg/apache/thrift/protocol/c;

.field private static final drd:Lorg/apache/thrift/protocol/i;

.field private static final dre:Lorg/apache/thrift/protocol/i;

.field public static final drf:Ljava/util/Map;

.field private static final drg:Lorg/apache/thrift/protocol/i;

.field private static final drh:Lorg/apache/thrift/protocol/i;

.field private static final dri:Lorg/apache/thrift/protocol/i;

.field private static final drj:Lorg/apache/thrift/protocol/i;

.field private static final drk:Lorg/apache/thrift/protocol/i;

.field private static final drl:Lorg/apache/thrift/protocol/i;

.field private static final drm:Lorg/apache/thrift/protocol/i;

.field private static final drn:Lorg/apache/thrift/protocol/i;

.field private static final dro:Lorg/apache/thrift/protocol/i;


# instance fields
.field private __isset_bit_vector:Ljava/util/BitSet;

.field public description:Ljava/lang/String;

.field public extra:Ljava/util/Map;

.field public id:Ljava/lang/String;

.field public ignoreRegInfo:Z

.field public internal:Ljava/util/Map;

.field public messageTs:J

.field public notifyId:I

.field public notifyType:I

.field public passThrough:I

.field public title:Ljava/lang/String;

.field public topic:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x1

    const/16 v8, 0xd

    const/16 v5, 0x8

    const/4 v7, 0x2

    const/16 v6, 0xb

    new-instance v0, Lorg/apache/thrift/protocol/c;

    const-string/jumbo v1, "PushMetaInfo"

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drc:Lorg/apache/thrift/protocol/c;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "id"

    invoke-direct {v0, v1, v6, v9}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drb:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "messageTs"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v7}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drh:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "topic"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->dri:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "title"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drn:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "description"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drk:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "notifyType"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drl:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "url"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drj:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "passThrough"

    invoke-direct {v0, v1, v5, v5}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->dro:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "notifyId"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drg:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "extra"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v8, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->dre:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "internal"

    invoke-direct {v0, v1, v8, v6}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drd:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "ignoreRegInfo"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v7, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drm:Lorg/apache/thrift/protocol/i;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxR:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "id"

    invoke-direct {v2, v4, v9, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxL:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    const/16 v4, 0xa

    invoke-direct {v3, v4}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "messageTs"

    invoke-direct {v2, v4, v9, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxU:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "topic"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxM:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "title"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxK:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "description"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxJ:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "notifyType"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxS:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "url"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxP:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "passThrough"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxN:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "notifyId"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxW:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/MapMetaData;

    new-instance v4, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v4, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    new-instance v5, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v5, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    invoke-direct {v3, v8, v4, v5}, Lorg/apache/thrift/meta_data/MapMetaData;-><init>(BLorg/apache/thrift/meta_data/FieldValueMetaData;Lorg/apache/thrift/meta_data/FieldValueMetaData;)V

    const-string/jumbo v4, "extra"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxQ:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/MapMetaData;

    new-instance v4, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v4, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    new-instance v5, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v5, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    invoke-direct {v3, v8, v4, v5}, Lorg/apache/thrift/meta_data/MapMetaData;-><init>(BLorg/apache/thrift/meta_data/FieldValueMetaData;Lorg/apache/thrift/meta_data/FieldValueMetaData;)V

    const-string/jumbo v4, "internal"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;->dxV:Lcom/xiaomi/xmpush/thrift/PushMetaInfo$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v7}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "ignoreRegInfo"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drf:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drf:Ljava/util/Map;

    const-class v1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    invoke-static {v1, v0}, Lorg/apache/thrift/meta_data/FieldMetaData;->esm(Ljava/lang/Class;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    return-void
.end method

.method public constructor <init>(Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZP()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    iget-wide v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    iput-wide v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZo()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZh()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZl()Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    iget v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZi()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_4
    iget v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    iget v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZF()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_5
    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZD()Z

    move-result v0

    if-nez v0, :cond_7

    :goto_6
    iget-boolean v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    iput-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    return-void

    :cond_0
    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    goto :goto_3

    :cond_4
    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    goto :goto_4

    :cond_5
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    iput-object v2, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    goto :goto_5

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_7
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_8

    iput-object v2, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    goto :goto_6

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8
.end method


# virtual methods
.method public cTq(Lorg/apache/thrift/protocol/a;)V
    .locals 10

    const/16 v9, 0x8

    const/4 v8, 0x2

    const/4 v1, 0x0

    const/16 v7, 0xb

    const/4 v6, 0x1

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erS()Lorg/apache/thrift/protocol/c;

    :goto_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esa()Lorg/apache/thrift/protocol/i;

    move-result-object v0

    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eqz v2, :cond_e

    iget-short v2, v0, Lorg/apache/thrift/protocol/i;->eXX:S

    packed-switch v2, :pswitch_data_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    :goto_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erA()V

    goto :goto_0

    :pswitch_0
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v7, :cond_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    goto :goto_1

    :pswitch_1
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xa

    if-eq v2, v3, :cond_1

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erw()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    invoke-virtual {p0, v6}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZI(Z)V

    goto :goto_1

    :pswitch_2
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v7, :cond_2

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v7, :cond_3

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v7, :cond_4

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    goto :goto_1

    :pswitch_5
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v9, :cond_5

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    invoke-virtual {p0, v6}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZu(Z)V

    goto :goto_1

    :pswitch_6
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v7, :cond_6

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_7
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v9, :cond_7

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    invoke-virtual {p0, v6}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZK(Z)V

    goto/16 :goto_1

    :pswitch_8
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v9, :cond_8

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esf()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    invoke-virtual {p0, v6}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZx(Z)V

    goto/16 :goto_1

    :pswitch_9
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xd

    if-eq v2, v3, :cond_9

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erV()Lorg/apache/thrift/protocol/d;

    move-result-object v2

    new-instance v0, Ljava/util/HashMap;

    iget v3, v2, Lorg/apache/thrift/protocol/d;->eXG:I

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    move v0, v1

    :goto_2
    iget v3, v2, Lorg/apache/thrift/protocol/d;->eXG:I

    if-lt v0, v3, :cond_a

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erP()V

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_a
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xd

    if-eq v2, v3, :cond_b

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_b
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erV()Lorg/apache/thrift/protocol/d;

    move-result-object v2

    new-instance v0, Ljava/util/HashMap;

    iget v3, v2, Lorg/apache/thrift/protocol/d;->eXG:I

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    move v0, v1

    :goto_3
    iget v3, v2, Lorg/apache/thrift/protocol/d;->eXG:I

    if-lt v0, v3, :cond_c

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erP()V

    goto/16 :goto_1

    :cond_c
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :pswitch_b
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v8, :cond_d

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_d
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erM()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    invoke-virtual {p0, v6}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZr(Z)V

    goto/16 :goto_1

    :cond_e
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erU()V

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZS()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZQ()V

    return-void

    :cond_f
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'messageTs\' was not found in serialized data! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public cTu(Lorg/apache/thrift/protocol/a;)V
    .locals 4

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZQ()V

    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drc:Lorg/apache/thrift/protocol/c;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erx(Lorg/apache/thrift/protocol/c;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    if-nez v0, :cond_6

    :goto_0
    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drh:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    invoke-virtual {p1, v0, v1}, Lorg/apache/thrift/protocol/a;->erF(J)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    if-nez v0, :cond_7

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    if-nez v0, :cond_8

    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    if-nez v0, :cond_9

    :cond_2
    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZA()Z

    move-result v0

    if-nez v0, :cond_a

    :goto_4
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    if-nez v0, :cond_b

    :cond_3
    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZj()Z

    move-result v0

    if-nez v0, :cond_c

    :goto_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZT()Z

    move-result v0

    if-nez v0, :cond_d

    :goto_7
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    if-nez v0, :cond_e

    :cond_4
    :goto_8
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    if-nez v0, :cond_10

    :cond_5
    :goto_9
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZn()Z

    move-result v0

    if-nez v0, :cond_12

    :goto_a
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erQ()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erL()V

    return-void

    :cond_6
    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drb:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZo()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->dri:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZh()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drn:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_2

    :cond_9
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZl()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drk:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_3

    :cond_a
    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drl:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_4

    :cond_b
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZi()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drj:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_5

    :cond_c
    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->dro:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_6

    :cond_d
    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drg:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erW(I)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_7

    :cond_e
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZF()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->dre:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    new-instance v0, Lorg/apache/thrift/protocol/d;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v3, v3, v1}, Lorg/apache/thrift/protocol/d;-><init>(BBI)V

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->esc(Lorg/apache/thrift/protocol/d;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erD()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_8

    :cond_f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    goto :goto_b

    :cond_10
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZD()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drd:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    new-instance v0, Lorg/apache/thrift/protocol/d;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v3, v3, v1}, Lorg/apache/thrift/protocol/d;-><init>(BBI)V

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->esc(Lorg/apache/thrift/protocol/d;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erD()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_9

    :cond_11
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    goto :goto_c

    :cond_12
    sget-object v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->drm:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->eru(Z)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_a
.end method

.method public cZA()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cZB(I)Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
    .locals 1

    iput p1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZK(Z)V

    return-object p0
.end method

.method public cZC(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    goto :goto_0
.end method

.method public cZD()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cZE()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    return-object v0
.end method

.method public cZF()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cZG(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    return-object p0
.end method

.method public cZH()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    return v0
.end method

.method public cZI(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cZJ(Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)I
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZP()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZP()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZP()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZS()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZS()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZS()Z

    move-result v0

    if-nez v0, :cond_10

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZo()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZo()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZo()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZh()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZh()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_13

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZh()Z

    move-result v0

    if-nez v0, :cond_14

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZl()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZl()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZl()Z

    move-result v0

    if-nez v0, :cond_16

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZA()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZA()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_17

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZA()Z

    move-result v0

    if-nez v0, :cond_18

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZi()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZi()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_19

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZi()Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZj()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZj()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1b

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZj()Z

    move-result v0

    if-nez v0, :cond_1c

    :cond_7
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZT()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZT()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1d

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZT()Z

    move-result v0

    if-nez v0, :cond_1e

    :cond_8
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZF()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZF()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_1f

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZF()Z

    move-result v0

    if-nez v0, :cond_20

    :cond_9
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZD()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZD()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_21

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZD()Z

    move-result v0

    if-nez v0, :cond_22

    :cond_a
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZn()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZn()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_23

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZn()Z

    move-result v0

    if-nez v0, :cond_24

    :cond_b
    return v4

    :cond_c
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_d
    return v0

    :cond_e
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_f
    return v0

    :cond_10
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    invoke-static {v0, v1, v2, v3}, Lorg/apache/thrift/b;->esF(JJ)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_11
    return v0

    :cond_12
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    :cond_13
    return v0

    :cond_14
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    :cond_15
    return v0

    :cond_16
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    :cond_17
    return v0

    :cond_18
    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    :cond_19
    return v0

    :cond_1a
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    :cond_1b
    return v0

    :cond_1c
    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_7

    return v0

    :cond_1d
    return v0

    :cond_1e
    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esD(II)I

    move-result v0

    if-eqz v0, :cond_8

    return v0

    :cond_1f
    return v0

    :cond_20
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esx(Ljava/util/Map;Ljava/util/Map;)I

    move-result v0

    if-eqz v0, :cond_9

    return v0

    :cond_21
    return v0

    :cond_22
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esx(Ljava/util/Map;Ljava/util/Map;)I

    move-result v0

    if-eqz v0, :cond_a

    return v0

    :cond_23
    return v0

    :cond_24
    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esy(ZZ)I

    move-result v0

    if-eqz v0, :cond_b

    return v0
.end method

.method public cZK(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cZL()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    return v0
.end method

.method public cZM()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    return-object v0
.end method

.method public cZN()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    return-object v0
.end method

.method public cZO()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    return-object v0
.end method

.method public cZP()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cZQ()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'id\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cZR(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    return-object p0
.end method

.method public cZS()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cZT()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cZU(Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZP()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZP()Z

    move-result v1

    if-eqz v0, :cond_3

    :cond_0
    if-nez v0, :cond_5

    :cond_1
    return v4

    :cond_2
    return v4

    :cond_3
    if-nez v1, :cond_0

    :cond_4
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    return v4

    :cond_5
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    return v4

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZo()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZo()Z

    move-result v1

    if-eqz v0, :cond_9

    :cond_7
    if-nez v0, :cond_d

    :cond_8
    return v4

    :cond_9
    if-nez v1, :cond_7

    :cond_a
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZh()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZh()Z

    move-result v1

    if-eqz v0, :cond_e

    :cond_b
    if-nez v0, :cond_12

    :cond_c
    return v4

    :cond_d
    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    return v4

    :cond_e
    if-nez v1, :cond_b

    :cond_f
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZl()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZl()Z

    move-result v1

    if-eqz v0, :cond_13

    :cond_10
    if-nez v0, :cond_17

    :cond_11
    return v4

    :cond_12
    if-eqz v1, :cond_c

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    return v4

    :cond_13
    if-nez v1, :cond_10

    :cond_14
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZA()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZA()Z

    move-result v1

    if-eqz v0, :cond_18

    :cond_15
    if-nez v0, :cond_1c

    :cond_16
    return v4

    :cond_17
    if-eqz v1, :cond_11

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    return v4

    :cond_18
    if-nez v1, :cond_15

    :cond_19
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZi()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZi()Z

    move-result v1

    if-eqz v0, :cond_1d

    :cond_1a
    if-nez v0, :cond_21

    :cond_1b
    return v4

    :cond_1c
    if-eqz v1, :cond_16

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    if-eq v0, v1, :cond_19

    return v4

    :cond_1d
    if-nez v1, :cond_1a

    :cond_1e
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZj()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZj()Z

    move-result v1

    if-eqz v0, :cond_22

    :cond_1f
    if-nez v0, :cond_26

    :cond_20
    return v4

    :cond_21
    if-eqz v1, :cond_1b

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    return v4

    :cond_22
    if-nez v1, :cond_1f

    :cond_23
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZT()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZT()Z

    move-result v1

    if-eqz v0, :cond_27

    :cond_24
    if-nez v0, :cond_2b

    :cond_25
    return v4

    :cond_26
    if-eqz v1, :cond_20

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    if-eq v0, v1, :cond_23

    return v4

    :cond_27
    if-nez v1, :cond_24

    :cond_28
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZF()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZF()Z

    move-result v1

    if-eqz v0, :cond_2c

    :cond_29
    if-nez v0, :cond_30

    :cond_2a
    return v4

    :cond_2b
    if-eqz v1, :cond_25

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    iget v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    if-eq v0, v1, :cond_28

    return v4

    :cond_2c
    if-nez v1, :cond_29

    :cond_2d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZD()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZD()Z

    move-result v1

    if-eqz v0, :cond_31

    :cond_2e
    if-nez v0, :cond_35

    :cond_2f
    return v4

    :cond_30
    if-eqz v1, :cond_2a

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2d

    return v4

    :cond_31
    if-nez v1, :cond_2e

    :cond_32
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZn()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZn()Z

    move-result v1

    if-eqz v0, :cond_36

    :cond_33
    if-nez v0, :cond_38

    :cond_34
    return v4

    :cond_35
    if-eqz v1, :cond_2f

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_32

    return v4

    :cond_36
    if-nez v1, :cond_33

    :cond_37
    return v5

    :cond_38
    if-eqz v1, :cond_34

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    if-eq v0, v1, :cond_37

    return v4
.end method

.method public cZh()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cZi()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cZj()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cZk(I)Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
    .locals 1

    iput p1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZu(Z)V

    return-object p0
.end method

.method public cZl()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cZm(I)Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
    .locals 1

    iput p1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZx(Z)V

    return-object p0
.end method

.method public cZn()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public cZo()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cZp(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    return-object p0
.end method

.method public cZq()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    return-wide v0
.end method

.method public cZr(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cZs(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    return-object p0
.end method

.method public cZt()Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
    .locals 1

    new-instance v0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    invoke-direct {v0, p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;-><init>(Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)V

    return-object v0
.end method

.method public cZu(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cZv(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    goto :goto_0
.end method

.method public cZw()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    return v0
.end method

.method public cZx(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cZy(Ljava/util/Map;)Lcom/xiaomi/xmpush/thrift/PushMetaInfo;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    return-object p0
.end method

.method public cZz()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZJ(Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    if-nez v0, :cond_1

    return v1

    :cond_0
    return v1

    :cond_1
    check-cast p1, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZU(Lcom/xiaomi/xmpush/thrift/PushMetaInfo;)Z

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "PushMetaInfo("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "messageTs:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->messageTs:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZo()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZh()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZl()Z

    move-result v1

    if-nez v1, :cond_5

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZA()Z

    move-result v1

    if-nez v1, :cond_7

    :goto_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZi()Z

    move-result v1

    if-nez v1, :cond_8

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZj()Z

    move-result v1

    if-nez v1, :cond_a

    :goto_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZT()Z

    move-result v1

    if-nez v1, :cond_b

    :goto_7
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZF()Z

    move-result v1

    if-nez v1, :cond_c

    :goto_8
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZD()Z

    move-result v1

    if-nez v1, :cond_e

    :goto_9
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->cZn()Z

    move-result v1

    if-nez v1, :cond_10

    :goto_a
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "topic:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->topic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "title:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_5
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "description:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_6
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_7
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "notifyType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    :cond_8
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "url:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    :cond_9
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    :cond_a
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "passThrough:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->passThrough:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_b
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "notifyId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->notifyId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_c
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "extra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->extra:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    :cond_d
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    :cond_e
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "internal:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->internal:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_f
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_10
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "ignoreRegInfo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/xmpush/thrift/PushMetaInfo;->ignoreRegInfo:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/16 :goto_a
.end method
