.class public final enum Lcom/xiaomi/xmpush/thrift/ActionType;
.super Ljava/lang/Enum;
.source "ActionType.java"


# static fields
.field public static final enum dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAH:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAI:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAJ:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAK:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAL:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAM:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAN:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAO:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAP:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAQ:Lcom/xiaomi/xmpush/thrift/ActionType;

.field public static final enum dAR:Lcom/xiaomi/xmpush/thrift/ActionType;

.field private static final synthetic dAS:[Lcom/xiaomi/xmpush/thrift/ActionType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "Registration"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAQ:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "UnRegistration"

    invoke-direct {v0, v1, v4, v5}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAO:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "Subscription"

    invoke-direct {v0, v1, v5, v6}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAH:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "UnSubscription"

    invoke-direct {v0, v1, v6, v7}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAR:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "SendMessage"

    invoke-direct {v0, v1, v7, v8}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAI:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "AckMessage"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAP:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "SetConfig"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAL:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "ReportFeedback"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAM:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "Notification"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "Command"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAJ:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "MultiConnectionBroadcast"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAN:Lcom/xiaomi/xmpush/thrift/ActionType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    const-string/jumbo v1, "MultiConnectionResult"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/xiaomi/xmpush/thrift/ActionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAK:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/xiaomi/xmpush/thrift/ActionType;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAQ:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAO:Lcom/xiaomi/xmpush/thrift/ActionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAH:Lcom/xiaomi/xmpush/thrift/ActionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAR:Lcom/xiaomi/xmpush/thrift/ActionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAI:Lcom/xiaomi/xmpush/thrift/ActionType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAP:Lcom/xiaomi/xmpush/thrift/ActionType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAL:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAM:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAJ:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAN:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ActionType;->dAK:Lcom/xiaomi/xmpush/thrift/ActionType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAS:[Lcom/xiaomi/xmpush/thrift/ActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/xiaomi/xmpush/thrift/ActionType;->value:I

    return-void
.end method

.method public static dfY(I)Lcom/xiaomi/xmpush/thrift/ActionType;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAQ:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAO:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAH:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_3
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAR:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_4
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAI:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_5
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAP:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_6
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAL:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_7
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAM:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_8
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAG:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_9
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAJ:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_a
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAN:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    :pswitch_b
    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAK:Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/ActionType;
    .locals 1

    const-class v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/xmpush/thrift/ActionType;
    .locals 1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/ActionType;->dAS:[Lcom/xiaomi/xmpush/thrift/ActionType;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/xmpush/thrift/ActionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/xmpush/thrift/ActionType;->value:I

    return v0
.end method
