.class public Lcom/xiaomi/xmpush/thrift/Target;
.super Ljava/lang/Object;
.source "Target.java"

# interfaces
.implements Lorg/apache/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final drp:Lorg/apache/thrift/protocol/c;

.field private static final drq:Lorg/apache/thrift/protocol/i;

.field private static final drr:Lorg/apache/thrift/protocol/i;

.field public static final drs:Ljava/util/Map;

.field private static final drt:Lorg/apache/thrift/protocol/i;

.field private static final dru:Lorg/apache/thrift/protocol/i;

.field private static final drv:Lorg/apache/thrift/protocol/i;


# instance fields
.field private __isset_bit_vector:Ljava/util/BitSet;

.field public channelId:J

.field public isPreview:Z

.field public resource:Ljava/lang/String;

.field public server:Ljava/lang/String;

.field public userId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v4, 0xa

    const/4 v7, 0x1

    const/16 v6, 0xb

    const/4 v5, 0x2

    new-instance v0, Lorg/apache/thrift/protocol/c;

    const-string/jumbo v1, "Target"

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drp:Lorg/apache/thrift/protocol/c;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "channelId"

    invoke-direct {v0, v1, v4, v7}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/Target;->dru:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "userId"

    invoke-direct {v0, v1, v6, v5}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drt:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "server"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drq:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "resource"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drv:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "isPreview"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drr:Lorg/apache/thrift/protocol/i;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/xiaomi/xmpush/thrift/Target$_Fields;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lcom/xiaomi/xmpush/thrift/Target$_Fields;->dlP:Lcom/xiaomi/xmpush/thrift/Target$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v4}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "channelId"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/Target$_Fields;->dlS:Lcom/xiaomi/xmpush/thrift/Target$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "userId"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/Target$_Fields;->dlQ:Lcom/xiaomi/xmpush/thrift/Target$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "server"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/Target$_Fields;->dlT:Lcom/xiaomi/xmpush/thrift/Target$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v6}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "resource"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/Target$_Fields;->dlU:Lcom/xiaomi/xmpush/thrift/Target$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "isPreview"

    invoke-direct {v2, v4, v5, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drs:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drs:Ljava/util/Map;

    const-class v1, Lcom/xiaomi/xmpush/thrift/Target;

    invoke-static {v1, v0}, Lorg/apache/thrift/meta_data/FieldMetaData;->esm(Ljava/lang/Class;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->__isset_bit_vector:Ljava/util/BitSet;

    const-wide/16 v0, 0x5

    iput-wide v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    const-string/jumbo v0, "xiaomi.com"

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->isPreview:Z

    return-void
.end method


# virtual methods
.method public cTq(Lorg/apache/thrift/protocol/a;)V
    .locals 5

    const/4 v4, 0x1

    const/16 v3, 0xb

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erS()Lorg/apache/thrift/protocol/c;

    :goto_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esa()Lorg/apache/thrift/protocol/i;

    move-result-object v0

    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eqz v1, :cond_5

    iget-short v1, v0, Lorg/apache/thrift/protocol/i;->eXX:S

    packed-switch v1, :pswitch_data_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    :goto_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erA()V

    goto :goto_0

    :pswitch_0
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erw()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    invoke-virtual {p0, v4}, Lcom/xiaomi/xmpush/thrift/Target;->cZV(Z)V

    goto :goto_1

    :pswitch_1
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v3, :cond_1

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v3, :cond_2

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v1, v3, :cond_3

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    iget-byte v1, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erM()Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->isPreview:Z

    invoke-virtual {p0, v4}, Lcom/xiaomi/xmpush/thrift/Target;->cZY(Z)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erU()V

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->dab()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->dac()V

    return-void

    :cond_6
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'channelId\' was not found in serialized data! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public cTu(Lorg/apache/thrift/protocol/a;)V
    .locals 2

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->dac()V

    sget-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drp:Lorg/apache/thrift/protocol/c;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erx(Lorg/apache/thrift/protocol/c;)V

    sget-object v0, Lcom/xiaomi/xmpush/thrift/Target;->dru:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    invoke-virtual {p1, v0, v1}, Lorg/apache/thrift/protocol/a;->erF(J)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    if-nez v0, :cond_2

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    if-nez v0, :cond_3

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    if-nez v0, :cond_4

    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->daa()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_3
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erQ()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erL()V

    return-void

    :cond_2
    sget-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drt:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZW()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drq:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZX()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drv:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/xiaomi/xmpush/thrift/Target;->drr:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->isPreview:Z

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->eru(Z)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_3
.end method

.method public cZV(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cZW()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cZX()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cZY(Z)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(IZ)V

    return-void
.end method

.method public cZZ(Lcom/xiaomi/xmpush/thrift/Target;)I
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->dab()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/Target;->dab()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->dab()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->dad()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/Target;->dad()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->dad()Z

    move-result v0

    if-nez v0, :cond_9

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZW()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/Target;->cZW()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZW()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZX()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/Target;->cZX()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZX()Z

    move-result v0

    if-nez v0, :cond_d

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->daa()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/Target;->daa()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->daa()Z

    move-result v0

    if-nez v0, :cond_f

    :cond_4
    return v4

    :cond_5
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_6
    return v0

    :cond_7
    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    invoke-static {v0, v1, v2, v3}, Lorg/apache/thrift/b;->esF(JJ)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_8
    return v0

    :cond_9
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_a
    return v0

    :cond_b
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    :cond_c
    return v0

    :cond_d
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    :cond_e
    return v0

    :cond_f
    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->isPreview:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/Target;->isPreview:Z

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esy(ZZ)I

    move-result v0

    if-eqz v0, :cond_4

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/Target;->cZZ(Lcom/xiaomi/xmpush/thrift/Target;)I

    move-result v0

    return v0
.end method

.method public daa()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public dab()Z
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->__isset_bit_vector:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public dac()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'userId\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dad()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dae(Lcom/xiaomi/xmpush/thrift/Target;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    iget-wide v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    iget-wide v2, p1, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    return v4

    :cond_0
    return v4

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->dad()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/Target;->dad()Z

    move-result v1

    if-eqz v0, :cond_4

    :cond_2
    if-nez v0, :cond_8

    :cond_3
    return v4

    :cond_4
    if-nez v1, :cond_2

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZW()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/Target;->cZW()Z

    move-result v1

    if-eqz v0, :cond_9

    :cond_6
    if-nez v0, :cond_d

    :cond_7
    return v4

    :cond_8
    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    return v4

    :cond_9
    if-nez v1, :cond_6

    :cond_a
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZX()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/Target;->cZX()Z

    move-result v1

    if-eqz v0, :cond_e

    :cond_b
    if-nez v0, :cond_12

    :cond_c
    return v4

    :cond_d
    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    return v4

    :cond_e
    if-nez v1, :cond_b

    :cond_f
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->daa()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/Target;->daa()Z

    move-result v1

    if-eqz v0, :cond_13

    :cond_10
    if-nez v0, :cond_15

    :cond_11
    return v4

    :cond_12
    if-eqz v1, :cond_c

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    return v4

    :cond_13
    if-nez v1, :cond_10

    :cond_14
    return v5

    :cond_15
    if-eqz v1, :cond_11

    iget-boolean v0, p0, Lcom/xiaomi/xmpush/thrift/Target;->isPreview:Z

    iget-boolean v1, p1, Lcom/xiaomi/xmpush/thrift/Target;->isPreview:Z

    if-eq v0, v1, :cond_14

    return v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/xiaomi/xmpush/thrift/Target;

    if-nez v0, :cond_1

    return v1

    :cond_0
    return v1

    :cond_1
    check-cast p1, Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/Target;->dae(Lcom/xiaomi/xmpush/thrift/Target;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Target("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "channelId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/xiaomi/xmpush/thrift/Target;->channelId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "userId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/Target;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZW()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->cZX()Z

    move-result v1

    if-nez v1, :cond_3

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/Target;->daa()Z

    move-result v1

    if-nez v1, :cond_5

    :goto_3
    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "server:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/Target;->server:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "resource:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/Target;->resource:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const-string/jumbo v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_5
    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "isPreview:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/xiaomi/xmpush/thrift/Target;->isPreview:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto :goto_3
.end method
