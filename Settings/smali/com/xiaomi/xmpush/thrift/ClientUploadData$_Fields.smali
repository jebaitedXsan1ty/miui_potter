.class public final enum Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;
.super Ljava/lang/Enum;
.source "ClientUploadData.java"


# static fields
.field private static final dAA:Ljava/util/Map;

.field public static final enum dAB:Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

.field private static final synthetic dAC:[Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;


# instance fields
.field private final _fieldName:Ljava/lang/String;

.field private final _thriftId:S


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    const-string/jumbo v1, "UPLOAD_DATA_ITEMS"

    const-string/jumbo v2, "uploadDataItems"

    invoke-direct {v0, v1, v3, v4, v2}, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->dAB:Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    new-array v0, v4, [Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->dAB:Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    aput-object v1, v0, v3

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->dAC:[Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->dAA:Ljava/util/Map;

    const-class v0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    sget-object v2, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->dAA:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->cTY()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ISLjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    int-to-short v0, p3

    iput-short v0, p0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->_thriftId:S

    iput-object p4, p0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->_fieldName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;
    .locals 1

    const-class v0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;
    .locals 1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->dAC:[Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;

    return-object v0
.end method


# virtual methods
.method public cTY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/ClientUploadData$_Fields;->_fieldName:Ljava/lang/String;

    return-object v0
.end method
