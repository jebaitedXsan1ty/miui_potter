.class public final enum Lcom/xiaomi/xmpush/thrift/NotificationType;
.super Ljava/lang/Enum;
.source "NotificationType.java"


# static fields
.field public static final enum dyK:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyL:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyM:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyN:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyO:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyP:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyQ:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyR:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyS:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyT:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyU:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyV:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyW:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyX:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyY:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dyZ:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzA:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzB:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzC:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzD:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzE:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzF:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzG:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzH:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dza:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzb:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzc:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzd:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dze:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzf:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzg:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzh:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzi:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field private static final synthetic dzj:[Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzk:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzl:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzm:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzn:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzo:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static dzp:Ljava/util/Map;

.field public static final enum dzq:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzr:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzs:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzt:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzu:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzv:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzw:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzx:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzy:Lcom/xiaomi/xmpush/thrift/NotificationType;

.field public static final enum dzz:Lcom/xiaomi/xmpush/thrift/NotificationType;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "Invalid"

    const-string/jumbo v2, "INVALID"

    invoke-direct {v0, v1, v4, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzA:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "BarClick"

    const-string/jumbo v2, "bar:click"

    invoke-direct {v0, v1, v5, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzo:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "BarCancel"

    const-string/jumbo v2, "bar:cancel"

    invoke-direct {v0, v1, v6, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyL:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "AppOpen"

    const-string/jumbo v2, "app:open"

    invoke-direct {v0, v1, v7, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzf:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "PackageUninstall"

    const-string/jumbo v2, "package uninstalled"

    invoke-direct {v0, v1, v8, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzl:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "AppUninstall"

    const-string/jumbo v2, "app_uninstalled"

    const/4 v3, 0x5

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzs:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "ClientInfoUpdate"

    const-string/jumbo v2, "client_info_update"

    const/4 v3, 0x6

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzt:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "ClientInfoUpdateOk"

    const-string/jumbo v2, "client_info_update_ok"

    const/4 v3, 0x7

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzy:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "ClientMIIDUpdate"

    const-string/jumbo v2, "client_miid_update"

    const/16 v3, 0x8

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzB:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "PullOfflineMessage"

    const-string/jumbo v2, "pull"

    const/16 v3, 0x9

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzD:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "IosSleep"

    const-string/jumbo v2, "ios_sleep"

    const/16 v3, 0xa

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzE:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "IosWakeUp"

    const-string/jumbo v2, "ios_wakeup"

    const/16 v3, 0xb

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzF:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "AwakeApp"

    const-string/jumbo v2, "awake_app"

    const/16 v3, 0xc

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzm:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "NormalClientConfigUpdate"

    const-string/jumbo v2, "normal_client_config_update"

    const/16 v3, 0xd

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyR:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "CustomClientConfigUpdate"

    const-string/jumbo v2, "custom_client_config_update"

    const/16 v3, 0xe

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyV:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "DailyCheckClientConfig"

    const-string/jumbo v2, "daily_check_client_config"

    const/16 v3, 0xf

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyY:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "DataCollection"

    const-string/jumbo v2, "data_collection"

    const/16 v3, 0x10

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzc:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "RegIdExpired"

    const-string/jumbo v2, "registration id expired"

    const/16 v3, 0x11

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzk:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "ConnectionDisabled"

    const-string/jumbo v2, "!!!MILINK CONNECTION DISABLED!!!"

    const/16 v3, 0x12

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzq:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "PackageUnregistered"

    const-string/jumbo v2, "package_unregistered"

    const/16 v3, 0x13

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dze:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "DecryptMessageFail"

    const-string/jumbo v2, "decrypt_msg_fail"

    const/16 v3, 0x14

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzg:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "SyncInfo"

    const-string/jumbo v2, "sync_info"

    const/16 v3, 0x15

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzw:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "SyncInfoResult"

    const-string/jumbo v2, "sync_info_result"

    const/16 v3, 0x16

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzH:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "ForceSync"

    const-string/jumbo v2, "force_sync"

    const/16 v3, 0x17

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzu:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "UploadClientLog"

    const-string/jumbo v2, "upload_client_log"

    const/16 v3, 0x18

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyW:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "NotificationBarInfo"

    const-string/jumbo v2, "notification_bar_info"

    const/16 v3, 0x19

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzC:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "SyncMIID"

    const-string/jumbo v2, "sync_miid"

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyM:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "UploadTinyData"

    const-string/jumbo v2, "upload"

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyX:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "GeoUpdateLoc"

    const-string/jumbo v2, "geo_update_loc"

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyO:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "GeoRegsiter"

    const-string/jumbo v2, "geo_reg"

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyK:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "GeoRegsiterResult"

    const-string/jumbo v2, "geo_reg_result"

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzn:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "GeoUnregsiter"

    const-string/jumbo v2, "geo_unreg"

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dza:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "GeoUnregsiterResult"

    const-string/jumbo v2, "geo_unreg_result"

    const/16 v3, 0x20

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyT:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "GeoSync"

    const-string/jumbo v2, "geo_sync"

    const/16 v3, 0x21

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyU:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "GeoUpload"

    const-string/jumbo v2, "geo_upload"

    const/16 v3, 0x22

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzr:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "GeoPackageUninstalled"

    const-string/jumbo v2, "geo_package_uninstalled"

    const/16 v3, 0x23

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyQ:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "GeoAuthorized"

    const-string/jumbo v2, "geo_authorized"

    const/16 v3, 0x24

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyS:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "CancelPushMessage"

    const-string/jumbo v2, "clear_push_message"

    const/16 v3, 0x25

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzh:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "DisablePushMessage"

    const-string/jumbo v2, "disable_push"

    const/16 v3, 0x26

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzb:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "EnablePushMessage"

    const-string/jumbo v2, "enable_push"

    const/16 v3, 0x27

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzz:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "ClientABTest"

    const-string/jumbo v2, "client_ab_test"

    const/16 v3, 0x28

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyZ:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "AwakeSystemApp"

    const-string/jumbo v2, "awake_system_app"

    const/16 v3, 0x29

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzx:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "HybridRegister"

    const-string/jumbo v2, "hb_register"

    const/16 v3, 0x2a

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyP:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "HybridRegisterResult"

    const-string/jumbo v2, "hb_register_res"

    const/16 v3, 0x2b

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzv:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "HybridUnregister"

    const-string/jumbo v2, "hb_unregister"

    const/16 v3, 0x2c

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzi:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "HybridUnregisterResult"

    const-string/jumbo v2, "hb_unregister_res"

    const/16 v3, 0x2d

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyN:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "ThirdPartyRegUpdate"

    const-string/jumbo v2, "3rd_party_reg_update"

    const/16 v3, 0x2e

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzd:Lcom/xiaomi/xmpush/thrift/NotificationType;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    const-string/jumbo v1, "VRUpload"

    const-string/jumbo v2, "vr_upload"

    const/16 v3, 0x2f

    invoke-direct {v0, v1, v3, v2}, Lcom/xiaomi/xmpush/thrift/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzG:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v0, 0x30

    new-array v0, v0, [Lcom/xiaomi/xmpush/thrift/NotificationType;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzA:Lcom/xiaomi/xmpush/thrift/NotificationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzo:Lcom/xiaomi/xmpush/thrift/NotificationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyL:Lcom/xiaomi/xmpush/thrift/NotificationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzf:Lcom/xiaomi/xmpush/thrift/NotificationType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzl:Lcom/xiaomi/xmpush/thrift/NotificationType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzs:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzt:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzy:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzB:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzD:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzE:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzF:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzm:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyR:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyV:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyY:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzc:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzk:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzq:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dze:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzg:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzw:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzH:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzu:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyW:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzC:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyM:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyX:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyO:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyK:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzn:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dza:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyT:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyU:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzr:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyQ:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyS:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzh:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzb:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzz:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyZ:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzx:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyP:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzv:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzi:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dyN:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzd:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzG:Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzj:[Lcom/xiaomi/xmpush/thrift/NotificationType;

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzp:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/NotificationType;
    .locals 1

    const-class v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/NotificationType;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/xmpush/thrift/NotificationType;
    .locals 1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/NotificationType;->dzj:[Lcom/xiaomi/xmpush/thrift/NotificationType;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/xmpush/thrift/NotificationType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/NotificationType;->value:Ljava/lang/String;

    return-object v0
.end method
