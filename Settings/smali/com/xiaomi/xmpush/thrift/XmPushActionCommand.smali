.class public Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;
.super Ljava/lang/Object;
.source "XmPushActionCommand.java"

# interfaces
.implements Lorg/apache/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final dAc:Lorg/apache/thrift/protocol/i;

.field private static final dAd:Lorg/apache/thrift/protocol/c;

.field private static final dAe:Lorg/apache/thrift/protocol/i;

.field private static final dAf:Lorg/apache/thrift/protocol/i;

.field private static final dAg:Lorg/apache/thrift/protocol/i;

.field private static final dAh:Lorg/apache/thrift/protocol/i;

.field private static final dAi:Lorg/apache/thrift/protocol/i;

.field public static final dAj:Ljava/util/Map;

.field private static final dAk:Lorg/apache/thrift/protocol/i;

.field private static final dAl:Lorg/apache/thrift/protocol/i;


# instance fields
.field public appId:Ljava/lang/String;

.field public category:Ljava/lang/String;

.field public cmdArgs:Ljava/util/List;

.field public cmdName:Ljava/lang/String;

.field public debug:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public packageName:Ljava/lang/String;

.field public target:Lcom/xiaomi/xmpush/thrift/Target;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/16 v9, 0xf

    const/16 v8, 0xc

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/16 v5, 0xb

    new-instance v0, Lorg/apache/thrift/protocol/c;

    const-string/jumbo v1, "XmPushActionCommand"

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAd:Lorg/apache/thrift/protocol/c;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "debug"

    invoke-direct {v0, v1, v5, v7}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAi:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "target"

    invoke-direct {v0, v1, v8, v6}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAk:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "id"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAc:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "appId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAe:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "cmdName"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAg:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "cmdArgs"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v9, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAh:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "packageName"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAf:Lorg/apache/thrift/protocol/i;

    new-instance v0, Lorg/apache/thrift/protocol/i;

    const-string/jumbo v1, "category"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/thrift/protocol/i;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAl:Lorg/apache/thrift/protocol/i;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;->dxz:Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "debug"

    invoke-direct {v2, v4, v6, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;->dxF:Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/StructMetaData;

    const-class v4, Lcom/xiaomi/xmpush/thrift/Target;

    invoke-direct {v3, v8, v4}, Lorg/apache/thrift/meta_data/StructMetaData;-><init>(BLjava/lang/Class;)V

    const-string/jumbo v4, "target"

    invoke-direct {v2, v4, v6, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;->dxA:Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "id"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;->dxB:Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "appId"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;->dxI:Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "cmdName"

    invoke-direct {v2, v4, v7, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;->dxC:Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/ListMetaData;

    new-instance v4, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v4, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    invoke-direct {v3, v9, v4}, Lorg/apache/thrift/meta_data/ListMetaData;-><init>(BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    const-string/jumbo v4, "cmdArgs"

    invoke-direct {v2, v4, v6, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;->dxG:Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "packageName"

    invoke-direct {v2, v4, v6, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;->dxH:Lcom/xiaomi/xmpush/thrift/XmPushActionCommand$_Fields;

    new-instance v2, Lorg/apache/thrift/meta_data/FieldMetaData;

    new-instance v3, Lorg/apache/thrift/meta_data/FieldValueMetaData;

    invoke-direct {v3, v5}, Lorg/apache/thrift/meta_data/FieldValueMetaData;-><init>(B)V

    const-string/jumbo v4, "category"

    invoke-direct {v2, v4, v6, v3}, Lorg/apache/thrift/meta_data/FieldMetaData;-><init>(Ljava/lang/String;BLorg/apache/thrift/meta_data/FieldValueMetaData;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAj:Ljava/util/Map;

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAj:Ljava/util/Map;

    const-class v1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;

    invoke-static {v1, v0}, Lorg/apache/thrift/meta_data/FieldMetaData;->esm(Ljava/lang/Class;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cTq(Lorg/apache/thrift/protocol/a;)V
    .locals 6

    const/4 v1, 0x0

    const/16 v5, 0xb

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erS()Lorg/apache/thrift/protocol/c;

    :goto_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->esa()Lorg/apache/thrift/protocol/i;

    move-result-object v0

    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eqz v2, :cond_9

    iget-short v2, v0, Lorg/apache/thrift/protocol/i;->eXX:S

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    :goto_1
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erA()V

    goto :goto_0

    :pswitch_1
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v5, :cond_0

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xc

    if-eq v2, v3, :cond_1

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/xiaomi/xmpush/thrift/Target;

    invoke-direct {v0}, Lcom/xiaomi/xmpush/thrift/Target;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/Target;->cTq(Lorg/apache/thrift/protocol/a;)V

    goto :goto_1

    :pswitch_3
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v5, :cond_2

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v5, :cond_3

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    goto :goto_1

    :pswitch_5
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v5, :cond_4

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    goto :goto_1

    :pswitch_6
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    const/16 v3, 0xf

    if-eq v2, v3, :cond_5

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erX()Lorg/apache/thrift/protocol/g;

    move-result-object v2

    new-instance v0, Ljava/util/ArrayList;

    iget v3, v2, Lorg/apache/thrift/protocol/g;->eXU:I

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    move v0, v1

    :goto_2
    iget v3, v2, Lorg/apache/thrift/protocol/g;->eXU:I

    if-lt v0, v3, :cond_6

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erv()V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_7
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v5, :cond_7

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_8
    iget-byte v2, v0, Lorg/apache/thrift/protocol/i;->type:B

    if-eq v2, v5, :cond_8

    iget-byte v0, v0, Lorg/apache/thrift/protocol/i;->type:B

    invoke-static {p1, v0}, Lorg/apache/thrift/protocol/f;->esk(Lorg/apache/thrift/protocol/a;B)V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erR()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    goto/16 :goto_1

    :cond_9
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erU()V

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfz()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public cTu(Lorg/apache/thrift/protocol/a;)V
    .locals 3

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfz()V

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAd:Lorg/apache/thrift/protocol/c;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erx(Lorg/apache/thrift/protocol/c;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    if-nez v0, :cond_5

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    if-nez v0, :cond_6

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    if-nez v0, :cond_7

    :goto_2
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    if-nez v0, :cond_8

    :goto_3
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    if-nez v0, :cond_9

    :goto_4
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    if-nez v0, :cond_a

    :cond_2
    :goto_5
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    if-nez v0, :cond_c

    :cond_3
    :goto_6
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    if-nez v0, :cond_d

    :cond_4
    :goto_7
    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erQ()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erL()V

    return-void

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfp()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAi:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfn()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAk:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {v0, p1}, Lcom/xiaomi/xmpush/thrift/Target;->cTu(Lorg/apache/thrift/protocol/a;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_1

    :cond_7
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAc:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_2

    :cond_8
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAe:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_3

    :cond_9
    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAg:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto :goto_4

    :cond_a
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfx()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAh:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    new-instance v0, Lorg/apache/thrift/protocol/g;

    iget-object v1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xb

    invoke-direct {v0, v2, v1}, Lorg/apache/thrift/protocol/g;-><init>(BI)V

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->esd(Lorg/apache/thrift/protocol/g;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->ese()V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_5

    :cond_b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    goto :goto_8

    :cond_c
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfq()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAf:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_6

    :cond_d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfv()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dAl:Lorg/apache/thrift/protocol/i;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->erT(Lorg/apache/thrift/protocol/i;)V

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/thrift/protocol/a;->ery(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/thrift/protocol/a;->erZ()V

    goto/16 :goto_7
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfu(Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;)I

    move-result v0

    return v0
.end method

.method public dfA(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    goto :goto_0
.end method

.method public dfB(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    return-object p0
.end method

.method public dfC(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    return-object p0
.end method

.method public dfm(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    return-object p0
.end method

.method public dfn()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dfo()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dfp()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dfq()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dfr(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    return-object p0
.end method

.method public dfs(Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;)Z
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfp()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfp()Z

    move-result v1

    if-eqz v0, :cond_3

    :cond_0
    if-nez v0, :cond_7

    :cond_1
    return v2

    :cond_2
    return v2

    :cond_3
    if-nez v1, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfn()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfn()Z

    move-result v1

    if-eqz v0, :cond_8

    :cond_5
    if-nez v0, :cond_c

    :cond_6
    return v2

    :cond_7
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    return v2

    :cond_8
    if-nez v1, :cond_5

    :cond_9
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfy()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfy()Z

    move-result v1

    if-eqz v0, :cond_d

    :cond_a
    if-nez v0, :cond_11

    :cond_b
    return v2

    :cond_c
    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {v0, v1}, Lcom/xiaomi/xmpush/thrift/Target;->dae(Lcom/xiaomi/xmpush/thrift/Target;)Z

    move-result v0

    if-nez v0, :cond_9

    return v2

    :cond_d
    if-nez v1, :cond_a

    :cond_e
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dft()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dft()Z

    move-result v1

    if-eqz v0, :cond_12

    :cond_f
    if-nez v0, :cond_16

    :cond_10
    return v2

    :cond_11
    if-eqz v1, :cond_b

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    return v2

    :cond_12
    if-nez v1, :cond_f

    :cond_13
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfo()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfo()Z

    move-result v1

    if-eqz v0, :cond_17

    :cond_14
    if-nez v0, :cond_1b

    :cond_15
    return v2

    :cond_16
    if-eqz v1, :cond_10

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    return v2

    :cond_17
    if-nez v1, :cond_14

    :cond_18
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfx()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfx()Z

    move-result v1

    if-eqz v0, :cond_1c

    :cond_19
    if-nez v0, :cond_20

    :cond_1a
    return v2

    :cond_1b
    if-eqz v1, :cond_15

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    return v2

    :cond_1c
    if-nez v1, :cond_19

    :cond_1d
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfq()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfq()Z

    move-result v1

    if-eqz v0, :cond_21

    :cond_1e
    if-nez v0, :cond_25

    :cond_1f
    return v2

    :cond_20
    if-eqz v1, :cond_1a

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d

    return v2

    :cond_21
    if-nez v1, :cond_1e

    :cond_22
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfv()Z

    move-result v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfv()Z

    move-result v1

    if-eqz v0, :cond_26

    :cond_23
    if-nez v0, :cond_28

    :cond_24
    return v2

    :cond_25
    if-eqz v1, :cond_1f

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    return v2

    :cond_26
    if-nez v1, :cond_23

    :cond_27
    const/4 v0, 0x1

    return v0

    :cond_28
    if-eqz v1, :cond_24

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_27

    return v2
.end method

.method public dft()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dfu(Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;)I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfp()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfp()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfp()Z

    move-result v0

    if-nez v0, :cond_a

    :cond_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfn()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfn()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfn()Z

    move-result v0

    if-nez v0, :cond_c

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfy()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfy()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfy()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_2
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dft()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dft()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dft()Z

    move-result v0

    if-nez v0, :cond_10

    :cond_3
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfo()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfo()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfo()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_4
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfx()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfx()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_13

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfx()Z

    move-result v0

    if-nez v0, :cond_14

    :cond_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfq()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfq()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfq()Z

    move-result v0

    if-nez v0, :cond_16

    :cond_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfv()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfv()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-nez v0, :cond_17

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfv()Z

    move-result v0

    if-nez v0, :cond_18

    :cond_7
    return v2

    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_9
    return v0

    :cond_a
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_b
    return v0

    :cond_c
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esE(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    :cond_d
    return v0

    :cond_e
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    :cond_f
    return v0

    :cond_10
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    :cond_11
    return v0

    :cond_12
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    :cond_13
    return v0

    :cond_14
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->eso(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    :cond_15
    return v0

    :cond_16
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_6

    return v0

    :cond_17
    return v0

    :cond_18
    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    iget-object v1, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/thrift/b;->esA(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    return v0
.end method

.method public dfv()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dfw(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    return-object p0
.end method

.method public dfx()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dfy()Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dfz()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    if-eqz v0, :cond_2

    return-void

    :cond_0
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'id\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'appId\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lorg/apache/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Required field \'cmdName\' was not present! Struct: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;

    if-nez v0, :cond_1

    return v1

    :cond_0
    return v1

    :cond_1
    check-cast p1, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;

    invoke-virtual {p0, p1}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfs(Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "XmPushActionCommand("

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfp()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfn()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    if-eqz v0, :cond_5

    :goto_2
    const-string/jumbo v0, "id:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->id:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "appId:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->appId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "cmdName:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfx()Z

    move-result v0

    if-nez v0, :cond_9

    :goto_6
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfq()Z

    move-result v0

    if-nez v0, :cond_b

    :goto_7
    invoke-virtual {p0}, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->dfv()Z

    move-result v0

    if-nez v0, :cond_d

    :goto_8
    const-string/jumbo v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "debug:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->debug:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_9
    move v0, v1

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    :cond_2
    if-eqz v0, :cond_3

    :goto_a
    const-string/jumbo v0, "target:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->target:Lcom/xiaomi/xmpush/thrift/Target;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_b
    move v0, v1

    goto/16 :goto_1

    :cond_3
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_a

    :cond_4
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_b

    :cond_5
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_6
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_7
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    :cond_8
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_9
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "cmdArgs:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->cmdArgs:Ljava/util/List;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_a
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_b
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "packageName:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_c
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_d
    const-string/jumbo v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "category:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionCommand;->category:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    :cond_e
    const-string/jumbo v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8
.end method
