.class public final enum Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;
.super Ljava/lang/Enum;
.source "XmPushActionRegistration.java"


# static fields
.field public static final enum dtW:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dtX:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dtY:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dtZ:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dua:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field private static final dub:Ljava/util/Map;

.field public static final enum duc:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dud:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum due:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum duf:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dug:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum duh:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dui:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field private static final synthetic duj:[Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum duk:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dul:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dum:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dun:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum duo:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dup:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum duq:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dur:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dus:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum dut:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

.field public static final enum duu:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;


# instance fields
.field private final _fieldName:Ljava/lang/String;

.field private final _thriftId:S


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "DEBUG"

    const-string/jumbo v2, "debug"

    invoke-direct {v0, v1, v5, v6, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duf:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "TARGET"

    const-string/jumbo v2, "target"

    invoke-direct {v0, v1, v6, v7, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dtY:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "ID"

    const-string/jumbo v2, "id"

    invoke-direct {v0, v1, v7, v8, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dun:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "APP_ID"

    const-string/jumbo v2, "appId"

    invoke-direct {v0, v1, v8, v9, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dus:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "APP_VERSION"

    const/4 v2, 0x5

    const-string/jumbo v3, "appVersion"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->due:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "PACKAGE_NAME"

    const/4 v2, 0x6

    const-string/jumbo v3, "packageName"

    const/4 v4, 0x5

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dtW:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "TOKEN"

    const/4 v2, 0x7

    const-string/jumbo v3, "token"

    const/4 v4, 0x6

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dtZ:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "DEVICE_ID"

    const/16 v2, 0x8

    const-string/jumbo v3, "deviceId"

    const/4 v4, 0x7

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duc:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "ALIAS_NAME"

    const/16 v2, 0x9

    const-string/jumbo v3, "aliasName"

    const/16 v4, 0x8

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duk:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "SDK_VERSION"

    const/16 v2, 0xa

    const-string/jumbo v3, "sdkVersion"

    const/16 v4, 0x9

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duh:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "REG_ID"

    const/16 v2, 0xb

    const-string/jumbo v3, "regId"

    const/16 v4, 0xa

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dud:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "PUSH_SDK_VERSION_NAME"

    const/16 v2, 0xc

    const-string/jumbo v3, "pushSdkVersionName"

    const/16 v4, 0xb

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dua:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "PUSH_SDK_VERSION_CODE"

    const/16 v2, 0xd

    const-string/jumbo v3, "pushSdkVersionCode"

    const/16 v4, 0xc

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dum:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "APP_VERSION_CODE"

    const/16 v2, 0xe

    const-string/jumbo v3, "appVersionCode"

    const/16 v4, 0xd

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dtX:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "ANDROID_ID"

    const/16 v2, 0xf

    const-string/jumbo v3, "androidId"

    const/16 v4, 0xe

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dup:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "IMEI"

    const/16 v2, 0x10

    const-string/jumbo v3, "imei"

    const/16 v4, 0xf

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dug:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "SERIAL"

    const/16 v2, 0x11

    const-string/jumbo v3, "serial"

    const/16 v4, 0x10

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duq:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "IMEI_MD5"

    const/16 v2, 0x12

    const-string/jumbo v3, "imeiMd5"

    const/16 v4, 0x11

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dul:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "SPACE_ID"

    const/16 v2, 0x13

    const-string/jumbo v3, "spaceId"

    const/16 v4, 0x12

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dui:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "REASON"

    const/16 v2, 0x14

    const-string/jumbo v3, "reason"

    const/16 v4, 0x13

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dur:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "CONNECTION_ATTRS"

    const/16 v2, 0x64

    const-string/jumbo v3, "connectionAttrs"

    const/16 v4, 0x14

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duo:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "CLEAN_OLD_REG_INFO"

    const/16 v2, 0x65

    const-string/jumbo v3, "cleanOldRegInfo"

    const/16 v4, 0x15

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dut:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const-string/jumbo v1, "OLD_REG_ID"

    const/16 v2, 0x66

    const-string/jumbo v3, "oldRegId"

    const/16 v4, 0x16

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duu:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duf:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dtY:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    aput-object v1, v0, v6

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dun:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    aput-object v1, v0, v7

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dus:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    aput-object v1, v0, v8

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->due:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    aput-object v1, v0, v9

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dtW:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dtZ:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duc:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duk:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duh:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dud:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dua:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dum:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dtX:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dup:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dug:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duq:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dul:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dui:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dur:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duo:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dut:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duu:Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duj:[Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dub:Ljava/util/Map;

    const-class v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    sget-object v2, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->dub:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->cTY()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ISLjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    int-to-short v0, p3

    iput-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->_thriftId:S

    iput-object p4, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->_fieldName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;
    .locals 1

    const-class v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;
    .locals 1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->duj:[Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;

    return-object v0
.end method


# virtual methods
.method public cTY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionRegistration$_Fields;->_fieldName:Ljava/lang/String;

    return-object v0
.end method
