.class public final enum Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;
.super Ljava/lang/Enum;
.source "XmPushActionAckMessage.java"


# static fields
.field public static final enum dyA:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyB:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyC:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyD:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyE:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyF:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyG:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyH:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyI:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyJ:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyo:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyp:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field private static final dyq:Ljava/util/Map;

.field private static final synthetic dyr:[Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dys:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyt:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyu:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyv:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyw:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyx:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyy:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

.field public static final enum dyz:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;


# instance fields
.field private final _fieldName:Ljava/lang/String;

.field private final _thriftId:S


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "DEBUG"

    const-string/jumbo v2, "debug"

    invoke-direct {v0, v1, v5, v6, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyG:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "TARGET"

    const-string/jumbo v2, "target"

    invoke-direct {v0, v1, v6, v7, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyF:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "ID"

    const-string/jumbo v2, "id"

    invoke-direct {v0, v1, v7, v8, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyu:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "APP_ID"

    const-string/jumbo v2, "appId"

    invoke-direct {v0, v1, v8, v9, v2}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyA:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "MESSAGE_TS"

    const/4 v2, 0x5

    const-string/jumbo v3, "messageTs"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyI:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "TOPIC"

    const/4 v2, 0x6

    const-string/jumbo v3, "topic"

    const/4 v4, 0x5

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dys:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "ALIAS_NAME"

    const/4 v2, 0x7

    const-string/jumbo v3, "aliasName"

    const/4 v4, 0x6

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyD:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "REQUEST"

    const/16 v2, 0x8

    const-string/jumbo v3, "request"

    const/4 v4, 0x7

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyw:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "PACKAGE_NAME"

    const/16 v2, 0x9

    const-string/jumbo v3, "packageName"

    const/16 v4, 0x8

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyv:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "CATEGORY"

    const/16 v2, 0xa

    const-string/jumbo v3, "category"

    const/16 v4, 0x9

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyt:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "IS_ONLINE"

    const/16 v2, 0xb

    const-string/jumbo v3, "isOnline"

    const/16 v4, 0xa

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyB:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "REG_ID"

    const/16 v2, 0xc

    const-string/jumbo v3, "regId"

    const/16 v4, 0xb

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyp:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "CALLBACK_URL"

    const/16 v2, 0xd

    const-string/jumbo v3, "callbackUrl"

    const/16 v4, 0xc

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyx:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "USER_ACCOUNT"

    const/16 v2, 0xe

    const-string/jumbo v3, "userAccount"

    const/16 v4, 0xd

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyC:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "DEVICE_STATUS"

    const/16 v2, 0xf

    const-string/jumbo v3, "deviceStatus"

    const/16 v4, 0xe

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyJ:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "GEO_MSG_STATUS"

    const/16 v2, 0x10

    const-string/jumbo v3, "geoMsgStatus"

    const/16 v4, 0xf

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyy:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "IMEI_MD5"

    const/16 v2, 0x14

    const-string/jumbo v3, "imeiMd5"

    const/16 v4, 0x10

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyz:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "DEVICE_ID"

    const/16 v2, 0x15

    const-string/jumbo v3, "deviceId"

    const/16 v4, 0x11

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyo:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "PASS_THROUGH"

    const/16 v2, 0x16

    const-string/jumbo v3, "passThrough"

    const/16 v4, 0x12

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyH:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const-string/jumbo v1, "EXTRA"

    const/16 v2, 0x17

    const-string/jumbo v3, "extra"

    const/16 v4, 0x13

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyE:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyG:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyF:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    aput-object v1, v0, v6

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyu:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    aput-object v1, v0, v7

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyA:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    aput-object v1, v0, v8

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyI:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    aput-object v1, v0, v9

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dys:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyD:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyw:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyv:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyt:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyB:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyp:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyx:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyC:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyJ:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyy:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyz:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyo:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyH:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyE:Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyr:[Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyq:Ljava/util/Map;

    const-class v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    sget-object v2, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyq:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->cTY()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ISLjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    int-to-short v0, p3

    iput-short v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->_thriftId:S

    iput-object p4, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->_fieldName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;
    .locals 1

    const-class v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;
    .locals 1

    sget-object v0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->dyr:[Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;

    return-object v0
.end method


# virtual methods
.method public cTY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/xmpush/thrift/XmPushActionAckMessage$_Fields;->_fieldName:Ljava/lang/String;

    return-object v0
.end method
