.class public Lcom/xiaomi/xmpush/thrift/a;
.super Ljava/lang/Object;
.source "XmPushThriftSerializeUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static daG(Lorg/apache/thrift/TBase;)[B
    .locals 3

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    :try_start_0
    new-instance v0, Lorg/apache/thrift/f;

    new-instance v1, Lorg/apache/thrift/protocol/TBinaryProtocol$Factory;

    invoke-direct {v1}, Lorg/apache/thrift/protocol/TBinaryProtocol$Factory;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/thrift/f;-><init>(Lorg/apache/thrift/protocol/TProtocolFactory;)V

    invoke-virtual {v0, p0}, Lorg/apache/thrift/f;->esH(Lorg/apache/thrift/TBase;)[B
    :try_end_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :cond_0
    return-object v2

    :catch_0
    move-exception v0

    const-string/jumbo v1, "convertThriftObjectToBytes catch TException."

    invoke-static {v1, v0}, Lcom/xiaomi/channel/commonutils/e/a;->czv(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v2
.end method

.method public static daH(Lorg/apache/thrift/TBase;[B)V
    .locals 4

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/thrift/e;

    new-instance v1, Lorg/apache/thrift/protocol/XmPushTBinaryProtocol$Factory;

    array-length v2, p1

    invoke-direct {v1, v3, v3, v2}, Lorg/apache/thrift/protocol/XmPushTBinaryProtocol$Factory;-><init>(ZZI)V

    invoke-direct {v0, v1}, Lorg/apache/thrift/e;-><init>(Lorg/apache/thrift/protocol/TProtocolFactory;)V

    invoke-virtual {v0, p0, p1}, Lorg/apache/thrift/e;->esG(Lorg/apache/thrift/TBase;[B)V

    return-void

    :cond_0
    new-instance v0, Lorg/apache/thrift/TException;

    const-string/jumbo v1, "the message byte is empty."

    invoke-direct {v0, v1}, Lorg/apache/thrift/TException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static daI(ZZZ)S
    .locals 3

    const/4 v1, 0x0

    if-nez p0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v2, v0, 0x0

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    if-nez p2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    int-to-short v0, v0

    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_2
.end method

.method public static daJ(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)S
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;->packageName:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/xiaomi/channel/commonutils/android/b;->czO(Landroid/content/Context;Ljava/lang/String;)Lcom/xiaomi/channel/commonutils/android/AppInfoUtils$AppNotificationOp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/channel/commonutils/android/AppInfoUtils$AppNotificationOp;->getValue()I

    move-result v0

    add-int/lit8 v2, v0, 0x0

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/h/j;->cBj(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v2, v0

    invoke-static {p0}, Lcom/xiaomi/channel/commonutils/h/j;->cBk(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    invoke-static {p0, p1}, Lcom/xiaomi/push/service/ak;->cQw(Landroid/content/Context;Lcom/xiaomi/xmpush/thrift/XmPushActionContainer;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    int-to-short v0, v0

    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1

    :cond_2
    const/16 v1, 0x10

    goto :goto_2
.end method
