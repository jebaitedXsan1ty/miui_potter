.class public Lcom/xiaomi/g/d;
.super Ljava/lang/Object;
.source "TinyDataCacheProcessor.java"

# interfaces
.implements Lcom/xiaomi/push/service/a;


# static fields
.field private static dBK:Z


# instance fields
.field private dBI:I

.field private dBJ:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/xiaomi/g/d;->dBK:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/g/d;->mContext:Landroid/content/Context;

    return-void
.end method

.method private dgU()Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/xiaomi/g/d;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "mipush_extra"

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "last_tiny_data_upload_timestamp"

    const-wide/16 v4, -0x1

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    sub-long v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    iget v4, p0, Lcom/xiaomi/g/d;->dBI:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private dgV(Lcom/xiaomi/g/b;)Z
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/g/d;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/xiaomi/g/d;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/g/d;->dgW(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/xiaomi/g/d;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "tiny_data.data"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/xiaomi/g/d;->dBK:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_0
    return v3

    :cond_1
    return v3

    :cond_2
    return v3

    :cond_3
    const-string/jumbo v0, "TinyData(TinyDataCacheProcessor) no ready file to get data."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    return v3

    :cond_4
    return v3
.end method

.method private dgW(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, "com.xiaomi.xmsf"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/g/d;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "pref_registered_pkg_names"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "1000271"

    goto :goto_0
.end method

.method private dgX(Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, Lcom/xiaomi/push/service/aU;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aU;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ConfigKey;->dqe:Lcom/xiaomi/xmpush/thrift/ConfigKey;

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/ConfigKey;->getValue()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/push/service/aU;->cTb(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/xiaomi/g/d;->dBJ:Z

    invoke-static {p1}, Lcom/xiaomi/push/service/aU;->getInstance(Landroid/content/Context;)Lcom/xiaomi/push/service/aU;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/xmpush/thrift/ConfigKey;->dpU:Lcom/xiaomi/xmpush/thrift/ConfigKey;

    invoke-virtual {v1}, Lcom/xiaomi/xmpush/thrift/ConfigKey;->getValue()I

    move-result v1

    const/16 v2, 0x1c20

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/push/service/aU;->cSW(II)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/g/d;->dBI:I

    iget v0, p0, Lcom/xiaomi/g/d;->dBI:I

    const/16 v1, 0x3c

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/g/d;->dBI:I

    return-void
.end method

.method public static dgY(Z)V
    .locals 0

    sput-boolean p0, Lcom/xiaomi/g/d;->dBK:Z

    return-void
.end method


# virtual methods
.method public cMu()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/g/d;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/xiaomi/g/d;->dgX(Landroid/content/Context;)V

    iget-boolean v0, p0, Lcom/xiaomi/g/d;->dBJ:Z

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/xiaomi/g/d;->dgU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/g/d;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/g/c;->getInstance(Landroid/content/Context;)Lcom/xiaomi/g/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/g/c;->dgT()Lcom/xiaomi/g/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/g/d;->dgV(Lcom/xiaomi/g/b;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    sput-boolean v1, Lcom/xiaomi/g/d;->dBK:Z

    iget-object v1, p0, Lcom/xiaomi/g/d;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/xiaomi/g/a;->dgK(Landroid/content/Context;Lcom/xiaomi/g/b;)V

    return-void

    :cond_2
    return-void
.end method
