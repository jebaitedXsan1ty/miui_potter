.class Lcom/xiaomi/smack/b;
.super Lcom/xiaomi/push/service/n;
.source "SocketConnection.java"


# instance fields
.field final synthetic cVf:J

.field final synthetic cVg:Lcom/xiaomi/smack/j;


# direct methods
.method constructor <init>(Lcom/xiaomi/smack/j;IJ)V
    .locals 1

    iput-object p1, p0, Lcom/xiaomi/smack/b;->cVg:Lcom/xiaomi/smack/j;

    iput-wide p3, p0, Lcom/xiaomi/smack/b;->cVf:J

    invoke-direct {p0, p2}, Lcom/xiaomi/push/service/n;-><init>(I)V

    return-void
.end method


# virtual methods
.method public cxd()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "check the ping-pong."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/xiaomi/smack/b;->cVf:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cxe()V
    .locals 4

    invoke-static {}, Ljava/lang/Thread;->yield()V

    iget-object v0, p0, Lcom/xiaomi/smack/b;->cVg:Lcom/xiaomi/smack/j;

    invoke-virtual {v0}, Lcom/xiaomi/smack/j;->cxy()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/smack/b;->cVg:Lcom/xiaomi/smack/j;

    iget-wide v2, p0, Lcom/xiaomi/smack/b;->cVf:J

    invoke-virtual {v0, v2, v3}, Lcom/xiaomi/smack/j;->cxs(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/smack/b;->cVg:Lcom/xiaomi/smack/j;

    iget-object v0, v0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    const/4 v1, 0x0

    const/16 v2, 0x16

    invoke-virtual {v0, v2, v1}, Lcom/xiaomi/push/service/XMPushService;->cPK(ILjava/lang/Exception;)V

    iget-object v0, p0, Lcom/xiaomi/smack/b;->cVg:Lcom/xiaomi/smack/j;

    iget-object v0, v0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/XMPushService;->cQf(Z)V

    goto :goto_0
.end method
