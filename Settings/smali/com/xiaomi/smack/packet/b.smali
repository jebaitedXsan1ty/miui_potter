.class public Lcom/xiaomi/smack/packet/b;
.super Ljava/lang/Object;
.source "IQ.java"


# static fields
.field public static final cTc:Lcom/xiaomi/smack/packet/b;

.field public static final cTd:Lcom/xiaomi/smack/packet/b;

.field public static final cTe:Lcom/xiaomi/smack/packet/b;

.field public static final cTf:Lcom/xiaomi/smack/packet/b;

.field public static final cTg:Lcom/xiaomi/smack/packet/b;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/xiaomi/smack/packet/b;

    const-string/jumbo v1, "get"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/b;->cTf:Lcom/xiaomi/smack/packet/b;

    new-instance v0, Lcom/xiaomi/smack/packet/b;

    const-string/jumbo v1, "set"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/b;->cTc:Lcom/xiaomi/smack/packet/b;

    new-instance v0, Lcom/xiaomi/smack/packet/b;

    const-string/jumbo v1, "result"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/b;->cTd:Lcom/xiaomi/smack/packet/b;

    new-instance v0, Lcom/xiaomi/smack/packet/b;

    const-string/jumbo v1, "error"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/b;->cTg:Lcom/xiaomi/smack/packet/b;

    new-instance v0, Lcom/xiaomi/smack/packet/b;

    const-string/jumbo v1, "command"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/b;->cTe:Lcom/xiaomi/smack/packet/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/smack/packet/b;->value:Ljava/lang/String;

    return-void
.end method

.method public static cva(Ljava/lang/String;)Lcom/xiaomi/smack/packet/b;
    .locals 3

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/smack/packet/b;->cTf:Lcom/xiaomi/smack/packet/b;

    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/xiaomi/smack/packet/b;->cTc:Lcom/xiaomi/smack/packet/b;

    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/xiaomi/smack/packet/b;->cTg:Lcom/xiaomi/smack/packet/b;

    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/xiaomi/smack/packet/b;->cTd:Lcom/xiaomi/smack/packet/b;

    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/xiaomi/smack/packet/b;->cTe:Lcom/xiaomi/smack/packet/b;

    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    return-object v2

    :cond_0
    return-object v2

    :cond_1
    sget-object v0, Lcom/xiaomi/smack/packet/b;->cTf:Lcom/xiaomi/smack/packet/b;

    return-object v0

    :cond_2
    sget-object v0, Lcom/xiaomi/smack/packet/b;->cTc:Lcom/xiaomi/smack/packet/b;

    return-object v0

    :cond_3
    sget-object v0, Lcom/xiaomi/smack/packet/b;->cTg:Lcom/xiaomi/smack/packet/b;

    return-object v0

    :cond_4
    sget-object v0, Lcom/xiaomi/smack/packet/b;->cTd:Lcom/xiaomi/smack/packet/b;

    return-object v0

    :cond_5
    sget-object v0, Lcom/xiaomi/smack/packet/b;->cTe:Lcom/xiaomi/smack/packet/b;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/b;->value:Ljava/lang/String;

    return-object v0
.end method
