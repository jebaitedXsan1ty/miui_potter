.class public final enum Lcom/xiaomi/smack/packet/Presence$Type;
.super Ljava/lang/Enum;
.source "Presence.java"


# static fields
.field private static final synthetic cTG:[Lcom/xiaomi/smack/packet/Presence$Type;

.field public static final enum cTH:Lcom/xiaomi/smack/packet/Presence$Type;

.field public static final enum cTI:Lcom/xiaomi/smack/packet/Presence$Type;

.field public static final enum cTJ:Lcom/xiaomi/smack/packet/Presence$Type;

.field public static final enum cTK:Lcom/xiaomi/smack/packet/Presence$Type;

.field public static final enum cTL:Lcom/xiaomi/smack/packet/Presence$Type;

.field public static final enum cTM:Lcom/xiaomi/smack/packet/Presence$Type;

.field public static final enum cTN:Lcom/xiaomi/smack/packet/Presence$Type;

.field public static final enum cTO:Lcom/xiaomi/smack/packet/Presence$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Type;

    const-string/jumbo v1, "available"

    invoke-direct {v0, v1, v3}, Lcom/xiaomi/smack/packet/Presence$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTM:Lcom/xiaomi/smack/packet/Presence$Type;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Type;

    const-string/jumbo v1, "unavailable"

    invoke-direct {v0, v1, v4}, Lcom/xiaomi/smack/packet/Presence$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTO:Lcom/xiaomi/smack/packet/Presence$Type;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Type;

    const-string/jumbo v1, "subscribe"

    invoke-direct {v0, v1, v5}, Lcom/xiaomi/smack/packet/Presence$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTH:Lcom/xiaomi/smack/packet/Presence$Type;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Type;

    const-string/jumbo v1, "subscribed"

    invoke-direct {v0, v1, v6}, Lcom/xiaomi/smack/packet/Presence$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTL:Lcom/xiaomi/smack/packet/Presence$Type;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Type;

    const-string/jumbo v1, "unsubscribe"

    invoke-direct {v0, v1, v7}, Lcom/xiaomi/smack/packet/Presence$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTK:Lcom/xiaomi/smack/packet/Presence$Type;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Type;

    const-string/jumbo v1, "unsubscribed"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/smack/packet/Presence$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTI:Lcom/xiaomi/smack/packet/Presence$Type;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Type;

    const-string/jumbo v1, "error"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/smack/packet/Presence$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTJ:Lcom/xiaomi/smack/packet/Presence$Type;

    new-instance v0, Lcom/xiaomi/smack/packet/Presence$Type;

    const-string/jumbo v1, "probe"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/xiaomi/smack/packet/Presence$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTN:Lcom/xiaomi/smack/packet/Presence$Type;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/xiaomi/smack/packet/Presence$Type;

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Type;->cTM:Lcom/xiaomi/smack/packet/Presence$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Type;->cTO:Lcom/xiaomi/smack/packet/Presence$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Type;->cTH:Lcom/xiaomi/smack/packet/Presence$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Type;->cTL:Lcom/xiaomi/smack/packet/Presence$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Type;->cTK:Lcom/xiaomi/smack/packet/Presence$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Type;->cTI:Lcom/xiaomi/smack/packet/Presence$Type;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Type;->cTJ:Lcom/xiaomi/smack/packet/Presence$Type;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/xiaomi/smack/packet/Presence$Type;->cTN:Lcom/xiaomi/smack/packet/Presence$Type;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTG:[Lcom/xiaomi/smack/packet/Presence$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/xiaomi/smack/packet/Presence$Type;
    .locals 1

    const-class v0, Lcom/xiaomi/smack/packet/Presence$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/packet/Presence$Type;

    return-object v0
.end method

.method public static values()[Lcom/xiaomi/smack/packet/Presence$Type;
    .locals 1

    sget-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTG:[Lcom/xiaomi/smack/packet/Presence$Type;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/xiaomi/smack/packet/Presence$Type;

    return-object v0
.end method
