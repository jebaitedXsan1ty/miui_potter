.class public Lcom/xiaomi/smack/packet/f;
.super Ljava/lang/Object;
.source "XMPPError.java"


# static fields
.field public static final cTP:Lcom/xiaomi/smack/packet/f;

.field public static final cTQ:Lcom/xiaomi/smack/packet/f;

.field public static final cTR:Lcom/xiaomi/smack/packet/f;

.field public static final cTS:Lcom/xiaomi/smack/packet/f;

.field public static final cTT:Lcom/xiaomi/smack/packet/f;

.field public static final cTU:Lcom/xiaomi/smack/packet/f;

.field public static final cTV:Lcom/xiaomi/smack/packet/f;

.field public static final cTW:Lcom/xiaomi/smack/packet/f;

.field public static final cTX:Lcom/xiaomi/smack/packet/f;

.field public static final cTY:Lcom/xiaomi/smack/packet/f;

.field public static final cTZ:Lcom/xiaomi/smack/packet/f;

.field public static final cUa:Lcom/xiaomi/smack/packet/f;

.field public static final cUb:Lcom/xiaomi/smack/packet/f;

.field public static final cUc:Lcom/xiaomi/smack/packet/f;

.field public static final cUd:Lcom/xiaomi/smack/packet/f;

.field public static final cUe:Lcom/xiaomi/smack/packet/f;

.field public static final cUf:Lcom/xiaomi/smack/packet/f;

.field public static final cUg:Lcom/xiaomi/smack/packet/f;

.field public static final cUh:Lcom/xiaomi/smack/packet/f;

.field public static final cUi:Lcom/xiaomi/smack/packet/f;

.field public static final cUj:Lcom/xiaomi/smack/packet/f;

.field public static final cUk:Lcom/xiaomi/smack/packet/f;

.field public static final cUl:Lcom/xiaomi/smack/packet/f;

.field public static final cUm:Lcom/xiaomi/smack/packet/f;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "internal-server-error"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTS:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "forbidden"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUh:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "bad-request"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTW:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "conflict"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUi:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "feature-not-implemented"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUa:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "gone"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTX:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "item-not-found"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTU:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "jid-malformed"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUk:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "not-acceptable"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUl:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "not-allowed"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUj:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "not-authorized"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTQ:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "payment-required"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTP:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "recipient-unavailable"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTY:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "redirect"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUc:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "registration-required"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUm:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "remote-server-error"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTV:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "remote-server-not-found"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTZ:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "remote-server-timeout"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUe:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "resource-constraint"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUb:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "service-unavailable"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTR:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "subscription-required"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cTT:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "undefined-condition"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUd:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "unexpected-request"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUf:Lcom/xiaomi/smack/packet/f;

    new-instance v0, Lcom/xiaomi/smack/packet/f;

    const-string/jumbo v1, "request-timeout"

    invoke-direct {v0, v1}, Lcom/xiaomi/smack/packet/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/xiaomi/smack/packet/f;->cUg:Lcom/xiaomi/smack/packet/f;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/smack/packet/f;->value:Ljava/lang/String;

    return-void
.end method

.method static synthetic cvU(Lcom/xiaomi/smack/packet/f;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/f;->value:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/packet/f;->value:Ljava/lang/String;

    return-object v0
.end method
