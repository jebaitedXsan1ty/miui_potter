.class public Lcom/xiaomi/smack/packet/i;
.super Lcom/xiaomi/smack/packet/d;
.source "Presence.java"


# instance fields
.field private cUu:Ljava/lang/String;

.field private cUv:I

.field private cUw:Lcom/xiaomi/smack/packet/Presence$Type;

.field private cUx:Lcom/xiaomi/smack/packet/Presence$Mode;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/xiaomi/smack/packet/d;-><init>(Landroid/os/Bundle;)V

    sget-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTM:Lcom/xiaomi/smack/packet/Presence$Type;

    iput-object v0, p0, Lcom/xiaomi/smack/packet/i;->cUw:Lcom/xiaomi/smack/packet/Presence$Type;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUu:Ljava/lang/String;

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/xiaomi/smack/packet/i;->cUv:I

    iput-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    const-string/jumbo v0, "ext_pres_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    const-string/jumbo v0, "ext_pres_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string/jumbo v0, "ext_pres_prio"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    const-string/jumbo v0, "ext_pres_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    return-void

    :cond_0
    const-string/jumbo v0, "ext_pres_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/smack/packet/Presence$Type;->valueOf(Ljava/lang/String;)Lcom/xiaomi/smack/packet/Presence$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/i;->cUw:Lcom/xiaomi/smack/packet/Presence$Type;

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "ext_pres_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/i;->cUu:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "ext_pres_prio"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/smack/packet/i;->cUv:I

    goto :goto_2

    :cond_3
    const-string/jumbo v0, "ext_pres_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/smack/packet/Presence$Mode;->valueOf(Ljava/lang/String;)Lcom/xiaomi/smack/packet/Presence$Mode;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    goto :goto_3
.end method

.method public constructor <init>(Lcom/xiaomi/smack/packet/Presence$Type;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/xiaomi/smack/packet/d;-><init>()V

    sget-object v0, Lcom/xiaomi/smack/packet/Presence$Type;->cTM:Lcom/xiaomi/smack/packet/Presence$Type;

    iput-object v0, p0, Lcom/xiaomi/smack/packet/i;->cUw:Lcom/xiaomi/smack/packet/Presence$Type;

    iput-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUu:Ljava/lang/String;

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/xiaomi/smack/packet/i;->cUv:I

    iput-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    invoke-virtual {p0, p1}, Lcom/xiaomi/smack/packet/i;->cwh(Lcom/xiaomi/smack/packet/Presence$Type;)V

    return-void
.end method


# virtual methods
.method public cve()Landroid/os/Bundle;
    .locals 3

    invoke-super {p0}, Lcom/xiaomi/smack/packet/d;->cve()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUw:Lcom/xiaomi/smack/packet/Presence$Type;

    if-nez v1, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUu:Ljava/lang/String;

    if-nez v1, :cond_2

    :goto_1
    iget v1, p0, Lcom/xiaomi/smack/packet/i;->cUv:I

    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_3

    :goto_2
    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    if-nez v1, :cond_4

    :cond_0
    :goto_3
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUw:Lcom/xiaomi/smack/packet/Presence$Type;

    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/Presence$Type;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ext_pres_type"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUu:Ljava/lang/String;

    const-string/jumbo v2, "ext_pres_status"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget v1, p0, Lcom/xiaomi/smack/packet/i;->cUv:I

    const-string/jumbo v2, "ext_pres_prio"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    sget-object v2, Lcom/xiaomi/smack/packet/Presence$Mode;->cUD:Lcom/xiaomi/smack/packet/Presence$Mode;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/Presence$Mode;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "ext_pres_mode"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public cvl()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "<presence"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvz()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :goto_0
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvB()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    :goto_1
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvO()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    :goto_2
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvx()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    :goto_3
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvN()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    :goto_4
    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUw:Lcom/xiaomi/smack/packet/Presence$Type;

    if-nez v1, :cond_6

    :goto_5
    const-string/jumbo v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUu:Ljava/lang/String;

    if-nez v1, :cond_7

    :goto_6
    iget v1, p0, Lcom/xiaomi/smack/packet/i;->cUv:I

    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_8

    :goto_7
    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    if-nez v1, :cond_9

    :cond_0
    :goto_8
    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvA()Lcom/xiaomi/smack/packet/j;

    move-result-object v1

    if-nez v1, :cond_a

    :goto_9
    const-string/jumbo v1, "</presence>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    const-string/jumbo v1, " xmlns=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvz()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    const-string/jumbo v1, " id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvB()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const-string/jumbo v1, " to=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvO()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_4
    const-string/jumbo v1, " from=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvx()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_5
    const-string/jumbo v1, " chid=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/xiaomi/smack/packet/i;->cvN()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    :cond_6
    const-string/jumbo v1, " type=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/smack/packet/i;->cUw:Lcom/xiaomi/smack/packet/Presence$Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    :cond_7
    const-string/jumbo v1, "<status>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/smack/packet/i;->cUu:Ljava/lang/String;

    invoke-static {v2}, Lcom/xiaomi/smack/c/e;->cwJ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</status>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    :cond_8
    const-string/jumbo v1, "<priority>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/xiaomi/smack/packet/i;->cUv:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</priority>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_9
    iget-object v1, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    sget-object v2, Lcom/xiaomi/smack/packet/Presence$Mode;->cUD:Lcom/xiaomi/smack/packet/Presence$Mode;

    if-eq v1, v2, :cond_0

    const-string/jumbo v1, "<show>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "</show>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    :cond_a
    invoke-virtual {v1}, Lcom/xiaomi/smack/packet/j;->cwk()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9
.end method

.method public cwf(Lcom/xiaomi/smack/packet/Presence$Mode;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/i;->cUx:Lcom/xiaomi/smack/packet/Presence$Mode;

    return-void
.end method

.method public cwg(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/i;->cUu:Ljava/lang/String;

    return-void
.end method

.method public cwh(Lcom/xiaomi/smack/packet/Presence$Type;)V
    .locals 2

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/xiaomi/smack/packet/i;->cUw:Lcom/xiaomi/smack/packet/Presence$Type;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Type cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cwi(I)V
    .locals 3

    const/16 v0, -0x80

    if-ge p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Priority value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " is not valid. Valid range is -128 through 128."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/16 v0, 0x80

    if-gt p1, v0, :cond_0

    iput p1, p0, Lcom/xiaomi/smack/packet/i;->cUv:I

    return-void
.end method
