.class Lcom/xiaomi/smack/d;
.super Lcom/xiaomi/push/service/n;
.source "SocketConnection.java"


# instance fields
.field final synthetic cVj:Ljava/lang/Exception;

.field final synthetic cVk:I

.field final synthetic cVl:Lcom/xiaomi/smack/j;


# direct methods
.method constructor <init>(Lcom/xiaomi/smack/j;IILjava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/d;->cVl:Lcom/xiaomi/smack/j;

    iput p3, p0, Lcom/xiaomi/smack/d;->cVk:I

    iput-object p4, p0, Lcom/xiaomi/smack/d;->cVj:Ljava/lang/Exception;

    invoke-direct {p0, p2}, Lcom/xiaomi/push/service/n;-><init>(I)V

    return-void
.end method


# virtual methods
.method public cxd()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "shutdown the connection. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/smack/d;->cVk:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/smack/d;->cVj:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cxe()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/smack/d;->cVl:Lcom/xiaomi/smack/j;

    iget-object v0, v0, Lcom/xiaomi/smack/j;->cVW:Lcom/xiaomi/push/service/XMPushService;

    iget v1, p0, Lcom/xiaomi/smack/d;->cVk:I

    iget-object v2, p0, Lcom/xiaomi/smack/d;->cVj:Ljava/lang/Exception;

    invoke-virtual {v0, v1, v2}, Lcom/xiaomi/push/service/XMPushService;->cPK(ILjava/lang/Exception;)V

    return-void
.end method
