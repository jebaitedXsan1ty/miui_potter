.class public Lcom/xiaomi/smack/h;
.super Ljava/lang/Object;
.source "ConnectionConfiguration.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final cVN:Ljava/lang/String;

.field public static cVT:Ljava/lang/String;

.field public static cVU:Ljava/lang/String;


# instance fields
.field private cVO:Z

.field private cVP:Ljava/lang/String;

.field private cVQ:Lcom/xiaomi/smack/i;

.field private cVR:I

.field private cVS:Z

.field private cVV:Ljava/lang/String;

.field private host:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "wcc-ml-test10.bj"

    sput-object v0, Lcom/xiaomi/smack/h;->cVT:Ljava/lang/String;

    sget-object v0, Lcom/xiaomi/channel/commonutils/h/o;->cXA:Ljava/lang/String;

    sput-object v0, Lcom/xiaomi/smack/h;->cVN:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/smack/h;->cVU:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;ILjava/lang/String;Lcom/xiaomi/smack/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v0, Lcom/xiaomi/smack/e;->cVs:Z

    iput-boolean v0, p0, Lcom/xiaomi/smack/h;->cVO:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/xiaomi/smack/h;->cVS:Z

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/xiaomi/smack/h;->cya(Ljava/util/Map;ILjava/lang/String;Lcom/xiaomi/smack/i;)V

    return-void
.end method

.method public static final cxS(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/xiaomi/smack/h;->cVU:Ljava/lang/String;

    return-void
.end method

.method private cya(Ljava/util/Map;ILjava/lang/String;Lcom/xiaomi/smack/i;)V
    .locals 0

    iput p2, p0, Lcom/xiaomi/smack/h;->cVR:I

    iput-object p3, p0, Lcom/xiaomi/smack/h;->cVV:Ljava/lang/String;

    iput-object p4, p0, Lcom/xiaomi/smack/h;->cVQ:Lcom/xiaomi/smack/i;

    return-void
.end method

.method public static final cyc()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/xiaomi/smack/h;->cVU:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/xiaomi/channel/commonutils/h/k;->cBm()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/xiaomi/channel/commonutils/h/k;->cBl()Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "app.chat.xiaomi.net"

    return-object v0

    :cond_0
    sget-object v0, Lcom/xiaomi/smack/h;->cVU:Ljava/lang/String;

    return-object v0

    :cond_1
    const-string/jumbo v0, "sandbox.xmpush.xiaomi.com"

    return-object v0

    :cond_2
    sget-object v0, Lcom/xiaomi/smack/h;->cVN:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public cxT()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/smack/h;->cVO:Z

    return v0
.end method

.method public cxU(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/h;->host:Ljava/lang/String;

    return-void
.end method

.method public cxV()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/h;->host:Ljava/lang/String;

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/smack/h;->host:Ljava/lang/String;

    return-object v0

    :cond_0
    invoke-static {}, Lcom/xiaomi/smack/h;->cyc()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/smack/h;->host:Ljava/lang/String;

    goto :goto_0
.end method

.method public cxW()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/h;->cVP:Ljava/lang/String;

    return-object v0
.end method

.method public cxX(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/xiaomi/smack/h;->cVO:Z

    return-void
.end method

.method public cxY()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public cxZ(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/smack/h;->cVP:Ljava/lang/String;

    return-void
.end method

.method public cyb()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/smack/h;->cVR:I

    return v0
.end method
