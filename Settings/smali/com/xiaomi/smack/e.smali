.class public abstract Lcom/xiaomi/smack/e;
.super Ljava/lang/Object;
.source "Connection.java"


# static fields
.field private static final cVr:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static cVs:Z


# instance fields
.field protected volatile cVA:J

.field protected final cVB:Ljava/util/Map;

.field protected volatile cVC:J

.field protected cVD:Ljava/lang/String;

.field protected final cVE:I

.field protected cVF:Lcom/xiaomi/smack/a/a;

.field protected cVm:Lcom/xiaomi/smack/h;

.field private cVn:I

.field protected cVo:Ljava/lang/String;

.field protected cVp:J

.field protected final cVq:Ljava/util/Map;

.field protected cVt:Lcom/xiaomi/push/service/XMPushService;

.field private cVu:Ljava/util/LinkedList;

.field protected cVv:I

.field private final cVw:Ljava/util/Collection;

.field private cVx:J

.field protected cVy:J

.field protected cVz:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/xiaomi/smack/e;->cVr:Ljava/util/concurrent/atomic/AtomicInteger;

    sput-boolean v1, Lcom/xiaomi/smack/e;->cVs:Z

    :try_start_0
    const-string/jumbo v0, "smack.debugEnabled"

    invoke-static {v0}, Ljava/lang/Boolean;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/xiaomi/smack/e;->cVs:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Lcom/xiaomi/smack/f;->cxO()Ljava/lang/String;

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected constructor <init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/smack/h;)V
    .locals 4

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/xiaomi/smack/e;->cVz:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/xiaomi/smack/e;->cVy:J

    iput-wide v2, p0, Lcom/xiaomi/smack/e;->cVC:J

    iput-wide v2, p0, Lcom/xiaomi/smack/e;->cVA:J

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/smack/e;->cVu:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/smack/e;->cVw:Ljava/util/Collection;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/smack/e;->cVB:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/smack/e;->cVq:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/smack/e;->cVF:Lcom/xiaomi/smack/a/a;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/e;->cVD:Ljava/lang/String;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/smack/e;->cVo:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p0, Lcom/xiaomi/smack/e;->cVn:I

    sget-object v0, Lcom/xiaomi/smack/e;->cVr:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lcom/xiaomi/smack/e;->cVE:I

    iput-wide v2, p0, Lcom/xiaomi/smack/e;->cVx:J

    iput-wide v2, p0, Lcom/xiaomi/smack/e;->cVp:J

    iput-object p2, p0, Lcom/xiaomi/smack/e;->cVm:Lcom/xiaomi/smack/h;

    iput-object p1, p0, Lcom/xiaomi/smack/e;->cVt:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {p0}, Lcom/xiaomi/smack/e;->cxh()V

    return-void
.end method

.method private cxj(I)V
    .locals 6

    iget-object v1, p0, Lcom/xiaomi/smack/e;->cVu:Ljava/util/LinkedList;

    monitor-enter v1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVu:Ljava/util/LinkedList;

    new-instance v2, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVu:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x6

    if-gt v0, v2, :cond_1

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVu:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVu:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private cxx(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const-string/jumbo v0, "unknown"

    return-object v0

    :cond_0
    const-string/jumbo v0, "connected"

    return-object v0

    :cond_1
    const-string/jumbo v0, "connecting"

    return-object v0

    :cond_2
    const-string/jumbo v0, "disconnected"

    return-object v0
.end method


# virtual methods
.method public cxA()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/xiaomi/smack/e;->cVn:I

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract cxB(Lcom/xiaomi/smack/packet/d;)V
.end method

.method public declared-synchronized cxC()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/smack/e;->cVx:J

    sub-long/2addr v2, v4

    invoke-static {}, Lcom/xiaomi/smack/f;->cxM()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cxD()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVm:Lcom/xiaomi/smack/h;

    invoke-virtual {v0}, Lcom/xiaomi/smack/h;->cxW()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public cxE(Lcom/xiaomi/smack/k;Lcom/xiaomi/smack/b/a;)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Lcom/xiaomi/smack/c;

    invoke-direct {v0, p1, p2}, Lcom/xiaomi/smack/c;-><init>(Lcom/xiaomi/smack/k;Lcom/xiaomi/smack/b/a;)V

    iget-object v1, p0, Lcom/xiaomi/smack/e;->cVq:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Packet listener is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cxF()V
    .locals 2

    iget-object v1, p0, Lcom/xiaomi/smack/e;->cVu:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVu:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract cxG(Lcom/xiaomi/push/service/as;)V
.end method

.method public abstract cxH(ILjava/lang/Exception;)V
.end method

.method public cxI()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/smack/e;->cVv:I

    return v0
.end method

.method public abstract cxJ(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public cxK()J
    .locals 2

    iget-wide v0, p0, Lcom/xiaomi/smack/e;->cVA:J

    return-wide v0
.end method

.method public declared-synchronized cxL()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/smack/e;->cVp:J

    sub-long/2addr v2, v4

    invoke-static {}, Lcom/xiaomi/smack/f;->cxM()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected cxh()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVm:Lcom/xiaomi/smack/h;

    invoke-virtual {v0}, Lcom/xiaomi/smack/h;->cxT()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVF:Lcom/xiaomi/smack/a/a;

    if-nez v0, :cond_0

    :try_start_0
    const-string/jumbo v0, "smack.debuggerClass"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_1
    if-nez v0, :cond_2

    :goto_2
    if-eqz v1, :cond_3

    const/4 v0, 0x3

    :try_start_1
    new-array v0, v0, [Ljava/lang/Class;

    const-class v2, Lcom/xiaomi/smack/e;

    const/4 v3, 0x0

    aput-object v2, v0, v3

    const-class v2, Ljava/io/Writer;

    const/4 v3, 0x1

    aput-object v2, v0, v3

    const-class v2, Ljava/io/Reader;

    const/4 v3, 0x2

    aput-object v2, v0, v3

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/a/a;

    iput-object v0, p0, Lcom/xiaomi/smack/e;->cVF:Lcom/xiaomi/smack/a/a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Can\'t initialize the configured debugger!"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/xiaomi/f/a/b;

    invoke-direct {v0, p0}, Lcom/xiaomi/f/a/b;-><init>(Lcom/xiaomi/smack/e;)V

    iput-object v0, p0, Lcom/xiaomi/smack/e;->cVF:Lcom/xiaomi/smack/a/a;

    goto :goto_0
.end method

.method public cxi(Lcom/xiaomi/smack/k;Lcom/xiaomi/smack/b/a;)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Lcom/xiaomi/smack/c;

    invoke-direct {v0, p1, p2}, Lcom/xiaomi/smack/c;-><init>(Lcom/xiaomi/smack/k;Lcom/xiaomi/smack/b/a;)V

    iget-object v1, p0, Lcom/xiaomi/smack/e;->cVB:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "Packet listener is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cxk()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVm:Lcom/xiaomi/smack/h;

    invoke-virtual {v0}, Lcom/xiaomi/smack/h;->cxV()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized cxl()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/xiaomi/smack/e;->cVx:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cxm(IILjava/lang/Exception;)V
    .locals 6

    const/16 v5, 0xa

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/xiaomi/smack/e;->cVn:I

    if-ne p1, v0, :cond_1

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVt:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/g/b;->cAI(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    if-eq p1, v3, :cond_3

    if-eqz p1, :cond_5

    if-eq p1, v4, :cond_7

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget v1, p0, Lcom/xiaomi/smack/e;->cVn:I

    invoke-direct {p0, v1}, Lcom/xiaomi/smack/e;->cxx(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string/jumbo v1, "update the connection status. %1$s -> %2$s : %3$s "

    invoke-direct {p0, p1}, Lcom/xiaomi/smack/e;->cxx(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    invoke-static {p2}, Lcom/xiaomi/push/service/b;->cMv(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/xiaomi/smack/e;->cxj(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVt:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v5}, Lcom/xiaomi/push/service/XMPushService;->cPp(I)V

    iget v0, p0, Lcom/xiaomi/smack/e;->cVn:I

    if-nez v0, :cond_4

    :goto_3
    iput p1, p0, Lcom/xiaomi/smack/e;->cVn:I

    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVw:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/a;

    invoke-interface {v0, p0}, Lcom/xiaomi/smack/a;->cwZ(Lcom/xiaomi/smack/e;)V

    goto :goto_4

    :cond_4
    const-string/jumbo v0, "try set connected while not connecting."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    iget v0, p0, Lcom/xiaomi/smack/e;->cVn:I

    if-ne v0, v4, :cond_6

    :goto_5
    iput p1, p0, Lcom/xiaomi/smack/e;->cVn:I

    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVw:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/a;

    invoke-interface {v0, p0}, Lcom/xiaomi/smack/a;->cwY(Lcom/xiaomi/smack/e;)V

    goto :goto_6

    :cond_6
    const-string/jumbo v0, "try set connecting while not disconnected."

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    goto :goto_5

    :cond_7
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVt:Lcom/xiaomi/push/service/XMPushService;

    invoke-virtual {v0, v5}, Lcom/xiaomi/push/service/XMPushService;->cPp(I)V

    iget v0, p0, Lcom/xiaomi/smack/e;->cVn:I

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/xiaomi/smack/e;->cVn:I

    if-eq v0, v3, :cond_b

    :cond_8
    iput p1, p0, Lcom/xiaomi/smack/e;->cVn:I

    goto/16 :goto_2

    :cond_9
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVw:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/a;

    if-eqz p3, :cond_a

    move-object v1, p3

    :goto_8
    invoke-interface {v0, p0, v1}, Lcom/xiaomi/smack/a;->cxb(Lcom/xiaomi/smack/e;Ljava/lang/Exception;)V

    goto :goto_7

    :cond_a
    new-instance v1, Ljava/util/concurrent/CancellationException;

    const-string/jumbo v3, "disconnect while connecting"

    invoke-direct {v1, v3}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    goto :goto_8

    :cond_b
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVw:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/a;

    invoke-interface {v0, p0, p2, p3}, Lcom/xiaomi/smack/a;->cxa(Lcom/xiaomi/smack/e;ILjava/lang/Exception;)V

    goto :goto_9
.end method

.method public cxn(Lcom/xiaomi/smack/a;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVw:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public cxo()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/smack/e;->cVz:I

    return v0
.end method

.method public abstract cxp([Lcom/xiaomi/a/e;)V
.end method

.method public cxq(Lcom/xiaomi/smack/a;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVw:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVw:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public cxr()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/xiaomi/smack/e;->cxH(ILjava/lang/Exception;)V

    return-void
.end method

.method public declared-synchronized cxs(J)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lcom/xiaomi/smack/e;->cVx:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v2, p1

    if-gez v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized cxt(Ljava/lang/String;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/xiaomi/smack/e;->cVn:I

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ignore setChallenge because connection was disconnected"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setChallenge hash = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/xiaomi/channel/commonutils/c/c;->czm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czB(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/xiaomi/smack/e;->cVD:Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Lcom/xiaomi/smack/e;->cxm(IILjava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract cxu(Z)V
.end method

.method public cxv()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public cxw()I
    .locals 1

    iget v0, p0, Lcom/xiaomi/smack/e;->cVn:I

    return v0
.end method

.method public cxy()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/xiaomi/smack/e;->cVn:I

    if-eq v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public abstract cxz(Lcom/xiaomi/a/e;)V
.end method

.method public getConfiguration()Lcom/xiaomi/smack/h;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/smack/e;->cVm:Lcom/xiaomi/smack/h;

    return-object v0
.end method
