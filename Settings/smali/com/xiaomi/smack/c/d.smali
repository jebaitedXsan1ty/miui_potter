.class public Lcom/xiaomi/smack/c/d;
.super Ljava/lang/Object;
.source "TrafficUtils.java"


# static fields
.field private static cUK:Lcom/xiaomi/push/d/a;

.field private static cUL:Ljava/lang/String;

.field private static cUM:Ljava/util/List;

.field private static cUN:Lcom/xiaomi/channel/commonutils/h/n;

.field private static cUO:I

.field private static final cUP:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/xiaomi/channel/commonutils/h/n;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/xiaomi/channel/commonutils/h/n;-><init>(Z)V

    sput-object v0, Lcom/xiaomi/smack/c/d;->cUN:Lcom/xiaomi/channel/commonutils/h/n;

    const/4 v0, -0x1

    sput v0, Lcom/xiaomi/smack/c/d;->cUO:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/xiaomi/smack/c/d;->cUP:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/smack/c/d;->cUM:Ljava/util/List;

    const-string/jumbo v0, ""

    sput-object v0, Lcom/xiaomi/smack/c/d;->cUL:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/xiaomi/smack/c/d;->cUK:Lcom/xiaomi/push/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cwA(Landroid/content/Context;Ljava/lang/String;JZJ)V
    .locals 11

    if-nez p0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.xiaomi.xmsf"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "com.xiaomi.xmsf"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/xiaomi/smack/c/d;->cws(Landroid/content/Context;)I

    move-result v4

    const/4 v0, -0x1

    if-eq v0, v4, :cond_2

    sget-object v9, Lcom/xiaomi/smack/c/d;->cUP:Ljava/lang/Object;

    monitor-enter v9

    :try_start_0
    sget-object v0, Lcom/xiaomi/smack/c/d;->cUM:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v10

    new-instance v0, Lcom/xiaomi/smack/c/g;

    if-nez p4, :cond_3

    const/4 v5, 0x0

    :goto_0
    if-eqz v4, :cond_4

    const-string/jumbo v6, ""

    :goto_1
    invoke-static {v4, p2, p3}, Lcom/xiaomi/smack/c/d;->cwy(IJ)J

    move-result-wide v7

    move-object v1, p1

    move-wide/from16 v2, p5

    invoke-direct/range {v0 .. v8}, Lcom/xiaomi/smack/c/g;-><init>(Ljava/lang/String;JIILjava/lang/String;J)V

    invoke-static {v0}, Lcom/xiaomi/smack/c/d;->cww(Lcom/xiaomi/smack/c/g;)V

    monitor-exit v9

    if-nez v10, :cond_5

    :goto_2
    return-void

    :cond_2
    return-void

    :cond_3
    const/4 v5, 0x1

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lcom/xiaomi/smack/c/d;->cwC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    sget-object v0, Lcom/xiaomi/smack/c/d;->cUN:Lcom/xiaomi/channel/commonutils/h/n;

    new-instance v1, Lcom/xiaomi/smack/c/a;

    invoke-direct {v1, p0}, Lcom/xiaomi/smack/c/a;-><init>(Landroid/content/Context;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/xiaomi/channel/commonutils/h/n;->cBv(Lcom/xiaomi/channel/commonutils/h/b;J)V

    goto :goto_2
.end method

.method public static cwB(Ljava/lang/String;)I
    .locals 1

    :try_start_0
    const-string/jumbo v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method private static declared-synchronized cwC(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-class v1, Lcom/xiaomi/smack/c/d;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/xiaomi/channel/commonutils/android/e;->cAf()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/xiaomi/smack/c/d;->cUL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :try_start_1
    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    :goto_0
    :try_start_2
    sget-object v0, Lcom/xiaomi/smack/c/d;->cUL:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_3
    const-string/jumbo v0, ""
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_1
    :try_start_4
    sget-object v0, Lcom/xiaomi/smack/c/d;->cUL:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_2
    :try_start_5
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/smack/c/d;->cUL:Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static cwD(Landroid/content/Context;)V
    .locals 1

    invoke-static {p0}, Lcom/xiaomi/smack/c/d;->cwE(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/xiaomi/smack/c/d;->cUO:I

    return-void
.end method

.method private static cwE(Landroid/content/Context;)I
    .locals 2

    const/4 v1, -0x1

    :try_start_0
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    return v0

    :catch_0
    move-exception v0

    return v1

    :cond_0
    return v1

    :catch_1
    move-exception v0

    return v1

    :cond_1
    return v1
.end method

.method private static cws(Landroid/content/Context;)I
    .locals 2

    sget v0, Lcom/xiaomi/smack/c/d;->cUO:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    sget v0, Lcom/xiaomi/smack/c/d;->cUO:I

    return v0

    :cond_0
    invoke-static {p0}, Lcom/xiaomi/smack/c/d;->cwE(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/xiaomi/smack/c/d;->cUO:I

    goto :goto_0
.end method

.method private static cwt(Landroid/content/Context;Ljava/util/List;)V
    .locals 8

    :try_start_0
    sget-object v1, Lcom/xiaomi/push/d/a;->dkW:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {p0}, Lcom/xiaomi/smack/c/d;->cwz(Landroid/content/Context;)Lcom/xiaomi/push/d/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/push/d/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_1
    return-void

    :cond_0
    :try_start_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/c/g;

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    iget-object v5, v0, Lcom/xiaomi/smack/c/g;->packageName:Ljava/lang/String;

    const-string/jumbo v6, "package_name"

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v6, v0, Lcom/xiaomi/smack/c/g;->messageTs:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-string/jumbo v6, "message_ts"

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget v5, v0, Lcom/xiaomi/smack/c/g;->cUZ:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string/jumbo v6, "network_type"

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-wide v6, v0, Lcom/xiaomi/smack/c/g;->cVa:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const-string/jumbo v6, "bytes"

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget v5, v0, Lcom/xiaomi/smack/c/g;->cUY:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string/jumbo v6, "rcv"

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, v0, Lcom/xiaomi/smack/c/g;->cUX:Ljava/lang/String;

    const-string/jumbo v5, "imsi"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "traffic"

    const/4 v5, 0x0

    invoke-virtual {v2, v0, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_6
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czu(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method static synthetic cwu()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/xiaomi/smack/c/d;->cUM:Ljava/util/List;

    return-object v0
.end method

.method static synthetic cwv()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/xiaomi/smack/c/d;->cUP:Ljava/lang/Object;

    return-object v0
.end method

.method private static cww(Lcom/xiaomi/smack/c/g;)V
    .locals 6

    sget-object v0, Lcom/xiaomi/smack/c/d;->cUM:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/xiaomi/smack/c/d;->cUM:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/smack/c/g;

    invoke-virtual {v0, p0}, Lcom/xiaomi/smack/c/g;->cwK(Lcom/xiaomi/smack/c/g;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, v0, Lcom/xiaomi/smack/c/g;->cVa:J

    iget-wide v4, p0, Lcom/xiaomi/smack/c/g;->cVa:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/xiaomi/smack/c/g;->cVa:J

    return-void
.end method

.method static synthetic cwx(Landroid/content/Context;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/xiaomi/smack/c/d;->cwt(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method private static cwy(IJ)J
    .locals 5

    if-eqz p0, :cond_0

    const/16 v0, 0xb

    :goto_0
    int-to-long v0, v0

    mul-long/2addr v0, p1

    const-wide/16 v2, 0xa

    div-long/2addr v0, v2

    return-wide v0

    :cond_0
    const/16 v0, 0xd

    goto :goto_0
.end method

.method private static cwz(Landroid/content/Context;)Lcom/xiaomi/push/d/a;
    .locals 1

    sget-object v0, Lcom/xiaomi/smack/c/d;->cUK:Lcom/xiaomi/push/d/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/xiaomi/push/d/a;

    invoke-direct {v0, p0}, Lcom/xiaomi/push/d/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/xiaomi/smack/c/d;->cUK:Lcom/xiaomi/push/d/a;

    sget-object v0, Lcom/xiaomi/smack/c/d;->cUK:Lcom/xiaomi/push/d/a;

    return-object v0

    :cond_0
    sget-object v0, Lcom/xiaomi/smack/c/d;->cUK:Lcom/xiaomi/push/d/a;

    return-object v0
.end method
