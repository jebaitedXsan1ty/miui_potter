.class public Lcom/xiaomi/e/d;
.super Ljava/lang/Object;
.source "StatsHelper.java"


# static fields
.field private static final dBg:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/xiaomi/push/thrift/ChannelStatsType;->djQ:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v0}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v0

    sput v0, Lcom/xiaomi/e/d;->dBg:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dgd()V
    .locals 4

    sget v0, Lcom/xiaomi/e/d;->dBg:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-static {v2, v0, v1, v3}, Lcom/xiaomi/e/d;->dgh(IILjava/lang/String;I)V

    return-void
.end method

.method public static dge(I)V
    .locals 2

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/e/e;->dgs()Lcom/xiaomi/push/thrift/StatsEvent;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djV:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v1}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTD(I)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/e/e;->dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V

    return-void
.end method

.method public static dgf(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    :try_start_0
    invoke-static {p1}, Lcom/xiaomi/e/k;->dgF(Ljava/lang/Exception;)Lcom/xiaomi/e/c;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/e/e;->dgs()Lcom/xiaomi/push/thrift/StatsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v2}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/thrift/StatsEvent;->cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;

    iget-object v0, v0, Lcom/xiaomi/e/c;->annotation:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTv(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v1, p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTw(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/e/e;->dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static dgg(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    :try_start_0
    invoke-static {p1}, Lcom/xiaomi/e/k;->dgC(Ljava/lang/Exception;)Lcom/xiaomi/e/c;

    move-result-object v0

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/e/e;->dgs()Lcom/xiaomi/push/thrift/StatsEvent;

    move-result-object v1

    iget-object v2, v0, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v2}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/xiaomi/push/thrift/StatsEvent;->cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;

    iget-object v0, v0, Lcom/xiaomi/e/c;->annotation:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTv(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v1, p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTw(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/xiaomi/e/e;->dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static declared-synchronized dgh(IILjava/lang/String;I)V
    .locals 8

    const-class v1, Lcom/xiaomi/e/d;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    shl-int/lit8 v0, p0, 0x18

    or-int/2addr v0, p1

    sget-object v4, Lcom/xiaomi/e/h;->dBt:Ljava/util/Hashtable;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v0, "stats key not found"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/xiaomi/e/e;->dgs()Lcom/xiaomi/push/thrift/StatsEvent;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;

    sget-object v5, Lcom/xiaomi/e/h;->dBt:Ljava/util/Hashtable;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v2, v6

    long-to-int v0, v2

    invoke-virtual {v4, v0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTU(I)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v4, p2}, Lcom/xiaomi/push/thrift/StatsEvent;->cTw(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    const/4 v0, -0x1

    if-gt p3, v0, :cond_1

    :goto_1
    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/xiaomi/e/e;->dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V

    sget-object v0, Lcom/xiaomi/e/h;->dBt:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_2
    invoke-virtual {v4, p3}, Lcom/xiaomi/push/thrift/StatsEvent;->cTD(I)Lcom/xiaomi/push/thrift/StatsEvent;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public static dgi(IIILjava/lang/String;I)V
    .locals 2

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/e/e;->dgs()Lcom/xiaomi/push/thrift/StatsEvent;

    move-result-object v0

    int-to-byte v1, p0

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTP(B)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v0, p2}, Lcom/xiaomi/push/thrift/StatsEvent;->cTU(I)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v0, p3}, Lcom/xiaomi/push/thrift/StatsEvent;->cTw(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v0, p4}, Lcom/xiaomi/push/thrift/StatsEvent;->cTD(I)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/e/e;->dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V

    return-void
.end method

.method public static dgj(Ljava/lang/String;ILjava/lang/Exception;)V
    .locals 3

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/e/e;->dgs()Lcom/xiaomi/push/thrift/StatsEvent;

    move-result-object v0

    if-gtz p1, :cond_0

    :try_start_0
    invoke-static {p2}, Lcom/xiaomi/e/k;->dgE(Ljava/lang/Exception;)Lcom/xiaomi/e/c;

    move-result-object v1

    iget-object v2, v1, Lcom/xiaomi/e/c;->dBf:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v2}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/xiaomi/push/thrift/StatsEvent;->cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;

    iget-object v1, v1, Lcom/xiaomi/e/c;->annotation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTv(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTw(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/e/e;->dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->dkg:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v1}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v0, p0}, Lcom/xiaomi/push/thrift/StatsEvent;->cTw(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-virtual {v0, p1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTU(I)Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/xiaomi/e/e;->dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static dgk(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V
    .locals 1

    new-instance v0, Lcom/xiaomi/e/f;

    invoke-direct {v0, p0, p1}, Lcom/xiaomi/e/f;-><init>(Lcom/xiaomi/push/service/XMPushService;Lcom/xiaomi/push/service/as;)V

    invoke-virtual {v0}, Lcom/xiaomi/e/f;->dgA()V

    return-void
.end method

.method public static dgl()V
    .locals 2

    sget v0, Lcom/xiaomi/e/d;->dBg:I

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/xiaomi/e/d;->dgn(II)V

    return-void
.end method

.method public static dgm()[B
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/xiaomi/e/e;->getInstance()Lcom/xiaomi/e/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/e/e;->dgo()Lcom/xiaomi/push/thrift/StatsEvents;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v1}, Lcom/xiaomi/xmpush/thrift/a;->daG(Lorg/apache/thrift/TBase;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized dgn(II)V
    .locals 6

    const-class v1, Lcom/xiaomi/e/d;

    monitor-enter v1

    const v0, 0xffffff

    if-lt p1, v0, :cond_0

    :try_start_0
    const-string/jumbo v0, "stats key should less than 16777215"

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    shl-int/lit8 v0, p0, 0x18

    or-int/2addr v0, p1

    :try_start_1
    sget-object v2, Lcom/xiaomi/e/h;->dBt:Ljava/util/Hashtable;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
