.class public Lcom/xiaomi/e/e;
.super Ljava/lang/Object;
.source "StatsHandler.java"


# instance fields
.field private dBh:J

.field private dBi:Lcom/xiaomi/channel/commonutils/d/a;

.field private dBj:I

.field private dBk:Z

.field private dBl:Lcom/xiaomi/e/b;

.field private uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/xiaomi/e/e;->dBk:Z

    invoke-static {}, Lcom/xiaomi/channel/commonutils/d/a;->czr()Lcom/xiaomi/channel/commonutils/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/e/e;->dBi:Lcom/xiaomi/channel/commonutils/d/a;

    return-void
.end method

.method private dgt(Lcom/xiaomi/channel/commonutils/d/b;)Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 2

    const/4 v0, 0x0

    iget v1, p1, Lcom/xiaomi/channel/commonutils/d/b;->key:I

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/xiaomi/e/e;->dgs()Lcom/xiaomi/push/thrift/StatsEvent;

    move-result-object v0

    sget-object v1, Lcom/xiaomi/push/thrift/ChannelStatsType;->djV:Lcom/xiaomi/push/thrift/ChannelStatsType;

    invoke-virtual {v1}, Lcom/xiaomi/push/thrift/ChannelStatsType;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTA(I)Lcom/xiaomi/push/thrift/StatsEvent;

    iget v1, p1, Lcom/xiaomi/channel/commonutils/d/b;->key:I

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTD(I)Lcom/xiaomi/push/thrift/StatsEvent;

    iget-object v1, p1, Lcom/xiaomi/channel/commonutils/d/b;->annotation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTv(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p1, Lcom/xiaomi/channel/commonutils/d/b;->cWo:Ljava/lang/Object;

    instance-of v1, v1, Lcom/xiaomi/push/thrift/StatsEvent;

    if-eqz v1, :cond_0

    iget-object v0, p1, Lcom/xiaomi/channel/commonutils/d/b;->cWo:Ljava/lang/Object;

    check-cast v0, Lcom/xiaomi/push/thrift/StatsEvent;

    goto :goto_0
.end method

.method private dgu()V
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/xiaomi/e/e;->dBk:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/xiaomi/e/e;->dBh:J

    sub-long/2addr v2, v4

    iget v0, p0, Lcom/xiaomi/e/e;->dBj:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/xiaomi/e/e;->dBk:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/xiaomi/e/e;->dBh:J

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private dgv(I)Lcom/xiaomi/push/thrift/StatsEvents;
    .locals 7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/xiaomi/push/thrift/StatsEvents;

    iget-object v0, p0, Lcom/xiaomi/e/e;->uuid:Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lcom/xiaomi/push/thrift/StatsEvents;-><init>(Ljava/lang/String;Ljava/util/List;)V

    iget-object v0, p0, Lcom/xiaomi/e/e;->dBl:Lcom/xiaomi/e/b;

    iget-object v0, v0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/g/b;->cAE(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    new-instance v3, Lorg/apache/thrift/transport/c;

    invoke-direct {v3, p1}, Lorg/apache/thrift/transport/c;-><init>(I)V

    new-instance v0, Lorg/apache/thrift/protocol/XmPushTBinaryProtocol$Factory;

    invoke-direct {v0}, Lorg/apache/thrift/protocol/XmPushTBinaryProtocol$Factory;-><init>()V

    invoke-virtual {v0, v3}, Lorg/apache/thrift/protocol/XmPushTBinaryProtocol$Factory;->ert(Lorg/apache/thrift/transport/a;)Lorg/apache/thrift/protocol/a;

    move-result-object v4

    :try_start_0
    invoke-virtual {v2, v4}, Lcom/xiaomi/push/thrift/StatsEvents;->cTu(Lorg/apache/thrift/protocol/a;)V
    :try_end_0
    .catch Lorg/apache/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/e/e;->dBi:Lcom/xiaomi/channel/commonutils/d/a;

    invoke-virtual {v0}, Lcom/xiaomi/channel/commonutils/d/a;->czq()Ljava/util/LinkedList;

    move-result-object v5

    :goto_2
    :try_start_1
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/apache/thrift/TException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v0

    if-gtz v0, :cond_2

    :cond_0
    :goto_3
    return-object v2

    :cond_1
    iget-object v0, p0, Lcom/xiaomi/e/e;->dBl:Lcom/xiaomi/e/b;

    iget-object v0, v0, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/android/c;->czZ(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/xiaomi/push/thrift/StatsEvents;->cTn(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvents;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-virtual {v5}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/channel/commonutils/d/b;

    invoke-direct {p0, v0}, Lcom/xiaomi/e/e;->dgt(Lcom/xiaomi/channel/commonutils/d/b;)Lcom/xiaomi/push/thrift/StatsEvent;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_4
    invoke-virtual {v3}, Lorg/apache/thrift/transport/c;->erq()I

    move-result v6

    if-gt v6, p1, :cond_0

    if-nez v0, :cond_4

    :goto_5
    invoke-virtual {v5}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_3

    :cond_3
    invoke-virtual {v0, v4}, Lcom/xiaomi/push/thrift/StatsEvent;->cTu(Lorg/apache/thrift/protocol/a;)V

    goto :goto_4

    :catch_2
    move-exception v0

    goto :goto_3

    :cond_4
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/apache/thrift/TException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_5
.end method

.method public static getContext()Lcom/xiaomi/e/b;
    .locals 2

    sget-object v1, Lcom/xiaomi/e/i;->dBu:Lcom/xiaomi/e/e;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/xiaomi/e/i;->dBu:Lcom/xiaomi/e/e;

    iget-object v0, v0, Lcom/xiaomi/e/e;->dBl:Lcom/xiaomi/e/b;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance()Lcom/xiaomi/e/e;
    .locals 1

    sget-object v0, Lcom/xiaomi/e/i;->dBu:Lcom/xiaomi/e/e;

    return-object v0
.end method


# virtual methods
.method declared-synchronized dgo()Lcom/xiaomi/push/thrift/StatsEvents;
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/e/e;->dgx()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/16 v0, 0x2ee

    :try_start_1
    iget-object v1, p0, Lcom/xiaomi/e/e;->dBl:Lcom/xiaomi/e/b;

    iget-object v1, v1, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/g/b;->cAE(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/xiaomi/e/e;->dgv(I)Lcom/xiaomi/push/thrift/StatsEvents;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v0, 0x177

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dgp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/xiaomi/e/e;->dBk:Z

    return v0
.end method

.method public declared-synchronized dgq(Lcom/xiaomi/push/service/XMPushService;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/xiaomi/e/b;

    invoke-direct {v0, p1}, Lcom/xiaomi/e/b;-><init>(Lcom/xiaomi/push/service/XMPushService;)V

    iput-object v0, p0, Lcom/xiaomi/e/e;->dBl:Lcom/xiaomi/e/b;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/xiaomi/e/e;->uuid:Ljava/lang/String;

    invoke-static {}, Lcom/xiaomi/push/service/an;->getInstance()Lcom/xiaomi/push/service/an;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/e/j;

    invoke-direct {v1, p0}, Lcom/xiaomi/e/j;-><init>(Lcom/xiaomi/e/e;)V

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/service/an;->cQE(Lcom/xiaomi/push/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized dgr(Lcom/xiaomi/push/thrift/StatsEvent;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/e/e;->dBi:Lcom/xiaomi/channel/commonutils/d/a;

    invoke-virtual {v0, p1}, Lcom/xiaomi/channel/commonutils/d/a;->czp(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized dgs()Lcom/xiaomi/push/thrift/StatsEvent;
    .locals 6

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/xiaomi/push/thrift/StatsEvent;

    invoke-direct {v0}, Lcom/xiaomi/push/thrift/StatsEvent;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/e/e;->dBl:Lcom/xiaomi/e/b;

    iget-object v1, v1, Lcom/xiaomi/e/b;->dAU:Lcom/xiaomi/push/service/XMPushService;

    invoke-static {v1}, Lcom/xiaomi/channel/commonutils/g/b;->cAN(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTQ(Ljava/lang/String;)Lcom/xiaomi/push/thrift/StatsEvent;

    const/4 v1, 0x0

    iput-byte v1, v0, Lcom/xiaomi/push/thrift/StatsEvent;->chid:B

    const/4 v1, 0x1

    iput v1, v0, Lcom/xiaomi/push/thrift/StatsEvent;->value:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTL(I)Lcom/xiaomi/push/thrift/StatsEvent;

    iget-object v1, p0, Lcom/xiaomi/e/e;->dBl:Lcom/xiaomi/e/b;

    iget-object v1, v1, Lcom/xiaomi/e/b;->dAZ:Lcom/xiaomi/smack/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/xiaomi/e/e;->dBl:Lcom/xiaomi/e/b;

    iget-object v1, v1, Lcom/xiaomi/e/b;->dAZ:Lcom/xiaomi/smack/e;

    invoke-virtual {v1}, Lcom/xiaomi/smack/e;->cxI()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/push/thrift/StatsEvent;->cTI(I)Lcom/xiaomi/push/thrift/StatsEvent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public dgw(I)V
    .locals 4

    const v1, 0x240c8400

    if-gtz p1, :cond_0

    :goto_0
    return-void

    :cond_0
    mul-int/lit16 v0, p1, 0x3e8

    if-gt v0, v1, :cond_2

    :goto_1
    iget v1, p0, Lcom/xiaomi/e/e;->dBj:I

    if-eq v1, v0, :cond_3

    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/xiaomi/e/e;->dBk:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/xiaomi/e/e;->dBh:J

    iput v0, p0, Lcom/xiaomi/e/e;->dBj:I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "enable dot duration = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " start = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/xiaomi/e/e;->dBh:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/channel/commonutils/e/a;->czE(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-boolean v1, p0, Lcom/xiaomi/e/e;->dBk:Z

    if-eqz v1, :cond_1

    goto :goto_0
.end method

.method dgx()Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/xiaomi/e/e;->dgu()V

    iget-boolean v1, p0, Lcom/xiaomi/e/e;->dBk:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/xiaomi/e/e;->dBi:Lcom/xiaomi/channel/commonutils/d/a;

    invoke-virtual {v1}, Lcom/xiaomi/channel/commonutils/d/a;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
