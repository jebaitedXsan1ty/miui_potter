.class public Lcom/xiaomi/miui/pushads/sdk/MiPushRelayTraceService;
.super Landroid/app/Service;
.source "MiPushRelayTraceService.java"


# static fields
.field private static cSl:Lcom/xiaomi/miui/pushads/sdk/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {}, Lcom/xiaomi/miui/pushads/sdk/a/a;->getInstance()Lcom/xiaomi/miui/pushads/sdk/a/a;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/miui/pushads/sdk/MiPushRelayTraceService;->cSl:Lcom/xiaomi/miui/pushads/sdk/a/a;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    if-nez p1, :cond_0

    return v6

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v0, "intenttype"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string/jumbo v2, "id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string/jumbo v4, "showType"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Lcom/xiaomi/miui/pushads/sdk/b/b;

    invoke-direct {v5}, Lcom/xiaomi/miui/pushads/sdk/b/b;-><init>()V

    iput-wide v2, v5, Lcom/xiaomi/miui/pushads/sdk/b/b;->cSk:J

    iput v4, v5, Lcom/xiaomi/miui/pushads/sdk/b/b;->cSj:I

    const-string/jumbo v2, ""

    iput-object v2, v5, Lcom/xiaomi/miui/pushads/sdk/b/b;->content:Ljava/lang/String;

    sget-object v2, Lcom/xiaomi/miui/pushads/sdk/MiPushRelayTraceService;->cSl:Lcom/xiaomi/miui/pushads/sdk/a/a;

    if-nez v2, :cond_1

    const-string/jumbo v0, "MiPushRelayTraceService"

    const-string/jumbo v1, "log sender is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v6

    :cond_1
    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_0
    const-string/jumbo v0, "notifyid"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v0, "action\uff0cremove noti"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/l;->cuN(Ljava/lang/String;)V

    const-string/jumbo v0, "notification"

    invoke-virtual {p0, v0}, Lcom/xiaomi/miui/pushads/sdk/MiPushRelayTraceService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    :try_start_0
    const-class v0, Landroid/content/Context;

    const-string/jumbo v1, "STATUS_BAR_SERVICE"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/xiaomi/miui/pushads/sdk/MiPushRelayTraceService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string/jumbo v2, "collapse"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "\u5173\u95edstatus bar \u6210\u529f"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    :goto_1
    return v6

    :pswitch_0
    const-string/jumbo v0, "clickT:"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/l;->cuN(Ljava/lang/String;)V

    sget-object v0, Lcom/xiaomi/miui/pushads/sdk/MiPushRelayTraceService;->cSl:Lcom/xiaomi/miui/pushads/sdk/a/a;

    invoke-virtual {v0, v5}, Lcom/xiaomi/miui/pushads/sdk/a/a;->ctw(Lcom/xiaomi/miui/pushads/sdk/b/b;)V

    const-string/jumbo v0, "pendingintent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    :try_start_1
    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/app/PendingIntent$CanceledException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "deleteT:"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/l;->cuN(Ljava/lang/String;)V

    sget-object v0, Lcom/xiaomi/miui/pushads/sdk/MiPushRelayTraceService;->cSl:Lcom/xiaomi/miui/pushads/sdk/a/a;

    invoke-virtual {v0, v5}, Lcom/xiaomi/miui/pushads/sdk/a/a;->cty(Lcom/xiaomi/miui/pushads/sdk/b/b;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string/jumbo v0, "ads-notify-fd5dfce4"

    const-string/jumbo v1, "Reflect failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
