.class public Lcom/xiaomi/miui/pushads/sdk/a/a;
.super Ljava/lang/Object;
.source "AdsLogSender.java"

# interfaces
.implements Lcom/xiaomi/miui/pushads/sdk/a/g;


# static fields
.field private static cRI:Lcom/xiaomi/miui/pushads/sdk/a/a;


# instance fields
.field private cRH:I

.field private cRJ:Ljava/lang/String;

.field private cRK:I

.field private cRL:Ljava/lang/String;

.field private cRM:Lcom/xiaomi/miui/pushads/sdk/a/b;

.field private cRN:I

.field private cRO:Ljava/util/HashMap;

.field private cRP:Ljava/lang/String;

.field private cRQ:I

.field private mContext:Landroid/content/Context;


# direct methods
.method private ctA(Ljava/util/ArrayList;Ljava/lang/String;I)V
    .locals 3

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/xiaomi/miui/pushads/sdk/a/a;->ctC(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/a/h;->ctO(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/xiaomi/miui/pushads/sdk/a/e;

    invoke-direct {v2, p3, v0, v1}, Lcom/xiaomi/miui/pushads/sdk/a/e;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/xiaomi/miui/pushads/sdk/a/a;->ctD(Lcom/xiaomi/miui/pushads/sdk/a/e;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/xiaomi/miui/pushads/sdk/a/e;

    invoke-direct {v2, p3, v0, v1}, Lcom/xiaomi/miui/pushads/sdk/a/e;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/xiaomi/miui/pushads/sdk/a/a;->ctB(Lcom/xiaomi/miui/pushads/sdk/a/e;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private ctB(Lcom/xiaomi/miui/pushads/sdk/a/e;)V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRO:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/xiaomi/miui/pushads/sdk/a/e;->cRX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRK:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRK:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "send: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRK:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/a/h;->ctL(Ljava/lang/String;)V

    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/a/d;

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRL:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRP:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/xiaomi/miui/pushads/sdk/a/d;-><init>(Lcom/xiaomi/miui/pushads/sdk/a/g;Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/miui/pushads/sdk/a/e;)V

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRO:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/xiaomi/miui/pushads/sdk/a/e;->cRX:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/xiaomi/miui/pushads/sdk/a/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private ctC(Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    const/4 v3, 0x0

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRJ:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "imei"

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRJ:Ljava/lang/String;

    invoke-static {v1}, Lcom/xiaomi/miui/pushads/sdk/a/h;->ctO(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    const-string/jumbo v0, "actionType"

    invoke-virtual {v4, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string/jumbo v0, "actionTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v0, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/miui/pushads/sdk/b/b;

    iget-object v0, v0, Lcom/xiaomi/miui/pushads/sdk/b/b;->content:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    move-object v2, v0

    :goto_2
    const-string/jumbo v6, "adId"

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/miui/pushads/sdk/b/b;

    iget-wide v8, v0, Lcom/xiaomi/miui/pushads/sdk/b/b;->cSk:J

    invoke-virtual {v2, v6, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/miui/pushads/sdk/b/b;

    iget-object v0, v0, Lcom/xiaomi/miui/pushads/sdk/b/b;->content:Ljava/lang/String;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    goto :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v0, "com.xiaomi.miui.ads.pushsdk"

    const-string/jumbo v2, "content \u4e0d\u662fjson\u4e32"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    goto :goto_1

    :cond_2
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, v5}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    const-string/jumbo v1, "adList"

    invoke-virtual {v4, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v2, v0

    goto :goto_2
.end method

.method private ctD(Lcom/xiaomi/miui/pushads/sdk/a/e;)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/a/c;->ctH(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/xiaomi/miui/pushads/sdk/a/a;->ctz(Lcom/xiaomi/miui/pushads/sdk/a/e;)V

    const/4 v0, 0x0

    return v0
.end method

.method private ctz(Lcom/xiaomi/miui/pushads/sdk/a/e;)V
    .locals 2

    iget v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRQ:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRQ:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "cacheCount: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRQ:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/a/h;->ctL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRM:Lcom/xiaomi/miui/pushads/sdk/a/b;

    invoke-virtual {v0, p1}, Lcom/xiaomi/miui/pushads/sdk/a/b;->ctF(Lcom/xiaomi/miui/pushads/sdk/a/e;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRM:Lcom/xiaomi/miui/pushads/sdk/a/b;

    invoke-virtual {v0}, Lcom/xiaomi/miui/pushads/sdk/a/b;->ctE()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/xiaomi/miui/pushads/sdk/a/a;
    .locals 2

    const-class v0, Lcom/xiaomi/miui/pushads/sdk/a/a;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRI:Lcom/xiaomi/miui/pushads/sdk/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public ctv(Lcom/xiaomi/miui/pushads/sdk/b/b;)V
    .locals 4

    iget-wide v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/b;->cSk:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "received"

    iget v2, p1, Lcom/xiaomi/miui/pushads/sdk/b/b;->cSj:I

    invoke-direct {p0, v0, v1, v2}, Lcom/xiaomi/miui/pushads/sdk/a/a;->ctA(Ljava/util/ArrayList;Ljava/lang/String;I)V

    return-void
.end method

.method public ctw(Lcom/xiaomi/miui/pushads/sdk/b/b;)V
    .locals 4

    iget-wide v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/b;->cSk:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "click"

    iget v2, p1, Lcom/xiaomi/miui/pushads/sdk/b/b;->cSj:I

    invoke-direct {p0, v0, v1, v2}, Lcom/xiaomi/miui/pushads/sdk/a/a;->ctA(Ljava/util/ArrayList;Ljava/lang/String;I)V

    return-void
.end method

.method public ctx(Ljava/lang/Integer;Lcom/xiaomi/miui/pushads/sdk/a/e;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRO:Ljava/util/HashMap;

    iget-object v1, p2, Lcom/xiaomi/miui/pushads/sdk/a/e;->cRX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRH:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRH:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "faild: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRH:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/xiaomi/miui/pushads/sdk/a/e;->cRX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRO:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/a/h;->ctL(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/xiaomi/miui/pushads/sdk/a/a;->ctz(Lcom/xiaomi/miui/pushads/sdk/a/e;)V

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRO:Ljava/util/HashMap;

    iget-object v1, p2, Lcom/xiaomi/miui/pushads/sdk/a/e;->cRX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRN:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRN:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "success: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/a;->cRN:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/a/h;->ctL(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cty(Lcom/xiaomi/miui/pushads/sdk/b/b;)V
    .locals 4

    iget-wide v0, p1, Lcom/xiaomi/miui/pushads/sdk/b/b;->cSk:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "remove"

    iget v2, p1, Lcom/xiaomi/miui/pushads/sdk/b/b;->cSj:I

    invoke-direct {p0, v0, v1, v2}, Lcom/xiaomi/miui/pushads/sdk/a/a;->ctA(Ljava/util/ArrayList;Ljava/lang/String;I)V

    return-void
.end method
