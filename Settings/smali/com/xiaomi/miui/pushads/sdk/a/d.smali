.class Lcom/xiaomi/miui/pushads/sdk/a/d;
.super Landroid/os/AsyncTask;
.source "AdsLogTraceTask.java"


# instance fields
.field cRT:Lcom/xiaomi/miui/pushads/sdk/a/g;

.field cRU:Ljava/lang/String;

.field cRV:Ljava/lang/String;

.field cRW:Lcom/xiaomi/miui/pushads/sdk/a/e;


# direct methods
.method public constructor <init>(Lcom/xiaomi/miui/pushads/sdk/a/g;Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/miui/pushads/sdk/a/e;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRV:Ljava/lang/String;

    iput-object p3, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRU:Ljava/lang/String;

    iput-object p1, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRT:Lcom/xiaomi/miui/pushads/sdk/a/g;

    iput-object p4, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRW:Lcom/xiaomi/miui/pushads/sdk/a/e;

    return-void
.end method


# virtual methods
.method protected varargs ctI([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRV:Ljava/lang/String;

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRU:Ljava/lang/String;

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRW:Lcom/xiaomi/miui/pushads/sdk/a/e;

    invoke-static {v0, v1, v2}, Lcom/xiaomi/miui/pushads/sdk/a/c;->ctG(Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/miui/pushads/sdk/a/e;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected ctJ(Ljava/lang/Integer;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRT:Lcom/xiaomi/miui/pushads/sdk/a/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRT:Lcom/xiaomi/miui/pushads/sdk/a/g;

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRW:Lcom/xiaomi/miui/pushads/sdk/a/e;

    invoke-interface {v0, p1, v1}, Lcom/xiaomi/miui/pushads/sdk/a/g;->ctx(Ljava/lang/Integer;Lcom/xiaomi/miui/pushads/sdk/a/e;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/xiaomi/miui/pushads/sdk/a/d;->ctI([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 3

    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRT:Lcom/xiaomi/miui/pushads/sdk/a/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRT:Lcom/xiaomi/miui/pushads/sdk/a/g;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/a/d;->cRW:Lcom/xiaomi/miui/pushads/sdk/a/e;

    invoke-interface {v0, v1, v2}, Lcom/xiaomi/miui/pushads/sdk/a/g;->ctx(Ljava/lang/Integer;Lcom/xiaomi/miui/pushads/sdk/a/e;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/xiaomi/miui/pushads/sdk/a/d;->ctJ(Ljava/lang/Integer;)V

    return-void
.end method
