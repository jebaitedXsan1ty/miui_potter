.class Lcom/xiaomi/miui/pushads/sdk/j;
.super Landroid/os/AsyncTask;
.source "NotifyAdsDownloader.java"


# instance fields
.field private cSJ:Ljava/lang/String;

.field private cSK:I

.field private cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

.field private cSM:Lcom/xiaomi/miui/pushads/sdk/a;

.field private cSN:Ljava/lang/String;

.field private cSO:Landroid/content/SharedPreferences;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;ILjava/lang/String;Lcom/xiaomi/miui/pushads/sdk/a;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/miui/pushads/sdk/j;->mContext:Landroid/content/Context;

    iput-object p6, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSM:Lcom/xiaomi/miui/pushads/sdk/a;

    iput-object p3, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSN:Ljava/lang/String;

    iput-object p2, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSO:Landroid/content/SharedPreferences;

    iput-object p5, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSJ:Ljava/lang/String;

    return-void
.end method

.method private cuC(Lorg/json/JSONObject;)I
    .locals 8

    const/4 v7, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/xiaomi/miui/pushads/sdk/j;->cuI(Lorg/json/JSONObject;)I

    move-result v3

    :try_start_0
    const-string/jumbo v0, "miui.util.NotificationFilterHelper"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v2, "canSendNotifications"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, Landroid/content/Context;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const-class v5, Ljava/lang/String;

    const/4 v6, 0x1

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSJ:Ljava/lang/String;

    invoke-static {v2}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/xiaomi/miui/pushads/sdk/j;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    aput-object v4, v2, v5

    iget-object v4, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSJ:Ljava/lang/String;

    const/4 v5, 0x1

    aput-object v4, v2, v5

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "\u662f\u5426\u7981\u7528\u4e86\u901a\u77e5\u680f\u5e7f\u544a "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    const-string/jumbo v2, "receiveUpperBound"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    invoke-direct {p0, v2, v3}, Lcom/xiaomi/miui/pushads/sdk/j;->cuG(II)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u662f\u5426\u8fbe\u5230\u4e0a\u9650 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    if-nez v2, :cond_0

    if-ne v3, v7, :cond_5

    if-eqz v0, :cond_5

    :cond_0
    :try_start_1
    const-string/jumbo v2, "\u4f7f\u7528\u5019\u9009\u5e7f\u544a "

    invoke-static {v2}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    const-string/jumbo v2, "subAdId"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    const-string/jumbo v0, "\u6ca1\u6709\u5019\u9009\u5e7f\u544a "

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v0, -0x5

    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v2, "NotifyAdsDownloader"

    const-string/jumbo v4, "reflect errors!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    :try_start_2
    const-string/jumbo v2, "subAdInfo"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/xiaomi/miui/pushads/sdk/j;->cuI(Lorg/json/JSONObject;)I

    move-result v2

    if-ne v2, v7, :cond_3

    if-eqz v0, :cond_3

    const/4 v0, -0x6

    return v0

    :cond_3
    invoke-direct {p0, v3}, Lcom/xiaomi/miui/pushads/sdk/j;->cuH(Lorg/json/JSONObject;)I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u5019\u9009\u5e7f\u544a\u89e3\u6790\u53c2\u6570\u5e76\u68c0\u67e5\uff1a "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    if-eqz v0, :cond_4

    return v0

    :cond_4
    invoke-direct {p0, v2}, Lcom/xiaomi/miui/pushads/sdk/j;->cuL(I)Lcom/xiaomi/miui/pushads/sdk/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

    invoke-virtual {v0, v3}, Lcom/xiaomi/miui/pushads/sdk/b/a;->ctQ(Lorg/json/JSONObject;)V

    :goto_2
    return v1

    :cond_5
    const-string/jumbo v0, "\u4f7f\u7528\u4e3b\u5e7f\u544a "

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/xiaomi/miui/pushads/sdk/j;->cuL(I)Lcom/xiaomi/miui/pushads/sdk/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

    invoke-virtual {v0, p1}, Lcom/xiaomi/miui/pushads/sdk/b/a;->ctQ(Lorg/json/JSONObject;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    const/4 v0, -0x1

    return v0
.end method

.method private cuE(I)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, ""

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "\u6210\u529f"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "\u672a\u77e5\u539f\u56e0"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "\u8fc7\u671f"

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "\u5230\u8fbe\u4e0a\u9650"

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "\u5e7f\u544a\u5931\u6548"

    goto :goto_0

    :pswitch_5
    const-string/jumbo v0, "\u6d88\u606f\u4e0d\u5339\u914d"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x5
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private cuG(II)Z
    .locals 12

    const-wide/16 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSO:Landroid/content/SharedPreferences;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSO:Landroid/content/SharedPreferences;

    const-string/jumbo v4, "starttime"

    const-wide/16 v6, 0x0

    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v0, v4, v10

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSO:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v4, "starttime"

    invoke-interface {v0, v4, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return v9

    :cond_0
    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSO:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "starttime"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSO:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "notifycount"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSO:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "bubblecount"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return v9

    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSO:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "notifycount"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-ge v0, p1, :cond_3

    monitor-exit v1

    return v9

    :cond_2
    if-ne p2, v9, :cond_3

    :try_start_3
    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSO:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "bubblecount"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    mul-int/lit8 v2, p1, 0x4

    if-ge v0, v2, :cond_3

    monitor-exit v1

    return v9

    :cond_3
    :try_start_4
    const-string/jumbo v0, "\u8d85\u8fc7\u4e86\u6bcf\u5929\u63a5\u53d7\u5e7f\u544a\u7684\u4e0a\u9650"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/b;->ctY(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit v1

    return v8

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private cuH(Lorg/json/JSONObject;)I
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    const/4 v2, -0x1

    if-nez p1, :cond_0

    return v2

    :cond_0
    const-string/jumbo v0, "status"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "success"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    return v2

    :cond_1
    const-string/jumbo v0, "nonsense"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v1, "MIUIADSPUSH"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\u5e7f\u544a\u65e0\u6548\u6807\u5fd7\u8bbe\u7f6e: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "\u5e7f\u544a\u65e0\u6548"

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    const/4 v0, -0x2

    return v0

    :cond_2
    const-string/jumbo v0, "lastShowTime"

    invoke-virtual {p1, v0, v8, v9}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "expireTime: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " currentTime: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/miui/pushads/sdk/l;->cuN(Ljava/lang/String;)V

    cmp-long v2, v0, v8

    if-eqz v2, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\u5e7f\u544a\u5df2\u7ecf\u8fc7\u671f lastShow: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " current: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    const/4 v0, -0x4

    return v0

    :cond_3
    return v6
.end method

.method private cuI(Lorg/json/JSONObject;)I
    .locals 1

    const-string/jumbo v0, "showType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private cuJ(Ljava/lang/String;)I
    .locals 4

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0, v0}, Lcom/xiaomi/miui/pushads/sdk/j;->cuH(Lorg/json/JSONObject;)I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\u89e3\u6790\u53c2\u6570\u5e76\u68c0\u67e5, \u8fd4\u56de\u7ed3\u679c: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v1}, Lcom/xiaomi/miui/pushads/sdk/j;->cuE(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    if-eqz v1, :cond_0

    return v1

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    return v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/xiaomi/miui/pushads/sdk/j;->cuC(Lorg/json/JSONObject;)I

    move-result v0

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\u5e7f\u544a\u83b7\u53d6\u6700\u7ec8\u7ed3\u679c\uff1a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " \u7c7b\u578b: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

    iget v2, v2, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/miui/pushads/sdk/g;->cun(Ljava/lang/String;)V

    :cond_1
    return v0
.end method

.method private cuK(Ljava/io/File;)I
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

    check-cast v0, Lcom/xiaomi/miui/pushads/sdk/f;

    iget-object v1, v0, Lcom/xiaomi/miui/pushads/sdk/f;->cSy:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/j;->mContext:Landroid/content/Context;

    invoke-static {v2, p1, v1, v0}, Lcom/xiaomi/miui/pushads/sdk/d;->cue(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Lcom/xiaomi/miui/pushads/sdk/f;)I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\u4e0b\u8f7d\u5e7f\u544a imgUrl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " \u7ed3\u679c\uff1a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/miui/pushads/sdk/j;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/xiaomi/miui/pushads/sdk/j;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "asynctask \u88abcancel"

    invoke-static {v1}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return v0

    :cond_3
    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/j;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/xiaomi/miui/pushads/sdk/h;->cuB(Landroid/content/Context;)Lcom/xiaomi/miui/pushads/sdk/NotifyAdsManager$NetState;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\u7f51\u7edc\u7c7b\u578b\u6539\u53d8\uff0c\u4e2d\u65ad\u4e0b\u8f7d: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private cuL(I)Lcom/xiaomi/miui/pushads/sdk/b/a;
    .locals 2

    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/b/a;

    invoke-direct {v0}, Lcom/xiaomi/miui/pushads/sdk/b/a;-><init>()V

    packed-switch p1, :pswitch_data_0

    :goto_0
    iget v1, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSK:I

    iput v1, v0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSh:I

    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSN:Ljava/lang/String;

    iput-object v1, v0, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSi:Ljava/lang/String;

    return-object v0

    :pswitch_0
    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/i;

    invoke-direct {v0}, Lcom/xiaomi/miui/pushads/sdk/i;-><init>()V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/xiaomi/miui/pushads/sdk/f;

    invoke-direct {v0}, Lcom/xiaomi/miui/pushads/sdk/f;-><init>()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected varargs cuD([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSN:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/xiaomi/miui/pushads/sdk/j;->cuJ(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\u5e7f\u544a\u89e3\u6790\u5931\u8d25 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

    iget v1, v1, Lcom/xiaomi/miui/pushads/sdk/b/a;->cSc:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "comxiaomimiuipushadssdk"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/miui/pushads/sdk/j;->cuK(Ljava/io/File;)I

    move-result v0

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected cuF(Ljava/lang/Integer;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSM:Lcom/xiaomi/miui/pushads/sdk/a;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "\u4e0b\u8f7d post \u7684\u7ed3\u679c\u662f: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/miui/pushads/sdk/b;->ctZ(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSM:Lcom/xiaomi/miui/pushads/sdk/a;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/xiaomi/miui/pushads/sdk/j;->cSL:Lcom/xiaomi/miui/pushads/sdk/b/a;

    invoke-interface {v0, v1, v2, p0}, Lcom/xiaomi/miui/pushads/sdk/a;->ctX(ILcom/xiaomi/miui/pushads/sdk/b/a;Lcom/xiaomi/miui/pushads/sdk/j;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/xiaomi/miui/pushads/sdk/j;->cuD([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    const-string/jumbo v0, "ADS_DOWNLOAD"

    const-string/jumbo v1, "onCancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/xiaomi/miui/pushads/sdk/j;->cuF(Ljava/lang/Integer;)V

    return-void
.end method
