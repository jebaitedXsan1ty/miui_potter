.class Lcom/miui/a/a/a/f;
.super Lse/dirac/acs/api/AudioControlService$Connection;
.source "SongDiracUtils.java"


# instance fields
.field final synthetic cRm:Lcom/miui/a/a/a/e;


# direct methods
.method constructor <init>(Lcom/miui/a/a/a/e;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    invoke-direct {p0}, Lse/dirac/acs/api/AudioControlService$Connection;-><init>()V

    return-void
.end method


# virtual methods
.method public csT(Lse/dirac/acs/api/AudioControlService;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    invoke-static {v0}, Lcom/miui/a/a/a/e;->csN(Lcom/miui/a/a/a/e;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    invoke-static {v0}, Lcom/miui/a/a/a/e;->csO(Lcom/miui/a/a/a/e;)Lse/dirac/acs/api/AudioControlService;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    invoke-static {v0, p1}, Lcom/miui/a/a/a/e;->csS(Lcom/miui/a/a/a/e;Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService;

    :cond_0
    iget-object v0, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    invoke-static {v0}, Lcom/miui/a/a/a/e;->csR(Lcom/miui/a/a/a/e;)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    invoke-static {v0}, Lcom/miui/a/a/a/e;->csP(Lcom/miui/a/a/a/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    invoke-static {v0}, Lcom/miui/a/a/a/e;->csQ(Lcom/miui/a/a/a/e;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    invoke-static {v0}, Lcom/miui/a/a/a/e;->csQ(Lcom/miui/a/a/a/e;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onServiceDisconnected()V
    .locals 2

    iget-object v0, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    invoke-static {v0}, Lcom/miui/a/a/a/e;->csN(Lcom/miui/a/a/a/e;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/miui/a/a/a/f;->cRm:Lcom/miui/a/a/a/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/a/a/a/e;->csS(Lcom/miui/a/a/a/e;Lse/dirac/acs/api/AudioControlService;)Lse/dirac/acs/api/AudioControlService;

    return-void
.end method
