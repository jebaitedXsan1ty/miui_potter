.class public final Lcom/google/protobuf/micro/d;
.super Ljava/lang/Object;
.source "ByteStringMicro.java"


# static fields
.field public static final dCc:Lcom/google/protobuf/micro/d;


# instance fields
.field private final dCb:[B

.field private volatile dCd:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/protobuf/micro/d;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/protobuf/micro/d;-><init>([B)V

    sput-object v0, Lcom/google/protobuf/micro/d;->dCc:Lcom/google/protobuf/micro/d;

    return-void
.end method

.method private constructor <init>([B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/micro/d;->dCd:I

    iput-object p1, p0, Lcom/google/protobuf/micro/d;->dCb:[B

    return-void
.end method

.method public static diF([B)Lcom/google/protobuf/micro/d;
    .locals 2

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/google/protobuf/micro/d;->diG([BII)Lcom/google/protobuf/micro/d;

    move-result-object v0

    return-object v0
.end method

.method public static diG([BII)Lcom/google/protobuf/micro/d;
    .locals 2

    new-array v0, p2, [B

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v1, Lcom/google/protobuf/micro/d;

    invoke-direct {v1, v0}, Lcom/google/protobuf/micro/d;-><init>([B)V

    return-object v1
.end method


# virtual methods
.method public diE()[B
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/protobuf/micro/d;->dCb:[B

    array-length v0, v0

    new-array v1, v0, [B

    iget-object v2, p0, Lcom/google/protobuf/micro/d;->dCb:[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    if-eq p1, p0, :cond_0

    instance-of v0, p1, Lcom/google/protobuf/micro/d;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/protobuf/micro/d;

    iget-object v0, p0, Lcom/google/protobuf/micro/d;->dCb:[B

    array-length v2, v0

    iget-object v0, p1, Lcom/google/protobuf/micro/d;->dCb:[B

    array-length v0, v0

    if-ne v2, v0, :cond_2

    iget-object v3, p0, Lcom/google/protobuf/micro/d;->dCb:[B

    iget-object v4, p1, Lcom/google/protobuf/micro/d;->dCb:[B

    move v0, v1

    :goto_0
    if-lt v0, v2, :cond_3

    return v7

    :cond_0
    return v7

    :cond_1
    return v1

    :cond_2
    return v1

    :cond_3
    aget-byte v5, v3, v0

    aget-byte v6, v4, v0

    if-ne v5, v6, :cond_4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    return v1
.end method

.method public hashCode()I
    .locals 5

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/protobuf/micro/d;->dCd:I

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v4, p0, Lcom/google/protobuf/micro/d;->dCb:[B

    iget-object v0, p0, Lcom/google/protobuf/micro/d;->dCb:[B

    array-length v2, v0

    move v0, v2

    :goto_1
    if-lt v1, v2, :cond_1

    if-eqz v0, :cond_2

    :goto_2
    iput v0, p0, Lcom/google/protobuf/micro/d;->dCd:I

    goto :goto_0

    :cond_1
    mul-int/lit8 v0, v0, 0x1f

    aget-byte v3, v4, v1

    add-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/protobuf/micro/d;->dCb:[B

    array-length v0, v0

    return v0
.end method
