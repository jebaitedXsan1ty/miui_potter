.class public final Lcom/google/protobuf/micro/a;
.super Ljava/lang/Object;
.source "CodedInputStreamMicro.java"


# instance fields
.field private dBM:I

.field private dBN:I

.field private dBO:I

.field private final dBP:Ljava/io/InputStream;

.field private dBQ:I

.field private dBR:I

.field private dBS:I

.field private dBT:I

.field private dBU:I

.field private final dBV:[B

.field private dBW:I


# direct methods
.method private constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBR:I

    const/16 v0, 0x40

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBU:I

    const/high16 v0, 0x4000000

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBN:I

    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    iput v1, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iput v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    iput-object p1, p0, Lcom/google/protobuf/micro/a;->dBP:Ljava/io/InputStream;

    return-void
.end method

.method private constructor <init>([BII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBR:I

    const/16 v0, 0x40

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBU:I

    const/high16 v0, 0x4000000

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBN:I

    iput-object p1, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    add-int v0, p2, p3

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iput p2, p0, Lcom/google/protobuf/micro/a;->dBO:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/micro/a;->dBP:Ljava/io/InputStream;

    return-void
.end method

.method public static dhA([BII)Lcom/google/protobuf/micro/a;
    .locals 1

    new-instance v0, Lcom/google/protobuf/micro/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/protobuf/micro/a;-><init>([BII)V

    return-object v0
.end method

.method private dhm()V
    .locals 2

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBS:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBR:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBS:I

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/google/protobuf/micro/a;->dBR:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBS:I

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBS:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    goto :goto_0
.end method

.method public static dht(Ljava/io/InputStream;)Lcom/google/protobuf/micro/a;
    .locals 1

    new-instance v0, Lcom/google/protobuf/micro/a;

    invoke-direct {v0, p0}, Lcom/google/protobuf/micro/a;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method private dhx(Z)Z
    .locals 4

    const/4 v1, -0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBO:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    if-lt v0, v2, :cond_1

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBR:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iput v3, p0, Lcom/google/protobuf/micro/a;->dBO:I

    iget-object v0, p0, Lcom/google/protobuf/micro/a;->dBP:Ljava/io/InputStream;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/protobuf/micro/a;->dBP:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    if-nez v0, :cond_5

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "InputStream#read(byte[]) returned invalid result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\nThe InputStream implementation is buggy."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "refillBuffer() called when buffer wasn\'t empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p1, :cond_3

    return v3

    :cond_3
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diL()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    if-eq v0, v1, :cond_7

    invoke-direct {p0}, Lcom/google/protobuf/micro/a;->dhm()V

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBS:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBN:I

    if-le v0, v1, :cond_9

    :cond_6
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diK()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_7
    iput v3, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    if-nez p1, :cond_8

    return v3

    :cond_8
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diL()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_9
    if-ltz v0, :cond_6

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public dhB(I)V
    .locals 1

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBT:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diN()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0
.end method

.method public dhC(I)Z
    .locals 3

    const/4 v2, 0x1

    invoke-static {p1}, Lcom/google/protobuf/micro/e;->diP(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diH()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhv()I

    return v2

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhy()J

    return v2

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhi()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/a;->dhu(I)V

    return v2

    :pswitch_3
    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhz()V

    invoke-static {p1}, Lcom/google/protobuf/micro/e;->diQ(I)I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/e;->diR(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/a;->dhB(I)V

    return v2

    :pswitch_4
    const/4 v0, 0x0

    return v0

    :pswitch_5
    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhl()I

    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public dhd(I)V
    .locals 0

    iput p1, p0, Lcom/google/protobuf/micro/a;->dBR:I

    invoke-direct {p0}, Lcom/google/protobuf/micro/a;->dhm()V

    return-void
.end method

.method public dhe()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhi()I

    move-result v0

    return v0
.end method

.method public dhf()Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/protobuf/micro/a;->dhx(Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dhg(I)I
    .locals 2

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBR:I

    if-gt v0, v1, :cond_1

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBR:I

    invoke-direct {p0}, Lcom/google/protobuf/micro/a;->dhm()V

    return v1

    :cond_0
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diJ()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_1
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diL()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0
.end method

.method public dhh(Lcom/google/protobuf/micro/c;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhi()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBW:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBU:I

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/a;->dhg(I)I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBW:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/protobuf/micro/a;->dBW:I

    invoke-virtual {p1, p0}, Lcom/google/protobuf/micro/c;->cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/protobuf/micro/a;->dhB(I)V

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBW:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/protobuf/micro/a;->dBW:I

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/a;->dhd(I)V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diO()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0
.end method

.method public dhi()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v0

    if-gez v0, :cond_0

    and-int/lit8 v0, v0, 0x7f

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v2

    if-gez v2, :cond_1

    and-int/lit8 v2, v2, 0x7f

    shl-int/lit8 v2, v2, 0x7

    or-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v2

    if-gez v2, :cond_2

    and-int/lit8 v2, v2, 0x7f

    shl-int/lit8 v2, v2, 0xe

    or-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v2

    if-gez v2, :cond_3

    and-int/lit8 v2, v2, 0x7f

    shl-int/lit8 v2, v2, 0x15

    or-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v2

    shl-int/lit8 v3, v2, 0x1c

    or-int/2addr v0, v3

    if-ltz v2, :cond_5

    :goto_0
    return v0

    :cond_0
    return v0

    :cond_1
    shl-int/lit8 v1, v2, 0x7

    or-int/2addr v0, v1

    goto :goto_0

    :cond_2
    shl-int/lit8 v1, v2, 0xe

    or-int/2addr v0, v1

    goto :goto_0

    :cond_3
    shl-int/lit8 v1, v2, 0x15

    or-int/2addr v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v2

    if-gez v2, :cond_6

    add-int/lit8 v1, v1, 0x1

    :cond_5
    const/4 v2, 0x5

    if-lt v1, v2, :cond_4

    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diM()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_6
    return v0
.end method

.method public dhj()I
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhf()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhi()I

    move-result v0

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBT:I

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBT:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBT:I

    return v0

    :cond_0
    iput v1, p0, Lcom/google/protobuf/micro/a;->dBT:I

    return v1

    :cond_1
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diI()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0
.end method

.method public dhk()J
    .locals 6

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    :goto_0
    const/16 v3, 0x40

    if-lt v2, v3, :cond_0

    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diM()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v3

    and-int/lit8 v4, v3, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v2

    or-long/2addr v0, v4

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_1

    add-int/lit8 v2, v2, 0x7

    goto :goto_0

    :cond_1
    return-wide v0
.end method

.method public dhl()I
    .locals 4

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v0

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v1

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v3

    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, v2, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 v1, v3, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public dhn()J
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhk()J

    move-result-wide v0

    return-wide v0
.end method

.method public dho()J
    .locals 2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhk()J

    move-result-wide v0

    return-wide v0
.end method

.method public dhp()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhi()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dhq()Lcom/google/protobuf/micro/d;
    .locals 3

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhi()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBO:I

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/a;->dhw(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/micro/d;->diF([B)Lcom/google/protobuf/micro/d;

    move-result-object v0

    return-object v0

    :cond_1
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBO:I

    invoke-static {v1, v2, v0}, Lcom/google/protobuf/micro/d;->diG([BII)Lcom/google/protobuf/micro/d;

    move-result-object v1

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBO:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBO:I

    return-object v1
.end method

.method public dhr()B
    .locals 3

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBO:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    if-eq v0, v1, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/protobuf/micro/a;->dBO:I

    aget-byte v0, v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/protobuf/micro/a;->dhx(Z)Z

    goto :goto_0
.end method

.method public dhs()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhi()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBO:I

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/a;->dhw(I)[B

    move-result-object v0

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v1

    :cond_1
    if-lez v0, :cond_0

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    iget v3, p0, Lcom/google/protobuf/micro/a;->dBO:I

    const-string/jumbo v4, "UTF-8"

    invoke-direct {v1, v2, v3, v0, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBO:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBO:I

    return-object v1
.end method

.method public dhu(I)V
    .locals 4

    const/4 v3, 0x0

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBR:I

    if-gt v0, v1, :cond_1

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    sub-int/2addr v0, v1

    if-le p1, v0, :cond_2

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iput v3, p0, Lcom/google/protobuf/micro/a;->dBO:I

    iput v3, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    move v1, v0

    :goto_0
    if-lt v1, p1, :cond_3

    :goto_1
    return-void

    :cond_0
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diJ()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/protobuf/micro/a;->dBR:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBM:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/a;->dhu(I)V

    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diL()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_2
    iget v0, p0, Lcom/google/protobuf/micro/a;->dBO:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBO:I

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/protobuf/micro/a;->dBP:Ljava/io/InputStream;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/protobuf/micro/a;->dBP:Ljava/io/InputStream;

    sub-int v2, p1, v1

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    long-to-int v0, v2

    :goto_2
    if-lez v0, :cond_5

    add-int/2addr v1, v0

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBM:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    goto :goto_0

    :cond_4
    const/4 v0, -0x1

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diL()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0
.end method

.method public dhv()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhi()I

    move-result v0

    return v0
.end method

.method public dhw(I)[B
    .locals 11

    const/16 v10, 0x1000

    const/4 v5, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBO:I

    add-int/2addr v0, v2

    add-int/2addr v0, p1

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBR:I

    if-gt v0, v2, :cond_1

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBO:I

    sub-int/2addr v0, v2

    if-le p1, v0, :cond_2

    if-lt p1, v10, :cond_3

    iget v5, p0, Lcom/google/protobuf/micro/a;->dBO:I

    iget v6, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iget v2, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBM:I

    iput v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    iput v1, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    sub-int v0, v6, v5

    sub-int v0, p1, v0

    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    move v4, v0

    :goto_0
    if-gtz v4, :cond_5

    new-array v4, p1, [B

    sub-int v0, v6, v5

    iget-object v2, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    invoke-static {v2, v5, v4, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v1

    move v3, v0

    :goto_1
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v2, v0, :cond_9

    return-object v4

    :cond_0
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diJ()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/protobuf/micro/a;->dBR:I

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBM:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/a;->dhu(I)V

    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diL()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_2
    new-array v0, p1, [B

    iget-object v2, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    iget v3, p0, Lcom/google/protobuf/micro/a;->dBO:I

    invoke-static {v2, v3, v0, v1, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    add-int/2addr v1, p1

    iput v1, p0, Lcom/google/protobuf/micro/a;->dBO:I

    return-object v0

    :cond_3
    new-array v2, p1, [B

    iget v0, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iget v3, p0, Lcom/google/protobuf/micro/a;->dBO:I

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    iget v4, p0, Lcom/google/protobuf/micro/a;->dBO:I

    invoke-static {v3, v4, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v3, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iput v3, p0, Lcom/google/protobuf/micro/a;->dBO:I

    invoke-direct {p0, v5}, Lcom/google/protobuf/micro/a;->dhx(Z)Z

    :goto_2
    sub-int v3, p1, v0

    iget v4, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    if-gt v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    sub-int v4, p1, v0

    invoke-static {v3, v1, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sub-int v0, p1, v0

    iput v0, p0, Lcom/google/protobuf/micro/a;->dBO:I

    return-object v2

    :cond_4
    iget-object v3, p0, Lcom/google/protobuf/micro/a;->dBV:[B

    iget v4, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    invoke-static {v3, v1, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v3, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/google/protobuf/micro/a;->dBQ:I

    iput v3, p0, Lcom/google/protobuf/micro/a;->dBO:I

    invoke-direct {p0, v5}, Lcom/google/protobuf/micro/a;->dhx(Z)Z

    goto :goto_2

    :cond_5
    invoke-static {v4, v10}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v8, v0, [B

    move v0, v1

    :goto_3
    array-length v2, v8

    if-lt v0, v2, :cond_6

    array-length v0, v8

    sub-int v0, v4, v0

    invoke-virtual {v7, v8}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    move v4, v0

    goto/16 :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/protobuf/micro/a;->dBP:Ljava/io/InputStream;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/protobuf/micro/a;->dBP:Ljava/io/InputStream;

    array-length v9, v8

    sub-int/2addr v9, v0

    invoke-virtual {v2, v8, v0, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    :goto_4
    if-eq v2, v3, :cond_8

    iget v9, p0, Lcom/google/protobuf/micro/a;->dBM:I

    add-int/2addr v9, v2

    iput v9, p0, Lcom/google/protobuf/micro/a;->dBM:I

    add-int/2addr v0, v2

    goto :goto_3

    :cond_7
    move v2, v3

    goto :goto_4

    :cond_8
    invoke-static {}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->diL()Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;

    move-result-object v0

    throw v0

    :cond_9
    invoke-virtual {v7, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    array-length v5, v0

    invoke-static {v0, v1, v4, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v0, v0

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1
.end method

.method public dhy()J
    .locals 14

    const-wide/16 v12, 0xff

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v0

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v1

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v2

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v3

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v4

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v5

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v6

    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhr()B

    move-result v7

    int-to-long v8, v0

    and-long/2addr v8, v12

    int-to-long v0, v1

    and-long/2addr v0, v12

    const/16 v10, 0x8

    shl-long/2addr v0, v10

    or-long/2addr v0, v8

    int-to-long v8, v2

    and-long/2addr v8, v12

    const/16 v2, 0x10

    shl-long/2addr v8, v2

    or-long/2addr v0, v8

    int-to-long v2, v3

    and-long/2addr v2, v12

    const/16 v8, 0x18

    shl-long/2addr v2, v8

    or-long/2addr v0, v2

    int-to-long v2, v4

    and-long/2addr v2, v12

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v5

    and-long/2addr v2, v12

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v6

    and-long/2addr v2, v12

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    int-to-long v2, v7

    and-long/2addr v2, v12

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public dhz()V
    .locals 1

    :goto_0
    invoke-virtual {p0}, Lcom/google/protobuf/micro/a;->dhj()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/a;->dhC(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method
