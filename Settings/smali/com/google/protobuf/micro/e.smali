.class public final Lcom/google/protobuf/micro/e;
.super Ljava/lang/Object;
.source "WireFormatMicro.java"


# static fields
.field static final dCe:I

.field static final dCf:I

.field static final dCg:I

.field static final dCh:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-static {v1, v3}, Lcom/google/protobuf/micro/e;->diR(II)I

    move-result v0

    sput v0, Lcom/google/protobuf/micro/e;->dCh:I

    const/4 v0, 0x4

    invoke-static {v1, v0}, Lcom/google/protobuf/micro/e;->diR(II)I

    move-result v0

    sput v0, Lcom/google/protobuf/micro/e;->dCe:I

    const/4 v0, 0x0

    invoke-static {v2, v0}, Lcom/google/protobuf/micro/e;->diR(II)I

    move-result v0

    sput v0, Lcom/google/protobuf/micro/e;->dCf:I

    invoke-static {v3, v2}, Lcom/google/protobuf/micro/e;->diR(II)I

    move-result v0

    sput v0, Lcom/google/protobuf/micro/e;->dCg:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static diP(I)I
    .locals 1

    and-int/lit8 v0, p0, 0x7

    return v0
.end method

.method public static diQ(I)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x3

    return v0
.end method

.method static diR(II)I
    .locals 1

    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method
