.class public abstract Lcom/google/protobuf/micro/c;
.super Ljava/lang/Object;
.source "MessageMicro.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract cIj()I
.end method

.method public abstract cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;
.end method

.method public abstract cIm(Lcom/google/protobuf/micro/b;)V
.end method

.method protected diA(Lcom/google/protobuf/micro/a;I)Z
    .locals 1

    invoke-virtual {p1, p2}, Lcom/google/protobuf/micro/a;->dhC(I)Z

    move-result v0

    return v0
.end method

.method public diB([BII)V
    .locals 2

    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/protobuf/micro/b;->diu([BII)Lcom/google/protobuf/micro/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/c;->cIm(Lcom/google/protobuf/micro/b;)V

    invoke-virtual {v0}, Lcom/google/protobuf/micro/b;->did()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public diC([B)Lcom/google/protobuf/micro/c;
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/protobuf/micro/c;->diD([BII)Lcom/google/protobuf/micro/c;

    move-result-object v0

    return-object v0
.end method

.method public diD([BII)Lcom/google/protobuf/micro/c;
    .locals 2

    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/protobuf/micro/a;->dhA([BII)Lcom/google/protobuf/micro/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/micro/c;->cIl(Lcom/google/protobuf/micro/a;)Lcom/google/protobuf/micro/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/micro/a;->dhB(I)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object p0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public diz()[B
    .locals 3

    invoke-virtual {p0}, Lcom/google/protobuf/micro/c;->getSerializedSize()I

    move-result v0

    new-array v0, v0, [B

    array-length v1, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/protobuf/micro/c;->diB([BII)V

    return-object v0
.end method

.method public abstract getSerializedSize()I
.end method
