.class public Lcom/android/setupwizardlib/a/a;
.super Landroid/text/style/ClickableSpan;
.source "LinkSpan.java"


# static fields
.field private static final csu:Landroid/graphics/Typeface;


# instance fields
.field private final csv:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "sans-serif-medium"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/android/setupwizardlib/a/a;->csu:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    iput-object p1, p0, Lcom/android/setupwizardlib/a/a;->csv:Ljava/lang/String;

    return-void
.end method

.method private caI(Landroid/view/View;)Z
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/android/setupwizardlib/a/c;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/setupwizardlib/a/c;

    invoke-interface {v0, p0}, Lcom/android/setupwizardlib/a/c;->caK(Lcom/android/setupwizardlib/a/a;)Z

    move-result v0

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/setupwizardlib/a/a;->caJ(Landroid/content/Context;)Lcom/android/setupwizardlib/a/b;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1, p0}, Lcom/android/setupwizardlib/a/b;->aOu(Lcom/android/setupwizardlib/a/a;)V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private caJ(Landroid/content/Context;)Lcom/android/setupwizardlib/a/b;
    .locals 2

    move-object v0, p1

    :goto_0
    instance-of v1, v0, Lcom/android/setupwizardlib/a/b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/setupwizardlib/a/b;

    return-object v0

    :cond_0
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/a/a;->csv:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/setupwizardlib/a/a;->caI(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->cancelPendingInputEvents()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "LinkSpan"

    const-string/jumbo v1, "Dropping click event. No listener attached."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    sget-object v0, Lcom/android/setupwizardlib/a/a;->csu:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    return-void
.end method
