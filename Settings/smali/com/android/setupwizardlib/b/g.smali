.class public Lcom/android/setupwizardlib/b/g;
.super Ljava/lang/Object;
.source "RequireScrollMixin.java"

# interfaces
.implements Lcom/android/setupwizardlib/b/b;


# instance fields
.field private final ctL:Landroid/os/Handler;

.field private ctM:Lcom/android/setupwizardlib/b/l;

.field private final ctN:Lcom/android/setupwizardlib/TemplateLayout;

.field private ctO:Lcom/android/setupwizardlib/b/o;

.field private ctP:Z

.field private ctQ:Z


# direct methods
.method public constructor <init>(Lcom/android/setupwizardlib/TemplateLayout;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/setupwizardlib/b/g;->ctL:Landroid/os/Handler;

    iput-boolean v2, p0, Lcom/android/setupwizardlib/b/g;->ctP:Z

    iput-boolean v2, p0, Lcom/android/setupwizardlib/b/g;->ctQ:Z

    iput-object p1, p0, Lcom/android/setupwizardlib/b/g;->ctN:Lcom/android/setupwizardlib/TemplateLayout;

    return-void
.end method

.method private cbI(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/setupwizardlib/b/g;->ctL:Landroid/os/Handler;

    new-instance v1, Lcom/android/setupwizardlib/b/d;

    invoke-direct {v1, p0, p1}, Lcom/android/setupwizardlib/b/d;-><init>(Lcom/android/setupwizardlib/b/g;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic cbL(Lcom/android/setupwizardlib/b/g;)Lcom/android/setupwizardlib/b/o;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/g;->ctO:Lcom/android/setupwizardlib/b/o;

    return-object v0
.end method


# virtual methods
.method public cbJ(Lcom/android/setupwizardlib/b/l;)V
    .locals 0

    iput-object p1, p0, Lcom/android/setupwizardlib/b/g;->ctM:Lcom/android/setupwizardlib/b/l;

    return-void
.end method

.method cbK(Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/setupwizardlib/b/g;->ctP:Z

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/android/setupwizardlib/b/g;->ctQ:Z

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/setupwizardlib/b/g;->cbI(Z)V

    iput-boolean v1, p0, Lcom/android/setupwizardlib/b/g;->ctP:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0, v2}, Lcom/android/setupwizardlib/b/g;->cbI(Z)V

    iput-boolean v2, p0, Lcom/android/setupwizardlib/b/g;->ctP:Z

    iput-boolean v1, p0, Lcom/android/setupwizardlib/b/g;->ctQ:Z

    goto :goto_0
.end method
