.class public Lcom/android/setupwizardlib/b/f;
.super Ljava/lang/Object;
.source "ListMixin.java"

# interfaces
.implements Lcom/android/setupwizardlib/b/b;


# instance fields
.field private ctF:I

.field private ctG:I

.field private ctH:Landroid/graphics/drawable/Drawable;

.field private ctI:Lcom/android/setupwizardlib/TemplateLayout;

.field private ctJ:Landroid/graphics/drawable/Drawable;

.field private ctK:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Lcom/android/setupwizardlib/TemplateLayout;Landroid/util/AttributeSet;I)V
    .locals 6

    const/4 v5, -0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/setupwizardlib/b/f;->ctI:Lcom/android/setupwizardlib/TemplateLayout;

    invoke-virtual {p1}, Lcom/android/setupwizardlib/TemplateLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/android/setupwizardlib/g;->cxK:[I

    invoke-virtual {v0, p2, v1, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    sget v2, Lcom/android/setupwizardlib/g;->cxL:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    if-eqz v2, :cond_0

    new-instance v3, Lcom/android/setupwizardlib/items/j;

    invoke-direct {v3, v0}, Lcom/android/setupwizardlib/items/j;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Lcom/android/setupwizardlib/items/j;->ccF(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/items/ItemGroup;

    new-instance v2, Lcom/android/setupwizardlib/items/d;

    invoke-direct {v2, v0}, Lcom/android/setupwizardlib/items/d;-><init>(Lcom/android/setupwizardlib/items/f;)V

    invoke-virtual {p0, v2}, Lcom/android/setupwizardlib/b/f;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    sget v0, Lcom/android/setupwizardlib/g;->cxM:I

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    if-eq v0, v5, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/b/f;->setDividerInset(I)V

    :goto_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_1
    sget v0, Lcom/android/setupwizardlib/g;->cxO:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    sget v2, Lcom/android/setupwizardlib/g;->cxN:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/android/setupwizardlib/b/f;->setDividerInsets(II)V

    goto :goto_0
.end method

.method private cbG()Landroid/widget/ListView;
    .locals 2

    iget-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctK:Landroid/widget/ListView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctI:Lcom/android/setupwizardlib/TemplateLayout;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/TemplateLayout;->cdq(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/ListView;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctK:Landroid/widget/ListView;

    :cond_0
    iget-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctK:Landroid/widget/ListView;

    return-object v0
.end method

.method private cbH()V
    .locals 7

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/f;->cbG()Landroid/widget/ListView;

    move-result-object v6

    if-nez v6, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v1, v3, :cond_1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctI:Lcom/android/setupwizardlib/TemplateLayout;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/TemplateLayout;->isLayoutDirectionResolved()Z

    move-result v0

    :cond_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctH:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    invoke-virtual {v6}, Landroid/widget/ListView;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctH:Landroid/graphics/drawable/Drawable;

    :cond_2
    iget-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctH:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/android/setupwizardlib/b/f;->ctG:I

    iget v3, p0, Lcom/android/setupwizardlib/b/f;->ctF:I

    iget-object v5, p0, Lcom/android/setupwizardlib/b/f;->ctI:Lcom/android/setupwizardlib/TemplateLayout;

    move v4, v2

    invoke-static/range {v0 .. v5}, Lcom/android/setupwizardlib/c/b;->cca(Landroid/graphics/drawable/Drawable;IIIILandroid/view/View;)Landroid/graphics/drawable/InsetDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctJ:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctJ:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public cbF()V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctJ:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/f;->cbH()V

    :cond_0
    return-void
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/f;->cbG()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0

    :cond_1
    return-object v1
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/f;->ctJ:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDividerInset()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/setupwizardlib/b/f;->getDividerInsetStart()I

    move-result v0

    return v0
.end method

.method public getDividerInsetEnd()I
    .locals 1

    iget v0, p0, Lcom/android/setupwizardlib/b/f;->ctF:I

    return v0
.end method

.method public getDividerInsetStart()I
    .locals 1

    iget v0, p0, Lcom/android/setupwizardlib/b/f;->ctG:I

    return v0
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/f;->cbG()Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/f;->cbG()Landroid/widget/ListView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    return-void
.end method

.method public setDividerInset(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/setupwizardlib/b/f;->setDividerInsets(II)V

    return-void
.end method

.method public setDividerInsets(II)V
    .locals 0

    iput p1, p0, Lcom/android/setupwizardlib/b/f;->ctG:I

    iput p2, p0, Lcom/android/setupwizardlib/b/f;->ctF:I

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/f;->cbH()V

    return-void
.end method
