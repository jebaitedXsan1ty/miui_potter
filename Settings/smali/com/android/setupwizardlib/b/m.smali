.class public Lcom/android/setupwizardlib/b/m;
.super Ljava/lang/Object;
.source "ListViewScrollHandlingDelegate.java"

# interfaces
.implements Lcom/android/setupwizardlib/b/l;
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private final ctV:Lcom/android/setupwizardlib/b/g;

.field private final ctW:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Lcom/android/setupwizardlib/b/g;Landroid/widget/ListView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/setupwizardlib/b/m;->ctV:Lcom/android/setupwizardlib/b/g;

    iput-object p2, p0, Lcom/android/setupwizardlib/b/m;->ctW:Landroid/widget/ListView;

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    iget-object v0, p0, Lcom/android/setupwizardlib/b/m;->ctV:Lcom/android/setupwizardlib/b/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/b/g;->cbK(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/setupwizardlib/b/m;->ctV:Lcom/android/setupwizardlib/b/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/b/g;->cbK(Z)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method
