.class public Lcom/android/setupwizardlib/b/a;
.super Ljava/lang/Object;
.source "RecyclerMixin.java"

# interfaces
.implements Lcom/android/setupwizardlib/b/b;


# instance fields
.field private cts:Landroid/graphics/drawable/Drawable;

.field private ctt:Landroid/view/View;

.field private final ctu:Landroid/support/v7/widget/RecyclerView;

.field private ctv:I

.field private ctw:I

.field private ctx:Landroid/graphics/drawable/Drawable;

.field private cty:Lcom/android/setupwizardlib/TemplateLayout;

.field private ctz:Lcom/android/setupwizardlib/h;


# direct methods
.method public constructor <init>(Lcom/android/setupwizardlib/TemplateLayout;Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/setupwizardlib/b/a;->cty:Lcom/android/setupwizardlib/TemplateLayout;

    new-instance v0, Lcom/android/setupwizardlib/h;

    iget-object v1, p0, Lcom/android/setupwizardlib/b/a;->cty:Lcom/android/setupwizardlib/TemplateLayout;

    invoke-virtual {v1}, Lcom/android/setupwizardlib/TemplateLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/setupwizardlib/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctz:Lcom/android/setupwizardlib/h;

    iput-object p2, p0, Lcom/android/setupwizardlib/b/a;->ctu:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctu:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/al;

    iget-object v2, p0, Lcom/android/setupwizardlib/b/a;->cty:Lcom/android/setupwizardlib/TemplateLayout;

    invoke-virtual {v2}, Lcom/android/setupwizardlib/TemplateLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/al;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/a;)V

    instance-of v0, p2, Lcom/android/setupwizardlib/view/HeaderRecyclerView;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/android/setupwizardlib/view/HeaderRecyclerView;

    invoke-virtual {p2}, Lcom/android/setupwizardlib/view/HeaderRecyclerView;->getHeader()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctt:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctu:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/android/setupwizardlib/b/a;->ctz:Lcom/android/setupwizardlib/h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->dpM(Landroid/support/v7/widget/d;)V

    return-void
.end method

.method private cbA()V
    .locals 6

    const/4 v2, 0x0

    const/4 v0, 0x1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v1, v3, :cond_0

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->cty:Lcom/android/setupwizardlib/TemplateLayout;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/TemplateLayout;->isLayoutDirectionResolved()Z

    move-result v0

    :cond_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctx:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctz:Lcom/android/setupwizardlib/h;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/h;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctx:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctx:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/android/setupwizardlib/b/a;->ctw:I

    iget v3, p0, Lcom/android/setupwizardlib/b/a;->ctv:I

    iget-object v5, p0, Lcom/android/setupwizardlib/b/a;->cty:Lcom/android/setupwizardlib/TemplateLayout;

    move v4, v2

    invoke-static/range {v0 .. v5}, Lcom/android/setupwizardlib/c/b;->cca(Landroid/graphics/drawable/Drawable;IIIILandroid/view/View;)Landroid/graphics/drawable/InsetDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/setupwizardlib/b/a;->cts:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctz:Lcom/android/setupwizardlib/h;

    iget-object v1, p0, Lcom/android/setupwizardlib/b/a;->cts:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/h;->cdf(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public cby()V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->cts:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/a;->cbA()V

    :cond_0
    return-void
.end method

.method public cbz(Landroid/util/AttributeSet;I)V
    .locals 6

    const/4 v5, -0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->cty:Lcom/android/setupwizardlib/TemplateLayout;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/TemplateLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/android/setupwizardlib/g;->cxT:[I

    invoke-virtual {v0, p1, v1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    sget v2, Lcom/android/setupwizardlib/g;->cxU:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    if-eqz v2, :cond_0

    new-instance v3, Lcom/android/setupwizardlib/items/j;

    invoke-direct {v3, v0}, Lcom/android/setupwizardlib/items/j;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Lcom/android/setupwizardlib/items/j;->ccF(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/items/f;

    new-instance v2, Lcom/android/setupwizardlib/items/b;

    invoke-direct {v2, v0}, Lcom/android/setupwizardlib/items/b;-><init>(Lcom/android/setupwizardlib/items/f;)V

    sget v0, Lcom/android/setupwizardlib/g;->cxY:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/android/setupwizardlib/items/b;->setHasStableIds(Z)V

    invoke-virtual {p0, v2}, Lcom/android/setupwizardlib/b/a;->setAdapter(Landroid/support/v7/widget/b;)V

    :cond_0
    sget v0, Lcom/android/setupwizardlib/g;->cxV:I

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    if-eq v0, v5, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/b/a;->setDividerInset(I)V

    :goto_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_1
    sget v0, Lcom/android/setupwizardlib/g;->cxX:I

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    sget v2, Lcom/android/setupwizardlib/g;->cxW:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/android/setupwizardlib/b/a;->setDividerInsets(II)V

    goto :goto_0
.end method

.method public getAdapter()Landroid/support/v7/widget/b;
    .locals 2

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/b;

    move-result-object v0

    instance-of v1, v0, Lcom/android/setupwizardlib/view/b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/setupwizardlib/view/b;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/view/b;->caU()Landroid/support/v7/widget/b;

    move-result-object v0

    return-object v0

    :cond_0
    return-object v0
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->cts:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDividerInset()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/setupwizardlib/b/a;->getDividerInsetStart()I

    move-result v0

    return v0
.end method

.method public getDividerInsetEnd()I
    .locals 1

    iget v0, p0, Lcom/android/setupwizardlib/b/a;->ctv:I

    return v0
.end method

.method public getDividerInsetStart()I
    .locals 1

    iget v0, p0, Lcom/android/setupwizardlib/b/a;->ctw:I

    return v0
.end method

.method public getHeader()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctt:Landroid/view/View;

    return-object v0
.end method

.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctu:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method public setAdapter(Landroid/support/v7/widget/b;)V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctu:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/b;)V

    return-void
.end method

.method public setDividerInset(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/setupwizardlib/b/a;->setDividerInsets(II)V

    return-void
.end method

.method public setDividerInsets(II)V
    .locals 0

    iput p1, p0, Lcom/android/setupwizardlib/b/a;->ctw:I

    iput p2, p0, Lcom/android/setupwizardlib/b/a;->ctv:I

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/a;->cbA()V

    return-void
.end method

.method public setDividerItemDecoration(Lcom/android/setupwizardlib/h;)V
    .locals 2

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctu:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/android/setupwizardlib/b/a;->ctz:Lcom/android/setupwizardlib/h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->doT(Landroid/support/v7/widget/d;)V

    iput-object p1, p0, Lcom/android/setupwizardlib/b/a;->ctz:Lcom/android/setupwizardlib/h;

    iget-object v0, p0, Lcom/android/setupwizardlib/b/a;->ctu:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/android/setupwizardlib/b/a;->ctz:Lcom/android/setupwizardlib/h;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->dpM(Landroid/support/v7/widget/d;)V

    invoke-direct {p0}, Lcom/android/setupwizardlib/b/a;->cbA()V

    return-void
.end method
