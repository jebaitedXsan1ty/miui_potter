.class Lcom/android/setupwizardlib/view/g;
.super Landroid/graphics/drawable/LayerDrawable;
.source "NavigationBarButton.java"


# instance fields
.field private ctj:Landroid/content/res/ColorStateList;


# direct methods
.method constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/setupwizardlib/view/g;->ctj:Landroid/content/res/ColorStateList;

    return-void
.end method

.method private cbp()Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/setupwizardlib/view/g;->ctj:Landroid/content/res/ColorStateList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/setupwizardlib/view/g;->ctj:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/android/setupwizardlib/view/g;->getState()[I

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, v0, v1}, Lcom/android/setupwizardlib/view/g;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    return v2
.end method

.method public static cbr(Landroid/graphics/drawable/Drawable;)Lcom/android/setupwizardlib/view/g;
    .locals 2

    instance-of v0, p0, Lcom/android/setupwizardlib/view/g;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/android/setupwizardlib/view/g;

    return-object p0

    :cond_0
    new-instance v0, Lcom/android/setupwizardlib/view/g;

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/setupwizardlib/view/g;-><init>(Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method


# virtual methods
.method public cbq(Landroid/content/res/ColorStateList;)V
    .locals 1

    iput-object p1, p0, Lcom/android/setupwizardlib/view/g;->ctj:Landroid/content/res/ColorStateList;

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/g;->cbp()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/setupwizardlib/view/g;->invalidateSelf()V

    :cond_0
    return-void
.end method

.method public isStateful()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setState([I)Z
    .locals 2

    invoke-super {p0, p1}, Landroid/graphics/drawable/LayerDrawable;->setState([I)Z

    move-result v1

    invoke-direct {p0}, Lcom/android/setupwizardlib/view/g;->cbp()Z

    move-result v0

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
