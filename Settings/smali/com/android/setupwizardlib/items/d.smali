.class public Lcom/android/setupwizardlib/items/d;
.super Landroid/widget/BaseAdapter;
.source "ItemAdapter.java"

# interfaces
.implements Lcom/android/setupwizardlib/items/h;


# instance fields
.field private cul:Lcom/android/setupwizardlib/items/o;

.field private final cum:Lcom/android/setupwizardlib/items/f;


# direct methods
.method public constructor <init>(Lcom/android/setupwizardlib/items/f;)V
    .locals 2

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Lcom/android/setupwizardlib/items/o;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/setupwizardlib/items/o;-><init>(Lcom/android/setupwizardlib/items/o;)V

    iput-object v0, p0, Lcom/android/setupwizardlib/items/d;->cul:Lcom/android/setupwizardlib/items/o;

    iput-object p1, p0, Lcom/android/setupwizardlib/items/d;->cum:Lcom/android/setupwizardlib/items/f;

    iget-object v0, p0, Lcom/android/setupwizardlib/items/d;->cum:Lcom/android/setupwizardlib/items/f;

    invoke-interface {v0, p0}, Lcom/android/setupwizardlib/items/f;->ccC(Lcom/android/setupwizardlib/items/h;)V

    invoke-direct {p0}, Lcom/android/setupwizardlib/items/d;->ccB()V

    return-void
.end method

.method private ccB()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/android/setupwizardlib/items/d;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/items/d;->getItem(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v1

    iget-object v2, p0, Lcom/android/setupwizardlib/items/d;->cul:Lcom/android/setupwizardlib/items/o;

    invoke-interface {v1}, Lcom/android/setupwizardlib/items/i;->getLayoutResource()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/android/setupwizardlib/items/o;->add(I)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public ccA(Lcom/android/setupwizardlib/items/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/setupwizardlib/items/d;->ccB()V

    invoke-virtual {p0}, Lcom/android/setupwizardlib/items/d;->notifyDataSetChanged()V

    return-void
.end method

.method public ccr(Lcom/android/setupwizardlib/items/f;II)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/setupwizardlib/items/d;->ccA(Lcom/android/setupwizardlib/items/f;)V

    return-void
.end method

.method public ccs(Lcom/android/setupwizardlib/items/f;II)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/setupwizardlib/items/d;->ccA(Lcom/android/setupwizardlib/items/f;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/items/d;->cum:Lcom/android/setupwizardlib/items/f;

    invoke-interface {v0}, Lcom/android/setupwizardlib/items/f;->getCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/android/setupwizardlib/items/i;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/items/d;->cum:Lcom/android/setupwizardlib/items/f;

    invoke-interface {v0, p1}, Lcom/android/setupwizardlib/items/f;->ccL(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/setupwizardlib/items/d;->getItem(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/setupwizardlib/items/d;->getItem(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/setupwizardlib/items/i;->getLayoutResource()I

    move-result v0

    iget-object v1, p0, Lcom/android/setupwizardlib/items/d;->cul:Lcom/android/setupwizardlib/items/o;

    invoke-virtual {v1, v0}, Lcom/android/setupwizardlib/items/o;->cdc(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    invoke-virtual {p0, p1}, Lcom/android/setupwizardlib/items/d;->getItem(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v0

    if-nez p2, :cond_0

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-interface {v0}, Lcom/android/setupwizardlib/items/i;->getLayoutResource()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-interface {v0, p2}, Lcom/android/setupwizardlib/items/i;->onBindView(Landroid/view/View;)V

    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/items/d;->cul:Lcom/android/setupwizardlib/items/o;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/items/o;->size()I

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/setupwizardlib/items/d;->getItem(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/setupwizardlib/items/i;->isEnabled()Z

    move-result v0

    return v0
.end method
