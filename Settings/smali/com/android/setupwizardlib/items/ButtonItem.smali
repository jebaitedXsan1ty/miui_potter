.class public Lcom/android/setupwizardlib/items/ButtonItem;
.super Lcom/android/setupwizardlib/items/AbstractItem;
.source "ButtonItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private cuA:I

.field private cux:Landroid/widget/Button;

.field private cuy:Lcom/android/setupwizardlib/items/c;

.field private cuz:Ljava/lang/CharSequence;

.field private mEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/setupwizardlib/items/AbstractItem;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->mEnabled:Z

    sget v0, Lcom/android/setupwizardlib/f;->cvD:I

    iput v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cuA:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2}, Lcom/android/setupwizardlib/items/AbstractItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v2, p0, Lcom/android/setupwizardlib/items/ButtonItem;->mEnabled:Z

    sget v0, Lcom/android/setupwizardlib/f;->cvD:I

    iput v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cuA:I

    sget-object v0, Lcom/android/setupwizardlib/g;->cwV:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/android/setupwizardlib/g;->cwW:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/setupwizardlib/items/ButtonItem;->mEnabled:Z

    sget v1, Lcom/android/setupwizardlib/g;->cwX:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cuz:Ljava/lang/CharSequence;

    sget v1, Lcom/android/setupwizardlib/g;->cwY:I

    sget v2, Lcom/android/setupwizardlib/f;->cvD:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cuA:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private ccV(Landroid/content/Context;)Landroid/widget/Button;
    .locals 4

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/android/setupwizardlib/e;->cvn:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method protected ccW(Landroid/view/ViewGroup;)Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cuA:I

    if-eqz v0, :cond_2

    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v2, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cuA:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/setupwizardlib/items/ButtonItem;->ccV(Landroid/content/Context;)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/android/setupwizardlib/items/ButtonItem;->mEnabled:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cuz:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/android/setupwizardlib/items/ButtonItem;->ccD()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cux:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getLayoutResource()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->mEnabled:Z

    return v0
.end method

.method public final onBindView(Landroid/view/View;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Cannot bind to ButtonItem\'s view"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cuy:Lcom/android/setupwizardlib/items/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/setupwizardlib/items/ButtonItem;->cuy:Lcom/android/setupwizardlib/items/c;

    invoke-interface {v0, p0}, Lcom/android/setupwizardlib/items/c;->ccz(Lcom/android/setupwizardlib/items/ButtonItem;)V

    :cond_0
    return-void
.end method
