.class public Lcom/android/setupwizardlib/items/b;
.super Landroid/support/v7/widget/b;
.source "RecyclerItemAdapter.java"

# interfaces
.implements Lcom/android/setupwizardlib/items/h;


# instance fields
.field private final cug:Lcom/android/setupwizardlib/items/f;

.field private cuh:Lcom/android/setupwizardlib/items/n;


# direct methods
.method public constructor <init>(Lcom/android/setupwizardlib/items/f;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/widget/b;-><init>()V

    iput-object p1, p0, Lcom/android/setupwizardlib/items/b;->cug:Lcom/android/setupwizardlib/items/f;

    iget-object v0, p0, Lcom/android/setupwizardlib/items/b;->cug:Lcom/android/setupwizardlib/items/f;

    invoke-interface {v0, p0}, Lcom/android/setupwizardlib/items/f;->ccC(Lcom/android/setupwizardlib/items/h;)V

    return-void
.end method

.method static synthetic ccp(Lcom/android/setupwizardlib/items/b;)Lcom/android/setupwizardlib/items/n;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/items/b;->cuh:Lcom/android/setupwizardlib/items/n;

    return-object v0
.end method


# virtual methods
.method public ccq(Lcom/android/setupwizardlib/items/a;I)V
    .locals 2

    invoke-virtual {p0, p2}, Lcom/android/setupwizardlib/items/b;->getItem(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v0

    iget-object v1, p1, Lcom/android/setupwizardlib/items/a;->itemView:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/android/setupwizardlib/items/i;->onBindView(Landroid/view/View;)V

    invoke-interface {v0}, Lcom/android/setupwizardlib/items/i;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/android/setupwizardlib/items/a;->setEnabled(Z)V

    invoke-virtual {p1, v0}, Lcom/android/setupwizardlib/items/a;->cco(Lcom/android/setupwizardlib/items/i;)V

    return-void
.end method

.method public ccr(Lcom/android/setupwizardlib/items/f;II)V
    .locals 0

    invoke-virtual {p0, p2, p3}, Lcom/android/setupwizardlib/items/b;->notifyItemRangeInserted(II)V

    return-void
.end method

.method public ccs(Lcom/android/setupwizardlib/items/f;II)V
    .locals 0

    invoke-virtual {p0, p2, p3}, Lcom/android/setupwizardlib/items/b;->notifyItemRangeChanged(II)V

    return-void
.end method

.method public getItem(I)Lcom/android/setupwizardlib/items/i;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/items/b;->cug:Lcom/android/setupwizardlib/items/f;

    invoke-interface {v0, p1}, Lcom/android/setupwizardlib/items/f;->ccL(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v0

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/items/b;->cug:Lcom/android/setupwizardlib/items/f;

    invoke-interface {v0}, Lcom/android/setupwizardlib/items/f;->getCount()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 4

    const-wide/16 v2, -0x1

    invoke-virtual {p0, p1}, Lcom/android/setupwizardlib/items/b;->getItem(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v0

    instance-of v1, v0, Lcom/android/setupwizardlib/items/AbstractItem;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/android/setupwizardlib/items/AbstractItem;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/items/AbstractItem;->getId()I

    move-result v0

    if-lez v0, :cond_0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0

    :cond_1
    return-wide v2
.end method

.method public getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/setupwizardlib/items/b;->getItem(I)Lcom/android/setupwizardlib/items/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/setupwizardlib/items/i;->getLayoutResource()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/p;I)V
    .locals 0

    check-cast p1, Lcom/android/setupwizardlib/items/a;

    invoke-virtual {p0, p1, p2}, Lcom/android/setupwizardlib/items/b;->ccq(Lcom/android/setupwizardlib/items/a;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/p;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/android/setupwizardlib/items/b;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/setupwizardlib/items/a;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/setupwizardlib/items/a;
    .locals 8

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p2, p1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/android/setupwizardlib/items/a;

    invoke-direct {v3, v2}, Lcom/android/setupwizardlib/items/a;-><init>(Landroid/view/View;)V

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    const-string/jumbo v1, "noBackground"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/android/setupwizardlib/g;->cxP:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v4

    sget v0, Lcom/android/setupwizardlib/g;->cxR:I

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    sget v0, Lcom/android/setupwizardlib/g;->cxS:I

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_1

    sget v1, Lcom/android/setupwizardlib/g;->cxQ:I

    invoke-virtual {v4, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_4

    :cond_2
    const-string/jumbo v5, "RecyclerItemAdapter"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Cannot resolve required attributes. selectableItemBackground="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v6, " background="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    :cond_3
    new-instance v0, Lcom/android/setupwizardlib/items/l;

    invoke-direct {v0, p0, v3}, Lcom/android/setupwizardlib/items/l;-><init>(Lcom/android/setupwizardlib/items/b;Lcom/android/setupwizardlib/items/a;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v3

    :cond_4
    const/4 v5, 0x2

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object v0, v5, v1

    new-instance v0, Lcom/android/setupwizardlib/items/RecyclerItemAdapter$PatchedLayerDrawable;

    invoke-direct {v0, v5}, Lcom/android/setupwizardlib/items/RecyclerItemAdapter$PatchedLayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
