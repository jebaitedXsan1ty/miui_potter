.class public Lcom/android/setupwizardlib/GlifRecyclerLayout;
.super Lcom/android/setupwizardlib/GlifLayout;
.source "GlifRecyclerLayout.java"


# instance fields
.field protected cyS:Lcom/android/setupwizardlib/b/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Lcom/android/setupwizardlib/GlifRecyclerLayout;-><init>(Landroid/content/Context;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/setupwizardlib/GlifRecyclerLayout;-><init>(Landroid/content/Context;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/android/setupwizardlib/GlifLayout;-><init>(Landroid/content/Context;II)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cdD(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/setupwizardlib/GlifLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cdD(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/setupwizardlib/GlifLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2, p3}, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cdD(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private cdD(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0, p2, p3}, Lcom/android/setupwizardlib/b/a;->cbz(Landroid/util/AttributeSet;I)V

    const-class v0, Lcom/android/setupwizardlib/b/a;

    iget-object v1, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {p0, v0, v1}, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cdx(Ljava/lang/Class;Lcom/android/setupwizardlib/b/b;)V

    const-class v0, Lcom/android/setupwizardlib/b/g;

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cdw(Ljava/lang/Class;)Lcom/android/setupwizardlib/b/b;

    move-result-object v0

    check-cast v0, Lcom/android/setupwizardlib/b/g;

    new-instance v1, Lcom/android/setupwizardlib/b/c;

    invoke-virtual {p0}, Lcom/android/setupwizardlib/GlifRecyclerLayout;->getRecyclerView()Landroid/support/v7/widget/RecyclerView;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/android/setupwizardlib/b/c;-><init>(Lcom/android/setupwizardlib/b/g;Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {v0, v1}, Lcom/android/setupwizardlib/b/g;->cbJ(Lcom/android/setupwizardlib/b/l;)V

    return-void
.end method


# virtual methods
.method protected cdk(I)Landroid/view/ViewGroup;
    .locals 1

    if-nez p1, :cond_0

    sget p1, Lcom/android/setupwizardlib/d;->cvl:I

    :cond_0
    invoke-super {p0, p1}, Lcom/android/setupwizardlib/GlifLayout;->cdk(I)Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method protected cdn(Landroid/view/LayoutInflater;I)Landroid/view/View;
    .locals 1

    if-nez p2, :cond_0

    sget p2, Lcom/android/setupwizardlib/e;->cvr:I

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/setupwizardlib/GlifLayout;->cdn(Landroid/view/LayoutInflater;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected cdp()V
    .locals 2

    sget v0, Lcom/android/setupwizardlib/d;->cvl:I

    invoke-virtual {p0, v0}, Lcom/android/setupwizardlib/GlifRecyclerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/setupwizardlib/b/a;

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-direct {v1, p0, v0}, Lcom/android/setupwizardlib/b/a;-><init>(Lcom/android/setupwizardlib/TemplateLayout;Landroid/support/v7/widget/RecyclerView;)V

    iput-object v1, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "GlifRecyclerLayout should use a template with recycler view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cdq(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/a;->getHeader()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/setupwizardlib/GlifLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/support/v7/widget/b;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/a;->getAdapter()Landroid/support/v7/widget/b;

    move-result-object v0

    return-object v0
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/a;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getDividerInset()I
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/a;->getDividerInset()I

    move-result v0

    return v0
.end method

.method public getDividerInsetEnd()I
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/a;->getDividerInsetEnd()I

    move-result v0

    return v0
.end method

.method public getDividerInsetStart()I
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/a;->getDividerInsetStart()I

    move-result v0

    return v0
.end method

.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/a;->getRecyclerView()Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    invoke-super/range {p0 .. p5}, Lcom/android/setupwizardlib/GlifLayout;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0}, Lcom/android/setupwizardlib/b/a;->cby()V

    return-void
.end method

.method public setAdapter(Landroid/support/v7/widget/b;)V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0, p1}, Lcom/android/setupwizardlib/b/a;->setAdapter(Landroid/support/v7/widget/b;)V

    return-void
.end method

.method public setDividerInset(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0, p1}, Lcom/android/setupwizardlib/b/a;->setDividerInset(I)V

    return-void
.end method

.method public setDividerInsets(II)V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/android/setupwizardlib/b/a;->setDividerInsets(II)V

    return-void
.end method

.method public setDividerItemDecoration(Lcom/android/setupwizardlib/h;)V
    .locals 1

    iget-object v0, p0, Lcom/android/setupwizardlib/GlifRecyclerLayout;->cyS:Lcom/android/setupwizardlib/b/a;

    invoke-virtual {v0, p1}, Lcom/android/setupwizardlib/b/a;->setDividerItemDecoration(Lcom/android/setupwizardlib/h;)V

    return-void
.end method
