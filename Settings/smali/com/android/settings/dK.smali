.class final Lcom/android/settings/dK;
.super Ljava/lang/Object;
.source "DiracEqualizer.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic chF:Lcom/android/settings/DiracEqualizer;

.field final synthetic chG:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/android/settings/DiracEqualizer;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/dK;->chF:Lcom/android/settings/DiracEqualizer;

    iput-object p2, p0, Lcom/android/settings/dK;->chG:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/dK;->chG:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "DiracEqualizer_Setting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "saveDialogOnClick name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/dK;->chF:Lcom/android/settings/DiracEqualizer;

    invoke-static {v1}, Lcom/android/settings/DiracEqualizer;->bja(Lcom/android/settings/DiracEqualizer;)Lcom/android/settings/x;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/dK;->chF:Lcom/android/settings/DiracEqualizer;

    invoke-static {v2}, Lcom/android/settings/DiracEqualizer;->bjb(Lcom/android/settings/DiracEqualizer;)Lcom/android/settings/w;

    move-result-object v2

    iget-object v2, v2, Lcom/android/settings/w;->bvU:[F

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/x;->bjn(Ljava/lang/String;[F)Lcom/android/settings/w;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/dK;->chF:Lcom/android/settings/DiracEqualizer;

    invoke-static {v1}, Lcom/android/settings/DiracEqualizer;->bjb(Lcom/android/settings/DiracEqualizer;)Lcom/android/settings/w;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/dK;->chF:Lcom/android/settings/DiracEqualizer;

    invoke-virtual {v1, v0}, Lcom/android/settings/DiracEqualizer;->biV(Lcom/android/settings/w;)V

    iget-object v0, p0, Lcom/android/settings/dK;->chF:Lcom/android/settings/DiracEqualizer;

    invoke-static {v0}, Lcom/android/settings/DiracEqualizer;->bjl(Lcom/android/settings/DiracEqualizer;)V

    :cond_0
    return-void
.end method
