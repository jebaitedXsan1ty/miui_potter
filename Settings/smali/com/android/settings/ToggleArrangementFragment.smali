.class public Lcom/android/settings/ToggleArrangementFragment;
.super Lcom/android/settings/BaseFragment;
.source "ToggleArrangementFragment.java"

# interfaces
.implements Lcom/android/settings/toggleposition/c;


# instance fields
.field private bHO:Lcom/android/settings/aT;

.field private bHP:Landroid/view/View;

.field private bHQ:I

.field private bHR:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

.field private bHS:Lcom/android/settings/ap;

.field private bHT:Lmiui/app/ToggleManager$OnToggleChangedListener;

.field private bHU:Ljava/util/ArrayList;

.field private bHV:Lmiui/app/ToggleManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic byA(Lcom/android/settings/ToggleArrangementFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHP:Landroid/view/View;

    return-object v0
.end method

.method static synthetic byB(Lcom/android/settings/ToggleArrangementFragment;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHQ:I

    return v0
.end method

.method static synthetic byC(Lcom/android/settings/ToggleArrangementFragment;)Lcom/android/settings/toggleposition/FixedDividerSortableListView;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHR:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    return-object v0
.end method

.method static synthetic byD(Lcom/android/settings/ToggleArrangementFragment;)Lcom/android/settings/ap;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHS:Lcom/android/settings/ap;

    return-object v0
.end method

.method static synthetic byE(Lcom/android/settings/ToggleArrangementFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic byF(Lcom/android/settings/ToggleArrangementFragment;)Lmiui/app/ToggleManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHV:Lmiui/app/ToggleManager;

    return-object v0
.end method

.method static synthetic byG(Lcom/android/settings/ToggleArrangementFragment;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHP:Landroid/view/View;

    return-object p1
.end method

.method static synthetic byz(Lcom/android/settings/ToggleArrangementFragment;)Lcom/android/settings/aT;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHO:Lcom/android/settings/aT;

    return-object v0
.end method


# virtual methods
.method public OnOrderChanged(II)V
    .locals 5

    const/16 v4, 0x1d

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p2, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHQ:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHQ:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v4, :cond_2

    const/4 v0, -0x1

    :goto_1
    iget-object v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    iget v2, p0, Lcom/android/settings/ToggleArrangementFragment;->bHQ:I

    add-int/2addr v2, v0

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    iget v3, p0, Lcom/android/settings/ToggleArrangementFragment;->bHQ:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHQ:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHV:Lmiui/app/ToggleManager;

    iget-object v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lmiui/app/ToggleManager;->setUserSelectedToggleOrder(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHO:Lcom/android/settings/aT;

    invoke-virtual {v0}, Lcom/android/settings/aT;->notifyDataSetChanged()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHV:Lmiui/app/ToggleManager;

    iget-object v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHT:Lmiui/app/ToggleManager$OnToggleChangedListener;

    invoke-virtual {v0, v1}, Lmiui/app/ToggleManager;->removeToggleChangedListener(Lmiui/app/ToggleManager$OnToggleChangedListener;)V

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onDestroy()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0d01e1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    iput-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHR:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/ToggleArrangementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/settings/dc;->bYu(Landroid/app/Activity;Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHR:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmiui/app/ToggleManager;->createInstance(Landroid/content/Context;)Lmiui/app/ToggleManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHV:Lmiui/app/ToggleManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmiui/app/ToggleManager;->getUserSelectedToggleOrder(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHU:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmiui/app/ToggleManager;->getEditFixedPosition(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHQ:I

    new-instance v0, Lcom/android/settings/aT;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/aT;-><init>(Lcom/android/settings/ToggleArrangementFragment;Lcom/android/settings/aT;)V

    iput-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHO:Lcom/android/settings/aT;

    new-instance v0, Lcom/android/settings/ap;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/ap;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHS:Lcom/android/settings/ap;

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHR:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setDividerHeight(I)V

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHR:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setVerticalFadingEdgeEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHR:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    iget-object v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHO:Lcom/android/settings/aT;

    invoke-virtual {v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHR:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    invoke-virtual {v0, p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setOnOrderChangedListener(Lcom/android/settings/toggleposition/c;)V

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHR:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    iget v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHQ:I

    invoke-virtual {v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setDividerItemPosition(I)V

    new-instance v0, Lcom/android/settings/gE;

    invoke-direct {v0, p0}, Lcom/android/settings/gE;-><init>(Lcom/android/settings/ToggleArrangementFragment;)V

    iput-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHT:Lmiui/app/ToggleManager$OnToggleChangedListener;

    iget-object v0, p0, Lcom/android/settings/ToggleArrangementFragment;->bHV:Lmiui/app/ToggleManager;

    iget-object v1, p0, Lcom/android/settings/ToggleArrangementFragment;->bHT:Lmiui/app/ToggleManager$OnToggleChangedListener;

    invoke-virtual {v0, v1}, Lmiui/app/ToggleManager;->setOnToggleChangedListener(Lmiui/app/ToggleManager$OnToggleChangedListener;)V

    return-void
.end method
