.class final enum Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;
.super Ljava/lang/Enum;
.source "SetUpChooseLockPattern.java"


# static fields
.field private static final synthetic bNF:[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNH:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNI:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNJ:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNK:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNM:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNN:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNO:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

.field public static final enum bNP:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;


# instance fields
.field final footerMessage:I

.field headerMessage:I

.field final leftMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

.field final patternEnabled:Z

.field final rightMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "Introduction"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNC:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/4 v2, 0x0

    const v3, 0x7f1209d4

    const/4 v6, -0x1

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "HelpScreen"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bND:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/4 v2, 0x1

    const v3, 0x7f1209e0

    const/4 v6, -0x1

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNK:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "ChoiceTooShort"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNC:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/4 v2, 0x2

    const v3, 0x7f1209d1

    const/4 v6, -0x1

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNH:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "FirstChoiceValid"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNC:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/4 v2, 0x3

    const v3, 0x7f1209cd

    const/4 v6, -0x1

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNJ:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "NeedToConfirm"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNv:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNz:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/4 v2, 0x4

    const v3, 0x7f1209ca

    const/4 v6, -0x1

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNM:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "ConfirmWrong"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNv:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNz:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/4 v2, 0x5

    const v3, 0x7f1209cb

    const/4 v6, -0x1

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNI:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "ChoiceConfirmed"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNv:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNy:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/4 v2, 0x6

    const v3, 0x7f1209cc

    const/4 v6, -0x1

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "SameWithOwnerUser"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNC:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/4 v2, 0x7

    const v3, 0x7f1209cf

    const/4 v6, -0x1

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNO:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "SameWithSecuritySpaceUser"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNC:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/16 v2, 0x8

    const v3, 0x7f1209d0

    const/4 v6, -0x1

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNP:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    new-instance v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const-string/jumbo v1, "SameWithOtherSpaceUser"

    sget-object v4, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;->bNu:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    sget-object v5, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;->bNC:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    const/16 v2, 0x9

    const v3, 0x7f1209ce

    const/4 v6, -0x1

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;-><init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNN:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNL:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNK:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNH:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNJ:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNM:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNI:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNG:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNO:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNP:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNN:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNF:[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->headerMessage:I

    iput-object p4, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->leftMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$LeftButtonMode;

    iput-object p5, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->rightMode:Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$RightButtonMode;

    iput p6, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->footerMessage:I

    iput-boolean p7, p0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->patternEnabled:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;
    .locals 1

    const-class v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    return-object v0
.end method

.method public static values()[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;
    .locals 1

    sget-object v0, Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;->bNF:[Lcom/android/settings/SetUpChooseLockPattern$SetUpChooseLockPatternFragment$Stage;

    return-object v0
.end method
