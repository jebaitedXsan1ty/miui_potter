.class final Lcom/android/settings/iX;
.super Ljava/lang/Object;
.source "FramesSequenceAnimation.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic cpd:Lcom/android/settings/bv;


# direct methods
.method constructor <init>(Lcom/android/settings/bv;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v0}, Lcom/android/settings/bv;->bKk(Lcom/android/settings/bv;)Ljava/lang/ref/SoftReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1}, Lcom/android/settings/bv;->bKg(Lcom/android/settings/bv;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v0, v6}, Lcom/android/settings/bv;->bKo(Lcom/android/settings/bv;Z)Z

    iget-object v0, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v0}, Lcom/android/settings/bv;->bKe(Lcom/android/settings/bv;)Lcom/android/settings/bw;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v0}, Lcom/android/settings/bv;->bKe(Lcom/android/settings/bv;)Lcom/android/settings/bw;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/settings/bw;->bKu()V

    :cond_1
    const-string/jumbo v0, "FramesSequenceAnimation"

    const-string/jumbo v1, "animation stop"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    const/4 v3, 0x1

    invoke-static {v1, v3}, Lcom/android/settings/bv;->bKo(Lcom/android/settings/bv;Z)Z

    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1}, Lcom/android/settings/bv;->bKj(Lcom/android/settings/bv;)Landroid/os/Handler;

    move-result-object v1

    iget-object v3, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v3}, Lcom/android/settings/bv;->bKi(Lcom/android/settings/bv;)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1}, Lcom/android/settings/bv;->bKq(Lcom/android/settings/bv;)I

    move-result v3

    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1}, Lcom/android/settings/bv;->bKf(Lcom/android/settings/bv;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1}, Lcom/android/settings/bv;->bKe(Lcom/android/settings/bv;)Lcom/android/settings/bw;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/settings/bw;->bKs()V

    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1, v6}, Lcom/android/settings/bv;->bKn(Lcom/android/settings/bv;Z)Z

    :cond_3
    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1}, Lcom/android/settings/bv;->bKd(Lcom/android/settings/bv;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1}, Lcom/android/settings/bv;->bKe(Lcom/android/settings/bv;)Lcom/android/settings/bw;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/settings/bw;->bKr()V

    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1, v6}, Lcom/android/settings/bv;->bKm(Lcom/android/settings/bv;Z)Z

    :cond_4
    iget-object v1, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v1}, Lcom/android/settings/bv;->bKh(Lcom/android/settings/bv;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_7

    :try_start_0
    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v4}, Lcom/android/settings/bv;->bKl(Lcom/android/settings/bv;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v4

    invoke-static {v1, v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_5
    :goto_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v2

    goto :goto_0

    :cond_6
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v0}, Lcom/android/settings/bv;->bKh(Lcom/android/settings/bv;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v0, p0, Lcom/android/settings/iX;->cpd:Lcom/android/settings/bv;

    invoke-static {v0, v2}, Lcom/android/settings/bv;->bKp(Lcom/android/settings/bv;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto :goto_1

    :cond_7
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
