.class Lcom/android/settings/aS;
.super Landroid/widget/ArrayAdapter;
.source "SettingsFragment.java"


# instance fields
.field private final bHj:I

.field private bHk:Landroid/view/LayoutInflater;

.field private bHl:Ljava/lang/String;

.field bHm:Ljava/util/List;

.field final synthetic bHn:Lcom/android/settings/SettingsFragment;

.field private mContext:Landroid/content/Context;

.field private mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Lcom/android/settings/SettingsFragment;Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/aS;->bHn:Lcom/android/settings/SettingsFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/settings/aS;->bHj:I

    iput-object p2, p0, Lcom/android/settings/aS;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/aS;->bHm:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/aS;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/settings/aS;->bHk:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/android/settings/aS;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/aS;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-static {}, Lcom/android/settings/SettingsFragment;->bxS()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/aS;->bHl:Ljava/lang/String;

    return-void
.end method

.method private bxY(Lcom/android/settings/search/SearchResultItem;)I
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p1, Lcom/android/settings/search/SearchResultItem;->type:I

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method private bya(Landroid/view/View;Lcom/android/settings/aR;Lcom/android/settings/search/SearchResultItem;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p3, Lcom/android/settings/search/SearchResultItem;->title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p3, Lcom/android/settings/search/SearchResultItem;->path:Ljava/lang/String;

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/aS;->bHn:Lcom/android/settings/SettingsFragment;

    iget-object v2, p0, Lcom/android/settings/aS;->bHn:Lcom/android/settings/SettingsFragment;

    invoke-static {v2}, Lcom/android/settings/SettingsFragment;->bxJ(Lcom/android/settings/SettingsFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/aS;->bHl:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/android/settings/SettingsFragment;->bxR(Lcom/android/settings/SettingsFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p2, Lcom/android/settings/aR;->bHh:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p3, Lcom/android/settings/search/SearchResultItem;->path:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p2, Lcom/android/settings/aR;->bHi:Landroid/widget/TextView;

    iget-object v1, p3, Lcom/android/settings/search/SearchResultItem;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p3, Lcom/android/settings/search/SearchResultItem;->icon:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p3, Lcom/android/settings/search/SearchResultItem;->pkg:Ljava/lang/String;

    const-string/jumbo v1, "com.android.settings"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/aS;->bHn:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0}, Lcom/android/settings/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p3, Lcom/android/settings/search/SearchResultItem;->icon:Ljava/lang/String;

    const-string/jumbo v2, "drawable"

    const-string/jumbo v3, "com.android.settings"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v1, p2, Lcom/android/settings/aR;->bHg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    iget v0, p3, Lcom/android/settings/search/SearchResultItem;->status:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    const v0, 0x3e99999a    # 0.3f

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    new-instance v0, Lcom/android/settings/gn;

    invoke-direct {v0, p0, p3}, Lcom/android/settings/gn;-><init>(Lcom/android/settings/aS;Lcom/android/settings/search/SearchResultItem;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_3
    iget-object v0, p2, Lcom/android/settings/aR;->bHg:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_4
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/aS;->bHn:Lcom/android/settings/SettingsFragment;

    invoke-virtual {v0}, Lcom/android/settings/SettingsFragment;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p3, Lcom/android/settings/search/SearchResultItem;->pkg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p2, Lcom/android/settings/aR;->bHg:Landroid/widget/ImageView;

    iget-object v2, p3, Lcom/android/settings/search/SearchResultItem;->icon:Ljava/lang/String;

    const-string/jumbo v3, "drawable"

    iget-object v4, p3, Lcom/android/settings/search/SearchResultItem;->pkg:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p2, Lcom/android/settings/aR;->bHg:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_5
    iget-object v0, p2, Lcom/android/settings/aR;->bHg:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_6
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_1
.end method


# virtual methods
.method public bxZ(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/aS;->bHm:Ljava/util/List;

    invoke-virtual {p0}, Lcom/android/settings/aS;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/aS;->bHm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/android/settings/search/SearchResultItem;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/aS;->bHm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/aS;->bHm:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/aS;->bHm:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/search/SearchResultItem;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/aS;->bHm:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/search/SearchResultItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/aS;->getItem(I)Lcom/android/settings/search/SearchResultItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/settings/aS;->getItem(I)Lcom/android/settings/search/SearchResultItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/aS;->bxY(Lcom/android/settings/search/SearchResultItem;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/android/settings/aS;->getItem(I)Lcom/android/settings/search/SearchResultItem;

    move-result-object v2

    iget v3, v2, Lcom/android/settings/search/SearchResultItem;->type:I

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/aR;

    :goto_0
    packed-switch v3, :pswitch_data_0

    :goto_1
    :pswitch_0
    return-object p2

    :cond_0
    new-instance v1, Lcom/android/settings/aR;

    invoke-direct {v1, v0}, Lcom/android/settings/aR;-><init>(Lcom/android/settings/aR;)V

    packed-switch v3, :pswitch_data_1

    move-object p2, v0

    :goto_2
    if-eqz p2, :cond_1

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/aS;->bHk:Landroid/view/LayoutInflater;

    const v4, 0x7f0d01b0

    invoke-virtual {v0, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p0}, Lcom/android/settings/aS;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v4, Lmiui/R$attr;->preferenceWithIconBackground:I

    invoke-static {v0, v4}, Lmiui/util/AttributeResolver;->resolveDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f0a03e1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/settings/aR;->bHg:Landroid/widget/ImageView;

    const v0, 0x7f0a03e2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/settings/aR;->bHh:Landroid/widget/TextView;

    const v0, 0x7f0a03e3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/settings/aR;->bHi:Landroid/widget/TextView;

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/aS;->bHk:Landroid/view/LayoutInflater;

    const v4, 0x7f0d01ae

    invoke-virtual {v0, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_2

    :pswitch_3
    invoke-direct {p0, p2, v0, v2}, Lcom/android/settings/aS;->bya(Landroid/view/View;Lcom/android/settings/aR;Lcom/android/settings/search/SearchResultItem;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
