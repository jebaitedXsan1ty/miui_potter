.class final Lcom/android/settings/c;
.super Landroid/database/ContentObserver;
.source "MiuiSoundSettings.java"


# instance fields
.field private final brG:Landroid/net/Uri;

.field private final brH:Landroid/net/Uri;

.field final synthetic brI:Lcom/android/settings/MiuiSoundSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/MiuiSoundSettings;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/c;->brI:Lcom/android/settings/MiuiSoundSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiSoundSettings;->beO(Lcom/android/settings/MiuiSoundSettings;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo v0, "zen_mode"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/c;->brH:Landroid/net/Uri;

    const-string/jumbo v0, "zen_mode_config_etag"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/c;->brG:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public beQ()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/android/settings/c;->brI:Lcom/android/settings/MiuiSoundSettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSoundSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/c;->brH:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lcom/android/settings/c;->brI:Lcom/android/settings/MiuiSoundSettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSoundSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/c;->brG:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method

.method public beR()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/c;->brI:Lcom/android/settings/MiuiSoundSettings;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSoundSettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 3

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/c;->brI:Lcom/android/settings/MiuiSoundSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiSoundSettings;->beO(Lcom/android/settings/MiuiSoundSettings;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/c;->brI:Lcom/android/settings/MiuiSoundSettings;

    invoke-static {v1}, Lcom/android/settings/MiuiSoundSettings;->beO(Lcom/android/settings/MiuiSoundSettings;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
