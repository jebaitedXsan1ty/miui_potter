.class Lcom/android/settings/aT;
.super Landroid/widget/BaseAdapter;
.source "ToggleArrangementFragment.java"


# instance fields
.field bHW:Landroid/view/View$OnClickListener;

.field final synthetic bHX:Lcom/android/settings/ToggleArrangementFragment;


# direct methods
.method private constructor <init>(Lcom/android/settings/ToggleArrangementFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Lcom/android/settings/gD;

    invoke-direct {v0, p0}, Lcom/android/settings/gD;-><init>(Lcom/android/settings/aT;)V

    iput-object v0, p0, Lcom/android/settings/aT;->bHW:Landroid/view/View$OnClickListener;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/ToggleArrangementFragment;Lcom/android/settings/aT;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/aT;-><init>(Lcom/android/settings/ToggleArrangementFragment;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byE(Lcom/android/settings/ToggleArrangementFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byE(Lcom/android/settings/ToggleArrangementFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byE(Lcom/android/settings/ToggleArrangementFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const v5, 0x7f0a042d

    const/4 v4, 0x0

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object p2, v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byE(Lcom/android/settings/ToggleArrangementFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v0, 0x1d

    if-ne v2, v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byA(Lcom/android/settings/ToggleArrangementFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byA(Lcom/android/settings/ToggleArrangementFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byC(Lcom/android/settings/ToggleArrangementFragment;)Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    const v2, 0x7f0d01e2

    invoke-virtual {v0, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/ToggleArrangementFragment;->byG(Lcom/android/settings/ToggleArrangementFragment;Landroid/view/View;)Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-virtual {v0}, Lcom/android/settings/ToggleArrangementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v1}, Lcom/android/settings/ToggleArrangementFragment;->byB(Lcom/android/settings/ToggleArrangementFragment;)I

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v3}, Lcom/android/settings/ToggleArrangementFragment;->byB(Lcom/android/settings/ToggleArrangementFragment;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f100036

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byA(Lcom/android/settings/ToggleArrangementFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byA(Lcom/android/settings/ToggleArrangementFragment;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_3
    if-nez p2, :cond_4

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byC(Lcom/android/settings/ToggleArrangementFragment;)Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d01e3

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f0a0155

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v1}, Lcom/android/settings/ToggleArrangementFragment;->byC(Lcom/android/settings/ToggleArrangementFragment;)Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getListenerForStartingSort()Landroid/view/View$OnTouchListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byC(Lcom/android/settings/ToggleArrangementFragment;)Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getLongPressListenerForStartingSort()Landroid/view/View$OnTouchListener;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/settings/aT;->bHW:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    const v0, 0x7f0a049a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a01f0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-static {v2, v0}, Lmiui/app/ToggleManager;->updateTextView(ILandroid/widget/TextView;)V

    iget-object v0, p0, Lcom/android/settings/aT;->bHX:Lcom/android/settings/ToggleArrangementFragment;

    invoke-static {v0}, Lcom/android/settings/ToggleArrangementFragment;->byD(Lcom/android/settings/ToggleArrangementFragment;)Lcom/android/settings/ap;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Lcom/android/settings/ap;->bqn(ILandroid/widget/ImageView;)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object p2
.end method
