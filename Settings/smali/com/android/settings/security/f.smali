.class final Lcom/android/settings/security/f;
.super Ljava/lang/Object;
.source "SecurityFeatureProviderImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic bfV:Lcom/android/settings/security/b;

.field final synthetic bfW:Ljava/lang/String;

.field final synthetic bfX:Landroid/support/v7/preference/Preference;


# direct methods
.method constructor <init>(Lcom/android/settings/security/b;Ljava/lang/String;Landroid/support/v7/preference/Preference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/security/f;->bfV:Lcom/android/settings/security/b;

    iput-object p2, p0, Lcom/android/settings/security/f;->bfW:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/security/f;->bfX:Landroid/support/v7/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/security/f;->bfW:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/security/f;->bfX:Landroid/support/v7/preference/Preference;

    invoke-virtual {v0}, Landroid/support/v7/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/security/f;->bfX:Landroid/support/v7/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/security/f;->bfW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/security/f;->bfW:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/security/f;->bfX:Landroid/support/v7/preference/Preference;

    invoke-virtual {v1}, Landroid/support/v7/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/security/f;->bfX:Landroid/support/v7/preference/Preference;

    iget-object v1, p0, Lcom/android/settings/security/f;->bfW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
