.class public Lcom/android/settings/security/b;
.super Ljava/lang/Object;
.source "SecurityFeatureProviderImpl.java"

# interfaces
.implements Lcom/android/settings/security/d;


# static fields
.field static final DEFAULT_ICON:Landroid/graphics/drawable/Drawable;

.field static sIconCache:Ljava/util/Map;

.field static sSummaryCache:Ljava/util/Map;


# instance fields
.field private bfN:Lcom/android/settings/d/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lcom/android/settings/security/b;->sIconCache:Ljava/util/Map;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lcom/android/settings/security/b;->sSummaryCache:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static initPreferences(Landroid/content/Context;Landroid/support/v7/preference/PreferenceScreen;Lcom/android/settingslib/drawer/DashboardCategory;)V
    .locals 9

    const/4 v1, 0x0

    const/4 v8, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/android/settingslib/drawer/DashboardCategory;->cek()I

    move-result v0

    move v2, v0

    :goto_0
    move v4, v1

    :goto_1
    if-ge v4, v2, :cond_5

    invoke-virtual {p2, v4}, Lcom/android/settingslib/drawer/DashboardCategory;->cei(I)Lcom/android/settingslib/drawer/Tile;

    move-result-object v5

    iget-object v0, v5, Lcom/android/settingslib/drawer/Tile;->key:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v5, Lcom/android/settingslib/drawer/Tile;->czw:Landroid/os/Bundle;

    if-nez v0, :cond_2

    :cond_0
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    iget-object v0, v5, Lcom/android/settingslib/drawer/Tile;->key:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v0, v5, Lcom/android/settingslib/drawer/Tile;->czw:Landroid/os/Bundle;

    const-string/jumbo v1, "com.android.settings.icon_uri"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/android/settings/security/b;->DEFAULT_ICON:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    sget-object v1, Lcom/android/settings/security/b;->sIconCache:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lcom/android/settings/security/b;->sIconCache:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    invoke-virtual {v1, v0, v7}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_3
    invoke-virtual {v6, v0}, Landroid/support/v7/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, v5, Lcom/android/settingslib/drawer/Tile;->czw:Landroid/os/Bundle;

    const-string/jumbo v1, "com.android.settings.summary_uri"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f1211e8

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_3

    sget-object v3, Lcom/android/settings/security/b;->sSummaryCache:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v0, Lcom/android/settings/security/b;->sSummaryCache:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_3
    invoke-virtual {v6, v0}, Landroid/support/v7/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    move-object v0, v3

    goto :goto_3

    :catch_0
    move-exception v0

    move-object v0, v3

    goto :goto_3

    :cond_5
    return-void

    :cond_6
    move-object v0, v3

    goto :goto_3
.end method


# virtual methods
.method public aTZ()Lcom/android/settings/d/b;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/security/b;->bfN:Lcom/android/settings/d/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/d/a;

    invoke-direct {v0}, Lcom/android/settings/d/a;-><init>()V

    iput-object v0, p0, Lcom/android/settings/security/b;->bfN:Lcom/android/settings/d/b;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/security/b;->bfN:Lcom/android/settings/d/b;

    return-object v0
.end method

.method updatePreferencesToRunOnWorkerThread(Landroid/content/Context;Landroid/support/v7/preference/PreferenceScreen;Lcom/android/settingslib/drawer/DashboardCategory;)V
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/android/settingslib/drawer/DashboardCategory;->cek()I

    move-result v0

    :goto_0
    new-instance v4, Landroid/util/ArrayMap;

    invoke-direct {v4}, Landroid/util/ArrayMap;-><init>()V

    move v3, v1

    :goto_1
    if-ge v3, v0, :cond_5

    invoke-virtual {p3, v3}, Lcom/android/settingslib/drawer/DashboardCategory;->cei(I)Lcom/android/settingslib/drawer/Tile;

    move-result-object v1

    iget-object v5, v1, Lcom/android/settingslib/drawer/Tile;->key:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v1, Lcom/android/settingslib/drawer/Tile;->czw:Landroid/os/Bundle;

    if-nez v5, :cond_2

    :cond_0
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v5, v1, Lcom/android/settingslib/drawer/Tile;->key:Ljava/lang/String;

    invoke-virtual {p2, v5}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v6, v1, Lcom/android/settingslib/drawer/Tile;->czw:Landroid/os/Bundle;

    const-string/jumbo v7, "com.android.settings.icon_uri"

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v1, Lcom/android/settingslib/drawer/Tile;->czw:Landroid/os/Bundle;

    const-string/jumbo v8, "com.android.settings.summary_uri"

    invoke-virtual {v7, v8, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, v1, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    if-eqz v8, :cond_6

    iget-object v1, v1, Lcom/android/settingslib/drawer/Tile;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-static {p1, v1, v6, v4}, Lcom/android/settingslib/drawer/k;->cey(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Landroid/util/Pair;

    move-result-object v1

    if-eqz v1, :cond_3

    sget-object v8, Lcom/android/settings/security/b;->sIconCache:Ljava/util/Map;

    invoke-interface {v8, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-direct {v6, v8}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v8, Lcom/android/settings/security/e;

    invoke-direct {v8, p0, v5, p1, v1}, Lcom/android/settings/security/e;-><init>(Lcom/android/settings/security/b;Landroid/support/v7/preference/Preference;Landroid/content/Context;Landroid/util/Pair;)V

    invoke-virtual {v6, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_3
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "com.android.settings.summary"

    invoke-static {p1, v7, v4, v1}, Lcom/android/settingslib/drawer/k;->cex(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lcom/android/settings/security/b;->sSummaryCache:Ljava/util/Map;

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v7, Lcom/android/settings/security/f;

    invoke-direct {v7, p0, v1, v5}, Lcom/android/settings/security/f;-><init>(Lcom/android/settings/security/b;Ljava/lang/String;Landroid/support/v7/preference/Preference;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    :cond_4
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_5
    return-void

    :cond_6
    move-object v1, v2

    goto :goto_3
.end method
