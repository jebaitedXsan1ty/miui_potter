.class public final Lcom/android/settings/bluetooth/BluetoothEnabler;
.super Ljava/lang/Object;
.source "BluetoothEnabler.java"

# interfaces
.implements Lcom/android/settings/widget/j;


# instance fields
.field private final aDh:Landroid/content/IntentFilter;

.field private final aDi:Lcom/android/settingslib/bluetooth/d;

.field private final aDj:I

.field private final aDk:Lcom/android/settings/bluetooth/RestrictionUtils;

.field private final aDl:Landroid/widget/Switch;

.field private final aDm:Lcom/android/settings/widget/i;

.field private aDn:Z

.field private mContext:Landroid/content/Context;

.field private final mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/widget/i;Lcom/android/settings/core/instrumentation/e;Lcom/android/settingslib/bluetooth/q;ILcom/android/settings/bluetooth/RestrictionUtils;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/bluetooth/BluetoothEnabler$1;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/BluetoothEnabler$1;-><init>(Lcom/android/settings/bluetooth/BluetoothEnabler;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iput-object p2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->getSwitch()Landroid/widget/Switch;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDl:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/i;->aAj(Lcom/android/settings/widget/j;)V

    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDn:Z

    iput p5, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDj:I

    if-nez p4, :cond_0

    iput-object v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDi:Lcom/android/settingslib/bluetooth/d;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    :goto_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDh:Landroid/content/IntentFilter;

    iput-object p6, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDk:Lcom/android/settings/bluetooth/RestrictionUtils;

    return-void

    :cond_0
    invoke-virtual {p4}, Lcom/android/settingslib/bluetooth/q;->ckl()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDi:Lcom/android/settingslib/bluetooth/d;

    goto :goto_0
.end method

.method private setChecked(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->isChecked()Z

    move-result v0

    if-eq p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAm()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/i;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDn:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAk()V

    :cond_1
    return-void
.end method


# virtual methods
.method public acB(Z)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothEnabler;->maybeEnforceRestrictions()Z

    move-result v0

    if-eqz v0, :cond_0

    return v4

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "bluetooth"

    invoke-static {v0, v1}, Lcom/android/settingslib/G;->csg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    const v1, 0x7f121549

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDl:Landroid/widget/Switch;

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setChecked(Z)V

    return v3

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mMetricsFeatureProvider:Lcom/android/settings/core/instrumentation/e;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDj:I

    invoke-virtual {v0, v1, v2, p1}, Lcom/android/settings/core/instrumentation/e;->ajV(Landroid/content/Context;IZ)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDi:Lcom/android/settingslib/bluetooth/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDi:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/d;->cjy(Z)Z

    move-result v0

    if-eqz p1, :cond_2

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDl:Landroid/widget/Switch;

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDl:Landroid/widget/Switch;

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v3}, Lcom/android/settings/widget/i;->aAo(Z)V

    return v3

    :cond_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v3}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    return v4
.end method

.method aqS(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, v1}, Lcom/android/settings/bluetooth/BluetoothEnabler;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v2}, Lcom/android/settings/bluetooth/BluetoothEnabler;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v1}, Lcom/android/settings/bluetooth/BluetoothEnabler;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public aqT(Landroid/content/Context;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothEnabler;->maybeEnforceRestrictions()Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDi:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/i;->setEnabled(Z)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDi:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothEnabler;->aqS(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAk()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDh:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDn:Z

    return-void
.end method

.method maybeEnforceRestrictions()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDk:Lcom/android/settings/bluetooth/RestrictionUtils;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "no_bluetooth"

    invoke-virtual {v0, v2, v3}, Lcom/android/settings/bluetooth/RestrictionUtils;->aqP(Landroid/content/Context;Ljava/lang/String;)Lcom/android/settingslib/n;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDk:Lcom/android/settings/bluetooth/RestrictionUtils;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "no_config_bluetooth"

    invoke-virtual {v0, v2, v3}, Lcom/android/settings/bluetooth/RestrictionUtils;->aqP(Landroid/content/Context;Ljava/lang/String;)Lcom/android/settingslib/n;

    move-result-object v0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v2, v0}, Lcom/android/settings/widget/i;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v2, v1}, Lcom/android/settings/widget/i;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDl:Landroid/widget/Switch;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDl:Landroid/widget/Switch;

    invoke-virtual {v2, v1}, Landroid/widget/Switch;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDl:Landroid/widget/Switch;

    invoke-virtual {v2, v1}, Landroid/widget/Switch;->setChecked(Z)V

    :cond_1
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDi:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDn:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDm:Lcom/android/settings/widget/i;

    invoke-virtual {v0}, Lcom/android/settings/widget/i;->aAm()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothEnabler;->aDn:Z

    :cond_1
    return-void
.end method
