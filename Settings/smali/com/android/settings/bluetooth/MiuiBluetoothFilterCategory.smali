.class public Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;
.super Landroid/preference/PreferenceCategory;
.source "MiuiBluetoothFilterCategory.java"


# instance fields
.field private aDe:Z

.field private aDf:I

.field private aDg:Lcom/android/settings/bluetooth/MiuiMiscBluetoothPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDf:I

    iput-boolean v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDe:Z

    const v0, 0x7f0d0125

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->setLayoutResource(I)V

    iput v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDf:I

    new-instance v0, Lcom/android/settings/bluetooth/MiuiMiscBluetoothPreference;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/android/settings/bluetooth/MiuiMiscBluetoothPreference;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDg:Lcom/android/settings/bluetooth/MiuiMiscBluetoothPreference;

    iput-boolean v2, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDe:Z

    return-void
.end method


# virtual methods
.method protected aqR(I)V
    .locals 1

    if-lez p1, :cond_1

    iget v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDf:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDf:I

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDe:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDg:Lcom/android/settings/bluetooth/MiuiMiscBluetoothPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->addPreference(Landroid/preference/Preference;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDe:Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDg:Lcom/android/settings/bluetooth/MiuiMiscBluetoothPreference;

    invoke-virtual {v0, p1}, Lcom/android/settings/bluetooth/MiuiMiscBluetoothPreference;->aqQ(I)V

    :cond_1
    return-void
.end method

.method public removeAll()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/preference/PreferenceCategory;->removeAll()V

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDf:I

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiBluetoothFilterCategory;->aDe:Z

    return-void
.end method
