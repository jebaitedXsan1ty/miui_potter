.class public Lcom/android/settings/bluetooth/BluetoothStatusController;
.super Lcom/android/settings/bA;
.source "BluetoothStatusController.java"


# instance fields
.field private aDb:Z

.field private final aDc:Landroid/content/IntentFilter;

.field private final aDd:Lcom/android/settingslib/bluetooth/d;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bA;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    new-instance v0, Lcom/android/settings/bluetooth/BluetoothStatusController$1;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/BluetoothStatusController$1;-><init>(Lcom/android/settings/bluetooth/BluetoothStatusController;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {p1}, Lcom/android/settings/bluetooth/Utils;->arq(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/q;

    move-result-object v0

    if-nez v0, :cond_0

    iput-object v1, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDd:Lcom/android/settingslib/bluetooth/d;

    :goto_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDc:Landroid/content/IntentFilter;

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/q;->ckl()Lcom/android/settingslib/bluetooth/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDd:Lcom/android/settingslib/bluetooth/d;

    goto :goto_0
.end method


# virtual methods
.method aqO(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->bRZ:Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->bRZ:Landroid/widget/TextView;

    const v1, 0x7f121656

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->bRZ:Landroid/widget/TextView;

    const v1, 0x7f121657

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public pause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDd:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDb:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDb:Z

    :cond_1
    return-void
.end method

.method public wt()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDd:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDd:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothStatusController;->aqO(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDc:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDb:Z

    return-void
.end method

.method protected wv()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDd:Lcom/android/settingslib/bluetooth/d;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->aDd:Lcom/android/settingslib/bluetooth/d;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/d;->cjJ()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothStatusController;->aqO(I)V

    return-void
.end method
