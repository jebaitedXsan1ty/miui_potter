.class public Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;
.super Lcom/android/settings/core/c;
.source "BluetoothMasterSwitchPreferenceController.java"

# interfaces
.implements Lcom/android/settings/widget/x;
.implements Lcom/android/settings/core/lifecycle/b;
.implements Lcom/android/settings/core/lifecycle/a/b;
.implements Lcom/android/settings/core/lifecycle/a/d;
.implements Lcom/android/settings/core/lifecycle/a/c;
.implements Lcom/android/settings/core/lifecycle/a/j;


# instance fields
.field private aDT:Lcom/android/settings/bluetooth/BluetoothEnabler;

.field private aDU:Lcom/android/settingslib/bluetooth/q;

.field private aDV:Lcom/android/settings/widget/MasterSwitchPreference;

.field private aDW:Lcom/android/settings/bluetooth/RestrictionUtils;

.field private mSummaryUpdater:Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/q;)V
    .locals 1

    new-instance v0, Lcom/android/settings/bluetooth/RestrictionUtils;

    invoke-direct {v0}, Lcom/android/settings/bluetooth/RestrictionUtils;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/q;Lcom/android/settings/bluetooth/RestrictionUtils;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/q;Lcom/android/settings/bluetooth/RestrictionUtils;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/settings/core/c;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDU:Lcom/android/settingslib/bluetooth/q;

    new-instance v0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDU:Lcom/android/settingslib/bluetooth/q;

    invoke-direct {v0, v1, p0, v2}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;-><init>(Landroid/content/Context;Lcom/android/settings/widget/x;Lcom/android/settingslib/bluetooth/q;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->mSummaryUpdater:Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;

    iput-object p3, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDW:Lcom/android/settings/bluetooth/RestrictionUtils;

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v7/preference/PreferenceScreen;)V
    .locals 7

    invoke-super {p0, p1}, Lcom/android/settings/core/c;->a(Landroid/support/v7/preference/PreferenceScreen;)V

    const-string/jumbo v0, "toggle_bluetooth"

    invoke-virtual {p1, v0}, Landroid/support/v7/preference/PreferenceScreen;->dlg(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/MasterSwitchPreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDV:Lcom/android/settings/widget/MasterSwitchPreference;

    new-instance v0, Lcom/android/settings/bluetooth/BluetoothEnabler;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/settings/widget/A;

    iget-object v3, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDV:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-direct {v2, v3}, Lcom/android/settings/widget/A;-><init>(Lcom/android/settings/widget/MasterSwitchPreference;)V

    iget-object v3, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/settings/overlay/a;->aIm()Lcom/android/settings/core/instrumentation/e;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDU:Lcom/android/settingslib/bluetooth/q;

    iget-object v6, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDW:Lcom/android/settings/bluetooth/RestrictionUtils;

    const/16 v5, 0x366

    invoke-direct/range {v0 .. v6}, Lcom/android/settings/bluetooth/BluetoothEnabler;-><init>(Landroid/content/Context;Lcom/android/settings/widget/i;Lcom/android/settings/core/instrumentation/e;Lcom/android/settingslib/bluetooth/q;ILcom/android/settings/bluetooth/RestrictionUtils;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDT:Lcom/android/settings/bluetooth/BluetoothEnabler;

    return-void
.end method

.method public ahr(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDV:Lcom/android/settings/widget/MasterSwitchPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDV:Lcom/android/settings/widget/MasterSwitchPreference;

    invoke-virtual {v0, p1}, Lcom/android/settings/widget/MasterSwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "toggle_bluetooth"

    return-object v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->mSummaryUpdater:Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aib(Z)V

    return-void
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->mSummaryUpdater:Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aib(Z)V

    return-void
.end method

.method public onStart()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDT:Lcom/android/settings/bluetooth/BluetoothEnabler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDT:Lcom/android/settings/bluetooth/BluetoothEnabler;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/BluetoothEnabler;->aqT(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDT:Lcom/android/settings/bluetooth/BluetoothEnabler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothMasterSwitchPreferenceController;->aDT:Lcom/android/settings/bluetooth/BluetoothEnabler;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/BluetoothEnabler;->pause()V

    :cond_0
    return-void
.end method
