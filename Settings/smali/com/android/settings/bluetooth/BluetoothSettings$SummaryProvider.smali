.class Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;
.super Ljava/lang/Object;
.source "BluetoothSettings.java"

# interfaces
.implements Lcom/android/settings/dashboard/D;
.implements Lcom/android/settings/widget/x;


# instance fields
.field private final aEv:Lcom/android/settingslib/bluetooth/q;

.field private final aEw:Lcom/android/settings/dashboard/C;

.field private final mContext:Landroid/content/Context;

.field mSummaryUpdater:Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/dashboard/C;Lcom/android/settingslib/bluetooth/q;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;->aEv:Lcom/android/settingslib/bluetooth/q;

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;->aEw:Lcom/android/settings/dashboard/C;

    new-instance v0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;->aEv:Lcom/android/settingslib/bluetooth/q;

    invoke-direct {v0, v1, p0, v2}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;-><init>(Landroid/content/Context;Lcom/android/settings/widget/x;Lcom/android/settingslib/bluetooth/q;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;->mSummaryUpdater:Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;

    return-void
.end method


# virtual methods
.method public ahr(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;->aEw:Lcom/android/settings/dashboard/C;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;->aEw:Lcom/android/settings/dashboard/C;

    invoke-virtual {v0, p0, p1}, Lcom/android/settings/dashboard/C;->Fd(Lcom/android/settings/dashboard/D;Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public jt(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSettings$SummaryProvider;->mSummaryUpdater:Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;

    invoke-virtual {v0, p1}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->aib(Z)V

    return-void
.end method
