.class final Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;
.super Ljava/lang/Object;
.source "DeviceProfilesSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic aFN:Lcom/android/settings/bluetooth/DeviceProfilesSettings;

.field final synthetic aFO:Lcom/android/settingslib/bluetooth/b;

.field final synthetic aFP:Lcom/android/settingslib/bluetooth/f;


# direct methods
.method constructor <init>(Lcom/android/settings/bluetooth/DeviceProfilesSettings;Lcom/android/settingslib/bluetooth/b;Lcom/android/settingslib/bluetooth/f;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFN:Lcom/android/settings/bluetooth/DeviceProfilesSettings;

    iput-object p2, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFO:Lcom/android/settingslib/bluetooth/b;

    iput-object p3, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFP:Lcom/android/settingslib/bluetooth/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFO:Lcom/android/settingslib/bluetooth/b;

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFP:Lcom/android/settingslib/bluetooth/f;

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/b;->ciN(Lcom/android/settingslib/bluetooth/f;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFP:Lcom/android/settingslib/bluetooth/f;

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFO:Lcom/android/settingslib/bluetooth/b;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/b;->ciV()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/settingslib/bluetooth/f;->ast(Landroid/bluetooth/BluetoothDevice;Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFP:Lcom/android/settingslib/bluetooth/f;

    instance-of v0, v0, Lcom/android/settingslib/bluetooth/H;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFO:Lcom/android/settingslib/bluetooth/b;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/b;->cjq(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFN:Lcom/android/settings/bluetooth/DeviceProfilesSettings;

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFP:Lcom/android/settingslib/bluetooth/f;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFN:Lcom/android/settings/bluetooth/DeviceProfilesSettings;

    iget-object v2, p0, Lcom/android/settings/bluetooth/DeviceProfilesSettings$2;->aFP:Lcom/android/settingslib/bluetooth/f;

    invoke-static {v1, v0, v2}, Lcom/android/settings/bluetooth/DeviceProfilesSettings;->asZ(Lcom/android/settings/bluetooth/DeviceProfilesSettings;Landroid/preference/CheckBoxPreference;Lcom/android/settingslib/bluetooth/f;)V

    :cond_1
    return-void
.end method
