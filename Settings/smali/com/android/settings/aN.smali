.class public Lcom/android/settings/aN;
.super Ljava/lang/Object;
.source "MiuiKeyguardSettingsHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bwN(Lcom/android/settings/KeyguardRestrictedListPreference;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/KeyguardRestrictedListPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {v0, p3, v1}, Lcom/android/settingslib/w;->cqR(Landroid/content/Context;II)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/cp;

    invoke-direct {v1, p1, p2, v0}, Lcom/android/settings/cp;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/android/settingslib/n;)V

    invoke-virtual {p0, v1}, Lcom/android/settings/KeyguardRestrictedListPreference;->bRv(Lcom/android/settings/cp;)V

    :cond_0
    return-void
.end method

.method public static bwO(Ljava/lang/String;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/settings/KeyguardAdvancedSettings;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p2, p0}, Lcom/android/settings/KeyguardAdvancedSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/MiuiRestrictedPreference;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/android/internal/widget/LockPatternUtils;->isDeviceOwnerInfoEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/android/settings/KeyguardAdvancedSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settingslib/w;->cqN(Landroid/content/Context;)Lcom/android/settingslib/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, v2}, Lcom/android/settingslib/MiuiRestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/android/internal/widget/LockPatternUtils;->isLockScreenDisabled(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method public static bwP(Landroid/content/Context;Landroid/os/Bundle;)I
    .locals 1

    invoke-static {p0, p1}, Lcom/android/settings/aq;->bqz(Landroid/content/Context;Landroid/os/Bundle;)I

    move-result v0

    return v0
.end method

.method public static bwQ(Landroid/app/Fragment;)Z
    .locals 1

    instance-of v0, p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;

    return v0
.end method

.method public static bwR(Landroid/app/Fragment;ILandroid/os/Bundle;)V
    .locals 0

    check-cast p0, Lcom/android/settings/MiuiSettingsPreferenceFragment;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->QH(ILandroid/os/Bundle;)V

    return-void
.end method

.method public static bwS(Landroid/os/Bundle;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/Activity;)I
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/app/Activity;->getActivityToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {p2}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v1, v3, v2}, Lcom/android/settings/aq;->bqL(Landroid/os/IBinder;Landroid/os/UserManager;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    const-string/jumbo v1, "android.app.action.SET_NEW_PARENT_PROFILE_PASSWORD"

    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeAllowed(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    :cond_0
    if-eqz p0, :cond_1

    :goto_0
    invoke-static {p2, p0}, Lcom/android/settings/aq;->bqz(Landroid/content/Context;Landroid/os/Bundle;)I

    move-result v0

    return v0

    :cond_1
    invoke-virtual {p2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p0

    goto :goto_0

    :cond_2
    return v0
.end method

.method public static bwT(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v0

    return v0
.end method

.method public static bwU(Lcom/android/internal/widget/LockPatternUtils;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeEnabled(I)Z

    move-result v0

    return v0
.end method

.method public static bwV(IZLandroid/preference/PreferenceScreen;Lcom/android/settings/MiuiSecurityChooseUnlock$MiuiSecurityChooseUnlockFragment;I)V
    .locals 11

    const-string/jumbo v0, "device_policy"

    invoke-virtual {p3, v0}, Lcom/android/settings/MiuiSecurityChooseUnlock$MiuiSecurityChooseUnlockFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p4}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;I)I

    move-result v6

    invoke-virtual {p3}, Lcom/android/settings/MiuiSecurityChooseUnlock$MiuiSecurityChooseUnlockFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p4}, Lcom/android/settingslib/w;->crh(Landroid/content/Context;I)Lcom/android/settingslib/n;

    move-result-object v7

    invoke-virtual {p2}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v5, v1

    :goto_0
    if-ltz v5, :cond_d

    invoke-virtual {p2, v5}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    instance-of v2, v1, Lcom/android/settingslib/MiuiRestrictedPreference;

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v8

    const/4 v4, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string/jumbo v9, "unlock_set_pattern"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    const/high16 v3, 0x10000

    if-gt p0, v3, :cond_2

    const/4 v4, 0x1

    :goto_1
    const/high16 v3, 0x10000

    if-le v6, v3, :cond_3

    const/4 v3, 0x1

    move v10, v3

    move v3, v4

    move v4, v10

    :goto_2
    if-eqz p1, :cond_0

    move v2, v3

    :cond_0
    if-nez v2, :cond_a

    invoke-virtual {p2, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    :goto_3
    add-int/lit8 v1, v5, -0x1

    move v5, v1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    move v10, v3

    move v3, v4

    move v4, v10

    goto :goto_2

    :cond_4
    const-string/jumbo v9, "unlock_set_pin"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v3, 0x0

    invoke-virtual {v0, v3, p4}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;I)I

    move-result v3

    const/high16 v4, 0x30000

    if-gt p0, v4, :cond_5

    const/4 v4, 0x4

    if-gt v3, v4, :cond_5

    const/4 v4, 0x1

    :goto_4
    const/high16 v3, 0x30000

    if-le v6, v3, :cond_6

    const/4 v3, 0x1

    move v10, v3

    move v3, v4

    move v4, v10

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    move v10, v3

    move v3, v4

    move v4, v10

    goto :goto_2

    :cond_7
    const-string/jumbo v9, "unlock_set_password"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    const/high16 v3, 0x60000

    if-gt p0, v3, :cond_8

    const/4 v4, 0x1

    :goto_5
    const/high16 v3, 0x60000

    if-le v6, v3, :cond_9

    const/4 v3, 0x1

    move v10, v3

    move v3, v4

    move v4, v10

    goto :goto_2

    :cond_8
    const/4 v4, 0x0

    goto :goto_5

    :cond_9
    const/4 v3, 0x0

    move v10, v3

    move v3, v4

    move v4, v10

    goto :goto_2

    :cond_a
    if-eqz v4, :cond_b

    if-eqz v7, :cond_b

    check-cast v1, Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {v1, v7}, Lcom/android/settingslib/MiuiRestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    goto :goto_3

    :cond_b
    if-nez v3, :cond_c

    move-object v2, v1

    check-cast v2, Lcom/android/settingslib/MiuiRestrictedPreference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/settingslib/MiuiRestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    const v2, 0x7f121305

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_3

    :cond_c
    check-cast v1, Lcom/android/settingslib/MiuiRestrictedPreference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/settingslib/MiuiRestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    goto :goto_3

    :cond_d
    return-void

    :cond_e
    move v10, v3

    move v3, v4

    move v4, v10

    goto :goto_2
.end method

.method public static bwW(Lcom/android/internal/widget/LockPatternUtils;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/internal/widget/LockPatternUtils;->setSeparateProfileChallengeEnabled(IZLjava/lang/String;)V

    invoke-virtual {p0, p2, p1}, Lcom/android/internal/widget/LockPatternUtils;->clearLock(Ljava/lang/String;I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->setLockScreenDisabled(ZI)V

    return-void
.end method

.method public static bwX(Landroid/content/Intent;I)V
    .locals 1

    const-string/jumbo v0, "android.intent.extra.USER_ID"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-void
.end method

.method public static bwY(Lcom/android/settings/BaseConfirmLockFragment;)V
    .locals 0

    return-void
.end method

.method public static bwZ()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static bxa(Z)I
    .locals 1

    if-eqz p0, :cond_0

    const v0, 0x7f12096a

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f12096c

    goto :goto_0
.end method

.method public static bxb()I
    .locals 1

    const v0, 0x7f12096b

    return v0
.end method

.method public static bxc()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static bxd(Lcom/android/internal/widget/LockPatternUtils;IZLjava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->setSeparateProfileChallengeEnabled(IZLjava/lang/String;)V

    return-void
.end method
