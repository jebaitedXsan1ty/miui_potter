.class public Lcom/android/settings/TimerView;
.super Landroid/widget/FrameLayout;
.source "TimerView.java"


# instance fields
.field private bFY:Landroid/graphics/Bitmap;

.field private bFZ:Landroid/graphics/Bitmap;

.field private bGa:Ljava/util/Calendar;

.field private bGb:Landroid/widget/TextView;

.field private bGc:I

.field private bGd:I

.field private bGe:I

.field private bGf:Landroid/widget/ImageView;

.field private bGg:Landroid/graphics/Matrix;

.field private bGh:Landroid/widget/ImageView;

.field private bGi:Landroid/widget/TextView;

.field private bGj:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/settings/TimerView;->bGg:Landroid/graphics/Matrix;

    invoke-direct {p0, p1}, Lcom/android/settings/TimerView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/settings/TimerView;->bGg:Landroid/graphics/Matrix;

    invoke-direct {p0, p1}, Lcom/android/settings/TimerView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/settings/TimerView;->bGg:Landroid/graphics/Matrix;

    invoke-direct {p0, p1}, Lcom/android/settings/TimerView;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 2

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0d0258

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/TimerView;->bFY:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/TimerView;->bFZ:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/android/settings/TimerView;->bFY:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/android/settings/TimerView;->bGj:I

    iget-object v0, p0, Lcom/android/settings/TimerView;->bFY:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/settings/TimerView;->bGe:I

    iget v0, p0, Lcom/android/settings/TimerView;->bGj:I

    shr-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/TimerView;->bGd:I

    iget v0, p0, Lcom/android/settings/TimerView;->bGe:I

    shr-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/TimerView;->bGc:I

    const v0, 0x7f0a04d7

    invoke-virtual {p0, v0}, Lcom/android/settings/TimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/TimerView;->bGi:Landroid/widget/TextView;

    const v0, 0x7f0a04d4

    invoke-virtual {p0, v0}, Lcom/android/settings/TimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/TimerView;->bGb:Landroid/widget/TextView;

    const v0, 0x7f0a00e3

    invoke-virtual {p0, v0}, Lcom/android/settings/TimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/TimerView;->bGf:Landroid/widget/ImageView;

    const v0, 0x7f0a00e4

    invoke-virtual {p0, v0}, Lcom/android/settings/TimerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/TimerView;->bGh:Landroid/widget/ImageView;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/TimerView;->bGa:Ljava/util/Calendar;

    return-void
.end method


# virtual methods
.method public setTimer(Ljava/lang/Long;)V
    .locals 9

    const/4 v5, 0x3

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/TimerView;->bGa:Ljava/util/Calendar;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, p0, Lcom/android/settings/TimerView;->bGa:Ljava/util/Calendar;

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/TimerView;->bGa:Ljava/util/Calendar;

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v7

    iget-object v2, p0, Lcom/android/settings/TimerView;->bGa:Ljava/util/Calendar;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const-string/jumbo v3, "%d:%02d:%02d"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/TimerView;->bGi:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/TimerView;->bGa:Ljava/util/Calendar;

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/TimerView;->bGa:Ljava/util/Calendar;

    invoke-virtual {v2, v8}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/android/settings/TimerView;->bGa:Ljava/util/Calendar;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const-string/jumbo v4, "%d.%d.%d"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/TimerView;->bGb:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/TimerView;->bGa:Ljava/util/Calendar;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/TimerView;->bGg:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    iget-object v2, p0, Lcom/android/settings/TimerView;->bGg:Landroid/graphics/Matrix;

    mul-int/lit8 v0, v0, 0x1e

    int-to-float v0, v0

    iget v3, p0, Lcom/android/settings/TimerView;->bGc:I

    int-to-float v3, v3

    iget v4, p0, Lcom/android/settings/TimerView;->bGd:I

    int-to-float v4, v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    iget-object v0, p0, Lcom/android/settings/TimerView;->bFY:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/android/settings/TimerView;->bGj:I

    iget v4, p0, Lcom/android/settings/TimerView;->bGe:I

    iget-object v5, p0, Lcom/android/settings/TimerView;->bGg:Landroid/graphics/Matrix;

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/TimerView;->bGf:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/settings/TimerView;->bGg:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lcom/android/settings/TimerView;->bGg:Landroid/graphics/Matrix;

    mul-int/lit8 v2, v7, 0x6

    int-to-float v2, v2

    iget v3, p0, Lcom/android/settings/TimerView;->bGc:I

    int-to-float v3, v3

    iget v4, p0, Lcom/android/settings/TimerView;->bGd:I

    int-to-float v4, v4

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    iget-object v0, p0, Lcom/android/settings/TimerView;->bFZ:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/android/settings/TimerView;->bGj:I

    iget v4, p0, Lcom/android/settings/TimerView;->bGe:I

    iget-object v5, p0, Lcom/android/settings/TimerView;->bGg:Landroid/graphics/Matrix;

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/TimerView;->bGh:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
