.class public Lcom/android/settings/notify/d;
.super Ljava/lang/Object;
.source "SettingsNotifyBuilder.java"


# instance fields
.field bmc:I

.field bmd:Landroid/content/Intent;

.field final synthetic bme:Lcom/android/settings/notify/c;

.field notifyId:I


# direct methods
.method public constructor <init>(Lcom/android/settings/notify/c;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notify/d;->bme:Lcom/android/settings/notify/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public aZA(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/notify/d;->notifyId:I

    return-void
.end method

.method public aZB(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/notify/d;->bmc:I

    return-void
.end method

.method public aZC(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notify/d;->bmd:Landroid/content/Intent;

    return-void
.end method

.method public aZy(Landroid/content/Context;)V
    .locals 1

    iget v0, p0, Lcom/android/settings/notify/d;->notifyId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/android/settings/notify/a;->aZo(Landroid/content/Context;Z)V

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/android/settings/notify/a;->aZt(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public aZz()I
    .locals 1

    iget v0, p0, Lcom/android/settings/notify/d;->bmc:I

    return v0
.end method

.method public getTargetIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/notify/d;->bmd:Landroid/content/Intent;

    return-object v0
.end method
