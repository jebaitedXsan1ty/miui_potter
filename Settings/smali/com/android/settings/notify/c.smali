.class public Lcom/android/settings/notify/c;
.super Ljava/lang/Object;
.source "SettingsNotifyBuilder.java"


# static fields
.field private static final bma:Ljava/util/List;

.field private static bmb:Lcom/android/settings/notify/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/settings/notify/c;->bmb:Lcom/android/settings/notify/c;

    new-instance v0, Lcom/android/settings/notify/SettingsNotifyBuilder$1;

    invoke-direct {v0}, Lcom/android/settings/notify/SettingsNotifyBuilder$1;-><init>()V

    sput-object v0, Lcom/android/settings/notify/c;->bma:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private aZx(Landroid/content/Context;I)Lcom/android/settings/notify/d;
    .locals 4

    const/4 v0, 0x0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {p1}, Lcom/android/settings/notify/a;->aZn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/android/settings/notify/d;

    invoke-direct {v0, p0}, Lcom/android/settings/notify/d;-><init>(Lcom/android/settings/notify/c;)V

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.xiaomi.account.action.VIEW_BIND_PHONE_INFO"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "com.xiaomi.account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "stat_key_source"

    const-string/jumbo v3, "com.android.settings:main"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/android/settings/notify/d;->aZC(Landroid/content/Intent;)V

    const v1, 0x7f12103b

    invoke-virtual {v0, v1}, Lcom/android/settings/notify/d;->aZB(I)V

    invoke-virtual {v0, p2}, Lcom/android/settings/notify/d;->aZA(I)V

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/android/settings/notify/a;->aZq(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/android/settings/notify/d;

    invoke-direct {v0, p0}, Lcom/android/settings/notify/d;-><init>(Lcom/android/settings/notify/c;)V

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.xiaomi.xmsf.action.MICLOUD_ENTRANCE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "com.miui.cloudservice"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/android/settings/notify/d;->aZC(Landroid/content/Intent;)V

    const v1, 0x7f12103a

    invoke-virtual {v0, v1}, Lcom/android/settings/notify/d;->aZB(I)V

    invoke-virtual {v0, p2}, Lcom/android/settings/notify/d;->aZA(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getInstance()Lcom/android/settings/notify/c;
    .locals 1

    sget-object v0, Lcom/android/settings/notify/c;->bmb:Lcom/android/settings/notify/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/notify/c;

    invoke-direct {v0}, Lcom/android/settings/notify/c;-><init>()V

    sput-object v0, Lcom/android/settings/notify/c;->bmb:Lcom/android/settings/notify/c;

    :cond_0
    sget-object v0, Lcom/android/settings/notify/c;->bmb:Lcom/android/settings/notify/c;

    return-object v0
.end method


# virtual methods
.method public aZw(Landroid/content/Context;)Lcom/android/settings/notify/d;
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/android/settings/notify/c;->bma:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/notify/c;->aZx(Landroid/content/Context;I)Lcom/android/settings/notify/d;

    move-result-object v0

    if-eqz v0, :cond_0

    :cond_1
    return-object v0
.end method
