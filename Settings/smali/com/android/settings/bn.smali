.class public Lcom/android/settings/bn;
.super Ljava/lang/Object;
.source "KeyguardSettingsCompatHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bFA(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedPasswordMinimumNumeric(I)I

    move-result v0

    return v0
.end method

.method public static bFB(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedPasswordMinimumSymbols(I)I

    move-result v0

    return v0
.end method

.method public static bFC(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedPasswordMinimumNonLetter(I)I

    move-result v0

    return v0
.end method

.method public static bFD(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/LockPatternUtils;->checkPasswordHistory(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static bFE(Lcom/android/settings/bM;)J
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/bM;->bNi()J

    move-result-wide v0

    return-wide v0
.end method

.method public static bFF(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;IIZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method public static bFG(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getActivePasswordQuality(I)I

    move-result v0

    return v0
.end method

.method public static bFH(Lcom/android/internal/widget/LockPatternUtils;Z)V
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/LockPatternUtils;->setLockScreenDisabled(ZI)V

    return-void
.end method

.method public static bFI(Lcom/android/internal/widget/LockPatternUtils;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v0

    return v0
.end method

.method public static bFJ(Lcom/android/settings/KeyguardSettingsPreferenceFragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z
    .locals 6

    move-object v0, p0

    move-object v1, p0

    move-object v2, p1

    move v3, p4

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public static bFK(Ljava/util/List;Lcom/android/settings/bM;Lcom/android/settings/bb;)V
    .locals 2

    const-string/jumbo v0, "0"

    invoke-static {p1, p0, p2}, Lcom/android/settings/bn;->bFY(Lcom/android/settings/bM;Ljava/util/List;Lcom/android/settings/bb;)Lcom/android/settings/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/bM;->bNk(Ljava/lang/String;Lcom/android/settings/f;)V

    return-void
.end method

.method public static bFL(Landroid/content/Context;Landroid/os/Bundle;)I
    .locals 2

    invoke-static {p0, p1}, Lcom/android/settings/aq;->bqz(Landroid/content/Context;Landroid/os/Bundle;)I

    move-result v0

    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/UserManager;->getCredentialOwnerProfile(I)I

    move-result v0

    return v0
.end method

.method public static bFM(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v0

    return v0
.end method

.method public static bFN(Lcom/android/internal/widget/LockPatternUtils;I)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getLockoutAttemptDeadline(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static bFO(Landroid/security/MiuiLockPatternUtils;II)J
    .locals 2

    invoke-virtual {p0, p1, p2}, Landroid/security/MiuiLockPatternUtils;->setLockoutAttemptDeadline(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public static bFP(Lcom/android/internal/widget/LockPatternUtils;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->isVisiblePatternEnabled(I)Z

    move-result v0

    return v0
.end method

.method public static bFQ(Lcom/android/internal/widget/LockPatternUtils;)I
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/internal/widget/LockPatternUtils;->getActivePasswordQuality(I)I

    move-result v0

    return v0
.end method

.method public static bFR(Lcom/android/internal/widget/LockPatternUtils;)Z
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/internal/widget/LockPatternUtils;->isVisiblePatternEnabled(I)Z

    move-result v0

    return v0
.end method

.method public static bFS(Lcom/android/internal/widget/LockPatternUtils;Z)V
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/LockPatternUtils;->setVisiblePatternEnabled(ZI)V

    return-void
.end method

.method public static bFT(Landroid/security/KeyStore;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "default_password"

    invoke-virtual {p0, v0}, Landroid/security/KeyStore;->onUserPasswordChanged(Ljava/lang/String;)Z

    return-void
.end method

.method public static bFU(Lcom/android/internal/widget/LockPatternUtils;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->isOwnerInfoEnabled(I)Z

    move-result v0

    return v0
.end method

.method public static bFV(Lcom/android/internal/widget/LockPatternUtils;ZI)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/internal/widget/LockPatternUtils;->setOwnerInfoEnabled(ZI)V

    return-void
.end method

.method public static bFW(Lcom/android/internal/widget/LockPatternUtils;)Z
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/internal/widget/LockPatternUtils;->getPowerButtonInstantlyLocks(I)Z

    move-result v0

    return v0
.end method

.method public static bFX(Lcom/android/internal/widget/LockPatternUtils;Z)V
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/LockPatternUtils;->setPowerButtonInstantlyLocks(ZI)V

    return-void
.end method

.method private static bFY(Lcom/android/settings/bM;Ljava/util/List;Lcom/android/settings/bb;)Lcom/android/settings/f;
    .locals 1

    new-instance v0, Lcom/android/settings/hZ;

    invoke-direct {v0, p2}, Lcom/android/settings/hZ;-><init>(Lcom/android/settings/bb;)V

    return-object v0
.end method

.method public static bFf(Lcom/android/settings/LockPatternView;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/LockPatternView;->setBitmapBtnTouched(I)V

    return-void
.end method

.method public static bFg(Landroid/app/Fragment;)V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/dc;->bYm(Landroid/app/Fragment;)V

    return-void
.end method

.method public static bFh(Landroid/app/Fragment;)V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/dc;->bYn(Landroid/app/Fragment;)V

    return-void
.end method

.method public static bFi(Lcom/android/internal/widget/LockPatternUtils;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->isPatternEverChosen(I)Z

    move-result v0

    return v0
.end method

.method public static bFj(Lcom/android/internal/widget/LockPatternUtils;IZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->clearLock(Ljava/lang/String;I)V

    return-void
.end method

.method public static bFk(Lcom/android/internal/widget/LockPatternUtils;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->setCredentialRequiredToDecrypt(Z)V

    return-void
.end method

.method public static bFl(Lcom/android/internal/widget/LockPatternUtils;ZI)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/internal/widget/LockPatternUtils;->setVisiblePatternEnabled(ZI)V

    return-void
.end method

.method public static bFm(Lcom/android/settings/bM;)J
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/bM;->bNi()J

    move-result-wide v0

    return-wide v0
.end method

.method public static bFn(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;IZ)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPattern(Ljava/util/List;I)V

    return-void
.end method

.method public static bFo(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;JI)[B
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/internal/widget/LockPatternUtils;->verifyPattern(Ljava/util/List;JI)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "verifyPattern"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public static bFp(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;I)Z
    .locals 3

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/widget/LockPatternUtils;->checkPattern(Ljava/util/List;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "checkPattern"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    return v0
.end method

.method public static bFq(Lcom/android/internal/widget/LockPatternUtils;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled(I)Z

    move-result v0

    return v0
.end method

.method public static bFr(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;JI)[B
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/internal/widget/LockPatternUtils;->verifyPassword(Ljava/lang/String;JI)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "VerifyPassword"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public static bFs(Lcom/android/internal/widget/LockPatternUtils;ILjava/lang/String;)Z
    .locals 3

    :try_start_0
    invoke-virtual {p0, p2, p1}, Lcom/android/internal/widget/LockPatternUtils;->checkPassword(Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "checkPassword"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    return v0
.end method

.method public static bFt(Lcom/android/internal/widget/LockPatternUtils;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled(I)Z

    move-result v0

    return v0
.end method

.method public static bFu(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;I)Z
    .locals 3

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/widget/LockPatternUtils;->checkPassword(Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "checkPassword"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    return v0
.end method

.method public static bFv(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedPasswordQuality(I)I

    move-result v0

    return v0
.end method

.method public static bFw(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedMinimumPasswordLength(I)I

    move-result v0

    return v0
.end method

.method public static bFx(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedPasswordMinimumLetters(I)I

    move-result v0

    return v0
.end method

.method public static bFy(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedPasswordMinimumUpperCase(I)I

    move-result v0

    return v0
.end method

.method public static bFz(Lcom/android/internal/widget/LockPatternUtils;I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/LockPatternUtils;->getRequestedPasswordMinimumLowerCase(I)I

    move-result v0

    return v0
.end method
