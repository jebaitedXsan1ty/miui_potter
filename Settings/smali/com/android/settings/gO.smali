.class final Lcom/android/settings/gO;
.super Ljava/lang/Object;
.source "MiuiDropDownPreference.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic cmo:Lcom/android/settings/MiuiDropDownPreference;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiDropDownPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/gO;->cmo:Lcom/android/settings/MiuiDropDownPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    if-ltz p3, :cond_0

    iget-object v0, p0, Lcom/android/settings/gO;->cmo:Lcom/android/settings/MiuiDropDownPreference;

    invoke-virtual {v0}, Lcom/android/settings/MiuiDropDownPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, p3

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/gO;->cmo:Lcom/android/settings/MiuiDropDownPreference;

    invoke-virtual {v1}, Lcom/android/settings/MiuiDropDownPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/gO;->cmo:Lcom/android/settings/MiuiDropDownPreference;

    invoke-static {v1, v0}, Lcom/android/settings/MiuiDropDownPreference;->bzs(Lcom/android/settings/MiuiDropDownPreference;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/gO;->cmo:Lcom/android/settings/MiuiDropDownPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/MiuiDropDownPreference;->setValue(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
