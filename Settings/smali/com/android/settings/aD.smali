.class public Lcom/android/settings/aD;
.super Lcom/android/settings/ab;
.source "MiuiSearchDrawable.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const v0, 0x7f080070

    invoke-direct {p0, p1, v0}, Lcom/android/settings/aD;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/ab;-><init>(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method protected amk(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Animatable;
    .locals 3

    check-cast p1, Landroid/graphics/drawable/StateListDrawable;

    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x101009e

    const/4 v2, 0x0

    aput v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawableIndex([I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/StateListDrawable;->getStateDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimatedRotateDrawable;

    return-object v0
.end method

.method public bmS()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/ab;->bmS()V

    invoke-virtual {p0}, Lcom/android/settings/aD;->bmP()Landroid/graphics/drawable/Animatable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimatedRotateDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedRotateDrawable;->stop()V

    return-void
.end method

.method public buX()Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/aD;->bmQ()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
