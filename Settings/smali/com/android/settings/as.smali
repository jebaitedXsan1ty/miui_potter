.class public Lcom/android/settings/as;
.super Lcom/android/settings/msim/a;
.source "DeviceUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/msim/a;-><init>()V

    return-void
.end method


# virtual methods
.method public brY(Landroid/content/Context;Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public brZ(Landroid/content/Context;)Ljava/util/Set;
    .locals 1

    invoke-static {p1}, Landroid/provider/MiuiSettings$System;->getHotSpotMacBlackSet(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bsa(Landroid/content/Context;I)I
    .locals 5

    invoke-static {p1}, Landroid/provider/MiuiSettings$System;->getHotSpotMaxStationNum(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f030098

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v1, :cond_0

    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/android/settings/as;->bsj(Landroid/content/Context;I)Z

    return p2
.end method

.method public bsb(I)Lcom/android/internal/telephony/Phone;
    .locals 1

    invoke-static {p1}, Lcom/android/internal/telephony/PhoneFactory;->getPhone(I)Lcom/android/internal/telephony/Phone;

    move-result-object v0

    return-object v0
.end method

.method public bsc(Landroid/content/Context;)Ljava/util/List;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectedStations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiDevice;

    new-instance v3, Lcom/android/settings/wifi/D;

    iget-object v4, v0, Landroid/net/wifi/WifiDevice;->deviceName:Ljava/lang/String;

    iget-object v0, v0, Landroid/net/wifi/WifiDevice;->deviceAddress:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Lcom/android/settings/wifi/D;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public bsd(Landroid/content/Context;)I
    .locals 1

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectedStations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bse()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "codeaurora.net.conn.TETHER_CONNECT_STATE_CHANGED"

    return-object v0
.end method

.method public bsf(Landroid/content/Context;)Z
    .locals 1

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiStaSapConcurrency()Z

    move-result v0

    return v0
.end method

.method public bsg(ILandroid/telephony/ServiceState;)Z
    .locals 4

    const/4 v0, 0x0

    const/16 v1, 0xd

    if-ne p1, v1, :cond_1

    const-string/jumbo v1, "isUsingCarrierAggregation"

    const-class v2, Ljava/lang/Boolean;

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {p2, v1, v2, v3}, Lmiui/util/ReflectionUtils;->tryCallMethod(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Object;)Lmiui/util/ObjectReference;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmiui/util/ObjectReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :cond_0
    return v0

    :cond_1
    return v0
.end method

.method public bsh(Landroid/content/Context;Landroid/net/wifi/WifiConfiguration;I)V
    .locals 2

    iget-object v0, p2, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    add-int/lit8 v1, p3, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setSimNum(I)V

    return-void
.end method

.method public bsi(Landroid/content/Context;Ljava/util/Set;)V
    .locals 0

    invoke-static {p1, p2}, Landroid/provider/MiuiSettings$System;->setHotSpotMacBlackSet(Landroid/content/Context;Ljava/util/Set;)V

    return-void
.end method

.method public bsj(Landroid/content/Context;I)Z
    .locals 1

    invoke-static {p1, p2}, Landroid/provider/MiuiSettings$System;->setHotSpotMaxStationNum(Landroid/content/Context;I)Z

    move-result v0

    return v0
.end method

.method public bsk(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    iput v0, p1, Landroid/net/wifi/WifiConfiguration;->wapiCertSelMode:I

    iput-object p2, p1, Landroid/net/wifi/WifiConfiguration;->wapiCertSel:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p1, Landroid/net/wifi/WifiConfiguration;->wapiCertSelMode:I

    iput-object v1, p1, Landroid/net/wifi/WifiConfiguration;->wapiCertSel:Ljava/lang/String;

    goto :goto_0
.end method

.method public bsl(Landroid/net/wifi/WifiConfiguration;ILjava/lang/String;)V
    .locals 2

    const/16 v1, 0x22

    iput p2, p1, Landroid/net/wifi/WifiConfiguration;->wapiPskType:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/net/wifi/WifiConfiguration;->wapiPsk:Ljava/lang/String;

    return-void
.end method
