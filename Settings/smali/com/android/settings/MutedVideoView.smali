.class public Lcom/android/settings/MutedVideoView;
.super Landroid/view/SurfaceView;
.source "MutedVideoView.java"

# interfaces
.implements Landroid/widget/MediaController$MediaPlayerControl;


# instance fields
.field private TAG:Ljava/lang/String;

.field private bJF:I

.field private bJG:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private bJH:Z

.field private bJI:Z

.field private bJJ:Z

.field private bJK:Landroid/media/MediaPlayer$OnCompletionListener;

.field private bJL:I

.field private bJM:I

.field private bJN:Landroid/media/MediaPlayer$OnErrorListener;

.field private bJO:Ljava/util/Map;

.field private bJP:Landroid/media/MediaPlayer$OnInfoListener;

.field private bJQ:Landroid/widget/MediaController;

.field private bJR:Landroid/media/MediaPlayer;

.field private bJS:Landroid/media/MediaPlayer$OnCompletionListener;

.field private bJT:Landroid/media/MediaPlayer$OnErrorListener;

.field private bJU:Landroid/media/MediaPlayer$OnInfoListener;

.field private bJV:Landroid/media/MediaPlayer$OnPreparedListener;

.field private bJW:Ljava/util/Vector;

.field bJX:Landroid/media/MediaPlayer$OnPreparedListener;

.field bJY:Landroid/view/SurfaceHolder$Callback;

.field private bJZ:I

.field bKa:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private bKb:I

.field private bKc:Landroid/view/SurfaceHolder;

.field private bKd:I

.field private bKe:I

.field private bKf:Landroid/net/Uri;

.field private bKg:I

.field private bKh:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const-string/jumbo v0, "MutedVideoView"

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->TAG:Ljava/lang/String;

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    iput-object v2, p0, Lcom/android/settings/MutedVideoView;->bKc:Landroid/view/SurfaceHolder;

    iput-object v2, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    new-instance v0, Lcom/android/settings/hd;

    invoke-direct {v0, p0}, Lcom/android/settings/hd;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bKa:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lcom/android/settings/he;

    invoke-direct {v0, p0}, Lcom/android/settings/he;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJX:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/android/settings/hf;

    invoke-direct {v0, p0}, Lcom/android/settings/hf;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJK:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/android/settings/hg;

    invoke-direct {v0, p0}, Lcom/android/settings/hg;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJP:Landroid/media/MediaPlayer$OnInfoListener;

    new-instance v0, Lcom/android/settings/hh;

    invoke-direct {v0, p0}, Lcom/android/settings/hh;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJN:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/android/settings/hi;

    invoke-direct {v0, p0}, Lcom/android/settings/hi;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJG:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lcom/android/settings/hj;

    invoke-direct {v0, p0}, Lcom/android/settings/hj;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJY:Landroid/view/SurfaceHolder$Callback;

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAq()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string/jumbo v0, "MutedVideoView"

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->TAG:Ljava/lang/String;

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    iput-object v2, p0, Lcom/android/settings/MutedVideoView;->bKc:Landroid/view/SurfaceHolder;

    iput-object v2, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    new-instance v0, Lcom/android/settings/hd;

    invoke-direct {v0, p0}, Lcom/android/settings/hd;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bKa:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lcom/android/settings/he;

    invoke-direct {v0, p0}, Lcom/android/settings/he;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJX:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/android/settings/hf;

    invoke-direct {v0, p0}, Lcom/android/settings/hf;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJK:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/android/settings/hg;

    invoke-direct {v0, p0}, Lcom/android/settings/hg;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJP:Landroid/media/MediaPlayer$OnInfoListener;

    new-instance v0, Lcom/android/settings/hh;

    invoke-direct {v0, p0}, Lcom/android/settings/hh;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJN:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/android/settings/hi;

    invoke-direct {v0, p0}, Lcom/android/settings/hi;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJG:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lcom/android/settings/hj;

    invoke-direct {v0, p0}, Lcom/android/settings/hj;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJY:Landroid/view/SurfaceHolder$Callback;

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAq()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string/jumbo v0, "MutedVideoView"

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->TAG:Ljava/lang/String;

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    iput-object v2, p0, Lcom/android/settings/MutedVideoView;->bKc:Landroid/view/SurfaceHolder;

    iput-object v2, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    new-instance v0, Lcom/android/settings/hd;

    invoke-direct {v0, p0}, Lcom/android/settings/hd;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bKa:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lcom/android/settings/he;

    invoke-direct {v0, p0}, Lcom/android/settings/he;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJX:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/android/settings/hf;

    invoke-direct {v0, p0}, Lcom/android/settings/hf;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJK:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/android/settings/hg;

    invoke-direct {v0, p0}, Lcom/android/settings/hg;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJP:Landroid/media/MediaPlayer$OnInfoListener;

    new-instance v0, Lcom/android/settings/hh;

    invoke-direct {v0, p0}, Lcom/android/settings/hh;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJN:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/android/settings/hi;

    invoke-direct {v0, p0}, Lcom/android/settings/hi;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJG:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lcom/android/settings/hj;

    invoke-direct {v0, p0}, Lcom/android/settings/hj;-><init>(Lcom/android/settings/MutedVideoView;)V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJY:Landroid/view/SurfaceHolder$Callback;

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAq()V

    return-void
.end method

.method static synthetic bAA(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer$OnInfoListener;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJU:Landroid/media/MediaPlayer$OnInfoListener;

    return-object v0
.end method

.method static synthetic bAB(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJV:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic bAC(Lcom/android/settings/MutedVideoView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bJZ:I

    return v0
.end method

.method static synthetic bAD(Lcom/android/settings/MutedVideoView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bKb:I

    return v0
.end method

.method static synthetic bAE(Lcom/android/settings/MutedVideoView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bKd:I

    return v0
.end method

.method static synthetic bAF(Lcom/android/settings/MutedVideoView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    return v0
.end method

.method static synthetic bAG(Lcom/android/settings/MutedVideoView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    return v0
.end method

.method static synthetic bAH(Lcom/android/settings/MutedVideoView;)I
    .locals 1

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    return v0
.end method

.method static synthetic bAI(Lcom/android/settings/MutedVideoView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MutedVideoView;->bJH:Z

    return p1
.end method

.method static synthetic bAJ(Lcom/android/settings/MutedVideoView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MutedVideoView;->bJI:Z

    return p1
.end method

.method static synthetic bAK(Lcom/android/settings/MutedVideoView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MutedVideoView;->bJJ:Z

    return p1
.end method

.method static synthetic bAL(Lcom/android/settings/MutedVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MutedVideoView;->bJL:I

    return p1
.end method

.method static synthetic bAM(Lcom/android/settings/MutedVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    return p1
.end method

.method static synthetic bAN(Lcom/android/settings/MutedVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MutedVideoView;->bKb:I

    return p1
.end method

.method static synthetic bAO(Lcom/android/settings/MutedVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MutedVideoView;->bKc:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic bAP(Lcom/android/settings/MutedVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MutedVideoView;->bKd:I

    return p1
.end method

.method static synthetic bAQ(Lcom/android/settings/MutedVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    return p1
.end method

.method static synthetic bAR(Lcom/android/settings/MutedVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    return p1
.end method

.method static synthetic bAS(Lcom/android/settings/MutedVideoView;I)I
    .locals 0

    iput p1, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    return p1
.end method

.method static synthetic bAT(Lcom/android/settings/MutedVideoView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAs()V

    return-void
.end method

.method static synthetic bAU(Lcom/android/settings/MutedVideoView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MutedVideoView;->bAt(Z)V

    return-void
.end method

.method private bAp()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v0, p0}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v1, v0}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method private bAq()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    iput v2, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJY:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-virtual {p0, v3}, Lcom/android/settings/MutedVideoView;->setFocusable(Z)V

    invoke-virtual {p0, v3}, Lcom/android/settings/MutedVideoView;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->requestFocus()Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJW:Ljava/util/Vector;

    iput v2, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    iput v2, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    return-void
.end method

.method private bAr()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    if-eq v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private bAs()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bKf:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bKc:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, v1}, Lcom/android/settings/MutedVideoView;->bAt(Z)V

    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->getContext()Landroid/content/Context;

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bJF:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/android/settings/MutedVideoView;->bJF:I

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioSessionId(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJX:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bKa:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJK:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJN:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJP:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJG:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/MutedVideoView;->bJL:I

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MutedVideoView;->bKf:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/settings/MutedVideoView;->bJO:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bKc:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAp()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJW:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/MutedVideoView;->bJF:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/MutedVideoView;->bKf:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJN:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJW:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void

    :catch_1
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/MutedVideoView;->bKf:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJN:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJW:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/settings/MutedVideoView;->bJW:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    throw v0
.end method

.method private bAt(Z)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v2, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJW:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    if-eqz p1, :cond_0

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    :cond_0
    return-void
.end method

.method private bAu()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    goto :goto_0
.end method

.method static synthetic bAv(Lcom/android/settings/MutedVideoView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    return-object v0
.end method

.method static synthetic bAx(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic bAy(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJS:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic bAz(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJT:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method


# virtual methods
.method public canPause()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MutedVideoView;->bJH:Z

    return v0
.end method

.method public canSeekBackward()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MutedVideoView;->bJI:Z

    return v0
.end method

.method public canSeekForward()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/MutedVideoView;->bJJ:Z

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/SurfaceView;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public getAudioSessionId()I
    .locals 2

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bJF:I

    if-nez v0, :cond_0

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v1

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bJF:I

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    :cond_0
    iget v0, p0, Lcom/android/settings/MutedVideoView;->bJF:I

    return v0
.end method

.method public getBufferPercentage()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bJL:I

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getDuration()I
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v0, Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x4

    if-eq p1, v2, :cond_0

    const/16 v2, 0x18

    if-eq p1, v2, :cond_0

    const/16 v2, 0x19

    if-eq p1, v2, :cond_0

    const/16 v2, 0xa4

    if-eq p1, v2, :cond_0

    const/16 v2, 0x52

    if-eq p1, v2, :cond_0

    const/4 v2, 0x5

    if-eq p1, v2, :cond_0

    const/4 v2, 0x6

    if-eq p1, v2, :cond_0

    move v0, v1

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v2

    if-eqz v2, :cond_9

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    if-eqz v0, :cond_9

    const/16 v0, 0x4f

    if-eq p1, v0, :cond_1

    const/16 v0, 0x55

    if-ne p1, v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->pause()V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    :goto_0
    return v1

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->start()V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    goto :goto_0

    :cond_3
    const/16 v0, 0x7e

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->start()V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_4
    return v1

    :cond_5
    const/16 v0, 0x56

    if-eq p1, v0, :cond_6

    const/16 v0, 0x7f

    if-ne p1, v0, :cond_8

    :cond_6
    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->pause()V

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    :cond_7
    return v1

    :cond_8
    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAu()V

    :cond_9
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    invoke-super/range {p0 .. p5}, Landroid/view/SurfaceView;->onLayout(ZIIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v6, -0x80000000

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    invoke-static {v0, p1}, Lcom/android/settings/MutedVideoView;->getDefaultSize(II)I

    move-result v1

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    invoke-static {v0, p2}, Lcom/android/settings/MutedVideoView;->getDefaultSize(II)I

    move-result v0

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    if-lez v2, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    if-ne v4, v3, :cond_2

    if-ne v5, v3, :cond_2

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    mul-int/2addr v3, v1

    if-ge v2, v3, :cond_1

    iget v1, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    div-int/2addr v1, v2

    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/android/settings/MutedVideoView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    mul-int/2addr v3, v1

    if-le v2, v3, :cond_0

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    mul-int/2addr v0, v1

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    div-int/2addr v0, v2

    goto :goto_0

    :cond_2
    if-ne v4, v3, :cond_4

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    mul-int/2addr v2, v1

    iget v3, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    div-int/2addr v2, v3

    if-ne v5, v6, :cond_3

    if-gt v2, v0, :cond_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    if-ne v5, v3, :cond_6

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    div-int/2addr v2, v3

    if-ne v4, v6, :cond_5

    if-gt v2, v1, :cond_0

    :cond_5
    move v1, v2

    goto :goto_0

    :cond_6
    iget v3, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    if-ne v5, v6, :cond_7

    if-le v2, v0, :cond_7

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    div-int/2addr v2, v3

    :goto_1
    if-ne v4, v6, :cond_8

    if-le v2, v1, :cond_8

    iget v0, p0, Lcom/android/settings/MutedVideoView;->bKg:I

    mul-int/2addr v0, v1

    iget v2, p0, Lcom/android/settings/MutedVideoView;->bKh:I

    div-int/2addr v0, v2

    goto :goto_0

    :cond_7
    move v0, v2

    move v2, v3

    goto :goto_1

    :cond_8
    move v1, v2

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAu()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAu()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 2

    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    :cond_0
    iput v1, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    return-void
.end method

.method public seekTo(I)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/MutedVideoView;->bJZ:I

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/android/settings/MutedVideoView;->bJZ:I

    goto :goto_0
.end method

.method public setMediaController(Landroid/widget/MediaController;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iput-object p1, p0, Lcom/android/settings/MutedVideoView;->bJQ:Landroid/widget/MediaController;

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAp()V

    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MutedVideoView;->bJS:Landroid/media/MediaPlayer$OnCompletionListener;

    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MutedVideoView;->bJT:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MutedVideoView;->bJU:Landroid/media/MediaPlayer$OnInfoListener;

    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MutedVideoView;->bJV:Landroid/media/MediaPlayer$OnPreparedListener;

    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/MutedVideoView;->setVideoURI(Landroid/net/Uri;)V

    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/MutedVideoView;->setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/MutedVideoView;->bKf:Landroid/net/Uri;

    iput-object p2, p0, Lcom/android/settings/MutedVideoView;->bJO:Ljava/util/Map;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/MutedVideoView;->bJZ:I

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAs()V

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->requestLayout()V

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->invalidate()V

    return-void
.end method

.method public start()V
    .locals 2

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/android/settings/MutedVideoView;->bAr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MutedVideoView;->bJR:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iput v1, p0, Lcom/android/settings/MutedVideoView;->bJM:I

    :cond_0
    iput v1, p0, Lcom/android/settings/MutedVideoView;->bKe:I

    return-void
.end method
