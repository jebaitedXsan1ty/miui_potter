.class Lcom/android/settings/toggleposition/d;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "FixedDividerSortableListView.java"


# instance fields
.field final synthetic blj:Lcom/android/settings/toggleposition/FixedDividerSortableListView;


# direct methods
.method constructor <init>(Lcom/android/settings/toggleposition/FixedDividerSortableListView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/toggleposition/d;->blj:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/d;->blj:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    invoke-static {v0, p1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYM(Lcom/android/settings/toggleposition/FixedDividerSortableListView;Landroid/view/MotionEvent;)Z

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/toggleposition/d;->blj:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    invoke-static {v0, p1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYN(Lcom/android/settings/toggleposition/FixedDividerSortableListView;Landroid/view/MotionEvent;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/toggleposition/d;->blj:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    iget-object v2, p0, Lcom/android/settings/toggleposition/d;->blj:Lcom/android/settings/toggleposition/FixedDividerSortableListView;

    invoke-virtual {v2}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
