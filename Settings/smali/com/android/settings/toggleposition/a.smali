.class public Lcom/android/settings/toggleposition/a;
.super Landroid/widget/BaseAdapter;
.source "DragAdapter.java"

# interfaces
.implements Lcom/android/settings/toggleposition/b;


# instance fields
.field private bkG:I

.field private bkH:Landroid/view/LayoutInflater;

.field bkI:Landroid/view/View$OnClickListener;

.field private bkJ:Ljava/util/List;

.field private bkK:Lcom/android/settings/ap;

.field private bkL:Lmiui/app/ToggleManager$OnToggleChangedListener;

.field private bkM:Lmiui/app/ToggleManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/toggleposition/a;->bkG:I

    new-instance v0, Lcom/android/settings/toggleposition/i;

    invoke-direct {v0, p0}, Lcom/android/settings/toggleposition/i;-><init>(Lcom/android/settings/toggleposition/a;)V

    iput-object v0, p0, Lcom/android/settings/toggleposition/a;->bkI:Landroid/view/View$OnClickListener;

    iput-object p2, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-static {p1}, Lmiui/app/ToggleManager;->createInstance(Landroid/content/Context;)Lmiui/app/ToggleManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/toggleposition/a;->bkM:Lmiui/app/ToggleManager;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/toggleposition/a;->bkH:Landroid/view/LayoutInflater;

    new-instance v0, Lcom/android/settings/ap;

    invoke-direct {v0, p1}, Lcom/android/settings/ap;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/toggleposition/a;->bkK:Lcom/android/settings/ap;

    new-instance v0, Lcom/android/settings/toggleposition/j;

    invoke-direct {v0, p0}, Lcom/android/settings/toggleposition/j;-><init>(Lcom/android/settings/toggleposition/a;)V

    iput-object v0, p0, Lcom/android/settings/toggleposition/a;->bkL:Lmiui/app/ToggleManager$OnToggleChangedListener;

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkM:Lmiui/app/ToggleManager;

    iget-object v1, p0, Lcom/android/settings/toggleposition/a;->bkL:Lmiui/app/ToggleManager$OnToggleChangedListener;

    invoke-virtual {v0, v1}, Lmiui/app/ToggleManager;->setOnToggleChangedListener(Lmiui/app/ToggleManager$OnToggleChangedListener;)V

    return-void
.end method

.method private aYD(I)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic aYH(Lcom/android/settings/toggleposition/a;)Lmiui/app/ToggleManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkM:Lmiui/app/ToggleManager;

    return-object v0
.end method


# virtual methods
.method public aYE()V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string/jumbo v3, "item_id"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkM:Lmiui/app/ToggleManager;

    invoke-virtual {v0, v1}, Lmiui/app/ToggleManager;->setUserSelectedToggleOrder(Ljava/util/ArrayList;)V

    return-void
.end method

.method public aYF(II)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public aYG(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/toggleposition/a;->bkG:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/a;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v2, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkH:Landroid/view/LayoutInflater;

    const v1, 0x7f0d01db

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    const v0, 0x7f0a022f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a022a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/settings/toggleposition/a;->bkJ:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string/jumbo v3, "item_id"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, v0}, Lmiui/app/ToggleManager;->updateTextView(ILandroid/widget/TextView;)V

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkK:Lcom/android/settings/ap;

    invoke-virtual {v0, v2, v1}, Lcom/android/settings/ap;->bqn(ILandroid/widget/ImageView;)V

    iget v0, p0, Lcom/android/settings/toggleposition/a;->bkG:I

    if-ne p1, v0, :cond_1

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/settings/toggleposition/a;->aYD(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Landroid/view/View;->setEnabled(Z)V

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/toggleposition/a;->bkI:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2
.end method
