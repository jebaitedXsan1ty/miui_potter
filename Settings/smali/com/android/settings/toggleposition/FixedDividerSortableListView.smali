.class public Lcom/android/settings/toggleposition/FixedDividerSortableListView;
.super Landroid/widget/MiuiListView;
.source "FixedDividerSortableListView.java"


# instance fields
.field private bkN:I

.field private bkO:I

.field private bkP:I

.field private bkQ:I

.field private bkR:I

.field private bkS:I

.field private bkT:I

.field private bkU:Z

.field private bkV:I

.field private bkW:I

.field private bkX:Landroid/view/View$OnTouchListener;

.field private bkY:Lcom/android/settings/toggleposition/c;

.field private bkZ:Landroid/view/View$OnTouchListener;

.field private bla:I

.field private blb:I

.field private blc:I

.field private bld:Landroid/graphics/drawable/BitmapDrawable;

.field private ble:Landroid/graphics/drawable/Drawable;

.field private blf:Landroid/graphics/drawable/Drawable;

.field private blg:I

.field private blh:I

.field private bli:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1010074

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/MiuiListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkV:I

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bli:[I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080402

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blf:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blf:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0x99

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blf:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blh:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blg:I

    new-instance v0, Lcom/android/settings/toggleposition/k;

    invoke-direct {v0, p0}, Lcom/android/settings/toggleposition/k;-><init>(Lcom/android/settings/toggleposition/FixedDividerSortableListView;)V

    iput-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkZ:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/android/settings/toggleposition/l;

    invoke-direct {v0, p0}, Lcom/android/settings/toggleposition/l;-><init>(Lcom/android/settings/toggleposition/FixedDividerSortableListView;)V

    iput-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkX:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method private aYI(IIII)Landroid/view/animation/Animation;
    .locals 5

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    return-object v0
.end method

.method private aYJ(Landroid/view/MotionEvent;)I
    .locals 4

    iget-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bli:[I

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getLocationOnScreen([I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bli:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    int-to-float v1, v1

    sub-float v2, v0, v1

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v1

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getLastVisiblePosition()I

    move-result v3

    if-gt v0, v3, :cond_1

    sub-int v3, v0, v1

    invoke-virtual {p0, v3}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method private aYK(Landroid/view/MotionEvent;)Z
    .locals 8

    const/16 v5, 0x99

    const/4 v3, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkY:Lcom/android/settings/toggleposition/c;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYJ(Landroid/view/MotionEvent;)I

    move-result v0

    if-ltz v0, :cond_1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iput-boolean v3, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkU:Z

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkR:I

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bli:[I

    invoke-virtual {p0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getLocationOnScreen([I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bli:[I

    aget v2, v2, v3

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkT:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkT:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkW:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkR:I

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bld:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bld:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bld:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    iget v4, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    invoke-virtual {v1, v2, v7, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->ble:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->ble:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->ble:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    iget v4, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    invoke-virtual {v1, v2, v7, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blf:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v3, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blh:I

    neg-int v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v4

    iget v5, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    iget v6, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blg:I

    add-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    const-string/jumbo v1, "FixedDividerSortableListView"

    const-string/jumbo v2, "onTouch() view.startAnimation()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkR:I

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkR:I

    invoke-direct {p0, v1, v2, v7, v7}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkU:Z

    return v0
.end method

.method private aYL(I)V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    if-eq p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() sort item from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";mDraggingTo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "To "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    if-ge v0, v1, :cond_4

    const-string/jumbo v0, "FixedDividerSortableListView"

    const-string/jumbo v1, "updateDraggingToPisition() for case: ...ddd---..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    if-le v0, p1, :cond_4

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    if-le v0, v1, :cond_4

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-gt p1, v0, :cond_2

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() at divider mDraggingTo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ge v0, v1, :cond_3

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ge p1, v0, :cond_3

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ne v0, v1, :cond_3

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() across divider item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " set move down reverse animation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkN:I

    add-int/2addr v1, v2

    neg-int v1, v1

    invoke-direct {p0, v3, v3, v1, v3}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimationByPisition(ILandroid/view/animation/Animation;)V

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    goto/16 :goto_0

    :cond_3
    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " set move down reverse animation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    neg-int v1, v1

    invoke-direct {p0, v3, v3, v1, v3}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimationByPisition(ILandroid/view/animation/Animation;)V

    goto/16 :goto_0

    :cond_4
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-le v0, v1, :cond_7

    const-string/jumbo v0, "FixedDividerSortableListView"

    const-string/jumbo v1, "updateDraggingToPisition() for case: ...---uuu..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    if-ge v0, p1, :cond_7

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    if-ge v0, v1, :cond_7

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-le v0, v1, :cond_5

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-lt p1, v0, :cond_5

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_5

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() at divider mDraggingTo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-le v0, v1, :cond_6

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-le p1, v0, :cond_6

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ne v0, v1, :cond_6

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() across divider item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " set move up reverse animation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    invoke-direct {p0, v3, v3, v1, v3}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimationByPisition(ILandroid/view/animation/Animation;)V

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " set move up reverse animation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    invoke-direct {p0, v3, v3, v1, v3}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimationByPisition(ILandroid/view/animation/Animation;)V

    goto/16 :goto_1

    :cond_7
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    if-ge v0, v1, :cond_a

    const-string/jumbo v0, "FixedDividerSortableListView"

    const-string/jumbo v1, "updateDraggingToPisition() for case: ...ddd+++..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    if-ge v0, p1, :cond_a

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ge v0, v1, :cond_8

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-lt p1, v0, :cond_8

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_8

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() at divider mDraggingTo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_8
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ge v0, v1, :cond_9

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-le p1, v0, :cond_9

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ne v0, v1, :cond_9

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkN:I

    add-int/2addr v1, v2

    neg-int v1, v1

    invoke-direct {p0, v3, v3, v3, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimationByPisition(ILandroid/view/animation/Animation;)V

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() across divider item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " set move up animation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_9
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    neg-int v1, v1

    invoke-direct {p0, v3, v3, v3, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimationByPisition(ILandroid/view/animation/Animation;)V

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " set move up animation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_a
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-le v0, v1, :cond_d

    const-string/jumbo v0, "FixedDividerSortableListView"

    const-string/jumbo v1, "updateDraggingToPisition() for case: ...+++uuu..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    if-le v0, p1, :cond_d

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-le v0, v1, :cond_b

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-gt p1, v0, :cond_b

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_b

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() at divider mDraggingTo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_b
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-le v0, v1, :cond_c

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ge p1, v0, :cond_c

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ne v0, v1, :cond_c

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkN:I

    add-int/2addr v1, v2

    invoke-direct {p0, v3, v3, v3, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimationByPisition(ILandroid/view/animation/Animation;)V

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() across divider item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " set move down animation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_c
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    invoke-direct {p0, v3, v3, v3, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimationByPisition(ILandroid/view/animation/Animation;)V

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateDraggingToPisition() item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " set move down animation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_d
    return-void
.end method

.method static synthetic aYM(Lcom/android/settings/toggleposition/FixedDividerSortableListView;Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYK(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic aYN(Lcom/android/settings/toggleposition/FixedDividerSortableListView;Landroid/view/MotionEvent;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYJ(Landroid/view/MotionEvent;)I

    move-result v0

    return v0
.end method

.method private setViewAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p1, p2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    goto :goto_0
.end method

.method private setViewAnimationByPisition(ILandroid/view/animation/Animation;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/widget/MiuiListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    if-ltz v0, :cond_5

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkT:I

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkW:I

    sub-int v1, v0, v1

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getHeaderViewsCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v2

    if-lt v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getLastVisiblePosition()I

    move-result v2

    if-le v0, v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFooterViewsCount()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v2

    if-lt v0, v2, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getLastVisiblePosition()I

    move-result v2

    if-le v0, v2, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getLastVisiblePosition()I

    move-result v0

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v1, v0

    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blf:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bld:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->ble:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkV:I

    if-ge v1, v2, :cond_4

    iget-object v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->ble:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_4
    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_5
    return-void
.end method

.method public getListenerForStartingSort()Landroid/view/View$OnTouchListener;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkZ:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method public getLongPressListenerForStartingSort()Landroid/view/View$OnTouchListener;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkX:Landroid/view/View$OnTouchListener;

    return-object v0
.end method

.method protected obtainView(I[Z)Landroid/view/View;
    .locals 5

    const/4 v3, 0x0

    const-string/jumbo v0, "FixedDividerSortableListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "obtainView() position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";mDraggingFrom="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ";mDraggingTo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2}, Landroid/widget/MiuiListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v1

    const/4 v0, 0x0

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    if-ne v2, p1, :cond_1

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkR:I

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkR:I

    invoke-direct {p0, v0, v2, v3, v3}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    const-string/jumbo v2, "FixedDividerSortableListView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "obtainView() item "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " set move out animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    return-object v1

    :cond_1
    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    if-ge v2, p1, :cond_2

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    if-gt p1, v2, :cond_2

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    neg-int v0, v0

    invoke-direct {p0, v3, v3, v3, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    const-string/jumbo v2, "FixedDividerSortableListView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "obtainView() item "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " set move up animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    if-le v2, p1, :cond_0

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    if-lt p1, v2, :cond_0

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkQ:I

    invoke-direct {p0, v3, v3, v3, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYI(IIII)Landroid/view/animation/Animation;

    move-result-object v0

    const-string/jumbo v2, "FixedDividerSortableListView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "obtainView() item "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " set move down animation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkU:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->requestDisallowInterceptTouchEvent(Z)V

    invoke-virtual {p0, p1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/MiuiListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/MiuiListView;->onSizeChanged(IIII)V

    int-to-float v0, p2

    const/high16 v1, 0x3e800000    # 0.25f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bla:I

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bla:I

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blc:I

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bla:I

    sub-int v0, p2, v0

    iput v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blb:I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, -0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkU:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/MiuiListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v2, v0

    iget-boolean v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkU:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkT:I

    if-eq v2, v0, :cond_1

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYJ(Landroid/view/MotionEvent;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getHeaderViewsCount()I

    move-result v3

    if-lt v0, v3, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getCount()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFooterViewsCount()I

    move-result v4

    sub-int/2addr v3, v4

    if-le v0, v3, :cond_4

    :cond_3
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    :cond_4
    iget v3, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    if-ne v0, v3, :cond_5

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {p0, v3}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    iput v3, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkN:I

    :cond_5
    invoke-direct {p0, v0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->aYL(I)V

    iput v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkT:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->invalidate()V

    iget v3, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blb:I

    if-le v2, v3, :cond_7

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blb:I

    sub-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x10

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bla:I

    div-int/2addr v1, v2

    :cond_6
    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v2, v0, v2

    invoke-virtual {p0, v2}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setSelectionFromTop(II)V

    goto :goto_0

    :cond_7
    iget v3, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blc:I

    if-ge v2, v3, :cond_6

    iget v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->blc:I

    sub-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x10

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bla:I

    div-int/2addr v1, v2

    goto :goto_1

    :pswitch_2
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    if-ltz v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkY:Lcom/android/settings/toggleposition/c;

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    if-eq v0, v2, :cond_9

    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    if-ltz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkY:Lcom/android/settings/toggleposition/c;

    iget v2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getHeaderViewsCount()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->getHeaderViewsCount()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-interface {v0, v2, v3}, Lcom/android/settings/toggleposition/c;->OnOrderChanged(II)V

    :cond_8
    :goto_2
    iput-boolean v1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkU:Z

    iput v5, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    iput v5, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkS:I

    invoke-virtual {p0}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->invalidate()V

    goto/16 :goto_0

    :cond_9
    iget v0, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkP:I

    invoke-direct {p0, v0, v3}, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->setViewAnimationByPisition(ILandroid/view/animation/Animation;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setDividerItemPosition(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkO:I

    return-void
.end method

.method public setItemUpperBound(ILandroid/graphics/drawable/Drawable;)V
    .locals 0

    iput p1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkV:I

    iput-object p2, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->ble:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setOnOrderChangedListener(Lcom/android/settings/toggleposition/c;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/toggleposition/FixedDividerSortableListView;->bkY:Lcom/android/settings/toggleposition/c;

    return-void
.end method
