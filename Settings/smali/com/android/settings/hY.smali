.class final Lcom/android/settings/hY;
.super Landroid/database/ContentObserver;
.source "CarrierCustomEditFragment.java"


# instance fields
.field final synthetic cnS:Lcom/android/settings/CarrierCustomEditFragment;

.field final synthetic cnT:I


# direct methods
.method constructor <init>(Lcom/android/settings/CarrierCustomEditFragment;Landroid/os/Handler;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/hY;->cnS:Lcom/android/settings/CarrierCustomEditFragment;

    iput p3, p0, Lcom/android/settings/hY;->cnT:I

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/hY;->cnS:Lcom/android/settings/CarrierCustomEditFragment;

    iget-object v0, v0, Lcom/android/settings/CarrierCustomEditFragment;->bNW:[Ljava/lang/String;

    iget v1, p0, Lcom/android/settings/hY;->cnT:I

    iget-object v2, p0, Lcom/android/settings/hY;->cnS:Lcom/android/settings/CarrierCustomEditFragment;

    invoke-virtual {v2}, Lcom/android/settings/CarrierCustomEditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "status_bar_real_carrier"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/settings/hY;->cnT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/MiuiSettings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/android/settings/hY;->cnS:Lcom/android/settings/CarrierCustomEditFragment;

    invoke-static {v0}, Lcom/android/settings/CarrierCustomEditFragment;->bFa(Lcom/android/settings/CarrierCustomEditFragment;)[Landroid/widget/EditText;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/hY;->cnT:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/android/settings/hY;->cnS:Lcom/android/settings/CarrierCustomEditFragment;

    iget-object v1, v1, Lcom/android/settings/CarrierCustomEditFragment;->bNW:[Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/hY;->cnT:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method
