.class public Lcom/android/settings/PrivacySettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "PrivacySettings.java"

# interfaces
.implements Lcom/android/settings/search/Indexable;


# static fields
.field static final AUTO_RESTORE:Ljava/lang/String; = "auto_restore"

.field static final BACKUP_DATA:Ljava/lang/String; = "backup_data"

.field static final CONFIGURE_ACCOUNT:Ljava/lang/String; = "configure_account"

.field static final DATA_MANAGEMENT:Ljava/lang/String; = "data_management"


# instance fields
.field private bDN:Lmiui/preference/ValuePreference;

.field private bDO:Landroid/preference/CheckBoxPreference;

.field private bDP:Landroid/preference/Preference;

.field private bDQ:Landroid/app/backup/IBackupManager;

.field private bDR:Landroid/preference/PreferenceScreen;

.field private bDS:Landroid/preference/PreferenceScreen;

.field private bDT:Landroid/preference/Preference;

.field private bDU:Landroid/preference/Preference;

.field private bDV:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private mEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/fM;

    invoke-direct {v0, p0}, Lcom/android/settings/fM;-><init>(Lcom/android/settings/PrivacySettings;)V

    iput-object v0, p0, Lcom/android/settings/PrivacySettings;->bDV:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-void
.end method

.method private static btW(Landroid/content/Context;Ljava/util/Collection;)V
    .locals 4

    const/4 v1, 0x0

    const-string/jumbo v0, "backup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    move-result-object v0

    :try_start_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/app/backup/IBackupManager;->isBackupServiceActive(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string/jumbo v3, "com.google.settings"

    invoke-virtual {v2, v3, v1}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_1

    const-string/jumbo v2, "backup_category"

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_1
    if-nez v1, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    const-string/jumbo v2, "backup_inactive"

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_3
    if-nez v1, :cond_4

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    :cond_4
    const-string/jumbo v0, "backup_data"

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "auto_restore"

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "configure_account"

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_5
    const-string/jumbo v0, "no_factory_reset"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/android/settingslib/w;->crn(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v0

    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "PrivacySettings"

    const-string/jumbo v2, "Failed querying backup manager service activity status. Assuming it is inactive."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method private btX(Landroid/content/Intent;)V
    .locals 8

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string/jumbo v1, "com.xiaomi"

    const-string/jumbo v2, "micloud"

    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getActivity()Landroid/app/Activity;

    move-result-object v5

    new-instance v6, Lcom/android/settings/fO;

    invoke-direct {v6, p0, p1}, Lcom/android/settings/fO;-><init>(Lcom/android/settings/PrivacySettings;Landroid/content/Intent;)V

    move-object v4, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method private btY(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDT:Landroid/preference/Preference;

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDT:Landroid/preference/Preference;

    const v1, 0x7f1201e2

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method

.method private btZ(Landroid/content/Intent;)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1209f4

    invoke-virtual {p0, v1}, Lcom/android/settings/PrivacySettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1209f3

    invoke-virtual {p0, v1}, Lcom/android/settings/PrivacySettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/fN;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/fN;-><init>(Lcom/android/settings/PrivacySettings;Landroid/content/Intent;)V

    const v2, 0x7f1209f5

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f1203c8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private bua()V
    .locals 10

    const/4 v7, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v8

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/PrivacySettings;->bDQ:Landroid/app/backup/IBackupManager;

    invoke-interface {v1}, Landroid/app/backup/IBackupManager;->isBackupEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    :try_start_1
    iget-object v1, p0, Lcom/android/settings/PrivacySettings;->bDQ:Landroid/app/backup/IBackupManager;

    invoke-interface {v1}, Landroid/app/backup/IBackupManager;->getCurrentTransport()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/android/settings/PrivacySettings;->bDQ:Landroid/app/backup/IBackupManager;

    invoke-interface {v1, v6}, Landroid/app/backup/IBackupManager;->getConfigurationIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "config"

    invoke-direct {p0, v1, v2}, Lcom/android/settings/PrivacySettings;->bub(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    :try_start_2
    iget-object v1, p0, Lcom/android/settings/PrivacySettings;->bDQ:Landroid/app/backup/IBackupManager;

    invoke-interface {v1, v6}, Landroid/app/backup/IBackupManager;->getDestinationString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v2

    :try_start_3
    iget-object v1, p0, Lcom/android/settings/PrivacySettings;->bDQ:Landroid/app/backup/IBackupManager;

    invoke-interface {v1, v6}, Landroid/app/backup/IBackupManager;->getDataManagementIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v9, "management"

    invoke-direct {p0, v1, v9}, Lcom/android/settings/PrivacySettings;->bub(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v1

    :try_start_4
    iget-object v9, p0, Lcom/android/settings/PrivacySettings;->bDQ:Landroid/app/backup/IBackupManager;

    invoke-interface {v9, v6}, Landroid/app/backup/IBackupManager;->getDataManagementLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v9, p0, Lcom/android/settings/PrivacySettings;->bDP:Landroid/preference/Preference;

    if-eqz v4, :cond_1

    const v6, 0x7f12004b

    :goto_0
    invoke-virtual {v9, v6}, Landroid/preference/Preference;->setSummary(I)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_1
    iget-object v9, p0, Lcom/android/settings/PrivacySettings;->bDO:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v6, "backup_auto_restore"

    invoke-static {v8, v6, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v7, :cond_2

    move v6, v7

    :goto_2
    invoke-virtual {v9, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v6, p0, Lcom/android/settings/PrivacySettings;->bDO:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    if-eqz v3, :cond_3

    move v6, v4

    :goto_3
    iget-object v8, p0, Lcom/android/settings/PrivacySettings;->bDT:Landroid/preference/Preference;

    invoke-virtual {v8, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v6, p0, Lcom/android/settings/PrivacySettings;->bDT:Landroid/preference/Preference;

    invoke-virtual {v6, v3}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    invoke-direct {p0, v2}, Lcom/android/settings/PrivacySettings;->btY(Ljava/lang/String;)V

    if-eqz v1, :cond_4

    :goto_4
    if-eqz v4, :cond_5

    iget-object v2, p0, Lcom/android/settings/PrivacySettings;->bDU:Landroid/preference/Preference;

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/PrivacySettings;->bDU:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_5
    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "local_auto_backup"

    invoke-static {v0, v1, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v7, :cond_6

    :goto_6
    if-eqz v7, :cond_7

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDN:Lmiui/preference/ValuePreference;

    const v1, 0x7f120918

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    :goto_7
    return-void

    :cond_1
    const v6, 0x7f12004a

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v1, v0

    move-object v2, v0

    move-object v3, v0

    move v4, v5

    :goto_8
    iget-object v6, p0, Lcom/android/settings/PrivacySettings;->bDP:Landroid/preference/Preference;

    invoke-virtual {v6, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_1

    :cond_2
    move v6, v5

    goto :goto_2

    :cond_3
    move v6, v5

    goto :goto_3

    :cond_4
    move v4, v5

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/PrivacySettings;->bDU:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_5

    :cond_6
    move v7, v5

    goto :goto_6

    :cond_7
    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDN:Lmiui/preference/ValuePreference;

    const v1, 0x7f120917

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setValue(I)V

    goto :goto_7

    :catch_1
    move-exception v1

    move-object v1, v0

    move-object v2, v0

    move-object v3, v0

    goto :goto_8

    :catch_2
    move-exception v1

    move-object v1, v0

    move-object v2, v0

    goto :goto_8

    :catch_3
    move-exception v1

    move-object v1, v0

    goto :goto_8

    :catch_4
    move-exception v6

    goto :goto_8
.end method

.method private bub(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string/jumbo v1, "PrivacySettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Backup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " intent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " fails to resolve; ignoring"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object p1, v0

    :cond_1
    return-object p1
.end method

.method static synthetic buc(Lcom/android/settings/PrivacySettings;)Landroid/preference/CheckBoxPreference;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDO:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic bud(Lcom/android/settings/PrivacySettings;)Landroid/app/backup/IBackupManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDQ:Landroid/app/backup/IBackupManager;

    return-object v0
.end method

.method static synthetic bue(Lcom/android/settings/PrivacySettings;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/PrivacySettings;->btX(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected aq()I
    .locals 1

    const v0, 0x7f120814

    return v0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x51

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/PrivacySettings;->mEnabled:Z

    iget-boolean v0, p0, Lcom/android/settings/PrivacySettings;->mEnabled:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v0, 0x7f1500ac

    invoke-virtual {p0, v0}, Lcom/android/settings/PrivacySettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v0, "backup"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/backup/IBackupManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/backup/IBackupManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/PrivacySettings;->bDQ:Landroid/app/backup/IBackupManager;

    invoke-virtual {p0, v1}, Lcom/android/settings/PrivacySettings;->setPreferenceReferences(Landroid/preference/PreferenceScreen;)V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/settings/PrivacySettings;->btW(Landroid/content/Context;Ljava/util/Collection;)V

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "cloud_restore"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/PrivacySettings;->bDS:Landroid/preference/PreferenceScreen;

    const-string/jumbo v0, "cloud_backup_settings"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/PrivacySettings;->bDR:Landroid/preference/PreferenceScreen;

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "cloud_backup_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/PrivacySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDS:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDR:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v0, "local_backup_category"

    invoke-virtual {p0, v0}, Lcom/android/settings/PrivacySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    const-string/jumbo v0, "local_backup"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    const-string/jumbo v0, "local_backup"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const-string/jumbo v1, "com.miui.backup.ui.MainFragmentPad"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setFragment(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDN:Lmiui/preference/ValuePreference;

    invoke-virtual {v0, v5}, Lmiui/preference/ValuePreference;->setIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDN:Lmiui/preference/ValuePreference;

    const-string/jumbo v1, "com.miui.backup.auto.AutoBackupFragmentPad"

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setFragment(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/PrivacySettings;->bua()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDS:Landroid/preference/PreferenceScreen;

    if-eq p2, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDR:Landroid/preference/PreferenceScreen;

    if-ne p2, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lmiui/accounts/ExtraAccountManager;->getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p2}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/PrivacySettings;->btZ(Landroid/content/Intent;)V

    return v2

    :cond_1
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_3

    const-string/jumbo v0, "local_backup"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/PrivacySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v1

    if-eqz v1, :cond_2

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_2
    invoke-virtual {p0, v0}, Lcom/android/settings/PrivacySettings;->startActivity(Landroid/content/Intent;)V

    return v2

    :cond_3
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    iget-boolean v0, p0, Lcom/android/settings/PrivacySettings;->mEnabled:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/PrivacySettings;->bua()V

    :cond_0
    return-void
.end method

.method setPreferenceReferences(Landroid/preference/PreferenceScreen;)V
    .locals 2

    const-string/jumbo v0, "backup_data"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/PrivacySettings;->bDP:Landroid/preference/Preference;

    const-string/jumbo v0, "auto_restore"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/PrivacySettings;->bDO:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDO:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/PrivacySettings;->bDV:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v0, "configure_account"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/PrivacySettings;->bDT:Landroid/preference/Preference;

    const-string/jumbo v0, "data_management"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/PrivacySettings;->bDU:Landroid/preference/Preference;

    const-string/jumbo v0, "local_auto_backup"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiui/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/PrivacySettings;->bDN:Lmiui/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/PrivacySettings;->bDN:Lmiui/preference/ValuePreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/preference/ValuePreference;->setShowRightArrow(Z)V

    return-void
.end method
