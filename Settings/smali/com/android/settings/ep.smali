.class final Lcom/android/settings/ep;
.super Ljava/lang/Object;
.source "NavigationBarGuideView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field final synthetic cix:Lcom/android/settings/NavigationBarGuideView;


# direct methods
.method constructor <init>(Lcom/android/settings/NavigationBarGuideView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    invoke-static {v0}, Lcom/android/settings/NavigationBarGuideView;->blt(Lcom/android/settings/NavigationBarGuideView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    invoke-virtual {v0, v1}, Lcom/android/settings/NavigationBarGuideView;->setScreenButtonHidden(Z)V

    iget-object v0, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    invoke-static {v0}, Lcom/android/settings/NavigationBarGuideView;->blv(Lcom/android/settings/NavigationBarGuideView;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    invoke-static {v0}, Lcom/android/settings/NavigationBarGuideView;->blx(Lcom/android/settings/NavigationBarGuideView;)Landroid/widget/RadioButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    invoke-static {v0}, Lcom/android/settings/NavigationBarGuideView;->blu(Lcom/android/settings/NavigationBarGuideView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    iget-object v2, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    invoke-static {v2}, Lcom/android/settings/NavigationBarGuideView;->bls(Lcom/android/settings/NavigationBarGuideView;)Lmiui/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/app/AlertDialog;->isChecked()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/android/settings/NavigationBarGuideView;->blA(Lcom/android/settings/NavigationBarGuideView;Z)Z

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "need_show_navigation_guide"

    iget-object v2, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    invoke-static {v2}, Lcom/android/settings/NavigationBarGuideView;->blw(Lcom/android/settings/NavigationBarGuideView;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ep;->cix:Lcom/android/settings/NavigationBarGuideView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/NavigationBarGuideView;->bly(Lcom/android/settings/NavigationBarGuideView;Lmiui/app/AlertDialog;)Lmiui/app/AlertDialog;

    return-void
.end method
