.class public Lcom/android/settings/NewFingerprintActivity;
.super Landroid/app/Activity;
.source "NewFingerprintActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private bgM(Ljava/lang/Class;I)V
    .locals 1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, p2}, Lcom/android/settings/NewFingerprintActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    invoke-virtual {p0, p2}, Lcom/android/settings/NewFingerprintActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/NewFingerprintActivity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "miui.permission.USE_INTERNAL_GENERAL_API"

    const-string/jumbo v0, "miui.permission.USE_INTERNAL_GENERAL_API"

    invoke-virtual {p0, v0}, Lcom/android/settings/NewFingerprintActivity;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string/jumbo v1, "Need miui.permission.USE_INTERNAL_GENERAL_API permission to access"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-class v0, Lcom/android/settings/NewFingerprintInternalActivity;

    const/16 v1, 0x65

    invoke-direct {p0, v0, v1}, Lcom/android/settings/NewFingerprintActivity;->bgM(Ljava/lang/Class;I)V

    return-void
.end method
