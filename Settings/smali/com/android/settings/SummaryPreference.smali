.class public Lcom/android/settings/SummaryPreference;
.super Landroid/preference/Preference;
.source "SummaryPreference.java"


# instance fields
.field private bxY:Ljava/lang/String;

.field private bxZ:Z

.field private bya:Z

.field private byb:Ljava/lang/String;

.field private byc:I

.field private byd:F

.field private bye:I

.field private byf:F

.field private byg:I

.field private byh:F

.field private byi:Ljava/lang/String;

.field private byj:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/SummaryPreference;->bya:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/SummaryPreference;->bxZ:Z

    const v0, 0x7f0d01c1

    invoke-virtual {p0, v0}, Lcom/android/settings/SummaryPreference;->setLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public blB(Ljava/lang/String;)V
    .locals 4

    iput-object p1, p0, Lcom/android/settings/SummaryPreference;->bxY:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/SummaryPreference;->bxY:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SummaryPreference;->byj:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SummaryPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1211a5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/android/settings/SummaryPreference;->bxY:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/android/settings/SummaryPreference;->byj:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/SummaryPreference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public blC(Ljava/lang/String;)V
    .locals 4

    iput-object p1, p0, Lcom/android/settings/SummaryPreference;->byj:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/SummaryPreference;->bxY:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SummaryPreference;->byj:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SummaryPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1211a5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/android/settings/SummaryPreference;->bxY:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/android/settings/SummaryPreference;->byj:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/SummaryPreference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 7

    const v6, 0x7f0a024e

    const/16 v5, 0x8

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a00e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/applications/LinearColorBar;

    iget-boolean v1, p0, Lcom/android/settings/SummaryPreference;->bxZ:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0, v4}, Lcom/android/settings/applications/LinearColorBar;->setVisibility(I)V

    iget v1, p0, Lcom/android/settings/SummaryPreference;->byd:F

    iget v2, p0, Lcom/android/settings/SummaryPreference;->byf:F

    iget v3, p0, Lcom/android/settings/SummaryPreference;->byh:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/applications/LinearColorBar;->setRatios(FFF)V

    iget-boolean v1, p0, Lcom/android/settings/SummaryPreference;->bya:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/settings/SummaryPreference;->byc:I

    iget v2, p0, Lcom/android/settings/SummaryPreference;->bye:I

    iget v3, p0, Lcom/android/settings/SummaryPreference;->byg:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/applications/LinearColorBar;->setColors(III)V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/android/settings/SummaryPreference;->bxZ:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/SummaryPreference;->byi:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/SummaryPreference;->byb:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    :cond_1
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/SummaryPreference;->byi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x1020015

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/SummaryPreference;->byb:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {v0, v5}, Lcom/android/settings/applications/LinearColorBar;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public setRatios(FFF)V
    .locals 0

    iput p1, p0, Lcom/android/settings/SummaryPreference;->byd:F

    iput p2, p0, Lcom/android/settings/SummaryPreference;->byf:F

    iput p3, p0, Lcom/android/settings/SummaryPreference;->byh:F

    invoke-virtual {p0}, Lcom/android/settings/SummaryPreference;->notifyChanged()V

    return-void
.end method
