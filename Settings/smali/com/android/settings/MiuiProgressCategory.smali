.class public Lcom/android/settings/MiuiProgressCategory;
.super Lcom/android/settings/MiuiProgressCategoryBase;
.source "MiuiProgressCategory.java"


# instance fields
.field private bYo:I

.field private bYp:Z

.field private bYq:Landroid/preference/Preference;

.field private bYr:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiProgressCategoryBase;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYr:Z

    const v0, 0x7f0d0147

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiProgressCategory;->setLayoutResource(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/MiuiProgressCategoryBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYr:Z

    const v0, 0x7f0d0147

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiProgressCategory;->setLayoutResource(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/MiuiProgressCategoryBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYr:Z

    const v0, 0x7f0d0147

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiProgressCategory;->setLayoutResource(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/MiuiProgressCategoryBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYr:Z

    const v0, 0x7f0d0147

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiProgressCategory;->setLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public bSt(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiProgressCategory;->bYr:Z

    invoke-virtual {p0}, Lcom/android/settings/MiuiProgressCategory;->notifyChanged()V

    return-void
.end method

.method public bSu(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/MiuiProgressCategory;->bYo:I

    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiProgressCategoryBase;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a03a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/settings/MiuiProgressCategory;->getPreferenceCount()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/MiuiProgressCategory;->getPreferenceCount()I

    move-result v0

    if-ne v0, v1, :cond_3

    invoke-virtual {p0, v2}, Lcom/android/settings/MiuiProgressCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/MiuiProgressCategory;->bYq:Landroid/preference/Preference;

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Lcom/android/settings/MiuiProgressCategory;->bYr:Z

    if-eqz v3, :cond_4

    move v3, v2

    :goto_1
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v3, p0, Lcom/android/settings/MiuiProgressCategory;->bYr:Z

    if-nez v3, :cond_0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYp:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYq:Landroid/preference/Preference;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiProgressCategory;->removePreference(Landroid/preference/Preference;)Z

    iput-boolean v2, p0, Lcom/android/settings/MiuiProgressCategory;->bYp:Z

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    const/16 v3, 0x8

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYp:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYq:Landroid/preference/Preference;

    if-nez v0, :cond_6

    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/MiuiProgressCategory;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYq:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYq:Landroid/preference/Preference;

    const v3, 0x7f0d0136

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setLayoutResource(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYq:Landroid/preference/Preference;

    iget v3, p0, Lcom/android/settings/MiuiProgressCategory;->bYo:I

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setTitle(I)V

    iget-object v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYq:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSelectable(Z)V

    :cond_6
    iget-object v0, p0, Lcom/android/settings/MiuiProgressCategory;->bYq:Landroid/preference/Preference;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiProgressCategory;->addPreference(Landroid/preference/Preference;)Z

    iput-boolean v1, p0, Lcom/android/settings/MiuiProgressCategory;->bYp:Z

    goto :goto_2
.end method
