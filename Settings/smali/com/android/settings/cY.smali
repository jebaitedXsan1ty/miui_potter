.class public Lcom/android/settings/cY;
.super Ljava/lang/Object;
.source "AppWidgetLoader.java"


# instance fields
.field private cfG:Landroid/appwidget/AppWidgetManager;

.field cfH:Lcom/android/settings/da;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;Lcom/android/settings/da;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/cY;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/cY;->cfG:Landroid/appwidget/AppWidgetManager;

    iput-object p3, p0, Lcom/android/settings/cY;->cfH:Lcom/android/settings/da;

    return-void
.end method


# virtual methods
.method protected bYg(Landroid/content/Intent;)Ljava/util/List;
    .locals 4

    const/4 v3, 0x1

    const-string/jumbo v0, "customSort"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v2, "categoryFilter"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/android/settings/cY;->bYj(Ljava/util/List;I)V

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1, p1}, Lcom/android/settings/cY;->bYi(Ljava/util/List;Landroid/content/Intent;)V

    :cond_0
    new-instance v2, Lcom/android/settings/lh;

    invoke-direct {v2, p0}, Lcom/android/settings/lh;-><init>(Lcom/android/settings/cY;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v0, p1}, Lcom/android/settings/cY;->bYi(Ljava/util/List;Landroid/content/Intent;)V

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-object v1
.end method

.method bYh(Ljava/util/List;Ljava/util/List;Ljava/util/List;IZ)V
    .locals 7

    const/4 v0, 0x0

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    if-nez p5, :cond_1

    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    and-int/2addr v1, p4

    if-nez v1, :cond_1

    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/settings/cY;->cfH:Lcom/android/settings/da;

    iget-object v6, p0, Lcom/android/settings/cY;->mContext:Landroid/content/Context;

    if-eqz p2, :cond_2

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    :goto_2
    invoke-interface {v5, v6, v0, v1}, Lcom/android/settings/da;->bir(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cZ;

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v1, v2

    goto :goto_2

    :cond_3
    return-void
.end method

.method bYi(Ljava/util/List;Landroid/content/Intent;)V
    .locals 8

    const/4 v4, 0x0

    const/4 v2, 0x0

    const-string/jumbo v0, "customInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string/jumbo v0, "AppWidgetAdapter"

    const-string/jumbo v1, "EXTRA_CUSTOM_INFO not present."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    :goto_0
    const/4 v5, 0x1

    move-object v0, p0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/cY;->bYh(Ljava/util/List;Ljava/util/List;Ljava/util/List;IZ)V

    return-void

    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v4

    :goto_1
    if-ge v1, v5, :cond_4

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    if-eqz v0, :cond_2

    instance-of v0, v0, Landroid/appwidget/AppWidgetProviderInfo;

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    :cond_2
    const-string/jumbo v0, "AppWidgetAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "error using EXTRA_CUSTOM_INFO index="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "customExtras"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_5

    const-string/jumbo v0, "AppWidgetAdapter"

    const-string/jumbo v3, "EXTRA_CUSTOM_INFO without EXTRA_CUSTOM_EXTRAS"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eq v5, v6, :cond_6

    const-string/jumbo v0, "AppWidgetAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "list size mismatch: EXTRA_CUSTOM_INFO: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " EXTRA_CUSTOM_EXTRAS: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    goto :goto_0

    :cond_6
    move v5, v4

    :goto_2
    if-ge v5, v6, :cond_9

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    if-eqz v0, :cond_7

    instance-of v0, v0, Landroid/os/Bundle;

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    :cond_7
    const-string/jumbo v0, "AppWidgetAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "error using EXTRA_CUSTOM_EXTRAS index="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    goto/16 :goto_0

    :cond_8
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    :cond_9
    move-object v2, v1

    move-object v1, v3

    goto/16 :goto_0
.end method

.method bYj(Ljava/util/List;I)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/cY;->cfG:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v0, p2}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders(I)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/cY;->bYh(Ljava/util/List;Ljava/util/List;Ljava/util/List;IZ)V

    return-void
.end method
