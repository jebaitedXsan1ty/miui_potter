.class final Lcom/android/settings/jx;
.super Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;
.source "FingerprintHelper.java"


# instance fields
.field final synthetic cpE:Lcom/android/settings/bM;

.field final synthetic cpF:Lcom/android/settings/bx;

.field final synthetic cpG:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/android/settings/bM;Lcom/android/settings/bx;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/jx;->cpE:Lcom/android/settings/bM;

    iput-object p2, p0, Lcom/android/settings/jx;->cpF:Lcom/android/settings/bx;

    iput-object p3, p0, Lcom/android/settings/jx;->cpG:Ljava/util/List;

    invoke-direct {p0}, Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnrollmentError(ILjava/lang/CharSequence;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;->onEnrollmentError(ILjava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/jx;->cpE:Lcom/android/settings/bM;

    invoke-static {v0}, Lcom/android/settings/bM;->bNp(Lcom/android/settings/bM;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/jx;->cpF:Lcom/android/settings/bx;

    invoke-interface {v0}, Lcom/android/settings/bx;->bKw()V

    iget-object v0, p0, Lcom/android/settings/jx;->cpE:Lcom/android/settings/bM;

    iget-object v1, p0, Lcom/android/settings/jx;->cpE:Lcom/android/settings/bM;

    invoke-static {v1}, Lcom/android/settings/bM;->bNp(Lcom/android/settings/bM;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/bM;->bNr(Lcom/android/settings/bM;Z)Z

    :cond_0
    return-void
.end method

.method public onEnrollmentHelp(ILjava/lang/CharSequence;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;->onEnrollmentHelp(ILjava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/jx;->cpF:Lcom/android/settings/bx;

    invoke-interface {v0, p1, p2}, Lcom/android/settings/bx;->onEnrollmentHelp(ILjava/lang/CharSequence;)V

    return-void
.end method

.method public onEnrollmentProgress(I)V
    .locals 4

    invoke-super {p0, p1}, Landroid/hardware/fingerprint/FingerprintManager$EnrollmentCallback;->onEnrollmentProgress(I)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/jx;->cpF:Lcom/android/settings/bx;

    invoke-interface {v0, p1}, Lcom/android/settings/bx;->bKx(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/jx;->cpF:Lcom/android/settings/bx;

    invoke-interface {v0}, Lcom/android/settings/bx;->bKv()V

    iget-object v0, p0, Lcom/android/settings/jx;->cpE:Lcom/android/settings/bM;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/settings/bM;->bNq(Lcom/android/settings/bM;Z)Z

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/jx;->cpE:Lcom/android/settings/bM;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/jx;->cpG:Ljava/util/List;

    iget-object v3, p0, Lcom/android/settings/jx;->cpE:Lcom/android/settings/bM;

    invoke-virtual {v3}, Lcom/android/settings/bM;->bNe()Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/settings/bM;->bNs(Lcom/android/settings/bM;ILjava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method
