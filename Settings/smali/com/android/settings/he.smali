.class final Lcom/android/settings/he;
.super Ljava/lang/Object;
.source "MutedVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field final synthetic cmG:Lcom/android/settings/MutedVideoView;


# direct methods
.method constructor <init>(Lcom/android/settings/MutedVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 6

    const/4 v5, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/settings/MutedVideoView;->bAM(Lcom/android/settings/MutedVideoView;I)I

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    iget-object v2, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v2, v3}, Lcom/android/settings/MutedVideoView;->bAK(Lcom/android/settings/MutedVideoView;Z)Z

    move-result v2

    invoke-static {v1, v2}, Lcom/android/settings/MutedVideoView;->bAJ(Lcom/android/settings/MutedVideoView;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/MutedVideoView;->bAI(Lcom/android/settings/MutedVideoView;Z)Z

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAB(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAB(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAx(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/MediaController;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/MutedVideoView;->bAS(Lcom/android/settings/MutedVideoView;I)I

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/MutedVideoView;->bAR(Lcom/android/settings/MutedVideoView;I)I

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAC(Lcom/android/settings/MutedVideoView;)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v1, v0}, Lcom/android/settings/MutedVideoView;->seekTo(I)V

    :cond_2
    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAH(Lcom/android/settings/MutedVideoView;)I

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAG(Lcom/android/settings/MutedVideoView;)I

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v1}, Lcom/android/settings/MutedVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v2}, Lcom/android/settings/MutedVideoView;->bAH(Lcom/android/settings/MutedVideoView;)I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v3}, Lcom/android/settings/MutedVideoView;->bAG(Lcom/android/settings/MutedVideoView;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAE(Lcom/android/settings/MutedVideoView;)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v2}, Lcom/android/settings/MutedVideoView;->bAH(Lcom/android/settings/MutedVideoView;)I

    move-result v2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAD(Lcom/android/settings/MutedVideoView;)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v2}, Lcom/android/settings/MutedVideoView;->bAG(Lcom/android/settings/MutedVideoView;)I

    move-result v2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->bAF(Lcom/android/settings/MutedVideoView;)I

    move-result v1

    if-ne v1, v5, :cond_4

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Lcom/android/settings/MutedVideoView;->start()V

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-object v1, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v1}, Lcom/android/settings/MutedVideoView;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_3

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Lcom/android/settings/MutedVideoView;->getCurrentPosition()I

    move-result v0

    if-lez v0, :cond_3

    :cond_5
    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAw(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/MediaController;->show(I)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-static {v0}, Lcom/android/settings/MutedVideoView;->bAF(Lcom/android/settings/MutedVideoView;)I

    move-result v0

    if-ne v0, v5, :cond_3

    iget-object v0, p0, Lcom/android/settings/he;->cmG:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Lcom/android/settings/MutedVideoView;->start()V

    goto :goto_0
.end method
