.class Lcom/android/settings/cT;
.super Lmiui/preference/RadioButtonPreference;
.source "MiuiSmartCoverSettingsFragment.java"


# instance fields
.field private ceU:Landroid/widget/ImageView;

.field private ceV:I

.field private ceW:I

.field final synthetic ceX:Lcom/android/settings/MiuiSmartCoverSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/MiuiSmartCoverSettingsFragment;Landroid/content/Context;II)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/cT;-><init>(Lcom/android/settings/MiuiSmartCoverSettingsFragment;Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput p3, p0, Lcom/android/settings/cT;->ceV:I

    iput p4, p0, Lcom/android/settings/cT;->ceW:I

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/MiuiSmartCoverSettingsFragment;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/cT;-><init>(Lcom/android/settings/MiuiSmartCoverSettingsFragment;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/MiuiSmartCoverSettingsFragment;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/cT;->ceX:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-direct {p0, p2, p3, p4}, Lmiui/preference/RadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const v0, 0x7f0d0181

    invoke-virtual {p0, v0}, Lcom/android/settings/cT;->setWidgetLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2}, Lmiui/preference/RadioButtonPreference;->getView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/cT;->ceX:Lcom/android/settings/MiuiSmartCoverSettingsFragment;

    invoke-virtual {v1}, Lcom/android/settings/MiuiSmartCoverSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070222

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setMinimumHeight(I)V

    return-object v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/preference/RadioButtonPreference;->onBindView(Landroid/view/View;)V

    const v0, 0x7f0a037a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/cT;->ceU:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/settings/cT;->ceU:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/settings/cT;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/cT;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/cT;->ceW:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void

    :cond_0
    iget v0, p0, Lcom/android/settings/cT;->ceV:I

    goto :goto_0
.end method
