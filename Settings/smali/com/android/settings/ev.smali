.class final Lcom/android/settings/ev;
.super Landroid/os/AsyncTask;
.source "LockPatternChecker.java"


# instance fields
.field private cja:I

.field private cjb:I

.field final synthetic cjc:Ljava/util/List;

.field final synthetic cjd:Lcom/android/internal/widget/LockPatternUtils;

.field final synthetic cje:Ljava/lang/String;

.field final synthetic cjf:Lcom/android/settings/N;


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;Lcom/android/settings/N;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/ev;->cjc:Ljava/util/List;

    iput-object p2, p0, Lcom/android/settings/ev;->cjd:Lcom/android/internal/widget/LockPatternUtils;

    iput-object p3, p0, Lcom/android/settings/ev;->cje:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/settings/ev;->cjf:Lcom/android/settings/N;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/16 v0, -0x2710

    iput v0, p0, Lcom/android/settings/ev;->cjb:I

    return-void
.end method


# virtual methods
.method protected bZU(Ljava/lang/Boolean;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/ev;->cjf:Lcom/android/settings/N;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget v2, p0, Lcom/android/settings/ev;->cjb:I

    iget v3, p0, Lcom/android/settings/ev;->cja:I

    invoke-interface {v0, v1, v2, v3}, Lcom/android/settings/N;->blY(ZII)V

    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 6

    const/4 v5, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/ev;->cjc:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget-object v2, p0, Lcom/android/settings/ev;->cjd:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v2, v3}, Lcom/android/settings/bn;->bFt(Lcom/android/internal/widget/LockPatternUtils;I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/ev;->cjd:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v3, p0, Lcom/android/settings/ev;->cje:Ljava/lang/String;

    iget v4, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v2, v3, v4}, Lcom/android/settings/bn;->bFu(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    iput v0, p0, Lcom/android/settings/ev;->cjb:I

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    iput v5, p0, Lcom/android/settings/ev;->cja:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/settings/ev;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/settings/ev;->bZU(Ljava/lang/Boolean;)V

    return-void
.end method
