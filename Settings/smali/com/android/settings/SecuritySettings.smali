.class public Lcom/android/settings/SecuritySettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;
.source "SecuritySettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/search/Indexable;
.implements Lcom/android/settings/widget/z;


# static fields
.field static final KEY_LOCKSCREEN_PREFERENCES:Ljava/lang/String; = "lockscreen_preferences"

.field static final KEY_MANAGE_TRUST_AGENTS:Ljava/lang/String; = "manage_trust_agents"

.field static final KEY_PACKAGE_VERIFIER_STATUS:Ljava/lang/String; = "security_status_package_verifier"

.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

.field public static final SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

.field private static final bEh:I

.field private static final bEi:[Ljava/lang/String;

.field private static final bEj:Landroid/content/Intent;


# instance fields
.field private bEA:Landroid/telephony/SubscriptionManager;

.field private bEB:Landroid/content/Intent;

.field private bEC:Lcom/android/settings/d/b;

.field private bED:Landroid/os/UserManager;

.field private bEE:Landroid/preference/CheckBoxPreference;

.field private bEF:Landroid/preference/CheckBoxPreference;

.field private bEG:Landroid/preference/CheckBoxPreference;

.field private bEH:Landroid/preference/CheckBoxPreference;

.field private bEk:Lcom/android/settings/cx;

.field private bEl:Ljava/lang/String;

.field private bEm:Ljava/lang/String;

.field private bEn:Landroid/app/admin/DevicePolicyManager;

.field private bEo:Lcom/android/settings/dashboard/n;

.field private bEp:Lcom/android/settings/enterprise/o;

.field private bEq:Z

.field private bEr:Landroid/security/KeyStore;

.field private bEs:Lcom/android/settings/location/LocationPreferenceController;

.field private bEt:Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

.field private bEu:Lcom/android/settings/enterprise/b;

.field private bEv:Lcom/android/settings/cv;

.field private bEw:I

.field private bEx:Lcom/android/settingslib/MiuiRestrictedPreference;

.field private bEy:Lcom/android/settings/security/d;

.field private bEz:Landroid/preference/CheckBoxPreference;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;


# direct methods
.method static synthetic -get0()I
    .locals 1

    sget v0, Lcom/android/settings/SecuritySettings;->bEh:I

    return v0
.end method

.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.service.trust.TrustAgentService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/settings/SecuritySettings;->bEj:Landroid/content/Intent;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "show_password"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string/jumbo v1, "unification"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string/jumbo v1, "visiblepattern_profile"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/SecuritySettings;->bEi:[Ljava/lang/String;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    sput v0, Lcom/android/settings/SecuritySettings;->bEh:I

    new-instance v0, Lcom/android/settings/aB;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/settings/aB;-><init>(Lcom/android/settings/aB;)V

    sput-object v0, Lcom/android/settings/SecuritySettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/Indexable$SearchIndexProvider;

    new-instance v0, Lcom/android/settings/fT;

    invoke-direct {v0}, Lcom/android/settings/fT;-><init>()V

    sput-object v0, Lcom/android/settings/SecuritySettings;->SUMMARY_PROVIDER_FACTORY:Lcom/android/settings/dashboard/F;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private static buA(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/settings/cv;I)I
    .locals 3

    const/4 v1, 0x0

    sget v0, Lcom/android/settings/SecuritySettings;->bEh:I

    if-ne p3, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, p3}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v2

    if-nez v2, :cond_3

    if-nez v0, :cond_1

    const v0, 0x7f1500c6

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p3}, Lcom/android/internal/widget/LockPatternUtils;->isLockScreenDisabled(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f1500c5

    goto :goto_1

    :cond_2
    const v0, 0x7f1500bf

    goto :goto_1

    :cond_3
    invoke-virtual {p1, p3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    goto :goto_1

    :sswitch_0
    if-eqz v0, :cond_4

    const v0, 0x7f1500cb

    goto :goto_1

    :cond_4
    const v0, 0x7f1500cc

    goto :goto_1

    :sswitch_1
    if-eqz v0, :cond_5

    const v0, 0x7f1500cf

    goto :goto_1

    :cond_5
    const v0, 0x7f1500d0

    goto :goto_1

    :sswitch_2
    if-eqz v0, :cond_6

    const v0, 0x7f1500c8

    goto :goto_1

    :cond_6
    const v0, 0x7f1500c9

    goto :goto_1

    :sswitch_3
    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Lcom/android/settings/cv;->bSJ(Z)I

    move-result v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x30000 -> :sswitch_1
        0x40000 -> :sswitch_2
        0x50000 -> :sswitch_2
        0x60000 -> :sswitch_2
        0x80000 -> :sswitch_3
    .end sparse-switch
.end method

.method private buB()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f12130b

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/android/settings/cx;

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    sget v2, Lcom/android/settings/SecuritySettings;->bEh:I

    const/16 v3, 0x80

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v0, v4, v2}, Lcom/android/settings/cx;->bSS(ILjava/lang/CharSequence;ZI)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buC()V

    :cond_0
    return-void
.end method

.method private buC()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f12130c

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/android/settings/cx;

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    iget v2, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    const/16 v3, 0x81

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v0, v4, v2}, Lcom/android/settings/cx;->bSS(ILjava/lang/CharSequence;ZI)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buF()V

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buw()Landroid/preference/PreferenceScreen;

    :cond_0
    return-void
.end method

.method private buD(Landroid/preference/PreferenceGroup;I)V
    .locals 0

    return-void
.end method

.method private buE()V
    .locals 5

    const/4 v0, 0x0

    const-string/jumbo v1, "ad_control_settings"

    invoke-virtual {p0, v1}, Lcom/android/settings/SecuritySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-eqz v2, :cond_1

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->bWA()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v3, "com.miui.systemAdSolution"

    const/16 v4, 0x80

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v3, :cond_0

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string/jumbo v3, "SupportPersonalizedAd"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    const-string/jumbo v0, "security_settings_access_control"

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private buF()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v0

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEm:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/internal/widget/LockPatternUtils;->stringToPattern(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEl:Ljava/lang/String;

    sget v3, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPattern(Ljava/util/List;Ljava/lang/String;I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEm:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/internal/widget/LockPatternUtils;->setSeparateProfileChallengeEnabled(IZLjava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isVisiblePatternEnabled(I)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v2, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-virtual {v1, v0, v2}, Lcom/android/internal/widget/LockPatternUtils;->setVisiblePatternEnabled(ZI)V

    iput-object v5, p0, Lcom/android/settings/SecuritySettings;->bEl:Ljava/lang/String;

    iput-object v5, p0, Lcom/android/settings/SecuritySettings;->bEm:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEm:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/SecuritySettings;->bEl:Ljava/lang/String;

    sget v4, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private buG()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEm:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/android/internal/widget/LockPatternUtils;->setSeparateProfileChallengeEnabled(IZLjava/lang/String;)V

    const-string/jumbo v2, "com.android.settings.ChooseLockGeneric$ChooseLockGenericFragment"

    const v3, 0x7f120984

    const/16 v4, 0x7b

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/SecuritySettings;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    return-void
.end method

.method private buH()V
    .locals 6

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "android.intent.extra.USER_ID"

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "com.android.settings.ChooseLockGeneric$ChooseLockGenericFragment"

    const v3, 0x7f120985

    const/16 v4, 0x7f

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/SecuritySettings;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    return-void
.end method

.method private buI()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEE:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEE:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v2, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeEnabled(I)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method static synthetic buJ(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/settings/cv;I)I
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lcom/android/settings/SecuritySettings;->buA(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/settings/cv;I)I

    move-result v0

    return v0
.end method

.method static synthetic buK(Landroid/content/Context;Lcom/android/settings/d/b;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/admin/DevicePolicyManager;)Ljava/lang/CharSequence;
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lcom/android/settings/SecuritySettings;->buy(Landroid/content/Context;Lcom/android/settings/d/b;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/admin/DevicePolicyManager;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic buL(Landroid/content/Context;Lcom/android/settings/d/b;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/admin/DevicePolicyManager;)Ljava/util/ArrayList;
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lcom/android/settings/SecuritySettings;->buz(Landroid/content/Context;Lcom/android/settings/d/b;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/admin/DevicePolicyManager;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic buM(Lcom/android/settings/SecuritySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buB()V

    return-void
.end method

.method static synthetic buN(Lcom/android/settings/SecuritySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buG()V

    return-void
.end method

.method static synthetic buO(Lcom/android/settings/SecuritySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buI()V

    return-void
.end method

.method private buv(Landroid/preference/PreferenceGroup;)I
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v1, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v3

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEC:Lcom/android/settings/d/b;

    iget-object v4, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v5, p0, Lcom/android/settings/SecuritySettings;->bEn:Landroid/app/admin/DevicePolicyManager;

    invoke-static {v0, v1, v4, v5}, Lcom/android/settings/SecuritySettings;->buz(Landroid/content/Context;Lcom/android/settings/d/b;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/admin/DevicePolicyManager;)Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cu;

    new-instance v5, Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/android/settingslib/MiuiRestrictedPreference;-><init>(Landroid/content/Context;)V

    const-string/jumbo v6, "trust_agent"

    invoke-virtual {v5, v6}, Lcom/android/settingslib/MiuiRestrictedPreference;->setKey(Ljava/lang/String;)V

    iget-object v6, v0, Lcom/android/settings/cu;->title:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/android/settingslib/MiuiRestrictedPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v6, v0, Lcom/android/settings/cu;->summary:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/android/settingslib/MiuiRestrictedPreference;->setSummary(Ljava/lang/CharSequence;)V

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    iget-object v7, v0, Lcom/android/settings/cu;->bYe:Landroid/content/ComponentName;

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string/jumbo v7, "android.intent.action.MAIN"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v5, v6}, Lcom/android/settingslib/MiuiRestrictedPreference;->setIntent(Landroid/content/Intent;)V

    iget-object v0, v0, Lcom/android/settings/cu;->bYf:Lcom/android/settingslib/n;

    invoke-virtual {v5, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/n;)V

    invoke-virtual {v5}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqf()Z

    move-result v0

    if-nez v0, :cond_0

    xor-int/lit8 v0, v3, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v5, v2}, Lcom/android/settingslib/MiuiRestrictedPreference;->setEnabled(Z)V

    const v0, 0x7f1205d4

    invoke-virtual {v5, v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->setSummary(I)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method private buw()Landroid/preference/PreferenceScreen;
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    const v0, 0x7f1500bb

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const v0, 0x7f1500d5

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEv:Lcom/android/settings/cv;

    sget v5, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-static {v0, v1, v2, v5}, Lcom/android/settings/SecuritySettings;->buA(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/settings/cv;I)I

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buE()V

    const-string/jumbo v0, "unlock_set_or_change"

    sget v1, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-direct {p0, v0, v1}, Lcom/android/settings/SecuritySettings;->bux(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bED:Landroid/os/UserManager;

    sget v1, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-static {v0, v1}, Lcom/android/settings/aq;->bqt(Landroid/os/UserManager;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    iget v0, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeAllowed(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f1500d2

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->addPreferencesFromResource(I)V

    const v0, 0x7f1500d7

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEv:Lcom/android/settings/cv;

    iget v5, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-static {v0, v1, v2, v5}, Lcom/android/settings/SecuritySettings;->buA(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/settings/cv;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->addPreferencesFromResource(I)V

    iget v0, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-direct {p0, v4, v0}, Lcom/android/settings/SecuritySettings;->buD(Landroid/preference/PreferenceGroup;I)V

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeEnabled(I)Z

    move-result v0

    if-nez v0, :cond_6

    const-string/jumbo v0, "unlock_set_or_change_profile"

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f12098f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    const-string/jumbo v0, "unlock_set_or_change"

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-direct {p0, v0, v1}, Lcom/android/settings/SecuritySettings;->bux(Ljava/lang/String;I)V

    :cond_1
    :goto_0
    const-string/jumbo v0, "unlock_set_or_change"

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    instance-of v1, v0, Lcom/android/settings/widget/MiuiGearPreference;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/android/settings/widget/MiuiGearPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/MiuiGearPreference;->aBO(Lcom/android/settings/widget/z;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bED:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/SecuritySettings;->bEq:Z

    const-string/jumbo v0, "security_category"

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_b

    invoke-direct {p0, v0}, Lcom/android/settings/SecuritySettings;->buv(Landroid/preference/PreferenceGroup;)I

    move-result v1

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->setLockscreenPreferencesSummary(Landroid/preference/PreferenceGroup;)V

    move v2, v1

    :goto_1
    const-string/jumbo v0, "visiblepattern_profile"

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEH:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "unification"

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEE:Landroid/preference/CheckBoxPreference;

    const v0, 0x7f1500c7

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "advanced_security"

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-static {}, Lcom/android/settings/FakeCellSettings;->bRF()Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "manage_fakecell_settings"

    invoke-virtual {p0, v1}, Lcom/android/settings/SecuritySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v5, "carrier_config"

    invoke-virtual {v1, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/CarrierConfigManager;

    invoke-virtual {v1}, Landroid/telephony/CarrierConfigManager;->getConfig()Landroid/os/PersistableBundle;

    move-result-object v1

    iget-boolean v5, p0, Lcom/android/settings/SecuritySettings;->bEq:Z

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/android/settings/SecuritySettings;->bED:Landroid/os/UserManager;

    invoke-virtual {v5}, Landroid/os/UserManager;->isSystemUser()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/settings/dc;->bYE()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-static {}, Lcom/android/settings/dc;->getInstance()Lcom/android/settings/dc;

    move-result-object v1

    const-string/jumbo v5, "sim_lock_settings"

    invoke-virtual {v1, v4, v5}, Lcom/android/settings/dc;->At(Landroid/preference/PreferenceScreen;Ljava/lang/String;)V

    :goto_2
    if-eqz v0, :cond_4

    const-string/jumbo v1, "screen_pinning_settings"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_4
    const-string/jumbo v0, "show_password"

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEz:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {p0, v4, v2}, Lcom/android/settings/SecuritySettings;->initTrustAgentPreference(Landroid/preference/PreferenceScreen;I)V

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/overlay/a;->aIt()Lcom/android/settings/search2/SearchFeatureProvider;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/android/settings/search2/SearchFeatureProvider;->getIndexingManager(Landroid/content/Context;)Lcom/android/settings/search2/DatabaseIndexingManager;

    move-result-object v0

    const-class v1, Lcom/android/settings/SecuritySettings;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/search2/DatabaseIndexingManager;->updateFromClassNameResource(Ljava/lang/String;Z)V

    const-string/jumbo v0, "security_status"

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_3
    sget-object v0, Lcom/android/settings/SecuritySettings;->bEi:[Ljava/lang/String;

    array-length v0, v0

    if-ge v3, v0, :cond_a

    sget-object v0, Lcom/android/settings/SecuritySettings;->bEi:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_6
    const-string/jumbo v0, "unlock_set_or_change_profile"

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-direct {p0, v0, v1}, Lcom/android/settings/SecuritySettings;->bux(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_7
    iget-boolean v5, p0, Lcom/android/settings/SecuritySettings;->bEq:Z

    if-eqz v5, :cond_8

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->isSimIccReady()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/android/settings/SecuritySettings;->bED:Landroid/os/UserManager;

    invoke-virtual {v5}, Landroid/os/UserManager;->isSystemUser()Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    if-nez v5, :cond_8

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/android/settings/security/a;->aTV(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_8

    const-string/jumbo v5, "hide_sim_lock_settings_bool"

    invoke-virtual {v1, v5}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_8
    const-string/jumbo v1, "sim_lock_settings"

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2

    :cond_9
    const-string/jumbo v1, "sim_lock_settings"

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->isSimReady()Z

    move-result v5

    invoke-virtual {v1, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_2

    :cond_a
    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEs:Lcom/android/settings/location/LocationPreferenceController;

    invoke-virtual {v0, v4}, Lcom/android/settings/location/LocationPreferenceController;->i(Landroid/preference/PreferenceScreen;)V

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEp:Lcom/android/settings/enterprise/o;

    invoke-virtual {v0, v4}, Lcom/android/settings/enterprise/o;->i(Landroid/preference/PreferenceScreen;)V

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEp:Lcom/android/settings/enterprise/o;

    invoke-virtual {v0}, Lcom/android/settings/enterprise/o;->onResume()V

    return-object v4

    :cond_b
    move v2, v3

    goto/16 :goto_1
.end method

.method private bux(Ljava/lang/String;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/settingslib/w;->crh(Landroid/content/Context;I)Lcom/android/settingslib/n;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEn:Landroid/app/admin/DevicePolicyManager;

    iget-object v0, v0, Lcom/android/settingslib/n;->cPP:Landroid/content/ComponentName;

    invoke-virtual {v1, v0, p2}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;I)I

    move-result v0

    const/high16 v1, 0x80000

    :cond_0
    return-void
.end method

.method private static buy(Landroid/content/Context;Lcom/android/settings/d/b;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/admin/DevicePolicyManager;)Ljava/lang/CharSequence;
    .locals 2

    invoke-static {p0, p1, p2, p3}, Lcom/android/settings/SecuritySettings;->buz(Landroid/content/Context;Lcom/android/settings/d/b;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/admin/DevicePolicyManager;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/cu;

    iget-object v0, v0, Lcom/android/settings/cu;->title:Ljava/lang/String;

    goto :goto_0
.end method

.method private static buz(Landroid/content/Context;Lcom/android/settings/d/b;Lcom/android/internal/widget/LockPatternUtils;Landroid/app/admin/DevicePolicyManager;)Ljava/util/ArrayList;
    .locals 10

    const/4 v9, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/android/settings/SecuritySettings;->bEj:Landroid/content/Intent;

    const/16 v1, 0x80

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    sget v0, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-virtual {p2, v0}, Lcom/android/internal/widget/LockPatternUtils;->getEnabledTrustAgents(I)Ljava/util/List;

    move-result-object v5

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const/16 v1, 0x10

    invoke-static {p0, v1, v0}, Lcom/android/settingslib/w;->cqR(Landroid/content/Context;II)Lcom/android/settingslib/n;

    move-result-object v6

    if-eqz v5, :cond_3

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-nez v7, :cond_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {p1, v0, v2}, Lcom/android/settings/d/b;->aYO(Landroid/content/pm/ResolveInfo;Landroid/content/pm/PackageManager;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v2, v0}, Lcom/android/settings/ct;->bRV(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;)Lcom/android/settings/cu;

    move-result-object v7

    iget-object v8, v7, Lcom/android/settings/cu;->bYe:Landroid/content/ComponentName;

    if-eqz v8, :cond_0

    invoke-static {v0}, Lcom/android/settings/ct;->bRW(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    xor-int/lit8 v8, v8, 0x1

    if-nez v8, :cond_0

    iget-object v8, v7, Lcom/android/settings/cu;->title:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    if-eqz v6, :cond_2

    invoke-static {v0}, Lcom/android/settings/ct;->bRW(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {p3, v9, v0}, Landroid/app/admin/DevicePolicyManager;->getTrustAgentConfiguration(Landroid/content/ComponentName;Landroid/content/ComponentName;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    iput-object v6, v7, Lcom/android/settings/cu;->bYf:Lcom/android/settingslib/n;

    :cond_2
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    return-object v3
.end method

.method private isSimIccReady()Z
    .locals 3

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEA:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSimSlotIndex()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/telephony/TelephonyManager;->hasIccCard(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private isSimReady()Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEA:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/SubscriptionInfo;

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSimSlotIndex()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/telephony/TelephonyManager;->getSimState(I)I

    move-result v0

    if-eq v0, v4, :cond_0

    if-eqz v0, :cond_0

    return v4

    :cond_1
    return v3
.end method


# virtual methods
.method public QS(Lcom/android/settings/widget/MiuiGearPreference;)V
    .locals 6

    const/4 v3, 0x0

    const-string/jumbo v0, "unlock_set_or_change"

    invoke-virtual {p1}, Lcom/android/settings/widget/MiuiGearPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/android/settings/SecuritySettings$SecuritySubSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/SecuritySettings;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    :cond_0
    return-void
.end method

.method protected aq()I
    .locals 1

    const v0, 0x7f120822

    return v0
.end method

.method public at(Landroid/preference/Preference;)Z
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unlock_set_or_change"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v0, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeEnabled(I)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/storage/StorageManager;->isFileEncryptedNativeOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bED:Landroid/os/UserManager;

    iget v2, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-static {v0, v1, v2}, Lcom/android/settings/aq;->bqu(Landroid/content/Context;Landroid/os/UserManager;I)Z

    move-result v0

    if-eqz v0, :cond_0

    return v3

    :cond_0
    const-string/jumbo v2, "com.android.settings.MiuiSecuritySettings"

    const v3, 0x7f120984

    const/16 v4, 0x7b

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/SecuritySettings;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->at(Landroid/preference/Preference;)Z

    move-result v0

    return v0

    :cond_2
    const-string/jumbo v1, "unlock_set_or_change_profile"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bED:Landroid/os/UserManager;

    iget v2, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-static {v0, v1, v2}, Lcom/android/settings/aq;->bqu(Landroid/content/Context;Landroid/os/UserManager;I)Z

    move-result v0

    if-eqz v0, :cond_3

    return v3

    :cond_3
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "android.intent.extra.USER_ID"

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "com.android.settings.ChooseLockGeneric$ChooseLockGenericFragment"

    const v3, 0x7f120985

    const/16 v4, 0x7f

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/SecuritySettings;->bWM(Landroid/app/Fragment;Ljava/lang/String;IILandroid/os/Bundle;)Z

    goto :goto_0

    :cond_4
    const-string/jumbo v1, "trust_agent"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v0, Lcom/android/settings/cx;

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    const/16 v2, 0x7e

    invoke-virtual {v0, v2, v1}, Lcom/android/settings/cx;->bSO(ILjava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->startActivity(Landroid/content/Intent;)V

    iput-object v5, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEG:Landroid/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_6

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEG:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/provider/MiuiSettings$Secure;->enableUserExperienceProgram(Landroid/content/ContentResolver;Z)V

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEF:Landroid/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_7

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/provider/MiuiSettings$Secure;->enableUploadDebugLog(Landroid/content/ContentResolver;Z)V

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v1, "lock_screen_unlock_show"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "show_lock_before_unlock"

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v1, v2, v0}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto/16 :goto_0
.end method

.method public getMetricsCategory()I
    .locals 1

    const/16 v0, 0x57

    return v0
.end method

.method initTrustAgentPreference(Landroid/preference/PreferenceScreen;I)V
    .locals 5

    const/4 v4, 0x0

    const-string/jumbo v0, "manage_trust_agents"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v2, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    const v1, 0x7f1205d4

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    :cond_0
    :goto_0
    const v0, 0x7f1500c1

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "user_experience_program"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEG:Landroid/preference/CheckBoxPreference;

    const-string/jumbo v0, "upload_debug_log"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEF:Landroid/preference/CheckBoxPreference;

    return-void

    :cond_1
    if-lez p2, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const v3, 0x7f100027

    invoke-virtual {v1, v3, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f120a11

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x7e

    if-ne p1, v0, :cond_1

    if-ne p2, v3, :cond_1

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->startActivity(Landroid/content/Intent;)V

    iput-object v4, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x80

    if-ne p1, v0, :cond_2

    if-ne p2, v3, :cond_2

    const-string/jumbo v0, "password"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEl:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buC()V

    return-void

    :cond_2
    const/16 v0, 0x81

    if-ne p1, v0, :cond_3

    if-ne p2, v3, :cond_3

    const-string/jumbo v0, "password"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEm:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buF()V

    return-void

    :cond_3
    const/16 v0, 0x82

    if-ne p1, v0, :cond_4

    if-ne p2, v3, :cond_4

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buH()V

    return-void

    :cond_4
    const/16 v0, 0x3ec

    if-ne p1, v0, :cond_6

    if-ne p2, v3, :cond_7

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "install_non_market_apps"

    if-eqz v0, :cond_5

    :goto_1
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_5
    move v1, v2

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buw()Landroid/preference/PreferenceScreen;

    return-void

    :cond_7
    move v0, v2

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEA:Landroid/telephony/SubscriptionManager;

    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v0, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-static {v1, v0}, Lcom/android/settings/cv;->bSH(Landroid/content/Context;I)Lcom/android/settings/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEv:Lcom/android/settings/cv;

    const-string/jumbo v0, "device_policy"

    invoke-virtual {p0, v0}, Lcom/android/settings/SecuritySettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEn:Landroid/app/admin/DevicePolicyManager;

    invoke-static {v1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bED:Landroid/os/UserManager;

    new-instance v0, Lcom/android/settings/cx;

    invoke-direct {v0, v1}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEk:Lcom/android/settings/cx;

    invoke-static {v1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/settings/overlay/a;->aIn(Landroid/content/Context;)Lcom/android/settings/dashboard/n;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEo:Lcom/android/settings/dashboard/n;

    invoke-static {v1}, Lcom/android/settings/overlay/a;->aIk(Landroid/content/Context;)Lcom/android/settings/overlay/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/a;->aIu()Lcom/android/settings/security/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEy:Lcom/android/settings/security/d;

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEy:Lcom/android/settings/security/d;

    invoke-interface {v0}, Lcom/android/settings/security/d;->aTZ()Lcom/android/settings/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEC:Lcom/android/settings/d/b;

    if-eqz p1, :cond_0

    const-string/jumbo v0, "trust_agent_click_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "trust_agent_click_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    :cond_0
    new-instance v0, Lcom/android/settings/location/LocationPreferenceController;

    invoke-direct {v0, v1}, Lcom/android/settings/location/LocationPreferenceController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEs:Lcom/android/settings/location/LocationPreferenceController;

    new-instance v0, Lcom/android/settings/enterprise/b;

    invoke-direct {v0, v1}, Lcom/android/settings/enterprise/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEu:Lcom/android/settings/enterprise/b;

    new-instance v0, Lcom/android/settings/enterprise/o;

    invoke-direct {v0, v1, v2}, Lcom/android/settings/enterprise/o;-><init>(Landroid/content/Context;Lcom/android/settings/core/lifecycle/c;)V

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEp:Lcom/android/settings/enterprise/o;

    new-instance v0, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

    invoke-direct {v0, v1}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/SecuritySettings;->bEt:Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/SecuritySettings;->bEk:Lcom/android/settings/cx;

    invoke-virtual {v3}, Lcom/android/settings/cx;->bSR()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v3

    const-string/jumbo v4, "visiblepattern_profile"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v4, p0, Lcom/android/settings/SecuritySettings;->bED:Landroid/os/UserManager;

    iget v5, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-static {v0, v4, v5}, Lcom/android/settings/aq;->bqu(Landroid/content/Context;Landroid/os/UserManager;I)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v3, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->setVisiblePatternEnabled(ZI)V

    :cond_1
    :goto_0
    return v2

    :cond_2
    const-string/jumbo v4, "unification"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/SecuritySettings;->bED:Landroid/os/UserManager;

    iget v4, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-static {v0, v3, v4}, Lcom/android/settings/aq;->bqu(Landroid/content/Context;Landroid/os/UserManager;I)Z

    move-result v0

    if-eqz v0, :cond_3

    return v1

    :cond_3
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v3, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v0, v3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v0

    const/high16 v3, 0x10000

    if-lt v0, v3, :cond_4

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeAllowedToUnify(I)Z

    move-result v0

    :goto_1
    invoke-static {v0}, Lcom/android/settings/SecuritySettings$UnificationConfirmationDialog;->buW(Z)Lcom/android/settings/SecuritySettings$UnificationConfirmationDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v3, "unification_dialog"

    invoke-virtual {v0, v1, v3}, Lcom/android/settings/SecuritySettings$UnificationConfirmationDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f12130b

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/android/settings/cx;

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v1, v3, p0}, Lcom/android/settings/cx;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    sget v3, Lcom/android/settings/SecuritySettings;->bEh:I

    const/16 v4, 0x82

    invoke-virtual {v1, v4, v0, v2, v3}, Lcom/android/settings/cx;->bSS(ILjava/lang/CharSequence;ZI)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buH()V

    goto :goto_0

    :cond_6
    const-string/jumbo v4, "show_password"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v4

    const-string/jumbo v5, "show_password"

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    move v1, v2

    :cond_7
    invoke-static {v4, v5, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sget v1, Lcom/android/settings/SecuritySettings;->bEh:I

    invoke-virtual {v3, v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->setVisiblePasswordEnabled(ZI)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buw()Landroid/preference/PreferenceScreen;

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEH:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEH:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/android/settings/SecuritySettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v4, p0, Lcom/android/settings/SecuritySettings;->bEw:I

    invoke-virtual {v3, v4}, Lcom/android/internal/widget/LockPatternUtils;->isVisiblePatternEnabled(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/SecuritySettings;->buI()V

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEz:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/SecuritySettings;->bEz:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "show_password"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_5

    :goto_0
    invoke-virtual {v2, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEG:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->isUserExperienceProgramEnable(Landroid/content/ContentResolver;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEG:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEF:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/SecuritySettings;->bWB()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->isUploadDebugLogEnable(Landroid/content/ContentResolver;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEF:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEs:Lcom/android/settings/location/LocationPreferenceController;

    invoke-virtual {v0}, Lcom/android/settings/location/LocationPreferenceController;->updateSummary()V

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEx:Lcom/android/settingslib/MiuiRestrictedPreference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEx:Lcom/android/settingslib/MiuiRestrictedPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/MiuiRestrictedPreference;->cqf()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEx:Lcom/android/settingslib/MiuiRestrictedPreference;

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEr:Landroid/security/KeyStore;

    invoke-virtual {v1}, Landroid/security/KeyStore;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/MiuiRestrictedPreference;->setEnabled(Z)V

    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "trust_agent_click_intent"

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEB:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method setLockscreenPreferencesSummary(Landroid/preference/PreferenceGroup;)V
    .locals 2

    const-string/jumbo v0, "lockscreen_preferences"

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/SecuritySettings;->bEt:Lcom/android/settings/notification/LockScreenNotificationPreferenceController;

    invoke-virtual {v1}, Lcom/android/settings/notification/LockScreenNotificationPreferenceController;->m()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    :cond_0
    return-void
.end method
