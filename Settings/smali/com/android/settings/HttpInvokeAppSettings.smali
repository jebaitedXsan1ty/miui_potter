.class public Lcom/android/settings/HttpInvokeAppSettings;
.super Lmiui/preference/PreferenceActivity;
.source "HttpInvokeAppSettings.java"


# instance fields
.field private cdP:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public bVK()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/HttpInvokeAppSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/MiuiSettings$Secure;->isHttpInvokeAppEnable(Landroid/content/ContentResolver;)Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiui/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f150063

    invoke-virtual {p0, v0}, Lcom/android/settings/HttpInvokeAppSettings;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "http_invoke_app"

    invoke-virtual {p0, v0}, Lcom/android/settings/HttpInvokeAppSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/HttpInvokeAppSettings;->cdP:Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/HttpInvokeAppSettings;->cdP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/HttpInvokeAppSettings;->bVK()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/HttpInvokeAppSettings;->cdP:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/kP;

    invoke-direct {v1, p0}, Lcom/android/settings/kP;-><init>(Lcom/android/settings/HttpInvokeAppSettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method
